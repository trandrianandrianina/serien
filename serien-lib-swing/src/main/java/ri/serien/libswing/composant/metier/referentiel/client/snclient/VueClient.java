/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.client.snclient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.EnumChampClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.CategorieClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.exploitation.configurationtableau.SNBoutonConfigurationTableau;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueClient extends AbstractVueDialogue<ModeleClient> {
  private static final int LARGEUR_MAX_BOITE_DIALOGUE = 1170;
  //@formatter:off
  public final static String RECHERCHE_GENERIQUE_TOOLTIP = "<html>"
      + "Recherche sur :<br>"
      + "- code client (entier)<br>"
      + "- nom ou raison sociale (début)<br>"
      + "- complément de nom (début)<br>"
      + "- mot de classement 1 (début)<br>"
      + "- mot de classement 2 (début)<br>"
      + "- téléphone (entier)<br>"
      + "- numéro SIREN (entier).<br>"
      + "</html>";
  //@formatter:on
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Constructeur.
   * @param pModele Le modèle du composant client.
   */
  public VueClient(ModeleClient pModele) {
    super(pModele);
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Construire le composant.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(LARGEUR_MAX_BOITE_DIALOGUE, 380);
    setResizable(false);
    
    // Paramétrage du bouton des préférences de colonnes
    btnConfigurationTableau.setSession(getModele().getSession());
    btnConfigurationTableau.setConfigurationTableau(getModele().getConfigurationTableau());
    
    // Infobulle pour description des champs utilisés pour la recherche
    tfTexteRecherche.setToolTipText(RECHERCHE_GENERIQUE_TOOLTIP);
    
    // Configurer le tableau
    constuireComposantListeClient();

    // Permet de redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER ferme la fenêtre
        if (tblListeClient.getSelectedRowCount() > 0) {
          getModele().quitterAvecValidation();
        }
      }
    };
    tblListeClient.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    
    // Etre notifier des changements de sélection
    tblListeClient.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeClientsValueChanged(e);
      }
    });
    
    // Configurer la barre de boutons
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Rafraichir le composant.
   */
  @Override
  public void rafraichir() {
    rafraichirTexteRecherche();
    rafraichirTitreListeClients();
    rafraichirCodePostalCommune();
    rafraichirTypeClient();
    rafraichirListeClient();
    rafraichirBoutonValider();
    
    if (getModele().getListeClientBase() != null && !getModele().getListeClientBase().isEmpty()) {
      tblListeClient.requestFocus();
    }
    else {
      tfTexteRecherche.requestFocus();
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Construire le composant contenant la liste des données du client.
   */
  private void constuireComposantListeClient() {
    // Configurer le tableau
    scpListeClients.getViewport().setBackground(Color.WHITE);
    tblListeClient.personnaliserAspect(getModele().getListeColonneConfigurationTableau().getTableTitreColonne(),
        getModele().getListeColonneConfigurationTableau().getTableTailleMinimumColonne(),
        getModele().getListeColonneConfigurationTableau().getTableTailleMaximumColonne(),
        getModele().getListeColonneConfigurationTableau().getTableJustificationColonne(), 13);
    tblListeClient.personnaliserAspectCellules(new JTableClientRenderer());

    // Rafraichir la largeur de la boite de dialogue suivant la largeur minimum des colonnes
    int largeurTheorique = 100;
    for (int tailleMinimumcolonne : getModele().getListeColonneConfigurationTableau().getTableTailleMinimumColonne()) {
      largeurTheorique += tailleMinimumcolonne;
    }
    setSize(largeurTheorique, 380);
    // A Améliorer plus tard afin que l'ascenseur horizontal apparaisse au dela d'une certaine largeur
    /*
    if (largeurTheorique < LARGEUR_MAX_BOITE_DIALOGUE) {
      setSize(largeurTheorique, 380);
    }
    else {
      scpListeClients.setAutoscrolls(true);
      scpListeClients.setPreferredSize(new Dimension(largeurTheorique, 380));
      tblListeClient.setPreferredSize(new Dimension(LARGEUR_MAX_BOITE_DIALOGUE, 380));
    }*/
  }

  private void rafraichirTexteRecherche() {
    if (getModele().getTexteRecherche() != null) {
      tfTexteRecherche.setText(getModele().getTexteRecherche());
    }
    else {
      tfTexteRecherche.setText("");
    }
  }

  private void rafraichirTitreListeClients() {
    lbTitreListe.setMessage(getModele().getTitreListe());
  }

  private void rafraichirCodePostalCommune() {
    CodePostalCommune codePostalCommune = getModele().getCodePostalCommune();
    snCodePostalCommune.setSession(getModele().getSession());
    snCodePostalCommune.setIdEtablissement(getModele().getIdEtablissement());
    snCodePostalCommune.charger(false);
    snCodePostalCommune.setSelection(codePostalCommune);
  }

  private void rafraichirTypeClient() {
    if (cbTypeCompteClient.getItemCount() == 0) {
      cbTypeCompteClient.addItem("Tous");
      for (EnumTypeCompteClient typeCompteClient : EnumTypeCompteClient.values()) {
        cbTypeCompteClient.addItem(typeCompteClient);
      }
      if (!getModele().isRechercheProspectAutorisee()) {
        cbTypeCompteClient.removeItem(EnumTypeCompteClient.PROSPECT);
      }
    }
    
    if (getModele().getTypeCompteClient() != null) {
      cbTypeCompteClient.setSelectedItem(getModele().getTypeCompteClient());
    }
    else {
      cbTypeCompteClient.setSelectedIndex(0);
    }
  }

  private void rafraichirBoutonValider() {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, tblListeClient.getSelectedRow() >= 0);
  }

  private void rafraichirListeClient() {
    // Récupérer la liste des clients à afficher
    List<ClientBase> listeClientBase = getModele().getListeClientBase();
    if (listeClientBase == null) {
      tblListeClient.mettreAJourDonnees(null);
      return;
    }

    // Mettre à jour le tableau
    String[][] donnees = null;
    donnees = new String[listeClientBase.size()][getModele().getListeColonneConfigurationTableau().size()];
    for (int ligne = 0; ligne < listeClientBase.size(); ligne++) {
      ClientBase clientBase = listeClientBase.get(ligne);
      for (int colonne = 0; colonne < getModele().getListeColonneConfigurationTableau().size(); colonne++) {
        int numeroChampMetier = getModele().getListeColonneConfigurationTableau().get(colonne).getChampMetier().getId().getNumero();
        switch (EnumChampClient.valueOfByCode(numeroChampMetier)) {
          case NUMERO:
            donnees[ligne][colonne] = clientBase.getId().getTexte();
            break;
          case CATEGORIE:
            CategorieClient categorieCLient = null;
            if (getModele().getListeCategorieClient() != null) {
              categorieCLient = getModele().getListeCategorieClient().get(clientBase.getIdCategorieClient());
            }
            if (categorieCLient != null) {
              donnees[ligne][colonne] = categorieCLient.getLibelle();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;
          case CIVILITE:
            if (clientBase.getAdresse() != null && clientBase.getAdresse().getIdCivilite() != null) {
              donnees[ligne][colonne] = clientBase.getAdresse().getIdCivilite().getTexte();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;
          case CODE_POSTAL:
            if (clientBase.getAdresse() != null) {
              donnees[ligne][colonne] = clientBase.getAdresse().getCodePostalFormate();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;
          case COMPLEMENT:
            if (clientBase.getAdresse() != null) {
              donnees[ligne][colonne] = clientBase.getAdresse().getComplementNom();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;
          case COMPTANT:
            donnees[ligne][colonne] = clientBase.getTypeCompteClient().getLibelle();
            break;
          case LOCALISATION:
            if (clientBase.getAdresse() != null) {
              donnees[ligne][colonne] = clientBase.getAdresse().getLocalisation();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;
          case NOM:
            if (clientBase.getAdresse() != null) {
              donnees[ligne][colonne] = clientBase.getAdresse().getNom();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;
          case PAYS:
            if (clientBase.getAdresse() != null) {
              donnees[ligne][colonne] = clientBase.getAdresse().getPays();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;
          case RUE:
            if (clientBase.getAdresse() != null) {
              donnees[ligne][colonne] = clientBase.getAdresse().getRue();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;
          case TELEPHONE:
            donnees[ligne][colonne] = clientBase.getNumeroTelephone();
            break;
          case TYPE:
            if (clientBase.getTypeImageClient() != null) {
              donnees[ligne][colonne] = clientBase.getTypeImageClient().getLibelle();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;
          case COMMUNE:
            if (clientBase.getAdresse() != null) {
              donnees[ligne][colonne] = clientBase.getAdresse().getVille();
            }
            else {
              donnees[ligne][colonne] = "";
            }
            break;

          default:
            donnees[ligne][colonne] = "";
            break;
        }
      }
    }

    tblListeClient.mettreAJourDonnees(donnees);

    // Sélectionner le client adéquat
    ClientBase clientBase = getModele().getClientSelectionne();
    if (clientBase != null) {
      int index = listeClientBase.indexOf(clientBase);
      if (index >= 0 && index < tblListeClient.getRowCount()) {
        tblListeClient.setRowSelectionInterval(index, index);
      }
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().lancerRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      tfTexteRecherche.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfTexteRechercheFocusLost(FocusEvent e) {
    try {
      try {
        getModele().modifierTexteRecherche(tfTexteRecherche.getText());
      }
      catch (Exception exception) {
        DialogueErreur.afficher(exception);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfTexteRechercheActionPerformed(ActionEvent e) {
    try {
      getModele().modifierTexteRecherche(tfTexteRecherche.getText());
      getModele().lancerRecherche();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void cbTypeCompteClientItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbTypeCompteClient.getSelectedItem() instanceof EnumTypeCompteClient) {
        getModele().modifierTypeCompteClient((EnumTypeCompteClient) cbTypeCompteClient.getSelectedItem());
      }
      else {
        getModele().modifierTypeCompteClient(null);
      }
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tblListeClientsValueChanged(ListSelectionEvent e) {
    try {
      if (!isEvenementsActifs() && !e.getValueIsAdjusting()) {
        return;
      }
      ClientBase clientBase = null;
      int index = tblListeClient.getSelectedRow();
      if (index >= 0 && index < tblListeClient.getRowCount()) {
        int indexModele = tblListeClient.convertRowIndexToModel(index);
        if (indexModele >= 0 || indexModele < getModele().getListeClientBase().size()) {
          clientBase = getModele().getListeClientBase().get(indexModele);
        }
      }
      getModele().setClientSelectionne(clientBase);
      rafraichirBoutonValider();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tblListeClientsMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void snCodePostalCommuneValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs() || !isDonneesChargees()) {
        return;
      }
      getModele().modifierCodePostalCommune(snCodePostalCommune.getSelection());
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Afficher la boite de dialogue de la personnalisation des colonnes.
   * @param pActionEvent L'évènement déclenché.
   */
  private void btnConfigurationTableauActionPerformed(ActionEvent pActionEvent) {
    try {
      if (btnConfigurationTableau.afficher()) {
        // L'utilisateur n'a effectué aucune modification
        if (btnConfigurationTableau.isSortieAvecAnnulation()) {
          return;
        }

        // Construire le composant avec les nouvelles colonnes sélectionnées
        constuireComposantListeClient();
        rafraichir();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new JPanel();
    pnlRecherche = new JPanel();
    pnlRechercheGauche = new JPanel();
    lbTexteRecherche = new SNLabelChamp();
    tfTexteRecherche = new SNTexte();
    lbTypeCompteClient = new SNLabelChamp();
    cbTypeCompteClient = new SNComboBox();
    lbCommune = new SNLabelChamp();
    snCodePostalCommune = new SNCodePostalCommune();
    pnlRechercheDroite = new JPanel();
    snBarreRecherche = new SNBarreRecherche();
    pnlTitreTableau = new SNPanel();
    lbTitreListe = new SNLabelTitre();
    btnConfigurationTableau = new SNBoutonConfigurationTableau();
    scpListeClients = new JScrollPane();
    tblListeClient = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1200, 600));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("S\u00e9lection d'un client");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setPreferredSize(new Dimension(950, 400));
        pnlContenu.setMinimumSize(new Dimension(950, 350));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 37, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlRecherche ========
        {
          pnlRecherche.setOpaque(false);
          pnlRecherche.setBorder(new TitledBorder(""));
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlRechercheGauche ========
          {
            pnlRechercheGauche.setOpaque(false);
            pnlRechercheGauche.setBorder(null);
            pnlRechercheGauche.setName("pnlRechercheGauche");
            pnlRechercheGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWidths = new int[] { 165, 300, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbTexteRecherche ----
            lbTexteRecherche.setText("Recherche g\u00e9n\u00e9rique");
            lbTexteRecherche.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTexteRecherche.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTexteRecherche.setPreferredSize(new Dimension(100, 30));
            lbTexteRecherche.setName("lbTexteRecherche");
            pnlRechercheGauche.add(lbTexteRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfTexteRecherche ----
            tfTexteRecherche.setName("tfTexteRecherche");
            tfTexteRecherche.addActionListener(e -> tfTexteRechercheActionPerformed(e));
            tfTexteRecherche.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                tfTexteRechercheFocusGained(e);
              }
              
              @Override
              public void focusLost(FocusEvent e) {
                tfTexteRechercheFocusLost(e);
              }
            });
            pnlRechercheGauche.add(tfTexteRecherche, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTypeCompteClient ----
            lbTypeCompteClient.setText("Type");
            lbTypeCompteClient.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTypeCompteClient.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTypeCompteClient.setPreferredSize(new Dimension(100, 30));
            lbTypeCompteClient.setName("lbTypeCompteClient");
            pnlRechercheGauche.add(lbTypeCompteClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbTypeCompteClient ----
            cbTypeCompteClient.setMinimumSize(new Dimension(200, 30));
            cbTypeCompteClient.setPreferredSize(new Dimension(200, 30));
            cbTypeCompteClient.setName("cbTypeCompteClient");
            cbTypeCompteClient.addItemListener(e -> cbTypeCompteClientItemStateChanged(e));
            pnlRechercheGauche.add(cbTypeCompteClient, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCommune ----
            lbCommune.setText("Commune");
            lbCommune.setHorizontalAlignment(SwingConstants.RIGHT);
            lbCommune.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbCommune.setPreferredSize(new Dimension(100, 30));
            lbCommune.setName("lbCommune");
            pnlRechercheGauche.add(lbCommune, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snCodePostalCommune ----
            snCodePostalCommune.setName("snCodePostalCommune");
            snCodePostalCommune.addSNComposantListener(e -> snCodePostalCommuneValueChanged(e));
            pnlRechercheGauche.add(snCodePostalCommune, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheGauche);
          
          // ======== pnlRechercheDroite ========
          {
            pnlRechercheDroite.setOpaque(false);
            pnlRechercheDroite.setName("pnlRechercheDroite");
            pnlRechercheDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            
            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlRechercheDroite.add(snBarreRecherche, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheDroite);
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTitreTableau ========
        {
          pnlTitreTableau.setName("pnlTitreTableau");
          pnlTitreTableau.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTitreTableau.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTitreTableau.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTitreTableau.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTitreTableau.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbTitreListe ----
          lbTitreListe.setText("Clients correspondant \u00e0 votre recherche");
          lbTitreListe.setName("lbTitreListe");
          pnlTitreTableau.add(lbTitreListe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- btnConfigurationTableau ----
          btnConfigurationTableau.setName("btnConfigurationTableau");
          btnConfigurationTableau.addActionListener(e -> btnConfigurationTableauActionPerformed(e));
          pnlTitreTableau.add(btnConfigurationTableau, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlTitreTableau, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeClients ========
        {
          scpListeClients.setPreferredSize(new Dimension(950, 424));
          scpListeClients.setName("scpListeClients");
          
          // ---- tblListeClient ----
          tblListeClient.setShowVerticalLines(true);
          tblListeClient.setShowHorizontalLines(true);
          tblListeClient.setBackground(Color.white);
          tblListeClient.setRowHeight(20);
          tblListeClient.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeClient.setSelectionBackground(new Color(57, 105, 138));
          tblListeClient.setGridColor(new Color(204, 204, 204));
          tblListeClient.setPreferredSize(new Dimension(950, 220));
          tblListeClient.setName("tblListeClient");
          tblListeClient.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeClientsMouseClicked(e);
            }
          });
          scpListeClients.setViewportView(tblListeClient);
        }
        pnlContenu.add(scpListeClients, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlContenu;
  private JPanel pnlRecherche;
  private JPanel pnlRechercheGauche;
  private SNLabelChamp lbTexteRecherche;
  private SNTexte tfTexteRecherche;
  private SNLabelChamp lbTypeCompteClient;
  private SNComboBox cbTypeCompteClient;
  private SNLabelChamp lbCommune;
  private SNCodePostalCommune snCodePostalCommune;
  private JPanel pnlRechercheDroite;
  private SNBarreRecherche snBarreRecherche;
  private SNPanel pnlTitreTableau;
  private SNLabelTitre lbTitreListe;
  private SNBoutonConfigurationTableau btnConfigurationTableau;
  private JScrollPane scpListeClients;
  private NRiTable tblListeClient;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
