/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.commun.sncivilite;

import ri.serien.libcommun.exploitation.personnalisation.civilite.Civilite;
import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.exploitation.personnalisation.civilite.ListeCivilite;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection des civilite.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une Civilite dans le logiciel.
 * Il gère l'affichage des Civilite existantes suivant l'établissement et gère la sélection de l'une de ces Civilite
 *
 * Utilisation :
 * - Ajouter un composant SNCivilite à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 */
public class SNCivilite extends SNComboBoxObjetMetier<IdCivilite, Civilite, ListeCivilite> {
  
  /**
   * Constructeur par défaut.
   */
  public SNCivilite() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeCivilite());
  }
  
  /**
   * Sélectionner une Civilite de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampCivilite) {
    if (pChampCivilite == null || pLexical.HostFieldGetData(pChampCivilite) == null) {
      throw new MessageErreurException("Impossible de sélectionner la civilité car son code est invalide.");
    }
    
    IdCivilite idCivilite = null;
    String valeurCivilite = pLexical.HostFieldGetData(pChampCivilite).trim();
    if (valeurCivilite != null && !valeurCivilite.trim().isEmpty() && !valeurCivilite.equals("**")) {
      idCivilite = IdCivilite.getInstance(pLexical.HostFieldGetData(pChampCivilite));
    }
    setIdSelection(idCivilite);
  }
  
  /**
   * Renseigner les champs RPG correspondant à la civilité sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampCivilite) {
    Civilite civilite = getSelection();
    if (civilite != null) {
      pLexical.HostFieldPutData(pChampCivilite, 0, civilite.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampCivilite, 0, "");
    }
  }
  
}
