/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.transport.snlieutransport;

import ri.serien.libcommun.gescom.personnalisation.lieutransport.IdLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.LieuTransport;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.ListeLieuTransport;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

public class SNLieuTransport extends SNComboBoxObjetMetier<IdLieuTransport, LieuTransport, ListeLieuTransport> {
  
  /**
   * Constructeur par défaut.
   */
  public SNLieuTransport() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeLieuTransport());
  }
  
  /**
   * Sélectionner un lieu transport de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampLieuTransport) {
    if (pChampLieuTransport == null || pLexical.HostFieldGetData(pChampLieuTransport) == null) {
      throw new MessageErreurException("Impossible de sélectionner le lieu de transport car il est invalide.");
    }
    
    IdLieuTransport idLieuTransport = null;
    String valeurTransporteur = pLexical.HostFieldGetData(pChampLieuTransport).trim();
    if (valeurTransporteur != null && !valeurTransporteur.trim().isEmpty() && !valeurTransporteur.equals("**")) {
      idLieuTransport = IdLieuTransport.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampLieuTransport));
    }
    setIdSelection(idLieuTransport);
    
  }
  
  /**
   * Renseigner le champs RPG correspondant au lieu de transport sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampLieuTransport) {
    LieuTransport lieuTransport = getSelection();
    if (lieuTransport != null) {
      pLexical.HostFieldPutData(pChampLieuTransport, 0, lieuTransport.getId().getTexte());
    }
    else {
      pLexical.HostFieldPutData(pChampLieuTransport, 0, "");
    }
  }
}
