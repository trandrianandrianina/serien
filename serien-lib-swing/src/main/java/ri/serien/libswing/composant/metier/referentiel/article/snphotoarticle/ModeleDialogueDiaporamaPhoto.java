/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.LookAndFeel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;

import ri.serien.libcommun.gescom.commun.imagearticle.IdImageArticle;
import ri.serien.libcommun.gescom.commun.imagearticle.ImageArticle;
import ri.serien.libcommun.gescom.commun.imagearticle.ListeImageArticle;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.SessionTransfert;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.confirmation.ModeleDialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composantrpg.autonome.FileDrop;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Gestion des données associées à l'affichage de la boite de dialogue de la visualisation des images associées à un article.
 *
 * Cette classe passe par la création une liste d'images associées à un article sous la forme de l'objet métier ImageArticle.
 * Cette liste est créée à partir d'un chemin d'accès à un dossier passé en paramètre.
 * Elle est chargée avec tous les fichiers de type image contenus dans ce dossier.
 *
 * Lorsqu'une modification est demandée par l'utilisateur :
 * - en cas de suppression, une nouvelle liste est générée, contenant les ImageArticle à supprimer.
 * - en cas d'ajout, une nouvelle liste est générée, contenant les ImagesArticles à ajouter.
 *
 * La liste d'affichage est modifiée en fonction des actions de l'utilisateur.
 * Lors de la sortie de la boîte de dialogue, si les modifications sont validée, elles sont appliquées aux images stockées.
 * Sinon, aucune modification n'est conservée.
 *
 */

public class ModeleDialogueDiaporamaPhoto extends AbstractModeleDialogue {
  // Constantes
  private final static String ZOOM_PLUS = "+";
  private final static String ZOOM_MOINS = "-";

  // Zoom de plus ou moins 10%
  private final static double FACTEUR_ZOOM_PLUS = 1.1;
  private final static double FACTEUR_ZOOM_MOINS = 0.9;
  private final static double FACTEUR_ZOOM_DEFAUT = 1;

  // Défini comme le facteur de ratio maximal à appliquer sur une image.
  // La taille d'affichage de l'image aura été multipliée par 3.
  private final static double RATIO_MAXIMUM = 3;
  // Défini comme le facteur de ratio minimal à appliquer sur une image.
  // La taille d'affichage de l'image aura été divisée par 5.
  private static final double RATIO_MINIMUM = 0.2;

  // Image affichée si la liste d'image est vide.
  private static final ImageIcon LOGO_PAS_DE_PHOTO =
      new ImageIcon(ModeleDialogueDiaporamaPhoto.class.getClassLoader().getResource("images/pas_photo.png"));
  private static final String TEXTE_PAS_DE_PHOTO =
      "En modification, déposez une image ou cliquez sur le bouton ajouter pour lier une photo à cet article.";

  // Variables
  private ManagerSessionClient ManagerSessionClient = null;
  private SessionTransfert transfertSession = null;

  private String dossierServeur = null;
  private ListeImageArticle listePhotoInitiale = new ListeImageArticle();
  private ListeImageArticle listePhotoPourAffichage = new ListeImageArticle();
  private List<IdImageArticle> listePhotoPourAjout = new ArrayList<IdImageArticle>();
  private List<IdImageArticle> listePhotoPourSuppression = new ArrayList<IdImageArticle>();

  private String nomPhotoPourAffichage = null;
  private Icon representationGraphiquePhotoPourAffichage = null;

  // Permet de gérer l'affichage du composant navigation de la barre de boutons
  private int nombrePhotoDansDiaporama = 0;
  private int indexImagePourAffichage = 0;

  private double facteurZoom = FACTEUR_ZOOM_DEFAUT;
  private double ratio = 1;

  private double largeurImage = 1;
  private double hauteurImage = 1;
  private int largeurComposantAffichage = 1;
  private int hauteurComposantAffichage = 1;

  private boolean isIFS = true;
  private boolean isZoomMini = false;
  private boolean isZoomMaxi = false;
  private boolean isTailleImageOrigine = true;
  
  // Si le paramétrage du composant ne comporte pas l'information sur l'état de consultation,
  // alors on considère que la modification n'est jamais possible.
  private boolean isConsultation = true;

  /**
   * Constructeur.
   * Sans mention de l'état de consultation.
   *
   * @param pEnvUser
   * @param pCheminDAcces
   */
  public ModeleDialogueDiaporamaPhoto(EnvUser pEnvUser, String pCheminDAccesDossier) {
    super(null);

    controlerPathIFS(pCheminDAccesDossier);
    dossierServeur = pCheminDAccesDossier;
    
    transfertSession = pEnvUser.getTransfertSession();
    chargerDonnees();
  }
  
  /**
   * Constructeur.
   * Avec mention de l'état de consultation.
   *
   * @param pEnvUser
   * @param pCheminDAcces
   */
  public ModeleDialogueDiaporamaPhoto(EnvUser pEnvUser, String pCheminDAccesDossier, boolean pIsConsultation) {
    super(null);

    controlerPathIFS(pCheminDAccesDossier);
    dossierServeur = pCheminDAccesDossier;
    isConsultation = pIsConsultation;
    
    transfertSession = pEnvUser.getTransfertSession();
    chargerDonnees();
  }

  // -- Méthodes publiques
  /**
   * Initialiser les données.
   *
   * @see AbstractModeleDialogue
   */
  @Override
  public void initialiserDonnees() {
    // On crée une liste d'IdImageArticle
    List<IdImageArticle> listeIdImageArticle = new ArrayList<IdImageArticle>();
    // On récupère les fichiers contenus dans le dossier
    String[] tableauCheminDAccesFichier = transfertSession.listerDossier(dossierServeur);
    // Si l'opération visant à lister le contenu du dossier retourne null, on informe l'utilisteur que le paramétrage du lecteur réseau
    // peut être mal fait et qu'il est impossible d'accéder aux images liées à un article.
    if (tableauCheminDAccesFichier == null) {
      throw new MessageErreurException("Impossible de visualiser ou d'ajouter des photos à l'article. \n"
          + "La racine du chemin d'accès aux photos liées à un article est invalide. \n\n"
          + "Il se peut que le paramétrage ne soit pas fait ou que le lecteur réseau paramétré ne soit pas créé sur votre poste.");
    }
    for (String fichier : tableauCheminDAccesFichier) {
      // Si le fichier est une image
      if (isFichierImage(fichier)) {
        // On crée une IdImageArticle et on l'ajoute à la liste
        IdImageArticle idImageArticle = IdImageArticle.getInstance(dossierServeur, fichier);
        listeIdImageArticle.add(idImageArticle);
      }
    }

    // On charge la liste initiale via la liste d'Id
    listePhotoInitiale = new ListeImageArticle().charger(null, listeIdImageArticle);
    // On initialise la liste des images à afficher à partir de la liste initiale
    // On utilise 2 listes afin de pouvoir effectuer une comparaison et créer les liste des images à ajouter et à supprimer à la
    // validation des opérations.
    initialiserListeImagePourAffichage();
  }

  /**
   * Charger les données.
   *
   * @see AbstractModeleDialogue
   */
  @Override
  public void chargerDonnees() {
    afficherImage();
  }

  /**
   * Sélectionner l'image suivante à afficher dans la liste, si elle existe.
   */
  public void afficherImageSuivante() {
    // Si l'index de l'image à affiché n'est pas supérieur au nombre d'images à afficher,
    if (indexImagePourAffichage <= nombrePhotoDansDiaporama) {
      // On incrémente l'index, et on affiche l'image correspondant.
      indexImagePourAffichage++;
      afficherImage();
    }
    else {
      // Sinon, rien ne se passe.
      return;
    }
  }

  /**
   * Sélectionner l'image précédente à afficher dans la liste, si elle existe.
   */
  public void afficherImagePrecedente() {
    // Si l'index de l'image à afficher est supérieur à 0,
    if (indexImagePourAffichage > 0) {
      // On décrémente l'index, et on affiche l'image correspondant.
      indexImagePourAffichage--;
      afficherImage();
    }
    else {
      // Sinon, rien ne se passe.
      return;
    }
  }

  /**
   * Actualiser l'image à afficher en fonction du zoom demandé par l'utilisateur
   *
   * @param pZoom + ou - en fonction du zoom désiré.
   */
  public void gererZoomImage(String pZoom) {
    // Si l'utilisateur a demandé un agrandissement de l'affichage de l'image,
    if (Constantes.equals(pZoom, ZOOM_PLUS)) {
      // La variable de facteur de zoom prend la valeur correspondant à la demande d'agrandissement.
      facteurZoom = FACTEUR_ZOOM_PLUS;
    }
    // Si l'utilisateur a demandé une réduction de l'affichage de l'image,
    else if (Constantes.equals(pZoom, ZOOM_MOINS)) {
      // La variable de facteur de zoom prend la valeur correspondant à la demande de réduction.
      facteurZoom = FACTEUR_ZOOM_MOINS;
    }
    // On affiche l'affichage en fonction de cette demande
    afficherImage();
    // On repasse la variable à sa valeur par défaut.
    facteurZoom = FACTEUR_ZOOM_DEFAUT;
  }

  /**
   * Gérer l'affichage de l'image en fonction de la taille de la fenêtre. <br>
   * Ajuste l'affichage de l'image afin qu'elle prenne toute la place disponible quand la boîte de dialogue ets redimmensionnée. <br>
   * La taille d'affichage maximale de l'image par cette méthode correspond aux dimensions d'origine de l'image. <br>
   *
   * @param pHauteurComposantAffichage Hauteur du composant d'affichage de l'image article.
   * @param pLargeurComposantAffichage Largeur du composant d'affichage de l'image article.
   */
  public void gererRedimensionnerFenetre(int pHauteurComposantAffichage, int pLargeurComposantAffichage) {
    // Les dimensions du composant d'affichage après redimension de la fenêtre sont stockées dans les variables correspondantes.
    largeurComposantAffichage = pLargeurComposantAffichage;
    hauteurComposantAffichage = pHauteurComposantAffichage;

    // On récupère l'image à l'index correspondant dans la liste.
    ImageIcon imageIcon = listePhotoPourAffichage.get(indexImagePourAffichage).getIconeImage();

    // On calcule le ratio de la taille de l'image par rapport à la taille de la zone d'affichage de l'image.
    ratio = getRatio(imageIcon);
    // Si le ratio est inférieur au ratio minimum autorisé, on indique que le zoom minimal a été atteint.
    isZoomMini = ratio < RATIO_MINIMUM;
    // Si le ratio est supérieur au ratio maximum autorise, on indique que le zoom maximal a été atteint.
    isZoomMaxi = ratio > RATIO_MAXIMUM;
    
    // Si le zoom minimum a été atteint, on affiche l'image avec le ratio minimum.
    if (isZoomMini) {
      representationGraphiquePhotoPourAffichage = redimensionnerImage(imageIcon, RATIO_MINIMUM);

    }
    // Si le zoom maximum a été atteint, on affiche l'image avec le ratio maximum.
    else if (isZoomMaxi) {
      representationGraphiquePhotoPourAffichage = redimensionnerImage(imageIcon, RATIO_MAXIMUM);
    }
    // Sinon, on affiche l'image avec le ratio calculé.
    else {
      representationGraphiquePhotoPourAffichage = redimensionnerImage(imageIcon, ratio);
    }
    
    // On rafraîchit l'affichage de la vue.
    rafraichir();
  }

  /**
   * Ajouter une ou plusieurs images à la liste des images via l'explorateur de fichier.
   * L'explorateur est un JFileChooser qui prend l'apparence du système d'exploitation de l'utilisateur.
   */
  public void ajouterImageParExplorateur() {
    // On stocke l'apparence par défaut du logiciel.
    LookAndFeel defautLookAndFeel = UIManager.getLookAndFeel();
    try {
      // On applique l'apparence associée au système de l'utilisateur si possible.
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (Exception e) {
      return;
    }
    
    // On crée l'explorateur de fichier, avec le dossier "home" de l'utilisateur.
    JFileChooser jFileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
    // On applique le paramétrage personnalisé de la boîte de dialogue explorateur de fichier.
    parametrerApparenceFileChooser(jFileChooser);
    
    // On ouvre l'explorateur de fichier.
    int returnValue = jFileChooser.showOpenDialog(null);
    
    // On rétablit l'apparence par défaut pour ne pas créer d'erreur dans le logiciel.
    try {
      UIManager.setLookAndFeel(defautLookAndFeel);
    }
    catch (UnsupportedLookAndFeelException e) {
      return;
    }

    // Si l'utilisateur a validé la sélection des fichiers dans l'explorateur,
    if (returnValue == JFileChooser.APPROVE_OPTION) {
      // On récupère le ou les fichiers sélectionnés dans un tableau.
      ajouterImage(jFileChooser.getSelectedFiles());

    }
  }

  /**
   * Gestion de l'ajout d'image par glisser/déposer.
   *
   * @param composantAffichageImage Composant permettant d'afficher les images dans la vue et qui reçoit le glisser/déposer.
   */
  public void gererGlisserDeposer(final JLabel composantAffichageImage) {

    new FileDrop(null, composantAffichageImage, new FileDrop.Listener() {
      
      // On récupère les objets déposés par l'utilisateur dans un tableau.
      @Override
      public void filesDropped(File[] pListeFichier) {
        // On ajoute les images aux images affichées.
        ajouterImage(pListeFichier);
      }
    });
    
  }

  /**
   * Supprimer l'image courante de la liste des images affichées.
   *
   * La suppression demande une confirmation à l'utilisateur.
   */
  public void supprimerImagePourAffichage() {
    // On demande la confirmation de l'opération à l'utilisateur.
    // Si l'utilisateur refuse,
    if (!isConfirmerDemande("Suppression de la photo liée à un article", "Vous avez demandé la suppression de l'image "
        + listePhotoPourAffichage.get(indexImagePourAffichage).getId().getNomImageArticle() + "\n Confirmez-vous cette suppression ?")) {
      // Rien ne se passe après la fermeture de la boîte de dialogue de demande de confirmation.
      return;
    }
    
    // On supprime l'image article de la liste des images à afficher.
    listePhotoPourAffichage.remove(listePhotoPourAffichage.get(indexImagePourAffichage));
    
    // On affiche l'image à la même position que celle avant suppression si elle existe, sinon on affiche l'image à la postion précédente.
    if (indexImagePourAffichage > 0 && indexImagePourAffichage == nombrePhotoDansDiaporama - 1) {
      indexImagePourAffichage--;
    }
    // On actualise le nombre de photo présente dans le diaporama.
    nombrePhotoDansDiaporama = listePhotoPourAffichage.size();
    
    // On affiche l'image.
    afficherImage();
  }

  /**
   * Quitter la boite de dialogue en validant les modifications.
   *
   * Supprime les images contenues dans la liste des images à supprimer.
   * Ajoute les images contenues dans la liste des images à supprimer.
   */
  @Override
  public void quitterAvecValidation() {
    // On génère les listes IdImageArticle à ajouter et à supprimer du serveur.
    genererListeAjoutEtSuppression();
    
    // On applique les modifications sur les images stockées sur le serveur ou en local.
    listePhotoPourAffichage.sauverPhoto(transfertSession, dossierServeur, listePhotoPourAjout);
    listePhotoPourAffichage.supprimerPhoto(transfertSession, dossierServeur, listePhotoPourSuppression);
    
    // On indique que la boîte de dialogue a été quittée en validant.
    super.quitterAvecValidation();
  }

  // -- Méthodes privées

  /**
   * Contrôler la nature du chemin d'accès.
   *
   * @param pCheminDAcces Chemin d'accès aux fichiers
   *
   * @return <b>True</b> si chemin IFS, <b>false</b> si chemin Windows.
   */
  private void controlerPathIFS(String pCheminDAcces) {
    if ((pCheminDAcces.indexOf(':') != -1) || (pCheminDAcces.startsWith("\\\\"))) {
      isIFS = false;
    }
    isIFS = true;
  }

  /**
   * Créer la liste des images à afficher à partir de la liste des images initiales.
   */
  private void initialiserListeImagePourAffichage() {
    // La liste des photos à afficher est initialisée avec la liste des images détectées dans le dossier.
    listePhotoPourAffichage.addAll(listePhotoInitiale);
    if (listePhotoPourAffichage.isEmpty()) {
      // Si la liste est vide, l'index pour l'affichage passe à 0
      indexImagePourAffichage = 0;
    }
    // On récupère le nombre de photos à afficher.
    nombrePhotoDansDiaporama = listePhotoPourAffichage.size();
  }

  /**
   * Définir l'image à afficher sur la vue et son nom.
   *
   * Redimensionne l'image pour l'affichage selon :
   * - Si un zoom a été demandé
   * - Si l'image est plus grande ou non que la zone d'affichage.
   */
  private void afficherImage() {
    // S'il n'y a pas d'images à afficher
    if (listePhotoPourAffichage.isEmpty()) {
      // On affiche un message à l'utilisateur indiquant qu'il n'y a pas de photo à afficher.
      nomPhotoPourAffichage = TEXTE_PAS_DE_PHOTO;
      // On affiche l'image placeholder.
      representationGraphiquePhotoPourAffichage = LOGO_PAS_DE_PHOTO;
    }

    else {
      // On récupère la représentation graphique de l'image dans l'objet métier correspondant.
      ImageIcon imageIcon = listePhotoPourAffichage.get(indexImagePourAffichage).getIconeImage();
      
      // On modifie le titre de l'image.
      nomPhotoPourAffichage = listePhotoPourAffichage.get(indexImagePourAffichage).getId().getNomImageArticle();
      
      // On modifie l'image à afficher.
      // Si un zoom a été demandé,
      if (facteurZoom != FACTEUR_ZOOM_DEFAUT) {
        // On applique le facteur de zoom sur le ratio d'affichage de l'image.
        ratio *= facteurZoom;
      }
      else {
        // Sinon, on calcule le ratio de l'image par rapport à la zone d'affichage pour savoir si sa taille doit être adaptée à
        // l'affichage.
        ratio = getRatio(imageIcon);
      }
      // On applique le ratio à la image pour son affichage.
      representationGraphiquePhotoPourAffichage = redimensionnerImage(imageIcon, ratio);
    }

    // On rafraîchit la vue.
    rafraichir();
  }

  /**
   * Calculer le ratio de l'image par rapport à la taille de la zone d'affichage.
   * Si l'image est plus grande que la zone d'affichage, calcule le ratio pour la redimensionner.
   * Le ratio vaut 1 si l'image est de même taille ou plus petite que la zone d'affichage.
   *
   * @param pImageIcon Image pour laquelle le ratio doit être calculé.
   *
   * @return Rapport entre la taille de l'image et la taille de la zone d'affichage.
   */
  private double getRatio(ImageIcon pImageIcon) {
    // On récupère les dimensions de l'image à afficher dans les variables correspondantes.
    largeurImage = pImageIcon.getIconWidth();
    hauteurImage = pImageIcon.getIconHeight();

    // On initialise les valeurs pour la mise à l'échelle de l'image.
    double valeurMiseAEchelle1 = 0;
    double valeurMiseAEchelle2 = 0;

    // Si l'image est plus petite que la zone d'affichage de l'image,
    if ((largeurImage <= largeurComposantAffichage) && (hauteurImage <= hauteurComposantAffichage)) {
      // On passe la valeur de mise à l'échelle à 1.
      valeurMiseAEchelle1 = 1;
      // On indique que l'image a sa taille d'origine.
      isTailleImageOrigine = true;
    }
    else {
      // Sinon on indique que l'image est modifiée pour l'affichage.
      isTailleImageOrigine = false;
      // On calcule le rapport entre l'image et le composant d'affichage pour la largeur et la hauteur.
      valeurMiseAEchelle1 = largeurComposantAffichage / largeurImage;
      valeurMiseAEchelle2 = hauteurComposantAffichage / hauteurImage;
      
      // On retourne la valeur de rapport la plus faible.
      if (valeurMiseAEchelle1 > valeurMiseAEchelle2) {
        valeurMiseAEchelle1 = valeurMiseAEchelle2;
      }
    }
    return valeurMiseAEchelle1;
  }

  /**
   * Redimensionner l'image affichée.
   *
   * Permet de redimensionner l'image affichée selon un facteur passé en paramètre.
   *
   * @param pImageIcon Image à redimensionner.
   * @param pRatio Facteur pour redimensionner.
   */
  private ImageIcon redimensionnerImage(ImageIcon pImageIcon, double pRatio) {
    // On vérifie si le ratio est inférieur au minimum ou supérieur au maximum.
    isZoomMini = (pRatio < RATIO_MINIMUM);
    isZoomMaxi = (pRatio > RATIO_MAXIMUM);

    // Si le zoom maximal ou minimal n'a pas été atteint,
    if (!(isZoomMini || isZoomMaxi)) {
      // On applique le ratio sur la hauteur et la largeur de l'image.
      largeurImage = pImageIcon.getIconWidth() * pRatio;
      hauteurImage = pImageIcon.getIconHeight() * pRatio;
      // On crée une nouvelle image avec ces dimensions.

      BufferedImage bufferedImage = new BufferedImage((int) (largeurImage), (int) (hauteurImage), BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bufferedImage.createGraphics();
      graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
      graphics.drawImage(pImageIcon.getImage(), 0, 0, (int) (largeurImage), (int) (hauteurImage), null);
      graphics.dispose();
      
      // On retourne cette image redimensionnée.
      return new ImageIcon(bufferedImage);
    }

    // Si les limites ont été atteintes,
    else {
      // On crée une image avec les dimensions existantes.
      BufferedImage bufferedImage = new BufferedImage((int) (largeurImage), (int) (hauteurImage), BufferedImage.TYPE_INT_ARGB);
      Graphics2D graphics = bufferedImage.createGraphics();
      graphics.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
      graphics.drawImage(pImageIcon.getImage(), 0, 0, (int) (largeurImage), (int) (hauteurImage), null);
      graphics.dispose();
      
      // On retourne cette image.
      return new ImageIcon(bufferedImage);
    }
  }

  /**
   * Paramétrer l'apparence du JFileChooser.
   * Définit les filtres de sélection des fichiers et traduit les différents composant de l'interface.
   *
   * @param pJFileChooser
   */
  private void parametrerApparenceFileChooser(JFileChooser pJFileChooser) {
    // Désactiver le filtre "Tous les fichiers".
    pJFileChooser.setAcceptAllFileFilterUsed(false);

    // pJFileChooser.
    // Créer les filtres de recherche.
    FileNameExtensionFilter filter = null;

    // Créer le filtre "Tous les fichiers images"
    filter = new FileNameExtensionFilter("Tous les fichiers images", "jpg", "jpeg", "gif", "png", "bmp");
    pJFileChooser.setFileFilter(filter);
    
    // Créer le filtre "JPEG"
    filter = new FileNameExtensionFilter("JPEG (*.jpeg, *.jpg)", "jpg", "jpeg");
    pJFileChooser.addChoosableFileFilter(filter);
    
    // Créer le filtre "BMP"
    filter = new FileNameExtensionFilter("Fichiers bitmap (*.bmp)", "bmp");
    pJFileChooser.addChoosableFileFilter(filter);
    
    // Créer le filtre "GIF"
    filter = new FileNameExtensionFilter("GIF (*.gif)", "gif");
    pJFileChooser.addChoosableFileFilter(filter);
    
    // Créer le filtre "PNG"
    filter = new FileNameExtensionFilter("PNG (*.png)", "png");
    pJFileChooser.addChoosableFileFilter(filter);

    // Il est possible de sélectionner plusieurs éléments.
    pJFileChooser.setMultiSelectionEnabled(true);

    // Seul des fichiers peuvent être sélectionnés.
    pJFileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

    // Traduire l'interface du JFileChooser
    // L'interface du JFileChooser est disponible en anglais et il n'est pas possible de la traduire avec Locale.French

    // Boutons
    pJFileChooser.setApproveButtonText("Sélectionner");
    UIManager.put("FileChooser.openDialogTitleText", "Sélectionner");
    UIManager.put("FileChooser.cancelButtonText", "Annuler");
    UIManager.put("FileChooser.newFolderButtonText", "Créer un nouveau dossier");
    UIManager.put("FileChooser.renameFileButtonText", "Renommer");
    UIManager.put("FileChooser.deleteFileButtonText", "Supprimer");
    UIManager.put("FileChooser.directoryOpenButtonText", "Ouvrir");

    // Libellés
    UIManager.put("FileChooser.lookInLabelText", "");
    UIManager.put("FileChooser.fileNameLabelText", "Nom du fichier :");
    UIManager.put("FileChooser.filesOfTypeLabelText", "Format de fichier :");
    UIManager.put("FileChooser.filterLabelText", "Format");

    // Tooltips
    pJFileChooser.setApproveButtonToolTipText("Sélectionner un ou plusieurs fichiers");
    UIManager.put("FileChooser.cancelButtonToolTipText", "Annuler");
    UIManager.put("FileChooser.upFolderToolTipText", "Remonter");
    UIManager.put("FileChooser.newFolderToolTipText", "Créer un nouveau dossier");
    UIManager.put("FileChooser.listViewButtonToolTipText", "Liste");
    UIManager.put("FileChooser.detailsViewButtonToolTipText", "Details");

    // En-tête de colonnes en vue Détails
    UIManager.put("FileChooser.fileNameHeaderText", "Nom");
    UIManager.put("FileChooser.fileSizeHeaderText", "Taille");
    UIManager.put("FileChooser.fileDateHeaderText", "Date de modification");
    SwingUtilities.updateComponentTreeUI(pJFileChooser);

  }
  
  /**
   * Ajouter des images à afficher.
   *
   * On contrôle que les objets contenus dans le tableau sont bien des fichiers image lisibles.
   *
   * @param pTableauFichier Tableau contenant des fichiers à afficher
   */
  private void ajouterImage(File[] pTableauFichier) {
    // On crée une liste d'identifiants d'images pour l'ajout.
    List<IdImageArticle> listeIdImageArticle = new ArrayList<IdImageArticle>();

    // Si le tableau est vide ou null, on sort de la méthode.
    if (pTableauFichier == null || pTableauFichier.length == 0) {
      return;
    }
    
    // On parcourt les fichiers contenus dans le tableau.
    for (File fichier : pTableauFichier) {

      // Si le fichier n'existe pas, on informe l'utilisateur avec un message.
      if (!fichier.exists()) {
        DialogueInformation.afficher("Le fichier sélectionné n'existe pas.");
      }

      else {
        // Si l'objet n'est pas un fichier, on informe l'utilisateur avec un message.
        if (!fichier.isFile()) {
          DialogueInformation.afficher("L'objet " + fichier.getName()
              + " n'est pas un fichier image. \n Seul un fichier image peut être ajouté (.jpg, .png, .bmp, .gif).");
        }

        else {
          // Sinon, si le fichier n'est pas un fichier de type image, on informe l'utilisateur avec un message.
          if (!isFichierImage(fichier.getName())) {
            DialogueInformation.afficher("Le fichier " + fichier.getName()
                + " n'est pas un fichier image. \n Seul un fichier image peut être ajouté (.jpg, .png, .bmp, .gif).");
          }

          else {
            // Sinon, si le fichier image n'est pas valide ou est corrompu, on informe l'utilisateur avec un message.
            if (!isFichierImageLisible(fichier)) {
              DialogueInformation.afficher("Le fichier " + fichier.getName() + " n'est pas un fichier image valide ou il est corrompu.");
            }

            else {
              // Si l'objet est bien un fichier image valide et lisible, on crée l'identifiant d'image à partir de son chemin d'accès.
              IdImageArticle idImageArticle = IdImageArticle.getInstance(fichier.toString());
              if (listePhotoPourAffichage.contains(idImageArticle)) {
                // Si la liste des images affichées contient déjà le fichier, on informe l'utilisateur avec un message.
                DialogueInformation.afficher("Le fichier " + idImageArticle.getNomImageArticle() + " existe déjà.");
              }
              else {
                // Sinon, on ajoute l'identifiant de l'image à la liste des identifiants des images à ajouter.
                listeIdImageArticle.add(idImageArticle);
              }
            }
          }
        }
      }
    }

    // S'il y a au moins un fichier à ajouter,
    if (listeIdImageArticle.size() > 0) {
      // On charge la liste des images à ajouter à partir de leur identifiant, et on l'ajoute à la liste des images à afficher.
      listePhotoPourAffichage.addAll(new ListeImageArticle().charger(null, listeIdImageArticle));
      // On met à jour le nombre d'images contenues dans le diaporama.;
      nombrePhotoDansDiaporama = listePhotoPourAffichage.size();
      // On met à jour l'affichage des images.
      afficherImage();
    }
  }

  /**
   * Générer les listes de photos pour ajout et suppression.
   *
   * Ces listes sont générées en comparant la liste initiale avec la liste affichée :
   * - Si une photo est dans la liste affichée mais pas dans la liste initiale, il faut l'ajouter.
   * - Si une photo est dans la liste initiale mais pas dans la liste affichée, il faut la supprimer.
   *
   */
  private void genererListeAjoutEtSuppression() {
    // Si la liste des images pour l'affichage est vide.
    if (listePhotoPourAffichage.isEmpty()) {
      // On parcourt toutes les images de la liste initiale.
      for (ImageArticle image : listePhotoInitiale) {
        // On ajoute chaque image dans la liste des images à supprimer du réseau.
        listePhotoPourSuppression.add(image.getId());
      }
    }
    
    else {
      // Sinon, on compare le contenu de la liste initiale avec la liste des images affichées.
      for (ImageArticle imageArticle : listePhotoInitiale) {
        // Si l'image de la liste initiale n'est pas dans la liste des images affichées,
        if (!listePhotoPourAffichage.contains(imageArticle.getId())) {
          // On l'ajoute à la liste des images à supprimer.
          listePhotoPourSuppression.add(imageArticle.getId());
        }
      }
    }

    // Si la liste initiale des images est vide,
    if (listePhotoInitiale.isEmpty()) {
      // On parcourt toutes les images de la liste à afficher,
      for (ImageArticle image : listePhotoPourAffichage) {
        // On ajoute chaque image dans la liste des images à ajouter sur le réseau.
        listePhotoPourAjout.add(image.getId());
      }
    }
    else {
      // Sinon, on compare le contenu de la liste des images à afficher avec la liste initiale.
      for (ImageArticle imageArticle : listePhotoPourAffichage) {
        // Si l'image de la liste des images affichées n'est pas dans la liste initiale.
        if (!listePhotoInitiale.contains(imageArticle.getId())) {
          // On l'ajout à la liste des images à ajouter.
          listePhotoPourAjout.add(imageArticle.getId());
        }
      }
    }
  }

  /**
   * Afficher une boite de dialogue pour demander la confirmation de l'opération.
   *
   * @param pTitre Titre de la boîte de dialogue de confirmation
   * @param pMessage Message à afficher à l'utilisateur.
   *
   * @return <b>True</b> si l'utilisateur a confirmé la demande, <b>false</b> sinon.
   */
  private boolean isConfirmerDemande(String pTitre, String pMessage) {
    // Formater les textes passés en paramètre.
    pTitre = Constantes.normerTexte(pTitre);
    pMessage = Constantes.normerTexte(pMessage);

    // Créer la boîte de dialogue de confirmation et l'afficher.
    ModeleDialogueConfirmation modele = new ModeleDialogueConfirmation(null, pTitre, pMessage);
    DialogueConfirmation vue = new DialogueConfirmation(modele);
    vue.afficher();
    return modele.isSortieAvecValidation();
  }

  /**
   * Contrôler si un fichier est une image à partir de son nom ou son chemin d'accès, en String. <br>
   * On considère que le fichier est une image si son extension correspond aux format jpg, jpeg, png, bmp ou gif.
   *
   * @param pFichier nom ou chemin d'accès au fichier à contrôler.
   * @return <b>True</b> si le fichier est une image, <b>false</b> sinon.
   */
  private boolean isFichierImage(String pFichier) {
    // Si la chaîne de caractère est à null,
    if (pFichier == null) {
      return false;
    }
    // On identifie que le fichier est une image en fonction de son extension.
    pFichier = pFichier.toLowerCase();
    return (!pFichier.startsWith(Constantes.THUMBAIL)) && ((pFichier.lastIndexOf(".gif") != -1) || (pFichier.lastIndexOf(".jpg") != -1)
        || (pFichier.lastIndexOf(".jpeg") != -1) || (pFichier.lastIndexOf(".png") != -1) || (pFichier.lastIndexOf(".bmp") != -1));
  }

  /**
   * Contrôler si un fichier est un fichier image lisible.
   *
   * @param pFichier fichier à contrôler
   * @return <b>True</b> si le fichier est lisible, <b>false</b> sinon.
   */
  private boolean isFichierImageLisible(File pFichier) {
    try {
      new ImageIcon(ImageIO.read(pFichier)).getImage();
    }
    catch (NullPointerException e) {
      return false;
    }
    catch (IOException e) {
      return false;
    }
    return true;
  }

  // -- Accesseurs

  /**
   * Renvoyer le titre de l'image à afficher.
   *
   * @return Nom de l'image suivi de l'extension de fichier.
   */
  public String getTitreImage() {
    return nomPhotoPourAffichage;
  }

  /**
   * Renvoyer l'image à afficher.
   *
   * @return Image à afficher.
   */
  public Icon getIconImage() {
    return representationGraphiquePhotoPourAffichage;
  }

  /**
   * Renvoyer la longueur de la liste d'images associées à l'article.
   *
   * @return Nombre d'images à afficher.
   */
  public int getNombrePhotoDansDiaporama() {
    return nombrePhotoDansDiaporama;
  }
  
  /**
   * Renvoyer l'index de l'image pour l'affichage.
   */
  public int getIndexImagePourAffichage() {
    return indexImagePourAffichage;
  }

  /**
   * Renvoyer la largeur de l'image à afficher.
   * @return Largeur de l'image.
   */
  public double getLargeurImage() {
    return largeurImage;
  }

  /**
   * Renvoyer la hauteur de l'image à afficher.
   * @return Hauteur de l'image.
   */
  public double getHauteurImage() {
    return hauteurImage;
  }

  /**
   * Renvoyer la valeur de contrôle du zoom minimum.
   * @return True si le zoom minimum est atteint, false sinon.
   */
  public boolean isZoomMini() {
    return isZoomMini;
  }

  /**
   * Renvoyer la valeur de contrôle du zoom minimum.
   * @return True si le zoom minimum est atteint, false sinon.
   */
  public boolean isZoomMaxi() {
    return isZoomMaxi;
  }

  /**
   * Renvoyer la largeur du composant d'affichage de l'image.
   * @return Largeur du composant d'affichage de l'image.
   */
  public int getLargeurComposantAffichage() {
    return largeurComposantAffichage;
  }

  /**
   * Renvoyer la hauteur du composant d'affichage de l'image.
   * @return Hauteur du composant d'affichage de l'image.
   */
  public int getHauteurComposantAffichage() {
    return hauteurComposantAffichage;
  }
  
  /**
   * Renvoyer si le mode de saisie est à consultation ou non.
   *
   * @return True si en consultation, false sinon.
   */
  public boolean isConsultation() {
    return isConsultation;
  }
  
  /**
   * Renvoyer la liste des photos affichées.
   *
   * @return Liste des photos affichées.
   */
  public ListeImageArticle getListeImages() {
    return listePhotoPourAffichage;
  }
}
