/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.combobox;

import java.beans.BeanDescriptor;
import java.beans.PropertyDescriptor;

import ri.serien.libswing.outil.bean.EnumProprieteBean;
import ri.serien.libswing.outil.bean.SNBeanInfo;

/**
 * BeanInfo pour SNComboBox.
 * Gère le composant graphique sous JFormDesigner.
 */
public class SNComboBoxBeanInfo extends SNBeanInfo {

  {
    // Renseigner la classe JavaBean associé à ce BeanInfo
    setBeanClass(SNComboBox.class);
  }

  @Override
  public BeanDescriptor getBeanDescriptor() {
    // Indiquer si le composant est un container ou non (pour ne pas avoir à choisir lors de l'import sous JFormDesigner)
    BeanDescriptor beanDescriptor = new BeanDescriptor(getBeanClass());
    beanDescriptor.setValue("isContainer", Boolean.FALSE);
    return beanDescriptor;
  }

  @Override
  public PropertyDescriptor[] getPropertyDescriptors() {
    // Mettre en lecture seule toutes les propriétes
    bloquerTout();

    // Activer les propriétés utiles de SNBouton
    /*setLectureSeule(EnumProprieteBean.BOUTON_PRECONFIGURE, false);
    setCategorieAvecNomComposant(EnumProprieteBean.BOUTON_PRECONFIGURE);

    setLectureSeule(EnumProprieteBean.LIBELLE, false);
    setCategorieAvecNomComposant(EnumProprieteBean.LIBELLE);

    setLectureSeule(EnumProprieteBean.MNEMONIC, false);
    setCategorieAvecNomComposant(EnumProprieteBean.MNEMONIC);

    // Propriété à cacher car valeur par défaut non maîtrisable "Text"
    setVisible(EnumProprieteBean.TEXT, false);*/

    setLectureSeule(EnumProprieteBean.PREFERRED_SIZE, false);
    setLectureSeule(EnumProprieteBean.MINIMUM_SIZE, false);
    setLectureSeule(EnumProprieteBean.MAXIMUM_SIZE, false);

    setLectureSeule(EnumProprieteBean.ENABLED, false);
    setLectureSeule(EnumProprieteBean.MODE_REDUIT, false);
    setCategorieAvecNomComposant(EnumProprieteBean.MODE_REDUIT);
    
    // Retourner la liste des propriétés configurées
    return super.getPropertyDescriptors();
  }
}
