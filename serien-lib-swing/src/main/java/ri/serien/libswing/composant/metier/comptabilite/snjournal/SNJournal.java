/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.comptabilite.snjournal;

import ri.serien.libcommun.comptabilite.personnalisation.journal.IdJournal;
import ri.serien.libcommun.comptabilite.personnalisation.journal.Journal;
import ri.serien.libcommun.comptabilite.personnalisation.journal.ListeJournal;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un journal.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un journal dans le logiciel. Il gère l'affichage des journaux existants
 * et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNJournal à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGAM40FM_B4.
 */
public class SNJournal extends SNComboBoxObjetMetier<IdJournal, Journal, ListeJournal> {
  
  /**
   * Constructeur par défaut.
   */
  public SNJournal() {
    super();
    
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeJournal());
  }
  
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampJournal) {
    if (pChampJournal == null || pLexical.HostFieldGetData(pChampJournal) == null) {
      throw new MessageErreurException("Impossible de sélectionner le journal car l'identifiant du champ est invalide.");
    }
    
    IdJournal idJournal = null;
    String valeurJournal = pLexical.HostFieldGetData(pChampJournal).trim();
    if (valeurJournal != null && !valeurJournal.trim().isEmpty() && !valeurJournal.equals("**")) {
      idJournal = IdJournal.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampJournal));
    }
    
    setIdSelection(idJournal);
  }
  
  /**
   * Renseigner les champs RPG correspondant au magasin sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampJournal) {
    IdJournal idJournal = getIdSelection();
    if (idJournal != null) {
      pLexical.HostFieldPutData(pChampJournal, 0, idJournal.getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampJournal, 0, "");
    }
  }
  
}
