/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.client.snclient;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.AbstractAction;

import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.CritereClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un client.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un client dans le logiciel. Il gère l'affichage d'un client et la
 * sélection de l'un d'entre eux.
 * Il comprend comprenant un TextField et un bouton permettant de sélectionner un client dans une boîte de dialogue.
 *
 * Utilisation :
 * - Ajouter un composant SNEtablissement à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé. Les informations nécessaires sont la session et l'établissement.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le VueListeConsultationDocument.
 */
public class SNClient extends SNComposantAvecEtablissement {
  private SNTexte tfTexteRecherche;
  private SNBoutonRecherche btnDialogueRechercheClient;
  private ClientBase clientBaseSelectionne = null;
  private ListeClientBase listeClientBase = null;
  private boolean fenetreClientAffichee = false;
  private boolean rafraichir = false;
  private boolean rechercheProspectAutorisee = false;
  private EnumTypeCompteClient typeCompteClientForcee = null;
  private String texteRecherchePrecedente = null;
  private CritereClient critereClient = new CritereClient();
  
  /**
   * Constructeur par défaut.
   */
  public SNClient() {
    super();
    setName("SNClient");
    
    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    
    setLayout(new GridBagLayout());
    
    // Rexte de recherche
    tfTexteRecherche = new SNTexte();
    tfTexteRecherche.setName("tfClient");
    tfTexteRecherche.setEditable(true);
    tfTexteRecherche.setToolTipText(VueClient.RECHERCHE_GENERIQUE_TOOLTIP);
    add(tfTexteRecherche,
        new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
    
    // Bouton loupe
    btnDialogueRechercheClient = new SNBoutonRecherche();
    btnDialogueRechercheClient.setToolTipText("Recherche de client");
    btnDialogueRechercheClient.setName("btnRechercheClient");
    add(btnDialogueRechercheClient, new GridBagConstraints());
    
    // Focus listener pour lancer la recherche quand on quitte la zone de saisie
    tfTexteRecherche.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        tfTexteRechercheFocusLost(e);
      }
      
      @Override
      public void focusGained(FocusEvent e) {
        tfTexteRechercheFocusGained(e);
      }
    });
    
    // ActionListener pour capturer l'utilisation de la touche entrée
    tfTexteRecherche.addActionListener(new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tfTexteRechercheActionPerformed(e);
      }
    });
    
    // Etre notifier lorsque le bouton est cliqué
    btnDialogueRechercheClient.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnDialogueRechercheClientActionPerformed(e);
      }
    });
  }
  
  /**
   * Activer ou désactiver le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    tfTexteRecherche.setEnabled(pEnabled);
    btnDialogueRechercheClient.setEnabled(pEnabled);
  }
  
  /**
   * Mettre le focus sur le champ saisi de texte.
   */
  @Override
  public void requestFocus() {
    tfTexteRecherche.requestFocus();
  }
  
  /**
   * Charger la liste des clients.
   * Cette méthode ne fait rien dans ce composant car la liste des clients est chargée à la demande en fonction de la saisie. Elle est
   * conservée par souci de comptabilité avec les autres composants graphiques.
   */
  public void charger(boolean pForcerRafraichissement) {
    // Tester s'il faut rafraîchir la liste
    if (!rafraichir && !pForcerRafraichissement) {
      return;
    }
    rafraichir = false;
  }
  
  /**
   * Initialiser les valeurs du composant pour se retrouver dans la même situation que si aucun client n'est
   * sélectionné.
   */
  public void initialiserRecherche() {
    // Effacer les critères de recherche
    critereClient.initialiser();
    
    // Ne sélectionner aucun client
    setSelection(null);
  }
  
  /**
   * Force la recherche sur un critère (pour appel avec affichage forcé d'un client)
   */
  public void forcerRechercheParID(IdClient pIdClient) {
    critereClient.setIdClient(pIdClient);
    lancerRecherche(false);
  }
  
  /**
   * Lancer une recherche parmi les clients.
   */
  private void lancerRecherche(boolean forcerAffichageDialogue) {
    // Vérifier la session
    if (getSession() == null) {
      throw new MessageErreurException("La session du composant de sélection client est invalide.");
    }
    
    // Ne rien faire si le texte à rechercher est vide
    if (tfTexteRecherche.getText().trim().isEmpty()) {
      setSelection(null);
      if (forcerAffichageDialogue) {
        afficherDialogueRechercheClient(null);
      }
      return;
    }
    
    // Ne rien faire si le texte à rechercher n'a pas changé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && tfTexteRecherche.getText().equals(texteRecherchePrecedente)) {
      return;
    }
    
    // Lancer une recherche de clients
    critereClient.setIdEtablissement(getIdEtablissement());
    critereClient.setTexteRechercheGenerique(tfTexteRecherche.getText());
    critereClient.setTypeCompteClient(typeCompteClientForcee);
    critereClient.setIsRechercheProspectAutorisee(rechercheProspectAutorisee);
    critereClient.setIdClient(null);
    listeClientBase = ListeClientBase.charger(getIdSession(), critereClient);
    
    // Sélectionner automatiquement le seul client trouvé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && listeClientBase.size() == 1) {
      setSelection(listeClientBase.get(0));
    }
    // Afficher la boîte de dialogue si : affichage forcé de la boîte de dialogue, aucun ou plusieurs clients trouvés
    else {
      afficherDialogueRechercheClient(listeClientBase);
    }
    
    // Sélectionner automatiquement l'ensemble du champ afin que la prochaine saisie vienne en remplacement
    tfTexteRecherche.selectAll();
  }
  
  /**
   * Affiche la fenêtre de recherche des clients
   */
  private void afficherDialogueRechercheClient(ListeClientBase listeClientBase) {
    // Sécurité pour éviter que la boîte de dialogue ne s'affiche deux fois
    if (fenetreClientAffichee) {
      return;
    }
    fenetreClientAffichee = true;
    
    // Afficher la boîte de dialogue de recherche d'un client
    critereClient.setIdEtablissement(getIdEtablissement());
    critereClient.setTexteRechercheGenerique(tfTexteRecherche.getText());
    critereClient.setIsRechercheProspectAutorisee(rechercheProspectAutorisee);
    critereClient.setTypeCompteClient(typeCompteClientForcee);
    ModeleClient modele = new ModeleClient(getSession(), critereClient);
    modele.setListeClientBase(listeClientBase);
    modele.modifierTypeCompteClient(typeCompteClientForcee);
    modele.setRechercheProspectAutorisee(rechercheProspectAutorisee);
    VueClient vue = new VueClient(modele);
    vue.afficher();
    
    // Sélectionner le client retourné par la boîte de dialogue (ou aucun si null)
    if (modele.isSortieAvecValidation()) {
      setSelection(modele.getClientSelectionne());
    }
    // La boîte de dialogue est annulé, on remet le client sélectionné précédement
    else {
      rafraichirTexteRecherche();
    }
    fenetreClientAffichee = false;
  }
  
  /**
   * Rafraîchir le texte de la recherche avec le client sélectionné.
   */
  private void rafraichirTexteRecherche() {
    // Mettre à jour le composant
    if (clientBaseSelectionne != null) {
      tfTexteRecherche.setText(clientBaseSelectionne.toString());
    }
    else {
      tfTexteRecherche.setText("");
    }
    
    // Mémoriser le dernier texte pour ne pas relancer une recherche inutile
    texteRecherchePrecedente = tfTexteRecherche.getText();
  }
  
  // -- Méthodes évènementielles
  
  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      tfTexteRecherche.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteRechercheFocusLost(FocusEvent e) {
    try {
      // Désélection du client si le champ de recherche est vide lors de la perte du focus
      if (tfTexteRecherche == null || Constantes.normerTexte(tfTexteRecherche.getText()).isEmpty()) {
        setSelection(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteRechercheActionPerformed(ActionEvent e) {
    try {
      lancerRecherche(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btnDialogueRechercheClientActionPerformed(ActionEvent e) {
    try {
      if (fenetreClientAffichee == true) {
        lancerRecherche(false);
      }
      else {
        lancerRecherche(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Sélectionner un client à partir des champs RPG.
   *
   * @param pLexical Lexique des champs RPG de l'écran.
   * @param pChampNumeroClient Nom du champ RPG contenant le numéro du client.
   * @param pChampSuffixeClient Nom du champ RPG contenant le suffixe du client.
   * @return true=le client a changé, false=le client n'a pas changé.
   */
  public boolean setSelectionParChampRPG(Lexical pLexical, String pChampNumeroClient, String pChampSuffixeClient) {
    // Contrôler les paramètres
    if (pChampNumeroClient == null || pLexical.HostFieldGetData(pChampNumeroClient) == null) {
      throw new MessageErreurException("Impossible de sélectionner le client car son numéro est invalide.");
    }
    if (pChampSuffixeClient == null || pLexical.HostFieldGetData(pChampSuffixeClient) == null) {
      throw new MessageErreurException("Impossible de sélectionner le client car son suffixe de livraison est invalide.");
    }
    
    // Récupérer les informations du client
    String numeroClient = pLexical.HostFieldGetData(pChampNumeroClient).trim();
    String suffixeClient = pLexical.HostFieldGetData(pChampSuffixeClient).trim();
    
    // Construire l'id client
    IdClient idClient = null;
    if (!numeroClient.isEmpty()) {
      idClient = IdClient.getInstance(getIdEtablissement(), numeroClient, suffixeClient);
    }
    
    // Sortir si rien n'a changé
    if (Constantes.equals(idClient, getIdSelection())) {
      return false;
    }
    
    // Chargement du client base correspondant à l'identifiant choisi
    if (idClient != null) {
      critereClient.setIdEtablissement(getIdEtablissement());
      critereClient.setTexteRechercheGenerique(idClient.getTexte());
      listeClientBase = ListeClientBase.charger(getIdSession(), critereClient);
    }
    
    setSelectionParId(idClient);
    return true;
  }
  
  /**
   * Renseigner le champ RPG correspondant au client sélectionné.
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampNumeroClient, String pChampSuffixeClient) {
    ClientBase client = getSelection();
    if (client != null) {
      pLexical.HostFieldPutData(pChampNumeroClient, 0, client.getId().getNumero().toString());
      pLexical.HostFieldPutData(pChampSuffixeClient, 0, client.getId().getSuffixe().toString());
    }
    else {
      pLexical.HostFieldPutData(pChampNumeroClient, 0, "");
      pLexical.HostFieldPutData(pChampSuffixeClient, 0, "");
    }
  }
  
  /**
   * Client sélectionné.
   */
  public ClientBase getSelection() {
    return clientBaseSelectionne;
  }
  
  /**
   * identifiant du client sélectionné.
   */
  public IdClient getIdSelection() {
    if (clientBaseSelectionne == null) {
      return null;
    }
    return clientBaseSelectionne.getId();
  }
  
  /**
   * Sélectionner le client correspondant à l'identifiant passé en paramètre.
   */
  public void setSelectionParId(IdClient pIdClient) {
    ClientBase client = null;
    if (pIdClient != null) {
      critereClient.setIdEtablissement(getIdEtablissement());
      critereClient.setTexteRechercheGenerique(pIdClient.getTexte());
      critereClient.setTypeCompteClient(typeCompteClientForcee);
      critereClient.setIsRechercheProspectAutorisee(rechercheProspectAutorisee);
      listeClientBase = ListeClientBase.charger(getIdSession(), critereClient);
    }
    
    if (listeClientBase != null) {
      client = listeClientBase.getClientBaseParId(pIdClient);
    }
    setSelection(client);
  }
  
  /**
   * Modifier le client sélectionné.
   */
  public void setSelection(ClientBase pClientBaseSelectionne) {
    // Tester si la sélection a changé
    boolean bSelectionModifiee = !Constantes.equals(clientBaseSelectionne, pClientBaseSelectionne);
    
    // Modifier le client sélectionné
    clientBaseSelectionne = pClientBaseSelectionne;
    
    // Effacer les critères de recherche si aucun client n'est sélectionné
    if (clientBaseSelectionne == null) {
      critereClient.initialiser();
    }
    
    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();
    
    // Notifier uniquement si la sélection a changé
    if (bSelectionModifiee) {
      fireValueChanged();
    }
  }
  
  /**
   * Modifier le type de compte client utilisé pour la recherche.
   * Si la valeur a changé, la liste sera rechargée lors du prochain appel à la méthode charger().
   */
  public void setTypeCompteClient(EnumTypeCompteClient pTypeCompteClient) {
    if (Constantes.equals(critereClient.getTypeCompteClient(), pTypeCompteClient)) {
      return;
    }
    critereClient.setTypeCompteClient(pTypeCompteClient);
    rafraichir = true;
  }
  
  /**
   * Indiquer la recherche de prospect est autorisée
   */
  public void setRechercheProspectAutorisee(boolean pRechercheProspectAutorisee) {
    rechercheProspectAutorisee = pRechercheProspectAutorisee;
  }
  
  /**
   * Force la recherche sur un type de fiche (client ou prospect)
   */
  public void forcerRechercheTypeCompte(EnumTypeCompteClient pTypeFicheClient) {
    typeCompteClientForcee = pTypeFicheClient;
  }
}
