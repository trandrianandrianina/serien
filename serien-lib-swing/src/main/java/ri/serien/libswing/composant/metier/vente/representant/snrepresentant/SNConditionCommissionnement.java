/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.representant.snrepresentant;

import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.ConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.IdConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.ListeConditionCommissionnement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection des Condition de commissionnement.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une ConditionCommissionnement dans le logiciel.
 * Il gère l'affichage des ConditionCommissionnement existantes suivant l'établissement et gère la sélection de l'une de ces
 * ConditionCommissionnement
 *
 * Utilisation :
 * - Ajouter un composant SNConditionCommissionnement à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le ... (A implementer).
 */
public class SNConditionCommissionnement
    extends SNComboBoxObjetMetier<IdConditionCommissionnement, ConditionCommissionnement, ListeConditionCommissionnement> {
  
  /**
   * Constructeur par défaut.
   */
  public SNConditionCommissionnement() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeConditionCommissionnement());
  }
  
  /**
   * Sélectionner une condition de commissionnement de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pConditionCommissionnement) {
    if (pConditionCommissionnement == null || pLexical.HostFieldGetData(pConditionCommissionnement) == null) {
      throw new MessageErreurException("Impossible de sélectionner la condition de commissionnement car son code est invalide.");
    }
    
    IdConditionCommissionnement idConditionCommissionnement = null;
    String valeurConditionVente = pLexical.HostFieldGetData(pConditionCommissionnement);
    if (valeurConditionVente != null && !valeurConditionVente.trim().isEmpty()) {
      idConditionCommissionnement =
          IdConditionCommissionnement.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pConditionCommissionnement));
    }
    setIdSelection(idConditionCommissionnement);
  }
  
  /**
   * Renseigner les champs RPG correspondant à la condition de commissionnement sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pConditionCommissionnement) {
    ConditionCommissionnement condition = getSelection();
    if (condition != null) {
      pLexical.HostFieldPutData(pConditionCommissionnement, 0, condition.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pConditionCommissionnement, 0, "");
    }
  }
  
}
