/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.saisie.sntelephone;

import ri.serien.libcommun.outils.Constantes;

/**
 * Classe permettant le stockage des règles de formatage des numéros de téléphone internationaux.
 * regexFormat : expression régulière pour le formatage.
 * pattern : modèle pour le formatage.
 */
public class RegleFormatage {
  private int longueurNumeroTelephone;
  private String regexFormat;
  private String pattern;

  public RegleFormatage(int pLongueurNumeroTelephone, String pRegexFormat, String pPattern) {
    longueurNumeroTelephone = pLongueurNumeroTelephone;
    regexFormat = pRegexFormat;
    pattern = pPattern;
  }

  /**
   * Renvoie un entier correspondant à la longueur du numéro de téléphone.
   */
  public int getLongueurNumeroTelephone() {
    return longueurNumeroTelephone;
  }

  /**
   * Renvoie l'expression régulière permettant le formatage du numéro de téléphone.
   */
  public String getRegexFormat() {
    Constantes.normerTexte(regexFormat);
    return regexFormat;
  }

  /**
   * Renvoie le modèle permettant le formatage du numéro de téléphone.
   */
  public String getPattern() {
    Constantes.normerTexte(pattern);
    return pattern;
  }
}
