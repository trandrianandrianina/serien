/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.glasspane;

/**
 * Panneau avec intégration du GlassPane de type "soleil d'attente".
 * Voir SNPanelGlassPane pour plus d'informations.
 */
public class SNPanelGlassPaneSoleilAttente extends SNPanelGlassPane {
  private final GlassPaneSoleilAttente glassPaneSoleilAttente = new GlassPaneSoleilAttente();

  /**
   * Constructeur par défaut.
   */
  public SNPanelGlassPaneSoleilAttente() {
    super();
    setGlassPane(glassPaneSoleilAttente);
  }

  /**
   * Démarrer l'animation du soleil sur le glasspane.
   */
  public void demarrerAnimation() {
    glassPaneSoleilAttente.start();
  }
  
  /**
   * Stopper l'animation du soleil sur le glasspane.
   */
  public void stopperAnimation() {
    glassPaneSoleilAttente.stop();
  }
}
