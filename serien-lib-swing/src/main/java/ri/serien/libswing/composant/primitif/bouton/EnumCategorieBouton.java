/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import java.awt.Color;
import java.awt.Font;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Catégorie de bouton (ne pas ajouter de nouvelles valeurs dans cette énumération sans en discuter avec l'équipe).
 * 
 * La catégorie de boutons permet de regrouper les boutons en fonction de leur rôle et de définir leurs aspect. Le rôle des cétégories est
 * décrit ci-dessous.
 * 
 * Description des catégories :
 * - Standard : Ce sont les boutons ordinaires. La majorité des boutons doit être de cette catégorie. Leur courleur est orange.
 * - Validation : La catégorie validation est réservée pour toutes les opérations permettant de confirmer une opération, valider une
 * modification ou lancer une édition. Leur couleur est verte.
 * - Annulation : La catégorie annulation est réservée aux opérations permettant d'annuler une modification en cours ou tout simplement
 * de quitter un écran sans faire de modification (fermer la session ou retourner au résultat de la recherche). Leur couleur est rouge.
 * - Image : Ce sont des boutons constitués d'une image.
 */
public enum EnumCategorieBouton {
  STANDARD("Standard", SNCharteGraphique.COULEUR_BOUTON_ORANGE, SNCharteGraphique.POLICE_BOUTON),
  VALIDATION("Validation", SNCharteGraphique.COULEUR_BOUTON_VERT, SNCharteGraphique.POLICE_BOUTON),
  ANNULATION("Annulation", SNCharteGraphique.COULEUR_BOUTON_ROUGE, SNCharteGraphique.POLICE_BOUTON),
  IMAGE("Image", SNCharteGraphique.COULEUR_BOUTON_ORANGE, SNCharteGraphique.POLICE_BOUTON);
  
  private final String libelle;
  private final Color couleurFond;
  private final Font police;
  
  /**
   * Constructeur.
   */
  private EnumCategorieBouton(String pLibelle, Color pCouleurFond, Font pPolice) {
    libelle = pLibelle;
    couleurFond = pCouleurFond;
    police = pPolice;
  }
  
  /**
   * Retourner le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Libellé.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Couleur de fond des boutons associés à cette catégorie.
   */
  public Color getCouleurFond() {
    return couleurFond;
  }
  
  /**
   * Police des boutons associés à cette catégorie.
   */
  public Font getPolice() {
    return police;
  }
}
