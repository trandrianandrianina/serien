/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.label;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;

/**
 * Cette classe permet de mettre un texte d'information lorsque un textfield est vide, cela permet d'indiquer les informations à
 * mettre dans la zone (Ex:Veuillez entrée un nom)
 *
 * Le textfield prendra par défaut les propriétés du composant parent mais il reste modifiable grâce au méthode implementer dans cette
 * classe
 *
 * Exemple d'implémentation:[GVM121] Gestion des ventes -> Fiches permanentes -> Clients -> Clients création d'un client pro (en date du
 * 09/12/19)
 */
public class SNLabelFantome extends SNLabelChamp implements FocusListener, DocumentListener {
  
  private JTextComponent composant;
  private Document document;
  
  /**
   * Constructeur du composant
   * Il faut rentrer le texte ainsi que le composant parent
   */
  public SNLabelFantome(String texte, JTextComponent composant) {
    this.composant = composant;
    document = composant.getDocument();
    
    setText(texte);
    setFont(getFont().deriveFont(Font.ITALIC));
    setForeground(Color.LIGHT_GRAY);
    setBorder(new EmptyBorder(composant.getInsets()));
    setHorizontalAlignment(SNLabelChamp.LEADING);
    
    composant.addFocusListener(this);
    document.addDocumentListener(this);
    
    composant.setLayout(new BorderLayout());
    composant.add(this);
  }
  
  // Implementation des Evenement
  @Override
  public void focusGained(FocusEvent e) {
    setVisible(false);
  }
  
  @Override
  public void focusLost(FocusEvent e) {
    if (document.getLength() > 0) {
      setVisible(false);
      return;
    }
    else {
      setVisible(true);
    }
  }
  
  @Override
  public void insertUpdate(DocumentEvent e) {
    if (document.getLength() > 0) {
      setVisible(false);
      return;
    }
  }
  
  @Override
  public void removeUpdate(DocumentEvent e) {
    if (document.getLength() > 0) {
      setVisible(false);
      return;
    }
    else {
      setVisible(true);
    }
  }
  
  @Override
  public void changedUpdate(DocumentEvent e) {
  }
  
}
