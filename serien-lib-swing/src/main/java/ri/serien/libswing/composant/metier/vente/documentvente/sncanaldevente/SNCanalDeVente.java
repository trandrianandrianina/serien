/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente;

import ri.serien.libcommun.gescom.personnalisation.canaldevente.CanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.IdCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.ListeCanalDeVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un CanalDeVente.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un CanalDeVente dans le logiciel.
 * Il gère l'affichage des CanalDeVente existantes et la sélection de l'une d'entre elle.
 *
 * Utilisation :
 * - Ajouter un composant SNCanalDeVente à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM22FM_B2
 */
public class SNCanalDeVente extends SNComboBoxObjetMetier<IdCanalDeVente, CanalDeVente, ListeCanalDeVente> {
  
  /**
   * Constructeur par défaut
   */
  public SNCanalDeVente() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeCanalDeVente());
  }
  
  /**
   * Sélectionner un canal de vente de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampCanalDeVente) {
    if (pChampCanalDeVente == null || pLexical.HostFieldGetData(pChampCanalDeVente) == null) {
      throw new MessageErreurException("Impossible de sélectionner le canal de vente car son code est invalide.");
    }
    
    IdCanalDeVente idCanal = null;
    String valeurCanalDeVente = pLexical.HostFieldGetData(pChampCanalDeVente);
    if (valeurCanalDeVente != null && !valeurCanalDeVente.trim().isEmpty()) {
      idCanal = IdCanalDeVente.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampCanalDeVente));
    }
    setIdSelection(idCanal);
  }
  
  /**
   * Renseigne les champs RPG correspondant au canal de vente sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampCanalDeVente) {
    CanalDeVente canal = getSelection();
    if (canal != null) {
      pLexical.HostFieldPutData(pChampCanalDeVente, 0, canal.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampCanalDeVente, 0, "");
    }
  }
}
