/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.dialoguestandard.information;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue "Message d'information".
 */
public class ModeleDialogueInformation extends AbstractModeleDialogue {
  private final String MESSAGE_PAR_DEFAUT = "Une erreur technique est survenue. Merci de contacter le service assistance.";
  
  // Variables
  private String message = "";
  private boolean centreTexte = true;
  
  /**
   * Constructeur.
   */
  public ModeleDialogueInformation() {
    super(null);
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
  }
  
  /**
   * Renseigner le message utilisateur.
   * Un message par défaut est affiché si le message reçu n'est pas valide (null ou vide).
   */
  public void setMessage(String pMessage) {
    // Mettre un message par défaut en cas d'absence de messsage
    if (pMessage != null && !pMessage.trim().isEmpty()) {
      message = pMessage;
    }
    else {
      message = MESSAGE_PAR_DEFAUT;
    }
    
    // Tracer le message en enlevant tous les retours à la ligne éventuels
    Trace.info(message.replaceAll("(\\r|\\n)", " "));
  }
  
  // -- Accesseurs
  
  /**
   * Message d'information
   */
  public String getMessage() {
    return message;
  }

  /**
   * Indique si le message doit être centré ou non.
   */
  public boolean isCentreTexte() {
    return centreTexte;
  }
  
  /**
   * Initialise le centrage du texte ou non.
   */
  public void setCentrerTexte(boolean pCentreTexte) {
    centreTexte = pCentreTexte;
  }
}
