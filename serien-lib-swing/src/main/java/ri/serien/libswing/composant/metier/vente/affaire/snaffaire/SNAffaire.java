/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.affaire.snaffaire;

import ri.serien.libcommun.gescom.personnalisation.affaire.Affaire;
import ri.serien.libcommun.gescom.personnalisation.affaire.IdAffaire;
import ri.serien.libcommun.gescom.personnalisation.affaire.ListeAffaire;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une affaire.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une affaire dans le logiciel. Il gère l'affichage des affaire existantes
 * et la sélection de l'une d'entre elle.
 *
 * Utilisation :
 * - Ajouter un composant SNAffaire à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM22FM_B2
 */
public class SNAffaire extends SNComboBoxObjetMetier<IdAffaire, Affaire, ListeAffaire> {
  
  /**
   * Constructeur par défaut
   */
  public SNAffaire() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeAffaire());
  }
  
  /**
   * Sélectionner une affaire de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampAffaire) {
    if (pChampAffaire == null || pLexical.HostFieldGetData(pChampAffaire) == null) {
      throw new MessageErreurException("Impossible de sélectionner l'affaire car son code est invalide.");
    }
    
    IdAffaire idAffaire = null;
    String valeurAffaire = pLexical.HostFieldGetData(pChampAffaire);
    if (valeurAffaire != null && !valeurAffaire.trim().isEmpty()) {
      idAffaire = IdAffaire.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampAffaire));
    }
    setIdSelection(idAffaire);
  }
  
  /**
   * Renseigne les champs RPG correspondant à l'affaire sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampAffaire) {
    Affaire affaire = getSelection();
    if (affaire != null) {
      pLexical.HostFieldPutData(pChampAffaire, 0, affaire.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampAffaire, 0, "");
    }
  }
}
