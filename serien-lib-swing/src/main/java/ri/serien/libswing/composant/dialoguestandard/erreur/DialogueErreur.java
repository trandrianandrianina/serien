/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.dialoguestandard.erreur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue d'affichage des messages d'erreur.
 *
 * Cette boîte de dialogue est conçue pour afficher les erreurs transmises via des exceptions. Il n'est pas possible d'afficher
 * directement un message d'erreur et c'est voulu. Lorsqu'on veut afficher un message d'erreur, il faut d'abord générer un
 * MessageUtilisateurException à partir de la classe métier ou du modèle de l'écran. Ensuite, la vue capture l'exception et se charge
 * d'afficher le message via cette classe.
 *
 * Lorsque que l'exception est constituée d'un ou plusieurs MessageUtilisateurException, les messages d'erreurs sont récupérés pour être
 * assemblés et affiché à l'utilisateur. Le détail de l'exception est consultable via le bouton "engrenage".
 *
 * Cette boîte de dialogue permet aussi d'afficher des exceptions autre que MessageUtilisateurException. Dans ce cas, un message générique
 * est affiché à l'utilisateur : "Une erreur technique est survenue. Merci de contacter le service assistance". Le détail de l'exception
 * est consultable via le bouton "engrenage".
 */
public class DialogueErreur extends AbstractVueDialogue<ModeleDialogueErreur> {
  private static final String BOUTON_COPIER_PRESSE_PAPIER = "Copier dans le presse-papier";

  /**
   * Contructeur.
   */
  private DialogueErreur(ModeleDialogueErreur pModele) {
    super(pModele, false);
  }

  /**
   * Afficher la boîte de dialogue avec un message issu d'une exception.
   *
   * Cette méthode affiche une boîte de dialogue via un invokeLater. Cela a deux avantages :
   * - Ainsi, on est cûr que la boîte d edialogue est affichée à partir de la thread graphique.
   * - Surout, cela permet aux évènements graphiques en cours de ne pas être interrompu par la boîte de dialogue modale
   * (par exemple, lors d'une erreur déclenchée par la sélection d'une valeur dans une liste déroulante, celle-ci se refermera
   * complètement avant l'affichage de la boîte de dialogue).
   */
  public static void afficher(Throwable pThrowable) {
    final ModeleDialogueErreur modele = new ModeleDialogueErreur();
    modele.setException(pThrowable);
    final DialogueErreur vue = new DialogueErreur(modele);

    // Deux raisons pour l'invokeLater :
    // - Etre sûr que l'on est dans la thread graphique.
    // - Permettre aux évènements graphiques en cours de ne pas être interrompu par la boîte de dialogue modale (liste
    // déroulante).
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        vue.afficher();
      }
    });
  }

  /**
   * Afficher la boîte de dialogue avec une erreur grave qui entraine l'arrêt de l'application.
   *
   * Cette version est utile lors du démarrage des logiciels. En effet, si une erreur se produit lors du démarrage et que le logiciel
   * est stoppé de suite via un System.exit(-1), la version avec invokeLater n'aura pas le temps d'afficher le message d'erreur.
   * Le logiciel s'arrête avant de traiter les évènements graphiques en attente. Cette méthode s'assure que le System.exit(-1) est
   * effectué après l'affichage de la boîte de dialogue.
   */
  public static void afficherErreurFatale(Throwable pThrowable) {
    final ModeleDialogueErreur modele = new ModeleDialogueErreur();
    modele.setException(pThrowable);
    final DialogueErreur vue = new DialogueErreur(modele);

    // Afficher la boîte de dialogue d'erreur dans la thread graphique
    // On termine l'application lorsque la boîte de dialogue est fermée
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        // Afficher la boîte de dialogue d'erreur
        vue.afficher();

        // Tracer l'arrêt du logiciel
        Trace.titre("ARRET DE SERIE N SUITE A UNE ERREUR FATALE");
        
        // Arrêter le logiciel
        System.exit(-1);
      }
    });
  }

  /**
   * Initialiser les composants graphiques de la boîte de dialogue.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setBackground(SNCharteGraphique.COULEUR_FOND);

    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_COPIER_PRESSE_PAPIER, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Rafraichissement des données à l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirMessage();
    rafraichirMessageDetaille();

    // Afficher le bouton détail uniquement s'il y a une exception
    btDetail.setVisible(getModele().getMessageDetaille() != null && !getModele().getMessageDetaille().isEmpty());
  }

  /**
   * Formater le message utilisateur pour l'afficher en HTML.
   */
  private void rafraichirMessage() {
    // Ne pas afficher le message si on est en mode détail
    if (getModele().isAfficherDetail()) {
      taMessageUtilisateur.setVisible(false);
      return;
    }
    taMessageUtilisateur.setVisible(true);

    // Afficher le message par défaut s'il n'y a pas de message utilisateur
    String message = getModele().getMessage();
    if (message == null || message.trim().isEmpty()) {
      message = MessageErreurException.MESSAGE_ERREUR_TECHNIQUE;
    }

    // Formater le message en HTML
    message = message.replaceAll("<", "&lt;");
    message = message.replaceAll(">", "&gt;");
    message = message.replaceAll("\\n", "<br>");
    message = "<html><div style='text-align: center;'>" + message + "</div></html>";

    taMessageUtilisateur.setText(message);
  }

  /**
   * Afficher le message détaillé.
   */
  private void rafraichirMessageDetaille() {
    // Ne pas afficher l'exception si on n'est pas en mode détail
    if (!getModele().isAfficherDetail()) {
      scpException.setVisible(false);
      lbAvertissement.setVisible(false);
      return;
    }
    scpException.setVisible(true);
    lbAvertissement.setVisible(true);

    // Afficher le message détaillé
    String messageDetaille = getModele().getMessageDetaille();
    if (messageDetaille != null && !messageDetaille.isEmpty()) {
      taMessageException.setText(messageDetaille);
    }
    else {
      taMessageException.setText(MessageErreurException.MESSAGE_ERREUR_TECHNIQUE);
    }

    // Mettre le curseur au début du texte
    taMessageException.setCaretPosition(0);
  }

  // Méthodes évènementielles

  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecValidation();
      }
      if (pSNBouton.isBouton(BOUTON_COPIER_PRESSE_PAPIER)) {
        getModele().copierMessageDetaille();
      }
    }
    catch (Exception exception) {
      // Générer uniquement une trace car nous sommes dans la boîte de dialogue d'affichage des erreurs !
      Trace.erreur(exception, "Erreur dans la boîte de dialogue d'affichage des messages d'erreur.");

    }
  }

  private void btDetailMouseClicked(MouseEvent e) {
    try {
      getModele().modifierAfficherDetail(!scpException.isVisible());
    }
    catch (Exception exception) {
      // Générer uniquement une trace car nous sommes dans la boîte de dialogue d'affichage des erreurs !
      Trace.erreur(exception, "Erreur dans la boîte de dialogue d'affichage des messages d'erreur.");
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlcontenu = new JPanel();
    taMessageUtilisateur = new JLabel();
    scpException = new JScrollPane();
    taMessageException = new JTextPane();
    lbAvertissement = new JLabel();
    btDetail = new JLabel();
    snBarreBouton = new SNBarreBouton();

    // ======== this ========
    setTitle("Message d'erreur");
    setBackground(new Color(238, 238, 210));
    setModal(true);
    setMinimumSize(new Dimension(600, 300));
    setLocationByPlatform(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBorder(null);
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      // ======== pnlcontenu ========
      {
        pnlcontenu.setBackground(new Color(238, 238, 210));
        pnlcontenu.setOpaque(false);
        pnlcontenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlcontenu.setName("pnlcontenu");
        pnlcontenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlcontenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlcontenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlcontenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlcontenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };

        // ---- taMessageUtilisateur ----
        taMessageUtilisateur.setFont(taMessageUtilisateur.getFont().deriveFont(taMessageUtilisateur.getFont().getStyle() | Font.BOLD,
            taMessageUtilisateur.getFont().getSize() + 3f));
        taMessageUtilisateur.setBackground(new Color(238, 238, 210));
        taMessageUtilisateur.setBorder(new EmptyBorder(10, 10, 10, 10));
        taMessageUtilisateur.setAutoscrolls(false);
        taMessageUtilisateur.setHorizontalAlignment(SwingConstants.CENTER);
        taMessageUtilisateur.setHorizontalTextPosition(SwingConstants.CENTER);
        taMessageUtilisateur.setName("taMessageUtilisateur");
        pnlcontenu.add(taMessageUtilisateur, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

        // ======== scpException ========
        {
          scpException.setName("scpException");

          // ---- taMessageException ----
          taMessageException.setBackground(new Color(238, 238, 210));
          taMessageException.setEditable(false);
          taMessageException.setOpaque(false);
          taMessageException.setName("taMessageException");
          scpException.setViewportView(taMessageException);
        }
        pnlcontenu.add(scpException, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));

        // ---- lbAvertissement ----
        lbAvertissement.setText("Ces informations sont destin\u00e9es au service technique de R\u00e9solution Informatique.");
        lbAvertissement.setFont(lbAvertissement.getFont().deriveFont(Font.ITALIC));
        lbAvertissement.setName("lbAvertissement");
        pnlcontenu.add(lbAvertissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

        // ---- btDetail ----
        btDetail.setFont(btDetail.getFont().deriveFont(btDetail.getFont().getStyle() | Font.BOLD, btDetail.getFont().getSize() - 1f));
        btDetail.setBackground(new Color(238, 238, 210));
        btDetail.setIcon(new ImageIcon(getClass().getResource("/images/engrenage.png")));
        btDetail.setToolTipText("D\u00e9tails du message d'erreur");
        btDetail.setVisible(false);
        btDetail.setName("btDetail");
        btDetail.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            btDetailMouseClicked(e);
          }
        });
        pnlcontenu.add(btDetail, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlPrincipal.add(pnlcontenu, BorderLayout.CENTER);

      // ---- snBarreBouton ----
      snBarreBouton.setBorder(null);
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlcontenu;
  private JLabel taMessageUtilisateur;
  private JScrollPane scpException;
  private JTextPane taMessageException;
  private JLabel lbAvertissement;
  private JLabel btDetail;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
