/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.snfamille;

import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.personnalisation.famille.IdFamille;
import ri.serien.libcommun.gescom.personnalisation.famille.ListeFamille;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.metier.referentiel.article.sngroupe.SNGroupe;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une famille.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une famille dans le logiciel. Il gère l'affichage des familles existantes
 * et la sélection de l'un d'entre eux
 *
 * Utilisation :
 * - Ajouter un composant SNFamille à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVX35FM_B1 (GVM3237)
 */
public class SNFamille extends SNComboBoxObjetMetier<IdFamille, Famille, ListeFamille> {
  private SNGroupe snGroupe = null;
  private SNSousFamille snSousFamille = null;
  
  /**
   * Constructeur par défaut
   */
  public SNFamille() {
    super();
  }
  
  /**
   * Indiquer si la famille est sélectionnable dans la liste déroulante.
   * Seule les familles du groupe sont sélectionnables (toutes si le groupe est null).
   */
  @Override
  public boolean isSelectionnable(Famille pFamille) {
    // Ne pas sélectionner les familles "null"
    if (pFamille == null) {
      return false;
    }
    
    // Ne pas sélectionner les familles qui ne sont pas dans le groupe sélectionné
    if (snGroupe != null && snGroupe.getIdSelection() != null && !pFamille.getId().isDansGroupe(snGroupe.getIdSelection())) {
      return false;
    }
    
    // Ne pas sélectionner les familles qui sont inférieures au groupe début sélectionné
    if (snGroupe != null && snGroupe.getComposantDebut() != null && snGroupe.getComposantDebut().getIdSelection() != null
        && pFamille.getId().getIdGroupe().compareTo(snGroupe.getComposantDebut().getIdSelection()) < 0) {
      return false;
    }
    
    // Ne pas sélectionner les familles qui sont supérieures au groupe fin sélectionné
    if (snGroupe != null && snGroupe.getComposantFin() != null && snGroupe.getComposantFin().getIdSelection() != null
        && pFamille.getId().getIdGroupe().compareTo(snGroupe.getComposantFin().getIdSelection()) > 0) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeFamille());
  }
  
  /**
   * Sélectionner une famille de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampFamille) {
    String valeurFamille = pLexical.HostFieldGetData(pChampFamille);
    if (valeurFamille == null) {
      throw new MessageErreurException("Impossible de sélectionner la famille car le nom du champ est invalide.");
    }
    
    // Construire l'identifiant de la famille
    IdFamille idFamille = null;
    if (!valeurFamille.trim().isEmpty()) {
      idFamille = IdFamille.getInstance(getIdEtablissement(), valeurFamille);
    }
    
    setIdSelection(idFamille);
  }
  
  /**
   * Renseigne les champs RPG correspondant à la famille sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampFamille) {
    IdFamille idFamille = getIdSelection();
    if (idFamille != null) {
      pLexical.HostFieldPutData(pChampFamille, 0, idFamille.getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampFamille, 0, "");
    }
  }
  
  /**
   * Etre notifié lorsque la sélection change.
   */
  @Override
  public void valueChanged() {
    // Modifier le groupe du composant groupe afin qu'il corresponde à la famille sélectionnée.
    if (snGroupe != null && getIdSelection() != null) {
      snGroupe.setIdSelection(getIdSelection().getIdGroupe());
    }
    
    // Rafraîchir la liste sélectionnable du composant sous-famille si la famille change
    if (snSousFamille != null) {
      snSousFamille.rafraichirListeSelectionnable();
    }
    
    // Rafraîchir la liste sélectionnable du composant sous-famille début lié
    if (getComposantDebut() != null) {
      SNFamille snFamilleDebut = (SNFamille) getComposantDebut();
      if (snFamilleDebut.getComposantSousFamille() != null) {
        snFamilleDebut.getComposantSousFamille().rafraichirListeSelectionnable();
      }
    }
    
    // Rafraîchir la liste sélectionnable du composant sous-famille fin lié
    if (getComposantFin() != null) {
      SNFamille snFamilleFin = (SNFamille) getComposantFin();
      if (snFamilleFin.getComposantSousFamille() != null) {
        snFamilleFin.getComposantSousFamille().rafraichirListeSelectionnable();
      }
    }
  }
  
  /**
   * Retourner le composant groupe lié à ce composant.
   */
  public SNGroupe getComposantGroupe() {
    return snGroupe;
  }
  
  /**
   * Lier un composant groupe à ce composant.
   */
  public void lierComposantGroupe(SNGroupe psnGroupe) {
    // Tester si al valeur a changée
    if (Constantes.equals(psnGroupe, snGroupe)) {
      return;
    }
    
    // Mettre à jour la valeur
    snGroupe = psnGroupe;
    
    // Faire le lien dans l'autre sens
    snGroupe.lierComposantFamille(this);
    
    // Faire également le lien avec la sous-famille si on la connaît
    if (snSousFamille != null) {
      snSousFamille.lierComposantGroupe(snGroupe);
    }
    
    // Rafraichir la liste des composants sélectionnable
    rafraichirListeSelectionnable();
  }
  
  /**
   * Retourner le composant sous-famille lié à ce composant.
   */
  public SNSousFamille getComposantSousFamille() {
    return snSousFamille;
  }
  
  /**
   * Lier un composant sous-famille à ce composant.
   */
  public void lierComposantSousFamille(SNSousFamille psnSousFamille) {
    // Tester si la valeur a changée
    if (Constantes.equals(psnSousFamille, snSousFamille)) {
      return;
    }
    
    // Mettre à jour la valeur
    snSousFamille = psnSousFamille;
    
    // Faire le lien dans l'autre sens
    snSousFamille.lierComposantFamille(this);
    
    // Faire également le lien avec le groupe si on le connaît
    if (snGroupe != null) {
      snGroupe.lierComposantSousFamille(snSousFamille);
    }
    
    // Rafraichir la liste des composants sélectionnable
    rafraichirListeSelectionnable();
  }
}
