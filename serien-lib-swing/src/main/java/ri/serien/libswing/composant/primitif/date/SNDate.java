/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.date;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JComboBox;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import org.jdesktop.swingx.JXMonthView;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonCalendrier;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.composant.SNComposant;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant de saisie d'une date.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une date au format Jour/Mois/Année (jj/mm/aaaa) dans le logiciel.
 *
 * Utilisation :
 * - Ajouter un composant SNDate à un écran.
 * - Utiliser l'accesseur setDateParChampRPG() pour renseigner les dates de début et de fin du composant graphique à partir de champs RPG.
 * Les méthodes renseignerChampRPG() permet au contraire de renseigner le champs RPG à partir du composant graphique.
 * - Utiliser l'accesseur setDate() pour renseigner la date manuellement. La méthode getDate() permet au contraire de récupérer la date.
 *
 * Voir un exemple d'implémentation dans : TODO
 */
public class SNDate extends SNComposant {
  // Constantes
  
  // La longueur de saisie correspond à la longueur de la chaine de caractère, séparateurs compris.
  private static final int LONGUEUR_SAISIE_JJMMAAAA = 10;
  
  // Variables
  private Date date = null;

  /**
   * Constructeur.
   */
  public SNDate() {
    super();
    initComponents();
    
    // Initialisation de la taille maximale acceptée à la saisie.
    tfDate.setLongueur(LONGUEUR_SAISIE_JJMMAAAA);

    // Initialise le bouton pour l'affichage de la popup calendrier.
    initPopupBouton();
    // Initialise la popup d'affichage du calendrier.
    initPopupMenu();
  }
  
  /**
   * Gère l'activation des champs du composant.
   * Si le composant est désactivé, l'ensemble de ses composants internes sont désactivés.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    tfDate.setEnabled(pEnabled);
    snBoutonCalendrier.setEnabled(pEnabled);
  }
  
  /**
   * Rafraichit l'affichage des composants.
   */
  private void rafraichir() {
    rafraichirDate();
  }
  
  /**
   * Rafraichit l'affichage de la date.
   */
  private void rafraichirDate() {
    // Contrôle des null.
    if (date == null) {
      tfDate.setText("");
    }
    // On affiche la date dans le composant de saisie, au format jj/mm/aaaa.
    tfDate.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
  }
  
  /**
   * Afficher une date à partir d'un champ RPG.
   *
   * @param pLexique : lexique
   * @param pChampDate : Champ RPG associé à la date.
   */
  public void setDateParChampRPG(Lexical pLexique, String pChampDate) {
    // Contrôle des paramètres
    if (pLexique == null) {
      throw new MessageErreurException("Impossible d'afficher la date car le lexique est invalide.");
    }
    if (pChampDate == null || pChampDate.isEmpty()) {
      throw new MessageErreurException("Impossible d'afficher la date car son champ est invalide.");
    }
    
    // Récupération de la date dans le buffer
    String dateDeRPG = pLexique.HostFieldGetData(pChampDate);
    setDate(Constantes.convertirTexteEnDate(dateDeRPG));
  }
  
  /**
   * Renseigner le champ RPG correspondant à la date.
   *
   * @param pLexique : lexique.
   * @param pChampMontant : Champ RPG associé au montant.
   */
  public void renseignerChampRPG(Lexical pLexique, String pChampDate) {
    // Contrôle des paramètres.
    if (pLexique == null) {
      throw new MessageErreurException("Impossible de renseigner la date car le lexique est invalide.");
    }
    if (pChampDate == null || pChampDate.isEmpty()) {
      throw new MessageErreurException("Impossible d'afficher la date car son champ est invalide.");
    }
    
    // Si la date est à null, on envoie un blanc au RPG.
    if (date == null) {
      pLexique.HostFieldPutData(pChampDate, 0, "");
      return;
    }
    // Sinon on formate la date pour le rpg, jj.mm.aa, et on l'envoie.
    SimpleDateFormat dateFormat = new SimpleDateFormat(DateHeure.FORMAT8_DATE);
    pLexique.HostFieldPutData(pChampDate, 0, dateFormat.format(date));
  }
  
  /**
   * Modifie la date.
   * La date peut être null.
   *
   * @param pDate Date.
   */
  public void setDate(Date pDate) {
    date = pDate;
    rafraichir();
  }
  
  /**
   * Retourner la date.
   *
   * @return Date.
   */
  public Date getDate() {
    return date;
  }

  // -- Méthodes privées

  /**
   * Initialise la popup d'affichage du calendrier pour sélection de date.
   */
  private void initPopupMenu() {
    // Modifie le layout du menu popup.
    popupMenu.setLayout(new BorderLayout());
    // Ajoute le composant MonthView dans le menu popup.
    popupMenu.add(xMonthView, BorderLayout.CENTER);
    // Permet de sélectionner le mois à afficher via les flèches latérales.
    xMonthView.setTraversable(true);
  }

  /**
   * Initialise le bouton calendrier pour l'affichage de la popup calendrier.
   */
  private void initPopupBouton() {
    JComboBox box = new JComboBox();
    Object preventHide = box.getClientProperty("doNotCancelPopup");
    snBoutonCalendrier.putClientProperty("doNotCancelPopup", preventHide);
    snBoutonCalendrier.setInheritsPopupMenu(true);
  }
  
  /**
   * Gestion de l'action clic sur le bouton calendrier.
   *
   * @param e
   */
  private void snBoutonCalendrierActionPerformed(ActionEvent e) {
    try {
      // Si la popup est visible, on la masque
      if (popupMenu.isVisible()) {
        popupMenu.setVisible(false);
        return;
      }
      // Sinon on l'affiche sous la zone de saisie de la date.
      tfDate.requestFocusInWindow();
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          // Si la date en mémoire n'est pas à null,
          if (date != null) {
            // La date en mémoire est sélectionnée dans le calendrier qui s'affiche.
            xMonthView.setSelectionDate(date);
            // La date en mémoire est affichée dans le calendrier.
            xMonthView.setFirstDisplayedDay(date);
          }
          popupMenu.show(tfDate, 0, tfDate.getHeight());
        }
      });
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Gestion de l'action sur la popup calendrier.
   *
   * @param e
   */
  private void xMonthViewActionPerformed(ActionEvent e) {
    try {
      // Contrôle des null
      if (xMonthView.getSelectionDate() != null) {
        // Si la date sélectionnée est la même que la date actuelle, on quitte la méthode.
        if (Constantes.equals(date, xMonthView.getSelectionDate())) {
          return;
        }
        
        // Sinon, on met à jour la date avec la date sélectionnée.
        date = xMonthView.getSelectionDate();
        // On masque la popup calendrier.
        popupMenu.setVisible(false);
        
        rafraichir();
        fireValueChanged();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Gestion de l'événement perte de focus sur le champ de saisie de texte.
   *
   * @param e
   */
  private void tfDateFocusLost(FocusEvent e) {
    try {
      // On transforme la date saisie sous forme de chaîne de caractère en objet Date.
      Date dateSaisie = Constantes.convertirTexteEnDate(tfDate.getText());
      
      // Si la date saisie est la même que la date actuelle, on quitte la méthode.
      if (Constantes.equals(date, dateSaisie)) {
        return;
      }
      
      // Sinon, on met à jour la date avec la date saisie.
      date = dateSaisie;
      
      rafraichir();
      fireValueChanged();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Sélection du texte du composant au clic.
   *
   * @param e
   */
  private void tfDateFocusGained(FocusEvent e) {
    try {
      if (tfDate.getText() == null || tfDate.getText().isEmpty()) {
        return;
      }
      // On sélectionne le texte contenu dans le composant de saisie.
      tfDate.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Récupération du focus en cas de clic sur le composant de saisie de texte.
   *
   * @param e
   */
  private void tfDateMouseClicked(MouseEvent e) {
    try {
      if (tfDate.isFocusable()) {
        tfDate.grabFocus();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Gestion du clic sur la touche "Entrer".
   *
   * @param e
   */
  private void tfDateKeyReleased(KeyEvent e) {
    try {
      // Si l'utilisateur a appuyé sur la touche Entrer,
      if (e.getKeyCode() == KeyEvent.VK_ENTER) {
        // On perd le focus sur le composant de saisie
        tfDate.transferFocus();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    tfDate = new SNTexte();
    snBoutonCalendrier = new SNBoutonCalendrier();
    popupMenu = new JPopupMenu();
    xMonthView = new JXMonthView();
    
    // ======== this ========
    setMaximumSize(new Dimension(115, 30));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- tfDate ----
    tfDate.setRequestFocusEnabled(false);
    tfDate.setPreferredSize(new Dimension(85, 31));
    tfDate.setMinimumSize(new Dimension(85, 30));
    tfDate.setMaximumSize(new Dimension(85, 30));
    tfDate.setName("tfDate");
    tfDate.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        tfDateFocusGained(e);
      }
      
      @Override
      public void focusLost(FocusEvent e) {
        tfDateFocusLost(e);
      }
    });
    tfDate.addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        tfDateMouseClicked(e);
      }
    });
    tfDate.addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent e) {
        tfDateKeyReleased(e);
      }
    });
    add(tfDate, BorderLayout.CENTER);
    
    // ---- snBoutonCalendrier ----
    snBoutonCalendrier.setPreferredSize(new Dimension(30, 26));
    snBoutonCalendrier.setMinimumSize(new Dimension(30, 30));
    snBoutonCalendrier.setMaximumSize(new Dimension(30, 30));
    snBoutonCalendrier.setFocusable(false);
    snBoutonCalendrier.setName("snBoutonCalendrier");
    snBoutonCalendrier.addActionListener(e -> snBoutonCalendrierActionPerformed(e));
    add(snBoutonCalendrier, BorderLayout.EAST);
    
    // ======== popupMenu ========
    {
      popupMenu.setName("popupMenu");
      
      // ---- xMonthView ----
      xMonthView.setFont(new Font("sansserif", Font.PLAIN, 14));
      xMonthView.setName("xMonthView");
      xMonthView.addActionListener(e -> xMonthViewActionPerformed(e));
      popupMenu.add(xMonthView);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNTexte tfDate;
  private SNBoutonCalendrier snBoutonCalendrier;
  private JPopupMenu popupMenu;
  private JXMonthView xMonthView;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
