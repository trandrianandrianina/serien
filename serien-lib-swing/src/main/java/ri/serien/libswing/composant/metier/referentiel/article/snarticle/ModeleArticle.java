/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.snarticle;

import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleConsigne;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleSpecial;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleStock;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreHorsGamme;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.personnalisation.famille.ListeFamille;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleArticle extends AbstractModeleDialogue {
  // Variables
  private CritereArticle critereArticle = null;
  private ListeFamille listeFamille = null;
  private ArticleBase articleBaseSelectionne = null;
  private List<IdArticle> listeIdArticle = null;
  private ListeArticleBase listeArticleBase = null;
  private int ligneSelectionnee = -1;

  /**
   * Constructeur.
   */
  public ModeleArticle(SessionBase pSession, CritereArticle pCriteresArticle) {
    super(pSession);
    critereArticle = pCriteresArticle;
    chargerDonnees();
  }

  // -- Méthodes publiques

  @Override
  public void initialiserDonnees() {
  }

  @Override
  public void chargerDonnees() {
    // Charger la liste des familles
    if (listeFamille == null) {
      listeFamille = new ListeFamille();
    }
    listeFamille = listeFamille.charger(getIdSession(), critereArticle.getIdEtablissement());

  }

  /**
   * Initialiser la recherche.
   */
  public void initialiserRecherche() {
    critereArticle.initialiser();
    listeIdArticle = null;
    articleBaseSelectionne = null;
    listeArticleBase = null;
    rafraichir();
  }

  /**
   * Lancer une recherche sur les articles.
   */
  public void lancerRecherche() {
    lancerRechercheInterne();
    rafraichir();
  }

  /**
   * Lancer une recherche sur les articles mais sans déclencher de rafraîchissement.
   */
  private void lancerRechercheInterne() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeArticleBase != null) {
      taillePage = listeArticleBase.getTaillePage();
    }

    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeIdArticle = null;
    listeArticleBase = null;
    ligneSelectionnee = -1;
    articleBaseSelectionne = null;
    rafraichir();

    critereArticle.setIdEtablissement(getIdEtablissement());

    // Analyse erreur sur recherche
    if (critereArticle.getRechercheGenerique().isEnErreur() && getFamille() == null) {
      return;
    }

    // Charger la liste des identifiants
    listeIdArticle = ManagerServiceArticle.chargerListeIdArticle(getIdSession(), critereArticle);

    // Initier la liste des articles à partir de la liste d'identifiants
    listeArticleBase = ListeArticleBase.creerListeNonChargee(listeIdArticle);

    // Charger la première page
    listeArticleBase.chargerPremierePage(getIdSession(), taillePage);
  }

  /**
   * Afficher la plage d'articles compris entre deux lignes.
   * Les informations des articles sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageArticleBaseAffiches(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeArticleBase != null) {
      listeArticleBase.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
    }

    // Rafraichir l'écran
    rafraichir();
  }

  /**
   * Modifie l'indice de la ligne séléctionnée dans le tableau.
   */
  public void modifierLigneSelectionnee(int pLigneSelectionnee) {
    if (ligneSelectionnee == pLigneSelectionnee) {
      return;
    }
    ligneSelectionnee = pLigneSelectionnee;

    // Déterminer le article sélectionné
    if (listeArticleBase != null && 0 <= ligneSelectionnee && ligneSelectionnee < listeArticleBase.size()) {
      articleBaseSelectionne = listeArticleBase.get(ligneSelectionnee);
    }
    else {
      articleBaseSelectionne = null;
    }

    rafraichir();
  }

  /**
   * Modifier le texte de la recherche générique.
   */
  public void modifierTexteRecherche(String pTexteRecherche) {
    if (Constantes.equals(critereArticle.getTexteRechercheGenerique(), pTexteRecherche)) {
      return;
    }
    critereArticle.setTexteRechercheGenerique(pTexteRecherche);

    lancerRechercheInterne();
    rafraichir();
  }

  /**
   * Modifier le filtre famille
   */
  public void modifierFamille(Famille pFamille) {
    if (pFamille == null) {
      critereArticle.setIdFamille(null);
    }
    else if (Constantes.equals(critereArticle.getIdFamille(), pFamille.getId())) {
      return;
    }
    else {
      critereArticle.setIdFamille(pFamille.getId());
    }
    rafraichir();
  }

  /**
   * Modifier le filtre hors gamme
   */
  public void modifierHorsGamme(EnumFiltreHorsGamme pFiltreHorsGamme) {
    if (pFiltreHorsGamme == null) {
      critereArticle.setHorsGamme(EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES);
    }
    else if (Constantes.equals(critereArticle.getFiltreHorsGamme(), pFiltreHorsGamme)) {
      return;
    }
    else {
      critereArticle.setHorsGamme(pFiltreHorsGamme);
    }
    rafraichir();
  }

  /**
   * Modifier le filtre article spécial
   */
  public void modifierSpecial(EnumFiltreArticleSpecial pFiltreSpecial) {
    if (pFiltreSpecial == null) {
      critereArticle.setFiltreSpecial(EnumFiltreArticleSpecial.OPTION_TOUS_LES_ARTICLES);
    }
    else if (Constantes.equals(critereArticle.getFiltreSpecial(), pFiltreSpecial)) {
      return;
    }
    else {
      critereArticle.setFiltreSpecial(pFiltreSpecial);
    }
    rafraichir();
  }
  
  /**
   * Modifier le filtre article consigné
   */
  public void modifierConsigne(EnumFiltreArticleConsigne pFiltreConsigne) {
    if (pFiltreConsigne == null) {
      critereArticle.setFiltreConsigne(EnumFiltreArticleConsigne.OPTION_TOUS_LES_ARTICLES);
    }
    else if (Constantes.equals(critereArticle.getFiltreConsigne(), pFiltreConsigne)) {
      return;
    }
    else {
      critereArticle.setFiltreConsigne(pFiltreConsigne);
    }
    rafraichir();
  }

  /**
   * Modifier le filtre article commentaire
   */
  
  public void modifierCommentaire(EnumFiltreArticleCommentaire pFiltreCommentaire) {
    if (pFiltreCommentaire == null) {
      critereArticle.setFiltreCommentaire(EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES);
    }
    else if (Constantes.equals(critereArticle.getFiltreCommentaire(), pFiltreCommentaire)) {
      return;
    }
    else {
      critereArticle.setFiltreCommentaire(pFiltreCommentaire);
      if (pFiltreCommentaire == EnumFiltreArticleCommentaire.OPTION_LES_ARTICLES_COMMENTAIRES_UNIQUEMENT) {
        critereArticle.setFiltreSpecial(EnumFiltreArticleSpecial.OPTION_TOUS_LES_ARTICLES);
        critereArticle.setFiltreConsigne(EnumFiltreArticleConsigne.OPTION_TOUS_LES_ARTICLES);
        critereArticle.setHorsGamme(EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES);
      }
    }
    rafraichir();
  }
  
  // -- Accesseurs

  /**
   * Liste des articles constituant le résultat de la recherche.
   */
  public ListeArticleBase getListeArticleBase() {
    return listeArticleBase;
  }

  /**
   * Renseigner la liste des identifiants articles correspondant au résultat de la recherche.
   */
  public void setListeIdArticle(List<IdArticle> pListeIdArticle) {
    listeIdArticle = pListeIdArticle;
  }

  /**
   * article sélectionné.
   */
  public ArticleBase getArticleSelectionne() {
    return articleBaseSelectionne;
  }

  /**
   * Modifier l'article sélectionné.
   */
  public void setArticleSelectionne(ArticleBase pArticleBase) {
    articleBaseSelectionne = pArticleBase;
  }

  /**
   * Filtre sur l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return critereArticle.getIdEtablissement();
  }

  /**
   * Teste de la recherche générique.
   */
  public String getTexteRecherche() {
    return critereArticle.getTexteRechercheGenerique();
  }

  /**
   * Liste des familles d'articles
   */
  public ListeFamille getListeFamille() {
    return listeFamille;
  }

  /**
   * retourner la famille sélectionnée
   */
  public Famille getFamille() {
    if (critereArticle.getIdFamille() == null) {
      return null;
    }
    return listeFamille.get(critereArticle.getIdFamille());
  }

  /**
   * Retourner le filtre sur les articles hors gamme
   */
  public EnumFiltreHorsGamme getHorsGamme() {
    return critereArticle.getFiltreHorsGamme();
  }

  /**
   * Retourner le filtre sur les articles spéciaux
   */
  public EnumFiltreArticleSpecial getSpecial() {
    return critereArticle.getFiltreSpecial();
  }

  /**
   * Retourner le filtre sur les articles stock
   */
  public EnumFiltreArticleStock getStock() {
    return critereArticle.getFiltreStock();
  }

  /**
   * Retourner le filtre sur les articles consignés
   */
  public EnumFiltreArticleConsigne getConsigne() {
    return critereArticle.getFiltreConsigne();
  }

  /**
   * Retourner le filtre sur les articles commentaires
   */
  public EnumFiltreArticleCommentaire getCommentaire() {
    return critereArticle.getFiltreCommentaire();
  }

  /**
   * Renvoyer si les filtres de recherches sur les articles sont modifiable par l'utilisateur
   * C'est utile si on veut forcer le type d'articles accessibles sur un écran donné ou dans un contexte métier donné
   * (par exemple ne pouvoir rechercher que des articles consignés)
   */
  public boolean isVerrouillerCriteres() {
    return critereArticle.isVerrouillerCritere();
  }

  /**
   * Message d'information à afficher en titre de liste dans la boîte de dialogue.
   */
  public Message getMessage() {
    if (listeIdArticle == null) {
      return Message.getMessageNormal("Recherche d'articles");
    }
    else if (listeIdArticle.size() == 0) {
      return Message.getMessageImportant("Aucun article ne correspond à vos critères");
    }
    else if (listeIdArticle.size() == 1) {
      return Message.getMessageNormal("Article correspondant à votre recherche");
    }
    else if (listeIdArticle.size() > 1) {
      return Message.getMessageNormal("Articles correspondants à votre recherche (" + listeIdArticle.size() + ")");
    }
    else {
      return Message.getMessageImportant("Aucun article ne correspond à vos critères");
    }
  }

}
