/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametreetablissement;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.checkbox.SNCheckBoxReduit;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNNombreEntier;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;

/**
 * Ce composant permet d'afficher les paramètres de l'établissement.
 */
public class SNParametreEtablissement extends SNPanelTitre {
  private static final String TITRE = "Etablissement";

  // Variables
  private ModeleDialogueDetailPrixVente modele = null;
  private boolean evenementsActifs = false;
  private ParametreEtablissement parametreEtablissement = null;
  private ParametreEtablissement parametreEtablissementInitial = null;

  /**
   * Constructeur.
   */
  public SNParametreEtablissement() {
    super();
    initComponents();

    // Ne permettre la saisie que de 1 chiffre
    tfArticleNonRemisable.setLongueur(1);
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Modèle associé au composant graphique.
   * @return Modèle de la boîte de dialogue.
   */
  public ModeleDialogueDetailPrixVente getModele() {
    return modele;
  }

  /**
   * Modifier le modèle associé au composant graphique.
   * @param pModele Modèle de la boîte de dialogue.
   */
  public void setModele(ModeleDialogueDetailPrixVente pModele) {
    modele = pModele;
  }

  /**
   * Indiquer si les évènements doivent être traités.
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Rafraichir les données du composant.
   */
  public void rafraichir() {
    // Empêcher les évènements pendant le rafraichissement
    evenementsActifs = false;

    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    parametreEtablissement = getModele().getParametreEtablissement();
    parametreEtablissementInitial = getModele().getParametreEtablissementInitial();

    // Rafraichir les composants
    rafraichirTitre();
    rafraichirLibelleTauxTVA();
    rafraichirTauxTVA1();
    rafraichirTauxTVA2();
    rafraichirTauxTVA3();
    rafraichirTauxTVA4();
    rafraichirTauxTVA5();
    rafraichirTauxTVA6();
    rafraichirModeNegocePS287();
    rafraichirColonneTarifPS105();
    rafraichirRechercheColonneTarifNonNullePS305();
    rafraichirPrixPublicCommePrixBasePS316();
    rafraichirArticleNonRemisablePS281();
    rafraichirTypeUniteCNVQuantitativeDG();
    rafraichirTypeCumulCNVQuantitativeDG();

    // Réactiver les évènements pendant le rafraichissement
    evenementsActifs = true;
  }

  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirTitre() {
    if (parametreEtablissement != null && parametreEtablissement.getIdEtablissement() != null) {
      setTitre(TITRE + " " + parametreEtablissement.getIdEtablissement());
    }
    else {
      setTitre(TITRE);
    }
  }

  /**
   * Rafraichir le libellé du taux de TVA.
   */
  private void rafraichirLibelleTauxTVA() {
    lbTauxTVA.setImportance(parametreEtablissement != null && parametreEtablissementInitial != null
        && (!Constantes.equals(parametreEtablissement.getTauxTVA1(), parametreEtablissementInitial.getTauxTVA1())
            || !Constantes.equals(parametreEtablissement.getTauxTVA2(), parametreEtablissementInitial.getTauxTVA2())
            || !Constantes.equals(parametreEtablissement.getTauxTVA3(), parametreEtablissementInitial.getTauxTVA3())
            || !Constantes.equals(parametreEtablissement.getTauxTVA4(), parametreEtablissementInitial.getTauxTVA4())
            || !Constantes.equals(parametreEtablissement.getTauxTVA5(), parametreEtablissementInitial.getTauxTVA5())
            || !Constantes.equals(parametreEtablissement.getTauxTVA6(), parametreEtablissementInitial.getTauxTVA6())));
  }
  
  /**
   * Rafraichir le taux de TVA 1.
   */
  private void rafraichirTauxTVA1() {
    if (parametreEtablissement != null && parametreEtablissement.getTauxTVA1() != null) {
      tfTauxTVA1.setText(parametreEtablissement.getTauxTVA1().getTexteSansSymbole());
    }
    else {
      tfTauxTVA1.setText("");
    }

    tfTauxTVA1.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir le taux de TVA 2.
   */
  private void rafraichirTauxTVA2() {
    if (parametreEtablissement != null && parametreEtablissement.getTauxTVA2() != null) {
      tfTauxTVA2.setText(parametreEtablissement.getTauxTVA2().getTexteSansSymbole());
    }
    else {
      tfTauxTVA2.setText("");
    }

    tfTauxTVA2.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de TVA 3.
   */
  private void rafraichirTauxTVA3() {
    if (parametreEtablissement != null && parametreEtablissement.getTauxTVA3() != null) {
      tfTauxTVA3.setText(parametreEtablissement.getTauxTVA3().getTexteSansSymbole());
    }
    else {
      tfTauxTVA3.setText("");
    }

    tfTauxTVA3.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de TVA 4.
   */
  private void rafraichirTauxTVA4() {
    if (parametreEtablissement != null && parametreEtablissement.getTauxTVA4() != null) {
      tfTauxTVA4.setText(parametreEtablissement.getTauxTVA4().getTexteSansSymbole());
    }
    else {
      tfTauxTVA4.setText("");
    }

    tfTauxTVA4.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir le taux de TVA 5.
   */
  private void rafraichirTauxTVA5() {
    if (parametreEtablissement != null && parametreEtablissement.getTauxTVA5() != null) {
      tfTauxTVA5.setText(parametreEtablissement.getTauxTVA5().getTexteSansSymbole());
    }
    else {
      tfTauxTVA5.setText("");
    }

    tfTauxTVA5.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de TVA 6.
   */
  private void rafraichirTauxTVA6() {
    if (parametreEtablissement != null && parametreEtablissement.getTauxTVA6() != null) {
      tfTauxTVA6.setText(parametreEtablissement.getTauxTVA6().getTexteSansSymbole());
    }
    else {
      tfTauxTVA6.setText("");
    }

    tfTauxTVA6.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le mode négoce (paramètres généraux).
   */
  private void rafraichirModeNegocePS287() {
    if (parametreEtablissement != null) {
      cbModeNegoce.setSelected(parametreEtablissement.isModeNegocePS287());
    }
    else {
      cbModeNegoce.setSelected(false);
    }

    cbModeNegoce.setEnabled(modele != null && modele.isModeSimulation());
    lbModeNegoce.setImportance(parametreEtablissement != null && parametreEtablissementInitial != null
        && !Constantes.equals(parametreEtablissement.isModeNegocePS287(), parametreEtablissementInitial.isModeNegocePS287()));
  }

  /**
   * Rafraichir la valeur du PS105 (colonne tarif).
   */
  private void rafraichirColonneTarifPS105() {
    if (parametreEtablissement != null && parametreEtablissement.getNumeroColonneTarifPS105() != null) {
      Integer numeroColonneTarif = parametreEtablissement.getNumeroColonneTarifPS105();
      if (numeroColonneTarif >= 1 && numeroColonneTarif <= 10) {
        cbColonneTarif.setSelectedIndex(numeroColonneTarif);
      }
      else {
        cbColonneTarif.setSelectedIndex(0);
      }
    }
    else {
      cbColonneTarif.setSelectedIndex(0);
    }

    cbColonneTarif.setEnabled(modele != null && modele.isModeSimulation());
    lbColonneTarif.setImportance(parametreEtablissement != null && parametreEtablissementInitial != null && !Constantes
        .equals(parametreEtablissement.getNumeroColonneTarifPS105(), parametreEtablissementInitial.getNumeroColonneTarifPS105()));
  }

  /**
   * Rafraichir la valeur du PS305 (colonne tarif précédente).
   */
  private void rafraichirRechercheColonneTarifNonNullePS305() {
    if (parametreEtablissement != null && parametreEtablissement.isRechercheColonneTarifNonNullePS305()) {
      cbRechercheColonneTarifNonNulle.setSelected(true);
    }
    else {
      cbRechercheColonneTarifNonNulle.setSelected(false);
    }

    cbRechercheColonneTarifNonNulle.setEnabled(modele != null && modele.isModeSimulation());
    lbRechercheColonneTarifNonNulle.setImportance(parametreEtablissement != null && parametreEtablissementInitial != null
        && !Constantes.equals(parametreEtablissement.isRechercheColonneTarifNonNullePS305(),
            parametreEtablissementInitial.isRechercheColonneTarifNonNullePS305()));
  }

  /**
   * Rafraichir la valeur du PS316 (prix public comme prix de base HT).
   */
  private void rafraichirPrixPublicCommePrixBasePS316() {
    if (parametreEtablissement != null && parametreEtablissement.isPrixPublicCommePrixBasePS316()) {
      cbPrixPublicCommePrixBase.setSelected(true);
    }
    else {
      cbPrixPublicCommePrixBase.setSelected(false);
    }

    cbPrixPublicCommePrixBase.setEnabled(modele != null && modele.isModeSimulation());
    lbPrixPublicCommePrixBase.setImportance(parametreEtablissement != null && parametreEtablissementInitial != null && !Constantes
        .equals(parametreEtablissement.isPrixPublicCommePrixBasePS316(), parametreEtablissementInitial.isPrixPublicCommePrixBasePS316()));
  }

  /**
   * Rafraichir la valeur du PS281 (aplication des CNV pour les articles non remisable).
   */
  private void rafraichirArticleNonRemisablePS281() {
    if (parametreEtablissement != null && parametreEtablissement.getArticleNonRemisablePS281() != null) {
      tfArticleNonRemisable.setText("" + parametreEtablissement.getArticleNonRemisablePS281());
    }
    else {
      tfArticleNonRemisable.setText("");
    }

    tfArticleNonRemisable.setEnabled(modele != null && modele.isModeSimulation());
    lbArticleNonRemisable.setImportance(parametreEtablissement != null && parametreEtablissementInitial != null && !Constantes
        .equals(parametreEtablissement.getArticleNonRemisablePS281(), parametreEtablissementInitial.getArticleNonRemisablePS281()));
  }

  /**
   * Rafraichir le type de l'unité des conditions de ventes quantitative de la DG (paramètres généraux).
   */
  private void rafraichirTypeUniteCNVQuantitativeDG() {
    if (parametreEtablissement == null || parametreEtablissement.getTypeUniteConditionQuantitative() == null) {
      tfTypeUniteCNVQuantitative.setText("");
      return;
    }

    tfTypeUniteCNVQuantitative.setText(parametreEtablissement.getTypeUniteConditionQuantitative().getLibelle());
  }

  /**
   * Rafraichir le type de cumul des conditions de ventes quantitative de la DG (paramètres généraux).
   */
  private void rafraichirTypeCumulCNVQuantitativeDG() {
    if (parametreEtablissement == null || parametreEtablissement.getTypeCumulConditionQuantitative() == null) {
      tfTypeCumulCNVQuantitative.setText("");
      return;
    }

    tfTypeCumulCNVQuantitative.setText(parametreEtablissement.getTypeCumulConditionQuantitative().getLibelle());
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void tfTauxTVA1FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxTVAEtablissement(tfTauxTVA1.getText(), 1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxTVA2FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxTVAEtablissement(tfTauxTVA2.getText(), 2);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxTVA3FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxTVAEtablissement(tfTauxTVA3.getText(), 3);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxTVA4FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxTVAEtablissement(tfTauxTVA4.getText(), 4);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxTVA5FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxTVAEtablissement(tfTauxTVA5.getText(), 5);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxTVA6FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxTVAEtablissement(tfTauxTVA6.getText(), 6);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbModeNegoceActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      modele.modifierModeNegoce(cbModeNegoce.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbColonneTarifItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbColonneTarif.getSelectedIndex() >= 1 && cbColonneTarif.getSelectedIndex() <= 10) {
        modele.modifierColonneTarifPS105(cbColonneTarif.getSelectedIndex());
      }
      else {
        modele.modifierColonneTarifPS105(0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbRechercheColonneTarifNonNulleActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      modele.modifierRechercheColonneTarifNonNulle(cbRechercheColonneTarifNonNulle.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbPrixPublicCommePrixBaseActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      modele.modifierPrixPublicCommePrixBase(cbPrixPublicCommePrixBase.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfCNVArticleNonRemisableFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      modele.modifierCNVArticleNonRemisable(tfArticleNonRemisable.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbTauxTVA = new SNLabelChamp();
    pnlTauxTVA = new SNPanel();
    tfTauxTVA1 = new SNTexte();
    tfTauxTVA2 = new SNTexte();
    tfTauxTVA3 = new SNTexte();
    tfTauxTVA4 = new SNTexte();
    tfTauxTVA5 = new SNTexte();
    tfTauxTVA6 = new SNTexte();
    lbModeNegoce = new SNLabelChamp();
    cbModeNegoce = new SNCheckBoxReduit();
    lbColonneTarif = new SNLabelChamp();
    cbColonneTarif = new SNComboBoxReduit();
    lbRechercheColonneTarifNonNulle = new SNLabelChamp();
    cbRechercheColonneTarifNonNulle = new JCheckBox();
    lbPrixPublicCommePrixBase = new SNLabelChamp();
    cbPrixPublicCommePrixBase = new SNCheckBoxReduit();
    lbArticleNonRemisable = new SNLabelChamp();
    tfArticleNonRemisable = new SNNombreEntier();
    lbTypeUniteCNVQuantitative = new SNLabelChamp();
    tfTypeUniteCNVQuantitative = new SNTexte();
    lbTypeCumulCNVQuantitative = new SNLabelChamp();
    tfTypeCumulCNVQuantitative = new SNTexte();

    // ======== this ========
    setTitre("Etablissement");
    setModeReduit(true);
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };

    // ---- lbTauxTVA ----
    lbTauxTVA.setModeReduit(true);
    lbTauxTVA.setText("Taux TVA (%)");
    lbTauxTVA.setMinimumSize(new Dimension(30, 22));
    lbTauxTVA.setPreferredSize(new Dimension(30, 22));
    lbTauxTVA.setName("lbTauxTVA");
    add(lbTauxTVA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 3), 0, 0));

    // ======== pnlTauxTVA ========
    {
      pnlTauxTVA.setName("pnlTauxTVA");
      pnlTauxTVA.setLayout(new GridLayout(2, 0, 3, 0));

      // ---- tfTauxTVA1 ----
      tfTauxTVA1.setEnabled(false);
      tfTauxTVA1.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxTVA1.setModeReduit(true);
      tfTauxTVA1.setMaximumSize(new Dimension(40, 22));
      tfTauxTVA1.setPreferredSize(new Dimension(40, 22));
      tfTauxTVA1.setMinimumSize(new Dimension(40, 22));
      tfTauxTVA1.setName("tfTauxTVA1");
      tfTauxTVA1.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxTVA1FocusLost(e);
        }
      });
      pnlTauxTVA.add(tfTauxTVA1);

      // ---- tfTauxTVA2 ----
      tfTauxTVA2.setEnabled(false);
      tfTauxTVA2.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxTVA2.setModeReduit(true);
      tfTauxTVA2.setMaximumSize(new Dimension(40, 22));
      tfTauxTVA2.setMinimumSize(new Dimension(40, 22));
      tfTauxTVA2.setPreferredSize(new Dimension(40, 22));
      tfTauxTVA2.setName("tfTauxTVA2");
      tfTauxTVA2.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxTVA2FocusLost(e);
        }
      });
      pnlTauxTVA.add(tfTauxTVA2);

      // ---- tfTauxTVA3 ----
      tfTauxTVA3.setEnabled(false);
      tfTauxTVA3.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxTVA3.setModeReduit(true);
      tfTauxTVA3.setMaximumSize(new Dimension(40, 22));
      tfTauxTVA3.setMinimumSize(new Dimension(40, 22));
      tfTauxTVA3.setPreferredSize(new Dimension(40, 22));
      tfTauxTVA3.setName("tfTauxTVA3");
      tfTauxTVA3.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxTVA3FocusLost(e);
        }
      });
      pnlTauxTVA.add(tfTauxTVA3);

      // ---- tfTauxTVA4 ----
      tfTauxTVA4.setEnabled(false);
      tfTauxTVA4.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxTVA4.setModeReduit(true);
      tfTauxTVA4.setMaximumSize(new Dimension(40, 22));
      tfTauxTVA4.setMinimumSize(new Dimension(40, 22));
      tfTauxTVA4.setPreferredSize(new Dimension(40, 22));
      tfTauxTVA4.setName("tfTauxTVA4");
      tfTauxTVA4.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxTVA4FocusLost(e);
        }
      });
      pnlTauxTVA.add(tfTauxTVA4);

      // ---- tfTauxTVA5 ----
      tfTauxTVA5.setEnabled(false);
      tfTauxTVA5.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxTVA5.setModeReduit(true);
      tfTauxTVA5.setMaximumSize(new Dimension(40, 22));
      tfTauxTVA5.setMinimumSize(new Dimension(40, 22));
      tfTauxTVA5.setPreferredSize(new Dimension(40, 22));
      tfTauxTVA5.setName("tfTauxTVA5");
      tfTauxTVA5.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxTVA5FocusLost(e);
        }
      });
      pnlTauxTVA.add(tfTauxTVA5);

      // ---- tfTauxTVA6 ----
      tfTauxTVA6.setEnabled(false);
      tfTauxTVA6.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxTVA6.setModeReduit(true);
      tfTauxTVA6.setMaximumSize(new Dimension(40, 22));
      tfTauxTVA6.setMinimumSize(new Dimension(40, 22));
      tfTauxTVA6.setPreferredSize(new Dimension(40, 22));
      tfTauxTVA6.setName("tfTauxTVA6");
      tfTauxTVA6.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxTVA6FocusLost(e);
        }
      });
      pnlTauxTVA.add(tfTauxTVA6);
    }
    add(pnlTauxTVA,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbModeNegoce ----
    lbModeNegoce.setText("Mode n\u00e9goce (PS287)");
    lbModeNegoce.setModeReduit(true);
    lbModeNegoce.setName("lbModeNegoce");
    add(lbModeNegoce,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbModeNegoce ----
    cbModeNegoce.setEnabled(false);
    cbModeNegoce.setName("cbModeNegoce");
    cbModeNegoce.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        cbModeNegoceActionPerformed(e);
      }
    });
    add(cbModeNegoce,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbColonneTarif ----
    lbColonneTarif.setText("Colonne tarif (PS105)");
    lbColonneTarif.setModeReduit(true);
    lbColonneTarif.setName("lbColonneTarif");
    add(lbColonneTarif,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbColonneTarif ----
    cbColonneTarif.setModel(new DefaultComboBoxModel(new String[] { "Aucune", "Colonne 1", "Colonne 2", "Colonne 3", "Colonne 4",
        "Colonne 5", "Colonne 6", "Colonne 7", "Colonne 8", "Colonne 9", "Colonne 10" }));
    cbColonneTarif.setSelectedIndex(-1);
    cbColonneTarif.setEnabled(false);
    cbColonneTarif.setName("cbColonneTarif");
    cbColonneTarif.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        cbColonneTarifItemStateChanged(e);
      }
    });
    add(cbColonneTarif,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbRechercheColonneTarifNonNulle ----
    lbRechercheColonneTarifNonNulle.setText("Colonne non nulle (PS305)");
    lbRechercheColonneTarifNonNulle.setModeReduit(true);
    lbRechercheColonneTarifNonNulle.setName("lbRechercheColonneTarifNonNulle");
    add(lbRechercheColonneTarifNonNulle,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbRechercheColonneTarifNonNulle ----
    cbRechercheColonneTarifNonNulle.setEnabled(false);
    cbRechercheColonneTarifNonNulle.setName("cbRechercheColonneTarifNonNulle");
    cbRechercheColonneTarifNonNulle.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        cbRechercheColonneTarifNonNulleActionPerformed(e);
      }
    });
    add(cbRechercheColonneTarifNonNulle,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbPrixPublicCommePrixBase ----
    lbPrixPublicCommePrixBase.setText("Col 1 en prix base (PS316)");
    lbPrixPublicCommePrixBase.setModeReduit(true);
    lbPrixPublicCommePrixBase.setName("lbPrixPublicCommePrixBase");
    add(lbPrixPublicCommePrixBase,
        new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- cbPrixPublicCommePrixBase ----
    cbPrixPublicCommePrixBase.setEnabled(false);
    cbPrixPublicCommePrixBase.setName("cbPrixPublicCommePrixBase");
    cbPrixPublicCommePrixBase.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        cbPrixPublicCommePrixBaseActionPerformed(e);
      }
    });
    add(cbPrixPublicCommePrixBase,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbArticleNonRemisable ----
    lbArticleNonRemisable.setText("Article non remisable (PS281)");
    lbArticleNonRemisable.setModeReduit(true);
    lbArticleNonRemisable.setName("lbArticleNonRemisable");
    add(lbArticleNonRemisable,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfArticleNonRemisable ----
    tfArticleNonRemisable.setEnabled(false);
    tfArticleNonRemisable.setMaximumSize(new Dimension(30, 22));
    tfArticleNonRemisable.setModeReduit(true);
    tfArticleNonRemisable.setPreferredSize(new Dimension(30, 22));
    tfArticleNonRemisable.setMinimumSize(new Dimension(30, 22));
    tfArticleNonRemisable.setName("tfArticleNonRemisable");
    tfArticleNonRemisable.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfCNVArticleNonRemisableFocusLost(e);
      }
    });
    add(tfArticleNonRemisable,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbTypeUniteCNVQuantitative ----
    lbTypeUniteCNVQuantitative.setText("Type unit\u00e9 CNV quantitative");
    lbTypeUniteCNVQuantitative.setModeReduit(true);
    lbTypeUniteCNVQuantitative.setName("lbTypeUniteCNVQuantitative");
    add(lbTypeUniteCNVQuantitative,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfTypeUniteCNVQuantitative ----
    tfTypeUniteCNVQuantitative.setEnabled(false);
    tfTypeUniteCNVQuantitative.setMaximumSize(new Dimension(200, 22));
    tfTypeUniteCNVQuantitative.setModeReduit(true);
    tfTypeUniteCNVQuantitative.setName("tfTypeUniteCNVQuantitative");
    add(tfTypeUniteCNVQuantitative,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbTypeCumulCNVQuantitative ----
    lbTypeCumulCNVQuantitative.setText("Type cumul CNV quantitative");
    lbTypeCumulCNVQuantitative.setModeReduit(true);
    lbTypeCumulCNVQuantitative.setName("lbTypeCumulCNVQuantitative");
    add(lbTypeCumulCNVQuantitative,
        new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));

    // ---- tfTypeCumulCNVQuantitative ----
    tfTypeCumulCNVQuantitative.setEnabled(false);
    tfTypeCumulCNVQuantitative.setMaximumSize(new Dimension(200, 22));
    tfTypeCumulCNVQuantitative.setModeReduit(true);
    tfTypeCumulCNVQuantitative.setName("tfTypeCumulCNVQuantitative");
    add(tfTypeCumulCNVQuantitative,
        new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbTauxTVA;
  private SNPanel pnlTauxTVA;
  private SNTexte tfTauxTVA1;
  private SNTexte tfTauxTVA2;
  private SNTexte tfTauxTVA3;
  private SNTexte tfTauxTVA4;
  private SNTexte tfTauxTVA5;
  private SNTexte tfTauxTVA6;
  private SNLabelChamp lbModeNegoce;
  private SNCheckBoxReduit cbModeNegoce;
  private SNLabelChamp lbColonneTarif;
  private SNComboBoxReduit cbColonneTarif;
  private SNLabelChamp lbRechercheColonneTarifNonNulle;
  private JCheckBox cbRechercheColonneTarifNonNulle;
  private SNLabelChamp lbPrixPublicCommePrixBase;
  private SNCheckBoxReduit cbPrixPublicCommePrixBase;
  private SNLabelChamp lbArticleNonRemisable;
  private SNNombreEntier tfArticleNonRemisable;
  private SNLabelChamp lbTypeUniteCNVQuantitative;
  private SNTexte tfTypeUniteCNVQuantitative;
  private SNLabelChamp lbTypeCumulCNVQuantitative;
  private SNTexte tfTypeCumulCNVQuantitative;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
