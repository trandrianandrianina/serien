/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur;

import ri.serien.libcommun.gescom.personnalisation.transporteur.IdTransporteur;
import ri.serien.libcommun.gescom.personnalisation.transporteur.ListeTransporteur;
import ri.serien.libcommun.gescom.personnalisation.transporteur.Transporteur;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un transporteur.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un transporteur dans le logiciel.
 * Il gère l'affichage des transporteurs existants pour un établissement donné et la sélection de l'un d'entre eux.
 * Utilisation :
 * - Ajouter un composant SNTransporteur à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM20FM_B1.
 */
public class SNTransporteur extends SNComboBoxObjetMetier<IdTransporteur, Transporteur, ListeTransporteur> {
  
  /**
   * Constructeur par défaut.
   */
  public SNTransporteur() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeTransporteur());
  }
  
  /**
   * Sélectionner un transporteur de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampTransporteur) {
    if (pChampTransporteur == null || pLexical.HostFieldGetData(pChampTransporteur) == null) {
      throw new MessageErreurException("Impossible de sélectionner le transporteur car il est invalide.");
    }
    
    IdTransporteur idTransporteur = null;
    String valeurTransporteur = pLexical.HostFieldGetData(pChampTransporteur).trim();
    if (valeurTransporteur != null && !valeurTransporteur.trim().isEmpty() && !valeurTransporteur.equals("**")) {
      idTransporteur = IdTransporteur.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampTransporteur));
    }
    setIdSelection(idTransporteur);
  }
  
  /**
   * Renseigner le champs RPG correspondant au transporteur sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampTransporteur) {
    Transporteur transporteur = getSelection();
    if (transporteur != null) {
      pLexical.HostFieldPutData(pChampTransporteur, 0, transporteur.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampTransporteur, 0, "");
    }
  }
}
