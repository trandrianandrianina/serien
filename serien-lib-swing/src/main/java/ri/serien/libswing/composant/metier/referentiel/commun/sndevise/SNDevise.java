/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.commun.sndevise;

import ri.serien.libcommun.gescom.commun.devise.Devise;
import ri.serien.libcommun.gescom.commun.devise.IdDevise;
import ri.serien.libcommun.gescom.commun.devise.ListeDevise;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une devise.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une Devise dans le logiciel. Il gère l'affichage des devise existantes et
 * la sélection de l'une d'entre elle.
 *
 * Utilisation :
 * - Ajouter un composant SNDevise à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM03FM_B1
 */
public class SNDevise extends SNComboBoxObjetMetier<IdDevise, Devise, ListeDevise> {
  
  private ListeDevise listeDevise = null;
  
  /**
   * Constructeur par défaut.
   */
  public SNDevise() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeDevise());
  }
  
  /**
   * Sélectionner un code postal de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pDevise) {
    if (pDevise == null || pLexical.HostFieldGetData(pDevise) == null || pDevise.trim().isEmpty()) {
      throw new MessageErreurException("Impossible de sélectionner la devises car son code est invalide.");
    }
    
    IdDevise idDevise = null;
    String valeurDevise = pLexical.HostFieldGetData(pDevise);
    valeurDevise = valeurDevise.trim();
    if (valeurDevise != null && !valeurDevise.trim().isEmpty() && !valeurDevise.equals("**")) {
      String resultatDevise = valeurDevise.trim();
      idDevise = IdDevise.getInstance(resultatDevise);
    }
    setIdSelection(idDevise);
  }
  
  /**
   * Renseigner les champs RPG correspondant au code postal sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pDevise) {
    renseignerChampRPG(pLexical, pDevise, false);
  }
  
  /**
   * Renseigner les champs RPG correspondant au devis sélectionné.
   * cette méthode permet de spécifier la valeur qui est envoyée au programme RPG comme valeur "Tous" :
   * - Si pTousAvecEtoile est à false, le champ RPG contient des espaces lorsque "Tous" est sélectionné".
   * - Si pTousAvecEtoile est à true, le champ RPG contient "**" lorsque "Tous" est sélectionné".
   */
  public void renseignerChampRPG(Lexical pLexical, String pDevise, boolean pTousAvecEtoile) {
    Devise devise = getSelection();
    if (devise != null) {
      pLexical.HostFieldPutData(pDevise, 0, devise.getId().getCodeDevise());
    }
    else if (pTousAvecEtoile) {
      pLexical.HostFieldPutData(pDevise, 0, "**");
    }
    else {
      pLexical.HostFieldPutData(pDevise, 0, "");
    }
  }
}
