/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.affichagelienligne;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.ListeLienLigne;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libswing.moteur.composant.SNComposant;
import ri.serien.libswing.moteur.interpreteur.SessionBase;

/**
 * Composant d'affichage des lignes liées à une ligne d'origine.
 *
 * Ce composant doit être utilisé pour tout affichage des liens entre lignes dans le logiciel.
 *
 * Utilisation :
 * - Ajouter un composant SNLignesLiees à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé. Les informations nécessaires sont la session et l'établissement.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le ...
 */
public class SNLienLigne extends SNComposant {
  // Variables
  private SessionBase session = null;
  private ListeUnite listeUnite = null;
  private List<VueEncartLienLigne> listeEncart = null;
  private LienLigne lienOrigine = null;

  /**
   * Cosntructeur.
   */
  public SNLienLigne() {
    initComponents();
  }

  /**
   * Charger la liste des liens.
   */
  public void charger(boolean pForcerRafraichissement) {
    // Récupération de la liste des unités
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(getSession().getIdSession());

    if (lienOrigine != null && session != null && listeUnite != null) {
      chargerLignesLiees();
    }
  }

  /**
   * Changer la couleur de fond du composant
   */
  @Override
  public void setBackground(Color pColor) {
    if (pColor == null || scLiens == null || pnlLiens == null) {
      return;
    }
    super.setBackground(pColor);
    scLiens.setBackground(pColor);
    pnlLiens.setBackground(pColor);
  }

  /**
   * Charger les lignes liées à la ligne en cours
   */
  private void chargerLignesLiees() {
    listeEncart = new ArrayList<VueEncartLienLigne>();
    Client client = null;
    VueEncartLienLigne encart = null;

    // Charger la liste des liens
    ListeLienLigne listelienLigne = ListeLienLigne.chargerTout(session.getIdSession(), lienOrigine.getId(), lienOrigine.getIdMagasin());
    if (listelienLigne == null) {
      return;
    }

    for (LienLigne lienLigne : listelienLigne) {
      // Vérifier que le lien n'est pas null
      if (lienLigne == null) {
        continue;
      }

      lienLigne.setListeUnite(listeUnite);
      encart = new VueEncartLienLigne(lienLigne, listeUnite);
      if (lienLigne.getDocumentVente() != null && lienLigne.getDocumentVente().getIdClientFacture() != null) {
        client = lienLigne.getDocumentVente().chargerClientFacture(session.getIdSession());
        encart.setEnCompte(client.isClientEnCompte());
      }

      listeEncart.add(encart);
    }

    rafraichirLiens();
  }

  /**
   * Construction de la vue des liens
   */
  private void rafraichirLiens() {
    // Si les liens sont affichables
    if (listeEncart != null) {
      pnlLiens.setVisible(true);

      int compteurAchat = 0;
      int compteurCommande = 0;
      int compteurBon = 0;

      for (int i = 0; i < listeEncart.size(); i++) {
        switch (listeEncart.get(i).getTypeLienLigne()) {
          // On ajoute un encart par lien sur une ligne de vente
          case LIEN_LIGNE_VENTE:
            if (listeEncart.get(i).isCommande()) {
              pnlLiens.add(listeEncart.get(i), new GridBagConstraints(1, compteurCommande, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
              compteurCommande++;
            }
            else if (!listeEncart.get(i).isNonDefini()) {
              pnlLiens.add(listeEncart.get(i), new GridBagConstraints(2, compteurBon, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
              compteurBon++;
            }
            listeEncart.get(i).setOpaque(true);
            listeEncart.get(i).setVisible(true);

            break;
          // On ajoute un encart par lien sur une ligne d'achat
          case LIEN_LIGNE_ACHAT:
            pnlLiens.add(listeEncart.get(i), new GridBagConstraints(0, compteurAchat, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
            listeEncart.get(i).setOpaque(true);
            listeEncart.get(i).setVisible(true);
            compteurAchat++;
            break;

          default:
            break;
        }
      }

      // On rafraichit les encarts
      for (int j = 0; j < listeEncart.size(); j++) {
        listeEncart.get(j).rafraichir();
      }

      // S'il n'y a pas d'encart de vente on met un panel vide pour centrer la présentation
      if (compteurCommande == 0) {
        JPanel panelComplement = new JPanel();
        panelComplement.setOpaque(false);
        panelComplement.setMinimumSize(new Dimension(300, 130));
        panelComplement.setPreferredSize(new Dimension(300, 130));
        pnlLiens.add(panelComplement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      if (compteurBon == 0) {
        JPanel panelComplement = new JPanel();
        panelComplement.setOpaque(false);
        panelComplement.setMinimumSize(new Dimension(300, 130));
        panelComplement.setPreferredSize(new Dimension(300, 130));
        pnlLiens.add(panelComplement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      // S'il n'y a pas d'encart d'achat on met un panel vide pour centrer la présentation
      if (compteurAchat == 0) {
        JPanel panelComplement = new JPanel();
        panelComplement.setOpaque(false);
        panelComplement.setMinimumSize(new Dimension(300, 130));
        panelComplement.setPreferredSize(new Dimension(300, 130));
        pnlLiens.add(panelComplement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }

      // On place la scrollbar en haut de la fenêtre
      javax.swing.SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          scLiens.getVerticalScrollBar().setValue(0);
        }
      });
    }
    // Si aucun lien on cache le panel
    else {
      pnlLiens.setVisible(false);
    }
  }

  public SessionBase getSession() {
    return session;
  }

  public void setSession(SessionBase session) {
    this.session = session;
  }

  public LienLigne getLienOrigine() {
    return lienOrigine;
  }

  public void setLienOrigine(LienLigne lienOrigine) {
    this.lienOrigine = lienOrigine;
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    scLiens = new JScrollPane();
    pnlLiens = new JPanel();

    // ======== this ========
    setPreferredSize(new Dimension(1000, 400));
    setMinimumSize(new Dimension(1000, 400));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };

    // ======== scLiens ========
    {
      scLiens.setBackground(new Color(238, 238, 210));
      scLiens.setBorder(null);
      scLiens.setOpaque(true);
      scLiens.setName("scLiens");

      // ======== pnlLiens ========
      {
        pnlLiens.setAutoscrolls(true);
        pnlLiens.setBackground(new Color(238, 238, 210));
        pnlLiens.setName("pnlLiens");
        pnlLiens.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlLiens.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlLiens.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlLiens.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlLiens.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      }
      scLiens.setViewportView(pnlLiens);
    }
    add(scLiens,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scLiens;
  private JPanel pnlLiens;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
