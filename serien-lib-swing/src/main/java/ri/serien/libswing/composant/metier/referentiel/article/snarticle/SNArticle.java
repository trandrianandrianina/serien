/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.snarticle;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;

import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleConsigne;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleStock;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un article standard.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un article standard dans le logiciel. Il gère l'affichage d'un article
 * standard et la sélection de l'un d'entre eux.
 * Il comprend comprenant un TextField et un bouton permettant de sélectionner un article standard dans une boîte de dialogue.
 *
 * Utilisation :
 * - Ajouter un composant SNArticle à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé. Les informations nécessaires sont la session et l'établissement.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM05FM format B5.
 */
public class SNArticle extends SNComposantAvecEtablissement {
  private SNTexte tfTexteRecherche;
  private SNBoutonRecherche btnDialogueRechercheArticle;
  private ArticleBase articleSelectionne = null;
  private boolean fenetreArticleAffichee = false;
  private boolean rafraichir = false;
  private String texteRecherchePrecedente = null;
  private CritereArticle critereArticle = new CritereArticle();
  
  /**
   * Constructeur par défaut.
   */
  public SNArticle() {
    super();
    setName("SNArticleStandard");

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);

    setLayout(new GridBagLayout());

    // Texte de recherche
    tfTexteRecherche = new SNTexte();
    tfTexteRecherche.setName("tfArticle");
    tfTexteRecherche.setEditable(true);
    tfTexteRecherche.setToolTipText(VueArticle.RECHERCHE_GENERIQUE_TOOLTIP);
    add(tfTexteRecherche,
        new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

    // Bouton loupe
    btnDialogueRechercheArticle = new SNBoutonRecherche();
    btnDialogueRechercheArticle.setToolTipText("Recherche d'article");
    btnDialogueRechercheArticle.setName("btnRechercheArticle");
    add(btnDialogueRechercheArticle, new GridBagConstraints());

    // Focus listener pour lancer la recherche quand on quitte la zone de saisie
    tfTexteRecherche.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        tfTexteRechercheFocusLost(e);
      }

      @Override
      public void focusGained(FocusEvent e) {
        tfTexteRechercheFocusGained(e);
      }
    });

    // ActionListener pour capturer l'utilisation de la touche entrée
    tfTexteRecherche.addActionListener(new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tfTexteRechercheActionPerformed(e);
      }
    });

    // Être notifié lorsque le bouton est cliqué
    btnDialogueRechercheArticle.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnDialogueRechercheArticleActionPerformed(e);
      }
    });
  }

  /**
   * Activer ou désactiver le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    tfTexteRecherche.setEnabled(pEnabled);
    btnDialogueRechercheArticle.setEnabled(pEnabled);
  }

  /**
   * Positionner le focus sur le composant de saisie.
   */
  @Override
  public boolean requestFocusInWindow() {
    return tfTexteRecherche.requestFocusInWindow();
  }

  /**
   * Charger la liste des articles standard.
   * Cette méthode ne fait rien dans ce composant car la liste des articles standard est chargée à la demande en
   * fonction de la saisie.
   * Elle est
   * conservée par souci de comptabilité avec les autres composants graphiques.
   */
  public void charger(boolean pForcerRafraichissement) {
    // Tester s'il faut rafraîchir la liste
    if (!rafraichir && !pForcerRafraichissement) {
      return;
    }
    rafraichir = false;
  }

  /**
   * Initialiser les valeurs du composant pour se retrouver dans la même situation que si aucun article standard n'est
   * sélectionné.
   */
  public void initialiserRecherche() {
    // Garde le filtre , permet de garder en mémoire si l'utilisateur à changer le filtre
    EnumFiltreArticleStock filtreStock = critereArticle.getFiltreStock();

    // Effacer les critères de recherche
    critereArticle.initialiser();

    // Remet le filtre stock
    critereArticle.setFiltreStock(filtreStock);

    // Ne sélectionner aucun article standard
    setSelection(null);
  }

  /**
   * Sélectionner un ArticleBase à partir des champs RPG.
   */
  public void setSelectionParChampRPG(Lexical pLexical, String pChampCodeArticle) {
    String valeurRPG = pLexical.HostFieldGetData(pChampCodeArticle);
    if (pChampCodeArticle == null || valeurRPG == null) {
      throw new MessageErreurException("Impossible de sélectionner l'article car son code est invalide.");
    }

    // Si le champ RPG est vide on initialise la recherche
    if (valeurRPG.trim().isEmpty()) {
      initialiserRecherche();
    }
    // Si le champ RPG contient un code article, on vérifie si celui-ci a changé
    else if (articleSelectionne == null || !articleSelectionne.getId().getCodeArticle().equals(valeurRPG)) {
      // On initialise le code article dans les critères pour de futures recherches
      critereArticle.setTexteRechercheGenerique(valeurRPG);

      // On sélectionne l'article à partir du code fourni par le RPG, sans passer par la recherche
      // chargement ultra léger : seulement l'identifiant et le libellé
      IdArticle idArticleBase = IdArticle.getInstance(getIdEtablissement(), valeurRPG);
      ArticleBase articleBase = ManagerServiceArticle.chargerArticleBase(getSession().getIdSession(), idArticleBase);
      setSelection(articleBase);
    }

    rafraichirTexteRecherche();

  }

  /**
   * Renseigner les champs RPG correspondant à l'ArticleBase sélectionné.
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampCodeArticle) {
    ArticleBase article = getSelection();
    if (article != null) {
      pLexical.HostFieldPutData(pChampCodeArticle, 0, article.getId().getCodeArticle());
    }
    else {
      pLexical.HostFieldPutData(pChampCodeArticle, 0, "");
    }
  }

  /**
   * Lancer une recherche parmi les articles standard.
   */
  private void lancerRecherche(boolean forcerAffichageDialogue) {
    // Vérifier la session
    if (getSession() == null) {
      throw new MessageErreurException("La session du composant de sélection d'article est invalide.");
    }

    // Ne rien faire si le texte à rechercher est vide
    if (tfTexteRecherche.getText().trim().isEmpty()) {
      setSelection(null);
      if (forcerAffichageDialogue) {
        afficherDialogueRechercheArticle(null);
      }
      return;
    }

    // Ne rien faire si le texte à rechercher n'a pas changé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && tfTexteRecherche.getText().equals(texteRecherchePrecedente)) {
      return;
    }

    articleSelectionne = null;

    // Rechercher les identifiants articles correspondant aux critères de recherche
    critereArticle.setIdEtablissement(getIdEtablissement());
    critereArticle.setTexteRechercheGenerique(tfTexteRecherche.getText());
    List<IdArticle> listeIdArticle = ManagerServiceArticle.chargerListeIdArticle(getSession().getIdSession(), critereArticle);

    // Sélectionner automatiquement le seul article trouvé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && listeIdArticle.size() == 1) {
      ListeArticleBase listeArticleBase = ListeArticleBase.chargerParId(getSession().getIdSession(), listeIdArticle);
      setSelection(listeArticleBase.get(0));

    }
    // Afficher la boîte de dialogue si : affichage forcé de la boîte de dialogue, aucun ou plusieurs articles trouvés
    else {
      afficherDialogueRechercheArticle(listeIdArticle);
    }

    // Sélectionner automatiquement l'ensemble du champ afin que la prochaine saisie vienne en remplacement
    tfTexteRecherche.selectAll();
  }

  /**
   * Affiche la fenêtre de recherche des articles
   */
  private void afficherDialogueRechercheArticle(List<IdArticle> listeIdArticle) {
    // Sécurité pour éviter que la boîte de dialogue ne s'affiche deux fois
    if (fenetreArticleAffichee) {
      return;
    }
    fenetreArticleAffichee = true;
    
    // Afficher la boîte de dialogue de recherche d'un article
    critereArticle.setIdEtablissement(getIdEtablissement());
    critereArticle.setTexteRechercheGenerique(tfTexteRecherche.getText());
    // Si on a déjà sélectionné un article, force la recherche par le code article, cela va permettre d'afficher l'article sélectionné
    // uniquement
    if (articleSelectionne != null) {
      critereArticle.setTexteRechercheGenerique(articleSelectionne.getId().getCodeArticle());
    }
    ModeleArticle modele = new ModeleArticle(getSession(), critereArticle);
    modele.setListeIdArticle(listeIdArticle);
    // La recherche est lancée uniquement si le texte de la recherche générique n'est pas vide dans le cas contraire ce sera à
    // l'utilisateur de lancer la recherche manuellement car le temps de chargement peut être long dans ce cas de figure
    if (!tfTexteRecherche.getText().trim().isEmpty()) {
      modele.lancerRecherche();
    }
    
    VueArticle vue = new VueArticle(modele);
    vue.afficher();
    
    // Sélectionner l'article retourné par la boîte de dialogue (ou aucun si null)
    if (modele.isSortieAvecValidation()) {
      setSelection(modele.getArticleSelectionne());
    }
    // La boîte de dialogue est annulée, on remet l'article sélectionné précédemment
    else {
      rafraichirTexteRecherche();
    }
    fenetreArticleAffichee = false;
  }

  /**
   * Rafraîchir le texte de la recherche avec l'article sélectionné.
   */
  private void rafraichirTexteRecherche() {
    // Mettre à jour le composant
    if (articleSelectionne != null) {
      tfTexteRecherche.setText(articleSelectionne.toString());
    }
    else {
      tfTexteRecherche.setText("");
    }

    tfTexteRecherche.setCaretPosition(0);
    // Mémoriser le dernier texte pour ne pas relancer une recherche inutile
    texteRecherchePrecedente = tfTexteRecherche.getText();
  }

  // -- Méthodes évènementielles

  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      if (getSelection() == null) {
        return;
      }

      // On sélectionne l'ensemble du libellé affiché
      tfTexteRecherche.selectAll();
      // Si le textField est trop petit pour tout afficher on met le curseur au début du libellé pour ne pas afficher que la fin
      if (getSelection().getLibelleComplet() != null
          && tfTexteRecherche.getSize().getWidth() < (getSelection().getLibelleComplet().length() * 10)) {
        tfTexteRecherche.setCaretPosition(0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfTexteRechercheFocusLost(FocusEvent e) {
    try {
      lancerRecherche(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfTexteRechercheActionPerformed(ActionEvent e) {
    try {
      lancerRecherche(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void btnDialogueRechercheArticleActionPerformed(ActionEvent e) {
    try {
      if (articleSelectionne == null) {
        lancerRecherche(true);
      }
      else {
        ArrayList<IdArticle> listeIdArticle = new ArrayList<IdArticle>();
        afficherDialogueRechercheArticle(listeIdArticle);
      }
    }
    catch (

    Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  // -- Accesseurs

  /**
   * Article sélectionné.
   */
  public ArticleBase getSelection() {
    return articleSelectionne;
  }

  /**
   * Modifier l'article sélectionné.
   */
  public void setSelection(ArticleBase pArticleBaseSelectionne) {
    // Tester si la sélection a changé
    boolean bSelectionModifiee = !Constantes.equals(articleSelectionne, pArticleBaseSelectionne);

    // Modifier l'article sélectionné
    articleSelectionne = pArticleBaseSelectionne;

    // Effacer les critères de recherche si aucun article n'est sélectionné
    if (articleSelectionne == null) {
      // Garde le filtre , permet de garder en mémoire si l'utilisateur à changer le filtre
      EnumFiltreArticleStock filtreStock = critereArticle.getFiltreStock();

      // Effacer les critères de recherche
      // critereArticle.initialiser();

      // Remets le filtre stock
      critereArticle.setFiltreStock(filtreStock);
    }

    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();

    // Notifier uniquement si la sélection a changé
    if (bSelectionModifiee) {
      fireValueChanged();
    }
  }

  /*
   * Rechercher les articles gérés en stock
   */
  public void setFiltreArticleStock(EnumFiltreArticleStock pEnumFiltreStock) {
    critereArticle.setFiltreStock(pEnumFiltreStock);
  }

  /*
   * Fixer le filtre sur les articles commentaires
   */
  public void setFiltreArticleCommentaire(EnumFiltreArticleCommentaire pEnumFiltreCommentaire) {
    critereArticle.setFiltreCommentaire(pEnumFiltreCommentaire);
  }

  /*
   * Fixer le filtre sur les articles consignés
   */
  public void setFiltreArticleConsigne(EnumFiltreArticleConsigne pEnumFiltreConsigne) {
    critereArticle.setFiltreConsigne(pEnumFiltreConsigne);
  }

  public void setVerrouillerCriteres(boolean pVerrouillerCriteres) {
    critereArticle.setVerrouillerCritere(pVerrouillerCriteres);
  }

}
