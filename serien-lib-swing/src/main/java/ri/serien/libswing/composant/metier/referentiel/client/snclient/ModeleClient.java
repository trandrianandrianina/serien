/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.client.snclient;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.configurationtableau.ConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.ManagerConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.colonneconfigurationtableau.ListeColonneConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.tableau.EnumTableau;
import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.CritereClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.ListeCategorieClient;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleClient extends AbstractModeleDialogue {
  private CritereClient critereClient = null;
  private ListeCategorieClient listeCategorieClient = null;
  private CodePostalCommune codePostalCommune = null;
  private boolean rechercheProspectAutorisee = false;
  private ConfigurationTableau configurationTableau = null;
  private ClientBase clientBaseSelectionne = null;
  private ListeClientBase listeClientBase = null;

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   * @param pSession La session.
   * @param pCriteresClient Les critères de recherche des clients.
   * @param pListeChampMetierParDefaut La liste des numéros des champs métiers à afficher par défaut.
   */
  public ModeleClient(SessionBase pSession, CritereClient pCriteresClient) {
    super(pSession);
    critereClient = pCriteresClient;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialiser les données du modèle.
   */
  @Override
  public void initialiserDonnees() {
    // Charger la configuration de ce tableau
    configurationTableau = ManagerConfigurationTableau.getInstance().charger(getIdSession(), critereClient.getIdEtablissement(),
        EnumTableau.SELECTION_CLIENT);
  }
  
  /**
   * Charger les données du modèle.
   */
  @Override
  public void chargerDonnees() {
    // Charger la liste des catégories clients
    listeCategorieClient = new ListeCategorieClient();
    listeCategorieClient = listeCategorieClient.charger(getIdSession(), critereClient.getIdEtablissement());
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialiser la recherche.
   */
  public void initialiserRecherche() {
    critereClient.initialiser();
    listeClientBase = null;
    clientBaseSelectionne = null;
    codePostalCommune = null;
    rafraichir();
  }
  
  /**
   * Lancer une recherche sur les clients.
   */
  public void lancerRecherche() {
    critereClient.setCodePostalCommune(codePostalCommune);
    listeClientBase = ListeClientBase.charger(getIdSession(), critereClient);
    rafraichir();
  }
  
  /**
   * Modifier le texte de la recherche générique.
   */
  public void modifierTexteRecherche(String pTexteRecherche) {
    if (Constantes.equals(critereClient.getTexteRechercheGenerique(), pTexteRecherche)) {
      return;
    }
    critereClient.setTexteRechercheGenerique(pTexteRecherche);
    rafraichir();
  }
  
  /**
   * Modifier le filtre sur le type de compte client.
   */
  public void modifierTypeCompteClient(EnumTypeCompteClient pTypeCompteClient) {
    if (Constantes.equals(critereClient.getTypeCompteClient(), pTypeCompteClient)) {
      return;
    }
    critereClient.setTypeCompteClient(pTypeCompteClient);
    rafraichir();
  }
  
  /**
   * Modifier le filtre sur le code postal et/ou la commune
   */
  public void modifierCodePostalCommune(CodePostalCommune pCodePostalCommune) {
    if (Constantes.equals(critereClient.getCodePostalCommune(), pCodePostalCommune)) {
      return;
    }
    
    if (pCodePostalCommune == null) {
      return;
    }
    
    if (codePostalCommune == null) {
      codePostalCommune = CodePostalCommune.getInstance("", "");
    }
    
    if (!codePostalCommune.getCodePostal().equalsIgnoreCase(pCodePostalCommune.getCodePostal())) {
      codePostalCommune.setCodePostal(pCodePostalCommune.getCodePostal());
    }
    if (!codePostalCommune.getVille().equalsIgnoreCase(pCodePostalCommune.getVille())) {
      codePostalCommune.setVille(pCodePostalCommune.getVille());
    }
    
    lancerRecherche();
    rafraichir();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Liste des clients constituant le résultat de la recherche.
   */
  public ListeClientBase getListeClientBase() {
    return listeClientBase;
  }
  
  /**
   * Renseigner la liste des clients à afficher comme résultat de la recherche.
   */
  public void setListeClientBase(ListeClientBase pListeClientBase) {
    listeClientBase = pListeClientBase;
  }
  
  /**
   * Client sélectionné.
   */
  public ClientBase getClientSelectionne() {
    return clientBaseSelectionne;
  }
  
  /**
   * Modifier le client sélectionné.
   */
  public void setClientSelectionne(ClientBase pClientBase) {
    clientBaseSelectionne = pClientBase;
  }
  
  /**
   * Filtre sur l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return critereClient.getIdEtablissement();
  }
  
  /**
   * Filtre sur le type de compte client.
   */
  public EnumTypeCompteClient getTypeCompteClient() {
    return critereClient.getTypeCompteClient();
  }
  
  /**
   * Texte de la recherche générique.
   */
  public String getTexteRecherche() {
    return critereClient.getTexteRechercheGenerique();
  }
  
  /**
   * Renvoyer le CodePostalCommune des critères de recherche client
   */
  public CodePostalCommune getCodePostalCommune() {
    return codePostalCommune;
  }
  
  /**
   * Liste des catégories de clients.
   */
  public ListeCategorieClient getListeCategorieClient() {
    return listeCategorieClient;
  }
  
  /**
   * Indiquer si la recherche de prospects est autorisée
   */
  public boolean isRechercheProspectAutorisee() {
    return rechercheProspectAutorisee;
  }
  
  /**
   * Indiquer si la recherche de prospects est autorisée
   */
  public void setRechercheProspectAutorisee(boolean pRechercheProspectAutorisee) {
    rechercheProspectAutorisee = pRechercheProspectAutorisee;
  }
  
  /**
   * Message d'information à afficher dans la boîte de dialogue.
   */
  public Message getTitreListe() {
    if (listeClientBase == null) {
      return Message.getMessageNormal("Saisir les critères de recherche");
    }
    else if (listeClientBase.size() == 1) {
      return Message.getMessageNormal("Client correspondant à votre recherche (1)");
    }
    else if (listeClientBase.size() > 1) {
      return Message.getMessageNormal("Clients correspondants à votre recherche (" + listeClientBase.size() + ")");
    }
    else {
      return Message.getMessageImportant("Aucun client ne correspond à votre recherche");
    }
  }
  
  /**
   * Retourner la configuration du tableau.
   * @return Condiguration du tableau.
   */
  
  public ConfigurationTableau getConfigurationTableau() {
    return configurationTableau;
  }
  
  /**
   * Retourner la liste des colonnes si une préférence a été sauvegardée.
   * @return La liste des colonne du tableau
   */
  public ListeColonneConfigurationTableau getListeColonneConfigurationTableau() {
    if (configurationTableau == null) {
      return null;
    }
    return configurationTableau.getListeColonne();
  }
}
