/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.panel;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Rectangle;

import javax.swing.JPanel;

import ri.serien.libcommun.outils.image.OutilImage;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Panneau présentant un fond avec un dégradé de couleurs.
 *
 * Le dégradé va d'une couleur dite foncée à une couleur dite claire. La oculeur claire est calculée à partir de la couleur foncée fournie
 * au composant. La direction du dégradé peut être horizontale ou verticale :
 * - Si la direction est horizontale, le dégradé va de la couleur foncée à gauche vers la couleur claire à droite.
 * - Si la direction est verticale, le dégradé va de la couleur foncée en bas à la couleur claire en haut.
 *
 * Composants alternatifs :
 * - SNPanelDegradeGris : Bien que SNPanelDegrade a des valeurs par défaut grises (en cas de paramètres invalides), il ne faut pas compter
 * dessus. Pour être certains d'avoir un dégradé gris, utiliser SNPanelDegradeGris.
 */
public class SNPanelDegrade extends JPanel {
  // Constantes
  public static final int DIRECTION_HORIZONTAL = 0;
  public static final int DIRECTION_VERTICAL = 1;
  private static final float POURCENTAGE_ECLAIRCISSEMENT = 0.90f;

  // Variables
  private int direction = DIRECTION_HORIZONTAL;
  private Color couleurFoncee = SNCharteGraphique.COULEUR_FOND_DEGRADE_GRIS_FONCE;

  /**
   * Constructeur par défaut.
   * La direction et les couleurs par défaut sont alors utilisées (horizontal et gris).
   */
  public SNPanelDegrade() {
    super();
  }

  /**
   * Constructeur avec précision de la direction et des couleurs claires et foncés du dégradé.
   */
  public SNPanelDegrade(int pDirection, Color pCouleurFoncee) {
    super();
    setDirection(pDirection);
    setCouleurFoncee(pCouleurFoncee);
  }

  /**
   * Dessiner le composant.
   */
  @Override
  protected void paintComponent(Graphics graphics) {
    super.paintComponent(graphics);

    // Récupérer les bords du panneau
    Rectangle bounds = getBounds();

    // Calculer la couleur claire
    Color couleurClaire = OutilImage.eclaircirCouleur(couleurFoncee, POURCENTAGE_ECLAIRCISSEMENT);

    // Construire le gradient de couleur
    Paint gradientPaint = null;
    if (direction == DIRECTION_HORIZONTAL) {
      gradientPaint = new GradientPaint(bounds.x, 0, couleurFoncee, bounds.x + bounds.width, 0, couleurClaire);
    }
    else {
      gradientPaint = new GradientPaint(0, bounds.y + bounds.height, couleurFoncee, 0, bounds.y, couleurClaire);
    }

    // Colorier le panneau en dérgadé
    Graphics2D graphics2D = (Graphics2D) graphics;
    graphics2D.setPaint(gradientPaint);
    graphics2D.fillRect(0, 0, bounds.width, bounds.height);
  }

  /**
   * Couleur foncée du dégradé.
   * La couleur claire est déterminée à partir de la couleur foncée.
   */
  public Color getCouleurFoncee() {
    return couleurFoncee;
  }

  /**
   * Changer la couleur foncée du dégradé.
   * On met gris foncé par défaut si le paramètre est null.
   * @param pCouleurFoncee Couleur foncée du dégradé.
   */
  public void setCouleurFoncee(Color pCouleurFoncee) {
    if (pCouleurFoncee != null) {
      couleurFoncee = pCouleurFoncee;
    }
    else {
      couleurFoncee = SNCharteGraphique.COULEUR_FOND_DEGRADE_GRIS_FONCE;
    }

    repaint();
  }

  /**
   * Direction du dégradé (horizontal ou vertical).
   */
  public int getDirection() {
    return direction;
  }

  /**
   * Changer la direction du dégradé (horizontal ou vertical).
   * Le sens horizontal est pris par défaut si le paramètre est invalide.
   */
  public void setDirection(int pDirection) {
    if (pDirection == DIRECTION_VERTICAL) {
      direction = DIRECTION_VERTICAL;
    }
    else {
      direction = DIRECTION_HORIZONTAL;
    }

    repaint();
  }
}
