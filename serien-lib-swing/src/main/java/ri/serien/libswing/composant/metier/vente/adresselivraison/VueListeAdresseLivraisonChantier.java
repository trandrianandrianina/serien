/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.adresselivraison;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.adressedocument.AdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.ListeAdresseDocument;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Boite de dialogue de gestion des adresses associées à un document de vente.
 */
public class VueListeAdresseLivraisonChantier extends AbstractVueDialogue<ModeleListeAdresseLivraisonChantier> {
  // Constantes
  private static final String BOUTON_CREER_ADRESSE = "Créer";
  private static final String BOUTON_MODIFIER_ADRESSE = "Modifier";
  private static final String BOUTON_SUPPRIMER_ADRESSE = "Supprimer";
  private static final String[] TITRE_LISTE_ADRESSE =
      new String[] { "Nom ou raison sociale", "Compl\u00e9ment de nom", "Adresse 1", "Adresse 2", "Code postal", "Ville" };

  // Variables
  private DefaultTableModel tableModelDocument = null;

  // Classe interne qui permet de gérer les flèches dans la table tblListe
  private class ActionCellule extends AbstractAction {
    // Variables
    private Action actionOriginale = null;

    public ActionCellule(String pAction, Action pActionOriginale) {
      super(pAction);
      actionOriginale = pActionOriginale;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      validerSelectionLigne(e, actionOriginale);
    }

  }

  /**
   * Constructeur
   */
  public VueListeAdresseLivraisonChantier(ModeleListeAdresseLivraisonChantier pModele) {
    super(pModele);
    setBackground(SNCharteGraphique.COULEUR_FOND);
  }

  @Override
  public void initialiserComposants() {
    initComponents();
    setTitle("Liste des adresses de livraison pour le chantier " + getModele().getIdDocument().getTexte());

    // La liste
    scpListeAdresse.getViewport().setBackground(Color.WHITE);
    tblListeAdresse.personnaliserAspect(TITRE_LISTE_ADRESSE, new int[] { 150, 150, 200, 200, 80, 150 },
        new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE }, 14);
    // Modification de la gestion des évènements sur la touche ENTER de la jtable
    final Action originalActionEnter = retournerActionComposant(tblListeAdresse, SNCharteGraphique.TOUCHE_ENTREE);
    String actionEnter = "actionEnter";
    ActionCellule actionCelluleEnter = new ActionCellule(actionEnter, originalActionEnter);
    tblListeAdresse.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_ENTREE, actionEnter);
    tblListeAdresse.getActionMap().put(actionEnter, actionCelluleEnter);
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewportLigneAchat = scpListeAdresse.getViewport();
    viewportLigneAchat.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeAdresseStateChanged(e);
      }
    });
    
    // Composant communes
    snCodePostalCommuneLivraison.setSession(getModele().getSession());
    snCodePostalCommuneLivraison.setIdEtablissement(getModele().getIdDocument().getIdEtablissement());
    snCodePostalCommuneLivraison.charger(false);

    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CREER_ADRESSE, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER_ADRESSE, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER_ADRESSE, 's', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });

    // on limite la taille de saisie à la taille maximum définie en base de données
    tfNomLivraison.setLongueur(Adresse.LONGUEUR_ADRESSE);
    tfComplementNomLivraison.setLongueur(Adresse.LONGUEUR_ADRESSE);
    tfRueLivraison.setLongueur(Adresse.LONGUEUR_ADRESSE);
    tfLocalisationLivraison.setLongueur(Adresse.LONGUEUR_ADRESSE);
  }
  
  @Override
  public void rafraichir() {
    rafraichirListeAdresses();
    rafraichirAdresseSelectionnee();
    rafraichirBoutons();
    rafraichirTitreListe();
  }
  
  private void rafraichirListeAdresses() {
    ListeAdresseDocument listeAdresse = getModele().getListeAdresseDocument();
    String[][] donnees = null;

    if (listeAdresse != null) {
      donnees = new String[listeAdresse.size()][TITRE_LISTE_ADRESSE.length];
      for (int i = 0; i < listeAdresse.size(); i++) {
        AdresseDocument adresseDocument = listeAdresse.get(i);
        String[] ligne = donnees[i];

        Adresse adresse = adresseDocument.getAdresse();
        if (adresse == null) {
          continue;
        }
        
        // Nom ou raison sociale
        ligne[0] = adresse.getNom();

        // Complément de nom
        ligne[1] = adresse.getComplementNom();

        // Adresse 1
        ligne[2] = adresse.getRue();

        // Adresse 2
        ligne[3] = adresse.getLocalisation();

        // Code postal
        ligne[4] = Constantes.convertirIntegerEnTexte(adresse.getCodePostal(), 0);

        // Ville
        ligne[5] = adresse.getVille();
      }
    }

    tblListeAdresse.mettreAJourDonnees(donnees);

    // Sélectionner la ligne en cours
    int index = getModele().getIndexAdresseSelectionne();
    if (index > -1 && index < tblListeAdresse.getRowCount()) {
      tblListeAdresse.getSelectionModel().setSelectionInterval(index, index);
    }
  }

  /**
   * Rafraichir pavé adresse sélectionnée
   */
  private void rafraichirAdresseSelectionnee() {
    Adresse adresseDocument = getModele().getBlocAdresseEnCours();
    
    if (adresseDocument == null) {
      tfNomLivraison.setText("");
      tfComplementNomLivraison.setText("");
      tfRueLivraison.setText("");
      tfLocalisationLivraison.setText("");
      snCodePostalCommuneLivraison.setSelection(null);
    }
    else {
      tfNomLivraison.setText(adresseDocument.getNom());
      tfComplementNomLivraison.setText(adresseDocument.getComplementNom());
      tfRueLivraison.setText(adresseDocument.getRue());
      tfLocalisationLivraison.setText(adresseDocument.getLocalisation());
      snCodePostalCommuneLivraison
          .setSelection(CodePostalCommune.getInstance(adresseDocument.getCodePostalFormate(), adresseDocument.getVille()));
    }

    tfNomLivraison.setEnabled(getModele().isAdresseModifiable());
    tfComplementNomLivraison.setEnabled(getModele().isAdresseModifiable());
    tfRueLivraison.setEnabled(getModele().isAdresseModifiable());
    tfLocalisationLivraison.setEnabled(getModele().isAdresseModifiable());
    snCodePostalCommuneLivraison.setEnabled(getModele().isAdresseModifiable());
  }

  /**
   * Rafraichir l'affichage des boutons
   */
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, isDonneesChargees()
        && (tblListeAdresse.getSelectedRow() >= 0 || getModele().getEtape() == ModeleListeAdresseLivraisonChantier.ETAPE_CREATION));
    snBarreBouton.activerBouton(BOUTON_MODIFIER_ADRESSE, isDonneesChargees() && tblListeAdresse.getSelectedRow() >= 0
        && getModele().getEtape() == ModeleListeAdresseLivraisonChantier.ETAPE_CONSULTATION && getModele().isAvecBonton());
    snBarreBouton.activerBouton(BOUTON_SUPPRIMER_ADRESSE, isDonneesChargees() && tblListeAdresse.getSelectedRow() > 0
        && getModele().getEtape() == ModeleListeAdresseLivraisonChantier.ETAPE_CONSULTATION && getModele().isAvecBonton());
    snBarreBouton.activerBouton(BOUTON_CREER_ADRESSE,
        getModele().getEtape() == ModeleListeAdresseLivraisonChantier.ETAPE_CONSULTATION && getModele().isAvecBonton());
  }
  
  /**
   * Rafraichir le message au-dessus de la liste de mail
   */
  private void rafraichirTitreListe() {
    Message message = getModele().getTitreListe();
    if (message != null) {
      lbTitreListe.setMessage(message);
    }
    else {
      lbTitreListe.setText("");
    }
  }
  
  /**
   * Récupère l'adresse sélectionnée.
   */
  private void validerListeSelectionAdresse() {
    getModele().modifierAdresseSelectionnee(tblListeAdresse.getIndiceSelection());
    getModele().afficherDetailAdresse();
  }
  
  /**
   * Traiter l'appui sur la touche entrée dans la tableau.
   */
  private void validerSelectionLigne(ActionEvent e, Action actionOriginale2) {
    try {
      validerListeSelectionAdresse();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  // -- Méthodes interractives

  private void scpListeAdresseStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeAdresse == null) {
        return;
      }

      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeAdresse.getViewport().getViewRect();
      int premiereLigne = tblListeAdresse.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeAdresse.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeAdresse.getRowCount() - 1;
      }

      // Charges les articles concernés si besoin
      getModele().modifierPlageAdresseAffiche(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      if (pSNBouton.isBouton(BOUTON_CREER_ADRESSE)) {
        getModele().creerAdresseDocument();
      }
      if (pSNBouton.isBouton(BOUTON_MODIFIER_ADRESSE)) {
        getModele().modifierAdresseDocument();
      }
      if (pSNBouton.isBouton(BOUTON_SUPPRIMER_ADRESSE)) {
        getModele().supprimerAdresseDocument();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tblListeAdresseMouseClicked(MouseEvent e) {
    try {
      getModele().modifierAdresseSelectionnee(tblListeAdresse.getIndiceSelection());

      if (e.getClickCount() == 2) {
        validerListeSelectionAdresse();
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfNomLivraisonKeyReleased(KeyEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierNom(tfNomLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfComplementNomLivraisonKeyReleased(KeyEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierComplementNom(tfComplementNomLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRueLivraisonKeyReleased(KeyEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierRue(tfRueLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLocalisationLivraisonKeyReleased(KeyEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierlocalisation(tfLocalisationLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snCodePostalCommuneLivraisonValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierCommune(snCodePostalCommuneLivraison.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbTitreListe = new SNLabelTitre();
    scpListeAdresse = new JScrollPane();
    tblListeAdresse = new NRiTable();
    pnlCoordonneesClientLivraison = new JPanel();
    lbNomLivraison = new SNLabelChamp();
    tfNomLivraison = new SNTexte();
    lbComplementNomLivraison = new JLabel();
    tfComplementNomLivraison = new SNTexte();
    lbLocalisationLivraison = new JLabel();
    tfRueLivraison = new SNTexte();
    lbRueLivraison = new JLabel();
    tfLocalisationLivraison = new SNTexte();
    lbCodePostalLivraison = new JLabel();
    snCodePostalCommuneLivraison = new SNCodePostalCommune();
    
    // ======== this ========
    setTitle("Liste des adresses de livraison");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(975, 590));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
        
        // ---- lbTitreListe ----
        lbTitreListe.setText("Adresses du chantier");
        lbTitreListe.setName("lbTitreListe");
        pnlContenu.add(lbTitreListe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeAdresse ========
        {
          scpListeAdresse.setPreferredSize(new Dimension(1050, 225));
          scpListeAdresse.setFont(new Font("sansserif", Font.PLAIN, 14));
          scpListeAdresse.setName("scpListeAdresse");
          
          // ---- tblListeAdresse ----
          tblListeAdresse.setShowVerticalLines(true);
          tblListeAdresse.setShowHorizontalLines(true);
          tblListeAdresse.setBackground(Color.white);
          tblListeAdresse.setRowHeight(20);
          tblListeAdresse.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeAdresse.setAutoCreateRowSorter(true);
          tblListeAdresse.setSelectionBackground(new Color(57, 105, 138));
          tblListeAdresse.setGridColor(new Color(204, 204, 204));
          tblListeAdresse.setName("tblListeAdresse");
          tblListeAdresse.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeAdresseMouseClicked(e);
            }
          });
          scpListeAdresse.setViewportView(tblListeAdresse);
        }
        pnlContenu.add(scpListeAdresse, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCoordonneesClientLivraison ========
        {
          pnlCoordonneesClientLivraison.setBorder(
              new CompoundBorder(new TitledBorder(null, "D\u00e9tail de l'adresse s\u00e9lectionn\u00e9e", TitledBorder.LEADING,
                  TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)), new EmptyBorder(10, 10, 10, 10)));
          pnlCoordonneesClientLivraison.setOpaque(false);
          pnlCoordonneesClientLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlCoordonneesClientLivraison.setMinimumSize(new Dimension(600, 230));
          pnlCoordonneesClientLivraison.setPreferredSize(new Dimension(600, 230));
          pnlCoordonneesClientLivraison.setMaximumSize(new Dimension(2000, 310));
          pnlCoordonneesClientLivraison.setName("pnlCoordonneesClientLivraison");
          pnlCoordonneesClientLivraison.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).columnWidths = new int[] { 135, 0, 0 };
          ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbNomLivraison ----
          lbNomLivraison.setText("Raison sociale");
          lbNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbNomLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbNomLivraison.setName("lbNomLivraison");
          pnlCoordonneesClientLivraison.add(lbNomLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNomLivraison ----
          tfNomLivraison.setBackground(Color.white);
          tfNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfNomLivraison.setEditable(false);
          tfNomLivraison.setMinimumSize(new Dimension(485, 30));
          tfNomLivraison.setPreferredSize(new Dimension(485, 30));
          tfNomLivraison.setName("tfNomLivraison");
          tfNomLivraison.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfNomLivraisonKeyReleased(e);
            }
          });
          pnlCoordonneesClientLivraison.add(tfNomLivraison, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 1, 1));
          
          // ---- lbComplementNomLivraison ----
          lbComplementNomLivraison.setText("Compl\u00e9ment");
          lbComplementNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbComplementNomLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbComplementNomLivraison.setName("lbComplementNomLivraison");
          pnlCoordonneesClientLivraison.add(lbComplementNomLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfComplementNomLivraison ----
          tfComplementNomLivraison.setBackground(Color.white);
          tfComplementNomLivraison.setMinimumSize(new Dimension(485, 30));
          tfComplementNomLivraison.setPreferredSize(new Dimension(485, 30));
          tfComplementNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfComplementNomLivraison.setName("tfComplementNomLivraison");
          tfComplementNomLivraison.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfComplementNomLivraisonKeyReleased(e);
            }
          });
          pnlCoordonneesClientLivraison.add(tfComplementNomLivraison, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbLocalisationLivraison ----
          lbLocalisationLivraison.setText("Adresse 1");
          lbLocalisationLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbLocalisationLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLocalisationLivraison.setName("lbLocalisationLivraison");
          pnlCoordonneesClientLivraison.add(lbLocalisationLivraison, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfRueLivraison ----
          tfRueLivraison.setBackground(Color.white);
          tfRueLivraison.setMinimumSize(new Dimension(485, 30));
          tfRueLivraison.setPreferredSize(new Dimension(485, 30));
          tfRueLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfRueLivraison.setName("tfRueLivraison");
          tfRueLivraison.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfRueLivraisonKeyReleased(e);
            }
          });
          pnlCoordonneesClientLivraison.add(tfRueLivraison, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbRueLivraison ----
          lbRueLivraison.setText("Adresse 2");
          lbRueLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbRueLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbRueLivraison.setName("lbRueLivraison");
          pnlCoordonneesClientLivraison.add(lbRueLivraison, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfLocalisationLivraison ----
          tfLocalisationLivraison.setBackground(Color.white);
          tfLocalisationLivraison.setMinimumSize(new Dimension(485, 30));
          tfLocalisationLivraison.setPreferredSize(new Dimension(485, 30));
          tfLocalisationLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfLocalisationLivraison.setName("tfLocalisationLivraison");
          tfLocalisationLivraison.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfLocalisationLivraisonKeyReleased(e);
            }
          });
          pnlCoordonneesClientLivraison.add(tfLocalisationLivraison, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbCodePostalLivraison ----
          lbCodePostalLivraison.setText("Commune");
          lbCodePostalLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbCodePostalLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCodePostalLivraison.setName("lbCodePostalLivraison");
          pnlCoordonneesClientLivraison.add(lbCodePostalLivraison, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snCodePostalCommuneLivraison ----
          snCodePostalCommuneLivraison.setName("snCodePostalCommuneLivraison");
          snCodePostalCommuneLivraison.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snCodePostalCommuneLivraisonValueChanged(e);
            }
          });
          pnlCoordonneesClientLivraison.add(snCodePostalCommuneLivraison, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlCoordonneesClientLivraison, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre lbTitreListe;
  private JScrollPane scpListeAdresse;
  private NRiTable tblListeAdresse;
  private JPanel pnlCoordonneesClientLivraison;
  private SNLabelChamp lbNomLivraison;
  private SNTexte tfNomLivraison;
  private JLabel lbComplementNomLivraison;
  private SNTexte tfComplementNomLivraison;
  private JLabel lbLocalisationLivraison;
  private SNTexte tfRueLivraison;
  private JLabel lbRueLivraison;
  private SNTexte tfLocalisationLivraison;
  private JLabel lbCodePostalLivraison;
  private SNCodePostalCommune snCodePostalCommuneLivraison;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
