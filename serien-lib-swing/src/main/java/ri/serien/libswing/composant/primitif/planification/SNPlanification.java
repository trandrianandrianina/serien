/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.planification;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.joursemaine.EnumJourSemaine;
import ri.serien.libcommun.commun.planification.EnumPlanificationPeriodicite;
import ri.serien.libcommun.commun.planification.EnumQuantieme;
import ri.serien.libcommun.commun.planification.IdPlanification;
import ri.serien.libcommun.commun.planification.Planification;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.moteur.composant.SNComposant;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Composant de saisie d'une planification pour le déclenchement d'une action.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une planification dans le logiciel.
 * Il calcule systématiquement la date prochaine date déclenchement lors de la modification d'une de ses valeurs.
 *
 * Ce composant pour fonctionner a besoin de :
 * - l'identifiant de l'établissement (obligatoire),
 * - un objet planification ou null (obligatoire).
 */
public class SNPlanification extends SNComposant {
  
  private boolean evenementActif = true;
  
  private IdEtablissement idEtablissement = null;
  private Planification planification = null;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public SNPlanification() {
    super();
    initComponents();

    // Initialisation des valeurs des composants
    
    // Liste des périodicités
    cbPeriodicite.removeAllItems();
    for (EnumPlanificationPeriodicite periodicite : EnumPlanificationPeriodicite.values()) {
      cbPeriodicite.addItem(periodicite);
    }
    
    // Liste des intervalles pour la périodicité journalière
    cbJournaliereIntervalle.removeAllItems();
    for (int i = 1; i <= 7; i++) {
      cbJournaliereIntervalle.addItem(i);
    }

    // Liste des intervalles pour la périodicité hebdomadaire
    cbHebdomadaireIntervalle.removeAllItems();
    for (int i = 1; i <= 5; i++) {
      cbHebdomadaireIntervalle.addItem(i);
    }

    // Liste des jours pour la périodicité hebdomadaire
    cbHebdomadaireJourEcheance.removeAllItems();
    for (EnumJourSemaine jourSemaine : EnumJourSemaine.values()) {
      cbHebdomadaireJourEcheance.addItem(jourSemaine);
    }
    
    // Liste des intervalles pour la périodicité menuselle
    cbMensuelleIntervalle.removeAllItems();
    for (int i = 1; i <= 12; i++) {
      cbMensuelleIntervalle.addItem(i);
    }
    
    // La liste des quantièmes pour la périodicité mensuelle
    cbMensuelleSelectionQuantieme.removeAllItems();
    for (EnumQuantieme jourSemaine : EnumQuantieme.values()) {
      cbMensuelleSelectionQuantieme.addItem(jourSemaine);
    }
    
    // Liste des jours pour la périodicité mensuelle
    cbMensuelleSelectionJour.removeAllItems();
    for (EnumJourSemaine jourSemaine : EnumJourSemaine.values()) {
      cbMensuelleSelectionJour.addItem(jourSemaine);
    }
    
    // La liste des quantièmes pour la périodicité annuelle
    cbAnnuelleQuantieme.removeAllItems();
    for (EnumQuantieme jourSemaine : EnumQuantieme.values()) {
      cbAnnuelleQuantieme.addItem(jourSemaine);
    }
    
    // Liste des heures de déclenchement possible
    cbHeure.removeAllItems();
    for (int i = 0; i <= 23; i++) {
      cbHeure.addItem(i);
    }

  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraîchir les composants.
   */
  public void rafraichir() {
    evenementActif = false;

    rafraichirChoixPeriodicite();
    rafraichirPanelPeriodicite();
    // Périodicité journalière
    rafraichirJournaliereIntervalle();
    // Périodicité hebdomadaire
    rafraichirHebdomadaireIntervalle();
    rafraichirHebdomadaireJourSemaine();
    // Périodicité mensuelle
    rafraichirMensuelleIntervalle();
    rafraichirMensuelleQuantieme();
    rafraichirMensuelleJourSemaine();
    // Périodicité annuelle
    rafraichirAnnuelleQuantieme();
    
    // Date et heure de déclenchement
    rafraichirDateDeclenchement();
    rafraichirHeureDeclenchement();

    evenementActif = true;
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes protégées
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Envoyer une notification pour signaler que la valeur du composant a changé.
   * Ceci est une redéfinition de la méthode originale afin de prendre en compte le calcul de la date de déclenchement.
   */
  @Override
  protected void fireValueChanged() {
    // La date de déclenchement est recalculée systématiquement à chaque évènement
    if (planification != null) {
      planification.calculerDateProchainDeclenchement();
      rafraichir();
    }
    
    // Appel de la méthode originale
    super.fireValueChanged();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraichir la liste des périodicités.
   */
  private void rafraichirChoixPeriodicite() {
    if (planification != null) {
      cbPeriodicite.setSelectedItem(planification.getPeriodicite());
    }
    else {
      cbPeriodicite.setSelectedItem(null);
    }
    cbPeriodicite.setEnabled(isEnabled());
  }

  /**
   * Rafraichir le panel lié à la périodicité sélectionnée.
   */
  private void rafraichirPanelPeriodicite() {
    if (planification == null || planification.getPeriodicite() == null) {
      pnlPeriode.setVisible(false);
    }
    else {
      pnlPeriode.setVisible(true);
      // Affichage du panel correspond au type de périodicité
      CardLayout cardLayout = (CardLayout) (pnlPeriode.getLayout());
      switch (planification.getPeriodicite()) {
        case AUCUNE:
          cardLayout.show(pnlPeriode, "Aucune");
          break;
        case JOURNALIERE:
          cardLayout.show(pnlPeriode, "Journaliere");
          break;
        case HEBDOMADAIRE:
          cardLayout.show(pnlPeriode, "Hebdomadaire");
          break;
        case MENSUELLE:
          cardLayout.show(pnlPeriode, "Mensuelle");
          break;
        case ANNUELLE:
          cardLayout.show(pnlPeriode, "Annuelle");
          break;
      }
    }
  }
  
  /**
   * Rafraichir l'intervalle en jour pour la périodicité journalière.
   */
  private void rafraichirJournaliereIntervalle() {
    if (planification == null || planification.getPeriodicite() != EnumPlanificationPeriodicite.JOURNALIERE
        || planification.getIntervalle() == null) {
      cbJournaliereIntervalle.setSelectedItem(null);
    }
    else {
      cbJournaliereIntervalle.setSelectedItem(planification.getIntervalle());
    }
    cbJournaliereIntervalle.setEnabled(isEnabled());
  }

  /**
   * Rafraichir l'intervalle en jour pour la périodicité hebdomadaire.
   */
  private void rafraichirHebdomadaireIntervalle() {
    if (planification == null || planification.getPeriodicite() != EnumPlanificationPeriodicite.HEBDOMADAIRE
        || planification.getIntervalle() == null) {
      cbHebdomadaireIntervalle.setSelectedItem(null);
    }
    else {
      cbHebdomadaireIntervalle.setSelectedItem(planification.getIntervalle());
    }
    cbHebdomadaireIntervalle.setEnabled(isEnabled());
  }
  
  /**
   * Rafraichir les jours de la semaine pour la périodicité hebdomadaire.
   */
  private void rafraichirHebdomadaireJourSemaine() {
    if (planification == null || planification.getPeriodicite() != EnumPlanificationPeriodicite.HEBDOMADAIRE
        || planification.getJourEcheance() == null) {
      cbHebdomadaireJourEcheance.setSelectedItem(null);
    }
    else {
      cbHebdomadaireJourEcheance.setSelectedItem(planification.getJourEcheance());
    }
    cbHebdomadaireJourEcheance.setEnabled(isEnabled());
  }

  /**
   * Rafraichir l'intervalle en jour pour la périodicité mensuelle.
   */
  private void rafraichirMensuelleIntervalle() {
    if (planification == null || planification.getPeriodicite() != EnumPlanificationPeriodicite.MENSUELLE
        || planification.getIntervalle() == null) {
      cbMensuelleIntervalle.setSelectedItem(null);
    }
    else {
      cbMensuelleIntervalle.setSelectedItem(planification.getIntervalle());
    }
    cbMensuelleIntervalle.setEnabled(isEnabled());
  }
  
  /**
   * Rafraichir le quantième pour la périodicité mensuelle.
   */
  private void rafraichirMensuelleQuantieme() {
    if (planification == null || planification.getPeriodicite() != EnumPlanificationPeriodicite.MENSUELLE
        || planification.getQuantiemeEcheance() == null) {
      cbMensuelleSelectionQuantieme.setSelectedItem(null);
    }
    else {
      cbMensuelleSelectionQuantieme.setSelectedItem(planification.getQuantiemeEcheance());
    }
    cbMensuelleSelectionQuantieme.setEnabled(isEnabled());
  }

  /**
   * Rafraichir les jours de la semaine pour la périodicité mensuelle.
   */
  private void rafraichirMensuelleJourSemaine() {
    if (planification == null || planification.getPeriodicite() != EnumPlanificationPeriodicite.MENSUELLE
        || planification.getJourEcheance() == null) {
      cbMensuelleSelectionJour.setSelectedItem(null);
    }
    else {
      cbMensuelleSelectionJour.setSelectedItem(planification.getJourEcheance());
    }
    cbMensuelleSelectionJour.setEnabled(isEnabled());
  }
  
  /**
   * Rafraichir le quantième pour la périodicité annuelle.
   */
  private void rafraichirAnnuelleQuantieme() {
    if (planification == null || planification.getPeriodicite() != EnumPlanificationPeriodicite.ANNUELLE
        || planification.getQuantiemeEcheance() == null) {
      cbAnnuelleQuantieme.setSelectedItem(null);
    }
    else {
      cbAnnuelleQuantieme.setSelectedItem(planification.getQuantiemeEcheance());
    }
    cbAnnuelleQuantieme.setEnabled(isEnabled());
  }
  
  /**
   * Rafraichir la date de déclenchement.
   */
  private void rafraichirDateDeclenchement() {
    if (planification == null || planification.getDateDeclenchement() == null) {
      snDateDeclenchement.setDate(null);
      lbTitreDateDeclenchement.setVisible(false);
      lbDateDeclenchement.setText("");
    }
    else {
      snDateDeclenchement.setDate(planification.getDateDeclenchement());
      lbTitreDateDeclenchement.setVisible(true);
      lbDateDeclenchement.setText("le " + planification.getDateHeureDeclenchementFormate());
    }
    snDateDeclenchement.setEnabled(isEnabled());
  }
  
  /**
   * Rafraichir l'heure de déclenchement.
   */
  private void rafraichirHeureDeclenchement() {
    if (planification == null || planification.getHeureDeclenchement() == null) {
      cbHeure.setSelectedItem(null);
    }
    else {
      cbHeure.setSelectedItem(planification.getHeureDeclenchement());
    }
    cbHeure.setEnabled(isEnabled());
  }

  /**
   * Modifier la périodicité du planning.
   * @param pPeriodicite La périodicité.
   */
  private void modifierPeriodiciteSelectionne(EnumPlanificationPeriodicite pPeriodicite) {
    if (planification == null || Constantes.equals(pPeriodicite, planification.getPeriodicite())) {
      return;
    }
    planification.setPeriodicite(pPeriodicite);
    planification.effacerVariable();
    
    // Rafraichir le composant
    rafraichir();
    fireValueChanged();
  }

  /**
   * Modifier la date de déclenchement lorsqu'il n'y a aucune périodicité.
   * @param pDate La date.
   */
  private void modifierDateDeclenchement(Date pDate) {
    // Contrôler la validité de la date
    pDate = Planification.controlerJourDeclenchement(pDate);
    if (planification == null || planification.getPeriodicite() != EnumPlanificationPeriodicite.AUCUNE
        || Constantes.equals(pDate, planification.getDateDeclenchement())) {
      return;
    }
    planification.setDateDeclenchement(pDate);
    
    // Rafraichir le composant
    rafraichir();
    fireValueChanged();
  }

  /**
   * Modifier l'intervalle de temps.
   * @param pIntervalle L'intervalle.
   */
  private void modifierIntervalleTemps(EnumPlanificationPeriodicite pPlanificationPeriodicite, Integer pIntervalle) {
    if (pPlanificationPeriodicite == null) {
      return;
    }
    // Contrôler la valeur en fonction de la périodicité
    switch (pPlanificationPeriodicite) {
      case AUCUNE:
        // Rien à contrôler
        break;
      case JOURNALIERE:
        pIntervalle = Planification.controlerIntervalleEnJour(pIntervalle);
        break;
      case HEBDOMADAIRE:
        pIntervalle = Planification.controlerIntervalleEnSemaine(pIntervalle);
        break;
      case MENSUELLE:
        pIntervalle = Planification.controlerIntervalleEnMois(pIntervalle);
        break;
      case ANNUELLE:
        break;
    }
    if (planification == null || Constantes.equals(pIntervalle, planification.getIntervalle())) {
      return;
    }
    if (planification.getPeriodicite() != pPlanificationPeriodicite) {
      throw new MessageErreurException("La périodicité de la planification n'est pas en adéquation avec celle de l'intervalle de temps.");
    }
    planification.setIntervalle(pIntervalle);
    
    // Rafraichir le composant
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Modifier le jour échéance.
   * @param pJourEchance Le jour de la semaine.
   */
  private void modifierJourEcheance(EnumJourSemaine pJourEchance) {
    if (planification == null || Constantes.equals(pJourEchance, planification.getJourEcheance())) {
      return;
    }
    planification.setJourEcheance(pJourEchance);
    
    // Rafraichir le composant
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Modifier le quantième échéance.
   * @param pQuantiemeEchance Le quantième.
   */
  private void modifierQuantiemeEcheance(EnumQuantieme pQuantiemeEchance) {
    if (planification == null || Constantes.equals(pQuantiemeEchance, planification.getQuantiemeEcheance())) {
      return;
    }
    planification.setQuantiemeEcheance(pQuantiemeEchance);
    
    // Rafraichir le composant
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Modifier l'heure de déclenchement.
   * @param pHeure L'heure.
   */
  private void modifierHeureDeclenchement(Integer pHeure) {
    // Contrôler la valeur
    pHeure = Planification.controlerHeure(pHeure);
    if (planification == null || Constantes.equals(pHeure, planification.getHeureDeclenchement())) {
      return;
    }
    planification.setHeureDeclenchement(pHeure);
    
    // Rafraichir le composant
    rafraichir();
    fireValueChanged();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Modifier l'identifiant établissement.
   * @param pIdEtablissement L'identifiant établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }

  /**
   * Retourner la planification.
   * @return La planification.
   */
  public Planification getPlanification() {
    return planification;
  }
  
  /**
   * Modifier la planification.
   * Si sa valeur est null alors une instance est créée car la variable ne peux pas être null.
   * @param pPlanification La planification.
   */
  public void setPlanification(Planification pPlanification) {
    planification = pPlanification;
    
    // Contrôle de la planification qui ne peut pas être à null
    if (planification == null) {
      IdPlanification idPlanification = IdPlanification.getInstanceAvecCreationId(idEtablissement);
      planification = new Planification(idPlanification);
    }
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Sélectionner une périodicité.
   * @param pItemEvent L'événement.
   */
  private void cbPeriodiciteItemStateChanged(ItemEvent pItemEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierPeriodiciteSelectionne((EnumPlanificationPeriodicite) cbPeriodicite.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Sélectionner la date de déclenchement dans le cas où il n'y a pas de périodicité.
   * @param pSNComposantEvent L'événement.
   */
  private void snDateDeclenchementValueChanged(SNComposantEvent pSNComposantEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierDateDeclenchement(snDateDeclenchement.getDate());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Sélectionner l'intervalle de temps en jours.
   * @param pItemEvent L'événement.
   */
  private void cbJournaliereIntervalleItemStateChanged(ItemEvent pItemEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierIntervalleTemps(EnumPlanificationPeriodicite.JOURNALIERE, (Integer) cbJournaliereIntervalle.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Sélectionner l'intervalle de temps en semaines.
   * @param pItemEvent L'événement.
   */
  private void cbHebdomadaireIntervalleItemStateChanged(ItemEvent pItemEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierIntervalleTemps(EnumPlanificationPeriodicite.HEBDOMADAIRE, (Integer) cbHebdomadaireIntervalle.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Sélectionner le jour échéance de déclenchement pour la périodicité hebdomadaire.
   * @param pItemEvent L'événement.
   */
  private void cbHebdomadaireJourEcheanceItemStateChanged(ItemEvent pItemEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierJourEcheance((EnumJourSemaine) cbHebdomadaireJourEcheance.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Sélectionner l'intervalle de temps en mois.
   * @param pItemEvent L'événement.
   */
  private void cbMensuelleIntervalleItemStateChanged(ItemEvent pItemEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierIntervalleTemps(EnumPlanificationPeriodicite.MENSUELLE, (Integer) cbMensuelleIntervalle.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Sélectionner le quantième pour la périodicité mensuelle.
   * @param pItemEvent L'événement.
   */
  private void cbMensuelleSelectionQuantiemeItemStateChanged(ItemEvent pItemEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierQuantiemeEcheance((EnumQuantieme) cbMensuelleSelectionQuantieme.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  /**
   * Sélectionner le jour pour la périodicité mensuelle.
   * @param pItemEvent L'événement.
   */
  private void cbMensuelleSelectionJourItemStateChanged(ItemEvent pItemEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierJourEcheance((EnumJourSemaine) cbMensuelleSelectionJour.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Sélectionner le quantième pour la périodicité annuelle.
   * @param pItemEvent L'événement.
   */
  private void cbAnnuelleQuantiemeItemStateChanged(ItemEvent e) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierQuantiemeEcheance((EnumQuantieme) cbAnnuelleQuantieme.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Sélectionner l'heure de déclenchement.
   * @param pItemEvent L'événement.
   */
  private void cbHeureItemStateChanged(ItemEvent pItemEvent) {
    try {
      if (!evenementActif) {
        return;
      }
      modifierHeureDeclenchement((Integer) cbHeure.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes auto-génrées par JFormdesigner
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPlanification = new SNPanel();
    lbPeriodicite = new SNLabelChamp();
    cbPeriodicite = new SNComboBox();
    pnlPeriode = new SNPanel();
    pnlPeriodeAucune = new SNPanel();
    pnlAucuneSelectionDate = new SNPanel();
    lbAucuneLe = new SNLabelChamp();
    snDateDeclenchement = new SNDate();
    pnlPeriodeJournaliere = new SNPanel();
    lbJournaliereTous = new SNLabelChamp();
    cbJournaliereIntervalle = new SNComboBox();
    lbJournaliereJour = new SNLabelChamp();
    pnlPeriodeHebdomadaire = new SNPanel();
    pnlHebdomadaireSelectionSemaine = new SNPanel();
    lbHebdomadaireTous = new SNLabelChamp();
    cbHebdomadaireIntervalle = new SNComboBox();
    lbHebdomadaireSemaine = new SNLabelChamp();
    pnlHebdomadaireSelectionJour = new SNPanel();
    lbHebdomadaireLe = new SNLabelChamp();
    cbHebdomadaireJourEcheance = new SNComboBox();
    pnlPeriodeMensuelle = new SNPanel();
    pnlSelectionMois = new SNPanel();
    lbMensuelleTous = new SNLabelChamp();
    cbMensuelleIntervalle = new SNComboBox();
    lbMensuelleMois = new SNLabelChamp();
    pnlSelectionJourSemaine = new SNPanel();
    lbMensuelleLe = new SNLabelChamp();
    cbMensuelleSelectionQuantieme = new SNComboBox();
    cbMensuelleSelectionJour = new SNComboBox();
    pnlPeriodeAnnuelle = new SNPanel();
    lbAnnuelleLe = new SNLabelChamp();
    cbAnnuelleQuantieme = new SNComboBox();
    lbAnnuelleJour = new SNLabelChamp();
    pnlHeure = new SNPanel();
    lbHeureA = new SNLabelChamp();
    cbHeure = new SNComboBox();
    lbHeureHeure = new SNLabelChamp();
    lbTitreDateDeclenchement = new SNLabelChamp();
    lbDateDeclenchement = new SNLabelChamp();
    
    // ======== this ========
    setPreferredSize(new Dimension(500, 220));
    setMinimumSize(new Dimension(500, 180));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPlanification ========
    {
      pnlPlanification.setBackground(new Color(239, 239, 222));
      pnlPlanification.setMinimumSize(new Dimension(500, 150));
      pnlPlanification.setPreferredSize(new Dimension(500, 180));
      pnlPlanification.setRequestFocusEnabled(false);
      pnlPlanification.setMaximumSize(new Dimension(500, 230));
      pnlPlanification.setName("pnlPlanification");
      pnlPlanification.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPlanification.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlPlanification.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlPlanification.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlPlanification.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ---- lbPeriodicite ----
      lbPeriodicite.setText("P\u00e9riodicit\u00e9");
      lbPeriodicite.setName("lbPeriodicite");
      pnlPlanification.add(lbPeriodicite,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- cbPeriodicite ----
      cbPeriodicite.setModel(new DefaultComboBoxModel<>(new String[] { "Journali\u00e8re", "Hebdomadaire", "Mensuelle", "Annuelle" }));
      cbPeriodicite.setPreferredSize(new Dimension(200, 30));
      cbPeriodicite.setMinimumSize(new Dimension(150, 30));
      cbPeriodicite.setMaximumSize(new Dimension(200, 30));
      cbPeriodicite.setName("cbPeriodicite");
      cbPeriodicite.addItemListener(e -> cbPeriodiciteItemStateChanged(e));
      pnlPlanification.add(cbPeriodicite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlPeriode ========
      {
        pnlPeriode.setName("pnlPeriode");
        pnlPeriode.setLayout(new CardLayout());
        
        // ======== pnlPeriodeAucune ========
        {
          pnlPeriodeAucune.setPreferredSize(new Dimension(265, 30));
          pnlPeriodeAucune.setName("pnlPeriodeAucune");
          pnlPeriodeAucune.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPeriodeAucune.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlPeriodeAucune.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPeriodeAucune.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPeriodeAucune.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlAucuneSelectionDate ========
          {
            pnlAucuneSelectionDate.setName("pnlAucuneSelectionDate");
            pnlAucuneSelectionDate.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlAucuneSelectionDate.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlAucuneSelectionDate.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlAucuneSelectionDate.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlAucuneSelectionDate.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ---- lbAucuneLe ----
            lbAucuneLe.setText("Le");
            lbAucuneLe.setHorizontalAlignment(SwingConstants.LEADING);
            lbAucuneLe.setMaximumSize(new Dimension(55, 30));
            lbAucuneLe.setMinimumSize(new Dimension(55, 30));
            lbAucuneLe.setPreferredSize(new Dimension(20, 30));
            lbAucuneLe.setName("lbAucuneLe");
            pnlAucuneSelectionDate.add(lbAucuneLe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snDateDeclenchement ----
            snDateDeclenchement.setName("snDateDeclenchement");
            snDateDeclenchement.addSNComposantListener(e -> snDateDeclenchementValueChanged(e));
            pnlAucuneSelectionDate.add(snDateDeclenchement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPeriodeAucune.add(pnlAucuneSelectionDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPeriode.add(pnlPeriodeAucune, "Aucune");
        
        // ======== pnlPeriodeJournaliere ========
        {
          pnlPeriodeJournaliere.setName("pnlPeriodeJournaliere");
          pnlPeriodeJournaliere.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPeriodeJournaliere.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlPeriodeJournaliere.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPeriodeJournaliere.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPeriodeJournaliere.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbJournaliereTous ----
          lbJournaliereTous.setText("Tous les");
          lbJournaliereTous.setHorizontalAlignment(SwingConstants.LEADING);
          lbJournaliereTous.setMaximumSize(new Dimension(55, 30));
          lbJournaliereTous.setMinimumSize(new Dimension(55, 30));
          lbJournaliereTous.setPreferredSize(new Dimension(55, 30));
          lbJournaliereTous.setName("lbJournaliereTous");
          pnlPeriodeJournaliere.add(lbJournaliereTous, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- cbJournaliereIntervalle ----
          cbJournaliereIntervalle.setFont(new Font("sansserif", Font.PLAIN, 14));
          cbJournaliereIntervalle.setPreferredSize(new Dimension(50, 30));
          cbJournaliereIntervalle.setMinimumSize(new Dimension(50, 30));
          cbJournaliereIntervalle.setName("cbJournaliereIntervalle");
          cbJournaliereIntervalle.addItemListener(e -> cbJournaliereIntervalleItemStateChanged(e));
          pnlPeriodeJournaliere.add(cbJournaliereIntervalle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbJournaliereJour ----
          lbJournaliereJour.setText("jours");
          lbJournaliereJour.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbJournaliereJour.setHorizontalAlignment(SwingConstants.LEADING);
          lbJournaliereJour.setName("lbJournaliereJour");
          pnlPeriodeJournaliere.add(lbJournaliereJour, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPeriode.add(pnlPeriodeJournaliere, "Journaliere");
        
        // ======== pnlPeriodeHebdomadaire ========
        {
          pnlPeriodeHebdomadaire.setName("pnlPeriodeHebdomadaire");
          pnlPeriodeHebdomadaire.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPeriodeHebdomadaire.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlPeriodeHebdomadaire.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPeriodeHebdomadaire.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlPeriodeHebdomadaire.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlHebdomadaireSelectionSemaine ========
          {
            pnlHebdomadaireSelectionSemaine.setName("pnlHebdomadaireSelectionSemaine");
            pnlHebdomadaireSelectionSemaine.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlHebdomadaireSelectionSemaine.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlHebdomadaireSelectionSemaine.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlHebdomadaireSelectionSemaine.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlHebdomadaireSelectionSemaine.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ---- lbHebdomadaireTous ----
            lbHebdomadaireTous.setText("Toutes les");
            lbHebdomadaireTous.setHorizontalAlignment(SwingConstants.LEADING);
            lbHebdomadaireTous.setMaximumSize(new Dimension(55, 30));
            lbHebdomadaireTous.setMinimumSize(new Dimension(55, 30));
            lbHebdomadaireTous.setPreferredSize(new Dimension(70, 30));
            lbHebdomadaireTous.setName("lbHebdomadaireTous");
            pnlHebdomadaireSelectionSemaine.add(lbHebdomadaireTous, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbHebdomadaireIntervalle ----
            cbHebdomadaireIntervalle.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbHebdomadaireIntervalle.setPreferredSize(new Dimension(50, 30));
            cbHebdomadaireIntervalle.setMinimumSize(new Dimension(50, 30));
            cbHebdomadaireIntervalle.setName("cbHebdomadaireIntervalle");
            cbHebdomadaireIntervalle.addItemListener(e -> cbHebdomadaireIntervalleItemStateChanged(e));
            pnlHebdomadaireSelectionSemaine.add(cbHebdomadaireIntervalle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbHebdomadaireSemaine ----
            lbHebdomadaireSemaine.setText("semaines");
            lbHebdomadaireSemaine.setHorizontalTextPosition(SwingConstants.RIGHT);
            lbHebdomadaireSemaine.setHorizontalAlignment(SwingConstants.LEADING);
            lbHebdomadaireSemaine.setName("lbHebdomadaireSemaine");
            pnlHebdomadaireSelectionSemaine.add(lbHebdomadaireSemaine, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPeriodeHebdomadaire.add(pnlHebdomadaireSelectionSemaine, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlHebdomadaireSelectionJour ========
          {
            pnlHebdomadaireSelectionJour.setName("pnlHebdomadaireSelectionJour");
            pnlHebdomadaireSelectionJour.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlHebdomadaireSelectionJour.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlHebdomadaireSelectionJour.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlHebdomadaireSelectionJour.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlHebdomadaireSelectionJour.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbHebdomadaireLe ----
            lbHebdomadaireLe.setText("Le");
            lbHebdomadaireLe.setHorizontalAlignment(SwingConstants.LEADING);
            lbHebdomadaireLe.setPreferredSize(new Dimension(17, 30));
            lbHebdomadaireLe.setName("lbHebdomadaireLe");
            pnlHebdomadaireSelectionJour.add(lbHebdomadaireLe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbHebdomadaireJourEcheance ----
            cbHebdomadaireJourEcheance.setModel(new DefaultComboBoxModel<>(new String[] { "lundi", "..." }));
            cbHebdomadaireJourEcheance.setName("cbHebdomadaireJourEcheance");
            cbHebdomadaireJourEcheance.addItemListener(e -> cbHebdomadaireJourEcheanceItemStateChanged(e));
            pnlHebdomadaireSelectionJour.add(cbHebdomadaireJourEcheance, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPeriodeHebdomadaire.add(pnlHebdomadaireSelectionJour, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPeriode.add(pnlPeriodeHebdomadaire, "Hebdomadaire");
        
        // ======== pnlPeriodeMensuelle ========
        {
          pnlPeriodeMensuelle.setName("pnlPeriodeMensuelle");
          pnlPeriodeMensuelle.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPeriodeMensuelle.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlPeriodeMensuelle.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPeriodeMensuelle.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlPeriodeMensuelle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlSelectionMois ========
          {
            pnlSelectionMois.setName("pnlSelectionMois");
            pnlSelectionMois.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlSelectionMois.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlSelectionMois.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlSelectionMois.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlSelectionMois.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ---- lbMensuelleTous ----
            lbMensuelleTous.setText("Tous les");
            lbMensuelleTous.setHorizontalAlignment(SwingConstants.LEADING);
            lbMensuelleTous.setMaximumSize(new Dimension(55, 30));
            lbMensuelleTous.setMinimumSize(new Dimension(55, 30));
            lbMensuelleTous.setPreferredSize(new Dimension(55, 30));
            lbMensuelleTous.setName("lbMensuelleTous");
            pnlSelectionMois.add(lbMensuelleTous, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbMensuelleIntervalle ----
            cbMensuelleIntervalle.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbMensuelleIntervalle.setPreferredSize(new Dimension(55, 30));
            cbMensuelleIntervalle.setName("cbMensuelleIntervalle");
            cbMensuelleIntervalle.addItemListener(e -> cbMensuelleIntervalleItemStateChanged(e));
            pnlSelectionMois.add(cbMensuelleIntervalle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbMensuelleMois ----
            lbMensuelleMois.setText("mois");
            lbMensuelleMois.setHorizontalTextPosition(SwingConstants.RIGHT);
            lbMensuelleMois.setHorizontalAlignment(SwingConstants.LEADING);
            lbMensuelleMois.setName("lbMensuelleMois");
            pnlSelectionMois.add(lbMensuelleMois, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPeriodeMensuelle.add(pnlSelectionMois, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlSelectionJourSemaine ========
          {
            pnlSelectionJourSemaine.setName("pnlSelectionJourSemaine");
            pnlSelectionJourSemaine.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlSelectionJourSemaine.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlSelectionJourSemaine.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlSelectionJourSemaine.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlSelectionJourSemaine.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbMensuelleLe ----
            lbMensuelleLe.setText("Le");
            lbMensuelleLe.setHorizontalAlignment(SwingConstants.LEADING);
            lbMensuelleLe.setPreferredSize(new Dimension(17, 30));
            lbMensuelleLe.setName("lbMensuelleLe");
            pnlSelectionJourSemaine.add(lbMensuelleLe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbMensuelleSelectionQuantieme ----
            cbMensuelleSelectionQuantieme.setModel(new DefaultComboBoxModel<>(new String[] { "premier", "dernier", "..." }));
            cbMensuelleSelectionQuantieme.setName("cbMensuelleSelectionQuantieme");
            cbMensuelleSelectionQuantieme.addItemListener(e -> cbMensuelleSelectionQuantiemeItemStateChanged(e));
            pnlSelectionJourSemaine.add(cbMensuelleSelectionQuantieme, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbMensuelleSelectionJour ----
            cbMensuelleSelectionJour.setModel(new DefaultComboBoxModel<>(new String[] { "lundi", "..." }));
            cbMensuelleSelectionJour.setName("cbMensuelleSelectionJour");
            cbMensuelleSelectionJour.addItemListener(e -> cbMensuelleSelectionJourItemStateChanged(e));
            pnlSelectionJourSemaine.add(cbMensuelleSelectionJour, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPeriodeMensuelle.add(pnlSelectionJourSemaine, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPeriode.add(pnlPeriodeMensuelle, "Mensuelle");
        
        // ======== pnlPeriodeAnnuelle ========
        {
          pnlPeriodeAnnuelle.setName("pnlPeriodeAnnuelle");
          pnlPeriodeAnnuelle.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPeriodeAnnuelle.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlPeriodeAnnuelle.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPeriodeAnnuelle.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPeriodeAnnuelle.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbAnnuelleLe ----
          lbAnnuelleLe.setText("Le");
          lbAnnuelleLe.setHorizontalAlignment(SwingConstants.LEADING);
          lbAnnuelleLe.setPreferredSize(new Dimension(17, 30));
          lbAnnuelleLe.setName("lbAnnuelleLe");
          pnlPeriodeAnnuelle.add(lbAnnuelleLe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- cbAnnuelleQuantieme ----
          cbAnnuelleQuantieme.setModel(new DefaultComboBoxModel<>(new String[] { "premier", "dernier" }));
          cbAnnuelleQuantieme.setName("cbAnnuelleQuantieme");
          cbAnnuelleQuantieme.addItemListener(e -> cbAnnuelleQuantiemeItemStateChanged(e));
          pnlPeriodeAnnuelle.add(cbAnnuelleQuantieme, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbAnnuelleJour ----
          lbAnnuelleJour.setText("jour");
          lbAnnuelleJour.setHorizontalAlignment(SwingConstants.LEADING);
          lbAnnuelleJour.setName("lbAnnuelleJour");
          pnlPeriodeAnnuelle.add(lbAnnuelleJour, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPeriode.add(pnlPeriodeAnnuelle, "Annuelle");
      }
      pnlPlanification.add(pnlPeriode,
          new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlHeure ========
      {
        pnlHeure.setName("pnlHeure");
        pnlHeure.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlHeure.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlHeure.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlHeure.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlHeure.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ---- lbHeureA ----
        lbHeureA.setText("\u00c0");
        lbHeureA.setHorizontalAlignment(SwingConstants.LEADING);
        lbHeureA.setPreferredSize(new Dimension(10, 30));
        lbHeureA.setName("lbHeureA");
        pnlHeure.add(lbHeureA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- cbHeure ----
        cbHeure.setFont(new Font("sansserif", Font.PLAIN, 14));
        cbHeure.setPreferredSize(new Dimension(55, 30));
        cbHeure.setName("cbHeure");
        cbHeure.addItemListener(e -> cbHeureItemStateChanged(e));
        pnlHeure.add(cbHeure, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbHeureHeure ----
        lbHeureHeure.setText("heure");
        lbHeureHeure.setHorizontalAlignment(SwingConstants.LEADING);
        lbHeureHeure.setName("lbHeureHeure");
        pnlHeure.add(lbHeureHeure, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPlanification.add(pnlHeure,
          new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbTitreDateDeclenchement ----
      lbTitreDateDeclenchement.setText("Date d\u00e9clenchement");
      lbTitreDateDeclenchement.setName("lbTitreDateDeclenchement");
      pnlPlanification.add(lbTitreDateDeclenchement,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- lbDateDeclenchement ----
      lbDateDeclenchement.setText("XX/XX/XXXX");
      lbDateDeclenchement.setHorizontalAlignment(SwingConstants.LEADING);
      lbDateDeclenchement.setName("lbDateDeclenchement");
      pnlPlanification.add(lbDateDeclenchement,
          new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPlanification, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlPlanification;
  private SNLabelChamp lbPeriodicite;
  private SNComboBox cbPeriodicite;
  private SNPanel pnlPeriode;
  private SNPanel pnlPeriodeAucune;
  private SNPanel pnlAucuneSelectionDate;
  private SNLabelChamp lbAucuneLe;
  private SNDate snDateDeclenchement;
  private SNPanel pnlPeriodeJournaliere;
  private SNLabelChamp lbJournaliereTous;
  private SNComboBox cbJournaliereIntervalle;
  private SNLabelChamp lbJournaliereJour;
  private SNPanel pnlPeriodeHebdomadaire;
  private SNPanel pnlHebdomadaireSelectionSemaine;
  private SNLabelChamp lbHebdomadaireTous;
  private SNComboBox cbHebdomadaireIntervalle;
  private SNLabelChamp lbHebdomadaireSemaine;
  private SNPanel pnlHebdomadaireSelectionJour;
  private SNLabelChamp lbHebdomadaireLe;
  private SNComboBox cbHebdomadaireJourEcheance;
  private SNPanel pnlPeriodeMensuelle;
  private SNPanel pnlSelectionMois;
  private SNLabelChamp lbMensuelleTous;
  private SNComboBox cbMensuelleIntervalle;
  private SNLabelChamp lbMensuelleMois;
  private SNPanel pnlSelectionJourSemaine;
  private SNLabelChamp lbMensuelleLe;
  private SNComboBox cbMensuelleSelectionQuantieme;
  private SNComboBox cbMensuelleSelectionJour;
  private SNPanel pnlPeriodeAnnuelle;
  private SNLabelChamp lbAnnuelleLe;
  private SNComboBox cbAnnuelleQuantieme;
  private SNLabelChamp lbAnnuelleJour;
  private SNPanel pnlHeure;
  private SNLabelChamp lbHeureA;
  private SNComboBox cbHeure;
  private SNLabelChamp lbHeureHeure;
  private SNLabelChamp lbTitreDateDeclenchement;
  private SNLabelChamp lbDateDeclenchement;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
