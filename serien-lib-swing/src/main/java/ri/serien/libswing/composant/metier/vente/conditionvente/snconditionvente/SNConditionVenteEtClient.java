/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.conditionvente.snconditionvente;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.IdGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.ListeGroupeConditionVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une CNV ou d'une condition client.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une condition quand on peut sélectionner soit une CNV soit une condition
 * client.
 * Il gère l'affichage du code de la condition sélectionnée. Il permet aussi, via le bouton de sélection, de choisir le type de condition
 * et de sélectionner la condition voulue.
 * Il comprend comprenant un TextField non saisissable et un bouton permettant de sélectionner la condition dans une boîte de dialogue.
 *
 * Utilisation :
 * - Ajouter un composant SNConditionVenteEtClient à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé. Les informations nécessaires sont la session et l'établissement.
 *
 * Voir un exemple d'implémentation dans le VGVM03FM format B1.
 */
public class SNConditionVenteEtClient extends SNComposantAvecEtablissement {
  private ModeleConditionVenteEtClient modele = null;
  private SNTexte tfTexteAffichage;
  private SNBoutonDetail btnDialogueSaisieCondition;
  private String libelleCondition = "";
  private IdGroupeConditionVente idConditionSelectionnee = null;
  private IdClient idClientSelectionne = null;
  
  /**
   * Constructeur par défaut.
   */
  public SNConditionVenteEtClient() {
    super();
    setName("SNConditionVenteEtClient");

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);

    setLayout(new GridBagLayout());

    // Texte d'affichage
    tfTexteAffichage = new SNTexte();
    tfTexteAffichage.setName("tfTexteAffichage");
    add(tfTexteAffichage,
        new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
    tfTexteAffichage.setEnabled(false);

    // Bouton saisie
    btnDialogueSaisieCondition = new SNBoutonDetail();
    btnDialogueSaisieCondition.setToolTipText("Modifier la condition");
    btnDialogueSaisieCondition.setName("btnDialogueSaisieCondition");
    add(btnDialogueSaisieCondition, new GridBagConstraints());

    // Être notifié lorsque le bouton est cliqué
    btnDialogueSaisieCondition.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btdDialogueSaisieConditionActionPerformed(e);
      }
    });

  }
  
  /**
   * Activer ou désactiver le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    btnDialogueSaisieCondition.setEnabled(pEnabled);
  }
  
  /**
   * Positionner le focus sur le composant de saisie.
   */
  @Override
  public boolean requestFocusInWindow() {
    return btnDialogueSaisieCondition.requestFocusInWindow();
  }

  /**
   * Sélectionner une condition à partir des champs RPG.
   */
  public void setSelectionParChampRPG(Lexical pLexical, String pChampCondition) {
    String valeurRPG = pLexical.HostFieldGetData(pChampCondition);
    if (pChampCondition == null || valeurRPG == null) {
      throw new MessageErreurException("Impossible de sélectionner la condition car son code est invalide.");
    }
    initialiserCondition(valeurRPG);
  }
  
  /**
   * Renseigner les champs RPG correspondant à la condition sélectionnée.
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampCondition) {
    if (pChampCondition == null) {
      throw new MessageErreurException("Impossible d'écrire la condition car son code est invalide.");
    }
    if (idClientSelectionne != null) {
      pLexical.HostFieldPutData(pChampCondition, 0, idClientSelectionne.getIndicatif(IdClient.INDICATIF_NUM));
    }
    else if (idConditionSelectionnee != null) {
      pLexical.HostFieldPutData(pChampCondition, 0, idConditionSelectionnee.getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampCondition, 0, "");
    }
  }

  // -- Méthodes privées
  /**
   * Charger la condition suivant son libellé
   */
  private void initialiserCondition(String pLibelle) {
    if (pLibelle == null) {
      return;
    }

    if (pLibelle.trim().length() == IdClient.LONGUEUR_NUMERO) {
      pLibelle = pLibelle.replaceAll("[()]", "");
      idClientSelectionne = IdClient.getInstance(getIdEtablissement(), Integer.parseInt(pLibelle), 0);
      idConditionSelectionnee = null;
      chargerClient();
    }
    else if (pLibelle.trim().length() > 0) {
      idConditionSelectionnee = IdGroupeConditionVente.getInstance(getIdEtablissement(), pLibelle.trim());
      idClientSelectionne = null;
      chargerCNV();
    }
    else {
      idConditionSelectionnee = null;
      idClientSelectionne = null;
      libelleCondition = "";
    }
    tfTexteAffichage.setText(libelleCondition);
  }

  /**
   * Charger le libellé CNV
   */
  private void chargerCNV() {
    ListeGroupeConditionVente listeCnv = new ListeGroupeConditionVente();
    listeCnv = listeCnv.charger(getIdSession(), getIdEtablissement());
    if (listeCnv != null && listeCnv.size() > 0) {
      if (idConditionSelectionnee == null) {
        libelleCondition = "";
      }
      else if (listeCnv.get(idConditionSelectionnee) == null) {
        libelleCondition = idConditionSelectionnee.getTexte();
      }
      else {
        libelleCondition = listeCnv.get(idConditionSelectionnee).getTexte();
      }
    }
  }

  /**
   * Charger le libellé client
   */
  private void chargerClient() {
    ClientBase client = ManagerServiceClient.chargerClient(getIdSession(), idClientSelectionne);
    if (client != null) {
      libelleCondition = client.getTexte();
    }
    else {
      libelleCondition = "";
    }
  }
  
  /**
   * Affiche la fenêtre de recherche des conditions
   */
  private void afficherDialogueSaisieCondition() {
    // Modèle
    modele = new ModeleConditionVenteEtClient(getSession(), getIdEtablissement());
    modele.modifierIdClientSelectionne(idClientSelectionne);
    modele.modifierIdConditionSelectionnee(idConditionSelectionnee);
    VueConditionVenteEtClient vue = new VueConditionVenteEtClient(modele);
    vue.afficher();
    
    // Sélectionner la condition retournée par la boîte de dialogue (ou aucun si null)
    if (modele.isSortieAvecValidation()) {
      if (modele.getIdClientSelectionne() != null) {
        setSelection(modele.getIdClientSelectionne());
        idConditionSelectionnee = null;
      }
      else if (modele.getIdConditionSelectionnee() != null) {
        setSelection(modele.getIdConditionSelectionnee());
        idClientSelectionne = null;
      }
      else if (modele.getIdClientSelectionne() == null && modele.getIdConditionSelectionnee() == null) {
        idConditionSelectionnee = null;
        idClientSelectionne = null;
        tfTexteAffichage.setText("");
        fireValueChanged();
      }
    }
  }
  
  // -- Méthodes évènementielles
  
  /**
   * Clic sur le bouton pour sélection d'une condition
   */
  private void btdDialogueSaisieConditionActionPerformed(ActionEvent e) {
    try {
      afficherDialogueSaisieCondition();
    }
    catch (
    
    Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Modifier la condition sélectionnée.
   */
  public void setSelection(IdGroupeConditionVente pIdConditionVente) {
    idConditionSelectionnee = pIdConditionVente;
    chargerCNV();
    tfTexteAffichage.setText(libelleCondition);
    fireValueChanged();
  }
  
  /**
   * Modifier la condition sélectionnée.
   */
  public void setSelection(IdClient pIdClient) {
    idClientSelectionne = pIdClient;
    chargerClient();
    tfTexteAffichage.setText(libelleCondition);
    fireValueChanged();
  }

}
