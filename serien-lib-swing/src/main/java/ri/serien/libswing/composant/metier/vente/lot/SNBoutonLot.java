/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.lot;

import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;

/**
 * Bouton affichant une image en forme de signe plus permettant d'afficher la boite de dialogue de saisie des lots.
 *
 * La taille standard est de 16 x 16 pixels car cette icône doit rester assez discrète. Elle est souvent utilisée dans un coin de panneau.
 */
public class SNBoutonLot extends SNBoutonDetail {
  private static final String INFOBULLE = "Saisir les lots";

  /**
   * Constructeur.
   */
  public SNBoutonLot() {
    setToolTipText(INFOBULLE);
  }
  
}
