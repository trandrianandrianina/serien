/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.image.IconeBouton;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton affichant une icône cliquable.
 *
 * Cette classe est destinée à être dérivée avec les informations spécifiques à chaque bouton. Le comportement commun est défini dans
 * cette classe à savoir :
 * - Le bouton est en forme de carré (hauteur = largeur).
 * - Toutes les parties du bouton sont invisibles à l'exception de l'image.
 * - Affiche un curseur en forme de "main" lorsque le curseur est au dessus du bouton.
 * - Affiche un message d'explication sur le rôle du bouton dans l'infobulle.
 * - L'icône s'assombrie lorsque le curseur de la souris est au dessus du bouton.
 * - L'icône s'éclaircie lorsqu'on clique sur le bouton.
 *
 * A noter que s'il y a un problème avec l'icône passée en paramètre, une icône par défaut est utilisée en substitution.
 */
public class SNBoutonIcone extends JButton {
  private IconeBouton iconeBouton = null;
  private int taille = 0;
  private String infoBulle = null;
  
  /**
   * Constructeur protégé afin que cette classe ne soit pas utilisée ailleurs que dans ce package.
   *
   * @param pIconeBouton Icône à afficher dans le bouton.
   * @param pTaille Taille de l'icône (taille = hauteur = largeur).
   * @param pInfobulle Texte de l'infobulle expliquant le rôle du bouton.
   */
  protected SNBoutonIcone(IconeBouton pIconeBouton, int pTaille, String pInfobulle) {
    super();
    
    // Mémoriser les valeurs fournies
    taille = pTaille;
    infoBulle = pInfobulle;

    // Mémoriser l'icône (on utilise une icône par défaut en cas de problème avec l'icône fournie)
    if (pIconeBouton != null && pIconeBouton.getIconeStandard() != null) {
      iconeBouton = pIconeBouton;
    }
    else if (taille < 26) {
      Trace.alerte("Utilisation de l'icône par défaut pour : " + pInfobulle);
      iconeBouton = SNCharteGraphique.ICONE_DEFAUT_16;
    }
    else {
      Trace.alerte("Utilisation de l'icône par défaut pour : " + pInfobulle);
      iconeBouton = SNCharteGraphique.ICONE_DEFAUT_26;
    }
    
    // Définir la taille préférée
    setPreferredSize(new Dimension(taille, taille));
    setMinimumSize(new Dimension(taille, taille));
    setMaximumSize(new Dimension(taille, taille));
    
    // Rendre invisible toutes les parties du bouton sauf l'image
    setBorderPainted(false);
    setContentAreaFilled(false);
    
    // Afficher un curseur en forme de "main" lorsdque le curseur est au dessus de ce bouton
    setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    
    // Afficher une infobulle
    setToolTipText(infoBulle);
    
    // Renseigner les images des états actifs et inactifs
    setIcon(iconeBouton.getIconeStandard());
    setDisabledIcon(iconeBouton.getIconeGrisee());
    
    // Griser l'image lorsque le curseur passe dessus
    addMouseListener(new MouseListener() {
      @Override
      public void mouseReleased(MouseEvent arg0) {
      }
      
      @Override
      public void mousePressed(MouseEvent arg0) {
        setIcon(iconeBouton.getIconeClaire());
      }
      
      @Override
      public void mouseExited(MouseEvent arg0) {
        setIcon(iconeBouton.getIconeStandard());
      }
      
      @Override
      public void mouseEntered(MouseEvent arg0) {
        setIcon(iconeBouton.getIconeSombre());
      }
      
      @Override
      public void mouseClicked(MouseEvent arg0) {
      }
    });
  }
}
