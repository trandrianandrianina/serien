/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.stock.detailstock;

import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.stock.StockDetaille;
import ri.serien.libcommun.gescom.personnalisation.magasin.CritereMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;
import ri.serien.libswing.outil.clipboard.Clipboard;

/**
 * Modèle de la boîte de dialogue qui affiche le détail du stock d'un article.
 *
 * Si l'établissement et le magasin ont été fourni à la boîte de dialogue, ils sont conservés. Dans le cas contraire, l'établissement
 * est récupéré dans la session et le magasin est configuré avec le magasin par défaut de l'utilisateur.
 *
 * Cette boîte de dialogue s'apppuie sur la classe StockDetaille.
 */
public class ModeleDetailStock extends AbstractModeleDialogue {
  // Constantes
  protected static final int ONGLET_GENERAL = 0;
  protected static final int ONGLET_ATTENDUS_COMMANDES = 1;
  protected static final int ONGLET_MOUVEMENTS_STOCKS = 2;
  protected static final int ONGLET_STOCK_PAR_MAGASIN = 3;

  // Variables
  private int indexOngletActif = ONGLET_GENERAL;
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private ListeMagasin listeMagasin = null;
  private IdArticle idArticle = null;
  private Article article = null;
  private StockDetaille stockDetaille = null;

  /**
   * Constructeur standard du modèle.
   */
  public ModeleDetailStock(SessionBase pSession, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, IdArticle pIdArticle) {
    super(pSession);
    idEtablissement = pIdEtablissement;
    idMagasin = pIdMagasin;
    idArticle = pIdArticle;

    // Titre
    setTitreEcran("Détail des stocks pour l'article " + idArticle);
  }

  @Override
  public void initialiserDonnees() {
    IdSession.controlerId(getIdSession(), true);
  }

  @Override
  public void chargerDonnees() {
    // Renseigner automatiquement l'établissement et le magasin si l'établissement n'est pas renseigné
    if (idEtablissement == null) {
      // Utiliser l"établissement en cours dans la session
      idEtablissement = ManagerSessionClient.getInstance().getIdEtablissement(getIdSession());

      // Récupérer le magasin par défaut de l'utilisateur
      if (idMagasin == null) {
        // Charger les paramètres de l'utilisateur
        UtilisateurGescom utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(),
            ManagerSessionClient.getInstance().getProfil(), ManagerSessionClient.getInstance().getCurlib(), null);

        idMagasin = utilisateurGescom.getIdMagasin();
      }
    }

    // Charger la Liste des magasins de l'établissement
    if (listeMagasin == null) {
      CritereMagasin critereMagasin = new CritereMagasin();
      critereMagasin.setIdEtablissement(idEtablissement);
      listeMagasin = ListeMagasin.charger(getIdSession(), critereMagasin);
    }

    // Charger l'article
    if (article == null) {
      article = ManagerServiceArticle.lireArticle(getIdSession(), idArticle);
    }
    
    // Charger le stock de l'article
    if (stockDetaille == null) {
      stockDetaille = StockDetaille.chargerStockDetaille(getIdSession(), idEtablissement, idMagasin, idArticle);
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    super.quitterAvecValidation();
  }

  @Override
  public void quitterAvecAnnulation() {
    super.quitterAvecAnnulation();
  }

  /**
   * Copier les informations de stock détaillées dans le presse-papier.
   */
  public void copierPressePapier() {
    // Générer le texte à mettre dans le presse-papier
    String textePressePapier = "";
    if (stockDetaille != null) {
      textePressePapier = stockDetaille.getTextePressePapier();
    }
    
    // Mettre à jour le presse-papier
    Clipboard.envoyerTexte(textePressePapier);
  }
  
  /**
   * Modifier l'onglet actif.
   *
   * @param pIndexOngletActif Indice de l'onglet actif.
   */
  public void modifierOngletActif(int pIndexOngletActif) {
    // Vérifier si la valeur a changé
    if (indexOngletActif == pIndexOngletActif) {
      return;
    }

    // Changer l'onglet actif
    indexOngletActif = pIndexOngletActif;

    // Charger les données si le changement d'onglet nécessite un nouveau chargement de données
    chargerDonnees();

    // Rafraîchir l'affichage
    rafraichir();
  }

  /**
   *
   * Modifier l'établissement en cours.
   *
   * @param IdEtablissement
   */
  public void modifierEtablissement(IdEtablissement pIdEtablissement) {
    // Vérifier que la valeur a changé
    if (Constantes.equals(idEtablissement, pIdEtablissement)) {
      return;
    }
    
    // Initialiser les valeurs
    idEtablissement = pIdEtablissement;
    idMagasin = null;
    stockDetaille = null;
    listeMagasin = null;
    
    // Recharger les données
    chargerDonnees();
    rafraichir();
  }

  /**
   *
   * Modifier le magasin en cours.
   *
   * @param IdMagasin
   */
  public void modifierMagasin(IdMagasin pIdMagasin) {
    // Vérifier que la valeur a changé
    if (Constantes.equals(idMagasin, pIdMagasin)) {
      return;
    }
    
    // Initialiser les valeurs
    idMagasin = pIdMagasin;
    stockDetaille = null;
    
    // Recharger les données
    chargerDonnees();
    rafraichir();
  }

  /**
   * Retourner l'index d el'onglet actif.
   * @return Index de l'onglet actif.
   */
  public int getIndexOngletActif() {
    return indexOngletActif;
  }

  /**
   * Retourner l'identifiant de l'établissement dont le stock est affiché.
   * @return identifiant de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }

  /**
   * Retourner l'identifiant du magasin dont le stock est affiché.
   * @return Identiffiant magasin.
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }

  /**
   * Retourner le nom d'un magasin.
   *
   * @param pIdMagasin Identifiant du magasin dont il faut le nom.
   * @return Nom du magasin.
   */
  public String getNomMagasin(IdMagasin pIdMagasin) {
    if (listeMagasin == null) {
      return "";
    }
    return listeMagasin.getNomMagasin(pIdMagasin);
  }

  /**
   * Retourner l'article dont le stock est affiché.
   * @return Article.
   */
  public Article getArticle() {
    return article;
  }

  /**
   * Retourner le nombre de décimales de l'unité de stock.
   * @return Nombre de décimales de l'utilité de stock (0 en l'absence d'article ou d'unité).
   */
  public int getNombreDecimalesUS() {
    if (article == null || article.getUniteStock() == null) {
      return 0;
    }
    return article.getUniteStock().getNombreDecimale();
  }

  /**
   * Retourner le stock détaillé de l'article.
   * @return Stock détaillé.
   */
  public StockDetaille getStockDetaille() {
    return stockDetaille;
  }

}
