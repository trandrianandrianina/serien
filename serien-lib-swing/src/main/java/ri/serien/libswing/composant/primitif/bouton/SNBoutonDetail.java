/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton affichant une image en forme de signe plus permettant d'afficher des informations plus détaillées.
 *
 * La taille standard est de 16 x 16 pixels car cette icône doit rester assez discrète. Elle est souvent utilisée dans un coin de panneau.
 */
public class SNBoutonDetail extends SNBoutonIcone {
  private static final int TAILLE_IMAGE = 16;
  private static final String INFOBULLE = "Afficher le détail";

  /**
   * Constructeur.
   */
  public SNBoutonDetail() {
    super(SNCharteGraphique.ICONE_DETAIL, TAILLE_IMAGE, INFOBULLE);
  }
}
