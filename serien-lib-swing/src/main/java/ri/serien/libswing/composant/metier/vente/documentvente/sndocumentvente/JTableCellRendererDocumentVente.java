/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.sndocumentvente;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.vente.document.EnumEtatBonDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatDevisDocumentVente;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;

public class JTableCellRendererDocumentVente extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  private static Color blanc = new Color(255, 255, 255);
  private static Color vert = new Color(152, 206, 168);
  private static Color orange = new Color(211, 187, 114);
  private static Color rouge = new Color(218, 166, 161);
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    try {
      // Taille et justification des colonnes
      redimensionnerColonnes(table);
      
      // Couleur dernière cellule de la ligne suivant statut du document
      if (column == 9) {
        if (value != null && (value.equals(EnumEtatDevisDocumentVente.ENREGISTRE.getLibelle())
            || value.equals(EnumEtatDevisDocumentVente.EDITE.getLibelle()) || value.equals(EnumEtatDevisDocumentVente.SIGNE.getLibelle())
            || value.equals(EnumEtatDevisDocumentVente.CLOTURE.getLibelle())
            || value.equals(EnumEtatBonDocumentVente.FACTURE.getLibelle())
            || value.equals(EnumEtatBonDocumentVente.COMPTABILISE.getLibelle())
            || value.equals(EnumEtatBonDocumentVente.EXPEDIE.getLibelle()))) {
          setBackground(vert);
          setForeground(vert);
          setToolTipText(value.toString());
        }
        else if (value != null && (value.equals(EnumEtatBonDocumentVente.RESERVE.getLibelle())
            || value.equals(EnumEtatBonDocumentVente.VALIDE.getLibelle()) || value.equals(EnumEtatDevisDocumentVente.ATTENTE.getLibelle())
            || value.equals(EnumEtatBonDocumentVente.ATTENTE.getLibelle()))) {
          setBackground(orange);
          setForeground(orange);
          setToolTipText(value.toString());
        }
        else if (value != null && (value.equals(EnumEtatDevisDocumentVente.PERDU.getLibelle())
            || value.equals(EnumEtatDevisDocumentVente.VALIDITE_DEPASSEE.getLibelle()))) {
          setBackground(rouge);
          setForeground(rouge);
          setToolTipText(value.toString());
        }
        else if (value != null) {
          setBackground(blanc);
          setForeground(blanc);
          setToolTipText(value.toString());
        }
        else {
          setBackground(blanc);
          setForeground(blanc);
        }
      }
    }
    catch (Exception e) {
      DialogueErreur.afficher(e);
    }
    return component;
  }
  
  /**
   * Redimensionne et justifie les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(70);
    cm.getColumn(0).setMaxWidth(70);
    cm.getColumn(1).setMaxWidth(80);
    cm.getColumn(1).setMinWidth(80);
    cm.getColumn(2).setMinWidth(170);
    cm.getColumn(2).setMaxWidth(170);
    cm.getColumn(3).setMinWidth(95);
    cm.getColumn(3).setMaxWidth(95);
    cm.getColumn(4).setMinWidth(95);
    cm.getColumn(4).setMaxWidth(95);
    cm.getColumn(5).setMinWidth(180);
    cm.getColumn(6).setMinWidth(120);
    cm.getColumn(7).setMinWidth(60);
    cm.getColumn(7).setMaxWidth(65);
    cm.getColumn(8).setMinWidth(100);
    cm.getColumn(9).setMinWidth(15);
    cm.getColumn(9).setMaxWidth(15);
    
    // Justification
    centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
    leftRenderer.setHorizontalAlignment(SwingConstants.LEFT);
    cm.getColumn(0).setCellRenderer(rightRenderer);
    cm.getColumn(1).setCellRenderer(centerRenderer);
    cm.getColumn(2).setCellRenderer(leftRenderer);
    cm.getColumn(3).setCellRenderer(rightRenderer);
    cm.getColumn(4).setCellRenderer(rightRenderer);
    cm.getColumn(5).setCellRenderer(leftRenderer);
    cm.getColumn(6).setCellRenderer(leftRenderer);
    cm.getColumn(7).setCellRenderer(leftRenderer);
    cm.getColumn(8).setCellRenderer(leftRenderer);
    
  }
}
