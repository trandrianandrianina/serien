/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton affichant une image en forme de point d'interrogation permettant d'afficher une aide.
 *
 * La taille standard est de 16 x 16 pixels car cette icône doit rester assez discrète.
 */
public class SNBoutonInformation extends SNBoutonIcone {
  private static final int TAILLE_IMAGE = 16;
  private static final String INFOBULLE = "Afficher des informations complémentaires";
  
  /**
   * Constructeur.
   */
  public SNBoutonInformation() {
    super(SNCharteGraphique.ICONE_INFORMATION, TAILLE_IMAGE, INFOBULLE);
  }
}
