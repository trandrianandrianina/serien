/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.saisie.snmontant;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposant;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Saisie ou consultation d'un montant, éventuellement accompagné d'une devise et/ou d'une unité.
 *
 * Il reprend les caractéristiques graphiques de SNTexte sauf qu'il est justifié à droite (comme les nombres dans un tableur). Les
 * caractères autorisés sont "+-012345679.,". Même si le composant accepte le point '.' et la virgule ',' comme signes pour les décimals,
 * le '.' est automatiquement converti en ',' pour respecter le standard français.
 *
 * Les valeurs par défaut pour la longueur des parties entière et décimale ont été définies à partir des valeurs les plus couramment
 * utilisées dans le GAP. Par défaut, la zone suffixe est masquée.
 *
 * Conernant la devise, les valeurs "EUR" ou "EURO" sont automatiquement remplacées par le symbole '€'.
 *
 * Le composant peut être paramétré selon différentes méthodes :
 * - setLibelleDevise() : définit la devise à afficher dans le suffixe. Par défaut, ce paramètre est vide.
 * - setLibelleUnite() : définit l'unité à afficher dans le suffixe. Par défaut, ce paramètre est vide.
 * - setModeNegatif(): affiche les valeurs négatives en rouge lorsque ce paramètre est à true. C'est false par défaut.
 * - setModeTotal() : affiche les valeurs en gras lorsque ce paramètre est à true. C'est false par défaut.
 * - setLongueurMaxPartieEntiere() : définit la longueur maximale de la partie entière. Par défaut, ce paramètre est initialisé à 9.
 * - setLongueurMaxPartieDecimale() : définit la longueur maximale de la partie décimale. Par défaut, ce paramètre est initialisé à 2.
 * - setNegatifAutorise(): autorise les montants négatifs. C'est false par défaut.
 * - setMinimum() : définit le montant minimum saisissable.
 * - setMaximum() : définit le montant maximum saisissable.
 *
 * Lors de la récupération de données depuis le RPG, les montants négatifs peuvent avoir le signe - à la fin de la chaine de caractère.
 * Ce cas est traité dans le setMontantParChampRPG en supprimant le - à la fin et en l'ajoutant au début de la chaîne de caractère avant
 * d'appeler la méthode setMontant().
 * Cependant, il est possible que le traitement des montants négatifs soit source de bug à l'avenir.
 */
public class SNMontant extends SNComposant {
  // Constantes
  private final Dimension TAILLE_MONTANT = new Dimension(120, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);
  private final Dimension TAILLE_SUFFIXE = new Dimension(80, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);
  private final Dimension TAILLE_MONTANT_REDUIT = new Dimension(80, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT_REDUIT);
  private final Dimension TAILLE_SUFFIXE_REDUIT = new Dimension(50, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT_REDUIT);
  private static final Color COULEUR_STANDARD = Color.black;
  private static final Color COULEUR_NEGATIF = Color.red;
  
  // Montant et formattage
  private BigDecimal montant = null;
  private SNMontantDocumentFilter snMontantDocumentFilter = new SNMontantDocumentFilter();
  private String regex = null;
  private String format = null;
  private DecimalFormat decimalFormat = null;
  
  // Présentation
  private boolean modeReduit = false;
  private boolean changementMode = false;
  private Font policeNormal = SNCharteGraphique.POLICE_STANDARD;
  private Font policeTotal = SNCharteGraphique.POLICE_TITRE;
  private String libelleUnite = null;
  private String libelleDevise = null;
  private boolean modeNegatif = false;
  private boolean modeTotal = false;
  
  // Valeurs autorisées
  private int longueurMaxPartieEntiere = 9;
  private int longueurPartieDecimale = 2;
  private boolean negatifAutorise = false;
  private BigDecimal maximum = null;
  private BigDecimal minimum = null;
  
  /**
   * Contrôler les saisies du composant SNMontant avec de s'assurer que le texte résultant est conforme aux attentes.
   *
   * Les saisies valides sont :
   * - la chaine vide "",
   * - les chaines avec uniquement le caractère "-" ou "+"
   * - les chaînes entières "123456"
   * - les chaînes entières suivies d'une virgule "123456." ou "123456,"
   * - les chaînes décimales "123456.12" ou "123456,12"
   * - les chaînes signées "-123456.12" ou "+123456.12"
   */
  private class SNMontantDocumentFilter extends DocumentFilter {
    
    /**
     * Constructeur par défaut.
     */
    public SNMontantDocumentFilter() {
      super();
    }
    
    /**
     * Vérifier si le texte saisi correspond à un montant valide.
     *
     * Cette méthode fait l'essentiel du travail de contrôle. Elle vérifie :
     * - le nombre de chiffres de la partie entière,
     * - le nombre de chiffres de la partie décimale,
     * - la capacité à convertir le texte saisit en BigDecimal,
     * - la comparaison avec les valeurs minimum et maximum.
     *
     * Cette méthode intervient également pour :
     * - mettre à jour l'attribut "montant" du composant avec la dernière valeur valide,
     * - changer la couleur du composant graphique pour les valeurs négatives (si le paramètre correspondant est activé).
     *
     * @param Texte à contrôler.
     * @return true=texte valide, false=tetxe invalide.
     */
    private boolean verifierTexte(String pTexte) {
      if (pTexte == null) {
        montant = null;
      }
      else {
        // Supprimer les séparateurs des milliers et convertir la virgule en point
        // Comme DecimalFormat utilise la caractère Unicode 160 (non-breaking space) comme séparateur des milliers, il faut utiliser
        // isSpaceChar() pour enlever les espaces entre les chiffres.
        StringBuilder sb = new StringBuilder(pTexte.length());
        for (int i = 0; i < pTexte.length(); i++) {
          char caractere = pTexte.charAt(i);
          if (caractere == ',') {
            sb.append('.');
          }
          else if (!Character.isSpaceChar(caractere)) {
            sb.append(pTexte.charAt(i));
          }
        }
        String texteNorme = sb.toString();

        // Contrôler que le texte est conforme à la regex
        if (!texteNorme.matches(regex)) {
          return false;
        }

        // Tester les textes valides mais atypiques
        // "isEmpty" lorsque le champ est vide
        // "+" lorsqu'on commence à saisir un texte positif (le + est facultatif mais permis au cas où)
        if (texteNorme.isEmpty() || texteNorme.equals("+")) {
          montant = null;
          tfMontant.setForeground(COULEUR_STANDARD);
        }
        // "-" lorsqu'on commence à saisir un texte négatif
        else if (pTexte.equals("-")) {
          montant = null;
          if (modeNegatif) {
            tfMontant.setForeground(COULEUR_NEGATIF);
          }
          else {
            tfMontant.setForeground(COULEUR_STANDARD);
          }
        }
        else {

          // Convertir le montant en BigDecimal
          BigDecimal montantSaisi = null;
          try {
            montantSaisi = new BigDecimal(texteNorme);
          }
          catch (Exception e) {
            // Mettre une erreur dans les traces car si la regex joue correctement son rôle, il ne doit pas y avoir d'erreur de conversion
            Trace.erreur(e, "Saisie invalide : " + texteNorme);
            return false;
          }

          // Tester si le montant ne dépasse pas le montant maximum
          if (maximum != null && montantSaisi.compareTo(maximum) > 0) {
            Trace.alerte("Le montant est supérieur au montant maximum autorisé : montant=" + montantSaisi + ", maximum=" + maximum);
            return false;
          }

          // Tester si le montant ne dépasse pas le montant minimuym
          if (minimum != null && montantSaisi.compareTo(minimum) < 0) {
            Trace.alerte("Le montant est inférieur au montant minimum autorisé : montant=" + montantSaisi + ", minimum=" + minimum);
            return false;
          }

          // Modifier le montant à la fin lorsqu'on est certain que la saisie a été acceptée
          montant = montantSaisi;

          // Colorier le texte en fonction du mode "Negatif"
          if (modeNegatif && montant.compareTo(BigDecimal.ZERO) < 0) {
            tfMontant.setForeground(COULEUR_NEGATIF);
          }
          else {
            tfMontant.setForeground(COULEUR_STANDARD);
          }
        }
      }
      return true;
    }
    
    /**
     * Insérer un caractère dans le texte du composant de saisie.
     *
     * @param pFilterBypass Permet de modifier le texte du composant sans repasser par le DocumentFilter.
     * @param pPosition Position du texte inséré.
     * @param pSaisie Caractère inséré.
     * @param pAttributeSet Non utilisé (je ne sais pas à quoi cela sert).
     */
    @Override
    public void insertString(FilterBypass pFilterBypass, int pPosition, String pSaisie, AttributeSet pAttributeSet)
        throws BadLocationException {
      // Remplacer le point par la virgule pour l'affichage des décimales
      if (pSaisie.equals(".")) {
        pSaisie = ",";
      }
      
      // Récupérer le texte non modifié
      String texteInitial = pFilterBypass.getDocument().getText(0, pFilterBypass.getDocument().getLength());
      
      // Déterminer le texte modifié
      String texteFinal = texteInitial.substring(0, pPosition) + pSaisie + texteInitial.substring(pPosition);
      
      // Tester si le texte modifié est valide
      if (verifierTexte(texteFinal)) {
        super.insertString(pFilterBypass, pPosition, pSaisie, pAttributeSet);
      }
      else {
        Trace.debug("Saisie refusée : texteInitial=" + texteInitial + ", position=" + pPosition + ", saisie=" + pSaisie + ", texteFinal="
            + texteFinal);
      }
    }
    
    /**
     * Remplacer une portion de texte par un caractère dans le texte du composant de saisie.
     *
     * @param pFilterBypass Permet de modifier le texte du composant sans repasser par le DocumentFilter.
     * @param pPosition Position du texte à supprimr.
     * @param pLongueur Taille du texte à supprimer.
     * @param pSaisie Caractère inséré.
     * @param pAttributeSet Non utilisé (je ne sais pas à quoi cela sert).
     */
    @Override
    public void replace(DocumentFilter.FilterBypass pFilterBypass, int pPosition, int pLongueur, String pSaisie,
        AttributeSet pAttributeSet) throws BadLocationException {
      // Remplacer le point par la virgule pour l'affichage des décimales
      if (pSaisie.equals(".")) {
        pSaisie = ",";
      }
      
      // Récupérer le texte non modifié
      String texteInitial = pFilterBypass.getDocument().getText(0, pFilterBypass.getDocument().getLength());
      
      // Déterminer le texte modifié
      String texteFinal = texteInitial.substring(0, pPosition) + pSaisie + texteInitial.substring(pPosition + pLongueur);
      
      // Tester si le texte modifié est valide
      if (verifierTexte(texteFinal)) {
        super.replace(pFilterBypass, pPosition, pLongueur, pSaisie, pAttributeSet);
      }
      else {
        Trace.debug("Saisie refusée : texteInitial=" + texteInitial + ", position=" + pPosition + ", pLongueur=" + pLongueur + ", saisie="
            + pSaisie + ", texteFinal=" + texteFinal);
      }
    }
    
    /**
     * Remplacer une portion de texte par un caractère dans le texte du composant de saisie.
     *
     * @param pFilterBypass Permet de modifier le texte du composant sans repasser par le DocumentFilter.
     * @param pPosition Position du texte à supprimr.
     * @param pLongueur Taille du texte à supprimer.
     */
    @Override
    public void remove(DocumentFilter.FilterBypass pFilterBypass, int pPosition, int pLongueur) throws BadLocationException {
      // Récupérer le texte non modifié
      String texteInitial = pFilterBypass.getDocument().getText(0, pFilterBypass.getDocument().getLength());
      
      // Déterminer le texte modifié
      String texteFinal = texteInitial.substring(0, pPosition) + texteInitial.substring(pPosition + pLongueur);
      
      // Tester si le texte modifié est valide
      if (verifierTexte(texteFinal)) {
        super.remove(pFilterBypass, pPosition, pLongueur);
      }
      else {
        Trace.debug("Saisie refusée : texteInitial=" + texteInitial + ", position=" + pPosition + ", texteFinal=" + texteFinal);
      }
    }
  }
  
  /**
   * Constructeur.
   */
  public SNMontant() {
    super();
    
    // Créer graphiquement le composant
    initComponents();

    // Exceptionnement pour l'initialisation le changement d'aspect est forcé
    changementMode = true;
    rafraichirAspect();
    
    // Masquer le suffixe par défaut
    lbSuffixe.setVisible(false);
    
    // Configurer le document filter du composant de saisie du montant
    AbstractDocument document = (AbstractDocument) tfMontant.getDocument();
    document.setDocumentFilter(snMontantDocumentFilter);
    
    // Paramètre le composant avec les valeurs par défaut.
    parametrerComposant();
  }
  
  /**
   * Générer l'expression régulière qui permet de contrôler les saisies.
   *
   * Cette méthode soit être en phase avec la méthode genererFormat() car c'est la chaîne de caractères formatée par cette dernière qui
   * sera soumise à la regex pour vérifier la validité.
   *
   * Exemple de regex pour 12345.67 : ^[0-9]{0,5}([.,][0-9]{0,2})?$
   */
  private void genererRegex() {
    // Ajouter l'indicateur de début
    regex = "^";
    
    // Ajouter la gestion du signe -
    if (negatifAutorise) {
      regex += "[-+]?";
    }
    
    // Ajouter la partie entière de la regex
    regex += "[0-9]";
    if (longueurMaxPartieEntiere > 0) {
      regex += "{0," + longueurMaxPartieEntiere + "}";
    }
    else {
      regex += "*";
    }
    
    // Ajouter la partie décimale de la regex
    if (longueurPartieDecimale > 0) {
      regex += "([.,][0-9]{0," + longueurPartieDecimale + "})?";
    }
    
    // Ajouter l'indicateur de fin
    regex += "$";
  }
  
  /**
   * Générer le format Java qui présente le montant conformément à la regex de contrôle.
   * Exemple de format pour 12345.67 : "##,##0.00"
   */
  private void genererFormat() {
    format = "";
    
    // Construire la partie entière du format
    for (int i = longueurMaxPartieEntiere; i > 0; i--) {
      if (i % 3 == 0 && i != longueurMaxPartieEntiere) {
        format += ",#";
      }
      else if (i == 1) {
        format += "0";
      }
      else {
        format += "#";
      }
    }
    
    // Construire la partie décimale du format
    if (longueurPartieDecimale > 0) {
      format = format + ".";
      for (int i = 1; i <= longueurPartieDecimale; i++) {
        format = format + "0";
      }
    }
    
    // Générer le formatter
    decimalFormat = new DecimalFormat(format, new DecimalFormatSymbols(Locale.FRENCH));
  }
  
  /**
   * Paramétrer le composant.
   */
  private void parametrerComposant() {
    // Générer la regex de contrôle et le format de présentation
    genererRegex();
    genererFormat();
    
    // Modifier l'infobulle du composant
    if (minimum != null && maximum != null) {
      tfMontant.setToolTipText("Le montant doit être compris entre " + minimum + " et " + maximum + " inclus (" + format + ")");
    }
    else if (minimum != null) {
      tfMontant.setToolTipText("Le montant doit être supérieur ou égal à " + minimum + " (" + format + ")");
    }
    else if (maximum != null) {
      tfMontant.setToolTipText("Le montant doit être inférieur ou égal à " + maximum + " (" + format + ")");
    }
    else {
      tfMontant.setToolTipText("(" + format + ")");
    }
  }
  
  /**
   * Rafraîchir l'affichage des composants.
   */
  private void rafraichir() {
    rafraichirMontant();
    rafraichirSuffixe();
  }
  
  /**
   * Rafraichir le contenu du champ de saisie.<p>
   *
   * Le montant est formaté à l'affichage selon :<br>
   * - si l'apparence "négatif" a été autorisée et si le composant contient un montant négatif ou non.<br>
   * - si le champ a été défini comme contenant un entier ou un nombre décimal.<p>
   *
   * Si le champ contient un nombre décimal, le format affiche le nombre de 0 après la virgule nécessaire en complément à la saisie de
   * l'utilisateur.
   */
  private void rafraichirMontant() {
    // Si le montant saisi est absent,
    if (montant == null) {
      tfMontant.setText("");
    }
    // Si le montant saisi est "-", pour ne pas passer par le formatage et générer d'erreur.
    else if (montant.toPlainString().equals("-")) {
      tfMontant.setText(montant.toPlainString());
    }
    else {
      tfMontant.setText(getMontantTexte());
    }
    
    // Présenter en fonction du mode "Total"
    if (modeTotal) {
      tfMontant.setFont(policeTotal);
    }
    else {
      tfMontant.setFont(policeNormal);
    }
    
    // Présenter en fonction du mode "Negatif"
    if (modeNegatif && montant != null && montant.compareTo(BigDecimal.ZERO) < 0) {
      tfMontant.setForeground(COULEUR_NEGATIF);
    }
    else {
      tfMontant.setForeground(COULEUR_STANDARD);
    }
  }
  
  /**
   * Rafraîchir le contenu du suffixe
   *
   * Le contenu du suffixe est une devise par unité, une unité, une devise, ou rien.
   * La devise et l'unité seront séparées par le symbole "/".
   */
  private void rafraichirSuffixe() {
    String libelleSuffixe = null;
    
    // Tester si la devise et l'unité sont présents
    if (libelleDevise != null && !libelleDevise.isEmpty() && libelleUnite != null && !libelleUnite.isEmpty()) {
      // Afficher l'unité et le suffixe.
      libelleSuffixe = libelleDevise + " / " + libelleUnite;
    }
    else if (libelleDevise != null && !libelleDevise.isEmpty()) {
      // Afficher la devise uniquement
      libelleSuffixe = libelleDevise;
    }
    else if (libelleUnite != null && !libelleUnite.isEmpty()) {
      // Afficher l'unité uniquement
      libelleSuffixe = "/ " + libelleUnite;
    }
    
    // Rafraîchir le composant graphique
    if (libelleSuffixe != null) {
      lbSuffixe.setText(libelleSuffixe);
      lbSuffixe.setVisible(true);
    }
    else {
      lbSuffixe.setText("");
      lbSuffixe.setVisible(false);
    }
    
    // Présenter en fonction du mode "Total"
    if (modeTotal) {
      lbSuffixe.setFont(policeTotal);
    }
    else {
      lbSuffixe.setFont(policeNormal);
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit l'aspect du composant.
   */
  private void rafraichirAspect() {
    // Sélection de la police en fonction du mode uniquement si le mode a été modifié
    if (changementMode) {
      if (modeReduit) {
        // Fixer la taille du montant et du suffixe
        tfMontant.setMinimumSize(TAILLE_MONTANT_REDUIT);
        tfMontant.setPreferredSize(TAILLE_MONTANT_REDUIT);
        tfMontant.setMaximumSize(TAILLE_MONTANT_REDUIT);
        lbSuffixe.setMinimumSize(TAILLE_SUFFIXE_REDUIT);
        lbSuffixe.setPreferredSize(TAILLE_SUFFIXE_REDUIT);
        lbSuffixe.setMaximumSize(TAILLE_SUFFIXE_REDUIT);
      }
      else {
        // Fixer la taille du montant et du suffixe
        tfMontant.setMinimumSize(TAILLE_MONTANT);
        tfMontant.setPreferredSize(TAILLE_MONTANT);
        tfMontant.setMaximumSize(TAILLE_MONTANT);
        lbSuffixe.setMinimumSize(TAILLE_SUFFIXE);
        lbSuffixe.setPreferredSize(TAILLE_SUFFIXE);
        lbSuffixe.setMaximumSize(TAILLE_SUFFIXE);
      }
      changementMode = false;
    }
    
    // Sélection des tailles en fonction du mode
    if (modeReduit) {
      policeTotal = SNCharteGraphique.POLICE_TITRE_REDUIT;
      policeNormal = SNCharteGraphique.POLICE_STANDARD_REDUIT;
    }
    else {
      policeTotal = SNCharteGraphique.POLICE_TITRE;
      policeNormal = SNCharteGraphique.POLICE_STANDARD;
    }

    // Les composants associés
    lbSuffixe.setModeReduit(modeReduit);

    rafraichir();
  }

  // -- Accesseurs
  
  /**
   * Active ou non le mode réduit du composant.
   * @param pActiverModeReduit
   */
  public void setModeReduit(boolean pActiverModeReduit) {
    // Contrôle s'il y a un chagement de mode (réduit ou non)
    if (modeReduit != pActiverModeReduit) {
      changementMode = true;
    }
    else {
      changementMode = false;
    }
    modeReduit = pActiverModeReduit;
    rafraichirAspect();
  }

  /**
   * Retourne si le mode réduit est actif.
   * @return
   */
  public boolean isModeReduit() {
    return modeReduit;
  }

  /**
   * Renvoyer le montant saisi sous la forme d'une chaine de caractères.
   *
   * @return Montant stocké par le composant, String.
   */
  @Override
  public String toString() {
    return getMontantTexte();
  }
  
  /**
   * Gèrer l'activation du composant.
   *
   * @param pEnabled activé si true, désactivé si false.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    tfMontant.setEnabled(pEnabled);
    lbSuffixe.setEnabled(pEnabled);
  }
  
  /**
   * Renvoyer la valeur de la devise paramétrée pour le composant.
   *
   * @return Libellé de la devise.
   */
  public String getLibelleDevise() {
    return libelleDevise;
  }
  
  /**
   * Définir la devise à afficher en suffixe.<p>
   * Si la valeur saisie en paramètre pour la devise est "eur" ou "euro", la devise devient "€".
   *
   * @param pLibelleDevise Libellé de la devise.
   */
  public void setLibelleDevise(String pLibelleDevise) {
    // Tester si la valeur a changé
    if (Constantes.equals(pLibelleDevise, libelleDevise)) {
      return;
    }
    
    // Tester si c'est la devise Euro
    if (pLibelleDevise != null && (pLibelleDevise.equalsIgnoreCase("eur") || pLibelleDevise.equalsIgnoreCase("euro"))) {
      libelleDevise = "\u20AC";
    }
    // Mettre la devis en majuscules
    else if (pLibelleDevise != null && !pLibelleDevise.isEmpty()) {
      libelleDevise = pLibelleDevise.toUpperCase();
    }
    else {
      libelleDevise = null;
    }
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Renvoyer la valeur de l'unité paramétrée pour le suffixe du composant.
   *
   * @return Libellé de l'unité.
   */
  public String getLibelleUnite() {
    return libelleUnite;
  }
  
  /**
   * Définir l'unité à afficher en suffixe.
   *
   * @param pLibelleUnite Libellé de l'unité.
   */
  public void setLibelleUnite(String pLibelleUnite) {
    // Tester si la valeur a changé
    if (Constantes.equals(pLibelleUnite, libelleUnite)) {
      return;
    }
    
    // Mettre à jour l'unité
    libelleUnite = pLibelleUnite;
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Renvoyer le paramétrage sélectionné pour la mise en forme des montants négatifs.<p>
   *
   * @return Mode "négatif" activé si true, désactivé si false.
   */
  public boolean isModeNegatif() {
    return modeNegatif;
  }
  
  /**
   * Définir l'utilisation de l'apparence conditionnée à la saisie d'un montant négatif.<p>
   * La mise en forme est appliquée au champ de saisie uniquement.
   *
   * @param pModeNegatif activé si true, désactivé si false.
   */
  public void setModeNegatif(Boolean pModeNegatif) {
    // Tester si la valeur a changé
    if (pModeNegatif == modeNegatif) {
      return;
    }
    
    // Mettre à jour la valeur
    modeNegatif = pModeNegatif;
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Renvoyer le paramétrage sélectionné pour l'affichage de type "total".
   *
   * @return Mode "total" activé si true, désactivé si false.
   */
  public boolean isModeTotal() {
    return modeTotal;
  }
  
  /**
   * Activer le mode "total" pour l'affichage.
   *
   * @param pModeTotal Activé si true, désactivé si false.
   */
  public void setModeTotal(boolean pModeTotal) {
    // Tester si la valeur a changé
    if (pModeTotal == modeTotal) {
      return;
    }
    
    // Mettre à jour la valeur
    modeTotal = pModeTotal;
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Définir la longueur maximale acceptée pour la saisie de la partie entière d'un montant.
   * Ce paramètre définit le nombre de caractères saisissables et affichables pour la partie entière du montant.
   *
   * @param pLongueurMaxPartieEntiere Longueur maximale de la partie entière.
   */
  public void setLongueurMaxPartieEntiere(int pLongueurMaxPartieEntiere) {
    // Si la valeur passée en paramètre est différente de la valeur stockée.
    if (pLongueurMaxPartieEntiere == longueurMaxPartieEntiere) {
      return;
    }
    // La valeur passée en paramètre doit être positive et non nulle, pour qu'au moins un chiffre puisse être saisi.
    if (pLongueurMaxPartieEntiere <= 0) {
      throw new MessageErreurException("La valeur saisie pour la longueur de la partie entière doit être supérieure à 0.");
    }
    
    // Modifier la valeur
    longueurMaxPartieEntiere = pLongueurMaxPartieEntiere;
    parametrerComposant();
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Renvoyer la longueur maximale acceptée pour la saisie de la partie entière du montant.
   *
   * @return Longueur maximale de la partie entière.
   */
  public int getLongueurMaxPartieEntiere() {
    return longueurMaxPartieEntiere;
  }
  
  /**
   * Définir la longueur de la partie décimale d'un montant.<p>
   * Ce paramètre définir la précision d'affichage du montant, le nombre de chiffres affichés après le séparateur décimal.<br>
   * En cas de saisie d'un nombre de chiffre inférieur, la saisie sera complétée par autant de 0 que nécessaire.<br>
   *
   * @param pLongueurPartieDecimale : Longueur maximale de la partie décimale.
   */
  public void setLongueurPartieDecimale(int pLongueurPartieDecimale) {
    // Si la valeur passée en paramètre est différente de la valeur stockée.
    if (pLongueurPartieDecimale == longueurPartieDecimale) {
      return;
    }
    // La valeur passée en paramètre doit être positive ou nulle.
    if (pLongueurPartieDecimale < 0) {
      throw new MessageErreurException("La valeur saisie pour la longueur de la partie décimale doit être supérieure ou égale à 0.");
    }
    
    // Modifier la valeur
    longueurPartieDecimale = pLongueurPartieDecimale;
    parametrerComposant();
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Renvoyer la longueur de la partie décimale du montant.
   *
   * @return Longueur maximale de la partie décimale.
   */
  public int getLongueurPartieDecimale() {
    return longueurPartieDecimale;
  }
  
  /**
   * Détecter si le montant est entier ou non.
   *
   * @return entier si true, false sinon.
   */
  public boolean isEntier() {
    return longueurPartieDecimale == 0;
  }
  
  /**
   * Renvoyer le paramétrage sélectionné pour la saisie des montants négatifs.
   *
   * @return Saisie autorisée si true, interdite si false.
   */
  public boolean isNegatifAutorise() {
    return negatifAutorise;
  }
  
  /**
   * Autoriser ou non la saisie d'un montant négatif.
   *
   * @param pNegatifAutorise autorisé si true, interdit si false.
   */
  public void setNegatifAutorise(Boolean pNegatifAutorise) {
    // Tester si la valeur a changé
    if (pNegatifAutorise == negatifAutorise) {
      return;
    }
    
    // Mettre à jour la valeur
    negatifAutorise = pNegatifAutorise;
    parametrerComposant();
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Montant maximum autorisé à la saisie.
   *
   * @return Montant maximum.
   */
  public BigDecimal getMaximum() {
    return maximum;
  }
  
  /**
   * Définir le montant maximal autorisé à la saisie par le composant.
   *
   * Ce paramètre a unqiuement une influence sur les montants saisis par l'utilisateur. Les montants renseigné via setMontant ne sont
   *
   *
   * @param pMaximum Montant maximum.
   */
  public void setMaximum(BigDecimal pMaximum) {
    // Tester si la valeur a changé
    if (Constantes.equals(pMaximum, maximum)) {
      return;
    }
    
    // Mettre à jour la valeur
    maximum = pMaximum;
    parametrerComposant();
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Définir le montant maximal autorisé à la saisie par le composant.
   *
   * @param pMaximum Montant maximum.
   */
  public void setMaximum(double pMaximum) {
    setMaximum(BigDecimal.valueOf(pMaximum).setScale(longueurPartieDecimale, RoundingMode.HALF_UP));
  }
  
  /**
   * Montant minimum autorisé à la saisie.
   *
   * @return Montant minimum.
   */
  public BigDecimal getMinimum() {
    return minimum;
  }
  
  /**
   * Définir le montant minimal autorisé à la saisie par le composant.
   *
   * @param pMinimum montant minimum.
   */
  public void setMinimum(BigDecimal pMinimum) {
    // Tester si la valeur a changé
    if (Constantes.equals(pMinimum, minimum)) {
      return;
    }
    
    // Mettre à jour la valeur
    minimum = pMinimum;
    parametrerComposant();
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Définir le montant minimal autorisé à la saisie par le composant.
   *
   * @param pMinimum Montant maximum.
   */
  public void setMinimum(double pMinimum) {
    setMinimum(BigDecimal.valueOf(pMinimum).setScale(longueurPartieDecimale, RoundingMode.HALF_UP));
  }
  
  /**
   * Montant sous la forme d'un BigDecimal.
   *
   * @return Montant stocké par le composant, BigDecimal.
   */
  public BigDecimal getMontant() {
    return montant;
  }
  
  /**
   * Montant sous la forme d'une chaîne de caractères.
   */
  public String getMontantTexte() {
    // Vérifier si le montant est null
    if (montant == null) {
      return "";
    }
    
    // Formatter le BigDecimal
    try {
      return decimalFormat.format(montant);
    }
    catch (NumberFormatException e) {
      Trace.erreur(e, "Erreur lors du formatage d'un montant dans le composant d'affichage d'un montant");
      return "";
    }
  }
  
  /**
   * Modifier le montant.
   *
   * La méthode génère une erreur si le montant n'est pas dans le plage de montant définie par le minimum et le maximum.
   *
   * @param pMontant Nouvelle valeur du montant.
   */
  public void setMontant(BigDecimal pMontant) {
    // Tester si la valeur a changé
    if (Constantes.equals(pMontant, montant)) {
      return;
    }
    
    // Tester si le montant est renseigné
    if (pMontant != null) {
      // Renseigner le momtant avec la bonne précision
      montant = pMontant.setScale(longueurPartieDecimale, RoundingMode.HALF_UP);
    }
    else {
      montant = null;
    }
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Modifier le montant.
   *
   * @param pMontant Nouvelle valeur du montant.
   */
  public void setMontant(String pMontant) {
    // Convertir le montant en BigDecimal
    BigDecimal montantConverti = null;
    if (pMontant != null && pMontant.equals("-")) {
      return;
    }
    else if (pMontant != null && !pMontant.isEmpty()) {
      // Pour ne pas générer d'erreur pendant la conversion en BigDecimal a valeur passée en paramètre doit être formatée pour ne pas
      // posséder de séparateur des millers, et avoir le point en séparateur décimal.
      pMontant = pMontant.replaceAll("[^0-9,.-]", "").replace(" ", "").replace(",", ".");
      montantConverti = new BigDecimal(pMontant);
    }
    
    // Modifier le montant
    setMontant(montantConverti);
  }
  
  /**
   * Modifier le montant.
   *
   * @param pMontant Nouvelle valeur du montant.
   */
  public void setMontant(int pMontant) {
    // Convertir le montant en BigDecimal
    BigDecimal montantConverti = new BigDecimal(pMontant);
    
    // Modifier le montant
    setMontant(montantConverti);
  }
  
  /**
   * Modifier le montant.
   *
   * @param pMontant Nouvelle valeur du montant.
   */
  public void setMontant(double pMontant) {
    // Convertir le montant en BigDecimal
    BigDecimal montantConverti = new BigDecimal(pMontant);
    
    // Modifier le montant
    setMontant(montantConverti);
  }
  
  /**
   * Afficher un montant à partir d'un champ RPG.
   *
   * @param pLexique lexique
   * @param pChampMontant Champ RPG associé au montant.
   */
  public void setMontantParChampRPG(Lexical pLexique, String pChampMontant) {
    String montantRPG = pLexique.HostFieldGetData(pChampMontant);
    if (montantRPG.endsWith("-")) {
      montantRPG = "-" + montantRPG.substring(0, montantRPG.length() - 1);
    }
    setMontant(montantRPG);
  }
  
  /**
   * Renseigner le champ RPG correspondant au montant.
   *
   * @param pLexique lexique.
   * @param pChampMontant Champ RPG associé au montant.
   */
  public void renseignerChampRPG(Lexical pLexique, String pChampMontant) {
    try {
      String montantPourRPG = toString().replace(".", ",");
      pLexique.HostFieldPutData(pChampMontant, 0, montantPourRPG);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourne le composant de saisie du montant.
   * Utile pour les composants qui dérive SNMontant.
   *
   * @return
   */
  public JTextField getComposantMontant() {
    return tfMontant;
  }

  /**
   * Sélectionner tout lorsque le composant gagne le focus.
   *
   * @param pFocusEvent Evènement.
   */
  private void tfMontantFocusGained(FocusEvent pFocusEvent) {
    try {
      tfMontant.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Sélectionner tout lorsque le composant gagne le focus.
   *
   * @param pFocusEvent Evènement.
   */
  private void tfMontantFocusLost(FocusEvent pFocusEvent) {
    try {
      // Rafraîchir l'affichage du composant afin que cela respecte le format souhaité pour l'affichage
      // On pense par exemple au zéros non significatifs après la virgule. Si l'utilisateur saisit "10.5" et quitte le champ, la valeur
      // du champ sera mise à jour avec "10,50" si le ocmposant ets configuré avec deux chiffres aprèx la virgule.
      if (montant == null) {
        tfMontant.setText("");
      }
      else {
        tfMontant.setText(getMontantTexte());
      }
      fireValueChanged();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    tfMontant = new SNTexte();
    lbSuffixe = new SNLabelChamp();

    // ======== this ========
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };

    // ---- tfMontant ----
    tfMontant.setForeground(Color.black);
    tfMontant.setHorizontalAlignment(SwingConstants.TRAILING);
    tfMontant.setName("tfMontant");
    tfMontant.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        tfMontantFocusGained(e);
      }

      @Override
      public void focusLost(FocusEvent e) {
        tfMontantFocusLost(e);
      }
    });
    add(tfMontant,
        new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // ---- lbSuffixe ----
    lbSuffixe.setHorizontalAlignment(SwingConstants.LEADING);
    lbSuffixe.setFont(new Font("sansserif", Font.BOLD, 14));
    lbSuffixe.setName("lbSuffixe");
    add(lbSuffixe,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 3, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNTexte tfMontant;
  private SNLabelChamp lbSuffixe;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
