/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.glasspane;

import java.awt.Component;
import java.awt.LayoutManager;

import javax.swing.JPanel;
import javax.swing.OverlayLayout;

/**
 * Panneau avec gestion du GlassPane.
 *
 * Le JDK gère nativement le GlassPane dans les JFrame mais pas dans les JPanel. Cette classe permet d'ajouter un GlassPane à un JPanel.
 *
 * Pour rappel, un GlassPane est comme une feuille de verre qui se positionne au dessus de tous les autres composants. Ainsi, il
 * intercepte tous les évènements en entrées (souris, clavier) et cela empêche l'utilisateur d'intervenir sur les composants situés en
 * dessous. C'est pratique pour empêcher temporairement toute saisie sur un composant. De plus, si on implèmente la méthode
 * paintComponent(), le GlassPane peut dessiner quelquechose comme par exemple une animation d'attente.
 *
 * Avec le constructeur par défaut, ce composant est associé à un GlassPane basique JPanel qui n'affiche rien. Il est possible d'associer
 * ce composant à GlassPaneSoleilAttente pour avoir une animation d'attente en utilisant la méthode setGlassPane(). C'est ce que fait la
 * classe SNPanelGlassPaneSoleilAttente. Enfin, cette classe présente un constructeur avec lequel il est possible de fournir son propre
 * glasspane.
 *
 * Le JPanel de cette classe se positionne entre la fenêtre parente et le JPanel qui va réellement recevoir les composants de l'interface
 * (pnlContenu). Les méthodes setLayout(), addImpl() et remove() sont surchargées pour que les opérations correspondantes soient
 * effectuées sur pnlContenu. On a toutefois besoin d'intervenir sur le JPanel intermédiare de temps en temps, c'est le rôle de la
 * variable isPourContenu de gérer l'aiguillage.
 */
public class SNPanelGlassPane extends JPanel {
  private JPanel pnlContenu = new JPanel();
  private Component glassPane = null;
  private boolean isPourContenu = false;
  private OverlayLayout layout = new OverlayLayout(SNPanelGlassPane.this);

  /**
   * Constructeur avec un simple JPanel comme glasspane.
   */
  public SNPanelGlassPane() {
    this(new JPanel());
  }

  /**
   * Constructeur dans lequel on précise le composant à utiliser comme glaspane.
   * @param pGlassPane Composant à utiliser comme glasspane.
   */
  public SNPanelGlassPane(Component pGlassPane) {
    super();

    // Les opérations suivantes sont pour le composant lui-même et pas pour le contenu
    isPourContenu = false;

    // Renseigner le glasspane
    glassPane = pGlassPane;

    // Ajouter le layout permettant de gérer deux couches de composant, le contenu et le glasspan
    setLayout(layout);
    add(glassPane, 0);
    add(pnlContenu, 1);

    // Ne pas rendre le contenu opaque
    pnlContenu.setOpaque(false);

    // Masquer le glasspane pour l'instant. Au début, on veut que les entréer de l'utilisateur arrivent au contenu.
    glassPane.setVisible(false);

    // Les opérations suivantes sont pour le contenu
    isPourContenu = true;
  }

  /**
   * Retourner false car les composants de ce panel se superposent et il ne faut pas que le JDK optimise l'affichage dans ce cas.
   */
  @Override
  public boolean isOptimizedDrawingEnabled() {
    return false;
  }

  /**
   * Version avec gestion de l'aiguillage entre le composant contenu et ce composant lui-même.
   */
  @Override
  public void setLayout(LayoutManager manager) {
    synchronized (this) {
      if (isPourContenu) {
        pnlContenu.setLayout(manager);
      }
      else {
        super.setLayout(manager);
      }
    }
  }

  /**
   * Version avec gestion de l'aiguillage entre le composant contenu et ce composant lui-même.
   */
  @Override
  protected void addImpl(Component comp, Object constraints, int index) {
    synchronized (this) {
      if (isPourContenu) {
        pnlContenu.add(comp, constraints, index);
      }
      else {
        super.addImpl(comp, constraints, index);
      }
    }
  }

  /**
   * Version avec gestion de l'aiguillage entre le composant contenu et ce composant lui-même.
   */
  @Override
  public void remove(Component comp) {
    synchronized (this) {
      if (isPourContenu) {
        pnlContenu.remove(comp);
      }
      else {
        super.remove(comp);
      }
    }
  }

  /**
   * Glasspane actuel (null sdi aucun).
   */
  public Component getGlassPane() {
    return glassPane;
  }

  /**
   * Définir la classe qui tiendra la rôle de GlassPane (null pour aucun).
   * @param pNouveauGlassPane Nouveau glasspane, null pour aucun.
   */
  public void setGlassPane(Component pNouveauGlassPane) {
    synchronized (this) {
      // Les opérations suivantes sont pour le composant lui-même et pas pour le contenu
      isPourContenu = false;
      
      // Supprimer le glasspane précédent s'il y en avait un
      boolean visibilite = false;
      if (glassPane != null) {
        visibilite = glassPane.isVisible();
        remove(glassPane);
      }

      // Tester s'il y a un nouveau glasspane ou non
      if (pNouveauGlassPane != null) {
        // Mémoriser le nouveau glasspane
        glassPane = pNouveauGlassPane;

        // Définir la visibilité du nouveau glasspane identique à celle du glasspane précédent
        // S'il n'y avait pas de glasspane précédent, le nouveau glasspane est désactivé par défaut
        glassPane.setVisible(visibilite);

        // Ajouter le nouveau glasspane sur la première couche
        add(glassPane, 0);
      }
      else {
        // Pas de nouveau glasspane
        glassPane = null;
      }

      // Les opérations suivantes sont pour le contenu
      isPourContenu = true;
    }
  }
}
