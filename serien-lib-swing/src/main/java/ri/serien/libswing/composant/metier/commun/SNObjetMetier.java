/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.commun;

import ri.serien.libcommun.commun.objetmetier.IdObjetMetier;
import ri.serien.libcommun.commun.objetmetier.ListeObjetMetier;
import ri.serien.libcommun.commun.objetmetier.ObjetMetier;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection des objets métier.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un objet métier dans le logiciel. Il gère l'affichage des groupes
 * existants et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNObjetMetier à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant.
 * - Utiliser charger() pour charger les données. Les données n'ont pas changées si la configuration n'a pas changé.
 * Il est possible de forcer le rechargement via le paramètre pForcerRafraichissement.
 *
 * Voir un exemple d'implémentation [EXPB1] Exploitation -> Imports et exports -> Exports
 */
public class SNObjetMetier extends SNComboBoxObjetMetier<IdObjetMetier, ObjetMetier, ListeObjetMetier> {

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur
   */
  public SNObjetMetier() {
    super();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Sélectionner un objet métier de la liste déroulante à partir des champs RPG.
   * Méthode non implémentée.
   * @param pLexical Le lexique.
   * @param pNomChampRPG Le nom du champ RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pNomChampRPG) {
    throw new MessageErreurException("La méthode n'est pas implémentée.");
  }

  /**
   * Renseigner le champ RPG correspondant à l'objet métier sélectionné.
   * Méthode non implémentée.
   * @param pLexical Le lexique.
   * @param pNomChampRPG Le nom du champ RPG.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pNomChampRPG) {
    throw new MessageErreurException("La méthode n'est pas implémentée.");
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeObjetMetier());
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
}
