/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.fournisseur.snadressefournisseur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.EnumTypeAdresseFournisseur;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue du composant de sélection d'une adresse fournisseur.
 */
public class VueAdresseFournisseur extends AbstractVueDialogue<ModeleAdresseFournisseur> {
  // Constantes
  public static final String[] TITRE_LISTE = new String[] { "Code", "Nom ou raison sociale", "CP", "Ville", "Information" };
  public static final String RECHERCHE_GENERIQUE_TOOLTIP =
      "<html>Recherche sur :<br>\r\n" + "- numéro fournisseur (entier)<br>\r\n" + "- nom ou raison sociale (milieu)<br>\r\n"
          + "- complément de nom (milieu)<br>\r\n" + "- code postal (début)<br>\r\n" + "- ville (milieu)<br>\r\n" + "</html>";

  // Variables
  private int derniereLigneSelectionnee = 0;

  /**
   * Constructeur.
   */
  public VueAdresseFournisseur(ModeleAdresseFournisseur pModele) {
    super(pModele);
  }

  // -- Méthodes publiques

  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setResizable(false);

    // Configurer le composant de saisie de texte
    tfRechercheGenerique.setToolTipText(VueAdresseFournisseur.RECHERCHE_GENERIQUE_TOOLTIP);

    // Configurer le tableau
    int[] justification = new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.CENTRE };
    int[] dimension = new int[] { 110, 340, 60, 250, 250 };
    tblListeFournisseur.personnaliserAspect(TITRE_LISTE, dimension, justification, 13);
    tblListeFournisseur.personnaliserAspectCellules(new JTableAdresseFournisseurRenderer(dimension, justification));

    // Redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        tblListeFournisseurEnterKey();
      }
    };
    tblListeFournisseur.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);

    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeFournisseur.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeFournisseurSelectionChanged(e);
      }
    });

    // Ajout un listener pour détecter les changements d'affichage de la liste
    JViewport viewportDevis = scpListeFournisseur.getViewport();
    viewportDevis.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeFournisseurStateChanged(e);
      }
    });
    scpListeFournisseur.setViewport(viewportDevis);
    scpListeFournisseur.getViewport().setBackground(Color.WHITE);

    // Configurer la barre de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });

    // Types d'adresses fournisseurs.
    // Si on est dans un contexte de saisie d'un document d'achat, seules les adresses de type siège et de type commande sont disponibles.
    if (cbTypeAdresse.getItemCount() == 0) {
      cbTypeAdresse.addItem("Tous types");
      if (getModele().getCritereFournisseur().getTypeRecherche() == CritereFournisseur.RECHERCHER_ADRESSE_FOURNISSEURS_DOCUMENTACHAT) {
        cbTypeAdresse.addItem(EnumTypeAdresseFournisseur.SIEGE);
        cbTypeAdresse.addItem(EnumTypeAdresseFournisseur.COMMANDE);
      }
      else {
        for (EnumTypeAdresseFournisseur typeAdresseFournisseur : EnumTypeAdresseFournisseur.values()) {
          cbTypeAdresse.addItem(typeAdresseFournisseur);
        }
      }
    }

    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });

  }

  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirMessageInformation();
    rafraichirRechercheGenerique();
    rafraichirTypeAdresse();
    rafraichirTitreListe();
    rafraichirListe();
    rafraichirBoutonValider();

    if (getModele().getAdresseFournisseurSelectionnee() == null) {
      tfRechercheGenerique.requestFocusInWindow();
    }
  }

  // -- Méthodes privées

  private void rafraichirMessageInformation() {
    if (getModele().getMessageInformation() != null) {
      lbMessageInformation.setText(getModele().getMessageInformation());
      lbMessageInformation.setVisible(true);
    }
    else {
      lbMessageInformation.setText("");
      lbMessageInformation.setVisible(false);
    }
  }

  private void rafraichirRechercheGenerique() {
    if (getModele().getCritereFournisseur() != null && getModele().getCritereFournisseur().getTexteRechercheGenerique() != null) {
      tfRechercheGenerique.setText(getModele().getCritereFournisseur().getTexteRechercheGenerique());
    }
    else {
      tfRechercheGenerique.setText("");
    }

    tfRechercheGenerique.setEnabled(isDonneesChargees());
  }

  private void rafraichirTypeAdresse() {
    if (getModele().getTypeAdresse() != null) {
      cbTypeAdresse.setSelectedItem(getModele().getTypeAdresse());
    }
    else {
      cbTypeAdresse.setSelectedIndex(0);
    }
  }

  private void rafraichirTitreListe() {
    lbTitreListe.setMessage(getModele().getTitreListe());
  }

  private void rafraichirListe() {
    String[][] donnees = null;

    // Préparer les données pour le tableau
    List<AdresseFournisseur> listeAdresseFournisseur = getModele().getListeAdresseFournisseur();
    if (listeAdresseFournisseur != null && !listeAdresseFournisseur.isEmpty()) {
      donnees = new String[listeAdresseFournisseur.size()][VueAdresseFournisseur.TITRE_LISTE.length];
      for (int ligne = 0; ligne < listeAdresseFournisseur.size(); ligne++) {
        AdresseFournisseur adresseFournisseur = listeAdresseFournisseur.get(ligne);
        if (adresseFournisseur != null) {
          donnees[ligne][0] = adresseFournisseur.getId().toString();

          // Rafraichir le nom
          if (adresseFournisseur.getAdresse() != null && adresseFournisseur.getAdresse().getNom() != null
              && adresseFournisseur.getAdresse().getComplementNom() != null) {
            donnees[ligne][1] = adresseFournisseur.getAdresse().getNom() + " " + adresseFournisseur.getAdresse().getComplementNom();
          }
          else {
            donnees[ligne][1] = "";
          }

          // Rafraichir l'adresse
          if (adresseFournisseur.getAdresse() != null && adresseFournisseur.getAdresse().getCodePostalFormate() != null) {
            donnees[ligne][2] = adresseFournisseur.getAdresse().getCodePostalFormate();
          }
          else {
            donnees[ligne][2] = "";
          }

          // Rafraichir la ville
          if (adresseFournisseur.getAdresse() != null && adresseFournisseur.getAdresse().getVille() != null) {
            donnees[ligne][3] = adresseFournisseur.getAdresse().getVille();
          }
          else {
            donnees[ligne][3] = "";
          }

          // Rafraichir le type d'adresse
          if (adresseFournisseur.getAdresse() != null && adresseFournisseur.getType() != null) {
            donnees[ligne][4] = adresseFournisseur.getType().getLibelle();
          }
          else {
            donnees[ligne][4] = "";
          }
        }
      }
    }
    else {
      scpListeFournisseur.getVerticalScrollBar().setValue(0);
    }

    // Metytre à jour le tableau
    tblListeFournisseur.mettreAJourDonnees(donnees);

    // Sélectionner la ligne en cours
    int indice = getModele().getLigneSelectionnee();
    if (indice > -1 && indice < tblListeFournisseur.getRowCount()) {
      tblListeFournisseur.setRowSelectionInterval(indice, indice);
    }
    else {
      tblListeFournisseur.clearSelection();
    }
  }

  private void rafraichirBoutonValider() {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, getModele().isSelectionValide());
  }

  // -- Méthodes évènementielles

  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().rechercher();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfRechercheGeneriqueFocusGained(FocusEvent e) {
    try {
      tfRechercheGenerique.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfRechercheGeneriqueFocusLost(FocusEvent e) {
    try {
      getModele().modifierRechercheGenerique(tfRechercheGenerique.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfRechercheFournisseurActionPerformed(ActionEvent e) {
    try {
      getModele().modifierRechercheGenerique(tfRechercheGenerique.getText());
      getModele().rechercher();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfRechercheFournisseurKeyReleased(KeyEvent e) {
    try {
      if (e.getKeyCode() == SNCharteGraphique.TOUCHE_BAS.getKeyCode() || e.getKeyCode() == SNCharteGraphique.TOUCHE_DROITE.getKeyCode()) {
        tblListeFournisseur.requestFocusInWindow();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void scpListeFournisseurStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeFournisseur == null) {
        return;
      }

      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeFournisseur.getViewport().getViewRect();
      int premiereLigne = tblListeFournisseur.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeFournisseur.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeFournisseur.getRowCount() - 1;
      }

      // Charges les articles concernés si besoin
      getModele().modifierPlageAdresseFournisseurAffiches(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * La ligne sélectionnée a changée dans le tableau résultat.
   */
  private void tblListeFournisseurSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }

      // Informer le modèle
      getModele().modifierLigneSelectionnee(tblListeFournisseur.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tblListeFournisseurMouseClicked(MouseEvent e) {
    try {
      getModele().modifierLigneSelectionnee(tblListeFournisseur.getIndiceSelection());
      if (e.getClickCount() == 2) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tblListeFournisseurKeyReleased(KeyEvent e) {
    try {
      if (derniereLigneSelectionnee == 0 && (e.getKeyCode() == SNCharteGraphique.TOUCHE_HAUT.getKeyCode()
          || e.getKeyCode() == SNCharteGraphique.TOUCHE_GAUCHE.getKeyCode())) {
        tfRechercheGenerique.requestFocusInWindow();
      }
      derniereLigneSelectionnee = tblListeFournisseur.getSelectedRow();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tblListeFournisseurEnterKey() {
    try {
      getModele().quitterAvecValidation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void cbTypeAdresseItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbTypeAdresse.getSelectedItem() instanceof EnumTypeAdresseFournisseur) {
        getModele().modifierTypeAdresseFournisseur((EnumTypeAdresseFournisseur) cbTypeAdresse.getSelectedItem());
      }
      else {
        getModele().modifierTypeAdresseFournisseur(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new SNPanelContenu();
    lbMessageInformation = new SNMessage();
    pnlRecherche = new SNPanelTitre();
    pnlRechercheGauche = new JPanel();
    lbTexteRecherche = new SNLabelChamp();
    tfRechercheGenerique = new SNTexte();
    lbTypeAdresse = new SNLabelChamp();
    cbTypeAdresse = new SNComboBox();
    pnlRechercheDroite = new JPanel();
    snBarreRecherche = new SNBarreRecherche();
    lbTitreListe = new SNLabelTitre();
    scpListeFournisseur = new JScrollPane();
    tblListeFournisseur = new NRiTable();
    snBarreBouton = new SNBarreBouton();

    // ======== this ========
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("S\u00e9lection d'une adresse fournisseur");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), null));
        pnlContenu.setPreferredSize(new Dimension(975, 500));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };

        // ---- lbMessageInformation ----
        lbMessageInformation.setName("lbMessageInformation");
        pnlContenu.add(lbMessageInformation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

        // ======== pnlRecherche ========
        {
          pnlRecherche.setOpaque(false);
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridLayout(1, 2, 5, 5));

          // ======== pnlRechercheGauche ========
          {
            pnlRechercheGauche.setOpaque(false);
            pnlRechercheGauche.setBorder(null);
            pnlRechercheGauche.setName("pnlRechercheGauche");
            pnlRechercheGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWidths = new int[] { 165, 300, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };

            // ---- lbTexteRecherche ----
            lbTexteRecherche.setText("Recherche g\u00e9n\u00e9rique");
            lbTexteRecherche.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTexteRecherche.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTexteRecherche.setPreferredSize(new Dimension(100, 30));
            lbTexteRecherche.setName("lbTexteRecherche");
            pnlRechercheGauche.add(lbTexteRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- tfRechercheGenerique ----
            tfRechercheGenerique.setMinimumSize(new Dimension(300, 30));
            tfRechercheGenerique.setPreferredSize(new Dimension(300, 30));
            tfRechercheGenerique.setName("tfRechercheGenerique");
            tfRechercheGenerique.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                tfRechercheFournisseurActionPerformed(e);
              }
            });
            tfRechercheGenerique.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                tfRechercheFournisseurKeyReleased(e);
              }
            });
            tfRechercheGenerique.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                tfRechercheGeneriqueFocusGained(e);
              }

              @Override
              public void focusLost(FocusEvent e) {
                tfRechercheGeneriqueFocusLost(e);
              }
            });
            pnlRechercheGauche.add(tfRechercheGenerique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            // ---- lbTypeAdresse ----
            lbTypeAdresse.setText("Type d'adresses");
            lbTypeAdresse.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTypeAdresse.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTypeAdresse.setPreferredSize(new Dimension(100, 30));
            lbTypeAdresse.setName("lbTypeAdresse");
            pnlRechercheGauche.add(lbTypeAdresse, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

            // ---- cbTypeAdresse ----
            cbTypeAdresse.setName("cbTypeAdresse");
            cbTypeAdresse.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeAdresseItemStateChanged(e);
              }
            });
            pnlRechercheGauche.add(cbTypeAdresse, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheGauche);

          // ======== pnlRechercheDroite ========
          {
            pnlRechercheDroite.setOpaque(false);
            pnlRechercheDroite.setName("pnlRechercheDroite");
            pnlRechercheDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };

            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlRechercheDroite.add(snBarreRecherche, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheDroite);
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ---- lbTitreListe ----
        lbTitreListe.setText("Liste des adresses fournisseurs");
        lbTitreListe.setImportanceMessage(EnumImportanceMessage.MOYEN);
        lbTitreListe.setName("lbTitreListe");
        pnlContenu.add(lbTitreListe, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ======== scpListeFournisseur ========
        {
          scpListeFournisseur.setName("scpListeFournisseur");

          // ---- tblListeFournisseur ----
          tblListeFournisseur.setShowVerticalLines(true);
          tblListeFournisseur.setShowHorizontalLines(true);
          tblListeFournisseur.setBackground(Color.white);
          tblListeFournisseur.setRowHeight(20);
          tblListeFournisseur.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeFournisseur.setSelectionBackground(new Color(57, 105, 138));
          tblListeFournisseur.setGridColor(new Color(204, 204, 204));
          tblListeFournisseur.setName("tblListeFournisseur");
          tblListeFournisseur.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeFournisseurMouseClicked(e);
            }
          });
          tblListeFournisseur.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tblListeFournisseurKeyReleased(e);
            }
          });
          scpListeFournisseur.setViewportView(tblListeFournisseur);
        }
        pnlContenu.add(scpListeFournisseur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);

      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNMessage lbMessageInformation;
  private SNPanelTitre pnlRecherche;
  private JPanel pnlRechercheGauche;
  private SNLabelChamp lbTexteRecherche;
  private SNTexte tfRechercheGenerique;
  private SNLabelChamp lbTypeAdresse;
  private SNComboBox cbTypeAdresse;
  private JPanel pnlRechercheDroite;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lbTitreListe;
  private JScrollPane scpListeFournisseur;
  private NRiTable tblListeFournisseur;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
