/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.boncour.ajoutboncour;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeBonCour;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Ajouter un bon de cour à un bon ou une facture d'enlèvement
 */
public class VueAjoutBonCour extends AbstractVueDialogue<ModeleAjoutBonCour> {

  /**
   * Constructeur.
   */
  public VueAjoutBonCour(ModeleAjoutBonCour pModele) {
    super(pModele);
    setBackground(SNCharteGraphique.COULEUR_FOND);
  }

  // -- Méthodes publiques

  @Override
  public void initialiserComposants() {
    initComponents();
    setTitle("Sélection d'un bon de cour");

    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  @Override
  public void rafraichir() {
    rafraichirMagasinier();
    rafraichirNumeroBon();
    rafraichirBoutonValider();
  }

  private void rafraichirMagasinier() {
    Vendeur magasinier = getModele().getMagasinierSelectionne();

    snVendeur.setSession(getModele().getSession());
    snVendeur.setIdEtablissement(getModele().getIdEtablissement());
    snVendeur.charger(false);
    snVendeur.setTousAutorise(true);
    if (magasinier != null) {
      snVendeur.setSelection(magasinier);
    }
    else {
      snVendeur.setSelection(null);
    }
  }

  private void rafraichirNumeroBon() {
    BonCour bonCourSelectionne = getModele().getBonCourSelectionne();
    ListeBonCour listeBonCour = getModele().getListeBonCour();

    cbNumeroBon.removeAllItems();

    if (listeBonCour == null) {
      return;
    }

    cbNumeroBon.addItem(null);
    for (BonCour bonCour : listeBonCour) {
      cbNumeroBon.addItem(bonCour);
    }
    cbNumeroBon.setSelectedItem(bonCourSelectionne);
  }

  private void rafraichirBoutonValider() {
    boolean actif = true;
    // Désactiver si aucun bon de cour n'a été sélectionné
    if (getModele().getBonCourSelectionne() == null) {
      actif = false;
    }

    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }

  // -- Méthodes interractives

  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void cbNumeroBonItemStateChanged(ItemEvent e) {
    try {
      if (isDonneesChargees() && isEvenementsActifs()) {
        if (cbNumeroBon.getSelectedItem() instanceof BonCour) {
          getModele().modifierBonCour((BonCour) cbNumeroBon.getSelectedItem());
        }
        else {
          getModele().modifierBonCour(null);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void snVendeurValueChanged(SNComposantEvent e) {
    try {
      if (isDonneesChargees() && isEvenementsActifs()) {
        getModele().modifierMagasinier(snVendeur.getSelection());
      }
      rafraichir();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbMagasinier = new SNLabelChamp();
    snVendeur = new SNVendeur();
    sNLabelChamp2 = new SNLabelChamp();
    cbNumeroBon = new SNComboBox();
    
    // ======== this ========
    setTitle("S\u00e9lection d'un bon de cour");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(640, 190));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setBackground(new Color(238, 238, 210));
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setOpaque(true);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbMagasinier ----
        lbMagasinier.setText("Magasinier");
        lbMagasinier.setName("lbMagasinier");
        pnlContenu.add(lbMagasinier, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snVendeur ----
        snVendeur.setName("snVendeur");
        snVendeur.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snVendeurValueChanged(e);
          }
        });
        pnlContenu.add(snVendeur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp2 ----
        sNLabelChamp2.setText("Num\u00e9ro de bon de cour");
        sNLabelChamp2.setMinimumSize(new Dimension(200, 30));
        sNLabelChamp2.setPreferredSize(new Dimension(200, 30));
        sNLabelChamp2.setMaximumSize(new Dimension(200, 30));
        sNLabelChamp2.setName("sNLabelChamp2");
        pnlContenu.add(sNLabelChamp2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- cbNumeroBon ----
        cbNumeroBon.setMinimumSize(new Dimension(150, 30));
        cbNumeroBon.setPreferredSize(new Dimension(150, 30));
        cbNumeroBon.setName("cbNumeroBon");
        cbNumeroBon.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbNumeroBonItemStateChanged(e);
          }
        });
        pnlContenu.add(cbNumeroBon, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNLabelChamp lbMagasinier;
  private SNVendeur snVendeur;
  private SNLabelChamp sNLabelChamp2;
  private SNComboBox cbNumeroBon;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
