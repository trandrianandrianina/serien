/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.barrebouton;

import java.awt.Dimension;

import ri.serien.libswing.composant.primitif.bouton.EnumBouton;

/**
 * Barre de boutons standard pour les zones de recherche.
 * La barre de bouton comporte deux boutons : un bouton "Initialiser recherche" et un bouton "Rechercher".
 */
public class SNBarreRecherche extends SNBarreBouton {
  private Dimension TAILLE = new Dimension(305, 65);
  
  /**
   * Constructeur par défaut.
   */
  public SNBarreRecherche() {
    super(true);
    setMinimumSize(TAILLE);
    setPreferredSize(TAILLE);
    setMaximumSize(TAILLE);
    ajouterBouton(EnumBouton.INITIALISER_RECHERCHE, true);
    ajouterBouton(EnumBouton.RECHERCHER, true);
  }
}
