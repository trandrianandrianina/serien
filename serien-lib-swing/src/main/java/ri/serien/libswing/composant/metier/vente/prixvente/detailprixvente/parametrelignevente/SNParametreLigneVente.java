/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametrelignevente;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.math.BigDecimal;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumModeTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceCoefficient;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.checkbox.SNCheckBoxReduit;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Ce composant permet d'afficher tous les paramètres d'une ligne de vente.
 */
public class SNParametreLigneVente extends SNPanelTitre {
  private static final String TITRE = "Ligne de vente";
  
  private ModeleDialogueDetailPrixVente modele = null;
  private boolean evenementsActifs = false;
  private ParametreLigneVente parametreLigneVente = null;
  private ParametreLigneVente parametreLigneVenteInitial = null;
  private FormulePrixVente formulePrixVente = null;
  
  /**
   * Constructeur.
   */
  public SNParametreLigneVente() {
    super();
    initComponents();

    // Renseigner la liste déroulante des provenances de la colonne tarif
    for (EnumProvenanceColonneTarif provenanceColonneTarif : EnumProvenanceColonneTarif.values()) {
      cbProvenanceColonneTarif.addItem(provenanceColonneTarif);
    }

    // Renseigner la liste déroulante des provenances du prix de base
    for (EnumProvenancePrixBase provenancePrixBase : EnumProvenancePrixBase.values()) {
      cbProvenancePrixBase.addItem(provenancePrixBase);
    }
    
    // Renseigner la liste déroulante des provenances des taux de remises
    for (EnumProvenanceTauxRemise provenanceTauxRemise : EnumProvenanceTauxRemise.values()) {
      cbProvenanceTauxRemise.addItem(provenanceTauxRemise);
    }

    // Renseigner la liste déroulante des modes d'application des taux de remises
    cbModeTauxRemise.addItem("Aucun");
    for (EnumModeTauxRemise modeTauxRemise : EnumModeTauxRemise.values()) {
      cbModeTauxRemise.addItem(modeTauxRemise);
    }
    
    // Renseigner la liste déroulante des assiettes des taux de remises
    cbAssietteTauxRemise.addItem("Aucun");
    for (EnumAssietteTauxRemise assietteTauxRemise : EnumAssietteTauxRemise.values()) {
      cbAssietteTauxRemise.addItem(assietteTauxRemise);
    }
    
    // Renseigner la liste déroulante des provenances du coefficient
    for (EnumProvenanceCoefficient provenanceCoefficient : EnumProvenanceCoefficient.values()) {
      cbProvenanceCoefficient.addItem(provenanceCoefficient);
    }
    
    // Renseigner la liste déroulante des provenances du prix net
    for (EnumProvenancePrixNet provenancePrixNet : EnumProvenancePrixNet.values()) {
      cbProvenancePrixNet.addItem(provenancePrixNet);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modèle associé au composant graphique.
   * @return Modèle de la boîte de dialogue.
   */
  public ModeleDialogueDetailPrixVente getModele() {
    return modele;
  }
  
  /**
   * Modifier le modèle associé au composant graphique.
   * @param pModele Modèle de la boîte de dialogue.
   */
  public void setModele(ModeleDialogueDetailPrixVente pModele) {
    modele = pModele;
  }
  
  /**
   * Indiquer si les évènements doivent être traités.
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraichir des données du composant.
   */
  public void rafraichir() {
    // Empêcher les évènements pendant le rafraichissement
    evenementsActifs = false;
    
    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    parametreLigneVente = getModele().getParametreLigneVente();
    parametreLigneVenteInitial = getModele().getParametreLigneVenteInitial();
    formulePrixVente = modele.getFormulePrixVente();
    
    // Configurer tous les composants SNMontant avec la précision souhaitée
    int nombreDecimale = ArrondiPrix.DECIMALE_STANDARD;
    if (formulePrixVente != null && formulePrixVente.getArrondiPrix() != null) {
      nombreDecimale = formulePrixVente.getArrondiPrix().getNombreDecimale();
    }
    tfPrixBaseHT.setLongueurPartieDecimale(nombreDecimale);
    tfPrixNetHT.setLongueurPartieDecimale(nombreDecimale);
    
    // Rafraichir les composants
    rafraichirTitre();
    rafraichirOriginePrixVente();
    rafraichirPrixGaranti();
    rafraichirNumeroTVAEntete();
    rafraichirNumeroTVAEtablissement();
    rafraichirUniteVente();
    rafraichirProvenanceColonneTarif();
    rafraichirColonneTarif();
    rafraichirProvenancePrixBase();
    rafraichirPrixBaseHT();
    rafraichirPrixBaseTTC();
    rafraichirProvenanceTauxRemise();
    rafraichirLibelleTauxRemise();
    rafraichirTauxRemise1();
    rafraichirTauxRemise2();
    rafraichirTauxRemise3();
    rafraichirTauxRemise4();
    rafraichirTauxRemise5();
    rafraichirTauxRemise6();
    rafraichirModeTauxRemise();
    rafraichirAssietteTauxRemise();
    rafraichirProvenanceCoefficient();
    rafraichirCoefficient();
    rafraichirTypeGratuit();
    rafraichirProvenancePrixNet();
    rafraichirPrixNetHT();
    rafraichirPrixNetTTC();
    rafraichirQuantiteUV();
    
    // Réactiver les évènements pendant le rafraichissement
    evenementsActifs = true;
  }
  
  // -- Paramètres de l'entête
  
  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirTitre() {
    if (parametreLigneVente != null && parametreLigneVente.getIdLigneVente() != null) {
      setTitre(TITRE + " " + parametreLigneVente.getIdLigneVente());
    }
    else {
      setTitre(TITRE);
    }
  }
  
  /**
   * Rafraichir l'origine du prix de vente.
   */
  private void rafraichirOriginePrixVente() {
    if (parametreLigneVente != null && parametreLigneVente.getOriginePrixVente() != null) {
      tfOriginePrixVente.setText(parametreLigneVente.getOriginePrixVente().getLibelle());
    }
    else {
      tfOriginePrixVente.setText("");
    }
  }

  /**
   * Rafraichir si le prix est garanti.
   */
  private void rafraichirPrixGaranti() {
    if (parametreLigneVente != null && parametreLigneVente.isPrixGaranti()) {
      cbPrixGaranti.setSelected(true);
    }
    else {
      cbPrixGaranti.setSelected(false);
    }
  }
  
  /**
   * Rafraichir le numéro de TVA issu de l'entête.
   * La liste contient "Aucun, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9" pour être capable d'afficher les bizzareries non possibles mais existantes.
   */
  private void rafraichirNumeroTVAEntete() {
    if (parametreLigneVente != null && parametreLigneVente.getNumeroTVAEntete() != null && parametreLigneVente.getNumeroTVAEntete() >= 0
        && parametreLigneVente.getNumeroTVAEntete() <= 9) {
      cbNumeroTVAEntete.setSelectedIndex(parametreLigneVente.getNumeroTVAEntete() + 1);
    }
    else {
      cbNumeroTVAEntete.setSelectedIndex(0);
    }
    
    cbNumeroTVAEntete.setEnabled(modele != null && getModele().isModeSimulation());
    lbNumeroTVAEntete.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getNumeroTVAEntete(), parametreLigneVenteInitial.getNumeroTVAEntete()));
  }
  
  /**
   * Rafraichir le numéro de TVA issu de l'établissement.
   * La liste contient "Aucun, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9" pour être capable d'afficher les bizzareries non possibles mais existantes.
   */
  private void rafraichirNumeroTVAEtablissement() {
    if (parametreLigneVente != null && parametreLigneVente.getNumeroTVAEtablissement() != null
        && parametreLigneVente.getNumeroTVAEtablissement() >= 0 && parametreLigneVente.getNumeroTVAEtablissement() <= 9) {
      cbNumeroTVAEtablissement.setSelectedIndex(parametreLigneVente.getNumeroTVAEtablissement() + 1);
    }
    else {
      cbNumeroTVAEtablissement.setSelectedIndex(0);
    }
    
    cbNumeroTVAEtablissement.setEnabled(modele != null && getModele().isModeSimulation());
    lbNumeroTVAEtablissement.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getNumeroTVAEtablissement(), parametreLigneVenteInitial.getNumeroTVAEtablissement()));
  }
  
  /**
   * Rafraichir l'unité de vente de la ligne de vente.
   */
  private void rafraichirUniteVente() {
    if (parametreLigneVente != null && parametreLigneVente.getCodeUniteVente() != null) {
      tfUniteVente.setText(parametreLigneVente.getCodeUniteVente());
    }
    else {
      tfUniteVente.setText("");
    }
    
    tfUniteVente.setEnabled(modele != null && getModele().isModeSimulation());
    lbUniteVente.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getCodeUniteVente(), parametreLigneVenteInitial.getCodeUniteVente()));
  }
  
  /**
   * Rafraichir la provenance de la colonne tarif de la ligne.
   */
  private void rafraichirProvenanceColonneTarif() {
    if (parametreLigneVente != null && parametreLigneVente.getProvenanceColonneTarif() != null) {
      cbProvenanceColonneTarif.setSelectedItem(parametreLigneVente.getProvenanceColonneTarif());
    }
    else {
      cbProvenanceColonneTarif.setSelectedItem(0);
    }
    
    cbProvenanceColonneTarif.setEnabled(modele != null && getModele().isModeSimulation());
    lbProvenanceColonneTarif.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getProvenanceColonneTarif(), parametreLigneVenteInitial.getProvenanceColonneTarif()));
  }
  
  /**
   * Rafraichir la colonne tarif de la ligne.
   */
  private void rafraichirColonneTarif() {
    if (parametreLigneVente != null && parametreLigneVente.getNumeroColonneTarif() != null) {
      Integer numeroColonneTarif = parametreLigneVente.getNumeroColonneTarif();
      if (numeroColonneTarif >= 1 && numeroColonneTarif <= 10) {
        cbColonneTarif.setSelectedIndex(numeroColonneTarif);
      }
      else {
        cbColonneTarif.setSelectedIndex(0);
      }
    }
    else {
      cbColonneTarif.setSelectedIndex(0);
    }
    
    cbColonneTarif.setEnabled(modele != null && getModele().isModeSimulation());
    lbColonneTarif.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getNumeroColonneTarif(), parametreLigneVenteInitial.getNumeroColonneTarif()));
  }
  
  /**
   * Rafraichir la provenance du prix de base (HT et TTC) de la ligne de vente.
   */
  private void rafraichirProvenancePrixBase() {
    if (parametreLigneVente != null && parametreLigneVente.getProvenancePrixBase() != null) {
      cbProvenancePrixBase.setSelectedItem(parametreLigneVente.getProvenancePrixBase());
    }
    else {
      cbProvenancePrixBase.setSelectedItem(0);
    }
    
    cbProvenancePrixBase.setEnabled(modele != null && getModele().isModeSimulation());
    lbProvenancePrixBase.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getProvenancePrixBase(), parametreLigneVenteInitial.getProvenancePrixBase()));
  }
  
  /**
   * Rafraichir le prix de base HT de la ligne de vente.
   */
  private void rafraichirPrixBaseHT() {
    if (parametreLigneVente != null && parametreLigneVente.getPrixBaseHT() != null) {
      tfPrixBaseHT.setMontant(parametreLigneVente.getPrixBaseHT());
    }
    else {
      tfPrixBaseHT.setMontant("");
    }
    
    tfPrixBaseHT.setEnabled(modele != null && getModele().isModeSimulation());
    lbPrixBaseHT.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getPrixBaseHT(), parametreLigneVenteInitial.getPrixBaseHT()));
  }
  
  /**
   * Rafraichir le prix de base TTC de la ligne de vente.
   */
  private void rafraichirPrixBaseTTC() {
    if (parametreLigneVente != null && parametreLigneVente.getPrixBaseTTC() != null) {
      tfPrixBaseTTC.setMontant(parametreLigneVente.getPrixBaseTTC());
    }
    else {
      tfPrixBaseTTC.setMontant("");
    }
    
    tfPrixBaseTTC.setEnabled(modele != null && getModele().isModeSimulation());
    lbPrixBaseTTC.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getPrixBaseTTC(), parametreLigneVenteInitial.getPrixBaseTTC()));
  }

  /**
   * Rafraichir la provenance des taux de remises de la ligne de vente.
   */
  private void rafraichirProvenanceTauxRemise() {
    if (parametreLigneVente != null && parametreLigneVente.getProvenanceTauxRemise() != null) {
      cbProvenanceTauxRemise.setSelectedItem(parametreLigneVente.getProvenanceTauxRemise());
    }
    else {
      cbProvenanceTauxRemise.setSelectedItem(0);
    }

    cbProvenanceTauxRemise.setEnabled(modele != null && getModele().isModeSimulation());
    lbProvenanceTauxRemise.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getProvenanceTauxRemise(), parametreLigneVenteInitial.getProvenanceTauxRemise()));
  }

  /**
   * Rafraichir le libellé du taux de remise.
   */
  private void rafraichirLibelleTauxRemise() {
    lbTauxRemise.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && (!Constantes.equals(parametreLigneVente.getTauxRemise1(), parametreLigneVenteInitial.getTauxRemise1())
            || !Constantes.equals(parametreLigneVente.getTauxRemise2(), parametreLigneVenteInitial.getTauxRemise2())
            || !Constantes.equals(parametreLigneVente.getTauxRemise3(), parametreLigneVenteInitial.getTauxRemise3())
            || !Constantes.equals(parametreLigneVente.getTauxRemise4(), parametreLigneVenteInitial.getTauxRemise4())
            || !Constantes.equals(parametreLigneVente.getTauxRemise5(), parametreLigneVenteInitial.getTauxRemise5())
            || !Constantes.equals(parametreLigneVente.getTauxRemise6(), parametreLigneVenteInitial.getTauxRemise6())));
  }
  
  /**
   * Rafraichir le taux de remise 1 de la ligne de vente.
   */
  private void rafraichirTauxRemise1() {
    if (parametreLigneVente != null && parametreLigneVente.getTauxRemise1() != null) {
      tfTauxRemise1.setText(parametreLigneVente.getTauxRemise1().getTexteSansSymbole());
    }
    else {
      tfTauxRemise1.setText("");
    }
    
    tfTauxRemise1.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 2 de la ligne de vente.
   */
  private void rafraichirTauxRemise2() {
    if (parametreLigneVente != null && parametreLigneVente.getTauxRemise2() != null) {
      tfTauxRemise2.setText(parametreLigneVente.getTauxRemise2().getTexteSansSymbole());
    }
    else {
      tfTauxRemise2.setText("");
    }
    
    tfTauxRemise2.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 3 de la ligne de vente.
   */
  private void rafraichirTauxRemise3() {
    if (parametreLigneVente != null && parametreLigneVente.getTauxRemise3() != null) {
      tfTauxRemise3.setText(parametreLigneVente.getTauxRemise3().getTexteSansSymbole());
    }
    else {
      tfTauxRemise3.setText("");
    }
    
    tfTauxRemise3.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 4 de la ligne de vente.
   */
  private void rafraichirTauxRemise4() {
    if (parametreLigneVente != null && parametreLigneVente.getTauxRemise4() != null) {
      tfTauxRemise4.setText(parametreLigneVente.getTauxRemise4().getTexteSansSymbole());
    }
    else {
      tfTauxRemise4.setText("");
    }
    
    tfTauxRemise4.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 5 de la ligne de vente.
   */
  private void rafraichirTauxRemise5() {
    if (parametreLigneVente != null && parametreLigneVente.getTauxRemise5() != null) {
      tfTauxRemise5.setText(parametreLigneVente.getTauxRemise5().getTexteSansSymbole());
    }
    else {
      tfTauxRemise5.setText("");
    }
    
    tfTauxRemise5.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 6 de la ligne de vente.
   */
  private void rafraichirTauxRemise6() {
    if (parametreLigneVente != null && parametreLigneVente.getTauxRemise6() != null) {
      tfTauxRemise6.setText(parametreLigneVente.getTauxRemise6().getTexteSansSymbole());
    }
    else {
      tfTauxRemise6.setText("");
    }
    
    tfTauxRemise6.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le mode d'application des taux de remises de la ligne de vente.
   */
  private void rafraichirModeTauxRemise() {
    if (parametreLigneVente != null && parametreLigneVente.getModeTauxRemise() != null) {
      cbModeTauxRemise.setSelectedItem(parametreLigneVente.getModeTauxRemise());
    }
    else {
      cbModeTauxRemise.setSelectedIndex(0);
    }
    
    cbModeTauxRemise.setEnabled(modele != null && modele.isModeSimulation());
    lbModeTauxRemise.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getModeTauxRemise(), parametreLigneVenteInitial.getModeTauxRemise()));
  }
  
  /**
   * Rafraichir l'assiette des taux de remises.
   */
  private void rafraichirAssietteTauxRemise() {
    if (parametreLigneVente != null && parametreLigneVente.getAssietteTauxRemise() != null) {
      cbAssietteTauxRemise.setSelectedItem(parametreLigneVente.getAssietteTauxRemise());
    }
    else {
      cbAssietteTauxRemise.setSelectedIndex(0);
    }
    
    cbAssietteTauxRemise.setEnabled(modele != null && modele.isModeSimulation());
    lbAssietteTauxRemise.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getAssietteTauxRemise(), parametreLigneVenteInitial.getAssietteTauxRemise()));
  }
  
  /**
   * Rafraichir la provenance du coefficient de la ligne de vente.
   */
  private void rafraichirProvenanceCoefficient() {
    if (parametreLigneVente != null && parametreLigneVente.getProvenanceCoefficient() != null) {
      cbProvenanceCoefficient.setSelectedItem(parametreLigneVente.getProvenanceCoefficient());
    }
    else {
      cbProvenanceCoefficient.setSelectedItem(0);
    }
    
    cbProvenanceCoefficient.setEnabled(modele != null && modele.isModeSimulation());
    lbProvenanceCoefficient.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getProvenanceCoefficient(), parametreLigneVenteInitial.getProvenanceCoefficient()));
  }
  
  /**
   * Rafraichir le coefficient de la ligne de vente.
   */
  private void rafraichirCoefficient() {
    if (parametreLigneVente != null && parametreLigneVente.getCoefficient() != null
        && parametreLigneVente.getCoefficient().compareTo(BigDecimal.ZERO) != 0) {
      tfCoefficient.setMontant(parametreLigneVente.getCoefficient());
    }
    else {
      tfCoefficient.setMontant("");
    }
    
    tfCoefficient.setEnabled(modele != null && modele.isModeSimulation());
    lbCoefficient.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getCoefficient(), parametreLigneVenteInitial.getCoefficient()));
  }
  
  /**
   * Rafraichir le type de gratuit de la ligne de vente.
   */
  private void rafraichirTypeGratuit() {
    if (parametreLigneVente != null && parametreLigneVente.getIdTypeGratuit() != null) {
      tfTypeGratuit.setText("" + parametreLigneVente.getIdTypeGratuit());
    }
    else {
      tfTypeGratuit.setText("");
    }
    
    tfTypeGratuit.setEnabled(modele != null && modele.isModeSimulation());
    lbTypeGratuit.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getIdTypeGratuit(), parametreLigneVenteInitial.getIdTypeGratuit()));
  }
  
  /**
   * Rafraichir la provenance du prix net de la ligne de vente.
   */
  private void rafraichirProvenancePrixNet() {
    if (parametreLigneVente != null && parametreLigneVente.getProvenancePrixNet() != null) {
      cbProvenancePrixNet.setSelectedItem(parametreLigneVente.getProvenancePrixNet());
    }
    else {
      cbProvenancePrixNet.setSelectedItem(null);
    }
    
    cbProvenancePrixNet.setEnabled(modele != null && modele.isModeSimulation());
    lbProvenancePrixNet.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getProvenancePrixNet(), parametreLigneVenteInitial.getProvenancePrixNet()));
  }
  
  /**
   * Rafraichir le prix net HT de la ligne de vente.
   */
  private void rafraichirPrixNetHT() {
    if (parametreLigneVente != null && parametreLigneVente.getPrixNetHT() != null) {
      tfPrixNetHT.setMontant(parametreLigneVente.getPrixNetHT());
    }
    else {
      tfPrixNetHT.setMontant("");
    }
    
    tfPrixNetHT.setEnabled(modele != null && modele.isModeSimulation());
    lbPrixNetHT.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getPrixNetHT(), parametreLigneVenteInitial.getPrixNetHT()));
  }
  
  /**
   * Rafraichir le prix net TTC de la ligne de vente.
   */
  private void rafraichirPrixNetTTC() {
    if (parametreLigneVente != null && parametreLigneVente.getPrixNetTTC() != null) {
      tfPrixNetTTC.setMontant(parametreLigneVente.getPrixNetTTC());
    }
    else {
      tfPrixNetTTC.setMontant("");
    }
    
    tfPrixNetTTC.setEnabled(modele != null && modele.isModeSimulation());
    lbPrixNetTTC.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getPrixNetTTC(), parametreLigneVenteInitial.getPrixNetTTC()));
  }

  /**
   * Rafraichir la quantité en UV de la ligne de vente.
   */
  private void rafraichirQuantiteUV() {
    tfQuantiteUV.setNegatifAutorise(true);

    if (parametreLigneVente != null && parametreLigneVente.getQuantiteUV() != null) {
      tfQuantiteUV.setMontant(parametreLigneVente.getQuantiteUV());
    }
    else {
      tfQuantiteUV.setMontant("");
    }
    
    tfQuantiteUV.setEnabled(modele != null && getModele().isModeSimulation());
    lbQuantiteUV.setImportance(parametreLigneVente != null && parametreLigneVenteInitial != null
        && !Constantes.equals(parametreLigneVente.getQuantiteUV(), parametreLigneVenteInitial.getQuantiteUV()));
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void cbNumeroTVAEnteteItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbNumeroTVAEntete.getSelectedIndex() >= 1 && cbNumeroTVAEntete.getSelectedIndex() <= 10) {
        getModele().modifierNumeroTVAEnteteLigneVente(cbNumeroTVAEntete.getSelectedIndex() - 1);
      }
      else {
        getModele().modifierNumeroTVAEnteteLigneVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbNumeroTVAEtablissementItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbNumeroTVAEtablissement.getSelectedIndex() >= 1 && cbNumeroTVAEtablissement.getSelectedIndex() <= 10) {
        getModele().modifierNumeroTVAEtablissementLigneVente(cbNumeroTVAEtablissement.getSelectedIndex() - 1);
      }
      else {
        getModele().modifierNumeroTVAEtablissementLigneVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfUniteVenteFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierUniteVenteLigneVente(tfUniteVente.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbProvenanceColonneTarifItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbProvenanceColonneTarif.getSelectedItem() instanceof EnumProvenanceColonneTarif) {
        getModele().modifierProvenanceColonneTarifLigneVente((EnumProvenanceColonneTarif) cbProvenanceColonneTarif.getSelectedItem());
      }
      else {
        getModele().modifierProvenanceColonneTarifLigneVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbColonneTarifItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbColonneTarif.getSelectedIndex() >= 1 && cbColonneTarif.getSelectedIndex() <= 10) {
        getModele().modifierColonneTarifLigneVente(cbColonneTarif.getSelectedIndex());
      }
      else {
        getModele().modifierColonneTarifLigneVente(0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbProvenancePrixBaseItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbProvenancePrixBase.getSelectedItem() instanceof EnumProvenancePrixBase) {
        getModele().modifierProvenancePrixBaseLigneVente((EnumProvenancePrixBase) cbProvenancePrixBase.getSelectedItem());
      }
      else {
        getModele().modifierProvenancePrixBaseLigneVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfPrixBaseHTValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTLigneVente(tfPrixBaseHT.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfPrixBaseTTCValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseTTCLigneVente(tfPrixBaseTTC.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbProvenanceTauxRemiseItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbProvenanceTauxRemise.getSelectedItem() instanceof EnumProvenanceTauxRemise) {
        getModele().modifierProvenanceTauxRemiseLigneVente((EnumProvenanceTauxRemise) cbProvenanceTauxRemise.getSelectedItem());
      }
      else {
        getModele().modifierProvenanceTauxRemiseLigneVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfTauxRemise1FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseLigneVente(tfTauxRemise1.getText(), 1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise2FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseLigneVente(tfTauxRemise2.getText(), 2);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise3FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseLigneVente(tfTauxRemise3.getText(), 3);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise4FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseLigneVente(tfTauxRemise4.getText(), 4);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise5FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseLigneVente(tfTauxRemise5.getText(), 5);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise6FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseLigneVente(tfTauxRemise6.getText(), 6);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbModeApplicationTauxRemiseItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbModeTauxRemise.getSelectedItem() instanceof EnumModeTauxRemise) {
        getModele().modifierModeTauxRemiseLigneVente((EnumModeTauxRemise) cbModeTauxRemise.getSelectedItem());
      }
      else {
        getModele().modifierModeTauxRemiseLigneVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbAssietteTauxRemiseItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      Object selection = cbAssietteTauxRemise.getSelectedItem();
      if (selection instanceof EnumAssietteTauxRemise) {
        getModele().modifierAssietteTauxRemiseLigneVente((EnumAssietteTauxRemise) selection);
      }
      else {
        getModele().modifierAssietteTauxRemiseLigneVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbProvenanceCoefficientItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbProvenanceCoefficient.getSelectedItem() instanceof EnumProvenanceCoefficient) {
        getModele().modifierProvenanceCoefficientLigneVente((EnumProvenanceCoefficient) cbProvenanceCoefficient.getSelectedItem());
      }
      else {
        getModele().modifierProvenanceTauxRemiseLigneVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfCoefficientValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierCoefficientLigneVente(tfCoefficient.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTypeGratuitFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTypeGratuitLigneVente(tfTypeGratuit.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbProvenancePrixNetItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbProvenancePrixNet.getSelectedItem() instanceof EnumProvenancePrixNet) {
        getModele().modifierProvenancePrixNetLigneVente((EnumProvenancePrixNet) cbProvenancePrixNet.getSelectedItem());
      }
      else {
        getModele().modifierProvenancePrixNetLigneVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfPrixNetHTValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixNetHTLigneVente(tfPrixNetHT.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfPrixNetTTCValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixNetTTCLigneVente(tfPrixNetTTC.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void tfQuantiteUVValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierQuantiteUVLigneVente(tfQuantiteUV.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbOriginePrixVente = new SNLabelChamp();
    tfOriginePrixVente = new SNTexte();
    lbPrixGaranti = new SNLabelChamp();
    cbPrixGaranti = new SNCheckBoxReduit();
    lbNumeroTVAEntete = new SNLabelChamp();
    cbNumeroTVAEntete = new SNComboBoxReduit();
    lbNumeroTVAEtablissement = new SNLabelChamp();
    cbNumeroTVAEtablissement = new SNComboBoxReduit();
    lbUniteVente = new SNLabelChamp();
    tfUniteVente = new SNTexte();
    lbProvenanceColonneTarif = new SNLabelChamp();
    cbProvenanceColonneTarif = new SNComboBoxReduit();
    lbColonneTarif = new SNLabelChamp();
    cbColonneTarif = new SNComboBoxReduit();
    lbProvenancePrixBase = new SNLabelChamp();
    cbProvenancePrixBase = new SNComboBoxReduit();
    lbPrixBaseHT = new SNLabelChamp();
    tfPrixBaseHT = new SNMontant();
    lbPrixBaseTTC = new SNLabelChamp();
    tfPrixBaseTTC = new SNMontant();
    lbProvenanceTauxRemise = new SNLabelChamp();
    cbProvenanceTauxRemise = new SNComboBoxReduit();
    lbTauxRemise = new SNLabelChamp();
    pnlTauxRemise = new SNPanel();
    tfTauxRemise1 = new SNTexte();
    tfTauxRemise2 = new SNTexte();
    tfTauxRemise3 = new SNTexte();
    tfTauxRemise4 = new SNTexte();
    tfTauxRemise5 = new SNTexte();
    tfTauxRemise6 = new SNTexte();
    lbModeTauxRemise = new SNLabelChamp();
    cbModeTauxRemise = new SNComboBoxReduit();
    lbAssietteTauxRemise = new SNLabelChamp();
    cbAssietteTauxRemise = new SNComboBoxReduit();
    lbProvenanceCoefficient = new SNLabelChamp();
    cbProvenanceCoefficient = new SNComboBoxReduit();
    lbCoefficient = new SNLabelChamp();
    tfCoefficient = new SNMontant();
    lbTypeGratuit = new SNLabelChamp();
    tfTypeGratuit = new SNTexte();
    lbProvenancePrixNet = new SNLabelChamp();
    cbProvenancePrixNet = new SNComboBoxReduit();
    lbPrixNetHT = new SNLabelChamp();
    tfPrixNetHT = new SNMontant();
    lbPrixNetTTC = new SNLabelChamp();
    tfPrixNetTTC = new SNMontant();
    lbQuantiteUV = new SNLabelChamp();
    tfQuantiteUV = new SNMontant();
    
    // ======== this ========
    setModeReduit(true);
    setTitre("Ligne de vente");
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights =
        new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    
    // ---- lbOriginePrixVente ----
    lbOriginePrixVente.setModeReduit(true);
    lbOriginePrixVente.setText("Origine prix de vente");
    lbOriginePrixVente.setMinimumSize(new Dimension(30, 22));
    lbOriginePrixVente.setPreferredSize(new Dimension(30, 22));
    lbOriginePrixVente.setName("lbOriginePrixVente");
    add(lbOriginePrixVente,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOriginePrixVente ----
    tfOriginePrixVente.setEnabled(false);
    tfOriginePrixVente.setModeReduit(true);
    tfOriginePrixVente.setName("tfOriginePrixVente");
    add(tfOriginePrixVente,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixGaranti ----
    lbPrixGaranti.setModeReduit(true);
    lbPrixGaranti.setText("Prix garanti");
    lbPrixGaranti.setMinimumSize(new Dimension(30, 22));
    lbPrixGaranti.setPreferredSize(new Dimension(30, 22));
    lbPrixGaranti.setName("lbPrixGaranti");
    add(lbPrixGaranti,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbPrixGaranti ----
    cbPrixGaranti.setEnabled(false);
    cbPrixGaranti.setName("cbPrixGaranti");
    add(cbPrixGaranti,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbNumeroTVAEntete ----
    lbNumeroTVAEntete.setText("Num\u00e9ro TVA ent\u00eate");
    lbNumeroTVAEntete.setModeReduit(true);
    lbNumeroTVAEntete.setName("lbNumeroTVAEntete");
    add(lbNumeroTVAEntete,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbNumeroTVAEntete ----
    cbNumeroTVAEntete.setModel(new DefaultComboBoxModel<>(new String[] { "Aucun", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }));
    cbNumeroTVAEntete.setSelectedIndex(-1);
    cbNumeroTVAEntete.setEnabled(false);
    cbNumeroTVAEntete.setName("cbNumeroTVAEntete");
    cbNumeroTVAEntete.addItemListener(e -> cbNumeroTVAEnteteItemStateChanged(e));
    add(cbNumeroTVAEntete,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbNumeroTVAEtablissement ----
    lbNumeroTVAEtablissement.setText("Num\u00e9ro TVA \u00e9tablissement");
    lbNumeroTVAEtablissement.setModeReduit(true);
    lbNumeroTVAEtablissement.setName("lbNumeroTVAEtablissement");
    add(lbNumeroTVAEtablissement,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbNumeroTVAEtablissement ----
    cbNumeroTVAEtablissement
        .setModel(new DefaultComboBoxModel<>(new String[] { "Aucun ", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }));
    cbNumeroTVAEtablissement.setSelectedIndex(-1);
    cbNumeroTVAEtablissement.setEnabled(false);
    cbNumeroTVAEtablissement.setName("cbNumeroTVAEtablissement");
    cbNumeroTVAEtablissement.addItemListener(e -> cbNumeroTVAEtablissementItemStateChanged(e));
    add(cbNumeroTVAEtablissement,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbUniteVente ----
    lbUniteVente.setText("Unit\u00e9 de vente");
    lbUniteVente.setModeReduit(true);
    lbUniteVente.setName("lbUniteVente");
    add(lbUniteVente,
        new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfUniteVente ----
    tfUniteVente.setEnabled(false);
    tfUniteVente.setModeReduit(true);
    tfUniteVente.setName("tfUniteVente");
    tfUniteVente.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfUniteVenteFocusLost(e);
      }
    });
    add(tfUniteVente,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbProvenanceColonneTarif ----
    lbProvenanceColonneTarif.setModeReduit(true);
    lbProvenanceColonneTarif.setText("Provenance colonne tarif");
    lbProvenanceColonneTarif.setMinimumSize(new Dimension(30, 22));
    lbProvenanceColonneTarif.setPreferredSize(new Dimension(30, 22));
    lbProvenanceColonneTarif.setName("lbProvenanceColonneTarif");
    add(lbProvenanceColonneTarif,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbProvenanceColonneTarif ----
    cbProvenanceColonneTarif.setEnabled(false);
    cbProvenanceColonneTarif.setName("cbProvenanceColonneTarif");
    cbProvenanceColonneTarif.addItemListener(e -> {
      cbModeApplicationTauxRemiseItemStateChanged(e);
      cbProvenanceColonneTarifItemStateChanged(e);
    });
    add(cbProvenanceColonneTarif,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbColonneTarif ----
    lbColonneTarif.setModeReduit(true);
    lbColonneTarif.setText("Colonne tarif");
    lbColonneTarif.setMinimumSize(new Dimension(30, 22));
    lbColonneTarif.setPreferredSize(new Dimension(30, 22));
    lbColonneTarif.setName("lbColonneTarif");
    add(lbColonneTarif,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbColonneTarif ----
    cbColonneTarif.setModel(new DefaultComboBoxModel<>(new String[] { "Aucune", "Colonne 1", "Colonne 2", "Colonne 3", "Colonne 4",
        "Colonne 5", "Colonne 6", "Colonne 7", "Colonne 8", "Colonne 9", "Colonne 10" }));
    cbColonneTarif.setSelectedIndex(-1);
    cbColonneTarif.setEnabled(false);
    cbColonneTarif.setName("cbColonneTarif");
    cbColonneTarif.addItemListener(e -> cbColonneTarifItemStateChanged(e));
    add(cbColonneTarif,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbProvenancePrixBase ----
    lbProvenancePrixBase.setModeReduit(true);
    lbProvenancePrixBase.setText("Provenance prix de base");
    lbProvenancePrixBase.setMinimumSize(new Dimension(30, 22));
    lbProvenancePrixBase.setPreferredSize(new Dimension(30, 22));
    lbProvenancePrixBase.setName("lbProvenancePrixBase");
    add(lbProvenancePrixBase,
        new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbProvenancePrixBase ----
    cbProvenancePrixBase.setEnabled(false);
    cbProvenancePrixBase.setName("cbProvenancePrixBase");
    cbProvenancePrixBase.addItemListener(e -> {
      cbModeApplicationTauxRemiseItemStateChanged(e);
      cbProvenancePrixBaseItemStateChanged(e);
    });
    add(cbProvenancePrixBase,
        new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseHT ----
    lbPrixBaseHT.setModeReduit(true);
    lbPrixBaseHT.setText("Prix de base HT");
    lbPrixBaseHT.setMinimumSize(new Dimension(30, 22));
    lbPrixBaseHT.setPreferredSize(new Dimension(30, 22));
    lbPrixBaseHT.setName("lbPrixBaseHT");
    add(lbPrixBaseHT,
        new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseHT ----
    tfPrixBaseHT.setModeReduit(true);
    tfPrixBaseHT.setEnabled(false);
    tfPrixBaseHT.setName("tfPrixBaseHT");
    tfPrixBaseHT.addSNComposantListener(e -> tfPrixBaseHTValueChanged(e));
    add(tfPrixBaseHT,
        new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseTTC ----
    lbPrixBaseTTC.setModeReduit(true);
    lbPrixBaseTTC.setText("Prix de base TTC");
    lbPrixBaseTTC.setMinimumSize(new Dimension(30, 22));
    lbPrixBaseTTC.setPreferredSize(new Dimension(30, 22));
    lbPrixBaseTTC.setName("lbPrixBaseTTC");
    add(lbPrixBaseTTC,
        new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseTTC ----
    tfPrixBaseTTC.setModeReduit(true);
    tfPrixBaseTTC.setEnabled(false);
    tfPrixBaseTTC.setName("tfPrixBaseTTC");
    tfPrixBaseTTC.addSNComposantListener(e -> tfPrixBaseTTCValueChanged(e));
    add(tfPrixBaseTTC,
        new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbProvenanceTauxRemise ----
    lbProvenanceTauxRemise.setModeReduit(true);
    lbProvenanceTauxRemise.setText("Provenance taux remises");
    lbProvenanceTauxRemise.setMinimumSize(new Dimension(30, 22));
    lbProvenanceTauxRemise.setPreferredSize(new Dimension(30, 22));
    lbProvenanceTauxRemise.setName("lbProvenanceTauxRemise");
    add(lbProvenanceTauxRemise,
        new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbProvenanceTauxRemise ----
    cbProvenanceTauxRemise.setEnabled(false);
    cbProvenanceTauxRemise.setName("cbProvenanceTauxRemise");
    cbProvenanceTauxRemise.addItemListener(e -> {
      cbModeApplicationTauxRemiseItemStateChanged(e);
      cbProvenanceCoefficientItemStateChanged(e);
      cbProvenanceTauxRemiseItemStateChanged(e);
    });
    add(cbProvenanceTauxRemise,
        new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbTauxRemise ----
    lbTauxRemise.setModeReduit(true);
    lbTauxRemise.setText("Taux remises (%)");
    lbTauxRemise.setMinimumSize(new Dimension(30, 22));
    lbTauxRemise.setPreferredSize(new Dimension(30, 22));
    lbTauxRemise.setName("lbTauxRemise");
    add(lbTauxRemise, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 3), 0, 0));
    
    // ======== pnlTauxRemise ========
    {
      pnlTauxRemise.setName("pnlTauxRemise");
      pnlTauxRemise.setLayout(new GridLayout(2, 0, 3, 0));
      
      // ---- tfTauxRemise1 ----
      tfTauxRemise1.setEnabled(false);
      tfTauxRemise1.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise1.setModeReduit(true);
      tfTauxRemise1.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise1.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise1.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise1.setName("tfTauxRemise1");
      tfTauxRemise1.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise1FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise1);
      
      // ---- tfTauxRemise2 ----
      tfTauxRemise2.setEnabled(false);
      tfTauxRemise2.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise2.setModeReduit(true);
      tfTauxRemise2.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise2.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise2.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise2.setName("tfTauxRemise2");
      tfTauxRemise2.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise2FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise2);
      
      // ---- tfTauxRemise3 ----
      tfTauxRemise3.setEnabled(false);
      tfTauxRemise3.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise3.setModeReduit(true);
      tfTauxRemise3.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise3.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise3.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise3.setName("tfTauxRemise3");
      tfTauxRemise3.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise3FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise3);
      
      // ---- tfTauxRemise4 ----
      tfTauxRemise4.setEnabled(false);
      tfTauxRemise4.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise4.setModeReduit(true);
      tfTauxRemise4.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise4.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise4.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise4.setName("tfTauxRemise4");
      tfTauxRemise4.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise4FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise4);
      
      // ---- tfTauxRemise5 ----
      tfTauxRemise5.setEnabled(false);
      tfTauxRemise5.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise5.setModeReduit(true);
      tfTauxRemise5.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise5.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise5.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise5.setName("tfTauxRemise5");
      tfTauxRemise5.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise5FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise5);
      
      // ---- tfTauxRemise6 ----
      tfTauxRemise6.setEnabled(false);
      tfTauxRemise6.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise6.setModeReduit(true);
      tfTauxRemise6.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise6.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise6.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise6.setName("tfTauxRemise6");
      tfTauxRemise6.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise6FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise6);
    }
    add(pnlTauxRemise,
        new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbModeTauxRemise ----
    lbModeTauxRemise.setModeReduit(true);
    lbModeTauxRemise.setText("Mode taux remises");
    lbModeTauxRemise.setMinimumSize(new Dimension(30, 22));
    lbModeTauxRemise.setPreferredSize(new Dimension(30, 22));
    lbModeTauxRemise.setName("lbModeTauxRemise");
    add(lbModeTauxRemise,
        new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbModeTauxRemise ----
    cbModeTauxRemise.setEnabled(false);
    cbModeTauxRemise.setName("cbModeTauxRemise");
    cbModeTauxRemise.addItemListener(e -> cbModeApplicationTauxRemiseItemStateChanged(e));
    add(cbModeTauxRemise,
        new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbAssietteTauxRemise ----
    lbAssietteTauxRemise.setModeReduit(true);
    lbAssietteTauxRemise.setText("Assiette taux remises");
    lbAssietteTauxRemise.setMinimumSize(new Dimension(30, 22));
    lbAssietteTauxRemise.setPreferredSize(new Dimension(30, 22));
    lbAssietteTauxRemise.setName("lbAssietteTauxRemise");
    add(lbAssietteTauxRemise,
        new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbAssietteTauxRemise ----
    cbAssietteTauxRemise.setEnabled(false);
    cbAssietteTauxRemise.setName("cbAssietteTauxRemise");
    cbAssietteTauxRemise.addItemListener(e -> cbAssietteTauxRemiseItemStateChanged(e));
    add(cbAssietteTauxRemise,
        new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbProvenanceCoefficient ----
    lbProvenanceCoefficient.setModeReduit(true);
    lbProvenanceCoefficient.setText("Provenance coefficient");
    lbProvenanceCoefficient.setMinimumSize(new Dimension(30, 22));
    lbProvenanceCoefficient.setPreferredSize(new Dimension(30, 22));
    lbProvenanceCoefficient.setName("lbProvenanceCoefficient");
    add(lbProvenanceCoefficient,
        new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbProvenanceCoefficient ----
    cbProvenanceCoefficient.setEnabled(false);
    cbProvenanceCoefficient.setName("cbProvenanceCoefficient");
    cbProvenanceCoefficient.addItemListener(e -> {
      cbModeApplicationTauxRemiseItemStateChanged(e);
      cbProvenanceCoefficientItemStateChanged(e);
    });
    add(cbProvenanceCoefficient,
        new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbCoefficient ----
    lbCoefficient.setModeReduit(true);
    lbCoefficient.setText("Coefficient");
    lbCoefficient.setMinimumSize(new Dimension(30, 22));
    lbCoefficient.setPreferredSize(new Dimension(30, 22));
    lbCoefficient.setName("lbCoefficient");
    add(lbCoefficient,
        new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfCoefficient ----
    tfCoefficient.setModeReduit(true);
    tfCoefficient.setEnabled(false);
    tfCoefficient.setName("tfCoefficient");
    tfCoefficient.addSNComposantListener(e -> tfCoefficientValueChanged(e));
    add(tfCoefficient, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbTypeGratuit ----
    lbTypeGratuit.setModeReduit(true);
    lbTypeGratuit.setText("Type de gratuit\u00e9");
    lbTypeGratuit.setMinimumSize(new Dimension(30, 22));
    lbTypeGratuit.setPreferredSize(new Dimension(30, 22));
    lbTypeGratuit.setName("lbTypeGratuit");
    add(lbTypeGratuit,
        new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfTypeGratuit ----
    tfTypeGratuit.setModeReduit(true);
    tfTypeGratuit.setEnabled(false);
    tfTypeGratuit.setMaximumSize(new Dimension(30, 22));
    tfTypeGratuit.setMinimumSize(new Dimension(30, 22));
    tfTypeGratuit.setPreferredSize(new Dimension(30, 22));
    tfTypeGratuit.setHorizontalAlignment(SwingConstants.CENTER);
    tfTypeGratuit.setName("tfTypeGratuit");
    tfTypeGratuit.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfTypeGratuitFocusLost(e);
      }
    });
    add(tfTypeGratuit, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbProvenancePrixNet ----
    lbProvenancePrixNet.setModeReduit(true);
    lbProvenancePrixNet.setText("Provenance prix net");
    lbProvenancePrixNet.setMinimumSize(new Dimension(30, 22));
    lbProvenancePrixNet.setPreferredSize(new Dimension(30, 22));
    lbProvenancePrixNet.setName("lbProvenancePrixNet");
    add(lbProvenancePrixNet,
        new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbProvenancePrixNet ----
    cbProvenancePrixNet.setEnabled(false);
    cbProvenancePrixNet.setName("cbProvenancePrixNet");
    cbProvenancePrixNet.addItemListener(e -> {
      cbModeApplicationTauxRemiseItemStateChanged(e);
      cbProvenanceCoefficientItemStateChanged(e);
      cbProvenancePrixNetItemStateChanged(e);
    });
    add(cbProvenancePrixNet,
        new GridBagConstraints(1, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixNetHT ----
    lbPrixNetHT.setModeReduit(true);
    lbPrixNetHT.setText("Prix net HT");
    lbPrixNetHT.setMinimumSize(new Dimension(30, 22));
    lbPrixNetHT.setPreferredSize(new Dimension(30, 22));
    lbPrixNetHT.setName("lbPrixNetHT");
    add(lbPrixNetHT,
        new GridBagConstraints(0, 18, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixNetHT ----
    tfPrixNetHT.setModeReduit(true);
    tfPrixNetHT.setEnabled(false);
    tfPrixNetHT.setName("tfPrixNetHT");
    tfPrixNetHT.addSNComposantListener(e -> tfPrixNetHTValueChanged(e));
    add(tfPrixNetHT, new GridBagConstraints(1, 18, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixNetTTC ----
    lbPrixNetTTC.setModeReduit(true);
    lbPrixNetTTC.setText("Prix net TTC");
    lbPrixNetTTC.setMinimumSize(new Dimension(30, 22));
    lbPrixNetTTC.setPreferredSize(new Dimension(30, 22));
    lbPrixNetTTC.setName("lbPrixNetTTC");
    add(lbPrixNetTTC,
        new GridBagConstraints(0, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixNetTTC ----
    tfPrixNetTTC.setModeReduit(true);
    tfPrixNetTTC.setEnabled(false);
    tfPrixNetTTC.setName("tfPrixNetTTC");
    tfPrixNetTTC.addSNComposantListener(e -> tfPrixNetTTCValueChanged(e));
    add(tfPrixNetTTC, new GridBagConstraints(1, 19, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbQuantiteUV ----
    lbQuantiteUV.setText("Quantit\u00e9 en UV");
    lbQuantiteUV.setModeReduit(true);
    lbQuantiteUV.setName("lbQuantiteUV");
    add(lbQuantiteUV,
        new GridBagConstraints(0, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfQuantiteUV ----
    tfQuantiteUV.setEnabled(false);
    tfQuantiteUV.setModeReduit(true);
    tfQuantiteUV.setName("tfQuantiteUV");
    tfQuantiteUV.addSNComposantListener(e -> tfQuantiteUVValueChanged(e));
    add(tfQuantiteUV, new GridBagConstraints(1, 20, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbOriginePrixVente;
  private SNTexte tfOriginePrixVente;
  private SNLabelChamp lbPrixGaranti;
  private SNCheckBoxReduit cbPrixGaranti;
  private SNLabelChamp lbNumeroTVAEntete;
  private SNComboBoxReduit cbNumeroTVAEntete;
  private SNLabelChamp lbNumeroTVAEtablissement;
  private SNComboBoxReduit cbNumeroTVAEtablissement;
  private SNLabelChamp lbUniteVente;
  private SNTexte tfUniteVente;
  private SNLabelChamp lbProvenanceColonneTarif;
  private SNComboBoxReduit cbProvenanceColonneTarif;
  private SNLabelChamp lbColonneTarif;
  private SNComboBoxReduit cbColonneTarif;
  private SNLabelChamp lbProvenancePrixBase;
  private SNComboBoxReduit cbProvenancePrixBase;
  private SNLabelChamp lbPrixBaseHT;
  private SNMontant tfPrixBaseHT;
  private SNLabelChamp lbPrixBaseTTC;
  private SNMontant tfPrixBaseTTC;
  private SNLabelChamp lbProvenanceTauxRemise;
  private SNComboBoxReduit cbProvenanceTauxRemise;
  private SNLabelChamp lbTauxRemise;
  private SNPanel pnlTauxRemise;
  private SNTexte tfTauxRemise1;
  private SNTexte tfTauxRemise2;
  private SNTexte tfTauxRemise3;
  private SNTexte tfTauxRemise4;
  private SNTexte tfTauxRemise5;
  private SNTexte tfTauxRemise6;
  private SNLabelChamp lbModeTauxRemise;
  private SNComboBoxReduit cbModeTauxRemise;
  private SNLabelChamp lbAssietteTauxRemise;
  private SNComboBoxReduit cbAssietteTauxRemise;
  private SNLabelChamp lbProvenanceCoefficient;
  private SNComboBoxReduit cbProvenanceCoefficient;
  private SNLabelChamp lbCoefficient;
  private SNMontant tfCoefficient;
  private SNLabelChamp lbTypeGratuit;
  private SNTexte tfTypeGratuit;
  private SNLabelChamp lbProvenancePrixNet;
  private SNComboBoxReduit cbProvenancePrixNet;
  private SNLabelChamp lbPrixNetHT;
  private SNMontant tfPrixNetHT;
  private SNLabelChamp lbPrixNetTTC;
  private SNMontant tfPrixNetTTC;
  private SNLabelChamp lbQuantiteUV;
  private SNMontant tfQuantiteUV;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
