/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.barrebouton;

import java.util.EventListener;

import ri.serien.libswing.composant.primitif.bouton.SNBouton;

/**
 * Interface pour recevoir les évènements liés aux boutons.
 */
public interface SNBoutonListener extends EventListener {
  /**
   * Appelé lorsqu'un bouton est cliqué.
   */
  public void traiterClicBouton(SNBouton pSNBouton);
}
