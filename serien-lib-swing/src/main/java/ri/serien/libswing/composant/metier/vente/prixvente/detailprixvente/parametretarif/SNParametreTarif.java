/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametretarif;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.EnumGraduationDecimaleTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Composant permet d'afficher toutes les prix de vente d'un tarif article.
 */
public class SNParametreTarif extends SNPanelTitre {
  private static final String TITRE = "Tarif";
  private static final String PREFIXE_ORIGINE = "= ";
  
  // Variables
  private ModeleDialogueDetailPrixVente modele = null;
  private boolean evenementsActifs = false;
  private ParametreTarif parametreTarif = null;
  private ParametreTarif parametreTarifInitial = null;
  private FormulePrixVente formulePrixVente = null;
  private List<SNMontant> listeComposantPrixColonne = null;
  private List<SNLabelChamp> listeComposantLibelleColonne = null;
  
  /**
   * Constructeur.
   */
  public SNParametreTarif() {
    super();
    initComponents();
    
    // Initialisation de la liste des composants
    listeComposantPrixColonne = new ArrayList<SNMontant>();
    listeComposantPrixColonne.add(snPrixBaseHT1);
    listeComposantPrixColonne.add(snPrixBaseHT2);
    listeComposantPrixColonne.add(snPrixBaseHT3);
    listeComposantPrixColonne.add(snPrixBaseHT4);
    listeComposantPrixColonne.add(snPrixBaseHT5);
    listeComposantPrixColonne.add(snPrixBaseHT6);
    listeComposantPrixColonne.add(snPrixBaseHT7);
    listeComposantPrixColonne.add(snPrixBaseHT8);
    listeComposantPrixColonne.add(snPrixBaseHT9);
    listeComposantPrixColonne.add(snPrixBaseHT10);
    
    listeComposantLibelleColonne = new ArrayList<SNLabelChamp>();
    listeComposantLibelleColonne.add(lbColonne1);
    listeComposantLibelleColonne.add(lbColonne2);
    listeComposantLibelleColonne.add(lbColonne3);
    listeComposantLibelleColonne.add(lbColonne4);
    listeComposantLibelleColonne.add(lbColonne5);
    listeComposantLibelleColonne.add(lbColonne6);
    listeComposantLibelleColonne.add(lbColonne7);
    listeComposantLibelleColonne.add(lbColonne8);
    listeComposantLibelleColonne.add(lbColonne9);
    listeComposantLibelleColonne.add(lbColonne10);
    
    // Renseigner la liste déroulante avec les graduations
    for (EnumGraduationDecimaleTarif graduationDecimaleTarif : EnumGraduationDecimaleTarif.values()) {
      cbGraduationDecimale.addItem(graduationDecimaleTarif);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modèle associé au composant graphique.
   * @return Modèle de la boîte de dialogue.
   */
  public ModeleDialogueDetailPrixVente getModele() {
    return modele;
  }
  
  /**
   * Modifier le modèle associé au composant graphique.
   * @param pModele Modèle de la boîte de dialogue.
   */
  public void setModele(ModeleDialogueDetailPrixVente pModele) {
    modele = pModele;
  }
  
  /**
   * Indiquer si les évènements doivent être traités.
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraichissement des données du composant.
   */
  public void rafraichir() {
    // Empêcher les évènements pendant le rafraichissement
    evenementsActifs = false;
    
    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    parametreTarif = getModele().getParametreTarif();
    parametreTarifInitial = getModele().getParametreTarifInitial();
    formulePrixVente = modele.getFormulePrixVente();
    
    // Configurer tous les composants SNMontant avec la précision souhaitée
    int nombreDecimale = ArrondiPrix.DECIMALE_STANDARD;
    if (formulePrixVente != null && formulePrixVente.getArrondiPrix() != null) {
      nombreDecimale = formulePrixVente.getArrondiPrix().getNombreDecimale();
    }
    snPrixBaseHT1.setLongueurPartieDecimale(nombreDecimale);
    snPrixBaseHT2.setLongueurPartieDecimale(nombreDecimale);
    snPrixBaseHT3.setLongueurPartieDecimale(nombreDecimale);
    snPrixBaseHT4.setLongueurPartieDecimale(nombreDecimale);
    snPrixBaseHT5.setLongueurPartieDecimale(nombreDecimale);
    snPrixBaseHT6.setLongueurPartieDecimale(nombreDecimale);
    snPrixBaseHT7.setLongueurPartieDecimale(nombreDecimale);
    snPrixBaseHT8.setLongueurPartieDecimale(nombreDecimale);
    snPrixBaseHT9.setLongueurPartieDecimale(nombreDecimale);
    snPrixBaseHT10.setLongueurPartieDecimale(nombreDecimale);
    
    // Rafraichir les composants
    rafraichirTitre();
    rafraichirDevise();
    rafraichirGraduationDecimaleTarif();
    rafraichirColonneTarif();
    
    // Réactiver les évènements pendant le rafraichissement
    evenementsActifs = true;
  }
  
  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirTitre() {
    if (parametreTarif != null && parametreTarif.getLibelle() != null) {
      setTitre(TITRE + " " + parametreTarif.getLibelle());
    }
    else {
      setTitre(TITRE);
    }
  }

  /**
   * Rafraichir la devise.
   */
  private void rafraichirDevise() {
    if (parametreTarif != null && parametreTarif.getCodeDevise() != null) {
      String codeDevise = Constantes.normerTexte(parametreTarif.getCodeDevise());
      if (codeDevise.isEmpty()) {
        codeDevise = "EUR";
      }
      tfDevise.setText(codeDevise);
    }
    else {
      tfDevise.setText("");
    }
  }
  
  /**
   * Rafraichir la graduation décimale utilisée pour le calcul.
   */
  private void rafraichirGraduationDecimaleTarif() {
    if (parametreTarif != null && parametreTarif.getGraduationDecimaleTarif() != null) {
      cbGraduationDecimale.setSelectedItem(parametreTarif.getGraduationDecimaleTarif());
    }
    else {
      cbGraduationDecimale.setSelectedItem(null);
    }
    
    cbGraduationDecimale.setEnabled(modele != null && modele.isModeSimulation());
    lbGraduationDecimale.setImportance(parametreTarif != null && parametreTarifInitial != null
        && !Constantes.equals(parametreTarif.getGraduationDecimaleTarif(), parametreTarifInitial.getGraduationDecimaleTarif()));
  }
  
  /**
   * Rafraichit les prix HT des colonnes.
   */
  private void rafraichirColonneTarif1() {
    if (parametreTarif != null && parametreTarif.getPrixBaseHT(1) != null) {
      snPrixBaseHT1.setMontant(parametreTarif.getPrixBaseHT(1));
    }
    else {
      snPrixBaseHT1.setMontant("");
    }
    
    snPrixBaseHT1.setEnabled(modele != null && modele.isModeSimulation());
    lbColonne1.setImportance(parametreTarif != null && parametreTarifInitial != null
        && !Constantes.equals(parametreTarif.getPrixBaseHT(1), parametreTarifInitial.getPrixBaseHT(1)));
  }
  
  /**
   * Rafraichit les prix HT des colonnes.
   */
  private void rafraichirColonneTarif() {
    for (int i = 0; i < listeComposantPrixColonne.size(); i++) {
      SNMontant snMontant = listeComposantPrixColonne.get(i);
      SNLabelChamp lbColonne = listeComposantLibelleColonne.get(i);
      
      if (parametreTarif != null && parametreTarif.getPrixBaseHT(i + 1) != null) {
        snMontant.setMontant(parametreTarif.getPrixBaseHT(i + 1));
      }
      else {
        snMontant.setMontant("");
      }
      
      snMontant.setEnabled(modele != null && modele.isModeSimulation());
      lbColonne.setImportance(parametreTarif != null && parametreTarifInitial != null
          && !Constantes.equals(parametreTarif.getPrixBaseHT(i + 1), parametreTarifInitial.getPrixBaseHT(i + 1)));
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void cbGraduationDecimaleItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierGraduationDecimaleTarif((EnumGraduationDecimaleTarif) cbGraduationDecimale.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT1ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT1.getMontant(), 1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT2ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT2.getMontant(), 2);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT3ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT3.getMontant(), 3);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT4ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT4.getMontant(), 4);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT5ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT5.getMontant(), 5);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT6ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT6.getMontant(), 6);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT7ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT7.getMontant(), 7);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT8ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT8.getMontant(), 8);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT9ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT9.getMontant(), 9);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPrixBaseHT10ValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixBaseHTColonneTarif(snPrixBaseHT10.getMontant(), 10);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlInformationTarif = new SNPanel();
    lbDevise = new SNLabelChamp();
    tfDevise = new SNTexte();
    lbGraduationDecimale = new SNLabelChamp();
    cbGraduationDecimale = new SNComboBoxReduit();
    pnlColonneTarif = new SNPanel();
    lbColonne1 = new SNLabelChamp();
    lbColonne2 = new SNLabelChamp();
    lbColonne3 = new SNLabelChamp();
    lbColonne4 = new SNLabelChamp();
    lbColonne5 = new SNLabelChamp();
    snPrixBaseHT1 = new SNMontant();
    snPrixBaseHT2 = new SNMontant();
    snPrixBaseHT3 = new SNMontant();
    snPrixBaseHT4 = new SNMontant();
    snPrixBaseHT5 = new SNMontant();
    lbColonne6 = new SNLabelChamp();
    lbColonne7 = new SNLabelChamp();
    lbColonne8 = new SNLabelChamp();
    lbColonne9 = new SNLabelChamp();
    lbColonne10 = new SNLabelChamp();
    snPrixBaseHT6 = new SNMontant();
    snPrixBaseHT7 = new SNMontant();
    snPrixBaseHT8 = new SNMontant();
    snPrixBaseHT9 = new SNMontant();
    snPrixBaseHT10 = new SNMontant();
    
    // ======== this ========
    setTitre("Tarif");
    setModeReduit(true);
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
    
    // ======== pnlInformationTarif ========
    {
      pnlInformationTarif.setName("pnlInformationTarif");
      pnlInformationTarif.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlInformationTarif.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlInformationTarif.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlInformationTarif.getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlInformationTarif.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ---- lbDevise ----
      lbDevise.setText("Devise");
      lbDevise.setModeReduit(true);
      lbDevise.setName("lbDevise");
      pnlInformationTarif.add(lbDevise,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
      
      // ---- tfDevise ----
      tfDevise.setEnabled(false);
      tfDevise.setModeReduit(true);
      tfDevise.setName("tfDevise");
      pnlInformationTarif.add(tfDevise, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbGraduationDecimale ----
      lbGraduationDecimale.setText("Graduation d\u00e9cimale");
      lbGraduationDecimale.setModeReduit(true);
      lbGraduationDecimale.setName("lbGraduationDecimale");
      pnlInformationTarif.add(lbGraduationDecimale,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
      
      // ---- cbGraduationDecimale ----
      cbGraduationDecimale.setEnabled(false);
      cbGraduationDecimale.setName("cbGraduationDecimale");
      cbGraduationDecimale.addItemListener(e -> cbGraduationDecimaleItemStateChanged(e));
      pnlInformationTarif.add(cbGraduationDecimale,
          new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlInformationTarif,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ======== pnlColonneTarif ========
    {
      pnlColonneTarif.setName("pnlColonneTarif");
      pnlColonneTarif.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlColonneTarif.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlColonneTarif.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlColonneTarif.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlColonneTarif.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
      
      // ---- lbColonne1 ----
      lbColonne1.setText("Col.1");
      lbColonne1.setModeReduit(true);
      lbColonne1.setPreferredSize(new Dimension(35, 22));
      lbColonne1.setMinimumSize(new Dimension(35, 22));
      lbColonne1.setMaximumSize(new Dimension(35, 22));
      lbColonne1.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne1.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne1.setName("lbColonne1");
      pnlColonneTarif.add(lbColonne1,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbColonne2 ----
      lbColonne2.setText("Col.2");
      lbColonne2.setModeReduit(true);
      lbColonne2.setPreferredSize(new Dimension(35, 22));
      lbColonne2.setMinimumSize(new Dimension(35, 22));
      lbColonne2.setMaximumSize(new Dimension(35, 22));
      lbColonne2.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne2.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne2.setName("lbColonne2");
      pnlColonneTarif.add(lbColonne2,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbColonne3 ----
      lbColonne3.setText("Col.3");
      lbColonne3.setModeReduit(true);
      lbColonne3.setPreferredSize(new Dimension(35, 22));
      lbColonne3.setMinimumSize(new Dimension(35, 22));
      lbColonne3.setMaximumSize(new Dimension(35, 22));
      lbColonne3.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne3.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne3.setName("lbColonne3");
      pnlColonneTarif.add(lbColonne3,
          new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbColonne4 ----
      lbColonne4.setText("Col.4");
      lbColonne4.setModeReduit(true);
      lbColonne4.setPreferredSize(new Dimension(35, 22));
      lbColonne4.setMinimumSize(new Dimension(35, 22));
      lbColonne4.setMaximumSize(new Dimension(35, 22));
      lbColonne4.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne4.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne4.setName("lbColonne4");
      pnlColonneTarif.add(lbColonne4,
          new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbColonne5 ----
      lbColonne5.setText("Col.5");
      lbColonne5.setModeReduit(true);
      lbColonne5.setPreferredSize(new Dimension(35, 22));
      lbColonne5.setMinimumSize(new Dimension(35, 22));
      lbColonne5.setMaximumSize(new Dimension(35, 22));
      lbColonne5.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne5.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne5.setName("lbColonne5");
      pnlColonneTarif.add(lbColonne5,
          new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT1 ----
      snPrixBaseHT1.setModeReduit(true);
      snPrixBaseHT1.setEnabled(false);
      snPrixBaseHT1.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT1.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT1.setName("snPrixBaseHT1");
      snPrixBaseHT1.addSNComposantListener(e -> snPrixBaseHT1ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT1,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT2 ----
      snPrixBaseHT2.setModeReduit(true);
      snPrixBaseHT2.setEnabled(false);
      snPrixBaseHT2.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT2.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT2.setName("snPrixBaseHT2");
      snPrixBaseHT2.addSNComposantListener(e -> snPrixBaseHT2ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT2,
          new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT3 ----
      snPrixBaseHT3.setModeReduit(true);
      snPrixBaseHT3.setEnabled(false);
      snPrixBaseHT3.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT3.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT3.setName("snPrixBaseHT3");
      snPrixBaseHT3.addSNComposantListener(e -> snPrixBaseHT3ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT3,
          new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT4 ----
      snPrixBaseHT4.setModeReduit(true);
      snPrixBaseHT4.setEnabled(false);
      snPrixBaseHT4.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT4.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT4.setName("snPrixBaseHT4");
      snPrixBaseHT4.addSNComposantListener(e -> snPrixBaseHT4ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT4,
          new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT5 ----
      snPrixBaseHT5.setModeReduit(true);
      snPrixBaseHT5.setEnabled(false);
      snPrixBaseHT5.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT5.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT5.setName("snPrixBaseHT5");
      snPrixBaseHT5.addSNComposantListener(e -> snPrixBaseHT5ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT5,
          new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbColonne6 ----
      lbColonne6.setText("Col.6");
      lbColonne6.setModeReduit(true);
      lbColonne6.setPreferredSize(new Dimension(35, 22));
      lbColonne6.setMinimumSize(new Dimension(35, 22));
      lbColonne6.setMaximumSize(new Dimension(35, 22));
      lbColonne6.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne6.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne6.setName("lbColonne6");
      pnlColonneTarif.add(lbColonne6,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbColonne7 ----
      lbColonne7.setText("Col.7");
      lbColonne7.setModeReduit(true);
      lbColonne7.setPreferredSize(new Dimension(35, 22));
      lbColonne7.setMinimumSize(new Dimension(35, 22));
      lbColonne7.setMaximumSize(new Dimension(35, 22));
      lbColonne7.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne7.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne7.setName("lbColonne7");
      pnlColonneTarif.add(lbColonne7,
          new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbColonne8 ----
      lbColonne8.setText("Col.8");
      lbColonne8.setModeReduit(true);
      lbColonne8.setPreferredSize(new Dimension(35, 22));
      lbColonne8.setMinimumSize(new Dimension(35, 22));
      lbColonne8.setMaximumSize(new Dimension(35, 22));
      lbColonne8.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne8.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne8.setName("lbColonne8");
      pnlColonneTarif.add(lbColonne8,
          new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbColonne9 ----
      lbColonne9.setText("Col.9");
      lbColonne9.setModeReduit(true);
      lbColonne9.setPreferredSize(new Dimension(35, 22));
      lbColonne9.setMinimumSize(new Dimension(35, 22));
      lbColonne9.setMaximumSize(new Dimension(35, 22));
      lbColonne9.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne9.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne9.setName("lbColonne9");
      pnlColonneTarif.add(lbColonne9,
          new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- lbColonne10 ----
      lbColonne10.setText("Col.10");
      lbColonne10.setModeReduit(true);
      lbColonne10.setPreferredSize(new Dimension(35, 22));
      lbColonne10.setMinimumSize(new Dimension(35, 22));
      lbColonne10.setMaximumSize(new Dimension(35, 22));
      lbColonne10.setHorizontalAlignment(SwingConstants.CENTER);
      lbColonne10.setVerticalAlignment(SwingConstants.BOTTOM);
      lbColonne10.setName("lbColonne10");
      pnlColonneTarif.add(lbColonne10,
          new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT6 ----
      snPrixBaseHT6.setModeReduit(true);
      snPrixBaseHT6.setEnabled(false);
      snPrixBaseHT6.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT6.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT6.setName("snPrixBaseHT6");
      snPrixBaseHT6.addSNComposantListener(e -> snPrixBaseHT6ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT6,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT7 ----
      snPrixBaseHT7.setModeReduit(true);
      snPrixBaseHT7.setEnabled(false);
      snPrixBaseHT7.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT7.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT7.setName("snPrixBaseHT7");
      snPrixBaseHT7.addSNComposantListener(e -> snPrixBaseHT7ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT7,
          new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT8 ----
      snPrixBaseHT8.setModeReduit(true);
      snPrixBaseHT8.setEnabled(false);
      snPrixBaseHT8.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT8.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT8.setName("snPrixBaseHT8");
      snPrixBaseHT8.addSNComposantListener(e -> snPrixBaseHT8ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT8,
          new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT9 ----
      snPrixBaseHT9.setModeReduit(true);
      snPrixBaseHT9.setEnabled(false);
      snPrixBaseHT9.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT9.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT9.setName("snPrixBaseHT9");
      snPrixBaseHT9.addSNComposantListener(e -> snPrixBaseHT9ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT9,
          new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snPrixBaseHT10 ----
      snPrixBaseHT10.setModeReduit(true);
      snPrixBaseHT10.setEnabled(false);
      snPrixBaseHT10.setMinimumSize(new Dimension(40, 22));
      snPrixBaseHT10.setPreferredSize(new Dimension(40, 22));
      snPrixBaseHT10.setName("snPrixBaseHT10");
      snPrixBaseHT10.addSNComposantListener(e -> snPrixBaseHT10ValueChanged(e));
      pnlColonneTarif.add(snPrixBaseHT10,
          new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlColonneTarif,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlInformationTarif;
  private SNLabelChamp lbDevise;
  private SNTexte tfDevise;
  private SNLabelChamp lbGraduationDecimale;
  private SNComboBoxReduit cbGraduationDecimale;
  private SNPanel pnlColonneTarif;
  private SNLabelChamp lbColonne1;
  private SNLabelChamp lbColonne2;
  private SNLabelChamp lbColonne3;
  private SNLabelChamp lbColonne4;
  private SNLabelChamp lbColonne5;
  private SNMontant snPrixBaseHT1;
  private SNMontant snPrixBaseHT2;
  private SNMontant snPrixBaseHT3;
  private SNMontant snPrixBaseHT4;
  private SNMontant snPrixBaseHT5;
  private SNLabelChamp lbColonne6;
  private SNLabelChamp lbColonne7;
  private SNLabelChamp lbColonne8;
  private SNLabelChamp lbColonne9;
  private SNLabelChamp lbColonne10;
  private SNMontant snPrixBaseHT6;
  private SNMontant snPrixBaseHT7;
  private SNMontant snPrixBaseHT8;
  private SNMontant snPrixBaseHT9;
  private SNMontant snPrixBaseHT10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
