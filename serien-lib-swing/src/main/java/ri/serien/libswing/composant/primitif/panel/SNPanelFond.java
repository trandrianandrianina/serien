/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.panel;

import java.awt.BorderLayout;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Panneau de fond standard des écrans et boîtes de dialogue.
 *
 * C'est le premier panneau qui est posé sur un écran ou une boîte de dialogue. Il a pour rôle de colorier le fond dans la couleur
 * standard de Série N. Il n'a aucun autre effet graphique (bord, marge, ...). Il est donc opaque tandis que les composants qui y sont
 * insérés sont transparents afin de conserver la couleur de fond imposée par ce composant.
 *
 * Il utilise un BorderLayout. Typiquement, la zone centrale contient un SNPanelContenu, la zone supérieure contient le bandeau de titre
 * et la zone inférieure contient la barre de bouton.
 *
 * Il est judicieux de nommer ce composant pnlFond pour le répérer rapidement.
 *
 * Caractéristiques graphiques :
 * - Fond opaque.
 * - Couleur de fond standard (jaune pâle).
 * - Pas de cadre et pas de titre.
 * - Pas de marge.
 * - BorderLayout.
 */
public class SNPanelFond extends SNPanel {
  /**
   * Constructeur standard.
   */
  public SNPanelFond() {
    super();
    setOpaque(true);
    setBackground(SNCharteGraphique.COULEUR_FOND);
    setLayout(new BorderLayout());
  }
}
