/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Affichage de la boîte de dialogue "Image liées à l'article" du logiciel Série N.
 *
 * Composé de :
 * - Un libellé affichant le nom de l'image affichée,
 * - Un libellé affichage l'image liée à l'article,
 * - Des boutons de navigation pour visualiser, si besoin, les différentes images associées à l'article,
 * - Des boutons pour zoomer et dézoomer l'image,
 * - Un bouton pour ajouter une image via l'explorateur de fichier,
 * - Un bouton pour supprimer l'image affichée,
 * - Une fonction d'ajout d'image par glisser déposer,
 *
 * L'affichage des différents boutons est conditonné selon plusieurs critères :
 * - L'état de consultation "isConsultation", qui désactive les accès aux fonction d'ajout et de suppression d'image.
 * - L'absence d'image à afficher masque les boutons de zoom et de supression.
 * - Le zoom est limité dans le modèle et lorsqu'une limite est atteinte le bouton associé est masqué.
 *
 */
public class DialogueDiaporamaPhoto extends AbstractVueDialogue<ModeleDialogueDiaporamaPhoto> {
  
  /**
   * Constructeur.
   */
  public DialogueDiaporamaPhoto(ModeleDialogueDiaporamaPhoto pModele) {
    super(pModele);
  }
  
  // Constantes
  // Libellés des boutons
  private static final String BOUTON_AJOUTER = "Ajouter";
  private static final String BOUTON_SUPPRIMER = "Supprimer";
  private static final String BOUTON_ZOOM_PLUS = "Agrandir image";
  private static final String BOUTON_ZOOM_MOINS = "Réduire image";
  private static final String ZOOM_PLUS = "+";
  private static final String ZOOM_MOINS = "-";
  
  // Dimensions minimales de la boîte de dialogue.
  private static final int LARGEUR_AFFICHAGE_MIN = 800;
  private static final int HAUTEUR_AFFICHAGE_MIN = 400;
  private static final int LARGEUR_AFFICHAGE_MIN_AVEC_BOUTONS = 1200;

  // Variables
  private boolean isConsultation = false;

  // -- Méthodes publiques
  /**
   * Afficher la boîte de dialogue.
   * Ne prend pas en compte l'état de consultation/interrogation.
   *
   * @param pInfoUser
   * @param pCheminDAccesDossier Chemin d'accès aux images à afficher.
   */
  public static boolean afficher(EnvUser pInfoUser, String pCheminDAccesDossier) {
    ModeleDialogueDiaporamaPhoto modele = new ModeleDialogueDiaporamaPhoto(pInfoUser, pCheminDAccesDossier);
    DialogueDiaporamaPhoto vue = new DialogueDiaporamaPhoto(modele);
    vue.afficher();
    return modele.isSortieAvecValidation();
  }

  /**
   * Afficher la boîte de dialogue.
   * Prend en compte l'état de consultation/interrogation.
   *
   * @param pInfoUser
   * @param pCheminDAccesDossier Chemin d'accès aux images à afficher.
   * @param pIsConsultation Etat de consultation de l'écran affichant l'image.
   */
  public static boolean afficher(EnvUser pInfoUser, String pCheminDAccesDossier, boolean pIsConsultation) {
    ModeleDialogueDiaporamaPhoto modele = new ModeleDialogueDiaporamaPhoto(pInfoUser, pCheminDAccesDossier, pIsConsultation);
    DialogueDiaporamaPhoto vue = new DialogueDiaporamaPhoto(modele);
    vue.afficher();
    return modele.isSortieAvecValidation();
  }

  /**
   * Initialisation des composants.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    // Récupérer l'information quant au mode d'utilisation de l'écran.
    isConsultation = getModele().isConsultation();
    // Si le mode d'utilisation n'est pas un mode de consultation,
    if (!isConsultation) {
      // On ajoute la possibiliter de glisser/déposer des images dans la boîte de dialogue pour les ajouter à la liste.
      getModele().gererGlisserDeposer(lbImage);
    }
    // Construction de la barre de boutons
    snBarreBouton.setModeNavigation(true);

    // Si le mode d'utilisation n'est pas un mode de consultation,
    if (!isConsultation) {
      // On affiche les boutons Annuler et Valider qui sont liés aux modifications.
      snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
      snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    }
    // Sinon, on affiche seulement le bouton Fermer.
    else {
      snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    }
    
    // On ajoute le reste des boutons, désactivés par défaut.
    // En cas de problème de chargement de l'écran, ils ne seront donc ni visibles, ni utilisables.
    snBarreBouton.ajouterBouton(BOUTON_ZOOM_PLUS, '+', false);
    snBarreBouton.ajouterBouton(BOUTON_ZOOM_MOINS, '-', false);
    snBarreBouton.ajouterBouton(BOUTON_AJOUTER, 'a', false);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER, 's', false);
    
    // On définit les compteurs pour la barre de navigation.
    // La numérotation des photos commence à 1 mais celle des index commence à 0, on ajoute donc 1 à l'index.
    snBarreBouton.setCompteurCourant(getModele().getIndexImagePourAffichage() + 1);
    snBarreBouton.setCompteurMax(getModele().getNombrePhotoDansDiaporama());
    
    // On ajoute le listene sur la barre de bouton.
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraîchir les composants de la boîte de dialogue.
   */
  @Override
  public void rafraichir() {
    rafraichirTailleBoiteDeDialogue();
    rafraichirImage();
    rafraichirTitre();

    // Rafraichissement de l'affichage des boutons
    rafraichirBarreBoutonNavigation();
    rafraichirBoutonZoomPlus();
    rafraichirBoutonZoomMoins();
    rafraichirBoutonAjouter();
    rafraichirBoutonSupprimer();
  }
  
  /**
   * Rafraîchir la taille de la boîte de dialogue.
   * La taille du composant d'affichage de l'image s'adapte à la taille de la boîte de dialogue.
   *
   * La largeur maximale de la boîte de dialogue permet l'affichage de tous les boutons de la barre de bouton dans le cas où il y a une
   * image à afficher en mode Modification.
   *
   */
  private void rafraichirTailleBoiteDeDialogue() {
    // Si il n'y a pas de photos à afficher,
    if (getModele().getNombrePhotoDansDiaporama() == 0) {
      // On utilise les dimensions minimales de la boîte de dialogue.
      setSize(LARGEUR_AFFICHAGE_MIN, HAUTEUR_AFFICHAGE_MIN);
      // On empêche l'utilisateur de pouvoir redimensionner la boîte de dialogue.
      setResizable(false);
    }
    else {
      // Sinon, on autorise l'utilisateur à redimensionner la boîte de dialogue.
      setResizable(true);
      // Dans le cas où il y a une image affichée,
      // on empêche la réduction de la taille de la boîte de dialogue en dessous d'une largeur où les boutons commencent à disparaître.
      if (this.getWidth() < LARGEUR_AFFICHAGE_MIN_AVEC_BOUTONS) {
        setSize(LARGEUR_AFFICHAGE_MIN_AVEC_BOUTONS, this.getHeight());
      }
      // On empêche la réduction de la boîte de dialogue en dessous de la dimension minimale autorisée.
      if (this.getHeight() < HAUTEUR_AFFICHAGE_MIN) {
        setSize(this.getWidth(), HAUTEUR_AFFICHAGE_MIN);
      }
    }
  }
  
  private void rafraichirBarreBoutonNavigation() {
    // S'il n'y a pas d'image à afficher.
    if (getModele().getNombrePhotoDansDiaporama() <= 0) {
      // On contrôle si la valeur est différente de la valeur actuelle.
      if (!snBarreBouton.isModeNavigation()) {
        return;
      }
      // On désactive la navigation.
      snBarreBouton.setModeNavigation(false);
    }

    else {
      // On contrôle si la valeur liée au mode de navigation est différente de la valeur actuelle.
      if (!snBarreBouton.isModeNavigation()) {
        snBarreBouton.setModeNavigation(true);
      }
      // On contrôle si le nombre d'images dans le diaporama est différente de la valeur actuelle.
      if (snBarreBouton.getCompteurMax() != getModele().getNombrePhotoDansDiaporama()) {
        snBarreBouton.setCompteurMax(getModele().getNombrePhotoDansDiaporama());
      }
      
      // On contrôle si l'index de l'image à afficher est différent de la valeur actuelle.
      // La numérotation des photos commence à 1 mais celle des index commence à 0, on ajoute donc 1 à l'index.
      if (snBarreBouton.getCompteurCourant() != getModele().getIndexImagePourAffichage() + 1) {
        snBarreBouton.setCompteurCourant(getModele().getIndexImagePourAffichage() + 1);
      }
    }
  }

  private void rafraichirBoutonZoomPlus() {
    // S'il n'y a pas d'image à afficher.
    if (getModele().getNombrePhotoDansDiaporama() <= 0) {
      // On contrôle si la valeur est différente de la valeur actuelle.
      if (!snBarreBouton.getBouton(BOUTON_ZOOM_PLUS).isEnabled()) {
        return;
      }
      // On masque les boutons de zoom.
      snBarreBouton.getBouton(BOUTON_ZOOM_PLUS).setEnabled(false);
      snBarreBouton.getBouton(BOUTON_ZOOM_PLUS).setVisible(false);
    }
    
    else {
      snBarreBouton.getBouton(BOUTON_ZOOM_PLUS).setVisible(true);
      // On contrôle si la valeur est différente de la valeur actuelle.
      if (snBarreBouton.getBouton(BOUTON_ZOOM_PLUS).isEnabled() == !getModele().isZoomMaxi()) {
        return;
      }
      // On active les boutons de zoom si on n'a pas atteint le zoom maximum.
      snBarreBouton.getBouton(BOUTON_ZOOM_PLUS).setEnabled(!getModele().isZoomMaxi());
    }
  }

  private void rafraichirBoutonZoomMoins() {
    // S'il n'y a pas d'image à afficher.
    if (getModele().getNombrePhotoDansDiaporama() <= 0) {
      // On contrôle si la valeur est différente de la valeur actuelle.
      if (!snBarreBouton.getBouton(BOUTON_ZOOM_MOINS).isEnabled()) {
        return;
      }
      // On masque les boutons de zoom.
      snBarreBouton.getBouton(BOUTON_ZOOM_MOINS).setEnabled(false);
      snBarreBouton.getBouton(BOUTON_ZOOM_MOINS).setVisible(false);
    }
    
    else {
      snBarreBouton.getBouton(BOUTON_ZOOM_MOINS).setVisible(true);
      // On contrôle si la valeur est différente de la valeur actuelle.
      if (snBarreBouton.getBouton(BOUTON_ZOOM_MOINS).isEnabled() == !getModele().isZoomMini()) {
        return;
      }
      // On active les boutons de zoom si on n'a pas atteint le zoom minimum.
      snBarreBouton.getBouton(BOUTON_ZOOM_MOINS).setEnabled(!getModele().isZoomMini());
    }
  }
  
  private void rafraichirBoutonAjouter() {
    // On contrôle si la valeur est différente de la valeur actuelle.
    if (snBarreBouton.getBouton(BOUTON_AJOUTER).isEnabled() == !isConsultation) {
      return;
    }
    // On affiche le bouton d'ajout seulement si la boîte de dialogue n'est pas en mode consultation.
    snBarreBouton.getBouton(BOUTON_AJOUTER).setEnabled(!isConsultation);
    snBarreBouton.getBouton(BOUTON_AJOUTER).setVisible(!isConsultation);
  }
  
  private void rafraichirBoutonSupprimer() {

    // S'il n'y a pas d'image à afficher.
    if (getModele().getNombrePhotoDansDiaporama() <= 0) {
      // On contrôle si la valeur est différente de la valeur actuelle.
      if (!snBarreBouton.getBouton(BOUTON_SUPPRIMER).isEnabled()) {
        return;
      }
      // On masque le boutons de suppression.
      snBarreBouton.getBouton(BOUTON_SUPPRIMER).setEnabled(false);
      snBarreBouton.getBouton(BOUTON_SUPPRIMER).setVisible(false);
    }

    else {
      if (snBarreBouton.getBouton(BOUTON_SUPPRIMER).isEnabled() == !isConsultation) {
        // On contrôle si la valeur est différente de la valeur actuelle.
        return;
      }
      // On affiche le bouton de suppression si la boîte de dialogue n'est pas en mode consultation.
      snBarreBouton.getBouton(BOUTON_SUPPRIMER).setEnabled(!isConsultation);
      snBarreBouton.getBouton(BOUTON_SUPPRIMER).setVisible(!isConsultation);
    }
  }

  /**
   * Rafraîchir l'affichage du titre de l'image.
   */
  private void rafraichirTitre() {
    // Si la valeur est à null,
    if (getModele().getTitreImage() == null) {
      // On met le titre de l'image à blanc.
      lbTitre.setText("");
      return;
    }
    // On contrôle si la valeur est différente de la valeur actuelle.
    if (getModele().getTitreImage().equals(lbTitre.getText())) {
      return;
    }
    lbTitre.setText(getModele().getTitreImage());
  }

  /**
   * Rafraîchir l'affichage de l'image.
   */
  private void rafraichirImage() {
    // Si l'image est à null, ou si le composant d'affichage est à null,
    if (getModele().getIconImage() == null || lbImage == null) {
      // On sort de la méthode.
      return;
    }
    // On contrôle si la valeur est différente de la valeur actuelle.
    if (getModele().getIconImage().equals(lbImage.getIcon())) {
      return;
    }
    lbImage.setIcon(getModele().getIconImage());
  }

  /**
   * Gérer les clics sur les boutons.
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      // Boutons associés à la fermetur de la boîte de dialogue.
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      if (pSNBouton.isBouton(EnumBouton.FERMER) || pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      
      // Boutons de navigation dans les images du diaporama.
      if (pSNBouton.isBouton(EnumBouton.NAVIGATION_PRECEDENT)) {
        getModele().afficherImagePrecedente();
      }
      if (pSNBouton.isBouton(EnumBouton.NAVIGATION_SUIVANT)) {
        getModele().afficherImageSuivante();
      }
      
      // Boutons associés à l'ajout et à la suppression d'images.
      if (pSNBouton.isBouton(BOUTON_SUPPRIMER)) {
        getModele().supprimerImagePourAffichage();
      }
      if (pSNBouton.isBouton(BOUTON_AJOUTER)) {
        getModele().ajouterImageParExplorateur();
      }
      
      // Boutons associés aux modifications d'affichage de l'image.
      if (pSNBouton.isBouton(BOUTON_ZOOM_PLUS)) {
        getModele().gererZoomImage(ZOOM_PLUS);
      }
      if (pSNBouton.isBouton(BOUTON_ZOOM_MOINS)) {
        getModele().gererZoomImage(ZOOM_MOINS);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Gérer la redimension de la fenêtre.
   *
   * Empêche la réduction de la taille de la fenêtre sous les dimensions minimum définies.
   */
  private void thisComponentResized(ComponentEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      // Si la boîte de dialogue ne contient pas d'image,
      if (getModele().getNombrePhotoDansDiaporama() <= 0) {
        return;
      }
      getModele().gererRedimensionnerFenetre(lbImage.getHeight(), lbImage.getWidth());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlFond = new JPanel();
    snBarreBouton = new SNBarreBouton();
    lbTitre = new SNLabelTitre();
    pnlImage = new SNPanelContenu();
    lbImage = new SNLabelTitre();
    
    // ======== this ========
    setTitle("Photos li\u00e9es \u00e0 l'article");
    setModal(true);
    setName("this");
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
        thisComponentResized(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setBackground(new Color(239, 239, 222));
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ---- lbTitre ----
      lbTitre.setText("titre image");
      lbTitre.setHorizontalAlignment(SwingConstants.CENTER);
      lbTitre.setName("lbTitre");
      pnlFond.add(lbTitre, BorderLayout.NORTH);
      
      // ======== pnlImage ========
      {
        pnlImage.setName("pnlImage");
        pnlImage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlImage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlImage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlImage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlImage.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ---- lbImage ----
        lbImage.setIcon(null);
        lbImage.setHorizontalAlignment(SwingConstants.CENTER);
        lbImage.setPreferredSize(new Dimension(550, 550));
        lbImage.setMaximumSize(null);
        lbImage.setMinimumSize(null);
        lbImage.setName("lbImage");
        pnlImage.add(lbImage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlImage, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNLabelTitre lbTitre;
  private SNPanelContenu pnlImage;
  private SNLabelTitre lbImage;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
