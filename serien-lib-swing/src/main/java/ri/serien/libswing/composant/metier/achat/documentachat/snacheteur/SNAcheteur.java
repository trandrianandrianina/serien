/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.achat.documentachat.snacheteur;

import ri.serien.libcommun.gescom.personnalisation.acheteur.Acheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.IdAcheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.ListeAcheteur;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un acheteur.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'unacheteur dans le logiciel. Il gère l'affichage des affaire existantes
 * et la sélection de l'une d'entre elle.
 *
 * Utilisation :
 * - Ajouter un composant SNAcheteur à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le VGAM20FM_B1
 */
public class SNAcheteur extends SNComboBoxObjetMetier<IdAcheteur, Acheteur, ListeAcheteur> {
  
  /**
   * Constructeur par défaut
   */
  public SNAcheteur() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeAcheteur());
  }
  
  /**
   * Sélectionner un acheteur de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampAcheteur) {
    if (pChampAcheteur == null || pLexical.HostFieldGetData(pChampAcheteur) == null) {
      throw new MessageErreurException("Impossible de sélectionner l'acheteur car son code est invalide.");
    }
    
    IdAcheteur idAcheteur = null;
    String valeurAcheteur = pLexical.HostFieldGetData(pChampAcheteur);
    if (valeurAcheteur != null && !valeurAcheteur.trim().isEmpty()) {
      idAcheteur = IdAcheteur.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampAcheteur));
    }
    setIdSelection(idAcheteur);
  }
  
  /**
   * Renseigne les champs RPG correspondant à l'acheteur sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampAcheteur) {
    Acheteur acheteur = getSelection();
    if (acheteur != null) {
      pLexical.HostFieldPutData(pChampAcheteur, 0, acheteur.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampAcheteur, 0, "");
    }
  }
}
