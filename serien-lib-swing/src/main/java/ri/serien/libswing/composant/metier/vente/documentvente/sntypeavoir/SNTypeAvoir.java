/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.sntypeavoir;

import ri.serien.libcommun.gescom.personnalisation.typeavoir.IdTypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.ListeTypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.TypeAvoir;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un type d'avoir.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un type d'avoir dans le logiciel. Il gère l'affichage des
 * TypeAvoir existants et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNTypeAvoir à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 */
public class SNTypeAvoir extends SNComboBoxObjetMetier<IdTypeAvoir, TypeAvoir, ListeTypeAvoir> {
  
  /**
   * Constructeur par défaut.
   */
  public SNTypeAvoir() {
    super();
    
  }
  
  /**
   * Charger les données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeTypeAvoir());
  }
  
  /**
   * Sélectionner un TypeAvoir de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampTypeAvoir) {
    if (pChampTypeAvoir == null || pLexical.HostFieldGetData(pChampTypeAvoir) == null) {
      throw new MessageErreurException("Impossible de sélectionner le type d'avoir car son code est invalide.");
    }
    
    IdTypeAvoir idTypeAvoir = null;
    String valeurTypeAvoir = pLexical.HostFieldGetData(pChampTypeAvoir);
    if (valeurTypeAvoir != null && !valeurTypeAvoir.trim().isEmpty()) {
      idTypeAvoir = IdTypeAvoir.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampTypeAvoir));
    }
    
    setIdSelection(idTypeAvoir);
  }
  
  /**
   * Renseigner les champs RPG correspondant au TypeAvoir sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampTypeAvoir) {
    TypeAvoir typeAvoir = getSelection();
    if (typeAvoir != null) {
      pLexical.HostFieldPutData(pChampTypeAvoir, 0, typeAvoir.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampTypeAvoir, 0, "");
    }
  }
  
}
