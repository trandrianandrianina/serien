/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient;

import ri.serien.libcommun.gescom.personnalisation.categorieclient.CategorieClient;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.IdCategorieClient;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.ListeCategorieClient;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection des catégories client.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une CategorieClient dans le logiciel.
 * Il gère l'affichage des CategorieClient existantes suivant l'établissement et gère la sélection de l'une de ces CategorieClient
 *
 * Utilisation :
 * - Ajouter un composant SNCategorieClient à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM59FM_B1.
 */
public class SNCategorieClient extends SNComboBoxObjetMetier<IdCategorieClient, CategorieClient, ListeCategorieClient> {
  
  /**
   * Constructeur par défaut.
   */
  public SNCategorieClient() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeCategorieClient());
  }
  
  /**
   * Sélectionner une catégorie client de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampCategorieClient) {
    if (pChampCategorieClient == null || pLexical.HostFieldGetData(pChampCategorieClient) == null) {
      throw new MessageErreurException("Impossible de sélectionner la catégorie car son code est invalide.");
    }
    
    IdCategorieClient idCategorieClient = null;
    String valeurCategorieClient = pLexical.HostFieldGetData(pChampCategorieClient).trim();
    if (valeurCategorieClient != null && !valeurCategorieClient.trim().isEmpty() && !valeurCategorieClient.equals("**")) {
      idCategorieClient = IdCategorieClient.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampCategorieClient));
    }
    setIdSelection(idCategorieClient);
  }
  
  /**
   * Renseigner les champs RPG correspondant à la catégorie client sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampCategorieClient) {
    CategorieClient categorie = getSelection();
    if (categorie != null) {
      pLexical.HostFieldPutData(pChampCategorieClient, 0, categorie.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampCategorieClient, 0, "");
    }
  }
}
