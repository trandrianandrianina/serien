/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.commun.snunite;

import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.Unite;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une unité.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une Unite dans le logiciel. Il gère l'affichage des unités existantes et
 * la sélection de l'une d'entre elle.
 *
 * Utilisation :
 * - Ajouter un composant SNUnite à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un
 * attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la
 * configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le VGVX052F
 */
public class SNUnite extends SNComboBoxObjetMetier<IdUnite, Unite, ListeUnite> {

  private ListeUnite listeUnite = null;

  /**
   * Constructeur par défaut.
   */
  public SNUnite() {
    super();
  }

  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeUnite());
  }

  /**
   * Sélectionner une unité de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pUnite) {
    if (pUnite == null || pLexical.HostFieldGetData(pUnite) == null || pUnite.trim().isEmpty()) {
      throw new MessageErreurException("Impossible de sélectionner la devises car son code est invalide.");
    }

    IdUnite idUnite = null;
    String valeurUnite = pLexical.HostFieldGetData(pUnite);
    valeurUnite = valeurUnite.trim();
    if (valeurUnite != null && !valeurUnite.trim().isEmpty() && !valeurUnite.equals("**")) {
      String resultatUnite = valeurUnite.trim();
      idUnite = IdUnite.getInstance(resultatUnite);
    }
    setIdSelection(idUnite);
  }

  /**
   * Renseigner les champs RPG correspondant à l'unité sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pUnite) {
    renseignerChampRPG(pLexical, pUnite, false);
  }

  /**
   * Renseigner les champs RPG correspondant à l'unité sélectionnée.
   * cette méthode permet de spécifier la valeur qui est envoyée au programme RPG comme valeur "Tous" :
   * - Si pTousAvecEtoile est à false, le champ RPG contient des espaces lorsque "Tous" est sélectionné".
   * - Si pTousAvecEtoile est à true, le champ RPG contient "**" lorsque "Tous" est sélectionné".
   */
  public void renseignerChampRPG(Lexical pLexical, String pUnite, boolean pTousAvecEtoile) {
    Unite unite = getSelection();
    if (unite != null) {
      pLexical.HostFieldPutData(pUnite, 0, unite.getId().getCode());
    }
    else if (pTousAvecEtoile) {
      pLexical.HostFieldPutData(pUnite, 0, "**");
    }
    else {
      pLexical.HostFieldPutData(pUnite, 0, "");
    }
  }
}
