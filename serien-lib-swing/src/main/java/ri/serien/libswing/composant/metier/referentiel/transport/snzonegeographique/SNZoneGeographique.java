/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.transport.snzonegeographique;

import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ListeZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ZoneGeographique;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une zone géographique.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une zone géographique dans le logiciel. Il gère l'affichage des
 * zone géographique existante et la sélection de l'une d'entre elle.
 *
 * Utilisation :
 * - Ajouter un composant SNZoneGeographique à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM59FM_B1.
 */
public class SNZoneGeographique extends SNComboBoxObjetMetier<IdZoneGeographique, ZoneGeographique, ListeZoneGeographique> {
  
  /**
   * Constructeur par défaut.
   */
  public SNZoneGeographique() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeZoneGeographique());
  }
  
  /**
   * Sélectionner une zone geographique de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampZoneGeographique) {
    // Récupérer la valeur du champ
    String valeurZoneGeographique = pLexical.HostFieldGetData(pChampZoneGeographique);
    if (valeurZoneGeographique == null) {
      throw new MessageErreurException("Impossible de sélectionner la zone géographique car le nom du champ est invalide.");
    }
    
    // Construire l'identifiant de la zone géographique
    IdZoneGeographique idZoneGeographique = null;
    if (!valeurZoneGeographique.trim().isEmpty()) {
      idZoneGeographique = IdZoneGeographique.getInstance(getIdEtablissement(), valeurZoneGeographique);
    }
    
    // Sélectionner la zone géorgraphique dans la liste déroulante
    setIdSelection(idZoneGeographique);
  }
  
  /**
   * Renseigner les champs RPG correspondant à la zone geographique sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampZoneGeographique) {
    IdZoneGeographique idZonegeographique = getIdSelection();
    if (idZonegeographique != null) {
      pLexical.HostFieldPutData(pChampZoneGeographique, 0, idZonegeographique.getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampZoneGeographique, 0, "");
    }
  }
  
}
