/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.detailcalcul;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.math.BigDecimal;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.EnumTypePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametrearticle.ParametreArticle;
import ri.serien.libcommun.gescom.vente.prixvente.parametreclient.ParametreClient;
import ri.serien.libcommun.gescom.vente.prixvente.parametreconditionvente.ParametreConditionVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumModeTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.ParametreDocumentVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametreetablissement.ParametreEtablissement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.checkbox.SNCheckBoxReduit;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;

/**
 * Ce composant permet d'afficher toutes les données entrants dans le calcul de prix.
 */
public class SNDetailCalcul extends SNPanelTitre {
  // Constantes
  private static final String LIBELLE_REMISE_VALEUR = "Remise en valeur";
  private static final String LIBELLE_AJOUT_VALEUR = "Ajout en valeur";
  private static final String LIBELLE_PRIX_BASE_REMISE_HT = "Prix base remisé HT";
  private static final String LIBELLE_PRIX_BASE_REMISE_TTC = "Prix base remisé TTC";
  private static final String LIBELLE_PRIX_BASE_AUGMENTE_HT = "Prix base augmenté HT";
  private static final String LIBELLE_PRIX_BASE_AUGMENTE_TTC = "Prix base augmenté TTC";
  private static final Color COULEUR_CHAMP_REFERENCE = new Color(255, 204, 51);
  private static final String PREFIXE_ORIGINE = "= ";

  // Variables
  private ModeleDialogueDetailPrixVente modele = null;
  private boolean evenementsActifs = false;
  private ParametreEtablissement parametreEtablissement = null;
  private ParametreArticle parametreArticle = null;
  private ParametreTarif parametreTarif = null;
  private ParametreClient parametreClient = null;
  private ParametreConditionVente parametreConditionVente = null;
  private ParametreLigneVente parametreLigneVente = null;
  private ParametreDocumentVente parametreDocumentVente = null;
  private FormulePrixVente formulePrixVente = null;
  private int nombreDecimale = ArrondiPrix.DECIMALE_STANDARD;

  /**
   * Constructeur.
   */
  public SNDetailCalcul() {
    super();
    initComponents();

    // Renseigner la liste déroulante des modes d'application des taux de remises
    for (EnumModeTauxRemise modeTauxRemise : EnumModeTauxRemise.values()) {
      cbModeTauxRemise.addItem(modeTauxRemise);
    }

    // Renseigner la liste déroulante des assiettes des taux de remises
    for (EnumAssietteTauxRemise assietteTauxRemise : EnumAssietteTauxRemise.values()) {
      cbAssietteTauxRemise.addItem(assietteTauxRemise);
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Modèle associé au composant graphique.
   * @return Modèle de la boîte de dialogue.
   */
  public ModeleDialogueDetailPrixVente getModele() {
    return modele;
  }

  /**
   * Modifier le modèle associé au composant graphique.
   * @param pModele Modèle de la boîte de dialogue.
   */
  public void setModele(ModeleDialogueDetailPrixVente pModele) {
    modele = pModele;
  }

  /**
   * Indiquer si les évènements doivent être traités.
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Rafraichissement des données du composant.
   */
  public void rafraichir() {
    // Empêcher les évènements pendant le rafraichissement
    evenementsActifs = false;

    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    parametreEtablissement = getModele().getParametreEtablissement();
    parametreArticle = getModele().getParametreArticle();
    parametreTarif = getModele().getParametreTarif();
    parametreClient = getModele().getParametreClientFacture();
    parametreConditionVente = getModele().getParametreConditionVente();
    parametreLigneVente = getModele().getParametreLigneVente();
    parametreDocumentVente = getModele().getParametreDocumentVente();
    formulePrixVente = modele.getFormulePrixVente();

    // Déterminer la précision souhaitée pour l'affichage des montants
    if (formulePrixVente != null && formulePrixVente.getArrondiPrix() != null) {
      nombreDecimale = formulePrixVente.getArrondiPrix().getNombreDecimale();
    }
    else {
      nombreDecimale = ArrondiPrix.DECIMALE_STANDARD;
    }

    // Mode HT/TTC
    rafraichirModeTTC();
    rafraichirOrigineTauxTVA();
    rafraichirTauxTVA();

    // Arrondi prix
    rafraichirOrigineUniteVente();
    rafraichirUniteVente();
    rafraichirArrondiPrix();

    // Colonne tarif
    rafraichirOrigineColonneTarif();
    rafraichirColonneTarif();

    // Prix de base calcul HT
    rafraichirPrixBaseColonneTarifHT();
    rafraichirPrixBaseColonneTarifTTC();
    rafraichirPrixBaseHeriteHT();
    rafraichirPrixBaseHeriteTTC();
    rafraichirOriginePrixBaseCalcul();
    rafraichirPrixBaseCalculHT();
    rafraichirPrixBaseCalculTTC();
    
    // Remise
    rafraichirRemisable();
    rafraichirRemiseEnValeur();
    rafraichirOrigineTauxRemise();
    rafraichirTauxRemise1();
    rafraichirTauxRemise2();
    rafraichirTauxRemise3();
    rafraichirTauxRemise4();
    rafraichirTauxRemise5();
    rafraichirTauxRemise6();
    rafraichirOrigineModeTAuxRemise();
    rafraichirModeTauxRemise();
    rafraichirOrigineAssietteTauxRemise();
    rafraichirAssietteTauxRemise();
    rafraichirOrigineCoefficient();
    rafraichirCoefficient();
    rafraichirPrixBaseRemiseHT();
    rafraichirPrixBaseRemiseTTC();

    // Prix net HT
    rafraichirGratuit();
    rafraichirOriginePrixNet();
    rafraichirPrixNetHT();
    rafraichirPrixNetTTC();

    // Taux de remise unitaire
    rafraichirTauxRemiseUnitaire();

    // Montant HT
    rafraichirQuantiteUV();
    rafraichirValeurRemiseMontantHT();
    rafraichirValeurRemiseMontantTTC();
    rafraichirMontantHT();
    rafraichirMontantTVA();
    rafraichirMontantTTC();

    // Réactiver les évènements pendant le rafraichissement
    evenementsActifs = true;
  }

  /**
   * Rafraichir le mode HT ou TTC.
   */
  private void rafraichirModeTTC() {
    if (formulePrixVente != null && formulePrixVente.isModeTTC()) {
      cbModeTTC.setSelected(true);
    }
    else {
      cbModeTTC.setSelected(false);
    }
  }

  /**
   * Rafraichir l'origine du taux de TVA.
   */
  private void rafraichirOrigineTauxTVA() {
    if (formulePrixVente != null && formulePrixVente.getOrigineTauxTVA() != null) {
      tfOrigineTauxTVA.setText(PREFIXE_ORIGINE + formulePrixVente.getOrigineTauxTVA().getLibelle());
    }
    else {
      tfOrigineTauxTVA.setText("");
    }
  }

  /**
   * Rafraichir le taux de TVA.
   */
  private void rafraichirTauxTVA() {
    if (formulePrixVente != null && formulePrixVente.getTauxTVA() != null) {
      tfTauxTVA.setMontant(formulePrixVente.getTauxTVA().getTexteSansSymbole());
    }
    else {
      tfTauxTVA.setMontant("");
    }
  }

  /**
   * Rafraichir l'origine de l'unité de vente utilisée pour le calcul.
   */
  private void rafraichirOrigineUniteVente() {
    if (formulePrixVente != null && formulePrixVente.getOrigineUniteVente() != null) {
      tfOrigineUniteVente.setText(PREFIXE_ORIGINE + formulePrixVente.getOrigineUniteVente());
    }
    else {
      tfOrigineUniteVente.setText("");
    }
  }

  /**
   * Rafraichir l'unité de vente utilisée pour le calcul.
   */
  private void rafraichirArrondiPrix() {
    if (formulePrixVente != null && formulePrixVente.getArrondiPrix() != null) {
      tfArrondiPrix.setText("" + formulePrixVente.getArrondiPrix().getNombreDecimale());
    }
    else {
      tfArrondiPrix.setText("");
    }
  }

  /**
   * Rafraichir la colonne tarif utilisée pour le calcul.
   */
  private void rafraichirUniteVente() {
    if (formulePrixVente != null && formulePrixVente.getCodeUniteVente() != null) {
      tfUniteVente.setText(formulePrixVente.getCodeUniteVente());
    }
    else {
      tfUniteVente.setText("");
    }
  }

  /**
   * Rafraichir l'origine de la colonne tarif utilisée pour le calcul.
   */
  private void rafraichirOrigineColonneTarif() {
    if (formulePrixVente != null && formulePrixVente.getOrigineColonneTarif() != null) {
      tfOrigineColonneTarif.setText(PREFIXE_ORIGINE + formulePrixVente.getOrigineColonneTarif());
    }
    else {
      tfOrigineColonneTarif.setText("");
    }
  }

  /**
   * Rafraichir la colonne tarif à appliquer.
   */
  private void rafraichirColonneTarif() {
    if (formulePrixVente != null && formulePrixVente.getNumeroColonneTarif() != null) {
      Integer numeroColonne = formulePrixVente.getNumeroColonneTarif();
      if (numeroColonne >= 1 && numeroColonne <= 10) {
        cbColonneTarif.setSelectedIndex(numeroColonne);
      }
      else {
        cbColonneTarif.setSelectedIndex(-1);
      }
    }
    else {
      cbColonneTarif.setSelectedIndex(-1);
    }
  }

  /**
   * Rafraichir le prix de base HT de la colonne tarif utilisée pour le calcul.
   */
  private void rafraichirPrixBaseColonneTarifHT() {
    tfPrixBaseColonneTarifHT.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseColonneTarifHT.setNegatifAutorise(true);
    tfPrixBaseColonneTarifHT.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixBaseColonneTarifHT() != null) {
      tfPrixBaseColonneTarifHT.setMontant(formulePrixVente.getPrixBaseColonneTarifHT());
    }
    else {
      tfPrixBaseColonneTarifHT.setMontant("");
    }

    lbPrixBaseColonneTarifHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
    tfPrixBaseColonneTarifHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
  }

  /**
   * Rafraichir le prix de base TTC de la colonne tarif utilisée pour le calcul.
   */
  private void rafraichirPrixBaseColonneTarifTTC() {
    tfPrixBaseColonneTarifTTC.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseColonneTarifTTC.setNegatifAutorise(true);
    tfPrixBaseColonneTarifTTC.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixBaseColonneTarifTTC() != null) {
      tfPrixBaseColonneTarifTTC.setMontant(formulePrixVente.getPrixBaseColonneTarifTTC());
    }
    else {
      tfPrixBaseColonneTarifTTC.setMontant("");
    }

    lbPrixBaseColonneTarifTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
    tfPrixBaseColonneTarifTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
  }

  /**
   * Rafraichir le prix de base HT hérité d'une autre formule prix de vente.
   */
  private void rafraichirPrixBaseHeriteHT() {
    tfPrixBaseHeriteHT.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseHeriteHT.setNegatifAutorise(true);
    tfPrixBaseHeriteHT.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixBaseHeriteHT() != null) {
      tfPrixBaseHeriteHT.setMontant(formulePrixVente.getPrixBaseHeriteHT());
    }
    else {
      tfPrixBaseHeriteHT.setMontant("");
    }

    lbPrixBaseHeriteHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
    tfPrixBaseHeriteHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
  }

  /**
   * Rafraichir le prix de base TTC hérité d'une autre formule prix de vente.
   */
  private void rafraichirPrixBaseHeriteTTC() {
    tfPrixBaseHeriteTTC.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseHeriteTTC.setNegatifAutorise(true);
    tfPrixBaseHeriteTTC.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixBaseHeriteTTC() != null) {
      tfPrixBaseHeriteTTC.setMontant(formulePrixVente.getPrixBaseHeriteTTC());
    }
    else {
      tfPrixBaseHeriteTTC.setMontant("");
    }
    
    lbPrixBaseHeriteTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
    tfPrixBaseHeriteTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
  }
  
  /**
   * Rafraichir l'origine du prix de base utilisé pour le calcul du prix net.
   */
  private void rafraichirOriginePrixBaseCalcul() {
    if (formulePrixVente != null && formulePrixVente.getOriginePrixBaseCalcul() != null) {
      tfOriginePrixBaseCalcul.setText(PREFIXE_ORIGINE + formulePrixVente.getOriginePrixBaseCalcul());
    }
    else {
      tfOriginePrixBaseCalcul.setText("");
    }
  }

  /**
   * Rafraichir le prix de base HT utilisé pour le calcul du prix net.
   */
  private void rafraichirPrixBaseCalculHT() {
    tfPrixBaseCalculHT.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseCalculHT.setNegatifAutorise(true);
    tfPrixBaseCalculHT.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixBaseCalculHT() != null) {
      tfPrixBaseCalculHT.setMontant(formulePrixVente.getPrixBaseCalculHT());
    }
    else {
      tfPrixBaseCalculHT.setMontant("");
    }
    
    lbPrixBaseCalculHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
    tfPrixBaseCalculHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
  }

  /**
   * Rafraichir le prix de base HT utilisé pour le calcul du prix net.
   */
  private void rafraichirPrixBaseCalculTTC() {
    tfPrixBaseCalculTTC.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseCalculTTC.setNegatifAutorise(true);
    tfPrixBaseCalculTTC.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixBaseCalculTTC() != null) {
      tfPrixBaseCalculTTC.setMontant(formulePrixVente.getPrixBaseCalculTTC());
    }
    else {
      tfPrixBaseCalculTTC.setMontant("");
    }

    lbPrixBaseCalculTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
    tfPrixBaseCalculTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
  }

  /**
   * Rafraichir le caractère remisable.
   */
  private void rafraichirRemisable() {
    if (formulePrixVente != null && formulePrixVente.isRemisable()) {
      cbRemisable.setSelected(true);
    }
    else {
      cbRemisable.setSelected(false);
    }
  }

  /**
   * Rafraichir la remise/ajout en valeur utilisée pour le calcul.
   */
  private void rafraichirRemiseEnValeur() {
    tfRemiseEnValeur.setLongueurPartieDecimale(nombreDecimale);
    tfRemiseEnValeur.setNegatifAutorise(true);
    tfRemiseEnValeur.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getRemiseEnValeur() != null) {
      BigDecimal remiseEnValeur = formulePrixVente.getRemiseEnValeur();
      tfRemiseEnValeur.setMontant(remiseEnValeur);

      // C'est une remise si la valeur est négative et un ajout si la valeur est positive.
      if (remiseEnValeur.compareTo(BigDecimal.ZERO) <= 0) {
        lbRemiseEnValeur.setText(LIBELLE_REMISE_VALEUR);
      }
      else {
        lbRemiseEnValeur.setText(LIBELLE_AJOUT_VALEUR);
      }
    }
    else {
      tfRemiseEnValeur.setMontant("");
      lbRemiseEnValeur.setText(LIBELLE_REMISE_VALEUR);
    }

  }

  /**
   * Rafraichir l'origine du taux de remise utilisé pour le calcul.
   */
  private void rafraichirOrigineTauxRemise() {
    if (formulePrixVente != null && formulePrixVente.getOrigineTauxRemise() != null) {
      tfOrigineTauxRemise.setText(PREFIXE_ORIGINE + formulePrixVente.getOrigineTauxRemise());

    }
    else {
      tfOrigineTauxRemise.setText("");
    }
  }

  /**
   * Rafraichir le taux de remise 1 utilisé pour le calcul.
   */
  private void rafraichirTauxRemise1() {
    if (formulePrixVente != null && formulePrixVente.getTauxRemise1() != null) {
      tfTauxRemise1.setText(formulePrixVente.getTauxRemise1().getTexteSansSymbole());

      return;
    }
    else {
      tfTauxRemise1.setText("");
    }
  }

  /**
   * Rafraichir le taux de remise 2 utilisé pour le calcul.
   */
  private void rafraichirTauxRemise2() {
    if (formulePrixVente != null && formulePrixVente.getTauxRemise2() != null) {
      tfTauxRemise2.setText(formulePrixVente.getTauxRemise2().getTexteSansSymbole());

      return;
    }
    else {
      tfTauxRemise2.setText("");
    }
  }

  /**
   * Rafraichir le taux de remise 3 utilisé pour le calcul.
   */
  private void rafraichirTauxRemise3() {
    if (formulePrixVente != null && formulePrixVente.getTauxRemise3() != null) {
      tfTauxRemise3.setText(formulePrixVente.getTauxRemise3().getTexteSansSymbole());

      return;
    }
    else {
      tfTauxRemise3.setText("");
    }
  }

  /**
   * Rafraichir le taux de remise 4 utilisé pour le calcul.
   */
  private void rafraichirTauxRemise4() {
    if (formulePrixVente != null && formulePrixVente.getTauxRemise4() != null) {
      tfTauxRemise4.setText(formulePrixVente.getTauxRemise4().getTexteSansSymbole());

      return;
    }
    else {
      tfTauxRemise4.setText("");
    }
  }

  /**
   * Rafraichir le taux de remise 5 utilisé pour le calcul.
   */
  private void rafraichirTauxRemise5() {
    if (formulePrixVente != null && formulePrixVente.getTauxRemise5() != null) {
      tfTauxRemise5.setText(formulePrixVente.getTauxRemise5().getTexteSansSymbole());

      return;
    }
    else {
      tfTauxRemise5.setText("");
    }
  }

  /**
   * Rafraichir le taux de remise 6 utilisé pour le calcul.
   */
  private void rafraichirTauxRemise6() {
    if (formulePrixVente != null && formulePrixVente.getTauxRemise6() != null) {
      tfTauxRemise6.setText(formulePrixVente.getTauxRemise6().getTexteSansSymbole());

      return;
    }
    else {
      tfTauxRemise6.setText("");
    }
  }

  /**
   * Rafraichir l'origine du mode d'application des taux de remises utilisé pour le calcul..
   */
  private void rafraichirOrigineModeTAuxRemise() {
    if (formulePrixVente != null && formulePrixVente.getOrigineModeTauxRemise() != null) {
      tfOrigineModeTauxRemise.setText(PREFIXE_ORIGINE + formulePrixVente.getOrigineModeTauxRemise());
    }
    else {
      tfOrigineModeTauxRemise.setText("");
    }
  }

  /**
   * Rafraichir le mode d'application des taux de remises utilisé pour le calcul.
   */
  private void rafraichirModeTauxRemise() {
    if (formulePrixVente != null && formulePrixVente.getModeTauxRemise() != null) {
      cbModeTauxRemise.setSelectedItem(formulePrixVente.getModeTauxRemise());
    }
    else {
      cbModeTauxRemise.setSelectedItem(null);
    }
  }

  /**
   * Rafraichir l'origine de l'assiette des taux de remises utilisée pour le calcul.
   */
  private void rafraichirOrigineAssietteTauxRemise() {
    if (formulePrixVente != null && formulePrixVente.getOrigineAssietteTauxRemise() != null) {
      tfOrigineAssietteTauxRemise.setText(PREFIXE_ORIGINE + formulePrixVente.getOrigineAssietteTauxRemise());
    }
    else {
      tfOrigineAssietteTauxRemise.setText("");
    }
  }

  /**
   * Rafraichir l'assiette des taux de remises utilisée pour le calcul.
   */
  private void rafraichirAssietteTauxRemise() {
    if (formulePrixVente != null && formulePrixVente.getAssietteTauxRemise() != null) {
      cbAssietteTauxRemise.setSelectedItem(formulePrixVente.getAssietteTauxRemise());
    }
    else {
      cbAssietteTauxRemise.setSelectedItem(null);
    }
  }

  /**
   * Rafraichir l'origine du coefficient utilisé pour le calcul.
   */
  private void rafraichirOrigineCoefficient() {
    if (formulePrixVente != null && formulePrixVente.getOrigineCoefficient() != null) {
      tfOrigineCoefficient.setText(PREFIXE_ORIGINE + formulePrixVente.getOrigineCoefficient());
    }
    else {
      tfOrigineCoefficient.setText("");
    }
  }

  /**
   * Rafraichir le coefficient utilisé pour le calcul.
   */
  private void rafraichirCoefficient() {
    if (formulePrixVente != null && formulePrixVente.getCoefficient() != null) {
      tfCoefficient.setMontant(formulePrixVente.getCoefficient());
    }
    else {
      tfCoefficient.setMontant("");
    }
  }

  /**
   * Rafraichir le prix de base remisé HT utilisé pour le calcul.
   */
  private void rafraichirPrixBaseRemiseHT() {
    tfPrixBaseRemiseHT.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseRemiseHT.setNegatifAutorise(true);
    tfPrixBaseRemiseHT.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixBaseRemiseHT() != null) {
      tfPrixBaseRemiseHT.setMontant(formulePrixVente.getPrixBaseRemiseHT());
    }
    else {
      tfPrixBaseRemiseHT.setMontant("");
    }

    if (formulePrixVente.isPrixBaseAugmente()) {
      lbPrixBaseRemiseHT.setText(LIBELLE_PRIX_BASE_AUGMENTE_HT);
    }
    else {
      lbPrixBaseRemiseHT.setText(LIBELLE_PRIX_BASE_REMISE_HT);
    }
    
    lbPrixBaseRemiseHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
    tfPrixBaseRemiseHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
  }
  
  /**
   * Rafraichir le prix de base remisé TTC utilisé pour le calcul.
   */
  private void rafraichirPrixBaseRemiseTTC() {
    tfPrixBaseRemiseTTC.setLongueurPartieDecimale(nombreDecimale);
    tfPrixBaseRemiseTTC.setNegatifAutorise(true);
    tfPrixBaseRemiseTTC.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixBaseRemiseTTC() != null) {
      tfPrixBaseRemiseTTC.setMontant(formulePrixVente.getPrixBaseRemiseTTC());
    }
    else {
      tfPrixBaseRemiseTTC.setMontant("");
    }

    if (formulePrixVente.isPrixBaseAugmente()) {
      lbPrixBaseRemiseTTC.setText(LIBELLE_PRIX_BASE_AUGMENTE_TTC);
    }
    else {
      lbPrixBaseRemiseTTC.setText(LIBELLE_PRIX_BASE_REMISE_TTC);
    }

    lbPrixBaseRemiseTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
    tfPrixBaseRemiseTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
  }

  /**
   * Rafraichir le coefficient utilisé pour le calcul.
   */
  private void rafraichirGratuit() {
    if (formulePrixVente != null && formulePrixVente.isGratuit()) {
      cbGratuit.setSelected(true);
    }
    else {
      cbGratuit.setSelected(false);
    }
  }

  /**
   * Rafraichir l'origine du prix net utilisé pour le calcul.
   */
  private void rafraichirOriginePrixNet() {
    if (formulePrixVente != null && formulePrixVente.getOriginePrixNet() != null) {
      tfOriginePrixNet.setText(PREFIXE_ORIGINE + formulePrixVente.getOriginePrixNet());
    }
    else {
      tfOriginePrixNet.setText("");
    }
  }

  /**
   * Rafraichir le prix net HT utilisé pour le calcul.
   */
  private void rafraichirPrixNetHT() {
    tfPrixNetHT.setLongueurPartieDecimale(nombreDecimale);
    tfPrixNetHT.setNegatifAutorise(true);
    tfPrixNetHT.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixNetHT() != null) {
      tfPrixNetHT.setMontant(formulePrixVente.getPrixNetHT());
    }
    else {
      tfPrixNetHT.setMontant("");
    }

    if (getModele().getTypePrixVente() == EnumTypePrixVente.PRIX_NET_HT) {
      tfPrixNetHT.setOpaque(true);
      tfPrixNetHT.setBackground(COULEUR_CHAMP_REFERENCE);
    }
    else {
      tfPrixNetHT.setOpaque(false);
    }
    
    lbPrixNetHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
    tfPrixNetHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
  }

  /**
   * Rafraichir le prix net TTC.
   */
  private void rafraichirPrixNetTTC() {
    tfPrixNetTTC.setLongueurPartieDecimale(nombreDecimale);
    tfPrixNetTTC.setNegatifAutorise(true);
    tfPrixNetTTC.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getPrixNetTTC() != null) {
      tfPrixNetTTC.setMontant(formulePrixVente.getPrixNetTTC());
    }
    else {
      tfPrixNetTTC.setMontant("");
    }

    if (getModele().getTypePrixVente() == EnumTypePrixVente.PRIX_NET_TTC) {
      tfPrixNetTTC.setOpaque(true);
      tfPrixNetTTC.setBackground(COULEUR_CHAMP_REFERENCE);
    }
    else {
      tfPrixNetTTC.setOpaque(false);
    }

    lbPrixNetTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
    tfPrixNetTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
  }

  /**
   * Rafraichir le taux de remise unitaire.
   */
  private void rafraichirTauxRemiseUnitaire() {
    if (formulePrixVente != null && formulePrixVente.getTauxRemiseUnitaire() != null) {
      tfTauxRemiseUnitaire.setText(formulePrixVente.getTauxRemiseUnitaire().getTexteSansSymbole());
    }
    else {
      tfTauxRemiseUnitaire.setText("");
    }
  }

  /**
   * Rafraichir la quantité d'article en UV.
   */
  private void rafraichirQuantiteUV() {
    if (formulePrixVente != null && formulePrixVente.getQuantiteUV() != null) {
      tfQuantiteUV.setMontant(formulePrixVente.getQuantiteUV());
    }
    else {
      tfQuantiteUV.setMontant("");
    }
  }

  /**
   * Rafraichir la valeur de la remise sur le montant HT.
   */
  private void rafraichirValeurRemiseMontantHT() {
    tfValeurRemiseMontantHT.setLongueurPartieDecimale(nombreDecimale);
    tfValeurRemiseMontantHT.setNegatifAutorise(true);
    tfValeurRemiseMontantHT.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getValeurRemiseMontantHT() != null) {
      tfValeurRemiseMontantHT.setMontant(formulePrixVente.getValeurRemiseMontantHT());
    }
    else {
      tfValeurRemiseMontantHT.setMontant("");
    }

    lbValeurRemiseMontantHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
    tfValeurRemiseMontantHT.setVisible(formulePrixVente == null || !formulePrixVente.isModeTTC() || getModele().isPrixHTVisible());
  }

  /**
   * Rafraichir la valeur de la remise sur le montant HT.
   */
  private void rafraichirValeurRemiseMontantTTC() {
    tfValeurRemiseMontantTTC.setLongueurPartieDecimale(nombreDecimale);
    tfValeurRemiseMontantTTC.setNegatifAutorise(true);
    tfValeurRemiseMontantTTC.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getValeurRemiseMontantTTC() != null) {
      tfValeurRemiseMontantTTC.setMontant(formulePrixVente.getValeurRemiseMontantTTC());
    }
    else {
      tfValeurRemiseMontantTTC.setMontant("");
    }
    
    lbValeurRemiseMontantTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
    tfValeurRemiseMontantTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
  }
  
  /**
   * Rafraichir le montant HT.
   */
  private void rafraichirMontantHT() {
    tfMontantHT.setLongueurPartieDecimale(nombreDecimale);
    tfMontantHT.setNegatifAutorise(true);
    tfMontantHT.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getMontantHT() != null) {
      tfMontantHT.setMontant(formulePrixVente.getMontantHT());
    }
    else {
      tfMontantHT.setMontant("");
    }

    if (getModele().getTypePrixVente() == EnumTypePrixVente.MONTANT_HT) {
      tfMontantHT.setOpaque(true);
      tfMontantHT.setBackground(COULEUR_CHAMP_REFERENCE);
    }
    else {
      tfMontantHT.setOpaque(false);
    }
  }

  /**
   * Rafraichir le montant de la TVA.
   */
  private void rafraichirMontantTVA() {
    tfMontantTVA.setLongueurPartieDecimale(nombreDecimale);
    tfMontantTVA.setNegatifAutorise(true);
    tfMontantTVA.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getMontantTVA() != null) {
      tfMontantTVA.setMontant(formulePrixVente.getMontantTVA());
    }
    else {
      tfMontantTVA.setMontant("");
    }

    lbMontantTVA.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
    tfMontantTVA.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
  }

  /**
   * Rafraichir le montant TTC.
   */
  private void rafraichirMontantTTC() {
    tfMontantTTC.setLongueurPartieDecimale(nombreDecimale);
    tfMontantTTC.setNegatifAutorise(true);
    tfMontantTTC.setModeNegatif(true);

    if (formulePrixVente != null && formulePrixVente.getMontantTTC() != null) {
      tfMontantTTC.setMontant(formulePrixVente.getMontantTTC());
    }
    else {
      tfMontantTTC.setMontant("");
    }

    if (getModele().getTypePrixVente() == EnumTypePrixVente.MONTANT_TTC) {
      tfMontantTTC.setOpaque(true);
      tfMontantTTC.setBackground(COULEUR_CHAMP_REFERENCE);
    }
    else {
      tfMontantTTC.setOpaque(false);
    }

    lbMontantTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
    tfMontantTTC.setVisible(formulePrixVente != null && formulePrixVente.isModeTTC());
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------

  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbModeTTC = new SNLabelChamp();
    cbModeTTC = new SNCheckBoxReduit();
    lbOrigineTauxTVA = new SNLabelChamp();
    tfOrigineTauxTVA = new SNLabelUnite();
    lbTauxTVA = new SNLabelChamp();
    tfTauxTVA = new SNMontant();
    lbOrigineUniteVente = new SNLabelChamp();
    tfOrigineUniteVente = new JLabel();
    lbUniteVente = new SNLabelChamp();
    tfUniteVente = new SNTexte();
    lbArrondiPrix = new SNLabelChamp();
    tfArrondiPrix = new SNTexte();
    lbOrigineColonneTarif = new SNLabelChamp();
    tfOrigineColonneTarif = new SNLabelUnite();
    lbColonneTarif = new SNLabelChamp();
    cbColonneTarif = new SNComboBoxReduit();
    lbPrixBaseColonneTarifHT = new SNLabelChamp();
    tfPrixBaseColonneTarifHT = new SNMontant();
    lbPrixBaseColonneTarifTTC = new SNLabelChamp();
    tfPrixBaseColonneTarifTTC = new SNMontant();
    lbPrixBaseHeriteHT = new SNLabelChamp();
    tfPrixBaseHeriteHT = new SNMontant();
    lbPrixBaseHeriteTTC = new SNLabelChamp();
    tfPrixBaseHeriteTTC = new SNMontant();
    lbOriginePrixBaseCalcul = new SNLabelChamp();
    tfOriginePrixBaseCalcul = new SNLabelUnite();
    lbPrixBaseCalculHT = new SNLabelChamp();
    tfPrixBaseCalculHT = new SNMontant();
    lbPrixBaseCalculTTC = new SNLabelChamp();
    tfPrixBaseCalculTTC = new SNMontant();
    lbRemisable = new SNLabelChamp();
    cbRemisable = new SNCheckBoxReduit();
    lbRemiseEnValeur = new SNLabelChamp();
    tfRemiseEnValeur = new SNMontant();
    lbOrigineTauxRemise = new SNLabelChamp();
    tfOrigineTauxRemise = new SNLabelUnite();
    lbTauxRemise = new SNLabelChamp();
    pnlTauxRemise = new SNPanel();
    tfTauxRemise1 = new SNTexte();
    tfTauxRemise2 = new SNTexte();
    tfTauxRemise3 = new SNTexte();
    tfTauxRemise4 = new SNTexte();
    tfTauxRemise5 = new SNTexte();
    tfTauxRemise6 = new SNTexte();
    lbOrigineModeTauxRemise = new SNLabelChamp();
    tfOrigineModeTauxRemise = new SNLabelUnite();
    lbModeTauxRemise = new SNLabelChamp();
    cbModeTauxRemise = new SNComboBoxReduit();
    lbOrigineAssietteTauxRemise = new SNLabelChamp();
    tfOrigineAssietteTauxRemise = new SNLabelUnite();
    lbAssietteTauxRemise = new SNLabelChamp();
    cbAssietteTauxRemise = new SNComboBoxReduit();
    lbOrigineCoefficient = new SNLabelChamp();
    tfOrigineCoefficient = new SNLabelUnite();
    lbCoefficient = new SNLabelChamp();
    tfCoefficient = new SNMontant();
    lbPrixBaseRemiseHT = new SNLabelChamp();
    tfPrixBaseRemiseHT = new SNMontant();
    lbPrixBaseRemiseTTC = new SNLabelChamp();
    tfPrixBaseRemiseTTC = new SNMontant();
    lbGratuit = new SNLabelChamp();
    cbGratuit = new SNCheckBoxReduit();
    lbOriginePrixNet = new SNLabelChamp();
    tfOriginePrixNet = new SNLabelUnite();
    lbPrixNetHT = new SNLabelChamp();
    tfPrixNetHT = new SNMontant();
    lbPrixNetTTC = new SNLabelChamp();
    tfPrixNetTTC = new SNMontant();
    lbTauxRemiseUnitaire = new SNLabelChamp();
    tfTauxRemiseUnitaire = new SNTexte();
    lbQuantiteUV = new SNLabelChamp();
    tfQuantiteUV = new SNMontant();
    lbValeurRemiseMontantHT = new SNLabelChamp();
    tfValeurRemiseMontantHT = new SNMontant();
    lbValeurRemiseMontantTTC = new SNLabelChamp();
    tfValeurRemiseMontantTTC = new SNMontant();
    lbMontantHT = new SNLabelChamp();
    tfMontantHT = new SNMontant();
    lbMontantTVA = new SNLabelChamp();
    tfMontantTVA = new SNMontant();
    lbMontantTTC = new SNLabelChamp();
    tfMontantTTC = new SNMontant();
    
    // ======== this ========
    setTitre("D\u00e9tail du calcul");
    setModeReduit(true);
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 143, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights =
        new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    
    // ---- lbModeTTC ----
    lbModeTTC.setText("Mode TTC");
    lbModeTTC.setModeReduit(true);
    lbModeTTC.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbModeTTC.setName("lbModeTTC");
    add(lbModeTTC,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbModeTTC ----
    cbModeTTC.setEnabled(false);
    cbModeTTC.setName("cbModeTTC");
    add(cbModeTTC,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOrigineTauxTVA ----
    lbOrigineTauxTVA.setText("Origine taux TVA");
    lbOrigineTauxTVA.setModeReduit(true);
    lbOrigineTauxTVA.setName("lbOrigineTauxTVA");
    add(lbOrigineTauxTVA,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOrigineTauxTVA ----
    tfOrigineTauxTVA.setText("-");
    tfOrigineTauxTVA.setFont(new Font("sansserif", Font.PLAIN, 11));
    tfOrigineTauxTVA.setModeReduit(true);
    tfOrigineTauxTVA.setName("tfOrigineTauxTVA");
    add(tfOrigineTauxTVA,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbTauxTVA ----
    lbTauxTVA.setText("Taux TVA (%)");
    lbTauxTVA.setModeReduit(true);
    lbTauxTVA.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbTauxTVA.setName("lbTauxTVA");
    add(lbTauxTVA,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfTauxTVA ----
    tfTauxTVA.setModeReduit(true);
    tfTauxTVA.setEnabled(false);
    tfTauxTVA.setName("tfTauxTVA");
    add(tfTauxTVA,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOrigineUniteVente ----
    lbOrigineUniteVente.setText("Origine unit\u00e9 de vente");
    lbOrigineUniteVente.setModeReduit(true);
    lbOrigineUniteVente.setName("lbOrigineUniteVente");
    add(lbOrigineUniteVente,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOrigineUniteVente ----
    tfOrigineUniteVente.setText("-");
    tfOrigineUniteVente.setFont(new Font("sansserif", Font.PLAIN, 11));
    tfOrigineUniteVente.setName("tfOrigineUniteVente");
    add(tfOrigineUniteVente,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbUniteVente ----
    lbUniteVente.setText("Unit\u00e9 de vente");
    lbUniteVente.setModeReduit(true);
    lbUniteVente.setName("lbUniteVente");
    add(lbUniteVente,
        new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfUniteVente ----
    tfUniteVente.setEnabled(false);
    tfUniteVente.setModeReduit(true);
    tfUniteVente.setName("tfUniteVente");
    add(tfUniteVente,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbArrondiPrix ----
    lbArrondiPrix.setText("Arrondi prix");
    lbArrondiPrix.setModeReduit(true);
    lbArrondiPrix.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbArrondiPrix.setName("lbArrondiPrix");
    add(lbArrondiPrix,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfArrondiPrix ----
    tfArrondiPrix.setEnabled(false);
    tfArrondiPrix.setModeReduit(true);
    tfArrondiPrix.setName("tfArrondiPrix");
    add(tfArrondiPrix,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOrigineColonneTarif ----
    lbOrigineColonneTarif.setText("Origine colonne tarif");
    lbOrigineColonneTarif.setModeReduit(true);
    lbOrigineColonneTarif.setName("lbOrigineColonneTarif");
    add(lbOrigineColonneTarif,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOrigineColonneTarif ----
    tfOrigineColonneTarif.setText("-");
    tfOrigineColonneTarif.setFont(new Font("sansserif", Font.PLAIN, 11));
    tfOrigineColonneTarif.setModeReduit(true);
    tfOrigineColonneTarif.setVerticalAlignment(SwingConstants.CENTER);
    tfOrigineColonneTarif.setName("tfOrigineColonneTarif");
    add(tfOrigineColonneTarif,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbColonneTarif ----
    lbColonneTarif.setText("Colonne tarif");
    lbColonneTarif.setFont(new Font("sansserif", Font.PLAIN, 11));
    lbColonneTarif.setModeReduit(true);
    lbColonneTarif.setVerticalAlignment(SwingConstants.CENTER);
    lbColonneTarif.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbColonneTarif.setName("lbColonneTarif");
    add(lbColonneTarif,
        new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbColonneTarif ----
    cbColonneTarif.setModel(new DefaultComboBoxModel<>(new String[] { "Aucune", "Colonne 1", "Colonne 2", "Colonne 3", "Colonne 4",
        "Colonne 5", "Colonne 6", "Colonne 7", "Colonne 8", "Colonne 9", "Colonne 10" }));
    cbColonneTarif.setEnabled(false);
    cbColonneTarif.setSelectedIndex(-1);
    cbColonneTarif.setName("cbColonneTarif");
    add(cbColonneTarif,
        new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseColonneTarifHT ----
    lbPrixBaseColonneTarifHT.setText("Prix base colonne tarif HT");
    lbPrixBaseColonneTarifHT.setModeReduit(true);
    lbPrixBaseColonneTarifHT.setName("lbPrixBaseColonneTarifHT");
    add(lbPrixBaseColonneTarifHT,
        new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseColonneTarifHT ----
    tfPrixBaseColonneTarifHT.setEnabled(false);
    tfPrixBaseColonneTarifHT.setModeReduit(true);
    tfPrixBaseColonneTarifHT.setName("tfPrixBaseColonneTarifHT");
    add(tfPrixBaseColonneTarifHT,
        new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseColonneTarifTTC ----
    lbPrixBaseColonneTarifTTC.setText("Prix base colonne tarif TTC");
    lbPrixBaseColonneTarifTTC.setModeReduit(true);
    lbPrixBaseColonneTarifTTC.setName("lbPrixBaseColonneTarifTTC");
    add(lbPrixBaseColonneTarifTTC,
        new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseColonneTarifTTC ----
    tfPrixBaseColonneTarifTTC.setEnabled(false);
    tfPrixBaseColonneTarifTTC.setModeReduit(true);
    tfPrixBaseColonneTarifTTC.setName("tfPrixBaseColonneTarifTTC");
    add(tfPrixBaseColonneTarifTTC,
        new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseHeriteHT ----
    lbPrixBaseHeriteHT.setText("Prix base h\u00e9rit\u00e9 HT");
    lbPrixBaseHeriteHT.setModeReduit(true);
    lbPrixBaseHeriteHT.setName("lbPrixBaseHeriteHT");
    add(lbPrixBaseHeriteHT,
        new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseHeriteHT ----
    tfPrixBaseHeriteHT.setEnabled(false);
    tfPrixBaseHeriteHT.setModeReduit(true);
    tfPrixBaseHeriteHT.setName("tfPrixBaseHeriteHT");
    add(tfPrixBaseHeriteHT, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseHeriteTTC ----
    lbPrixBaseHeriteTTC.setText("Prix base h\u00e9rit\u00e9 TTC");
    lbPrixBaseHeriteTTC.setModeReduit(true);
    lbPrixBaseHeriteTTC.setName("lbPrixBaseHeriteTTC");
    add(lbPrixBaseHeriteTTC,
        new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseHeriteTTC ----
    tfPrixBaseHeriteTTC.setEnabled(false);
    tfPrixBaseHeriteTTC.setModeReduit(true);
    tfPrixBaseHeriteTTC.setName("tfPrixBaseHeriteTTC");
    add(tfPrixBaseHeriteTTC, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOriginePrixBaseCalcul ----
    lbOriginePrixBaseCalcul.setText("Origine prix base");
    lbOriginePrixBaseCalcul.setModeReduit(true);
    lbOriginePrixBaseCalcul.setName("lbOriginePrixBaseCalcul");
    add(lbOriginePrixBaseCalcul,
        new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOriginePrixBaseCalcul ----
    tfOriginePrixBaseCalcul.setText("-");
    tfOriginePrixBaseCalcul.setFont(new Font("sansserif", Font.PLAIN, 11));
    tfOriginePrixBaseCalcul.setModeReduit(true);
    tfOriginePrixBaseCalcul.setVerticalAlignment(SwingConstants.CENTER);
    tfOriginePrixBaseCalcul.setName("tfOriginePrixBaseCalcul");
    add(tfOriginePrixBaseCalcul,
        new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseCalculHT ----
    lbPrixBaseCalculHT.setText("Prix base HT");
    lbPrixBaseCalculHT.setFont(new Font("sansserif", Font.PLAIN, 11));
    lbPrixBaseCalculHT.setModeReduit(true);
    lbPrixBaseCalculHT.setVerticalAlignment(SwingConstants.CENTER);
    lbPrixBaseCalculHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbPrixBaseCalculHT.setName("lbPrixBaseCalculHT");
    add(lbPrixBaseCalculHT,
        new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseCalculHT ----
    tfPrixBaseCalculHT.setEnabled(false);
    tfPrixBaseCalculHT.setModeReduit(true);
    tfPrixBaseCalculHT.setName("tfPrixBaseCalculHT");
    add(tfPrixBaseCalculHT, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseCalculTTC ----
    lbPrixBaseCalculTTC.setText("Prix base TTC");
    lbPrixBaseCalculTTC.setFont(new Font("sansserif", Font.PLAIN, 11));
    lbPrixBaseCalculTTC.setModeReduit(true);
    lbPrixBaseCalculTTC.setVerticalAlignment(SwingConstants.CENTER);
    lbPrixBaseCalculTTC.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbPrixBaseCalculTTC.setName("lbPrixBaseCalculTTC");
    add(lbPrixBaseCalculTTC,
        new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseCalculTTC ----
    tfPrixBaseCalculTTC.setEnabled(false);
    tfPrixBaseCalculTTC.setModeReduit(true);
    tfPrixBaseCalculTTC.setName("tfPrixBaseCalculTTC");
    add(tfPrixBaseCalculTTC, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbRemisable ----
    lbRemisable.setModeReduit(true);
    lbRemisable.setText("Remisable");
    lbRemisable.setMinimumSize(new Dimension(30, 22));
    lbRemisable.setPreferredSize(new Dimension(30, 22));
    lbRemisable.setName("lbRemisable");
    add(lbRemisable,
        new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbRemisable ----
    cbRemisable.setEnabled(false);
    cbRemisable.setName("cbRemisable");
    add(cbRemisable, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbRemiseEnValeur ----
    lbRemiseEnValeur.setText("Remise en valeur");
    lbRemiseEnValeur.setModeReduit(true);
    lbRemiseEnValeur.setName("lbRemiseEnValeur");
    add(lbRemiseEnValeur,
        new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfRemiseEnValeur ----
    tfRemiseEnValeur.setEnabled(false);
    tfRemiseEnValeur.setModeReduit(true);
    tfRemiseEnValeur.setName("tfRemiseEnValeur");
    add(tfRemiseEnValeur, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOrigineTauxRemise ----
    lbOrigineTauxRemise.setText("Origine taux remise ");
    lbOrigineTauxRemise.setModeReduit(true);
    lbOrigineTauxRemise.setName("lbOrigineTauxRemise");
    add(lbOrigineTauxRemise,
        new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOrigineTauxRemise ----
    tfOrigineTauxRemise.setText("-");
    tfOrigineTauxRemise.setModeReduit(true);
    tfOrigineTauxRemise.setName("tfOrigineTauxRemise");
    add(tfOrigineTauxRemise,
        new GridBagConstraints(1, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbTauxRemise ----
    lbTauxRemise.setText("Taux remise (%)");
    lbTauxRemise.setFont(new Font("sansserif", Font.PLAIN, 11));
    lbTauxRemise.setModeReduit(true);
    lbTauxRemise.setVerticalAlignment(SwingConstants.CENTER);
    lbTauxRemise.setName("lbTauxRemise");
    add(lbTauxRemise, new GridBagConstraints(0, 18, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 3), 0, 0));
    
    // ======== pnlTauxRemise ========
    {
      pnlTauxRemise.setName("pnlTauxRemise");
      pnlTauxRemise.setLayout(new GridLayout(2, 3, 3, 0));
      
      // ---- tfTauxRemise1 ----
      tfTauxRemise1.setEnabled(false);
      tfTauxRemise1.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise1.setModeReduit(true);
      tfTauxRemise1.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise1.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise1.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise1.setName("tfTauxRemise1");
      pnlTauxRemise.add(tfTauxRemise1);
      
      // ---- tfTauxRemise2 ----
      tfTauxRemise2.setEnabled(false);
      tfTauxRemise2.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise2.setModeReduit(true);
      tfTauxRemise2.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise2.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise2.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise2.setName("tfTauxRemise2");
      pnlTauxRemise.add(tfTauxRemise2);
      
      // ---- tfTauxRemise3 ----
      tfTauxRemise3.setEnabled(false);
      tfTauxRemise3.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise3.setModeReduit(true);
      tfTauxRemise3.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise3.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise3.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise3.setName("tfTauxRemise3");
      pnlTauxRemise.add(tfTauxRemise3);
      
      // ---- tfTauxRemise4 ----
      tfTauxRemise4.setEnabled(false);
      tfTauxRemise4.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise4.setModeReduit(true);
      tfTauxRemise4.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise4.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise4.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise4.setName("tfTauxRemise4");
      pnlTauxRemise.add(tfTauxRemise4);
      
      // ---- tfTauxRemise5 ----
      tfTauxRemise5.setEnabled(false);
      tfTauxRemise5.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise5.setModeReduit(true);
      tfTauxRemise5.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise5.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise5.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise5.setName("tfTauxRemise5");
      pnlTauxRemise.add(tfTauxRemise5);
      
      // ---- tfTauxRemise6 ----
      tfTauxRemise6.setEnabled(false);
      tfTauxRemise6.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise6.setModeReduit(true);
      tfTauxRemise6.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise6.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise6.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise6.setName("tfTauxRemise6");
      pnlTauxRemise.add(tfTauxRemise6);
    }
    add(pnlTauxRemise,
        new GridBagConstraints(1, 18, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOrigineModeTauxRemise ----
    lbOrigineModeTauxRemise.setText("Origine mode taux remise");
    lbOrigineModeTauxRemise.setModeReduit(true);
    lbOrigineModeTauxRemise.setName("lbOrigineModeTauxRemise");
    add(lbOrigineModeTauxRemise,
        new GridBagConstraints(0, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOrigineModeTauxRemise ----
    tfOrigineModeTauxRemise.setText("-");
    tfOrigineModeTauxRemise.setModeReduit(true);
    tfOrigineModeTauxRemise.setName("tfOrigineModeTauxRemise");
    add(tfOrigineModeTauxRemise,
        new GridBagConstraints(1, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbModeTauxRemise ----
    lbModeTauxRemise.setText("Mode taux remise");
    lbModeTauxRemise.setModeReduit(true);
    lbModeTauxRemise.setName("lbModeTauxRemise");
    add(lbModeTauxRemise,
        new GridBagConstraints(0, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbModeTauxRemise ----
    cbModeTauxRemise.setEnabled(false);
    cbModeTauxRemise.setName("cbModeTauxRemise");
    add(cbModeTauxRemise,
        new GridBagConstraints(1, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOrigineAssietteTauxRemise ----
    lbOrigineAssietteTauxRemise.setText("Origine assiette taux remise");
    lbOrigineAssietteTauxRemise.setModeReduit(true);
    lbOrigineAssietteTauxRemise.setName("lbOrigineAssietteTauxRemise");
    add(lbOrigineAssietteTauxRemise,
        new GridBagConstraints(0, 21, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOrigineAssietteTauxRemise ----
    tfOrigineAssietteTauxRemise.setText("-");
    tfOrigineAssietteTauxRemise.setModeReduit(true);
    tfOrigineAssietteTauxRemise.setName("tfOrigineAssietteTauxRemise");
    add(tfOrigineAssietteTauxRemise,
        new GridBagConstraints(1, 21, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbAssietteTauxRemise ----
    lbAssietteTauxRemise.setText("Assiette taux remise");
    lbAssietteTauxRemise.setModeReduit(true);
    lbAssietteTauxRemise.setName("lbAssietteTauxRemise");
    add(lbAssietteTauxRemise,
        new GridBagConstraints(0, 22, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbAssietteTauxRemise ----
    cbAssietteTauxRemise.setEnabled(false);
    cbAssietteTauxRemise.setName("cbAssietteTauxRemise");
    add(cbAssietteTauxRemise,
        new GridBagConstraints(1, 22, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOrigineCoefficient ----
    lbOrigineCoefficient.setText("Origine coefficient");
    lbOrigineCoefficient.setModeReduit(true);
    lbOrigineCoefficient.setName("lbOrigineCoefficient");
    add(lbOrigineCoefficient,
        new GridBagConstraints(0, 23, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOrigineCoefficient ----
    tfOrigineCoefficient.setText("-");
    tfOrigineCoefficient.setFont(new Font("sansserif", Font.PLAIN, 11));
    tfOrigineCoefficient.setModeReduit(true);
    tfOrigineCoefficient.setName("tfOrigineCoefficient");
    add(tfOrigineCoefficient,
        new GridBagConstraints(1, 23, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbCoefficient ----
    lbCoefficient.setText("Coefficient");
    lbCoefficient.setModeReduit(true);
    lbCoefficient.setName("lbCoefficient");
    add(lbCoefficient,
        new GridBagConstraints(0, 24, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfCoefficient ----
    tfCoefficient.setModeReduit(true);
    tfCoefficient.setEnabled(false);
    tfCoefficient.setName("tfCoefficient");
    add(tfCoefficient, new GridBagConstraints(1, 24, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseRemiseHT ----
    lbPrixBaseRemiseHT.setText("Prix base remis\u00e9 HT");
    lbPrixBaseRemiseHT.setModeReduit(true);
    lbPrixBaseRemiseHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbPrixBaseRemiseHT.setName("lbPrixBaseRemiseHT");
    add(lbPrixBaseRemiseHT,
        new GridBagConstraints(0, 25, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseRemiseHT ----
    tfPrixBaseRemiseHT.setEnabled(false);
    tfPrixBaseRemiseHT.setModeReduit(true);
    tfPrixBaseRemiseHT.setName("tfPrixBaseRemiseHT");
    add(tfPrixBaseRemiseHT, new GridBagConstraints(1, 25, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixBaseRemiseTTC ----
    lbPrixBaseRemiseTTC.setText("Prix base remis\u00e9 TTC");
    lbPrixBaseRemiseTTC.setModeReduit(true);
    lbPrixBaseRemiseTTC.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbPrixBaseRemiseTTC.setName("lbPrixBaseRemiseTTC");
    add(lbPrixBaseRemiseTTC,
        new GridBagConstraints(0, 26, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixBaseRemiseTTC ----
    tfPrixBaseRemiseTTC.setEnabled(false);
    tfPrixBaseRemiseTTC.setModeReduit(true);
    tfPrixBaseRemiseTTC.setName("tfPrixBaseRemiseTTC");
    add(tfPrixBaseRemiseTTC, new GridBagConstraints(1, 26, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbGratuit ----
    lbGratuit.setModeReduit(true);
    lbGratuit.setText("Gratuit");
    lbGratuit.setMinimumSize(new Dimension(30, 22));
    lbGratuit.setPreferredSize(new Dimension(30, 22));
    lbGratuit.setName("lbGratuit");
    add(lbGratuit,
        new GridBagConstraints(0, 27, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbGratuit ----
    cbGratuit.setEnabled(false);
    cbGratuit.setName("cbGratuit");
    add(cbGratuit, new GridBagConstraints(1, 27, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbOriginePrixNet ----
    lbOriginePrixNet.setText("Origine prix net");
    lbOriginePrixNet.setModeReduit(true);
    lbOriginePrixNet.setName("lbOriginePrixNet");
    add(lbOriginePrixNet,
        new GridBagConstraints(0, 28, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfOriginePrixNet ----
    tfOriginePrixNet.setText("-");
    tfOriginePrixNet.setFont(new Font("sansserif", Font.BOLD, 11));
    tfOriginePrixNet.setModeReduit(true);
    tfOriginePrixNet.setName("tfOriginePrixNet");
    add(tfOriginePrixNet,
        new GridBagConstraints(1, 28, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixNetHT ----
    lbPrixNetHT.setText("Prix net HT");
    lbPrixNetHT.setFont(new Font("sansserif", Font.PLAIN, 11));
    lbPrixNetHT.setVerticalAlignment(SwingConstants.CENTER);
    lbPrixNetHT.setModeReduit(true);
    lbPrixNetHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbPrixNetHT.setName("lbPrixNetHT");
    add(lbPrixNetHT,
        new GridBagConstraints(0, 29, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixNetHT ----
    tfPrixNetHT.setEnabled(false);
    tfPrixNetHT.setModeReduit(true);
    tfPrixNetHT.setName("tfPrixNetHT");
    add(tfPrixNetHT, new GridBagConstraints(1, 29, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPrixNetTTC ----
    lbPrixNetTTC.setText("Prix net TTC");
    lbPrixNetTTC.setModeReduit(true);
    lbPrixNetTTC.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbPrixNetTTC.setName("lbPrixNetTTC");
    add(lbPrixNetTTC,
        new GridBagConstraints(0, 30, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPrixNetTTC ----
    tfPrixNetTTC.setEnabled(false);
    tfPrixNetTTC.setModeReduit(true);
    tfPrixNetTTC.setName("tfPrixNetTTC");
    add(tfPrixNetTTC, new GridBagConstraints(1, 30, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbTauxRemiseUnitaire ----
    lbTauxRemiseUnitaire.setText("Taux remise unitaire (%)");
    lbTauxRemiseUnitaire.setModeReduit(true);
    lbTauxRemiseUnitaire.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbTauxRemiseUnitaire.setName("lbTauxRemiseUnitaire");
    add(lbTauxRemiseUnitaire,
        new GridBagConstraints(0, 31, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfTauxRemiseUnitaire ----
    tfTauxRemiseUnitaire.setEnabled(false);
    tfTauxRemiseUnitaire.setHorizontalAlignment(SwingConstants.TRAILING);
    tfTauxRemiseUnitaire.setModeReduit(true);
    tfTauxRemiseUnitaire.setName("tfTauxRemiseUnitaire");
    add(tfTauxRemiseUnitaire, new GridBagConstraints(1, 31, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbQuantiteUV ----
    lbQuantiteUV.setText("Quantit\u00e9 en UV");
    lbQuantiteUV.setModeReduit(true);
    lbQuantiteUV.setName("lbQuantiteUV");
    add(lbQuantiteUV,
        new GridBagConstraints(0, 32, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfQuantiteUV ----
    tfQuantiteUV.setEnabled(false);
    tfQuantiteUV.setModeReduit(true);
    tfQuantiteUV.setName("tfQuantiteUV");
    add(tfQuantiteUV, new GridBagConstraints(1, 32, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbValeurRemiseMontantHT ----
    lbValeurRemiseMontantHT.setText("Valeur remise montant HT");
    lbValeurRemiseMontantHT.setModeReduit(true);
    lbValeurRemiseMontantHT.setName("lbValeurRemiseMontantHT");
    add(lbValeurRemiseMontantHT,
        new GridBagConstraints(0, 33, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfValeurRemiseMontantHT ----
    tfValeurRemiseMontantHT.setEnabled(false);
    tfValeurRemiseMontantHT.setModeReduit(true);
    tfValeurRemiseMontantHT.setName("tfValeurRemiseMontantHT");
    add(tfValeurRemiseMontantHT, new GridBagConstraints(1, 33, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbValeurRemiseMontantTTC ----
    lbValeurRemiseMontantTTC.setText("Valeur remise montant TTC");
    lbValeurRemiseMontantTTC.setModeReduit(true);
    lbValeurRemiseMontantTTC.setName("lbValeurRemiseMontantTTC");
    add(lbValeurRemiseMontantTTC,
        new GridBagConstraints(0, 34, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfValeurRemiseMontantTTC ----
    tfValeurRemiseMontantTTC.setEnabled(false);
    tfValeurRemiseMontantTTC.setModeReduit(true);
    tfValeurRemiseMontantTTC.setName("tfValeurRemiseMontantTTC");
    add(tfValeurRemiseMontantTTC, new GridBagConstraints(1, 34, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbMontantHT ----
    lbMontantHT.setText("Montant HT");
    lbMontantHT.setModeReduit(true);
    lbMontantHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbMontantHT.setName("lbMontantHT");
    add(lbMontantHT,
        new GridBagConstraints(0, 35, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfMontantHT ----
    tfMontantHT.setEnabled(false);
    tfMontantHT.setModeReduit(true);
    tfMontantHT.setName("tfMontantHT");
    add(tfMontantHT, new GridBagConstraints(1, 35, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbMontantTVA ----
    lbMontantTVA.setText("Montant TVA");
    lbMontantTVA.setModeReduit(true);
    lbMontantTVA.setName("lbMontantTVA");
    add(lbMontantTVA,
        new GridBagConstraints(0, 36, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfMontantTVA ----
    tfMontantTVA.setEnabled(false);
    tfMontantTVA.setModeReduit(true);
    tfMontantTVA.setName("tfMontantTVA");
    add(tfMontantTVA, new GridBagConstraints(1, 36, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbMontantTTC ----
    lbMontantTTC.setText("Montant TTC");
    lbMontantTTC.setFont(new Font("sansserif", Font.PLAIN, 11));
    lbMontantTTC.setModeReduit(true);
    lbMontantTTC.setVerticalAlignment(SwingConstants.CENTER);
    lbMontantTTC.setImportanceMessage(EnumImportanceMessage.MOYEN);
    lbMontantTTC.setName("lbMontantTTC");
    add(lbMontantTTC,
        new GridBagConstraints(0, 37, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfMontantTTC ----
    tfMontantTTC.setEnabled(false);
    tfMontantTTC.setModeReduit(true);
    tfMontantTTC.setName("tfMontantTTC");
    add(tfMontantTTC, new GridBagConstraints(1, 37, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbModeTTC;
  private SNCheckBoxReduit cbModeTTC;
  private SNLabelChamp lbOrigineTauxTVA;
  private SNLabelUnite tfOrigineTauxTVA;
  private SNLabelChamp lbTauxTVA;
  private SNMontant tfTauxTVA;
  private SNLabelChamp lbOrigineUniteVente;
  private JLabel tfOrigineUniteVente;
  private SNLabelChamp lbUniteVente;
  private SNTexte tfUniteVente;
  private SNLabelChamp lbArrondiPrix;
  private SNTexte tfArrondiPrix;
  private SNLabelChamp lbOrigineColonneTarif;
  private SNLabelUnite tfOrigineColonneTarif;
  private SNLabelChamp lbColonneTarif;
  private SNComboBoxReduit cbColonneTarif;
  private SNLabelChamp lbPrixBaseColonneTarifHT;
  private SNMontant tfPrixBaseColonneTarifHT;
  private SNLabelChamp lbPrixBaseColonneTarifTTC;
  private SNMontant tfPrixBaseColonneTarifTTC;
  private SNLabelChamp lbPrixBaseHeriteHT;
  private SNMontant tfPrixBaseHeriteHT;
  private SNLabelChamp lbPrixBaseHeriteTTC;
  private SNMontant tfPrixBaseHeriteTTC;
  private SNLabelChamp lbOriginePrixBaseCalcul;
  private SNLabelUnite tfOriginePrixBaseCalcul;
  private SNLabelChamp lbPrixBaseCalculHT;
  private SNMontant tfPrixBaseCalculHT;
  private SNLabelChamp lbPrixBaseCalculTTC;
  private SNMontant tfPrixBaseCalculTTC;
  private SNLabelChamp lbRemisable;
  private SNCheckBoxReduit cbRemisable;
  private SNLabelChamp lbRemiseEnValeur;
  private SNMontant tfRemiseEnValeur;
  private SNLabelChamp lbOrigineTauxRemise;
  private SNLabelUnite tfOrigineTauxRemise;
  private SNLabelChamp lbTauxRemise;
  private SNPanel pnlTauxRemise;
  private SNTexte tfTauxRemise1;
  private SNTexte tfTauxRemise2;
  private SNTexte tfTauxRemise3;
  private SNTexte tfTauxRemise4;
  private SNTexte tfTauxRemise5;
  private SNTexte tfTauxRemise6;
  private SNLabelChamp lbOrigineModeTauxRemise;
  private SNLabelUnite tfOrigineModeTauxRemise;
  private SNLabelChamp lbModeTauxRemise;
  private SNComboBoxReduit cbModeTauxRemise;
  private SNLabelChamp lbOrigineAssietteTauxRemise;
  private SNLabelUnite tfOrigineAssietteTauxRemise;
  private SNLabelChamp lbAssietteTauxRemise;
  private SNComboBoxReduit cbAssietteTauxRemise;
  private SNLabelChamp lbOrigineCoefficient;
  private SNLabelUnite tfOrigineCoefficient;
  private SNLabelChamp lbCoefficient;
  private SNMontant tfCoefficient;
  private SNLabelChamp lbPrixBaseRemiseHT;
  private SNMontant tfPrixBaseRemiseHT;
  private SNLabelChamp lbPrixBaseRemiseTTC;
  private SNMontant tfPrixBaseRemiseTTC;
  private SNLabelChamp lbGratuit;
  private SNCheckBoxReduit cbGratuit;
  private SNLabelChamp lbOriginePrixNet;
  private SNLabelUnite tfOriginePrixNet;
  private SNLabelChamp lbPrixNetHT;
  private SNMontant tfPrixNetHT;
  private SNLabelChamp lbPrixNetTTC;
  private SNMontant tfPrixNetTTC;
  private SNLabelChamp lbTauxRemiseUnitaire;
  private SNTexte tfTauxRemiseUnitaire;
  private SNLabelChamp lbQuantiteUV;
  private SNMontant tfQuantiteUV;
  private SNLabelChamp lbValeurRemiseMontantHT;
  private SNMontant tfValeurRemiseMontantHT;
  private SNLabelChamp lbValeurRemiseMontantTTC;
  private SNMontant tfValeurRemiseMontantTTC;
  private SNLabelChamp lbMontantHT;
  private SNMontant tfMontantHT;
  private SNLabelChamp lbMontantTVA;
  private SNMontant tfMontantTVA;
  private SNLabelChamp lbMontantTTC;
  private SNMontant tfMontantTTC;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
