/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.contact.sncontact;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;

public class JTableCellRendererContact extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    try {
      // Taille et justification des colonnes
      redimensionnerColonnes(table);
      
      // colonne contact principal
      if (column == 9) {
        // Pour les cellules de type checkbox
        if (value != null && value.equals("P")) {
          JCheckBox c = new JCheckBox();
          c.setHorizontalAlignment(JCheckBox.CENTER);
          c.setOpaque(false);
          c.setSelected(true);
          return c;
        }
        else {
          JCheckBox c = new JCheckBox();
          c.setHorizontalAlignment(JCheckBox.CENTER);
          c.setOpaque(false);
          c.setSelected(false);
          return c;
        }
      }
    }
    catch (Exception e) {
      DialogueErreur.afficher(e);
    }
    return component;
  }
  
  /**
   * Redimensionne et justifie les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(80);
    cm.getColumn(0).setMaxWidth(80);
    cm.getColumn(1).setMinWidth(50);
    cm.getColumn(1).setMaxWidth(50);
    cm.getColumn(2).setMaxWidth(200);
    cm.getColumn(2).setMinWidth(300);
    cm.getColumn(3).setMinWidth(200);
    cm.getColumn(3).setMaxWidth(300);
    cm.getColumn(4).setMinWidth(100);
    cm.getColumn(4).setMaxWidth(100);
    cm.getColumn(5).setMinWidth(200);
    cm.getColumn(5).setMaxWidth(200);
    cm.getColumn(6).setMinWidth(70);
    cm.getColumn(6).setMinWidth(70);
    cm.getColumn(7).setMinWidth(150);
    cm.getColumn(7).setMaxWidth(150);
    cm.getColumn(8).setMinWidth(200);
    cm.getColumn(8).setMaxWidth(300);
    cm.getColumn(9).setMinWidth(50);
    cm.getColumn(9).setMaxWidth(50);
  }
}
