/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.sndocumentvente;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;

import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.plage.SNComposantPlage;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant de sélection d'un document de vente.<br><br>
 * Ce composant doit être utilisé pour tout choix de document de vente dans le logiciel.<br>
 * Il comprend un champ de saisie et un bouton loupe permettant de sélectionner un document de vente dans une boîte de dialogue.<br>
 * Les informations nécessaires sont la session et l'établissement.<br><br>
 * Utilisation :<br>
 * - Ajouter un composant SNDocumentVente à un écran.<br>
 * - Renseigner le champ de recherche pour faire une recherche à partir des références commandes.<br>
 * - Utiliser la loupe pour faire une recherche avancée.
 */
public class SNDocumentVente extends SNComposantPlage {
  private SNTexte tfTexteRecherche;
  private SNBoutonRecherche btnDialogueRechercheDocumentVente;
  private DocumentVenteBase documentVenteSelectionne = null;
  private boolean isFenetreDocumentVenteAffichee = false;
  private boolean rafraichir = false;
  private String texteRecherchePrecedente = null;
  private CritereDocumentVente critereDocumentVente = new CritereDocumentVente();

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Constructeur.
   */
  public SNDocumentVente() {
    super();
    setName("SNDocumentVente");

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);

    setLayout(new GridBagLayout());

    // Texte de recherche
    tfTexteRecherche = new SNTexte();
    tfTexteRecherche.setName("tfDocumentVente");
    tfTexteRecherche.setEditable(true);
    tfTexteRecherche.setToolTipText(VueRechercheDocumentVente.RECHERCHE_GENERIQUE_TOOLTIP);
    add(tfTexteRecherche,
        new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

    // Bouton loupe
    btnDialogueRechercheDocumentVente = new SNBoutonRecherche();
    btnDialogueRechercheDocumentVente.setToolTipText("Recherche de document de vente");
    btnDialogueRechercheDocumentVente.setName("btnRechercheDocumentVente");
    add(btnDialogueRechercheDocumentVente, new GridBagConstraints());

    // Focus listener pour lancer la recherche quand on quitte la zone de saisie
    tfTexteRecherche.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        tfTexteRechercheFocusLost(e);
      }

      @Override
      public void focusGained(FocusEvent e) {
        tfTexteRechercheFocusGained(e);
      }
    });

    // ActionListener pour capturer l'utilisation de la touche entrée
    tfTexteRecherche.addActionListener(new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tfTexteRechercheActionPerformed(e);
      }
    });

    // Être notifié lorsque le bouton est cliqué
    btnDialogueRechercheDocumentVente.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnDialogueRechercheDocumentVenteActionPerformed(e);
      }
    });
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Activer ou désactiver le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    tfTexteRecherche.setEnabled(pEnabled);
    btnDialogueRechercheDocumentVente.setEnabled(pEnabled);
  }

  /**
   * Positionner le focus sur le composant de saisie.
   */
  @Override
  public boolean requestFocusInWindow() {
    return tfTexteRecherche.requestFocusInWindow();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Sélectionner un document de vente à partir d'un champ RPG comportant son numéro.
   * @param pLexical Lexique des champs RPG.
   * @param pChampIndicatifDocumentVente Nom du champ RPG contenant l'indicatif du document de ventes.
   * @return
   *         true si la sélection a changé.<br>
   *         false si rien n'a changé.
   */
  public boolean setSelectionParChampRPG(Lexical pLexical, String pChampIndicatifDocumentVente) {
    if (pChampIndicatifDocumentVente == null || pLexical.HostFieldGetData(pChampIndicatifDocumentVente) == null) {
      throw new MessageErreurException("Impossible de sélectionner le document de vente car son numéro est invalide.");
    }
    IdDocumentVente idDocumentVente = null;

    // Construire l'id document de vente
    if (!pLexical.HostFieldGetData(Constantes.normerTexte(pChampIndicatifDocumentVente)).isEmpty()) {
      idDocumentVente = IdDocumentVente.getInstanceParIndicatif(pLexical.HostFieldGetData(pChampIndicatifDocumentVente));
    }

    // Sortir si rien n'a changé
    if (Constantes.equals(idDocumentVente, getIdSelection())) {
      return false;
    }

    setSelectionParId(idDocumentVente);
    return true;
  }

  /**
   * Sélectionner le document de vente correspondant à l'identifiant en paramètre.
   * @param pIdDocumentVente Identifiant du docuement de ventes.
   */
  public void setSelectionParId(IdDocumentVente pIdDocumentVente) {
    if (pIdDocumentVente == null) {
      setSelection(null);
      return;
    }
    ListeDocumentVenteBase listeDocumentVenteBase = new ListeDocumentVenteBase();
    DocumentVenteBase documentVenteBase = listeDocumentVenteBase.charger(getIdSession(), pIdDocumentVente);
    setSelection(documentVenteBase);
  }

  /**
   * Renseigner le champ RPG correspondant au document de vente sélectionné via sont indicatif.
   * @param pLexical Lexique des champs RPG.
   * @param pChampIndicatifDocumentVente Nom du champ RPG contenant l'indicatif du document de vente.
   */
  public void renseignerChampRPG(Lexical pLexical, String pChampIndicatifDocumentVente) {
    DocumentVenteBase documentVente = getSelection();
    if (documentVente != null) {
      // Le type indicatif utilisé ici est INDICATIF_NUM
      pLexical.HostFieldPutData(pChampIndicatifDocumentVente, 0, documentVente.getId().getIndicatif(0));
    }
    else {
      pLexical.HostFieldPutData(pChampIndicatifDocumentVente, 0, "");
    }
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Lancer une recherche de document de ventes.
   * @param forcerAffichageDialogue Indicatif d'affichage de la boîte de dialogue.
   */
  private void lancerRecherche(boolean forcerAffichageDialogue) {
    // Vérifier la session
    if (getSession() == null) {
      throw new MessageErreurException("La session du composant de sélection de document de vente est invalide.");
    }

    critereDocumentVente.setIdEtablissement(getIdEtablissement());
    // Indiquer dans le critère de recherche que c'est un composant de plage.
    if (isComposantDeDebut() || isComposantDeFin()) {
      critereDocumentVente.setComposantDePlage(true);
    }

    // Définition des critères pour les composants de plage :
    // Si le composant est un composant de début, n'afficher que les documents de ventes inférieurs au document de ventes de fin.
    if (isComposantDeDebut()) {
      SNDocumentVente composantFin = (SNDocumentVente) snComposantFin;
      if (composantFin.getIdSelection() != null) {
        if (composantFin.getIdSelection().getNumero() != null) {
          critereDocumentVente.setNumeroDocumentFin(composantFin.getIdSelection().getNumero());
        }
        if (composantFin.getIdSelection().getNumeroFacture() != null) {
          critereDocumentVente.setNumeroFactureFin(composantFin.getIdSelection().getNumeroFacture());
        }
      }
    }
    // Si le composant est un composant de fin, n'afficher que les documents de ventes supérieurs au document de ventes de début.
    else if (isComposantDeFin()) {
      SNDocumentVente composantDebut = (SNDocumentVente) snComposantDebut;
      if (composantDebut.getIdSelection() != null) {
        if (composantDebut.getIdSelection().getNumero() != null) {
          critereDocumentVente.setNumeroDocumentDebut(composantDebut.getIdSelection().getNumero());
        }
        if (composantDebut.getIdSelection().getNumeroFacture() != null) {
          critereDocumentVente.setNumeroFactureDebut(composantDebut.getIdSelection().getNumeroFacture());
        }
      }
    }

    // Ne rien faire si le texte à rechercher est vide
    if (Constantes.normerTexte(tfTexteRecherche.getText()).isEmpty()) {
      setSelection(null);
      if (forcerAffichageDialogue) {
        afficherDialogueRechercheDocumentVente(null);
      }
      return;
    }

    // Ne rien faire si le texte à rechercher n'a pas changé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && Constantes.equals(tfTexteRecherche.getText(), texteRecherchePrecedente)) {
      return;
    }
    
    // Rechercher les identifiants des documents de ventes correspondants aux critères de recherche
    List<IdDocumentVente> listeIdDocumentVente =
        ManagerServiceDocumentVente.chargerListeIdDocumentVente(getIdSession(), critereDocumentVente);

    if (!forcerAffichageDialogue && listeIdDocumentVente != null && listeIdDocumentVente.size() == 1) {
      ListeDocumentVenteBase listeDocumentVenteBase = new ListeDocumentVenteBase();
      listeDocumentVenteBase = listeDocumentVenteBase.charger(getIdSession(), listeIdDocumentVente);
      setSelection(listeDocumentVenteBase.get(0));
    }
    else {
      afficherDialogueRechercheDocumentVente(listeIdDocumentVente);
    }

    // Sélectionner automatiquement l'ensemble du champ afin que la prochaine saisie vienne en remplacement
    tfTexteRecherche.selectAll();
  }

  /**
   * Afficher la fenêtre de recherche de documents de ventes.
   * @param pListeIdDocumentVente Liste d'identifiants de documents de ventes.
   */
  private void afficherDialogueRechercheDocumentVente(List<IdDocumentVente> pListeIdDocumentVente) {
    // Sécurité pour éviter que la boîte de dialogue ne s'affiche deux fois
    if (isFenetreDocumentVenteAffichee) {
      return;
    }
    isFenetreDocumentVenteAffichee = true;

    // Afficher la boîte de dialogue de recherche de document de vente
    critereDocumentVente.setIdEtablissement(getIdEtablissement());
    critereDocumentVente.setTexteRechercheGenerique(tfTexteRecherche.getText());
    // Permettre de proposer des résultats sans la contraînte du libellé du document de vente
    if (documentVenteSelectionne != null && Constantes.equals(tfTexteRecherche.getText(), texteRecherchePrecedente)) {
      critereDocumentVente.setTexteRechercheGenerique("");
    }
    // Définition des critères pour les composants de plage :
    // Si le composant est un composant de début, n'afficher que les documents de ventes inférieurs au document de ventes de fin.
    if (isComposantDeDebut()) {
      SNDocumentVente composantFin = (SNDocumentVente) snComposantFin;
      if (composantFin.getIdSelection() != null) {
        if (composantFin.getIdSelection().getNumero() != null) {
          critereDocumentVente.setNumeroDocumentFin(composantFin.getIdSelection().getNumero());
        }
        if (composantFin.getIdSelection().getNumeroFacture() != null) {
          critereDocumentVente.setNumeroFactureFin(composantFin.getIdSelection().getNumeroFacture());
        }
      }
    }
    // Si le composant est un composant de fin, n'afficher que les documents de ventes supérieurs au document de ventes de début.
    else if (isComposantDeFin()) {
      SNDocumentVente composantDebut = (SNDocumentVente) snComposantDebut;
      if (composantDebut.getIdSelection() != null) {
        if (composantDebut.getIdSelection().getNumero() != null) {
          critereDocumentVente.setNumeroDocumentDebut(composantDebut.getIdSelection().getNumero());
        }
        if (composantDebut.getIdSelection().getNumeroFacture() != null) {
          critereDocumentVente.setNumeroFactureDebut(composantDebut.getIdSelection().getNumeroFacture());
        }
      }
    }

    ModeleDocumentVente modeleDocumentVente = new ModeleDocumentVente(getSession(), getIdEtablissement(), critereDocumentVente);
    modeleDocumentVente.setListeIdDocumentVente(pListeIdDocumentVente);
    
    // La recherche est lancée uniquement si le texte de la recherche générique n'est pas vide dans le cas contraire ce sera à
    // l'utilisateur de lancer la recherche manuellement car le temps de chargement peut être long dans ce cas de figure
    if (!Constantes.normerTexte(tfTexteRecherche.getText()).isEmpty()) {
      modeleDocumentVente.lancerRecherche();
    }

    VueRechercheDocumentVente vueRechercheDocumentVente = new VueRechercheDocumentVente(modeleDocumentVente);
    vueRechercheDocumentVente.afficher();

    // Sélectionner le document de vente retourné par la boîte de dialogue (ou aucun si null)
    if (modeleDocumentVente.isSortieAvecValidation()) {
      setSelection(modeleDocumentVente.getDocumentVenteSelectionne());
    }
    // La boîte de dialogue est annulé, on remet le document de ventes sélectionné précédement
    else {
      rafraichirTexteRecherche();
    }
    isFenetreDocumentVenteAffichee = false;
  }

  /**
   * Rafraîchir le texte de la recherche avec le document de vente sélectionné.
   */
  private void rafraichirTexteRecherche() {
    // Mettre à jour le composant
    if (documentVenteSelectionne != null) {
      tfTexteRecherche.setText(documentVenteSelectionne.getLibelle());
    }
    else {
      tfTexteRecherche.setText("");
    }

    tfTexteRecherche.setCaretPosition(0);
    // Mémoriser le dernier texte pour ne pas relancer une recherche inutile
    texteRecherchePrecedente = tfTexteRecherche.getText();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner l'identifiant du document de ventes sélectionné.
   * @return L'identifiant du document de ventes.
   */
  public IdDocumentVente getIdSelection() {
    if (documentVenteSelectionne == null) {
      return null;
    }
    return documentVenteSelectionne.getId();
  }

  /**
   * Retourner le document de vente sélectionné.
   * @return Le document de ventes de base.
   */
  public DocumentVenteBase getSelection() {
    return documentVenteSelectionne;
  }

  /**
   * Modifier le document de vente à sélectionner.
   * @param pDocumentVenteSelectionne Document de ventes sélectionné.
   */
  public void setSelection(DocumentVenteBase pDocumentVenteSelectionne) {
    // Tester si la sélection a changé
    boolean bSelectionModifiee = !Constantes.equals(documentVenteSelectionne, pDocumentVenteSelectionne);

    // Modifier le document de vente sélectionné
    documentVenteSelectionne = pDocumentVenteSelectionne;

    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();

    // Notifier uniquement si la sélection a changé
    if (bSelectionModifiee) {
      fireValueChanged();
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Sélectionner le contenu de la zone de texte de recherche lorsqu'on est focalisé sur le composant.
   */
  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      if (getSelection() == null) {
        return;
      }
      // On sélectionne l'ensemble du libellé affiché
      tfTexteRecherche.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Lancer la recherche lorsqu'on perd le focus sur le composant.
   */
  private void tfTexteRechercheFocusLost(FocusEvent e) {
    try {
      lancerRecherche(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Lancer la recherche lorsqu'on a validé sur le composant.
   */
  private void tfTexteRechercheActionPerformed(ActionEvent e) {
    try {
      lancerRecherche(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Lancer la recherche lors du click sur le bouton de recherche.
   */
  private void btnDialogueRechercheDocumentVenteActionPerformed(ActionEvent e) {
    try {
      if (documentVenteSelectionne == null) {
        lancerRecherche(true);
      }
      else {
        ArrayList<IdDocumentVente> listeIdDocumentVente = new ArrayList<IdDocumentVente>();
        afficherDialogueRechercheDocumentVente(listeIdDocumentVente);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
}
