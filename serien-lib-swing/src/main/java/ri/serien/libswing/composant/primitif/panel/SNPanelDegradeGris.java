/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.panel;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Panneau présentant un fond avec un dégradé de couleurs gris.
 * Ce panneau est utilisé comme trame de fond pour tous les écrans RPG v2 (c'est le gris qui entoure la partie jaune de l'écran).
 * Ce coutour gris autour des écrans n'est plus souhaité et nous l'enlevons progressivement.
 */
public class SNPanelDegradeGris extends SNPanelDegrade {
  /**
   * Constructeur.
   */
  public SNPanelDegradeGris() {
    super(DIRECTION_VERTICAL, SNCharteGraphique.COULEUR_FOND_DEGRADE_GRIS_FONCE);
  }
}
