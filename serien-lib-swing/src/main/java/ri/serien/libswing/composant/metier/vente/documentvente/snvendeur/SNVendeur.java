/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.documentvente.snvendeur;

import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un vendeur.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un vendeur dans le logiciel. Il gère l'affichage des vendeurs existants
 * pour un vendeur donné et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNvendeur à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le RGVX08FM_A4.
 */
public class SNVendeur extends SNComboBoxObjetMetier<IdVendeur, Vendeur, ListeVendeur> {
  
  /**
   * Constructeur par défaut.
   */
  public SNVendeur() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeVendeur());
  }
  
  /**
   * Sélectionner un vendeur de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampVendeur) {
    if (pChampVendeur == null || pLexical.HostFieldGetData(pChampVendeur) == null) {
      throw new MessageErreurException("Impossible de sélectionner le vendeur car son code est invalide.");
    }
    
    IdVendeur idVendeur = null;
    String valeurVendeur = pLexical.HostFieldGetData(pChampVendeur);
    if (valeurVendeur != null && !valeurVendeur.trim().isEmpty()) {
      idVendeur = IdVendeur.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampVendeur));
    }
    
    setIdSelection(idVendeur);
  }
  
  /**
   * Renseigner le champs RPG correspondant au vendeur sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampVendeur) {
    Vendeur Vendeur = getSelection();
    if (Vendeur != null) {
      pLexical.HostFieldPutData(pChampVendeur, 0, Vendeur.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampVendeur, 0, "");
    }
  }
  
}
