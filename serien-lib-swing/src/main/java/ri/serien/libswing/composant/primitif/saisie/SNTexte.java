/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.saisie;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Highlighter;

import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.outil.clipboard.Clipboard;
import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Saisie ou consultation d'un texte alphanumérique (caractères autorisées : aplhapnumériques).
 *
 * Ce composant remplace JTextField. Il est à utiliser pour tous les saisies de textes aplhanumériques. Il est possible de contrainte le
 * nombre de caractères saisissables via la méthode setLongueur().
 *
 * Ce composant est équipé d'un menu contextuel qui apparait avec un clic droit de la souris et permet de copier le texte qu'il contient
 * dans le presse papier.
 *
 * Il existe aussi :
 * - SNNombreEntier pour les nombres entiers.
 * - SNNombreDecimal pour les nombres décimaux.
 * - SNIdentifiant pour les identifiants.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 * - Alignement à gauche.
 */
public class SNTexte extends JTextField {
  private final JPopupMenu menuContextuel = new JPopupMenu("Copie");
  private JMenuItem copie = new JMenuItem("Copier");
  private int longueur = 0;
  private boolean modeReduit = false;
  private boolean changementMode = false;
  private Font policeStandard = SNCharteGraphique.POLICE_STANDARD;

  /**
   * Constructeur par défaut.
   */
  public SNTexte() {
    super();

    // Exceptionnement pour l'initialisation le changement d'aspect est forcé
    changementMode = true;
    rafraichirAspect();
    
    // Rendre le composant transparent
    setOpaque(false);

    // Menu contextuel de copie
    copie.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        // Si une partie du texte du composant est surlignée (sélectionnée) c'est elle que l'on copie
        Highlighter surlignement = getHighlighter();
        if (surlignement.getHighlights() != null && surlignement.getHighlights().length > 0) {
          String texteSelectionne =
              getText().substring(surlignement.getHighlights()[0].getStartOffset(), surlignement.getHighlights()[0].getEndOffset());
          Clipboard.envoyerTexte(texteSelectionne);
        }
        // Sinon on copie tout le texte
        else {
          Clipboard.envoyerTexte(getText());
        }
      }
    });
    menuContextuel.add(copie);
    setComponentPopupMenu(menuContextuel);
  }

  // -- Méthodes privées
  
  /**
   * Rafraichit l'aspect du composant.
   */
  private void rafraichirAspect() {
    // Sélection de la police en fonction du mode uniquement si le mode a été modifié
    if (changementMode) {
      if (modeReduit) {
        setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE_REDUIT);
        setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE_REDUIT);
        setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE_REDUIT);
      }
      else {
        setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
        setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
        setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SAISIE);
      }
      changementMode = false;
    }

    // Sélection des tailles en fonction du mode
    if (modeReduit) {
      policeStandard = SNCharteGraphique.POLICE_STANDARD_REDUIT;
    }
    else {
      policeStandard = SNCharteGraphique.POLICE_STANDARD;
    }
    setFont(policeStandard);
  }
  
  // -- Accesseurs
  
  /**
   * Active ou non le mode réduit du composant.
   * @param pActiverModeReduit
   */
  public void setModeReduit(boolean pActiverModeReduit) {
    // Contrôle s'il y a un chagement de mode (réduit ou non)
    if (modeReduit != pActiverModeReduit) {
      changementMode = true;
    }
    else {
      changementMode = false;
    }
    modeReduit = pActiverModeReduit;
    rafraichirAspect();
  }
  
  /**
   * Retourne si le mode réduit est actif.
   * @return
   */
  public boolean isModeReduit() {
    return modeReduit;
  }

  /**
   * Grise ou non le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    setEditable(pEnabled);
    setFocusable(pEnabled);
    if (!pEnabled) {
      setBackground(SNCharteGraphique.COULEUR_FOND_CHAMP_DESACTIVE);
    }
    else {
      setBackground(Color.WHITE);
    }
  }
  
  /**
   * Nombre de caractères autorisés pour la saisie.
   * @return Nombre de caractères.
   */
  public int getLongueur() {
    return longueur;
  }

  /**
   * Modifier le nombre de caractéres autorisés pour la saisie.
   * @param pLongueur Nombre de caractères (ne peut être inférieur à 1).
   */
  public void setLongueur(int pLongueur) {
    setLongueur(pLongueur, true);
  }
  
  /**
   * Modifier le nombre de caractéres autorisés pour la saisie.
   * @param pLongueur Nombre de caractères (ne peut être inférieur à 1).
   * @param pAlphanumerique true si alphanumérique, false si numérique uniquement.
   */
  public void setLongueur(int pLongueur, boolean pAlphanumerique) {
    // Contrôler le paramètre
    if (pLongueur < 1) {
      pLongueur = 1;
    }

    // Formater la saisie
    SaisieDefinition saisieDefinition = new SaisieDefinition(pLongueur, false, pAlphanumerique, false);
    ((AbstractDocument) this.getDocument()).setDocumentFilter(saisieDefinition);
  }

}
