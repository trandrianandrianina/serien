/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.chantier.snchantier;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.chantier.EnumStatutChantier;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue de sélection d'un chantier.
 */
public class VueChantier extends AbstractVueDialogue<ModeleChantier> {
  // Constantes
  public static final String RECHERCHE_GENERIQUE_TOOLTIP = "<html>Recherche sur :<br>\r\n" + "- Libell\u00e9 chantier (milieu)<br>\r\n"
      + "- R\u00e9f\u00e9rence courte (milieu)<br>\r\n" + "</html>";
  
  private static final String[] TITRE_LISTE =
      new String[] { "Num\u00e9ro", "Libell\u00e9", "R\u00e9f\u00e9rence courte", "Date de d\u00e9but", "Date de fin", "Statut" };
  
  // Variables
  private int derniereLigneSelectionnee = 0;
  
  /**
   * Constructeur.
   */
  public VueChantier(ModeleChantier pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setResizable(false);
    
    // Infobulle pour description des champs utilisés pour la recherche
    tfRechercheGenerique.setToolTipText(RECHERCHE_GENERIQUE_TOOLTIP);
    
    scpListeChantier.getViewport().setBackground(Color.WHITE);
    int[] justification =
        new int[] { NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.GAUCHE };
    int[] dimension = new int[] { 70, 400, 150, 90, 90, 100 };
    tblListeChantier.personnaliserAspect(TITRE_LISTE, dimension, justification, 13);
    tblListeChantier.personnaliserAspectCellules(new JTableChantierRenderer(dimension, justification));
    
    // Permet de redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        tblListeChantierEnterKey();
      }
    };
    tblListeChantier.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeChantier.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeChantierSelectionChanged(e);
      }
    });
    
    // Ajouter un listener si la zone d'affichage du tableau est modifiée
    JViewport viewport = scpListeChantier.getViewport();
    viewport.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        if (isEvenementsActifs()) {
          scpListeChantierStateChanged(e);
        }
      }
    });
    scpListeChantier.setViewport(viewport);
    scpListeChantier.getViewport().setBackground(Color.WHITE);
    
    // Configurer la barre de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Rafraîchir l'écran.
   */
  @Override
  public void rafraichir() {
    
    rafraichirRechercheChantier();
    rafraichirStatut();
    rafraichirTitreListe();
    rafraichirListe();
    rafraichirBoutonValider();
    
    if (getModele().getChantierSelectionne() == null) {
      tfRechercheGenerique.requestFocusInWindow();
    }
  }
  
  // -- Méthodes privées
  
  private void rafraichirRechercheChantier() {
    if (getModele().getCriteresChantier() != null && getModele().getCriteresChantier().getTexteRechercheGenerique() != null) {
      tfRechercheGenerique.setText(getModele().getCriteresChantier().getTexteRechercheGenerique());
    }
    else {
      tfRechercheGenerique.setText("");
    }
  }
  
  private void rafraichirStatut() {
    if (cbStatutChantier.getItemCount() == 0) {
      cbStatutChantier.addItem("Tous");
      for (EnumStatutChantier statutChantier : EnumStatutChantier.values()) {
        cbStatutChantier.addItem(statutChantier);
      }
    }
    
    if (getModele().getCriteresChantier() != null && getModele().getCriteresChantier().getStatut() != null) {
      cbStatutChantier.setSelectedItem(getModele().getCriteresChantier().getStatut());
    }
    else {
      cbStatutChantier.setSelectedIndex(0);
    }
  }
  
  private void rafraichirTitreListe() {
    lbTitreListe.setMessage(getModele().getTitreListe());
  }
  
  private void rafraichirListe() {
    String[][] donnees = null;
    
    List<Chantier> listeChantier = getModele().getListeChantier();
    if (listeChantier != null && !listeChantier.isEmpty()) {
      // Préparer les données pour le tableau
      donnees = new String[listeChantier.size()][VueChantier.TITRE_LISTE.length];
      for (int ligne = 0; ligne < listeChantier.size(); ligne++) {
        Chantier chantier = listeChantier.get(ligne);
        if (chantier != null && chantier.isCharge()) {
          donnees[ligne][0] = Constantes.convertirIntegerEnTexte(chantier.getId().getNumero(), 6);
          
          // Afficher le libellé
          if (chantier.getLibelle() != null) {
            donnees[ligne][1] = chantier.getLibelle();
          }
          else {
            donnees[ligne][1] = "";
          }
          
          // Afficher la référence courte
          if (chantier.getReferenceCourte() != null) {
            donnees[ligne][2] = chantier.getReferenceCourte();
          }
          else {
            donnees[ligne][2] = "";
          }
          
          // Afficher la date de début de validité
          if (chantier.getDateDebutValidite() != null) {
            donnees[ligne][3] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, chantier.getDateDebutValidite());
            
          }
          else {
            donnees[ligne][3] = "Non précisé";
          }
          
          // Afficher la date de fin de validité
          if (chantier.getDateFinValidite() != null) {
            donnees[ligne][4] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, chantier.getDateFinValidite());
            
          }
          else {
            donnees[ligne][4] = "Non précisé";
          }

          // Afficher le statut
          if (chantier.isVerrouille()) {
            donnees[ligne][5] = "Verrouillé";
          }
          else if (chantier.getStatutChantier() != null) {
            donnees[ligne][5] = chantier.getStatutChantier().getLibelle();
          }
          else {
            donnees[ligne][5] = "";
          }
        }
      }
    }
    else {
      scpListeChantier.getVerticalScrollBar().setValue(0);
    }
    tblListeChantier.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours
    int indice = getModele().getLigneSelectionnee();
    if (indice > -1 && indice < tblListeChantier.getRowCount()) {
      tblListeChantier.setRowSelectionInterval(indice, indice);
    }
    else {
      tblListeChantier.clearSelection();
    }
  }
  
  private void rafraichirBoutonValider() {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, getModele().isChantierSelectionnable());
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().lancerRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRechercheGeneriqueFocusLost(FocusEvent e) {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      getModele().modifierRechercheGenerique(tfRechercheGenerique.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRechercheGeneriqueActionPerformed(ActionEvent e) {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      getModele().modifierRechercheGenerique(tfRechercheGenerique.getText());
      getModele().lancerRecherche();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRechercheGeneriqueKeyReleased(KeyEvent e) {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      if (e.getKeyCode() == SNCharteGraphique.TOUCHE_BAS.getKeyCode() || e.getKeyCode() == SNCharteGraphique.TOUCHE_DROITE.getKeyCode()) {
        tblListeChantier.requestFocusInWindow();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbStatutChantierItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbStatutChantier.getSelectedItem() instanceof EnumStatutChantier) {
        getModele().modifierStatut((EnumStatutChantier) cbStatutChantier.getSelectedItem());
      }
      else {
        getModele().modifierStatut(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpListeChantierStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeChantier == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeChantier.getViewport().getViewRect();
      int premiereLigne = tblListeChantier.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeChantier.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeChantier.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageChantierAffiches(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * La ligne sélectionnée a changée dans le tableau résultat.
   */
  private void tblListeChantierSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      // Informer le modèle
      getModele().modifierLigneSelectionnee(tblListeChantier.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeChantierMouseClicked(MouseEvent e) {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      getModele().modifierLigneSelectionnee(tblListeChantier.getIndiceSelection());
      if (e.getClickCount() == 2) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Valide la sélection lorsque l'on appuie sur Entrée dans le tableau.
   */
  private void tblListeChantierEnterKey() {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      getModele().quitterAvecValidation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeChantierKeyReleased(KeyEvent e) {
    if (!isEvenementsActifs()) {
      return;
    }
    try {
      if (derniereLigneSelectionnee == 0 && (e.getKeyCode() == SNCharteGraphique.TOUCHE_HAUT.getKeyCode()
          || e.getKeyCode() == SNCharteGraphique.TOUCHE_GAUCHE.getKeyCode())) {
        tfRechercheGenerique.requestFocusInWindow();
      }
      derniereLigneSelectionnee = tblListeChantier.getSelectedRow();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new SNPanelContenu();
    pnlRecherche = new JPanel();
    pnlRechercheGauche = new JPanel();
    lbRechercheGenerique = new SNLabelChamp();
    tfRechercheGenerique = new SNTexte();
    lbTypeCompteClient = new SNLabelChamp();
    cbStatutChantier = new SNComboBox();
    pnlRechercheDroite = new JPanel();
    snBarreRecherche = new SNBarreRecherche();
    lbTitreListe = new SNLabelTitre();
    scpListeChantier = new JScrollPane();
    tblListeChantier = new NRiTable();
    snBarreBouton = new SNBarreBouton();

    // ======== this ========
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("S\u00e9lection d'un chantier");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setMinimumSize(new Dimension(1070, 494));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };

        // ======== pnlRecherche ========
        {
          pnlRecherche.setOpaque(false);
          pnlRecherche.setBorder(new TitledBorder(""));
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridLayout(1, 2, 5, 5));

          // ======== pnlRechercheGauche ========
          {
            pnlRechercheGauche.setOpaque(false);
            pnlRechercheGauche.setBorder(null);
            pnlRechercheGauche.setName("pnlRechercheGauche");
            pnlRechercheGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWidths = new int[] { 165, 300, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };

            // ---- lbRechercheGenerique ----
            lbRechercheGenerique.setText("Recherche g\u00e9n\u00e9rique");
            lbRechercheGenerique.setHorizontalAlignment(SwingConstants.RIGHT);
            lbRechercheGenerique.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbRechercheGenerique.setPreferredSize(new Dimension(100, 30));
            lbRechercheGenerique.setName("lbRechercheGenerique");
            pnlRechercheGauche.add(lbRechercheGenerique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- tfRechercheGenerique ----
            tfRechercheGenerique.setName("tfRechercheGenerique");
            tfRechercheGenerique.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                tfRechercheGeneriqueActionPerformed(e);
              }
            });
            tfRechercheGenerique.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                tfRechercheGeneriqueKeyReleased(e);
              }
            });
            tfRechercheGenerique.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRechercheGeneriqueFocusLost(e);
              }
            });
            pnlRechercheGauche.add(tfRechercheGenerique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            // ---- lbTypeCompteClient ----
            lbTypeCompteClient.setText("Statut");
            lbTypeCompteClient.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTypeCompteClient.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTypeCompteClient.setPreferredSize(new Dimension(100, 30));
            lbTypeCompteClient.setName("lbTypeCompteClient");
            pnlRechercheGauche.add(lbTypeCompteClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- cbStatutChantier ----
            cbStatutChantier.setMinimumSize(new Dimension(200, 30));
            cbStatutChantier.setPreferredSize(new Dimension(200, 30));
            cbStatutChantier.setName("cbStatutChantier");
            cbStatutChantier.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbStatutChantierItemStateChanged(e);
              }
            });
            pnlRechercheGauche.add(cbStatutChantier, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheGauche);

          // ======== pnlRechercheDroite ========
          {
            pnlRechercheDroite.setOpaque(false);
            pnlRechercheDroite.setName("pnlRechercheDroite");
            pnlRechercheDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };

            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlRechercheDroite.add(snBarreRecherche, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheDroite);
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ---- lbTitreListe ----
        lbTitreListe.setText("Chantiers correspondant \u00e0 votre recherche");
        lbTitreListe.setName("lbTitreListe");
        pnlContenu.add(lbTitreListe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ======== scpListeChantier ========
        {
          scpListeChantier.setPreferredSize(new Dimension(1050, 424));
          scpListeChantier.setName("scpListeChantier");

          // ---- tblListeChantier ----
          tblListeChantier.setShowVerticalLines(true);
          tblListeChantier.setShowHorizontalLines(true);
          tblListeChantier.setBackground(Color.white);
          tblListeChantier.setRowHeight(20);
          tblListeChantier.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeChantier.setSelectionBackground(new Color(57, 105, 138));
          tblListeChantier.setGridColor(new Color(204, 204, 204));
          tblListeChantier.setPreferredScrollableViewportSize(new Dimension(450, 380));
          tblListeChantier.setName("tblListeChantier");
          tblListeChantier.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeChantierMouseClicked(e);
            }
          });
          tblListeChantier.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tblListeChantierKeyReleased(e);
            }
          });
          scpListeChantier.setViewportView(tblListeChantier);
        }
        pnlContenu.add(scpListeChantier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);

      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private JPanel pnlRecherche;
  private JPanel pnlRechercheGauche;
  private SNLabelChamp lbRechercheGenerique;
  private SNTexte tfRechercheGenerique;
  private SNLabelChamp lbTypeCompteClient;
  private SNComboBox cbStatutChantier;
  private JPanel pnlRechercheDroite;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lbTitreListe;
  private JScrollPane scpListeChantier;
  private NRiTable tblListeChantier;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
