/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.panel;

import javax.swing.JPanel;

/**
 * Panneau avec la présentation standard (sans cadre et sans titre).
 *
 * Le panneau standard permet de regrouper les composants. Il doit être utilisé à la place de JPanel lorsqu'il faut regrouper des
 * composants sans artefacts graphiques. Le fond est transparent pour laisse apparaître la couleur de fond de la fenêtre.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Pas de cadre et pas de titre.
 */
public class SNPanel extends JPanel {
  /**
   * Constructeur standard.
   */
  public SNPanel() {
    super();
    setOpaque(false);
  }
}
