/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.saisie.sntelephone;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.composant.SNComposant;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant de saisie d'un numéro de téléphone.
 *
 * Ce composant doit être utilisé pour tout affichage et saisie d'un numéro de téléphone dans le logiciel.
 *
 * Utilisation :
 * - Ajouter un composant SNTelephone à un écran.
 * - Utiliser l'accesseur setSelectionParChampRPG() pour renseigner le numéro de téléphone du composant graphique à partir de champs RPG.
 * - Utiliser l'accesseur renseignerChampRPG() pour renseigner les champs RPG à partir du composant graphique.
 * - Utiliser l'accesseur setSelection() pour renseigner le numéro de téléphone manuellement à partir d'un numéro brut ou d'un numero brut
 * et d'un objet EnumTypeNumeroTelephoneInternational.
 * - Utiliser l'accesseur getSelection() permet de récupérer le numéro de téléphone complet, code pays + numéro de téléphone.
 *
 * L'utilisateur peut sélectionner "Libre" dans la combo box. Dans ce cas, le numéro est enregistré tel que l'utilisateur l'a saisi.
 */
public class SNTelephone extends SNComposant {
  // Constantes
  private static final int LONGUEUR_MAX_NUMERO_TELEPHONE = 20;
  
  // Variables
  private boolean isEvenementActif = true;
  
  private EnumTypeNumeroTelephoneInternational enumPrefixeInternational = null;
  
  private Integer longueurNumeroTelephone = null;
  
  private String numeroTelephone = null;
  
  /**
   * Constructeur.
   */
  public SNTelephone() {
    super();
    initComponents();
    
    // Charge la ComboBox avec les éléments de EnumTelephone
    cbPrefixeInternational.removeAllItems();
    for (EnumTypeNumeroTelephoneInternational prefixeInternational : EnumTypeNumeroTelephoneInternational.values()) {
      cbPrefixeInternational.addItem(prefixeInternational);
    }
  }
  
  /**
   * Gère l'activation des champs du composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    cbPrefixeInternational.setEnabled(pEnabled);
    tfNumeroTelephone.setEnabled(pEnabled);
  }
  
  /**
   * Rafraichit l'affichage des composants.
   */
  private void rafraichir() {
    isEvenementActif = false;
    rafraichirPrefixe();
    rafraichirNumero();
    isEvenementActif = true;
  }
  
  /**
   * Le contenu de la combobox des préfixes internationaux est définit par le préfixe international sélectionné par l'utilisateur ou
   * définit par identification dans le numéro brut récupéré du RPG.
   *
   * Par défaut il est initialisé avec le préfixe international français.
   */
  private void rafraichirPrefixe() {
    if (enumPrefixeInternational != null) {
      cbPrefixeInternational.setSelectedItem(enumPrefixeInternational);
    }
    else {
      cbPrefixeInternational.setSelectedItem(EnumTypeNumeroTelephoneInternational.FRANCE);
    }
  }
  
  /**
   * Le contenu du champ de saisie de numéro de téléphone est mis en forme en fonction du préfixe international sélectionné.
   */
  private void rafraichirNumero() {
    if (numeroTelephone != null & enumPrefixeInternational != null) {
      if (numeroTelephone.trim().isEmpty()) {
        tfNumeroTelephone.setText("");
        return;
      }
      if (enumPrefixeInternational.equals(EnumTypeNumeroTelephoneInternational.LIBRE)) {
        tfNumeroTelephone.setText(numeroTelephone);
        return;
      }
      tfNumeroTelephone.setText(formaterNumeroPourAffichage(numeroTelephone, enumPrefixeInternational, numeroTelephone.length()));
    }
  }
  
  /**
   * Afficher un numéro de téléphone à partir d'un champ RPG.
   *
   * @param pLexique : lexique
   * @param pChampMontant : Champ RPG associé au montant.
   */
  public void setNumeroParChampRPG(Lexical pLexique, String pChampNumeroTelephone) {
    setNumeroTelephone(pLexique.HostFieldGetData(pChampNumeroTelephone));
  }
  
  /**
   * Renseigner le champ RPG correspondant au numéro de téléphone.
   *
   * @param pLexique : lexique.
   * @param pChampMontant : Champ RPG associé au montant.
   */
  public void renseignerChampRPG(Lexical pLexique, String pChampNumeroTelephone) {
    if (getNumeroTelephoneComplet().equals(enumPrefixeInternational.getPrefixeInternational())) {
      pLexique.HostFieldPutData(pChampNumeroTelephone, 0, " ");
    }
    else {
      pLexique.HostFieldPutData(pChampNumeroTelephone, 0, getNumeroTelephoneComplet());
    }
  }
  
  /**
   * Afficher le numéro de téléphone et le préfixe international.
   *
   * Si le numéro passé en paramètre est "", le composant affiche par défaut le préfixe international français dans la combobox.
   *
   * @param pNumeroComplet : numéro de téléphone brut.
   */
  public void setNumeroTelephone(String pNumeroComplet) {
    if (pNumeroComplet.trim().isEmpty()) {
      enumPrefixeInternational = EnumTypeNumeroTelephoneInternational.FRANCE;
      numeroTelephone = "";
      rafraichir();
      fireValueChanged();
      return;
    }
    
    enumPrefixeInternational = EnumTypeNumeroTelephoneInternational.valueOfByNumeroComplet(formaterNumeroBrut(pNumeroComplet));
    if (!enumPrefixeInternational.equals(EnumTypeNumeroTelephoneInternational.LIBRE)) {
      numeroTelephone = formaterNumeroBrut(pNumeroComplet).substring((enumPrefixeInternational.getPrefixeInternational()).length());
    }
    else {
      numeroTelephone = pNumeroComplet;
    }
    longueurNumeroTelephone = numeroTelephone.length();
    
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Afficher le numéro de téléphone et le préfixe international
   *
   * @param pEnumPrefixeInternational : un objet de type EnumTypeNumeroTelephoneInternational
   * @param pNumeroTelephone : un numero de téléphone au format national, sans préfixe.
   *
   * @see EnumTypeNumeroTelephoneInternational
   */
  public void setNumeroTelephone(EnumTypeNumeroTelephoneInternational pEnumPrefixeInternational, String pNumeroTelephone) {
    testerValiditerNumero(formaterChiffresUniquement(pNumeroTelephone));
    enumPrefixeInternational = pEnumPrefixeInternational;
    numeroTelephone = formaterChiffresUniquement(pNumeroTelephone);
    longueurNumeroTelephone = numeroTelephone.length();
    
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Renvoyer le numéro de téléphone sous la forme d'une chaine de caractères.
   *
   * @return Numéro de téléphone.
   */
  public String getNumeroTelephone() {
    if (numeroTelephone != null) {
      return numeroTelephone;
    }
    return "";
  }
  
  /**
   * Renvoyer le préfixe international sélectionné.
   *
   * @return Préfixe international.
   */
  public EnumTypeNumeroTelephoneInternational getPrefixeInternational() {
    if (enumPrefixeInternational != null) {
      return enumPrefixeInternational;
    }
    return EnumTypeNumeroTelephoneInternational.FRANCE;
  }
  
  /**
   * Renvoyer le numéro de téléphone complet : Préfixe international + numéro local
   * Si le numéro complet, formaté pour l'affichage, est composé de 20 caractères ou moins, le formatage est conservé.
   * Sinon, le numéro est formaté avec un espace seul séparant le préfixe international du numéro local.
   *
   * @return Numéro de téléphone composé du préfixe international et du numéro local
   */
  public String getNumeroTelephoneComplet() {
    String numeroCompletFormate = enumPrefixeInternational.getPrefixeInternational() + " "
        + formaterNumeroPourAffichage(numeroTelephone, enumPrefixeInternational, numeroTelephone.length());
    if (numeroCompletFormate.length() <= LONGUEUR_MAX_NUMERO_TELEPHONE) {
      return numeroCompletFormate;
    }
    else {
      return enumPrefixeInternational.getPrefixeInternational() + " " + numeroTelephone;
    }
  }
  
  /**
   * Formate les numeros bruts passés en paramètre en numéro complets lisibles par le composant.
   *
   * @param pNumeroBrut : Numéro de téléphone brut.
   * @return Numéro de téléphone complet interprétable par le composant.
   */
  
  private String formaterNumeroBrut(String pNumeroBrut) {
    pNumeroBrut = formaterChiffresUniquement(pNumeroBrut);
    if (pNumeroBrut.startsWith("00")) {
      pNumeroBrut = pNumeroBrut.substring(2);
    }
    if (isNumeroFrancais(pNumeroBrut)) {
      pNumeroBrut = "33" + pNumeroBrut.substring(1);
    }
    pNumeroBrut = "+" + pNumeroBrut;
    return pNumeroBrut;
  }
  
  /**
   * Supprime tous les caractères non numériques de la chaine de caractères passée en paramètre.
   *
   * @param pNumeroTelephone : Numéro de téléphone.
   * @return Numéro de téléphone sans caractères spéciaux ni espaces.
   */
  private String formaterChiffresUniquement(String pNumeroTelephone) {
    return pNumeroTelephone.replaceAll("[^0-9]", "");
  }
  
  /**
   * Verifie si le numero passé en paramètre est un numero français.
   *
   * @param pNumeroBrut : Numéro de téléphone brut, sans préfixe international
   * @return <b>true</b> si le numéro de téléphone correspond à un numéro français, <b>false</b> sinon.
   *
   *
   */
  private boolean isNumeroFrancais(String pNumeroBrut) {
    if (pNumeroBrut.length() == 10) {
      pNumeroBrut = pNumeroBrut.substring(1);
    }
    
    Pattern p = Pattern.compile(EnumTypeNumeroTelephoneInternational.FRANCE.getRegexTest());
    Matcher m = p.matcher(pNumeroBrut);
    return m.matches();
  }
  
  /**
   * Vérifie si le numéro passé en paramètre correspond au format attendu associé au préfixe international passé en paramètre.
   *
   * @param pNumeroTelephone : Numéro de téléphone sans préfixe international.
   * @return <b>true</b> si le numéro correspond, <b>false</b> sinon.
   */
  private boolean isNumeroCorrect(String pNumeroTelephone, EnumTypeNumeroTelephoneInternational pEnumPrefixeInternational) {
    Pattern p = Pattern.compile(pEnumPrefixeInternational.getRegexTest());
    Matcher m = p.matcher(pNumeroTelephone);
    return m.matches();
  }
  
  /**
   * Formate le numéro de téléphone passé en paramètre selon le format associé au préfixe international passé en paramètre.
   *
   * @param pNumeroTelephone : Numéro de téléphone sans préfixe international.
   * @return Numéro de téléphone formaté pour l'affichage selon le préfixe international.
   */
  private String formaterNumeroPourAffichage(String pNumeroTelephone, EnumTypeNumeroTelephoneInternational pEnumPrefixeInternational,
      int pLongueurNumero) {
    pNumeroTelephone = supprimerPrefixeNational(pNumeroTelephone, pEnumPrefixeInternational);
    pLongueurNumero = pNumeroTelephone.length();
    pNumeroTelephone = pNumeroTelephone.replaceAll(pEnumPrefixeInternational.getRegexFormat(pLongueurNumero),
        pEnumPrefixeInternational.getPattern(pLongueurNumero));
    return pNumeroTelephone;
    
  }
  
  /**
   * Supprime le préfixe national du numéro de téléphone.<p>
   * Le préfixe national correspond au pays dont le préfixe international est passé en paramètre.
   *
   * @param pNumeroTelephone : Numéro de téléphone brut sans préfixe international.
   * @param pEnumPrefixeInternational : Préfixe international.
   */
  private String supprimerPrefixeNational(String pNumeroTelephone, EnumTypeNumeroTelephoneInternational pEnumPrefixeInternational) {
    if (pNumeroTelephone.startsWith(pEnumPrefixeInternational.getPrefixeNational())) {
      pNumeroTelephone = pNumeroTelephone.substring(pEnumPrefixeInternational.getPrefixeNational().length());
    }
    return pNumeroTelephone;
  }
  
  /**
   * Vérifie si le numéro de téléphone passé en paramètre correspond au format attendu pour le préfixe international sélectionné.
   * Supprime le préfixe national avant d'effectueur la vérification.
   *
   * @param pNumeroAValider : Numéro de téléphone.
   */
  private void testerValiditerNumero(String pNumeroAValider) {
    if (isNumeroCorrect(supprimerPrefixeNational(pNumeroAValider, enumPrefixeInternational), enumPrefixeInternational)) {
      numeroTelephone = pNumeroAValider;
    }
    else {
      throw new MessageErreurException(
          "Le numéro saisi ne correspond pas au format défini par le plan de numérotation téléphonique de la région sélectionnée");
    }
  }
  
  /**
   * Modifie les données lorsqu'un code pays est sélectionné dans la combo box.
   */
  private void cbCodePaysItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementActif) {
        return;
      }
      setNumeroTelephone(getPrefixeInternational(), getNumeroTelephone());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Modifie les données lorsque le focus sur le champ de saisie de numéro de téléphone est perdu.
   */
  private void tfNumeroTelephoneFocusLost(FocusEvent e) {
    try {
      if (!isEvenementActif) {
        return;
      }
      setNumeroTelephone((EnumTypeNumeroTelephoneInternational) cbPrefixeInternational.getSelectedItem(), tfNumeroTelephone.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Contrôle la saisie de l'utilisateur.
   * Seuls des caractères numériques peuvent être saisis.
   */
  private void tfNumeroTelephoneKeyTyped(KeyEvent e) {
    try {
      char toucheAppuyee = e.getKeyChar();
      if (!(toucheAppuyee >= '0' & toucheAppuyee <= '9')) {
        e.consume();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    cbPrefixeInternational = new SNComboBox();
    tfNumeroTelephone = new SNTexte();
    
    // ======== this ========
    setMinimumSize(new Dimension(250, 30));
    setPreferredSize(new Dimension(250, 30));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
    
    // ---- cbPrefixeInternational ----
    cbPrefixeInternational.setModel(new DefaultComboBoxModel(new String[] { "(FR) +33" }));
    cbPrefixeInternational.setName("cbPrefixeInternational");
    add(cbPrefixeInternational,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- tfNumeroTelephone ----
    tfNumeroTelephone.setName("tfNumeroTelephone");
    tfNumeroTelephone.addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfNumeroTelephoneFocusLost(e);
      }
    });
    tfNumeroTelephone.addKeyListener(new KeyAdapter() {
      @Override
      public void keyTyped(KeyEvent e) {
        tfNumeroTelephoneKeyTyped(e);
      }
    });
    add(tfNumeroTelephone,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNComboBox cbPrefixeInternational;
  private SNTexte tfNumeroTelephone;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
