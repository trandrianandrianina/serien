/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.fournisseur.sncategoriefournisseur;

import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.CategorieFournisseur;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.IdCategorieFournisseur;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.ListeCategorieFournisseur;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

public class SNCategorieFournisseur
    extends SNComboBoxObjetMetier<IdCategorieFournisseur, CategorieFournisseur, ListeCategorieFournisseur> {
  
  /**
   * Constructeur par défaut.
   */
  public SNCategorieFournisseur() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeCategorieFournisseur());
  }
  
  /**
   * Sélectionner une catégorie fournisseur de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampCategorieFournisseur) {
    if (pChampCategorieFournisseur == null || pLexical.HostFieldGetData(pChampCategorieFournisseur) == null) {
      throw new MessageErreurException("Impossible de sélectionner la catégorie car son code est invalide.");
    }
    
    IdCategorieFournisseur idCategorieFournisseur = null;
    String valeurCategorieFournisseur = pLexical.HostFieldGetData(pChampCategorieFournisseur);
    if (valeurCategorieFournisseur != null && !valeurCategorieFournisseur.trim().isEmpty()) {
      idCategorieFournisseur =
          IdCategorieFournisseur.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampCategorieFournisseur));
    }
    setIdSelection(idCategorieFournisseur);
  }
  
  /**
   * Renseigner les champs RPG correspondant à la catégorie fournisseur sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampCategorieFournisseur) {
    CategorieFournisseur categorie = getSelection();
    if (categorie != null) {
      pLexical.HostFieldPutData(pChampCategorieFournisseur, 0, categorie.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampCategorieFournisseur, 0, "");
    }
  }
}
