/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur;

import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.FournisseurBase;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseurBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleFournisseur extends AbstractModeleDialogue {
  // Variables
  private CritereFournisseur critereFournisseur = null;
  private ListeFournisseur listeFournisseur = null;
  private FournisseurBase fournisseurBaseSelectionne = null;
  private List<IdFournisseur> listeIdFournisseur = null;
  private ListeFournisseurBase listeFournisseurBase = null;
  private int ligneSelectionnee = -1;

  /**
   * Constructeur.
   */
  public ModeleFournisseur(SessionBase pSession, CritereFournisseur pCriteresFournisseur) {
    super(pSession);
    critereFournisseur = pCriteresFournisseur;
    chargerDonnees();
  }

  // -- Méthodes publiques

  @Override
  public void initialiserDonnees() {
  }

  @Override
  public void chargerDonnees() {

  }

  /**
   * Initialiser la recherche.
   */
  public void initialiserRecherche() {
    critereFournisseur.initialiser();
    listeIdFournisseur = null;
    fournisseurBaseSelectionne = null;
    listeFournisseurBase = null;
    rafraichir();
  }

  /**
   * Lancer une recherche sur les fournisseur.
   */
  public void lancerRecherche() {
    lancerRechercheInterne();
    rafraichir();
  }

  /**
   * Modifie la recherche.
   */
  public void modifierRechercheGenerique(String pRechercheGenerique) {
    pRechercheGenerique = Constantes.normerTexte(pRechercheGenerique);
    if (Constantes.equals(pRechercheGenerique, critereFournisseur.getTexteRechercheGenerique())) {
      return;
    }
    critereFournisseur.setTexteRechercheGenerique(pRechercheGenerique);
    lancerRecherche();
  }

  /**
   * Lancer une recherche sur les fournisseurs mais sans déclencher de rafraîchissement.
   */
  private void lancerRechercheInterne() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeFournisseurBase != null) {
      taillePage = listeFournisseurBase.getTaillePage();
    }

    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeIdFournisseur = null;
    listeFournisseurBase = null;
    ligneSelectionnee = -1;
    fournisseurBaseSelectionne = null;
    rafraichir();

    // Analyse erreur sur recherche
    if (critereFournisseur.getRechercheGenerique().isEnErreur()) {
      return;
    }

    // Charger la liste des identifiants
    listeIdFournisseur = ManagerServiceFournisseur.chargerListeIdFournisseur(getIdSession(), critereFournisseur);

    // Initier la liste des fournisseur à partir de la liste d'identifiants
    listeFournisseurBase = ListeFournisseurBase.creerListeNonChargee(listeIdFournisseur);

    listeFournisseurBase.trierParId();

    // Charger la première page
    listeFournisseurBase.chargerPremierePage(getIdSession(), taillePage);
  }

  /**
   * Afficher la plage de fournisseur compris entre deux lignes.
   * Les informations des fournisseur sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageFournisseurBaseAffiches(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeFournisseurBase != null) {
      listeFournisseurBase.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
    }

    // Rafraichir l'écran
    rafraichir();
  }

  /**
   * Modifie l'indice de la ligne séléctionnée dans le tableau.
   */
  public void modifierLigneSelectionnee(int pLigneSelectionnee) {
    if (ligneSelectionnee == pLigneSelectionnee) {
      return;
    }
    ligneSelectionnee = pLigneSelectionnee;

    // Déterminer le fournisseur sélectionné
    if (listeFournisseurBase != null && 0 <= ligneSelectionnee && ligneSelectionnee < listeFournisseurBase.size()) {
      fournisseurBaseSelectionne = listeFournisseurBase.get(ligneSelectionnee);
    }
    else {
      fournisseurBaseSelectionne = null;
    }

    rafraichir();
  }

  /**
   * Modifier le texte de la recherche générique.
   */
  public void modifierTexteRecherche(String pTexteRecherche) {
    if (Constantes.equals(critereFournisseur.getTexteRechercheGenerique(), pTexteRecherche)) {
      return;
    }
    critereFournisseur.setTexteRechercheGenerique(pTexteRecherche);

    lancerRechercheInterne();
    rafraichir();
  }

  // -- Accesseurs

  /**
   * Liste des fournisseur constituant le résultat de la recherche.
   */
  public ListeFournisseurBase getListeFournisseurBase() {
    return listeFournisseurBase;
  }

  /**
   * Renseigner la liste des identifiants fournisseur correspondant au résultat de la recherche.
   */
  public void setListeIdFournisseur(List<IdFournisseur> pListeIdFournisseur) {
    listeIdFournisseur = pListeIdFournisseur;
  }

  /**
   * fournisseur sélectionné.
   */
  public FournisseurBase getFournisseurSelectionne() {
    return fournisseurBaseSelectionne;
  }

  /**
   * Modifier le fournisseur sélectionné.
   */
  public void setFournisseurSelectionne(FournisseurBase pFournisseurBase) {
    fournisseurBaseSelectionne = pFournisseurBase;
  }

  /**
   * Teste de la recherche générique.
   */
  public String getTexteRecherche() {
    return critereFournisseur.getTexteRechercheGenerique();
  }

  /**
   * Critère de recherche des fournisseurs.
   */
  public CritereFournisseur getCritereFournisseur() {
    return critereFournisseur;
  }

  /**
   * Message d'information à afficher en titre de liste dans la boîte de dialogue.
   */
  public Message getMessage() {
    if (listeIdFournisseur == null) {
      return Message.getMessageNormal("Recherche de fournisseur");
    }
    else if (listeIdFournisseur.size() == 0) {
      return Message.getMessageImportant("Aucun fournisseur ne correspond à vos critères");
    }
    else if (listeIdFournisseur.size() == 1) {
      return Message.getMessageNormal("Fournisseur correspondant à votre recherche");
    }
    else if (listeIdFournisseur.size() > 1) {
      return Message.getMessageNormal("Fournisseurs correspondants à votre recherche (" + listeIdFournisseur.size() + ")");
    }
    else {
      return Message.getMessageImportant("Aucun fournisseur ne correspond à vos critères");
    }
  }

  /**
   * Indique si le fournisseur sélectionnée est valide.
   */
  public boolean isSelectionValide() {
    return fournisseurBaseSelectionne != null;
  }

}
