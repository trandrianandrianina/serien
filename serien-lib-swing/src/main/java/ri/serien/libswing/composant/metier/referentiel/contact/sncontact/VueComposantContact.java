/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.contact.sncontact;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.CategorieContact;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue du composant de sélection d'un contact.
 */
public class VueComposantContact extends AbstractVueDialogue<ModeleComposantContact> {
  // Constantes
  public static final String[] TITRE_LISTE = new String[] { "Code", "Nom", "Fonction", "Téléphone", "Princ" };
  public static final String RECHERCHE_GENERIQUE_TOOLTIP = "<html>Recherche sur :<br>\r\n" + "- numéro contact (entier)<br>\r\n"
      + "- nom (milieu)<br>\r\n" + "- prénom (milieu)<br>\r\n" + "- mot de classement (milieu)<br>\r\n" + "</html>";
  private static final String BOUTON_CREER_CONTACT = "Créer";
  private static final String BOUTON_MODIFIER_CONTACT = "Modifier";
  private static final String BOUTON_CREER_LIEN = "Lier contact";
  private static final String BOUTON_SUPPRIMER_LIEN = "Supprimer lien contact";
  private static final String BOUTON_REDIGER_MAIL = "Rédiger E-mail";

  // Variables
  private int derniereLigneSelectionnee = 0;

  /**
   * Constructeur.
   */
  public VueComposantContact(ModeleComposantContact pModele) {
    super(pModele);
  }

  // -- Méthodes publiques

  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setResizable(false);

    // Configurer le composant de saisie de texte
    tfRechercheGenerique.setToolTipText(VueComposantContact.RECHERCHE_GENERIQUE_TOOLTIP);

    // Configurer le tableau
    int[] justification = new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.CENTRE };
    int[] dimension = new int[] { 100, 380, 200, 150, 50 };
    int[] dimensionMax = new int[] { 100, 380, 5000, 150, 50 };
    tblListeContact.personnaliserAspect(TITRE_LISTE, dimension, dimensionMax, justification, 13);

    // Redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        tblListeContactEnterKey();
      }
    };
    tblListeContact.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);

    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeContact.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeContactSelectionChanged(e);
      }
    });

    // Ajout un listener pour détecter les changements d'affichage de la liste
    JViewport viewportDevis = scpListeContact.getViewport();
    viewportDevis.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeContactStateChanged(e);
      }
    });
    scpListeContact.setViewport(viewportDevis);
    scpListeContact.getViewport().setBackground(Color.WHITE);

    // Configurer la barre de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });

    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CREER_CONTACT, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER_CONTACT, 'm', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_LIEN, 'l', false);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER_LIEN, 's', true);
    snBarreBouton.ajouterBouton(BOUTON_REDIGER_MAIL, 'r', true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirMessageInformation();
    rafraichirRechercheGenerique();
    rafraichirCategorieContact();
    rafraichirTiers();
    rafraichirTitreListe();
    rafraichirListe();
    rafraichirBoutonValider();
    rafraichirBoutonModifier();
    rafraichirBoutonLien();

    if (getModele().getContactSelectionnee() == null) {
      tfRechercheGenerique.requestFocusInWindow();
    }
  }

  // -- Méthodes privées

  private void rafraichirMessageInformation() {
    if (getModele().getMessageInformation() != null) {
      lbMessageInformation.setText(getModele().getMessageInformation());
      lbMessageInformation.setVisible(true);
    }
    else {
      lbMessageInformation.setText("");
      lbMessageInformation.setVisible(false);
    }
  }

  private void rafraichirRechercheGenerique() {
    if (getModele().getCritereContact() != null && getModele().getCritereContact().getTexteRechercheGenerique() != null) {
      tfRechercheGenerique.setText(getModele().getCritereContact().getTexteRechercheGenerique());
    }
    else {
      tfRechercheGenerique.setText("");
    }

    tfRechercheGenerique.setEnabled(isDonneesChargees());
  }

  private void rafraichirCategorieContact() {
    // Chargement de la combo avec les catégories contact
    cbFonction.removeAllItems();
    cbFonction.addItem("Toutes les fonctions");
    if (getModele().getListeCategorieContact() != null) {
      for (CategorieContact categorieContact : getModele().getListeCategorieContact()) {
        cbFonction.addItem(categorieContact);
      }
    }

    // Sélection de l'item
    if (getModele().getFiltreCategorieContact() != null) {
      cbFonction.setSelectedItem(getModele().getFiltreCategorieContact());
    }
    else {
      cbFonction.setSelectedIndex(0);
    }
  }

  /**
   * Composant client ou fournisseur selon le contexte
   */
  private void rafraichirTiers() {
    switch (getModele().getTypeContact()) {
      case CLIENT:
      case PROSPECT:
        snClient.setSession(getModele().getSession());
        snClient.setIdEtablissement(getModele().getCritereContact().getIdEtablissement());
        snClient.setRechercheProspectAutorisee(true);
        snClient.charger(false);
        if (getModele().getIdClientSelectionne() != null) {
          snClient.forcerRechercheParID(getModele().getIdClientSelectionne());
          snClient.setSelectionParId(getModele().getIdClientSelectionne());
        }
        else {
          snClient.forcerRechercheParID(getModele().getIdClient());
          snClient.setSelectionParId(getModele().getIdClient());
        }
        snClient.setVisible(true);
        snFournisseur.setVisible(false);
        break;

      case FOURNISSEUR:
        snFournisseur.setSession(getModele().getSession());
        snFournisseur.setIdEtablissement(getModele().getCritereContact().getIdEtablissement());
        snFournisseur.charger(false);
        if (getModele().getIdFournisseurSelectionne() != null) {
          snFournisseur.setSelectionParId(getModele().getIdFournisseurSelectionne());
        }
        else {
          snFournisseur.setSelectionParId(getModele().getIdFournisseur());
        }
        snClient.setVisible(false);
        snFournisseur.setVisible(true);
        break;

      default:
        snClient.setVisible(false);
        snFournisseur.setVisible(false);
        break;
    }
  }

  private void rafraichirTitreListe() {
    lbTitreListe.setMessage(getModele().getTitreListe());
  }

  private void rafraichirListe() {
    String[][] donnees = null;

    // Préparer les données pour le tableau
    List<Contact> listeContact = getModele().getListeContact();
    if (listeContact != null && !listeContact.isEmpty()) {
      donnees = new String[listeContact.size()][VueComposantContact.TITRE_LISTE.length];
      for (int ligne = 0; ligne < listeContact.size(); ligne++) {
        Contact contact = listeContact.get(ligne);
        if (contact != null) {
          donnees[ligne][0] = contact.getId().toString();

          // Rafraichir la civilité, le nom et le prénom (attention il ne s'agit pas ici du champ REPAC qui est ignoré)
          if (contact.getNomComplet() != null && !contact.getNomComplet().isEmpty()) {
            donnees[ligne][1] = contact.getNomComplet();
          }
          else {
            donnees[ligne][1] = "";
          }

          // Rafraichir la fonction
          if (contact.getIdCategorie() != null && getModele().getListeCategorieContact() != null) {
            CategorieContact categorieContact = getModele().getListeCategorieContact().get(contact.getIdCategorie());
            if (categorieContact != null) {
              donnees[ligne][2] = categorieContact.getLibelle();
            }
            else {
              donnees[ligne][2] = "";
            }
          }
          else {
            donnees[ligne][2] = "";
          }

          // Rafraichir le téléphone
          if (contact.getNumeroTelephone1() != null) {
            donnees[ligne][3] = contact.getNumeroTelephone1();
          }
          else if (contact.getNumeroTelephone2() != null) {
            donnees[ligne][3] = contact.getNumeroTelephone2();
          }
          else {
            donnees[ligne][3] = "";
          }

          // Contact principal
          if (getModele().isContactPrincipal(contact)) {
            donnees[ligne][4] = "✓";
          }
          else {
            donnees[ligne][4] = "";
          }

        }
      }
    }
    else {
      scpListeContact.getVerticalScrollBar().setValue(0);
    }

    // Metytre à jour le tableau
    tblListeContact.mettreAJourDonnees(donnees);

    // Sélectionner la ligne en cours
    int indice = getModele().getLigneSelectionnee();
    if (indice > -1 && indice < tblListeContact.getRowCount()) {
      tblListeContact.setRowSelectionInterval(indice, indice);
    }
    else {
      tblListeContact.clearSelection();
    }
  }

  private void rafraichirBoutonValider() {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, getModele().isSelectionValide());
  }

  private void rafraichirBoutonModifier() {
    snBarreBouton.activerBouton(BOUTON_MODIFIER_CONTACT, getModele().isSelectionValide());
  }

  private void rafraichirBoutonLien() {
    snBarreBouton.activerBouton(BOUTON_CREER_CONTACT, !getModele().isLiaisonValide());
    snBarreBouton.activerBouton(BOUTON_CREER_LIEN, getModele().isLiaisonValide());
    snBarreBouton.activerBouton(BOUTON_SUPPRIMER_LIEN, getModele().isSelectionValide());
    snBarreBouton.activerBouton(BOUTON_REDIGER_MAIL, getModele().isSelectionValide());
  }

  // -- Méthodes évènementielles

  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_LIEN)) {
        getModele().creerLien();
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER_LIEN)) {
        getModele().supprimerLien();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_CONTACT)) {
        setVisible(false);
        getModele().creerContact();
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER_CONTACT)) {
        setVisible(false);
        getModele().modifierContact();
      }
      else if (pSNBouton.isBouton(BOUTON_REDIGER_MAIL)) {
        getModele().redigerMail();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().rechercher();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfRechercheGeneriqueFocusGained(FocusEvent e) {
    try {
      tfRechercheGenerique.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfRechercheGeneriqueFocusLost(FocusEvent e) {
    try {
      getModele().modifierRechercheGenerique(tfRechercheGenerique.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfRechercheFournisseurActionPerformed(ActionEvent e) {
    try {
      getModele().modifierRechercheGenerique(tfRechercheGenerique.getText());
      getModele().rechercher();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfRechercheFournisseurKeyReleased(KeyEvent e) {
    try {
      if (e.getKeyCode() == SNCharteGraphique.TOUCHE_BAS.getKeyCode() || e.getKeyCode() == SNCharteGraphique.TOUCHE_DROITE.getKeyCode()) {
        tblListeContact.requestFocusInWindow();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void scpListeContactStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeContact == null) {
        return;
      }

      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeContact.getViewport().getViewRect();
      int premiereLigne = tblListeContact.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeContact.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeContact.getRowCount() - 1;
      }

      // Charges les articles concernés si besoin
      getModele().modifierPlageContactAffiches(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * La ligne sélectionnée a changée dans le tableau résultat.
   */
  private void tblListeContactSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }

      // Informer le modèle
      getModele().modifierLigneSelectionnee(tblListeContact.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tblListeContactMouseClicked(MouseEvent e) {
    try {
      getModele().modifierLigneSelectionnee(tblListeContact.getIndiceSelection());
      if (e.getClickCount() == 2) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tblListeContactKeyReleased(KeyEvent e) {
    try {
      if (derniereLigneSelectionnee == 0 && (e.getKeyCode() == SNCharteGraphique.TOUCHE_HAUT.getKeyCode()
          || e.getKeyCode() == SNCharteGraphique.TOUCHE_GAUCHE.getKeyCode())) {
        tfRechercheGenerique.requestFocusInWindow();
      }
      derniereLigneSelectionnee = tblListeContact.getSelectedRow();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tblListeContactEnterKey() {
    try {
      getModele().quitterAvecValidation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void cbFonctionItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbFonction.getSelectedItem() instanceof CategorieContact) {
        getModele().modifierCategorieContact((CategorieContact) cbFonction.getSelectedItem());
      }
      else {
        getModele().modifierCategorieContact(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void snClientValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierClientSelectionne(snClient.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void snFournisseurValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierFournisseurSelectionne(snFournisseur.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new SNPanelContenu();
    lbMessageInformation = new SNMessage();
    pnlRecherche = new SNPanelTitre();
    pnlRechercheGauche = new JPanel();
    lbTexteRecherche = new SNLabelChamp();
    tfRechercheGenerique = new SNTexte();
    lbFonction = new SNLabelChamp();
    cbFonction = new SNComboBox();
    lbLien = new SNLabelChamp();
    pnlLien = new SNPanel();
    snClient = new SNClient();
    snFournisseur = new SNFournisseur();
    pnlRechercheDroite = new JPanel();
    snBarreRecherche = new SNBarreRecherche();
    lbTitreListe = new SNLabelTitre();
    scpListeContact = new JScrollPane();
    tblListeContact = new NRiTable();
    snBarreBouton = new SNBarreBouton();

    // ======== this ========
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("S\u00e9lection d'un contact");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 10, 10), null));
        pnlContenu.setPreferredSize(new Dimension(975, 500));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };

        // ---- lbMessageInformation ----
        lbMessageInformation.setName("lbMessageInformation");
        pnlContenu.add(lbMessageInformation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

        // ======== pnlRecherche ========
        {
          pnlRecherche.setOpaque(false);
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRecherche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRecherche.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlRecherche.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };

          // ======== pnlRechercheGauche ========
          {
            pnlRechercheGauche.setOpaque(false);
            pnlRechercheGauche.setBorder(null);
            pnlRechercheGauche.setName("pnlRechercheGauche");
            pnlRechercheGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWidths = new int[] { 165, 300, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };

            // ---- lbTexteRecherche ----
            lbTexteRecherche.setText("Recherche g\u00e9n\u00e9rique");
            lbTexteRecherche.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTexteRecherche.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTexteRecherche.setPreferredSize(new Dimension(100, 30));
            lbTexteRecherche.setName("lbTexteRecherche");
            pnlRechercheGauche.add(lbTexteRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- tfRechercheGenerique ----
            tfRechercheGenerique.setMinimumSize(new Dimension(300, 30));
            tfRechercheGenerique.setPreferredSize(new Dimension(300, 30));
            tfRechercheGenerique.setName("tfRechercheGenerique");
            tfRechercheGenerique.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                tfRechercheFournisseurActionPerformed(e);
              }
            });
            tfRechercheGenerique.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                tfRechercheFournisseurKeyReleased(e);
              }
            });
            tfRechercheGenerique.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                tfRechercheGeneriqueFocusGained(e);
              }

              @Override
              public void focusLost(FocusEvent e) {
                tfRechercheGeneriqueFocusLost(e);
              }
            });
            pnlRechercheGauche.add(tfRechercheGenerique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            // ---- lbFonction ----
            lbFonction.setText("Fonction");
            lbFonction.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFonction.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFonction.setPreferredSize(new Dimension(100, 30));
            lbFonction.setName("lbFonction");
            pnlRechercheGauche.add(lbFonction, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));

            // ---- cbFonction ----
            cbFonction.setName("cbFonction");
            cbFonction.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbFonctionItemStateChanged(e);
              }
            });
            pnlRechercheGauche.add(cbFonction, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));

            // ---- lbLien ----
            lbLien.setText("Lien");
            lbLien.setName("lbLien");
            pnlRechercheGauche.add(lbLien, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

            // ======== pnlLien ========
            {
              pnlLien.setName("pnlLien");
              pnlLien.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlLien.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlLien.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlLien.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlLien.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };

              // ---- snClient ----
              snClient.setMaximumSize(new Dimension(300, 30));
              snClient.setMinimumSize(new Dimension(300, 30));
              snClient.setPreferredSize(new Dimension(300, 30));
              snClient.setName("snClient");
              snClient.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snClientValueChanged(e);
                }
              });
              pnlLien.add(snClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));

              // ---- snFournisseur ----
              snFournisseur.setMaximumSize(new Dimension(300, 30));
              snFournisseur.setMinimumSize(new Dimension(300, 30));
              snFournisseur.setPreferredSize(new Dimension(300, 30));
              snFournisseur.setName("snFournisseur");
              snFournisseur.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snFournisseurValueChanged(e);
                }
              });
              pnlLien.add(snFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlRechercheGauche.add(pnlLien, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheGauche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

          // ======== pnlRechercheDroite ========
          {
            pnlRechercheDroite.setOpaque(false);
            pnlRechercheDroite.setName("pnlRechercheDroite");
            pnlRechercheDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };

            // ---- snBarreRecherche ----
            snBarreRecherche.setMaximumSize(new Dimension(150, 65));
            snBarreRecherche.setMinimumSize(new Dimension(150, 65));
            snBarreRecherche.setName("snBarreRecherche");
            pnlRechercheDroite.add(snBarreRecherche, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlRechercheDroite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ---- lbTitreListe ----
        lbTitreListe.setText("Liste des contacts");
        lbTitreListe.setImportanceMessage(EnumImportanceMessage.MOYEN);
        lbTitreListe.setName("lbTitreListe");
        pnlContenu.add(lbTitreListe, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

        // ======== scpListeContact ========
        {
          scpListeContact.setName("scpListeContact");

          // ---- tblListeContact ----
          tblListeContact.setShowVerticalLines(true);
          tblListeContact.setShowHorizontalLines(true);
          tblListeContact.setBackground(Color.white);
          tblListeContact.setRowHeight(20);
          tblListeContact.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeContact.setSelectionBackground(new Color(57, 105, 138));
          tblListeContact.setGridColor(new Color(204, 204, 204));
          tblListeContact.setName("tblListeContact");
          tblListeContact.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeContactMouseClicked(e);
            }
          });
          tblListeContact.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tblListeContactKeyReleased(e);
            }
          });
          scpListeContact.setViewportView(tblListeContact);
        }
        pnlContenu.add(scpListeContact, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);

      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);

    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNMessage lbMessageInformation;
  private SNPanelTitre pnlRecherche;
  private JPanel pnlRechercheGauche;
  private SNLabelChamp lbTexteRecherche;
  private SNTexte tfRechercheGenerique;
  private SNLabelChamp lbFonction;
  private SNComboBox cbFonction;
  private SNLabelChamp lbLien;
  private SNPanel pnlLien;
  private SNClient snClient;
  private SNFournisseur snFournisseur;
  private JPanel pnlRechercheDroite;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lbTitreListe;
  private JScrollPane scpListeContact;
  private NRiTable tblListeContact;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables

}
