/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.lot;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.lot.CritereLot;
import ri.serien.libcommun.gescom.vente.lot.EnumModeSaisieLot;
import ri.serien.libcommun.gescom.vente.lot.IdLot;
import ri.serien.libcommun.gescom.vente.lot.ListeLot;
import ri.serien.libcommun.gescom.vente.lot.Lot;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 *
 * Modèle pour la saisie de quantités sur les lots pour un article
 *
 */
public class ModeleLot extends AbstractModeleDialogue {
  private static final boolean SAISIE_LOT_INCOMPLETE = false;
  private static final boolean SAISIE_LOT_COMPLETE = true;
  private IdEtablissement idEtablissement = null;
  private IdLigneVente idLigneVente = null;
  private Article article = null;
  private BigDecimal quantiteCommande = null;
  private IdMagasin idMagasin = null;
  private ListeLot listeLot = null;
  private String texteRecherche = "";
  private BigDecimal quantiteTotaleSaisie = BigDecimal.ZERO;
  private BigDecimal quantiteTotalePrecedementSaisie = BigDecimal.ZERO;
  private BigDecimal quantiteASaisir = BigDecimal.ZERO;
  private Message messageResteASaisir = null;
  private EnumModeSaisieLot modeSaisie = EnumModeSaisieLot.MODE_SAISIE_LOT;
  private boolean modeSortie = SAISIE_LOT_INCOMPLETE;
  private boolean affichageAvecDisponibleSeulement = true;
  private boolean affichageAvecSaisieSeulement = false;
  private Message titreListe = null;
  private IdDocumentVente idDocumentOrigine = null;

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Constructeur.
   * @param pSession La session
   * @param pIdLigneVente la ligne de vente contenant un article loti
   * @param pArticle l'article lot
   * @param pQuantiteSaisie la quantité d'articles vendue.
   */
  public ModeleLot(SessionBase pSession, IdLigneVente pIdLigneVente, Article pArticle, BigDecimal pQuantiteSaisie) {
    super(pSession);
    idLigneVente = pIdLigneVente;
    article = pArticle;
    quantiteCommande = pQuantiteSaisie;
    idEtablissement = article.getId().getIdEtablissement();
    idMagasin = article.getIdMagasin();
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Initialiser les données du modèle.
   */
  @Override
  public void initialiserDonnees() {
    // On charge le document d'origine pour tester s'il est modifiable
    try {
      IdDocumentVente idDocument = idDocumentOrigine;
      if (idDocument == null || idDocumentOrigine.isIdDevis()) {
        idDocument = idLigneVente.getIdDocumentVente();
      }
      DocumentVente document = ManagerServiceDocumentVente.chargerEnteteDocumentVente(getIdSession(), idDocument);
      // Si la quantité vendue est négative c'est que l'on est en retour d'articles lotis
      if ((quantiteCommande != null && quantiteCommande.compareTo(BigDecimal.ZERO) < 0)) {
        modeSaisie = EnumModeSaisieLot.MODE_RETOUR_LOT;
      }
      // Si on a un document d'origine et des quantités positives c'est que l'on est en extraction
      else if (idDocumentOrigine != null && !idDocumentOrigine.isIdDevis()
          && (quantiteCommande != null && quantiteCommande.compareTo(BigDecimal.ZERO) > 0)) {
        modeSaisie = EnumModeSaisieLot.MODE_EXTRACTION_LOT;
      }
      // Si le document n'est pas modifiable on bloque la saisie des lots
      else if (document != null && !document.isModifiable()) {
        modeSaisie = EnumModeSaisieLot.MODE_LECTURE_SEULE;
      }
    }
    catch (Exception e) {
      modeSaisie = EnumModeSaisieLot.MODE_SAISIE_LOT;
    }
    
    setTitreEcran(modeSaisie.getLibelle());
  }

  /**
   * Charger les données du modèle.
   */
  @Override
  public void chargerDonnees() {
    listeLot = new ListeLot();
    chargerListeLot();
    if (listeLot == null) {
      throw new MessageErreurException("Il n'existe aucun lot pour cet article dans la base de données");
    }
    quantiteASaisir = quantiteCommande;
    for (Lot lot : listeLot) {
      if (!Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_RETOUR_LOT)) {
        quantiteASaisir =
            quantiteASaisir.subtract(lot.getQuantitePrecedentSaisi()).setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);
        quantiteTotaleSaisie =
            quantiteTotaleSaisie.add(lot.getQuantitePrecedentSaisi()).setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);
      }
    }
    if (Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_RETOUR_LOT)) {
      quantiteASaisir = quantiteCommande.abs();
    }
    if (quantiteASaisir.equals(BigDecimal.ZERO.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP))) {
      messageResteASaisir =
          Message.getMessageMoyen("Toutes les quantités de cet article sont affectées à un numéro de lot (Reste à saisir : 0).");
    }
    else if (Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_EXTRACTION_LOT) && quantiteASaisir.compareTo(BigDecimal.ZERO) < 0) {
      messageResteASaisir =
          Message.getMessageImportant("La quantité affectée sur lots dans le document d'origine est supérieure à la quantité extraite : "
              + Constantes.convertirBigDecimalEnTexte(quantiteASaisir) + "\nAjustez la quantité sur lot.");
    }
    else {
      messageResteASaisir = Message
          .getMessageImportant("Quantité restante à affecter sur lots : " + Constantes.convertirBigDecimalEnTexte(quantiteASaisir));
    }
  }
  
  /**
   * Quitter avec validation
   */
  @Override
  public void quitterAvecValidation() {
    // On met à jour les quantités en tenant compte de la saisie précédente (saisie partielle)
    for (Lot lot : listeLot) {
      // Mise à zéro des quantité précédente à null
      if (lot.getQuantitePrecedentSaisi() == null) {
        lot.setQuantitePrecedentSaisi(BigDecimal.ZERO);
      }
      // En retour d'article, on enlève la quantité saisie de la quantité précédemment saisie
      if (Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_RETOUR_LOT) && !article.isArticleSpecial()) {
        if (lot.getQuantiteSaisi() == null) {
          lot.setQuantiteSaisi(lot.getQuantitePrecedentSaisi().setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP));
        }
        else {
          lot.setQuantiteSaisi((lot.getQuantitePrecedentSaisi().subtract(lot.getQuantiteSaisi())).setScale(article.getNombreDecimaleUV(),
              RoundingMode.HALF_UP));
        }
      }
      // En extraction de lot on envoi au service la quantité cumulée précédemment saisi + quantité saisie pour tenir compte des quantités
      // du document d'origine
      if (Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_EXTRACTION_LOT)) {
        if (lot.getQuantiteSaisi() == null) {
          lot.setQuantiteSaisi(lot.getQuantitePrecedentSaisi().setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP));
        }
        else {
          lot.setQuantiteSaisi((lot.getQuantitePrecedentSaisi().add(lot.getQuantiteSaisi())).setScale(article.getNombreDecimaleUV(),
              RoundingMode.HALF_UP));
        }
      }
      // En saisie de lot on envoi au service la quantité cumulée précédemment saisi + quantité saisie
      // Sauf si on veut remettre à zéro (supprimer un lot). Dans ce cas il faut mettre quantité saisie à zéro sans toucher au
      // précédemment saisi (donc on ne cumule rien et on envoi les champs tels quels au service)
      else {
        if (!Constantes.equals(lot.getQuantiteSaisi(), BigDecimal.ZERO)) {
          if (lot.getQuantiteSaisi() == null) {
            lot.setQuantiteSaisi(lot.getQuantitePrecedentSaisi().setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP));
          }
          else {
            lot.setQuantiteSaisi((lot.getQuantitePrecedentSaisi().add(lot.getQuantiteSaisi())).setScale(article.getNombreDecimaleUV(),
                RoundingMode.HALF_UP));
          }
        }
      }
    }
    
    // On met à jour si les quantités de lots saisies correspondent à la quantité commandée (saisie complète)
    if (quantiteASaisir.compareTo(BigDecimal.ZERO) == 0) {
      modeSortie = SAISIE_LOT_COMPLETE;
    }
    else {
      modeSortie = SAISIE_LOT_INCOMPLETE;
    }
    
    // Si on est en retour d'article et que la saisie est incomplète on sort sans enregistrer car il faut une saisie complète
    if (Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_RETOUR_LOT) && !isSaisieLotComplete()) {
      throw new MessageErreurException(
          "Votre saisie est incomplète et ne sera pas enregistrée :\ntous les retours d'articles doivent être affectés à un lot.");
    }
    
    // Service de sauvegarde des affectations de lots
    ManagerServiceArticle.sauverListeLot(getIdSession(), idLigneVente, quantiteCommande, quantiteTotaleSaisie, listeLot);
    // }
    
    super.quitterAvecValidation();
  }

  /**
   * Quitter avec annulation
   * En extraction on enregistre systématiquement les saisie sur lot, même sur le bouton Annuler. Charge à l'utilisateur d'initialiser
   * la saisie s'il ne veut pas des lots saisis en commande
   */
  @Override
  public void quitterAvecAnnulation() {
    // Si on est en extraction on valide systématiquement
    if (Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_EXTRACTION_LOT)) {
      quitterAvecValidation();
    }
    else {
      selectionnerLot(null);
      super.quitterAvecAnnulation();
    }
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Permet de sélectionner des documents d'achats dans la liste de documents d'achats
   * @param pListeIndicesSelectionnes liste des indices sélectionnés dans la liste par l'utilisateur
   */
  public void selectionnerLot(ArrayList<Integer> pListeIndicesSelectionnes) {
    // Si aucun document d'achat généré on sort
    if (listeLot == null || listeLot.size() == 0) {
      return;
    }

    // Si aucun document sélectionné on déssélectionne tous les documents d'achats et on sort
    if (pListeIndicesSelectionnes == null || pListeIndicesSelectionnes.size() == 0) {
      listeLot.deselectionnerTout();
    }
    // On contrôle si le nombre de documents sélectionnés ne dépasse pas le nombre maximal de sessions que l'on peut ouvrir dans le client
    else if (!ManagerSessionClient.getInstance().verifierNombreSessionDisponible(pListeIndicesSelectionnes.size())) {
      listeLot.deselectionnerTout();
      rafraichir();
      throw new MessageErreurException("Vous ne pouvez pas ouvrir plus de " + ManagerSessionClient.NOMBRE_MAX_SESSION
          + " points de menu.\nMerci de sélectionner moins de documents d'achats."
          + "\nVous pouvez retrouver l'ensemble de vos documents non traités par le point de menu des achats.");
    }
    // On rafraichi la liste des éléments sélectionnés dans la liste des documents d'achats
    else {
      listeLot.selectionner(pListeIndicesSelectionnes);
    }
  }

  /**
   * Modifier le texte de la recherche générique.
   * @param pTexteRecherche texte saisi par l'utilisateur
   */
  public void modifierRecherche(String pTexteRecherche) {
    texteRecherche = pTexteRecherche;
    rafraichir();
  }

  /**
   * Modifier la quantité saisie sur un lot.
   * @param pIndexLigneSeletionnee ligne sélectionnée par l'utilisateur
   * @param pValeurSaisie valeur saisie par l'utilisateur
   */
  public void modifierQuantiteLot(int pIndexLigneSeletionnee, String pValeurSaisie) {
    if (pIndexLigneSeletionnee < 0) {
      return;
    }

    if (pValeurSaisie == null || pValeurSaisie.trim().isEmpty()) {
      pValeurSaisie = "0";
    }
    BigDecimal quantite = Constantes.convertirTexteEnBigDecimal(pValeurSaisie);
    quantite = quantite.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);

    // Si la quantité est valide
    if (controlerQuantiteLot(quantite, pIndexLigneSeletionnee)) {
      // On ajoute la quantité sur le lot
      listeLot.get(pIndexLigneSeletionnee).setQuantiteSaisi(quantite);
      // On recalcule la quantité totale saisie
      quantiteTotaleSaisie = quantiteTotaleSaisie.add(quantite).setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);
      // On met à jour la quantité à saisir
      quantiteASaisir = quantiteASaisir.subtract(quantite);
      // On met à jour le message sur la quantité restante à saisir
      if (quantiteASaisir.equals(BigDecimal.ZERO.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP))) {
        messageResteASaisir =
            Message.getMessageMoyen("Toutes les quantités de cet article sont affectées à un numéro de lot (Reste à saisir : 0).");
      }
      else {
        messageResteASaisir = Message
            .getMessageImportant("Quantité restante à affecter sur lots : " + Constantes.convertirBigDecimalEnTexte(quantiteASaisir));
      }
    }

    rafraichir();
  }

  /**
   * Modifier le numéro de lot en vue de sa création.
   * @param pValeurSaisie valeur saisie par l'utilisateur
   */
  public void modifierNumeroLot(String pValeurSaisie) {
    if (pValeurSaisie == null || pValeurSaisie.trim().isEmpty()) {
      return;
    }
    
    // Si aucun magasin (article spécial)
    if (idMagasin == null) {
      UtilisateurGescom utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getSession().getIdSession(),
          ManagerSessionClient.getInstance().getProfil(), ManagerSessionClient.getInstance().getCurlib(), idEtablissement);
      idMagasin = utilisateurGescom.getIdMagasin();
    }

    // On ajoute le numéro de lot
    IdLot idLot = IdLot.getInstance(idEtablissement, article.getId(), idMagasin, pValeurSaisie);
    Lot lot = new Lot(idLot);
    listeLot.add(lot);
    modeSaisie = EnumModeSaisieLot.MODE_RETOUR_LOT;
    rafraichir();
  }

  /**
   * Initialiser la recherche de lots.
   */
  public void initialiserRecherche() {
    texteRecherche = "";
    rechercher();
  }

  /**
   * Lancer la recherche de lots.
   */
  public void rechercher() {
    chargerListeLot();
    rafraichir();
  }

  /**
   * Sélectionner les indices de tous les documents affichés
   */
  public void selectionnerTout() {
    listeLot.selectionnerTout();
    rafraichir();
  }

  /**
   * Modifier s'il faut filtrer la liste sur les lots sur lesquels on a affecté des quantités.
   * @param pQuantiteSaisie choix saisi par l'utilisateur
   */
  public void modifierQuantiteSaisieSeule(boolean pQuantiteSaisie) {
    affichageAvecSaisieSeulement = pQuantiteSaisie;
    rechercher();
  }
  
  /**
   * Supprimer toutes les quantités saisies pour une ligne de vente.
   */
  public void supprimerSaisieLot() {
    chargerDonnees();
    if (listeLot == null || listeLot.size() == 0) {
      return;
    }
    for (Lot lot : listeLot) {
      if (lot.getQuantitePrecedentSaisi().compareTo(BigDecimal.ZERO) > 0) {
        lot.setQuantiteSaisi(BigDecimal.ZERO);
      }
    }
    quantiteASaisir = quantiteCommande;
    if (modeSaisie == EnumModeSaisieLot.MODE_RETOUR_LOT) {
      quantiteASaisir = quantiteCommande.abs();
    }
    quantiteTotaleSaisie = BigDecimal.ZERO;
    // Service de sauvegarde des affectations de lots
    ManagerServiceArticle.sauverListeLot(getIdSession(), idLigneVente, quantiteCommande, BigDecimal.ZERO, listeLot);

  }
  
  /**
   * Remettre à 0 toutes les quantités en cours de saisie.
   */
  public void initialiserSaisie() {
    // On supprime les saisies issues du document d'origine
    supprimerSaisieLot();
    // On recharge tous les lots de l'article
    idDocumentOrigine = null;
    chargerListeLot();
    // on recharge les messages
    if (quantiteASaisir.equals(BigDecimal.ZERO.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP))) {
      messageResteASaisir =
          Message.getMessageMoyen("Toutes les quantités de cet article sont affectées à un numéro de lot (Reste à saisir : 0).");
    }
    else {
      messageResteASaisir = Message
          .getMessageImportant("Quantité restante à affecter sur lots : " + Constantes.convertirBigDecimalEnTexte(quantiteASaisir));
    }
    // On passe en mode saisie
    modeSaisie = EnumModeSaisieLot.MODE_SAISIE_LOT;
    rafraichir();
  }
  
  /**
   * Modifier la quantité vendue à partir de la quantité affectée sur lots.
   */
  public void modifierQuantiteVendue() {
    quantiteCommande = quantiteTotaleSaisie;
    quantiteASaisir = BigDecimal.ZERO;
    // On met à jour le message sur la quantité restante à saisir
    messageResteASaisir =
        Message.getMessageMoyen("Toutes les quantités de cet article sont affectées à un numéro de lot (Reste à saisir : 0).");
    rafraichir();
  }
  
  /**
   * Retourner si la quantité vendue est modifiable.
   * @return boolean
   */
  public boolean isQuantiteVendueModifiable() {
    if (!Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_LECTURE_SEULE) && quantiteTotaleSaisie.compareTo(BigDecimal.ZERO) != 0
        && quantiteCommande.compareTo(quantiteTotaleSaisie) > 0) {
      return true;
    }
    return false;
  }
  
  /**
   * Créer un nouveau lot.
   */
  public void creerLot() {
    if (!isCreationLotPossible()) {
      return;
    }
    modeSaisie = EnumModeSaisieLot.MODE_CREATION_LOT;
    rafraichir();
  }

  /**
   * Indiquer si la création de lot est possible.
   * @return boolean
   */
  public boolean isCreationLotPossible() {
    if (Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_RETOUR_LOT) && article.isArticleSpecial()) {
      return true;
    }
    return false;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * charger la liste des lots
   */
  private void chargerListeLot() {
    if (article == null) {
      return;
    }
    CritereLot critere = new CritereLot();
    critere.setIdEtablissement(idEtablissement);
    critere.setIdArticle(article.getId());
    critere.setIdMagasin(idMagasin);
    critere.setTexteRechercheGenerique(texteRecherche);
    critere.setIdLigneVente(idLigneVente);
    critere.setIdDocumentOrigine(idDocumentOrigine);

    // Si un document d'origine existe et que l'on est en mode retour ou extraction, on charge les lots affectés sur le document d'origine
    if ((Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_RETOUR_LOT)
        || Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_EXTRACTION_LOT)) && idDocumentOrigine != null) {
      listeLot = ManagerServiceArticle.chargerListeLotDocument(getSession().getIdSession(), critere);
    }
    // Sinon on charge tous les lots de l'article
    else {
      listeLot = ManagerServiceArticle.chargerListeLot(getSession().getIdSession(), critere);
    }
    // Tri sur la liste des lots trouvés
    if (listeLot != null) {
      trierListeLot();
    }
    // Adaptation du titre de la liste
    construireTitreListe();
  }

  /**
   * Construire le message titre de la liste selon le contenu de la liste de lots.
   */
  private void construireTitreListe() {
    titreListe = Message.getMessageNormal("Liste des lots");
    if (listeLot.size() == 0) {
      titreListe = Message.getMessageImportant("Aucun lot n'a été trouvé pour cet article");
    }
    else if (listeLot.size() == 1) {
      titreListe = Message.getMessageNormal("Lot trouvé pour cet article");
    }
    else {
      titreListe = Message.getMessageNormal("Liste des lots pour cet article (" + listeLot.size() + ")");
    }
    
  }

  /**
   * Trier la liste des lots suivant la valeur définie par le paramètre système 030.
   */
  private void trierListeLot() {
    // Filtrage des lots avec quantité affectée seulement
    if (affichageAvecSaisieSeulement) {
      listeLot = listeLot.filtrerQuantiteSaisieAZero();
    }
    
    // Filtrage des lots dont le disponible est à 0
    if (affichageAvecDisponibleSeulement) {
      listeLot = listeLot.filtrerDisponibleAZero();
    }

    // Lecture de la valeur du PS030
    Character ps30 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS030);
    // 1 == tri par numéro de lot
    if (ps30.equals('1')) {
      listeLot.trierParNumero();
    }
    // 2 == tri par ordre croissant de quantité disponible
    if (ps30.equals('2')) {
      listeLot.trierParQuantiteDisponible();
    }
    // 3 == Affichage des lots dont la quantité disponible est supérieure ou égale à la quantité commandée
    if (ps30.equals('3')) {
      listeLot = listeLot.filtrerParQuantiteDisponible(quantiteCommande);
    }
    // 4 == tri par date de péremption
    if (ps30.equals('4')) {
      listeLot.trierParDatePeremption();
    }
  }

  /**
   * Contrôler les quantités saisies sur les lots.
   * - La quantité totale saisie doit être inférieur ou égale à la quantité d'articles lotis commandés
   * - La quantité saisie sur un lot doit être inférieure ou égale à la quantité disponible sur le lot
   * @return boolean
   */
  private boolean controlerQuantiteLot(BigDecimal pQuantite, int pIndexLigneSeletionnee) {
    if (pQuantite == BigDecimal.ZERO) {
      return true;
    }
    // On vérifie qu'il reste des quantités sur lot à saisir
    if (quantiteASaisir.compareTo(pQuantite) < 0) {
      DialogueInformation.afficher("Vous avez saisi une quantité sur lots supérieure à la quantité d'articles");
      return false;
    }
    // On vérifie que le disponible sur le lot est suffisant
    if (!Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_RETOUR_LOT)
        && listeLot.get(pIndexLigneSeletionnee).getStockDisponible().compareTo(pQuantite) < 0) {
      DialogueInformation.afficher("La quantité disponible sur ce lot est insuffisante");
      return false;
    }
    return true;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Retourner la liste des lots
   * @return ListeLot
   */
  public ListeLot getListeLot() {
    return listeLot;
  }

  /**
   * Retourner la liste des incides de lots sélectionnés
   * null signifie qu'aucun document n'est sélectionné
   */
  public List<Integer> getListeIndiceLotSelectionne() {
    if (listeLot == null) {
      return null;
    }
    return listeLot.getListeIndexSelection();
  }

  /**
   * Retourner si on peut valider l'affectation sur lots.
   * @return boolean
   */
  public boolean isValidationPossible() {
    // Si on est en lecture seule, pas de validation (on sort en annulant)
    if (Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_LECTURE_SEULE)) {
      return false;
    }
    // S'il n'y a pas de lots pas de validation
    if (listeLot == null) {
      return false;
    }
    // Si on a saisi plus que la quantité commandée on ne peut pas valider
    if (quantiteTotaleSaisie.compareTo(quantiteCommande.abs()) > 0) {
      return false;
    }
    // Si on a saisi un nouveau numéro de lot on peut valider
    if (Constantes.equals(modeSaisie, EnumModeSaisieLot.MODE_CREATION_LOT)) {
      return true;
    }
    // Si on a tout saisi on peut valider
    if (quantiteASaisir.compareTo(BigDecimal.ZERO) == 0) {
      return true;
    }
    // Si on a réalisé une saisie, même partielle, on peut valider
    if (quantiteTotaleSaisie.compareTo(BigDecimal.ZERO) > 0) {
      return true;
    }
    // Dans tous les autres cas on ne peut pas valider
    return false;
  }

  /**
   * Retourner l'article loti.
   * @return Article
   */
  public Article getArticle() {
    return article;
  }

  /**
   * Retourner le message qui indique la quantité restante à saisir.
   * @return Message
   */
  public Message getMessageResteASaisir() {
    return messageResteASaisir;
  }

  /**
   * Retourner le texte recherché.
   * @return String
   */
  public String getTexteRecherche() {
    return texteRecherche;
  }
  
  /**
   * Retourner si on a saisi les lots pour la totalité de la quantité vendue sur la ligne.
   * @return boolean
   */
  public boolean isSaisieLotComplete() {
    return modeSortie;
  }

  /**
   * Retourner si on a saisi des quantités sur lots.
   * @return boolean
   */
  public boolean isQuantiteSaisie() {
    return quantiteTotaleSaisie.compareTo(BigDecimal.ZERO) > 0;
  }
  
  /**
   * Retourner si l'on affiche la liste des lots en excluant ceux dont le disponible est inférieur ou égal à 0.
   * @return boolean
   */
  public boolean isAffichageAvecDisponibleSeulement() {
    return affichageAvecDisponibleSeulement;
  }

  /**
   * Retourner si l'on affiche la liste des lots en excluant ceux pour lesquels aucune quantité n'a été saisie.
   * @return boolean
   */
  public boolean isAffichageAvecSaisieSeulement() {
    return affichageAvecSaisieSeulement;
  }

  /**
   * Retourner le message titre de la liste.
   * @return Message
   */
  public Message getTitreListe() {
    return titreListe;
  }
  
  /**
   * Retourner la quantité articlee.
   * @return BigDecimal
   */
  public BigDecimal getQuantiteCommande() {
    return quantiteCommande;
  }
  
  /**
   * Retourner le mode de saisie des lots (saisie d'articles ou retours d'articles).
   * @return int
   */
  public EnumModeSaisieLot getModeSaisie() {
    return modeSaisie;
  }
  
  /**
   * Modifier l'identifiant du document à l'origine du document en cours (pour les extractions).
   * @param IdDocumentVente
   */
  public void setIdDocumentOrigine(IdDocumentVente pIdDocumentOrigineAvoir) {
    idDocumentOrigine = pIdDocumentOrigineAvoir;
  }
  
}
