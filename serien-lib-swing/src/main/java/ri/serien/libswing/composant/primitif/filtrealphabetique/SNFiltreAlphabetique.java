/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.filtrealphabetique;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JToggleButton;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposant;

/**
 * Composant de filtre alphabétique.
 *
 * Ce composant doit être utilisé pour mettre en place un filtre alphabétique dans le logiciel.
 * Il est composé d'une série de boutons portant chacun une des lettres de l'alphabet et d'un bouton "Tous".
 * L'utilisateur clique sur un bouton pour sélectionner une lettre. Le bouton reste enfoncé pour visualiser la lettre sélectionnée.
 * L'utilisateur ne peut sélectionner qu'une lettre à la fois.
 * Le composant renvoie la valeur sélectionnée : une lettre ou "".
 *
 * Utilisation :
 * - Ajouter un composant SNFiltreAlphabétique à un écran.
 *
 * Voir un exemple d'implémentation dans le VueContactRecherche.
 */
public class SNFiltreAlphabetique extends SNComposant {
  
  // Variable
  private String valeurSelectionnee = null;
  private JToggleButton boutonFiltrePrecedent = null;
  private JToggleButton boutonFiltreActif = null;
  
  /**
   * Constructeur
   */
  public SNFiltreAlphabetique() {
    super();
    initComponents();
  }
  
  /**
   * Rafraichir l'affichage.
   */
  private void rafraichir() {
    rafraichirBoutonsAlphabetique();
  }
  
  /**
   * Rafraichir l'affichage des boutons.
   */
  private void rafraichirBoutonsAlphabetique() {
    valeurSelectionnee = getSelection();
    // Si un bouton a été sélectionné.
    if (boutonFiltreActif != null) {
      // On colore le bouton sélectionné par l'utilisateur
      boutonFiltreActif.setBackground(SNCharteGraphique.COULEUR_BOUTON_ORANGE);
      boutonFiltreActif.setSelected(true);

      // Si un bouton a été sélectionné précédemment.
      if (boutonFiltrePrecedent != null) {
        // Si le bouton sélectionné est différent du précédent, on remet le bouton précedent dans la couleur d'origine
        if (!boutonFiltrePrecedent.getText().equalsIgnoreCase(valeurSelectionnee)) {
          boutonFiltrePrecedent.setBackground(SNCharteGraphique.COULEUR_FOND);
        }
      }
    }
    // On stocke le bouton actif
    boutonFiltrePrecedent = boutonFiltreActif;
  }

  public void initialiserFiltre() {
    if (boutonFiltreActif != null) {
      boutonFiltreActif = btFiltreTous;
      setSelection("Tous");
    }
  }
  
  /**
   * Retourner la valeur du bouton sélectionné.
   */
  public String getSelection() {
    return valeurSelectionnee;
  }
  
  /**
   * Modifier la valeur du filtre alphabétique
   * Pour "Tous" la valeur est "".
   */
  public void setSelection(String pFiltreAlpha) {
    // Mettre à jour la valeur du filtre
    if (valeurSelectionnee != pFiltreAlpha) {
      valeurSelectionnee = pFiltreAlpha;
      if (valeurSelectionnee.equalsIgnoreCase("Tous")) {
        valeurSelectionnee = "";
      }
    }
    
    // Rafraîchir l'affichage
    rafraichir();
    fireValueChanged();
  }
  
  /**
   * Lorsque l'utilisateur clique sur un bouton, il devient le bouton actif.
   * La lettre du bouton actif est sélectionnée.
   */
  private void btFiltreAlphaPerformed(ActionEvent e) {
    try {
      if (e.getSource() instanceof JToggleButton) {
        // On stocke le bouton actif
        boutonFiltreActif = (JToggleButton) e.getSource();
        
        setSelection(boutonFiltreActif.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlFiltreAlpha = new SNPanel();
    btFiltreTous = new JToggleButton();
    btFiltreA = new JToggleButton();
    btFiltreB = new JToggleButton();
    btFiltreC = new JToggleButton();
    btFiltreD = new JToggleButton();
    btFiltreE = new JToggleButton();
    btFiltreF = new JToggleButton();
    btFiltreG = new JToggleButton();
    btFiltreH = new JToggleButton();
    btFiltreI = new JToggleButton();
    btFiltreJ = new JToggleButton();
    btFiltreK = new JToggleButton();
    btFiltreL = new JToggleButton();
    btFiltreM = new JToggleButton();
    btFiltreN = new JToggleButton();
    btFiltreO = new JToggleButton();
    btFiltreP = new JToggleButton();
    btFiltreQ = new JToggleButton();
    btFiltreR = new JToggleButton();
    btFiltreS = new JToggleButton();
    btFiltreT = new JToggleButton();
    btFiltreU = new JToggleButton();
    btFiltreV = new JToggleButton();
    btFiltreW = new JToggleButton();
    btFiltreX = new JToggleButton();
    btFiltreY = new JToggleButton();
    btFiltreZ = new JToggleButton();
    buttonGroup1 = new ButtonGroup();
    
    // ======== this ========
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
    
    // ======== pnlFiltreAlpha ========
    {
      pnlFiltreAlpha.setMinimumSize(new Dimension(1200, 25));
      pnlFiltreAlpha.setMaximumSize(new Dimension(2000, 25));
      pnlFiltreAlpha.setPreferredSize(new Dimension(1200, 25));
      pnlFiltreAlpha.setName("pnlFiltreAlpha");
      pnlFiltreAlpha.setLayout(new GridLayout());
      
      // ---- btFiltreTous ----
      btFiltreTous.setText("Tous");
      btFiltreTous.setBorder(null);
      btFiltreTous.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreTous.setPreferredSize(new Dimension(55, 25));
      btFiltreTous.setMinimumSize(new Dimension(55, 25));
      btFiltreTous.setMaximumSize(new Dimension(400, 30));
      btFiltreTous.setBorderPainted(false);
      btFiltreTous.setBackground(new Color(239, 239, 222));
      btFiltreTous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreTous.setName("btFiltreTous");
      btFiltreTous.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreTous);
      
      // ---- btFiltreA ----
      btFiltreA.setText("A");
      btFiltreA.setBorder(null);
      btFiltreA.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreA.setPreferredSize(new Dimension(40, 25));
      btFiltreA.setMinimumSize(new Dimension(40, 25));
      btFiltreA.setMaximumSize(new Dimension(40, 30));
      btFiltreA.setBorderPainted(false);
      btFiltreA.setBackground(new Color(239, 239, 222));
      btFiltreA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreA.setName("btFiltreA");
      btFiltreA.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreA);
      
      // ---- btFiltreB ----
      btFiltreB.setText("B");
      btFiltreB.setBorder(null);
      btFiltreB.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreB.setPreferredSize(new Dimension(40, 25));
      btFiltreB.setMinimumSize(new Dimension(40, 25));
      btFiltreB.setMaximumSize(new Dimension(40, 30));
      btFiltreB.setBorderPainted(false);
      btFiltreB.setBackground(new Color(239, 239, 222));
      btFiltreB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreB.setName("btFiltreB");
      btFiltreB.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreB);
      
      // ---- btFiltreC ----
      btFiltreC.setText("C");
      btFiltreC.setBorder(null);
      btFiltreC.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreC.setPreferredSize(new Dimension(40, 25));
      btFiltreC.setMinimumSize(new Dimension(40, 25));
      btFiltreC.setMaximumSize(new Dimension(40, 30));
      btFiltreC.setBorderPainted(false);
      btFiltreC.setBackground(new Color(239, 239, 222));
      btFiltreC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreC.setName("btFiltreC");
      btFiltreC.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreC);
      
      // ---- btFiltreD ----
      btFiltreD.setText("D");
      btFiltreD.setBorder(null);
      btFiltreD.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreD.setPreferredSize(new Dimension(40, 25));
      btFiltreD.setMinimumSize(new Dimension(40, 25));
      btFiltreD.setMaximumSize(new Dimension(40, 30));
      btFiltreD.setBorderPainted(false);
      btFiltreD.setBackground(new Color(239, 239, 222));
      btFiltreD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreD.setName("btFiltreD");
      btFiltreD.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreD);
      
      // ---- btFiltreE ----
      btFiltreE.setText("E");
      btFiltreE.setBorder(null);
      btFiltreE.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreE.setPreferredSize(new Dimension(40, 25));
      btFiltreE.setMinimumSize(new Dimension(40, 25));
      btFiltreE.setMaximumSize(new Dimension(40, 30));
      btFiltreE.setBorderPainted(false);
      btFiltreE.setBackground(new Color(239, 239, 222));
      btFiltreE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreE.setName("btFiltreE");
      btFiltreE.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreE);
      
      // ---- btFiltreF ----
      btFiltreF.setText("F");
      btFiltreF.setBorder(null);
      btFiltreF.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreF.setPreferredSize(new Dimension(40, 25));
      btFiltreF.setMinimumSize(new Dimension(40, 25));
      btFiltreF.setMaximumSize(new Dimension(40, 30));
      btFiltreF.setBorderPainted(false);
      btFiltreF.setBackground(new Color(239, 239, 222));
      btFiltreF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreF.setName("btFiltreF");
      btFiltreF.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreF);
      
      // ---- btFiltreG ----
      btFiltreG.setText("G");
      btFiltreG.setBorder(null);
      btFiltreG.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreG.setPreferredSize(new Dimension(40, 25));
      btFiltreG.setMinimumSize(new Dimension(40, 25));
      btFiltreG.setMaximumSize(new Dimension(40, 30));
      btFiltreG.setBorderPainted(false);
      btFiltreG.setBackground(new Color(239, 239, 222));
      btFiltreG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreG.setName("btFiltreG");
      btFiltreG.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreG);
      
      // ---- btFiltreH ----
      btFiltreH.setText("H");
      btFiltreH.setBorder(null);
      btFiltreH.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreH.setPreferredSize(new Dimension(40, 25));
      btFiltreH.setMinimumSize(new Dimension(40, 25));
      btFiltreH.setMaximumSize(new Dimension(40, 30));
      btFiltreH.setBorderPainted(false);
      btFiltreH.setBackground(new Color(239, 239, 222));
      btFiltreH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreH.setName("btFiltreH");
      btFiltreH.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreH);
      
      // ---- btFiltreI ----
      btFiltreI.setText("I");
      btFiltreI.setBorder(null);
      btFiltreI.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreI.setPreferredSize(new Dimension(40, 25));
      btFiltreI.setMinimumSize(new Dimension(40, 25));
      btFiltreI.setMaximumSize(new Dimension(40, 30));
      btFiltreI.setBorderPainted(false);
      btFiltreI.setBackground(new Color(239, 239, 222));
      btFiltreI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreI.setName("btFiltreI");
      btFiltreI.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreI);
      
      // ---- btFiltreJ ----
      btFiltreJ.setText("J");
      btFiltreJ.setBorder(null);
      btFiltreJ.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreJ.setPreferredSize(new Dimension(40, 25));
      btFiltreJ.setMinimumSize(new Dimension(40, 25));
      btFiltreJ.setMaximumSize(new Dimension(40, 30));
      btFiltreJ.setBorderPainted(false);
      btFiltreJ.setBackground(new Color(239, 239, 222));
      btFiltreJ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreJ.setName("btFiltreJ");
      btFiltreJ.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreJ);
      
      // ---- btFiltreK ----
      btFiltreK.setText("K");
      btFiltreK.setBorder(null);
      btFiltreK.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreK.setPreferredSize(new Dimension(40, 25));
      btFiltreK.setMinimumSize(new Dimension(40, 25));
      btFiltreK.setMaximumSize(new Dimension(40, 30));
      btFiltreK.setBorderPainted(false);
      btFiltreK.setBackground(new Color(239, 239, 222));
      btFiltreK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreK.setName("btFiltreK");
      btFiltreK.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreK);
      
      // ---- btFiltreL ----
      btFiltreL.setText("L");
      btFiltreL.setBorder(null);
      btFiltreL.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreL.setPreferredSize(new Dimension(40, 25));
      btFiltreL.setMinimumSize(new Dimension(40, 25));
      btFiltreL.setMaximumSize(new Dimension(40, 30));
      btFiltreL.setBorderPainted(false);
      btFiltreL.setBackground(new Color(239, 239, 222));
      btFiltreL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreL.setName("btFiltreL");
      btFiltreL.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreL);
      
      // ---- btFiltreM ----
      btFiltreM.setText("M");
      btFiltreM.setBorder(null);
      btFiltreM.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreM.setPreferredSize(new Dimension(40, 25));
      btFiltreM.setMinimumSize(new Dimension(40, 25));
      btFiltreM.setMaximumSize(new Dimension(40, 30));
      btFiltreM.setBorderPainted(false);
      btFiltreM.setBackground(new Color(239, 239, 222));
      btFiltreM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreM.setName("btFiltreM");
      btFiltreM.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreM);
      
      // ---- btFiltreN ----
      btFiltreN.setText("N");
      btFiltreN.setBorder(null);
      btFiltreN.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreN.setPreferredSize(new Dimension(40, 25));
      btFiltreN.setMinimumSize(new Dimension(40, 25));
      btFiltreN.setMaximumSize(new Dimension(40, 30));
      btFiltreN.setBorderPainted(false);
      btFiltreN.setBackground(new Color(239, 239, 222));
      btFiltreN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreN.setName("btFiltreN");
      btFiltreN.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreN);
      
      // ---- btFiltreO ----
      btFiltreO.setText("O");
      btFiltreO.setBorder(null);
      btFiltreO.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreO.setPreferredSize(new Dimension(40, 25));
      btFiltreO.setMinimumSize(new Dimension(40, 25));
      btFiltreO.setMaximumSize(new Dimension(40, 30));
      btFiltreO.setBorderPainted(false);
      btFiltreO.setBackground(new Color(239, 239, 222));
      btFiltreO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreO.setName("btFiltreO");
      btFiltreO.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreO);
      
      // ---- btFiltreP ----
      btFiltreP.setText("P");
      btFiltreP.setBorder(null);
      btFiltreP.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreP.setPreferredSize(new Dimension(40, 25));
      btFiltreP.setMinimumSize(new Dimension(40, 25));
      btFiltreP.setMaximumSize(new Dimension(40, 30));
      btFiltreP.setBorderPainted(false);
      btFiltreP.setBackground(new Color(239, 239, 222));
      btFiltreP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreP.setName("btFiltreP");
      btFiltreP.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreP);
      
      // ---- btFiltreQ ----
      btFiltreQ.setText("Q");
      btFiltreQ.setBorder(null);
      btFiltreQ.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreQ.setPreferredSize(new Dimension(40, 25));
      btFiltreQ.setMinimumSize(new Dimension(40, 25));
      btFiltreQ.setMaximumSize(new Dimension(40, 30));
      btFiltreQ.setBorderPainted(false);
      btFiltreQ.setBackground(new Color(239, 239, 222));
      btFiltreQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreQ.setName("btFiltreQ");
      btFiltreQ.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreQ);
      
      // ---- btFiltreR ----
      btFiltreR.setText("R");
      btFiltreR.setBorder(null);
      btFiltreR.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreR.setPreferredSize(new Dimension(40, 25));
      btFiltreR.setMinimumSize(new Dimension(40, 25));
      btFiltreR.setMaximumSize(new Dimension(40, 30));
      btFiltreR.setBorderPainted(false);
      btFiltreR.setBackground(new Color(239, 239, 222));
      btFiltreR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreR.setName("btFiltreR");
      btFiltreR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreR);
      
      // ---- btFiltreS ----
      btFiltreS.setText("S");
      btFiltreS.setBorder(null);
      btFiltreS.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreS.setPreferredSize(new Dimension(40, 25));
      btFiltreS.setMinimumSize(new Dimension(40, 25));
      btFiltreS.setMaximumSize(new Dimension(40, 30));
      btFiltreS.setBorderPainted(false);
      btFiltreS.setBackground(new Color(239, 239, 222));
      btFiltreS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreS.setName("btFiltreS");
      btFiltreS.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreS);
      
      // ---- btFiltreT ----
      btFiltreT.setText("T");
      btFiltreT.setBorder(null);
      btFiltreT.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreT.setPreferredSize(new Dimension(40, 25));
      btFiltreT.setMinimumSize(new Dimension(40, 25));
      btFiltreT.setMaximumSize(new Dimension(40, 30));
      btFiltreT.setBorderPainted(false);
      btFiltreT.setBackground(new Color(239, 239, 222));
      btFiltreT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreT.setName("btFiltreT");
      btFiltreT.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreT);
      
      // ---- btFiltreU ----
      btFiltreU.setText("U");
      btFiltreU.setBorder(null);
      btFiltreU.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreU.setPreferredSize(new Dimension(40, 25));
      btFiltreU.setMinimumSize(new Dimension(40, 25));
      btFiltreU.setMaximumSize(new Dimension(40, 30));
      btFiltreU.setBorderPainted(false);
      btFiltreU.setBackground(new Color(239, 239, 222));
      btFiltreU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreU.setName("btFiltreU");
      btFiltreU.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreU);
      
      // ---- btFiltreV ----
      btFiltreV.setText("V");
      btFiltreV.setBorder(null);
      btFiltreV.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreV.setPreferredSize(new Dimension(40, 25));
      btFiltreV.setMinimumSize(new Dimension(40, 25));
      btFiltreV.setMaximumSize(new Dimension(40, 30));
      btFiltreV.setBorderPainted(false);
      btFiltreV.setBackground(new Color(239, 239, 222));
      btFiltreV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreV.setName("btFiltreV");
      btFiltreV.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreV);
      
      // ---- btFiltreW ----
      btFiltreW.setText("W");
      btFiltreW.setBorder(null);
      btFiltreW.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreW.setPreferredSize(new Dimension(40, 25));
      btFiltreW.setMinimumSize(new Dimension(40, 25));
      btFiltreW.setMaximumSize(new Dimension(40, 30));
      btFiltreW.setBorderPainted(false);
      btFiltreW.setBackground(new Color(239, 239, 222));
      btFiltreW.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreW.setName("btFiltreW");
      btFiltreW.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreW);
      
      // ---- btFiltreX ----
      btFiltreX.setText("X");
      btFiltreX.setBorder(null);
      btFiltreX.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreX.setPreferredSize(new Dimension(40, 25));
      btFiltreX.setMinimumSize(new Dimension(40, 25));
      btFiltreX.setMaximumSize(new Dimension(40, 30));
      btFiltreX.setBorderPainted(false);
      btFiltreX.setBackground(new Color(239, 239, 222));
      btFiltreX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreX.setName("btFiltreX");
      btFiltreX.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreX);
      
      // ---- btFiltreY ----
      btFiltreY.setText("Y");
      btFiltreY.setBorder(null);
      btFiltreY.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreY.setPreferredSize(new Dimension(40, 25));
      btFiltreY.setMinimumSize(new Dimension(40, 25));
      btFiltreY.setMaximumSize(new Dimension(40, 30));
      btFiltreY.setBorderPainted(false);
      btFiltreY.setBackground(new Color(239, 239, 222));
      btFiltreY.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreY.setName("btFiltreY");
      btFiltreY.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreY);
      
      // ---- btFiltreZ ----
      btFiltreZ.setText("Z");
      btFiltreZ.setBorder(null);
      btFiltreZ.setFont(new Font("sansserif", Font.BOLD, 14));
      btFiltreZ.setPreferredSize(new Dimension(40, 25));
      btFiltreZ.setMinimumSize(new Dimension(40, 25));
      btFiltreZ.setMaximumSize(new Dimension(40, 30));
      btFiltreZ.setBorderPainted(false);
      btFiltreZ.setBackground(new Color(239, 239, 222));
      btFiltreZ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      btFiltreZ.setName("btFiltreZ");
      btFiltreZ.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          btFiltreAlphaPerformed(e);
        }
      });
      pnlFiltreAlpha.add(btFiltreZ);
    }
    add(pnlFiltreAlpha,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(btFiltreTous);
    buttonGroup1.add(btFiltreA);
    buttonGroup1.add(btFiltreB);
    buttonGroup1.add(btFiltreC);
    buttonGroup1.add(btFiltreD);
    buttonGroup1.add(btFiltreE);
    buttonGroup1.add(btFiltreF);
    buttonGroup1.add(btFiltreG);
    buttonGroup1.add(btFiltreH);
    buttonGroup1.add(btFiltreI);
    buttonGroup1.add(btFiltreJ);
    buttonGroup1.add(btFiltreK);
    buttonGroup1.add(btFiltreL);
    buttonGroup1.add(btFiltreM);
    buttonGroup1.add(btFiltreN);
    buttonGroup1.add(btFiltreO);
    buttonGroup1.add(btFiltreP);
    buttonGroup1.add(btFiltreQ);
    buttonGroup1.add(btFiltreR);
    buttonGroup1.add(btFiltreS);
    buttonGroup1.add(btFiltreT);
    buttonGroup1.add(btFiltreU);
    buttonGroup1.add(btFiltreV);
    buttonGroup1.add(btFiltreW);
    buttonGroup1.add(btFiltreX);
    buttonGroup1.add(btFiltreY);
    buttonGroup1.add(btFiltreZ);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlFiltreAlpha;
  private JToggleButton btFiltreTous;
  private JToggleButton btFiltreA;
  private JToggleButton btFiltreB;
  private JToggleButton btFiltreC;
  private JToggleButton btFiltreD;
  private JToggleButton btFiltreE;
  private JToggleButton btFiltreF;
  private JToggleButton btFiltreG;
  private JToggleButton btFiltreH;
  private JToggleButton btFiltreI;
  private JToggleButton btFiltreJ;
  private JToggleButton btFiltreK;
  private JToggleButton btFiltreL;
  private JToggleButton btFiltreM;
  private JToggleButton btFiltreN;
  private JToggleButton btFiltreO;
  private JToggleButton btFiltreP;
  private JToggleButton btFiltreQ;
  private JToggleButton btFiltreR;
  private JToggleButton btFiltreS;
  private JToggleButton btFiltreT;
  private JToggleButton btFiltreU;
  private JToggleButton btFiltreV;
  private JToggleButton btFiltreW;
  private JToggleButton btFiltreX;
  private JToggleButton btFiltreY;
  private JToggleButton btFiltreZ;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
