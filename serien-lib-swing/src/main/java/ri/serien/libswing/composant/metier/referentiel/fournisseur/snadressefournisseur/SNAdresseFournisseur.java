/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.referentiel.fournisseur.snadressefournisseur;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;

import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;

/**
 * Composant d'affichage et de sélection d'un client.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une adresse fournisseur dans le logiciel. Il gère l'affichage d'une
 * adresse fournisseur et la sélection de l'une d'entre elles.
 * Il comprend un TextField et un bouton permettant de sélectionner une adresse fournisseur dans une boîte de dialogue.
 *
 * Utilisation :
 * - Ajouter un composant SNAdresseFournisseur à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé. Les informations nécessaires sont la session et l'établissement.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 * - utiliser setContexteSaisieDocumentAchat(true) si le composant est utilisé dans un contexte de saisie de document d'achat
 * Voir un exemple d'implémentation dans le VueOngletFournisseur.
 */
public class SNAdresseFournisseur extends SNComposantAvecEtablissement {
  private SNTexte tfTexteRecherche;
  private SNBoutonRecherche btnDialogueRechercheAdresseFournisseur;
  private AdresseFournisseur adresseFournisseurSelectionne = null;
  private boolean fenetreAdresseFournisseurAffichee = false;
  private boolean rafraichir = false;
  private String texteRecherchePrecedente = null;
  private String message = null;
  private CritereFournisseur critereAdresseFournisseur = new CritereFournisseur();
  private boolean keyEnterPressed = false;
  // Défini si le composant est utilisé dans le contexte d'une saisie de document d'achat
  private boolean isContexteSaisieDocumentAchat = false;

  /**
   * Constructeur par défaut.
   */
  public SNAdresseFournisseur() {
    super();
    setName("SNAdresseFournisseur");

    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);

    setLayout(new GridBagLayout());

    // Texte de recherche
    tfTexteRecherche = new SNTexte();
    tfTexteRecherche.setName("tfTexteRecherche");
    tfTexteRecherche.setEditable(true);
    tfTexteRecherche.setToolTipText(VueAdresseFournisseur.RECHERCHE_GENERIQUE_TOOLTIP);
    add(tfTexteRecherche,
        new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));

    // Bouton loupe
    btnDialogueRechercheAdresseFournisseur = new SNBoutonRecherche();
    btnDialogueRechercheAdresseFournisseur.setToolTipText("Recherche d'adresse fournisseur");
    btnDialogueRechercheAdresseFournisseur.setName("btnAdresseFournisseur");
    add(btnDialogueRechercheAdresseFournisseur, new GridBagConstraints());

    // Focus listener pour lancer la recherche quand on quitte la zone de saisie
    tfTexteRecherche.addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent e) {
        tfTexteRechercheFocusLost(e);
      }

      @Override
      public void focusGained(FocusEvent e) {
        tfTexteRechercheFocusGained(e);
      }
    });

    // ActionListener pour capturer l'utilisation de la touche entrée
    tfTexteRecherche.addActionListener(new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        lancerRecherche(false);
      }
    });

    // Etre notifier lorsque le bouton est cliqué
    btnDialogueRechercheAdresseFournisseur.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnDialogueRechercheAdresseFournisseurActionPerformed(e);
      }
    });
  }

  /**
   * Activer ou désactiver le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    tfTexteRecherche.setEnabled(pEnabled);
    btnDialogueRechercheAdresseFournisseur.setEnabled(pEnabled);
  }

  /**
   * Charger la liste des adresses fournisseurs.
   */
  public void charger(boolean pForcerRafraichissement, boolean pForcerAffichageDialogue) {
    // Tester s'il faut rafraîchir la liste
    if (!rafraichir && !pForcerRafraichissement) {
      return;
    }
    lancerRecherche(pForcerAffichageDialogue);

    rafraichir = false;
  }

  @Override
  public boolean hasFocus() {
    if (tfTexteRecherche != null && btnDialogueRechercheAdresseFournisseur != null) {
      return (tfTexteRecherche.hasFocus() || btnDialogueRechercheAdresseFournisseur.hasFocus());
    }
    return false;
  }

  /**
   * Initialiser les valeurs du composant pour se retrouver dans la même situation que si aucune adresse fournisseur n'est sélectionnée.
   */
  public void initialiserRecherche() {
    // Effacer les critères de recherche
    critereAdresseFournisseur.initialiser();

    // Effacer champs texte
    tfTexteRecherche.setText("");

    // Ne sélectionner aucun client
    setSelection(null);
  }

  /**
   * Lancer une recherche parmi les adresses fournisseurs
   */
  private void lancerRecherche(boolean forcerAffichageDialogue) {
    // Vérifier la session
    if (getIdSession() == null) {
      throw new MessageErreurException("La session du composant de sélection d'adresse fournisseur est invalide.");
    }

    // Ne rien faire si le texte à rechercher est vide
    if (tfTexteRecherche.getText().trim().isEmpty()) {
      setSelection(null);
      if (forcerAffichageDialogue) {
        afficherDialogueRechercheAdresseFournisseur(null);
      }
      return;
    }

    // Ne rien faire si le texte à rechercher n'a pas changé (sauf si la boîte de dialogue est forcée)
    if (!forcerAffichageDialogue && tfTexteRecherche.getText().equals(texteRecherchePrecedente)) {
      return;
    }

    // Lancer une recherche d'adresse fournisseur
    critereAdresseFournisseur.setIdEtablissement(getIdEtablissement());
    if (isContexteSaisieDocumentAchat) {
      critereAdresseFournisseur.setTypeRecherche(CritereFournisseur.RECHERCHER_ADRESSE_FOURNISSEURS_DOCUMENTACHAT);
    }
    else {
      critereAdresseFournisseur.setTypeRecherche(CritereFournisseur.RECHERCHER_TOUTES_ADRESSE_FOURNISSEURS);
    }
    critereAdresseFournisseur.setTexteRechercheGenerique(tfTexteRecherche.getText());
    List<IdAdresseFournisseur> listeIdAdresseFournisseur =
        ManagerServiceFournisseur.chargerListeIdAdresseFournisseur(getIdSession(), critereAdresseFournisseur);

    // Sélectionner automatiquement le seul client trouvé (sauf si la boîte de dialogue est forcée)
    if (listeIdAdresseFournisseur != null && !forcerAffichageDialogue && listeIdAdresseFournisseur.size() == 1) {
      setSelection(ManagerServiceFournisseur.chargerAdresseFournisseur(getIdSession(), listeIdAdresseFournisseur.get(0)));
    }
    // Afficher la boîte de dialogue si : affichage forcé de la boîte de dialogue, aucun ou plusieurs clients trouvés
    else {
      afficherDialogueRechercheAdresseFournisseur(listeIdAdresseFournisseur);
    }

    // Sélectionner automatiquement l'ensemble du champ afin que la prochaine saisie vienne en remplacement
    tfTexteRecherche.selectAll();
  }

  /**
   * Affiche la fenêtre de recherche des adresses fournisseurs
   */
  private void afficherDialogueRechercheAdresseFournisseur(List<IdAdresseFournisseur> pListeIdAdresseFournisseur) {
    // Sécurité pour éviter que la boîte de dialogue ne s'affiche deux fois
    if (fenetreAdresseFournisseurAffichee) {
      return;
    }
    fenetreAdresseFournisseurAffichee = true;

    // Afficher la boîte de dialogue de recherche d'une adresse fournisseur
    critereAdresseFournisseur.setIdEtablissement(getIdEtablissement());
    critereAdresseFournisseur.setTypeRecherche(CritereFournisseur.RECHERCHER_ADRESSE_FOURNISSEURS_DOCUMENTACHAT);
    critereAdresseFournisseur.setTexteRechercheGenerique(tfTexteRecherche.getText());
    ModeleAdresseFournisseur modele = new ModeleAdresseFournisseur(getSession());
    modele.setCritereFournisseur(critereAdresseFournisseur);
    modele.setListeIdAdresseFournisseur(pListeIdAdresseFournisseur);
    modele.setMessageInformation(message);
    VueAdresseFournisseur vue = new VueAdresseFournisseur(modele);
    vue.afficher();

    // Sélectionner le client retourné par la boîte de dialogue (ou aucun si null)
    if (modele.isSortieAvecValidation()) {
      setSelection(modele.getAdresseFournisseurSelectionnee());
    }
    // La boîte de dialogue est annulé, on remet l'adresse fournisseur sélectionnée précédement
    else {
      rafraichirTexteRecherche();
    }
    fenetreAdresseFournisseurAffichee = false;
  }

  /**
   * Rafraîchir le texte de la recherche avec le client sélectionné.
   */
  private void rafraichirTexteRecherche() {
    // Mettre à jour le composant
    if (adresseFournisseurSelectionne != null) {
      tfTexteRecherche.setText(adresseFournisseurSelectionne.toString());
    }
    else {
      tfTexteRecherche.setText("");
    }

    // Mémoriser le dernier texte pour ne pas relancer une recherche inutile
    texteRecherchePrecedente = tfTexteRecherche.getText();
  }

  // Méthodes évènementielles

  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      tfTexteRecherche.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void tfTexteRechercheFocusLost(FocusEvent e) {
    try {
      if (keyEnterPressed) {
        lancerRecherche(false);
      }
      keyEnterPressed = false;
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  private void btnDialogueRechercheAdresseFournisseurActionPerformed(ActionEvent e) {
    try {
      lancerRecherche(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  // Accesseurs

  /**
   * Adresse fournisseur sélectionné.
   */
  public AdresseFournisseur getSelection() {
    return adresseFournisseurSelectionne;
  }

  /**
   * Modifier l'adresse fournisseur sélectionnée.
   */
  public void setSelection(AdresseFournisseur pAdresseFournisseurSelectionne) {
    // Tester si la sélection a changé
    boolean bSelectionModifiee = !Constantes.equals(adresseFournisseurSelectionne, pAdresseFournisseurSelectionne);

    // Modifier le client sélectionné
    adresseFournisseurSelectionne = pAdresseFournisseurSelectionne;

    // Effacer les critères de recherche si aucun client n'est sélectionné
    if (adresseFournisseurSelectionne == null) {
      critereAdresseFournisseur.initialiser();
    }

    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();

    // Notifier uniquement si la sélection a changé
    if (bSelectionModifiee) {
      fireValueChanged();
    }
  }

  /**
   * Modifier l'adresse fournisseur sélectionnée à partir de son identifiant.
   */
  public void setSelectionParId(IdAdresseFournisseur pIdAdresseFournisseurSelectionne) {
    if (pIdAdresseFournisseurSelectionne == null) {
      return;
    }

    boolean bSelectionModifiee = true;
    if (adresseFournisseurSelectionne != null) {
      bSelectionModifiee = !Constantes.equals(adresseFournisseurSelectionne.getId(), pIdAdresseFournisseurSelectionne);
    }

    List<IdAdresseFournisseur> listeIdAdresseFournisseur = new ArrayList<IdAdresseFournisseur>();
    listeIdAdresseFournisseur.add(pIdAdresseFournisseurSelectionne);

    // Modifier le client sélectionné
    adresseFournisseurSelectionne = ManagerServiceFournisseur.chargerAdresseFournisseur(getIdSession(), listeIdAdresseFournisseur.get(0));

    // Effacer les critères de recherche si aucun client n'est sélectionné
    if (adresseFournisseurSelectionne == null) {
      critereAdresseFournisseur.initialiser();
    }

    // Rafraichir l'affichage dans tous les cas
    // Le contenu du TextField peut avoir été modifié sans que la sélection ait été changé
    rafraichirTexteRecherche();

    // Notifier uniquement si la sélection a changé
    if (bSelectionModifiee) {
      fireValueChanged();
    }
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String pMessage) {
    message = pMessage;
  }

  /**
   * Renvoyer si le composant sert dans le cadre d'une saisie de document d'achat
   */
  public boolean isContexteSaisieDocumentAchat() {
    return isContexteSaisieDocumentAchat;
  }

  /**
   * Mettre à jour si le composant sert dans le cadre d'une saisie de document d'achat
   */
  public void setContexteSaisieDocumentAchat(boolean pIsSaisieDocumentAchat) {
    isContexteSaisieDocumentAchat = pIsSaisieDocumentAchat;
  }

  /**
   * Mettre le focus sur le champ saisie de texte.
   */
  @Override
  public void requestFocus() {
    tfTexteRecherche.requestFocus();
  }

}
