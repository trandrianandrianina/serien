/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique;

import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.ListeSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.SectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.idSectionAnalytique;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'une SectionAnalytique.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'une SectionAnalytique dans le logiciel. Il gère l'affichage des
 * SectionAnalytique existantes et la sélection de l'une d'entre elle.
 *
 * Utilisation :
 * - Ajouter un composant SNSectionAnalytique à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le SGVM22FM_B2
 */
public class SNSectionAnalytique extends SNComboBoxObjetMetier<idSectionAnalytique, SectionAnalytique, ListeSectionAnalytique> {
  
  /**
   * Constructeur par défaut
   */
  public SNSectionAnalytique() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeSectionAnalytique());
  }
  
  /**
   * Sélectionner une section analytique de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampSectionAnalytique) {
    if (pChampSectionAnalytique == null || pLexical.HostFieldGetData(pChampSectionAnalytique) == null) {
      throw new MessageErreurException("Impossible de sélectionner la section analytique car son code est invalide.");
    }
    
    idSectionAnalytique idSection = null;
    String valeurSectionAnalytique = pLexical.HostFieldGetData(pChampSectionAnalytique);
    if (valeurSectionAnalytique != null && !valeurSectionAnalytique.trim().isEmpty()) {
      idSection = idSectionAnalytique.getInstance(getIdEtablissement(), pLexical.HostFieldGetData(pChampSectionAnalytique));
    }
    setIdSelection(idSection);
  }
  
  /**
   * Renseigne les champs RPG correspondant à la section analytique sélectionnée.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampSectionAnalytique) {
    SectionAnalytique section = getSelection();
    if (section != null) {
      pLexical.HostFieldPutData(pChampSectionAnalytique, 0, section.getId().getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampSectionAnalytique, 0, "");
    }
  }
}
