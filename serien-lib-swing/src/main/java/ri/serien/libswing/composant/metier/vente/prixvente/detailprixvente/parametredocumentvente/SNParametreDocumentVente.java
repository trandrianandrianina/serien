/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.parametredocumentvente;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumAssietteTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.EnumModeTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametredocumentvente.ParametreDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.checkbox.SNCheckBoxReduit;
import ri.serien.libswing.composant.primitif.combobox.SNComboBoxReduit;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;

/**
 * Ce composant permet d'afficher tous les paramètres d'un document de vente.
 */
public class SNParametreDocumentVente extends SNPanelTitre {
  private static final String TITRE = "Document de vente";
  
  // Variables
  private ModeleDialogueDetailPrixVente modele = null;
  private boolean evenementsActifs = false;
  private ParametreDocumentVente parametreDocumentVente = null;
  private ParametreDocumentVente parametreDocumentVenteInitial = null;
  
  /**
   * Constructeur.
   */
  public SNParametreDocumentVente() {
    super();
    initComponents();
    
    // Renseigner la liste déroulante des modes d'application des taux de remises
    cbModeTauxRemise.addItem("Aucun");
    for (EnumModeTauxRemise modeTauxRemise : EnumModeTauxRemise.values()) {
      cbModeTauxRemise.addItem(modeTauxRemise);
    }
    
    // Renseigner la liste déroulante des assiettes des taux de remises
    cbAssietteTauxRemise.addItem("Aucun");
    for (EnumAssietteTauxRemise assietteTauxRemise : EnumAssietteTauxRemise.values()) {
      cbAssietteTauxRemise.addItem(assietteTauxRemise);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modèle associé au composant graphique.
   * @return Modèle de la boîte de dialogue.
   */
  public ModeleDialogueDetailPrixVente getModele() {
    return modele;
  }
  
  /**
   * Modifier le modèle associé au composant graphique.
   * @param pModele Modèle de la boîte de dialogue.
   */
  public void setModele(ModeleDialogueDetailPrixVente pModele) {
    modele = pModele;
  }
  
  /**
   * Indiquer si les évènements doivent être traités.
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraichissement des données du composant.
   */
  public void rafraichir() {
    // Empêcher les évènements pendant le rafraichissement
    evenementsActifs = false;
    
    // Mettre à jour les raccourcis pour être sûr d'avoir les derniers objets
    parametreDocumentVente = getModele().getParametreDocumentVente();
    parametreDocumentVenteInitial = getModele().getParametreDocumentVenteInitial();
    
    // Rafraichir les composants
    rafraichirTitre();
    rafraichirPrixGaranti();
    rafraichirModeTTC();
    rafraichirLibelleTauxTVA();
    rafraichirTauxTVA1();
    rafraichirTauxTVA2();
    rafraichirTauxTVA3();
    rafraichirColonneTarif();
    rafraichirLibelleTauxRemise();
    rafraichirTauxRemise1();
    rafraichirTauxRemise2();
    rafraichirTauxRemise3();
    rafraichirTauxRemise4();
    rafraichirTauxRemise5();
    rafraichirTauxRemise6();
    rafraichirModeTauxRemise();
    rafraichirAssietteTauxRemise();
    
    // Réactiver les évènements pendant le rafraichissement
    evenementsActifs = true;
  }
  
  /**
   * Rafraichir le titre de l'encart.
   */
  private void rafraichirTitre() {
    if (parametreDocumentVente != null && parametreDocumentVente.getIdDocumentVente() != null) {
      setTitre(TITRE + " " + parametreDocumentVente.getIdDocumentVente());
    }
    else {
      setTitre(TITRE);
    }
  }

  /**
   * Rafraichir si le prix est garanti.
   */
  private void rafraichirPrixGaranti() {
    if (parametreDocumentVente != null && parametreDocumentVente.isPrixGaranti()) {
      cbPrixGaranti.setSelected(true);
    }
    else {
      cbPrixGaranti.setSelected(false);
    }
  }

  /**
   * Rafraichir le mode HT ou TTC.
   */
  private void rafraichirModeTTC() {
    if (parametreDocumentVente != null && parametreDocumentVente.isModeTTC()) {
      cbModeTTC.setSelected(true);
    }
    else {
      cbModeTTC.setSelected(false);
    }
    
    cbModeTTC.setEnabled(modele != null && getModele().isModeSimulation());
    lbModeTTC.setImportance(parametreDocumentVente != null && parametreDocumentVenteInitial != null
        && !Constantes.equals(parametreDocumentVente.isModeTTC(), parametreDocumentVenteInitial.isModeTTC()));
  }
  
  /**
   * Rafraichir le libellé du taux de TVA.
   */
  private void rafraichirLibelleTauxTVA() {
    lbTauxTVA.setImportance(parametreDocumentVente != null && parametreDocumentVenteInitial != null
        && (!Constantes.equals(parametreDocumentVente.getTauxTVA1(), parametreDocumentVenteInitial.getTauxTVA1())
            || !Constantes.equals(parametreDocumentVente.getTauxTVA2(), parametreDocumentVenteInitial.getTauxTVA2())
            || !Constantes.equals(parametreDocumentVente.getTauxTVA3(), parametreDocumentVenteInitial.getTauxTVA3())));
  }

  /**
   * Rafraichir le taux de TVA 1.
   */
  private void rafraichirTauxTVA1() {
    if (parametreDocumentVente != null && parametreDocumentVente.getTauxTVA1() != null) {
      tfTauxTVA1.setText(parametreDocumentVente.getTauxTVA1().getTexteSansSymbole());
    }
    else {
      tfTauxTVA1.setText("");
    }
    
    tfTauxTVA1.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de TVA 2.
   */
  private void rafraichirTauxTVA2() {
    if (parametreDocumentVente != null && parametreDocumentVente.getTauxTVA2() != null) {
      tfTauxTVA2.setText(parametreDocumentVente.getTauxTVA2().getTexteSansSymbole());
    }
    else {
      tfTauxTVA2.setText("");
    }
    
    tfTauxTVA2.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir le taux de TVA 3.
   */
  private void rafraichirTauxTVA3() {
    if (parametreDocumentVente != null && parametreDocumentVente.getTauxTVA3() != null) {
      tfTauxTVA3.setText(parametreDocumentVente.getTauxTVA3().getTexteSansSymbole());
    }
    else {
      tfTauxTVA3.setText("");
    }
    
    tfTauxTVA3.setEnabled(modele != null && getModele().isModeSimulation());
  }

  /**
   * Rafraichir la colonne tarif.
   */
  private void rafraichirColonneTarif() {
    if (parametreDocumentVente != null && parametreDocumentVente.getNumeroColonneTarif() != null) {
      Integer numeroColonneTarif = parametreDocumentVente.getNumeroColonneTarif();
      if (numeroColonneTarif >= 1 && numeroColonneTarif <= 10) {
        cbColonneTarif.setSelectedIndex(numeroColonneTarif);
      }
      else {
        cbColonneTarif.setSelectedIndex(0);
      }
    }
    else {
      cbColonneTarif.setSelectedIndex(0);
    }
    
    cbColonneTarif.setEnabled(modele != null && getModele().isModeSimulation());
    lbColonneTarif.setImportance(parametreDocumentVente != null && parametreDocumentVenteInitial != null
        && !Constantes.equals(parametreDocumentVente.getNumeroColonneTarif(), parametreDocumentVenteInitial.getNumeroColonneTarif()));
  }
  
  /**
   * Rafraichir le libellé du taux de remise.
   */
  private void rafraichirLibelleTauxRemise() {
    lbTauxRemise.setImportance(parametreDocumentVente != null && parametreDocumentVenteInitial != null
        && (!Constantes.equals(parametreDocumentVente.getTauxRemise1(), parametreDocumentVenteInitial.getTauxRemise1())
            || !Constantes.equals(parametreDocumentVente.getTauxRemise2(), parametreDocumentVenteInitial.getTauxRemise2())
            || !Constantes.equals(parametreDocumentVente.getTauxRemise3(), parametreDocumentVenteInitial.getTauxRemise3())
            || !Constantes.equals(parametreDocumentVente.getTauxRemise4(), parametreDocumentVenteInitial.getTauxRemise4())
            || !Constantes.equals(parametreDocumentVente.getTauxRemise5(), parametreDocumentVenteInitial.getTauxRemise5())
            || !Constantes.equals(parametreDocumentVente.getTauxRemise6(), parametreDocumentVenteInitial.getTauxRemise6())));
  }
  
  /**
   * Rafraichir le taux de remise 1.
   */
  private void rafraichirTauxRemise1() {
    if (parametreDocumentVente != null && parametreDocumentVente.getTauxRemise1() != null) {
      tfTauxRemise1.setText(parametreDocumentVente.getTauxRemise1().getTexteSansSymbole());
    }
    else {
      tfTauxRemise1.setText("");
    }
    
    tfTauxRemise1.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 2.
   */
  private void rafraichirTauxRemise2() {
    if (parametreDocumentVente != null && parametreDocumentVente.getTauxRemise2() != null) {
      tfTauxRemise2.setText(parametreDocumentVente.getTauxRemise2().getTexteSansSymbole());
    }
    else {
      tfTauxRemise2.setText("");
    }
    
    tfTauxRemise2.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 3.
   */
  private void rafraichirTauxRemise3() {
    if (parametreDocumentVente != null && parametreDocumentVente.getTauxRemise3() != null) {
      tfTauxRemise3.setText(parametreDocumentVente.getTauxRemise3().getTexteSansSymbole());
    }
    else {
      tfTauxRemise3.setText("");
    }
    
    tfTauxRemise3.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 4.
   */
  private void rafraichirTauxRemise4() {
    if (parametreDocumentVente != null && parametreDocumentVente.getTauxRemise4() != null) {
      tfTauxRemise4.setText(parametreDocumentVente.getTauxRemise4().getTexteSansSymbole());
    }
    else {
      tfTauxRemise4.setText("");
    }
    
    tfTauxRemise4.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 5.
   */
  private void rafraichirTauxRemise5() {
    if (parametreDocumentVente != null && parametreDocumentVente.getTauxRemise5() != null) {
      tfTauxRemise5.setText(parametreDocumentVente.getTauxRemise5().getTexteSansSymbole());
    }
    else {
      tfTauxRemise5.setText("");
    }
    
    tfTauxRemise5.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le taux de remise 6.
   */
  private void rafraichirTauxRemise6() {
    if (parametreDocumentVente != null && parametreDocumentVente.getTauxRemise6() != null) {
      tfTauxRemise6.setText(parametreDocumentVente.getTauxRemise6().getTexteSansSymbole());
    }
    else {
      tfTauxRemise6.setText("");
    }
    
    tfTauxRemise6.setEnabled(modele != null && getModele().isModeSimulation());
  }
  
  /**
   * Rafraichir le mode d'application des taux de remises.
   */
  private void rafraichirModeTauxRemise() {
    if (parametreDocumentVente != null && parametreDocumentVente.getModeTauxRemise() != null) {
      cbModeTauxRemise.setSelectedItem(parametreDocumentVente.getModeTauxRemise());
    }
    else {
      cbModeTauxRemise.setSelectedIndex(0);
    }
    
    cbModeTauxRemise.setEnabled(modele != null && modele.isModeSimulation());
    lbModeTauxRemise.setImportance(parametreDocumentVente != null && parametreDocumentVenteInitial != null
        && !Constantes.equals(parametreDocumentVente.getModeTauxRemise(), parametreDocumentVenteInitial.getModeTauxRemise()));
  }
  
  /**
   * Rafraichir l'assiette des taux de remises.
   */
  private void rafraichirAssietteTauxRemise() {
    if (parametreDocumentVente != null && parametreDocumentVente.getAssietteTauxRemise() != null) {
      cbAssietteTauxRemise.setSelectedItem(parametreDocumentVente.getAssietteTauxRemise());
    }
    else {
      cbAssietteTauxRemise.setSelectedIndex(0);
    }
    
    cbAssietteTauxRemise.setEnabled(modele != null && modele.isModeSimulation());
    lbAssietteTauxRemise.setImportance(parametreDocumentVente != null && parametreDocumentVenteInitial != null
        && !Constantes.equals(parametreDocumentVente.getAssietteTauxRemise(), parametreDocumentVenteInitial.getAssietteTauxRemise()));
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void cbModeTTCActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      modele.modifierModeTTCDocumentVente(cbModeTTC.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxTVA1FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxTVADocumentVente(tfTauxTVA1.getText(), 1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxTVA2FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxTVADocumentVente(tfTauxTVA2.getText(), 2);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxTVA3FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxTVADocumentVente(tfTauxTVA3.getText(), 3);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }

  private void cbColonneTarifItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbColonneTarif.getSelectedIndex() >= 1 && cbColonneTarif.getSelectedIndex() <= 10) {
        getModele().modifierColonneTarifDocumentVente(cbColonneTarif.getSelectedIndex());
      }
      else {
        getModele().modifierColonneTarifDocumentVente(0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise1FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseDocumentVente(tfTauxRemise1.getText(), 1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise2FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseDocumentVente(tfTauxRemise2.getText(), 2);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise3FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseDocumentVente(tfTauxRemise3.getText(), 3);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise4FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseDocumentVente(tfTauxRemise4.getText(), 4);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise5FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseDocumentVente(tfTauxRemise5.getText(), 5);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTauxRemise6FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTauxRemiseDocumentVente(tfTauxRemise6.getText(), 6);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbModeTauxRemiseItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbModeTauxRemise.getSelectedItem() instanceof EnumModeTauxRemise) {
        getModele().modifierModeTauxRemiseDocumentVente((EnumModeTauxRemise) cbModeTauxRemise.getSelectedItem());
      }
      else {
        getModele().modifierModeTauxRemiseDocumentVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbAssietteTauxRemiseItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbAssietteTauxRemise.getSelectedItem() instanceof EnumAssietteTauxRemise) {
        getModele().modifierAssiettetauxRemiseDocumentVente((EnumAssietteTauxRemise) cbAssietteTauxRemise.getSelectedItem());
      }
      else {
        getModele().modifierAssiettetauxRemiseDocumentVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbPrixGaranti = new SNLabelChamp();
    cbPrixGaranti = new SNCheckBoxReduit();
    lbModeTTC = new SNLabelChamp();
    cbModeTTC = new SNCheckBoxReduit();
    lbTauxTVA = new SNLabelChamp();
    pnlTauxTVA = new SNPanel();
    tfTauxTVA1 = new SNTexte();
    tfTauxTVA2 = new SNTexte();
    tfTauxTVA3 = new SNTexte();
    lbColonneTarif = new SNLabelChamp();
    cbColonneTarif = new SNComboBoxReduit();
    lbTauxRemise = new SNLabelChamp();
    pnlTauxRemise = new SNPanel();
    tfTauxRemise1 = new SNTexte();
    tfTauxRemise2 = new SNTexte();
    tfTauxRemise3 = new SNTexte();
    tfTauxRemise4 = new SNTexte();
    tfTauxRemise5 = new SNTexte();
    tfTauxRemise6 = new SNTexte();
    lbModeTauxRemise = new SNLabelChamp();
    cbModeTauxRemise = new SNComboBoxReduit();
    lbAssietteTauxRemise = new SNLabelChamp();
    cbAssietteTauxRemise = new SNComboBoxReduit();
    
    // ======== this ========
    setTitre("Document de vente");
    setModeReduit(true);
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.4, 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    
    // ---- lbPrixGaranti ----
    lbPrixGaranti.setModeReduit(true);
    lbPrixGaranti.setText("Prix garanti");
    lbPrixGaranti.setMinimumSize(new Dimension(30, 22));
    lbPrixGaranti.setPreferredSize(new Dimension(30, 22));
    lbPrixGaranti.setName("lbPrixGaranti");
    add(lbPrixGaranti,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbPrixGaranti ----
    cbPrixGaranti.setEnabled(false);
    cbPrixGaranti.setName("cbPrixGaranti");
    add(cbPrixGaranti,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbModeTTC ----
    lbModeTTC.setText("Mode TTC");
    lbModeTTC.setModeReduit(true);
    lbModeTTC.setName("lbModeTTC");
    add(lbModeTTC,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbModeTTC ----
    cbModeTTC.setEnabled(false);
    cbModeTTC.setName("cbModeTTC");
    cbModeTTC.addActionListener(e -> cbModeTTCActionPerformed(e));
    add(cbModeTTC,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbTauxTVA ----
    lbTauxTVA.setModeReduit(true);
    lbTauxTVA.setText("Taux TVA (%)");
    lbTauxTVA.setMinimumSize(new Dimension(30, 22));
    lbTauxTVA.setPreferredSize(new Dimension(30, 22));
    lbTauxTVA.setName("lbTauxTVA");
    add(lbTauxTVA, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 3), 0, 0));
    
    // ======== pnlTauxTVA ========
    {
      pnlTauxTVA.setName("pnlTauxTVA");
      pnlTauxTVA.setLayout(new GridLayout(1, 0, 3, 0));
      
      // ---- tfTauxTVA1 ----
      tfTauxTVA1.setEnabled(false);
      tfTauxTVA1.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxTVA1.setModeReduit(true);
      tfTauxTVA1.setMaximumSize(new Dimension(40, 22));
      tfTauxTVA1.setPreferredSize(new Dimension(40, 22));
      tfTauxTVA1.setMinimumSize(new Dimension(40, 22));
      tfTauxTVA1.setName("tfTauxTVA1");
      tfTauxTVA1.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxTVA1FocusLost(e);
        }
      });
      pnlTauxTVA.add(tfTauxTVA1);
      
      // ---- tfTauxTVA2 ----
      tfTauxTVA2.setEnabled(false);
      tfTauxTVA2.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxTVA2.setModeReduit(true);
      tfTauxTVA2.setMaximumSize(new Dimension(40, 22));
      tfTauxTVA2.setMinimumSize(new Dimension(40, 22));
      tfTauxTVA2.setPreferredSize(new Dimension(40, 22));
      tfTauxTVA2.setName("tfTauxTVA2");
      tfTauxTVA2.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxTVA2FocusLost(e);
        }
      });
      pnlTauxTVA.add(tfTauxTVA2);
      
      // ---- tfTauxTVA3 ----
      tfTauxTVA3.setEnabled(false);
      tfTauxTVA3.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxTVA3.setModeReduit(true);
      tfTauxTVA3.setMaximumSize(new Dimension(40, 22));
      tfTauxTVA3.setMinimumSize(new Dimension(40, 22));
      tfTauxTVA3.setPreferredSize(new Dimension(40, 22));
      tfTauxTVA3.setName("tfTauxTVA3");
      tfTauxTVA3.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxTVA3FocusLost(e);
        }
      });
      pnlTauxTVA.add(tfTauxTVA3);
    }
    add(pnlTauxTVA,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbColonneTarif ----
    lbColonneTarif.setModeReduit(true);
    lbColonneTarif.setText("Colonne tarif");
    lbColonneTarif.setMinimumSize(new Dimension(100, 22));
    lbColonneTarif.setPreferredSize(new Dimension(100, 22));
    lbColonneTarif.setName("lbColonneTarif");
    add(lbColonneTarif,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbColonneTarif ----
    cbColonneTarif.setModel(new DefaultComboBoxModel<>(new String[] { "Aucune", "Colonne 1", "Colonne 2", "Colonne 3", "Colonne 4",
        "Colonne 5", "Colonne 6", "Colonne 7", "Colonne 8", "Colonne 9", "Colonne 10" }));
    cbColonneTarif.setSelectedIndex(-1);
    cbColonneTarif.setEnabled(false);
    cbColonneTarif.setName("cbColonneTarif");
    cbColonneTarif.addItemListener(e -> cbColonneTarifItemStateChanged(e));
    add(cbColonneTarif,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbTauxRemise ----
    lbTauxRemise.setModeReduit(true);
    lbTauxRemise.setText("Taux remises (%)");
    lbTauxRemise.setMinimumSize(new Dimension(30, 22));
    lbTauxRemise.setPreferredSize(new Dimension(30, 22));
    lbTauxRemise.setName("lbTauxRemise");
    add(lbTauxRemise, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 3), 0, 0));
    
    // ======== pnlTauxRemise ========
    {
      pnlTauxRemise.setName("pnlTauxRemise");
      pnlTauxRemise.setLayout(new GridLayout(2, 0, 3, 0));
      
      // ---- tfTauxRemise1 ----
      tfTauxRemise1.setEnabled(false);
      tfTauxRemise1.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise1.setModeReduit(true);
      tfTauxRemise1.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise1.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise1.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise1.setName("tfTauxRemise1");
      tfTauxRemise1.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise1FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise1);
      
      // ---- tfTauxRemise2 ----
      tfTauxRemise2.setEnabled(false);
      tfTauxRemise2.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise2.setModeReduit(true);
      tfTauxRemise2.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise2.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise2.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise2.setName("tfTauxRemise2");
      tfTauxRemise2.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise2FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise2);
      
      // ---- tfTauxRemise3 ----
      tfTauxRemise3.setEnabled(false);
      tfTauxRemise3.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise3.setModeReduit(true);
      tfTauxRemise3.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise3.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise3.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise3.setName("tfTauxRemise3");
      tfTauxRemise3.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise3FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise3);
      
      // ---- tfTauxRemise4 ----
      tfTauxRemise4.setEnabled(false);
      tfTauxRemise4.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise4.setModeReduit(true);
      tfTauxRemise4.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise4.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise4.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise4.setName("tfTauxRemise4");
      tfTauxRemise4.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise4FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise4);
      
      // ---- tfTauxRemise5 ----
      tfTauxRemise5.setEnabled(false);
      tfTauxRemise5.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise5.setModeReduit(true);
      tfTauxRemise5.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise5.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise5.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise5.setName("tfTauxRemise5");
      tfTauxRemise5.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise5FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise5);
      
      // ---- tfTauxRemise6 ----
      tfTauxRemise6.setEnabled(false);
      tfTauxRemise6.setHorizontalAlignment(SwingConstants.RIGHT);
      tfTauxRemise6.setModeReduit(true);
      tfTauxRemise6.setMaximumSize(new Dimension(40, 22));
      tfTauxRemise6.setMinimumSize(new Dimension(40, 22));
      tfTauxRemise6.setPreferredSize(new Dimension(40, 22));
      tfTauxRemise6.setName("tfTauxRemise6");
      tfTauxRemise6.addFocusListener(new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
          tfTauxRemise6FocusLost(e);
        }
      });
      pnlTauxRemise.add(tfTauxRemise6);
    }
    add(pnlTauxRemise,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbModeTauxRemise ----
    lbModeTauxRemise.setModeReduit(true);
    lbModeTauxRemise.setText("Mode taux remises");
    lbModeTauxRemise.setMinimumSize(new Dimension(30, 22));
    lbModeTauxRemise.setPreferredSize(new Dimension(30, 22));
    lbModeTauxRemise.setName("lbModeTauxRemise");
    add(lbModeTauxRemise,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbModeTauxRemise ----
    cbModeTauxRemise.setEnabled(false);
    cbModeTauxRemise.setName("cbModeTauxRemise");
    cbModeTauxRemise.addItemListener(e -> cbModeTauxRemiseItemStateChanged(e));
    add(cbModeTauxRemise,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbAssietteTauxRemise ----
    lbAssietteTauxRemise.setModeReduit(true);
    lbAssietteTauxRemise.setText("Assiette taux remise");
    lbAssietteTauxRemise.setMinimumSize(new Dimension(30, 22));
    lbAssietteTauxRemise.setPreferredSize(new Dimension(30, 22));
    lbAssietteTauxRemise.setName("lbAssietteTauxRemise");
    add(lbAssietteTauxRemise,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- cbAssietteTauxRemise ----
    cbAssietteTauxRemise.setEnabled(false);
    cbAssietteTauxRemise.setName("cbAssietteTauxRemise");
    cbAssietteTauxRemise.addItemListener(e -> cbAssietteTauxRemiseItemStateChanged(e));
    add(cbAssietteTauxRemise,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbPrixGaranti;
  private SNCheckBoxReduit cbPrixGaranti;
  private SNLabelChamp lbModeTTC;
  private SNCheckBoxReduit cbModeTTC;
  private SNLabelChamp lbTauxTVA;
  private SNPanel pnlTauxTVA;
  private SNTexte tfTauxTVA1;
  private SNTexte tfTauxTVA2;
  private SNTexte tfTauxTVA3;
  private SNLabelChamp lbColonneTarif;
  private SNComboBoxReduit cbColonneTarif;
  private SNLabelChamp lbTauxRemise;
  private SNPanel pnlTauxRemise;
  private SNTexte tfTauxRemise1;
  private SNTexte tfTauxRemise2;
  private SNTexte tfTauxRemise3;
  private SNTexte tfTauxRemise4;
  private SNTexte tfTauxRemise5;
  private SNTexte tfTauxRemise6;
  private SNLabelChamp lbModeTauxRemise;
  private SNComboBoxReduit cbModeTauxRemise;
  private SNLabelChamp lbAssietteTauxRemise;
  private SNComboBoxReduit cbAssietteTauxRemise;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
