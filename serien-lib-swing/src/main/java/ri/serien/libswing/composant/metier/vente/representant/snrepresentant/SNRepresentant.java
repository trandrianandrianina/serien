/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.metier.vente.representant.snrepresentant;

import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.commun.representant.ListeRepresentant;
import ri.serien.libcommun.gescom.commun.representant.Representant;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComboBoxObjetMetier;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Composant d'affichage et de sélection d'un représentant.
 *
 * Ce composant doit être utilisé pour tout affichage et choix d'un représentant dans le logiciel. Il gère l'affichage des représentants
 * existants pour un représentant donné et la sélection de l'un d'entre eux.
 *
 * Utilisation :
 * - Ajouter un composant SNRepresentant à un écran.
 * - Utiliser les accesseurs set...() pour configurer le composant. Pour information, les accesseurs mettent à jour un attribut
 * "rafraichir" lorsque leur valeur a changé.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Voir un exemple d'implémentation dans le RGVX08FM_A4.
 */
public class SNRepresentant extends SNComboBoxObjetMetier<IdRepresentant, Representant, ListeRepresentant> {
  
  /**
   * Constructeur par défaut.
   */
  public SNRepresentant() {
    super();
  }
  
  /**
   * Chargement des données de la combo
   */
  public void charger(boolean pForcerRafraichissement) {
    charger(pForcerRafraichissement, new ListeRepresentant());
  }
  
  /**
   * Sélectionner un représentant de la liste déroulante à partir des champs RPG.
   */
  @Override
  public void setSelectionParChampRPG(Lexical pLexical, String pChampRepresentant) {
    // Récupérer la valeur du champ
    String valeurRepresentant = pLexical.HostFieldGetData(pChampRepresentant).trim();
    if (valeurRepresentant == null) {
      throw new MessageErreurException("Impossible de sélectionner le représentant car le nom du champ est invalide.");
    }
    
    // Construire l'identifiant du représentant
    IdRepresentant idRepresentant = null;
    if (!valeurRepresentant.trim().isEmpty() && !valeurRepresentant.equals("**")) {
      idRepresentant = IdRepresentant.getInstance(getIdEtablissement(), valeurRepresentant);
    }
    
    // Sélectionner le représentant dans la liste déroulante
    setIdSelection(idRepresentant);
  }
  
  /**
   * Renseigner le champs RPG correspondant au représentant sélectionné.
   */
  @Override
  public void renseignerChampRPG(Lexical pLexical, String pChampRepresentant) {
    IdRepresentant idRepresentant = getIdSelection();
    if (idRepresentant != null) {
      pLexical.HostFieldPutData(pChampRepresentant, 0, idRepresentant.getCode());
    }
    else {
      pLexical.HostFieldPutData(pChampRepresentant, 0, "");
    }
  }
}
