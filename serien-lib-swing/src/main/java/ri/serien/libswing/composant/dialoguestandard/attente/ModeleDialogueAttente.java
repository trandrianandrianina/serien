/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.dialoguestandard.attente;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue "Message d'information".
 */
public class ModeleDialogueAttente extends AbstractModeleDialogue {
  private final String MESSAGE_PAR_DEFAUT = "Veuillez patienter ...";

  // Variables
  private String message = "";

  /**
   * Constructeur.
   */
  public ModeleDialogueAttente() {
    super(null);
  }

  // Méthodes standards du modèle

  @Override
  public void initialiserDonnees() {
  }

  @Override
  public void chargerDonnees() {
  }

  /**
   * Renseigner le message utilisateur.
   * Un message par défaut est affiché si le message reçu n'est pas valide (null ou vide).
   */
  public void setMessage(String pMessage) {
    // Mettre un message par défaut en cas d'absence de messsage
    if (pMessage != null && !pMessage.trim().isEmpty()) {
      message = pMessage;
    }
    else {
      message = MESSAGE_PAR_DEFAUT;
    }

    // Tracer le message en enlevant tous les retours à la ligne éventuels
    Trace.info(message.replaceAll("(\\r|\\n)", " "));
  }

  // -- Accesseurs

  /**
   * Message d'information
   */
  public String getMessage() {
    return message;
  }
}
