/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import java.awt.event.KeyEvent;

/**
 * Bouton pré-configuré (ne pas ajouter de nouvelles valeurs dans cette énumération sans en discuter avec l'équipe).
 *
 * Cette énumération contient les boutons standardisés. Chaque bouton a un libellé et une fonction bien précise. Afin d'avoir un logiciel
 * homogène, il est fortement recommandé d'utiliser ces boutons lorsque la fonction proposé par le bouton correspond au besoin de
 * l'écran.
 *
 * Le premier paramètre est la catégorie de boutons (validation, annulation, standard ou image). Le second paramètre est le libellé du
 * bouton pour les boutons textuels ou le nom de l'image du bouton pour les boutons images. Le troisème paramètre est le mnémonique
 * associé au bouton, soit sous forme de caractère (char) soit sous forme de code unicode (int).
 *
 * Boutons de validation :
 * - VALIDER : valider une modification ou une action en cours.
 * - EDITER : comme "VALIDER" mais suivi de la génération d'une édition.
 * - CONSULTER : afficher l'élément sélectionné (écran liste/détail).
 * - CONTINUER : passer à la page suivante dans un assistant.
 * - CONNECTER : se connecter au logiciel.
 * - RECHERCHER : lancer une recherche.
 *
 * Boutons d'annulation :
 * - ANNULER : annuler une modification ou une action en cours.
 * - FERMER : fermer un écran sur lequel aucune saisie ou action n'a été effectué (rien à valider ou annuler).
 * - RETOURNER_RECHERCHE : comme "FERMER" mais avec retour au résultat de la recherche (écran liste/détail).
 * - QUITTER : comme "FERMER" mais cela ferme la session en cours (l'onglet de la barre de session disparaît).
 *
 * Boutons standard :
 * - INITIALISER RECHERCHE : effacer les critères de recherche et la liste de résultats.
 *
 * Boutons image :
 * - NAVIGATION_PRECEDENT : se déplacer sur le résultat précédent d'une recherche.
 * - NAVIGATION_SUIVANT : se déplacer sur le résultat suivant d'une recherche.
 */
public enum EnumBouton {
  VALIDER(EnumCategorieBouton.VALIDATION, "Valider", KeyEvent.VK_ENTER),
  EDITER(EnumCategorieBouton.VALIDATION, "Editer", KeyEvent.VK_ENTER),
  CONSULTER(EnumCategorieBouton.VALIDATION, "Consulter", KeyEvent.VK_ENTER),
  CONTINUER(EnumCategorieBouton.VALIDATION, "Continuer", KeyEvent.VK_ENTER),
  CONNECTER(EnumCategorieBouton.VALIDATION, "Connecter", KeyEvent.VK_ENTER),
  LIGNES(EnumCategorieBouton.VALIDATION, "Aller lignes", KeyEvent.VK_ENTER),
  PIED(EnumCategorieBouton.VALIDATION, "Aller pied", KeyEvent.VK_ENTER),
  RECHERCHER(EnumCategorieBouton.VALIDATION, "Rechercher", 'R'),
  ANNULER(EnumCategorieBouton.ANNULATION, "Annuler", KeyEvent.VK_BACK_SPACE),
  FERMER(EnumCategorieBouton.ANNULATION, "Fermer", KeyEvent.VK_BACK_SPACE),
  RETOURNER_RECHERCHE(EnumCategorieBouton.ANNULATION, "Retourner recherche", KeyEvent.VK_BACK_SPACE),
  ALLER_ECRAN_PRECEDENT(EnumCategorieBouton.ANNULATION, "Retourner écran précédent", KeyEvent.VK_BACK_SPACE),
  RETOURNER_ENTETE(EnumCategorieBouton.ANNULATION, "Retourner entête", KeyEvent.VK_BACK_SPACE),
  RETOURNER_LIGNES(EnumCategorieBouton.ANNULATION, "Retourner lignes", KeyEvent.VK_BACK_SPACE),
  QUITTER(EnumCategorieBouton.ANNULATION, "Quitter", 'Q'),
  INITIALISER_RECHERCHE(EnumCategorieBouton.STANDARD, "Initialiser recherche", 'I'),
  NAVIGATION_PRECEDENT(EnumCategorieBouton.IMAGE, "fleche_gauche", KeyEvent.VK_LEFT),
  NAVIGATION_SUIVANT(EnumCategorieBouton.IMAGE, "fleche_droite", KeyEvent.VK_RIGHT);
  
  private final EnumCategorieBouton categorie;
  private final String texte;
  private final Character mnemonique;
  
  /**
   * Constructeur avec un char comme mnémonique.
   */
  EnumBouton(EnumCategorieBouton pCategorie, String pTexte, Character pMnemonique) {
    categorie = pCategorie;
    texte = pTexte;
    mnemonique = pMnemonique;
  }
  
  /**
   * Constructeur avec un code unicode comme mnémonique.
   */
  EnumBouton(EnumCategorieBouton pCategorie, String pTexte, int pMnemonique) {
    categorie = pCategorie;
    texte = pTexte;
    mnemonique = (char) pMnemonique;
  }
  
  /**
   * Catégorie du bouton : standard, validation, annulation, image.
   */
  public EnumCategorieBouton getCategorie() {
    return categorie;
  }
  
  /**
   * Texte associé au bouton.
   * Pour les boutons contenant du texte, c'est le texte du libellé. Pour les boutons de la catégorie image, c'est le nom de l'image.
   */
  public String getTexte() {
    return texte;
  }
  
  /**
   * Mnémonique associé au bouton.
   */
  public Character getMnemonique() {
    return mnemonique;
  }
  
  /**
   * Retourner le texte du bouton.
   */
  @Override
  public String toString() {
    return String.valueOf(texte);
  }
}
