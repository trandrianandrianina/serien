/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton affichant une icône en forme de cloche permettant d'avertir qu'il y a des notifications importantes à consulter.
 *
 * La taille standard est de 26 x 26 pixels afin de correspondre à la hauteur standard des composants qui est de 30 pixels.
 * Avec les effets de dégradés des composants, il faut que l'icône soit plus petite que 30x30 pixels sinon cela donne l'impression
 * qu'elle est plus haute que les composants standards.
 * Cette icone doit ausi pouvoir être insérée dans la barre de sessions.
 */
public class SNBoutonNotificationImportanteALire extends SNBoutonIcone {
  private static final int TAILLE_IMAGE = 26;
  private static final String INFOBULLE = "Afficher les notifications importantes à lire";

  /**
   * Constructeur.
   */
  public SNBoutonNotificationImportanteALire() {
    super(SNCharteGraphique.ICONE_NOTIFICATION_IMPORTANTE_ALIRE, TAILLE_IMAGE, INFOBULLE);
  }
  
}
