/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.barrebouton;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.KeyEvent;
import java.beans.Beans;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BorderFactory;
import javax.swing.InputMap;
import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JViewport;
import javax.swing.SwingUtilities;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.EnumCategorieBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Barre de boutons situées dans les écrans et boîtes de dialogue.
 *
 * Ce composant doit être utilisé pour tout affichage de boutons dans le logiciel. Il gère l'affichage des boutons des catégories
 * standards (orange), validation (vert), annulation (rouge) et des boutons de type image.
 *
 * En mode standard (non condensé), les boutons standards sont justifiés à gauche tandis que les boutons de validation et d'annulation
 * sont justifiés à droite. En mode condensé, tous les boutons sont justifiés à droite, qu'ils soient des catégories standard, validation
 * ou annulation. C'est par exemple utilisé dans les barres de boutons pour les encarts de recherche.
 *
 * La barre de boutons peut gérer l'affichage d'un menu contextuel. Pour cela, il suffit de lier la barre de boutons avec le menu
 * contextuel de la fenêtre via la méthode setMenuContextuel() puis d'ajouter les boutons devant être présents dans le menu contextuel
 * via la méthode ajouterBoutonContextuel(). Le bouton sera ajouté à la barre et dans le menu contextuel. Son libellé, son mnémonique et
 * sa visibilité seront gérés de concert dans la barre de boutons et le menu contextuel. Sous JFormDesigner, le menu contextuel est un
 * composant JPopupMenu ajouté dans la zone libre autour de la fenêtre principale. Il ne doit pas contenir d'entrées puisque celles-ci
 * sont gérées via la barre de boutons. Pour afficher le menu contextuel sur un composant, il faut renseigner la propriété
 * "componentPopupMenu" du composant avec le menu contextuel.
 *
 * Sa hauteur est de 65 pixels, ce qui correspond à la hauteur standard de deux composants (30 pixels) et d'une marge standard (5 pixels).
 *
 * Utilisation :
 * - Ajouter un composant SNBarreBouton à un écran.
 * - Utiliser la méthode setModeCondense(true) pour activer le mode condensé.
 * - Utiliser la méthode setModeNavigation(true) pour activer le mode navigation.
 * - Utiliser snBarreBouton.ajouterBouton(EnumBouton.QUITTER) pour ajouter un bouton pré-configuré à un écran.
 * - Utiliser snBarreBouton.ajouterBouton(BOUTON_AFFICHERDETAIL, 'c') pour ajouter un bouton personnalisé à un écran.
 * - Utiliser la méthode ajouterBoutonListener() pour être notifié lorsqu'un bouton est cliqué.
 */
public class SNBarreBouton extends JPanel {
  static private final int HAUTEUR = 50;
  static private final int MARGE_INTERNE = 5;
  static private final int MARGE_EXTERNE = 10;

  private List<SNBouton> listeBoutonStandard = new ArrayList<SNBouton>();
  private List<SNBouton> listeBoutonValidation = new ArrayList<SNBouton>();
  private List<SNBouton> listeBoutonAnnulation = new ArrayList<SNBouton>();
  private List<SNBouton> listeBoutonImage = new ArrayList<SNBouton>();
  private JPanel pnlGauche = null;
  private JViewport vpGauche = null;
  private JPanel pnlDroite = null;
  private JPanel pnlNavigation = null;
  private SNBouton btnNavigationPrecedent = null;
  private JLabel lbCompteur = null;
  private SNBouton btnNavigationSuivant = null;
  private SNBouton snPageSuivante = null;
  private SNBoutonListener boutonListener = null;
  private boolean modeCondense = false;
  private boolean modeNavigation = false;
  private int compteurMax = 0;
  private int compteurCourant = 0;
  private JPopupMenu popupMenu = null;

  /**
   * Constructeur par défaut.
   */
  public SNBarreBouton() {
    this(false);
  }

  /**
   * Constructeur paramétré.
   * Le panneau principal est utilisé comme premier panneau des écrans et boîtes de dialogue. Il contient l'ensemble des composants
   * à l'exception de la barre de boutons (SNBarreBouton). En tant que composant parent, il est opaque et colorie la zone avec la couleur
   * de fond standard. Il comporte également une marge pour séparer les composants des bords de l'écran ou de laboîte de dialogue.
   */
  public SNBarreBouton(boolean pModeCondense) {
    modeCondense = pModeCondense;

    // Nommer le composant (pour QFTest)
    setName(getClass().getSimpleName());

    // Définir la taille minimum de la barre de boutons
    setMinimumSize(new Dimension(0, HAUTEUR + MARGE_INTERNE + MARGE_EXTERNE));
    setPreferredSize(new Dimension(0, HAUTEUR + MARGE_INTERNE + MARGE_EXTERNE));

    // Définir le layout principal
    setLayout(new GridBagLayout());

    // Apparence du composant dans le designer
    if (Beans.isDesignTime()) {
      setOpaque(true);
      setBackground(SNCharteGraphique.COULEUR_BOUTON_ORANGE);
      add(new JLabel(getClass().getSimpleName()), new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.CENTER,
          GridBagConstraints.BASELINE, new Insets(0, 0 - 0, 0, 0), 0, 0));
      return;
    }

    // Rendre le composant transparent
    setOpaque(false);

    // Afficher un bord en mode design ou en mode debug
    if (Beans.isDesignTime() || Trace.isModeDebug()) {
      setBorder(BorderFactory.createLineBorder(SNCharteGraphique.COULEUR_DEBUG));
    }

    // Configurer le panel supérieur, présent uniquement pour remplir l'espace, les boutons devant toujours être en bas
    JPanel pnlHaut = new JPanel();
    pnlHaut.setName("pnlHaut");
    pnlHaut.setOpaque(false);
    add(pnlHaut,
        new GridBagConstraints(0, 0, 4, 1, 1, 1, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

    // Configurer le panel de gauche
    FlowLayout layoutGauche = new FlowLayout();
    layoutGauche.setAlignment(FlowLayout.LEFT);
    layoutGauche.setHgap(MARGE_INTERNE);
    layoutGauche.setVgap(0);
    pnlGauche = new JPanel(layoutGauche);
    pnlGauche.setName("pnlGauche");
    pnlGauche.setOpaque(false);
    pnlGauche.setMinimumSize(new Dimension(0, HAUTEUR));
    pnlGauche.setPreferredSize(new Dimension(0, HAUTEUR));

    // Configurer le viewport de gauche
    vpGauche = new JViewport();
    vpGauche.setView(pnlGauche);
    vpGauche.setMinimumSize(new Dimension(0, HAUTEUR));
    vpGauche.setPreferredSize(new Dimension(0, HAUTEUR));
    vpGauche.setOpaque(false);
    add(vpGauche, new GridBagConstraints(2, 1, 1, 1, 1, 0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
        new Insets(MARGE_INTERNE, MARGE_EXTERNE - MARGE_INTERNE, MARGE_EXTERNE, 0), 0, 0));

    // Configurer le pannel de droite
    FlowLayout layoutDroite = new FlowLayout();
    layoutDroite.setAlignment(FlowLayout.RIGHT);
    layoutDroite.setHgap(MARGE_INTERNE);
    layoutDroite.setVgap(0);
    pnlDroite = new JPanel(layoutDroite);
    pnlDroite.setName("pnlDroite");
    pnlDroite.setOpaque(false);
    add(pnlDroite, new GridBagConstraints(4, 1, 1, 1, 0, 0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
        new Insets(MARGE_INTERNE, 0, MARGE_EXTERNE, MARGE_EXTERNE - MARGE_INTERNE), 0, 0));

    // Gérer l'affichage du viewport dans le panel de gauche
    rafraichirPageGauche();

    // Etre notifier lorsque le viewport est redimensionné
    vpGauche.addComponentListener(new ComponentListener() {
      @Override
      public void componentResized(ComponentEvent e) {
        rafraichirPageGauche();
      }

      @Override
      public void componentMoved(ComponentEvent e) {
      }

      @Override
      public void componentShown(ComponentEvent e) {
      }

      @Override
      public void componentHidden(ComponentEvent e) {
      }
    });

    // Appeler la méthode valider() lors d'un apppui sur la touche "Entrée"
    InputMap inputMap = getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    ActionMap actionMap = getActionMap();
    inputMap.put(SNCharteGraphique.TOUCHE_ENTREE, "Enter");
    actionMap.put("Enter", new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        valider();
      }
    });
  }

  // -- Méthodes privées

  /**
   * Traiter un appui sur la touche "Entrée".
   *
   * Si le composant avec le focus a un InputVerifier, on l'appelle pour vérifier que la saisie en cours est valide. Si la saisie n'est
   * pas valide, l'action sur la touche entrée ne produit rien. Si la saisie est valide, le focus sur le composant en cours est enlevé
   * pour que les listeners correspondants soient appelés.
   */
  private void valider() {
    try {
      // Ne rien faire s'il n'y a pas de listener
      if (boutonListener == null) {
        return;
      }

      // Rechercher le premier bouton de la catégorie "Validation" qui est actif
      SNBouton snBoutonValide = null;
      for (SNBouton snBouton : listeBoutonValidation) {
        if (snBouton.isActif()) {
          snBoutonValide = snBouton;
          break;
        }
      }

      // Sortir si aucun bouton "Validation" est actif
      if (snBoutonValide == null) {
        return;
      }

      // Si le composant avec le focus a un InputVerifier, on l'appelle pour vérifier que la saisie en cours est valide.
      Component component = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
      if (component != null && component instanceof JComponent) {
        JComponent jcomponent = (JComponent) component;
        InputVerifier inputVerifier = jcomponent.getInputVerifier();
        if (inputVerifier != null) {
          if (!inputVerifier.shouldYieldFocus(jcomponent)) {
            return;
          }
        }
      }

      // Enlever le focus du composant en cours pour que les listeners correspondants soient appelés
      KeyboardFocusManager.getCurrentKeyboardFocusManager().clearGlobalFocusOwner();

      // La méthode quitterAvecValidation est appelée plus tard pour laisser à la thread graphique le temps de traiter la perte de focus.
      final SNBouton snBoutonValide2 = snBoutonValide;
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          try {
            boutonListener.traiterClicBouton(snBoutonValide2);
          }
          catch (Exception exception) {
            DialogueErreur.afficher(exception);
          }
        }
      });
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Rafraîchir les entrées du menu contextuel.
   */
  private void rafraichirMenuContextuel() {
    // Tester si un menu contextuel est défini
    if (popupMenu == null) {
      return;
    }

    // Supprimer tous les points de menu du menu contextuel
    popupMenu.removeAll();

    // Ajouter les boutons pré-configurés s'ils sont contextuels et actifs
    for (SNBouton snBouton : listeBoutonValidation) {
      // Tester si le bouton est contextuel et actif
      if (snBouton.isContextuel() && snBouton.isEnabled()) {
        // La police des menus pour ce type de bouton est mise en gras
        String texte = snBouton.getText();
        texte = texte.replace("<html>", "<html><b>").replace("</html>", "</b></html>");
        // Création du point de menu
        JMenuItem menuItem = new JMenuItem(texte);
        // Le nom du menu est identique au bouton afin de faire le lien pour les actions
        menuItem.setName(snBouton.getName());
        menuItem.setMnemonic(snBouton.getMnemonic());
        menuItem.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            miActionPerformed(e);
          }
        });
        popupMenu.add(menuItem);
      }
    }

    // Ajouter les boutons standards s'ils sont contextuels et actifs
    for (SNBouton snBouton : listeBoutonStandard) {
      // Tester si le bouton est contextuel et actif
      if (snBouton.isContextuel() && snBouton.isEnabled()) {
        JMenuItem menuItem = new JMenuItem(snBouton.getText());
        // Le nom du menu est identique au bouton afin de faire le lien pour les actions
        menuItem.setName(snBouton.getName());
        menuItem.setMnemonic(snBouton.getMnemonic());
        menuItem.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            miActionPerformed(e);
          }
        });
        popupMenu.add(menuItem);
      }
    }
  }

  /**
   * Rafraîchir l'ensemble des boutons de la barre.
   */
  private void rafraichirBouton() {
    if (pnlGauche == null || pnlDroite == null) {
      return;
    }

    // Effacer les panels
    pnlGauche.removeAll();
    pnlDroite.removeAll();

    // Ajouter les boutons standards
    for (SNBouton snBouton : listeBoutonStandard) {
      if (modeCondense) {
        pnlDroite.add(snBouton);
      }
      else {
        pnlGauche.add(snBouton);
      }
    }

    // Ajouter les boutons de la catégorie "Validation"
    for (SNBouton snBouton : listeBoutonValidation) {
      pnlDroite.add(snBouton);
    }

    // Ajouter les boutons de la catégorie "Annulation" en dernier pour qu'ils soient toujours le plus à droite
    for (SNBouton snBouton : listeBoutonAnnulation) {
      pnlDroite.add(snBouton);
    }

    // Rafraichir le Viewport de gauche
    rafraichirPageGauche();

    // Rafraichir le menu contextuel
    rafraichirMenuContextuel();

    // Forcer la rafraîchissement du composant
    // Utile si la barre de boutons est modifiées dynamiquement dans la méthode rafraichir() d'une vue.
    revalidate();
    repaint();
  }

  /**
   * Calculer la largeur du panel de gauche en fonction des boutons visibles.
   */
  private int calculerLargeurPanelGauche() {
    if (modeCondense) {
      return 0;
    }
    int largeur = MARGE_INTERNE;
    for (SNBouton snBouton : listeBoutonStandard) {
      if (snBouton.isVisible()) {
        largeur += snBouton.getPreferredSize().getWidth() + MARGE_INTERNE;
      }
    }
    return largeur;
  }

  /**
   * Calculer la longueur du déplacement lorsqu'on clique sur les boutons précédent ou suivant.
   * L'objectif est d'avancer d'un nombre entier de boutons afin que le bouton tronqué en fin de ligne soit affiché en entier
   * dans la page suivante.
   */
  private int calculerDeplacement() {
    int nombreBouton = vpGauche.getWidth() / (SNCharteGraphique.TAILLE_BOUTON_STANDARD.width + MARGE_INTERNE);
    return nombreBouton * (SNCharteGraphique.TAILLE_BOUTON_STANDARD.width + MARGE_INTERNE);
  }

  /**
   * Recalculer le positionnement du panel de gauche dans le viewport.
   */
  private void rafraichirPageGauche() {
    rafraichirPageGauche(vpGauche.getViewPosition());
  }

  /**
   * Calculer le positionnement du panel de gauche dans le viewport.
   * En fonction de leur taille respective et de la positon, les boutons "Page précédente" et "Page suivante" sont affichés ou non.
   */
  private void rafraichirPageGauche(Point pPoint) {
    if (vpGauche == null || pnlGauche == null) {
      return;
    }

    // Redéfinir la dimension du panel gauche en focntion de son contenu.
    // Etape importante car ses dimensions ne sont pas dictées par le parent (ViewPort).
    Dimension dimensionPanelGauche = new Dimension(calculerLargeurPanelGauche(), pnlGauche.getHeight());
    pnlGauche.setMinimumSize(dimensionPanelGauche);
    pnlGauche.setPreferredSize(dimensionPanelGauche);
    pnlGauche.setSize(dimensionPanelGauche);

    // Masquer les boutons pages précédentes et suivantes avant de vérifier les dimensions
    // Le fait de masquer les boutons peut libérer de la place pour pourvoir afficher tous les boutons
    if (snPageSuivante != null) {
      snPageSuivante.setVisible(false);
    }

    // Se positionner à zéro si on est trop loin vers la droite
    if (pPoint.x <= 0) {
      pPoint.x = 0;
    }
    // Se positionner à zéro si la barre de boutons à afficher est plus petite que l'espace disponible.
    else if (pnlGauche.getWidth() < vpGauche.getWidth()) {
      pPoint.x = 0;
    }
    // Se positionner de façon à ce que l'espace disponible soit rempli de boutons.
    else if (pPoint.x > pnlGauche.getWidth() - vpGauche.getWidth()) {
      pPoint.x = pnlGauche.getWidth() - vpGauche.getWidth();
    }

    // Décaler la barre de boutons dans le viewport
    vpGauche.setViewPosition(pPoint);

    // Afficher les boutons si on est pas à fond à gauche et à fond à droite
    if (pPoint.x > 0 || pPoint.x < pnlGauche.getWidth() - vpGauche.getWidth()) {
      // Créer le bouton suivant la première fois
      if (snPageSuivante == null) {
        snPageSuivante = new SNBouton(EnumCategorieBouton.IMAGE, "bouton_suivant", KeyEvent.VK_LESS, false);
        add(snPageSuivante, new GridBagConstraints(3, 1, 1, 1, 0, 0, GridBagConstraints.SOUTH, GridBagConstraints.NONE,
            new Insets(MARGE_EXTERNE, MARGE_INTERNE, MARGE_EXTERNE, MARGE_EXTERNE), 0, 0));
        snPageSuivante.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            afficherPageSuivante();
          }
        });
      }

      // Afficher les boutons
      snPageSuivante.setVisible(true);
    }
  }

  /**
   * Afficher la page suivante de la barre de boutons.
   */
  private void afficherPageSuivante() {
    Point point = vpGauche.getViewPosition();
    if (point.x >= pnlGauche.getWidth() - vpGauche.getWidth()) {
      point.x = 0;
    }
    else {
      point.x += calculerDeplacement();
    }
    rafraichirPageGauche(point);
  }

  /**
   * Rafraîchir les données de navigation.
   */
  private void rafraichirNavigation() {
    // Rafraîchir le texte
    if (compteurCourant <= 0 || compteurMax <= 0) {
      lbCompteur.setText("");
    }
    else {
      lbCompteur.setText("" + compteurCourant + " / " + compteurMax);
    }

    // Activer ou désactiver le bouton précédent
    if (compteurCourant > 1) {
      btnNavigationPrecedent.setEnabled(true);
    }
    else {
      btnNavigationPrecedent.setEnabled(false);
    }

    // Activer ou désactiver le bouton suivant
    if (compteurCourant < compteurMax) {
      btnNavigationSuivant.setEnabled(true);
    }
    else {
      btnNavigationSuivant.setEnabled(false);
    }
  }

  /**
   * Traiter les clics sur les boutons situés dans la barre de boutons.
   */
  private void btActionPerformed(ActionEvent e) {
    try {
      // Tester s'il y a un listener
      if (boutonListener == null) {
        throw new MessageErreurException("Aucune opération n'a été associée aux boutons de cette barre de boutons.");
      }

      // Récupérer le bouton à l'origine de l'évènement
      SNBouton bouton = (SNBouton) e.getSource();
      boutonListener.traiterClicBouton(bouton);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Traiter les clics sur les entrées du menu contextuel.
   */
  private void miActionPerformed(ActionEvent e) {
    try {
      // Tester s'il y a un listener
      if (boutonListener == null) {
        throw new MessageErreurException("Aucune opération n'a été associée aux boutons de cette barre de boutons.");
      }

      // Rechercher le bouton correspondant au menu contextuel via les libellés
      JMenuItem menuItem = (JMenuItem) e.getSource();
      for (SNBouton snBouton : listeBoutonValidation) {
        if (snBouton.isContextuel() && Constantes.equals(snBouton.getName(), menuItem.getName())) {
          boutonListener.traiterClicBouton(snBouton);
          return;
        }
      }
      for (SNBouton snBouton : listeBoutonStandard) {
        if (snBouton.isContextuel() && Constantes.equals(snBouton.getName(), menuItem.getName())) {
          boutonListener.traiterClicBouton(snBouton);
          return;
        }
      }

      // Afficher un message d'erreur si le menu contextuel ne correspond à aucun bouton
      throw new MessageErreurException("Aucun bouton n'est associé au menu contextuel : " + menuItem.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }

  // -- Méthodes publiques

  /**
   * Ajouter un listener lorsque l'un des boutons de la barre est cliqué.
   */
  public void ajouterBoutonListener(SNBoutonListener pBoutonListener) {
    boutonListener = pBoutonListener;
  }

  /**
   * Ajouter un bouton à la barre de boutons.
   * Le paramètre visibilité permet d'indiquer si le bouton doit être visible ou non lors de l'affichage initial de la barre d'outils.
   */
  public void ajouterBouton(SNBouton pSNBouton, boolean pVisible) {
    if (pSNBouton == null) {
      throw new MessageErreurException("Le bouton à ajouter à la barre de boutons est invalide.");
    }
    SNBouton snBoutonExistant = getBouton(pSNBouton.getName());
    if (snBoutonExistant != null) {
      throw new MessageErreurException("Impossible d'ajouter deux boutons avec le même nom : " + pSNBouton.getName());
    }

    // Ajouter le bouton à la liste
    switch (pSNBouton.getCategorie()) {
      case STANDARD:
        listeBoutonStandard.add(pSNBouton);
        pSNBouton.activer(pVisible, false);
        break;

      case VALIDATION:
        listeBoutonValidation.add(pSNBouton);
        pSNBouton.activer(pVisible, listeBoutonValidation.size() > 1);

        // Modifier les boutons validation existant
        if (listeBoutonValidation.size() > 1) {
          for (SNBouton snBouton : listeBoutonValidation) {
            snBouton.activer(snBouton.isActif(), true);
          }
        }
        break;

      case ANNULATION:
        listeBoutonAnnulation.add(pSNBouton);
        pSNBouton.activer(pVisible, false);
        break;

      case IMAGE:
        listeBoutonImage.add(pSNBouton);
        pSNBouton.activer(pVisible, false);
        break;

      default:
        throw new MessageErreurException("La catégorie de boutons est inconnue.");
    }

    // Ajouter un listener au bouton
    pSNBouton.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btActionPerformed(e);
      }
    });

    // Rafraîchir la barre de boutons
    rafraichirBouton();
  }

  /**
   * Associer la barre de boutons à un menu contextuel.
   */
  public void setMenuContextuel(JPopupMenu pPopupMenu) {
    // Sortir si la valeur n'a pas changée
    if (Constantes.equals(popupMenu, pPopupMenu)) {
      return;
    }

    // Mémoriser le nouveau menu contextuel
    popupMenu = pPopupMenu;

    // Rafraîchir les entrées du menu contextuel
    if (popupMenu != null) {
      rafraichirMenuContextuel();
    }
  }

  /**
   * Ajouter un bouton contextuel à la barre de boutons.
   * Un bouton contextuel est comme un bouton stantard mais il sera également affiché dans le menu contextuel s'il est actif.
   */
  public void ajouterBoutonContextuel(String pLibelle, Character pMnemonique, boolean pVisible) {
    if (pLibelle == null) {
      throw new MessageErreurException("Le libellé du bouton est invalide.");
    }
    if (pMnemonique == null) {
      throw new MessageErreurException("Le bouton \"" + pLibelle + "\" ne comporte pas de mnémonique.");
    }
    ajouterBouton(new SNBouton(pLibelle, pMnemonique, true), pVisible);
  }

  /**
   * Ajouter un bouton contextuel à la barre de boutons.
   * Un bouton contextuel est comme un bouton pré-configuré mais il sera également affiché dans le menu contextuel s'il est actif.
   */
  public void ajouterBoutonContextuel(EnumBouton pTypeBouton, boolean pVisible) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    ajouterBouton(new SNBouton(pTypeBouton, true), pVisible);
  }

  /**
   * Ajouter un bouton standard à la barre de boutons.
   * Un bouton standard est de couleur orange. On doit définir un libellé et un mnémonique. Le paramètre visibilité permet d'indiquer
   * si le bouton doit être visible ou non lors de l'affichage initial de la barre d'outils.
   */
  public void ajouterBouton(String pLibelle, Character pMnemonique, boolean pVisible) {
    if (pLibelle == null) {
      throw new MessageErreurException("Le libellé du bouton est invalide.");
    }
    if (pMnemonique == null) {
      throw new MessageErreurException("Le bouton \"" + pLibelle + "\" ne comporte pas de mnémonique.");
    }
    ajouterBouton(new SNBouton(pLibelle, pMnemonique, false), pVisible);
  }

  /**
   * Ajouter un bouton pré-configuré à la barre de boutons.
   * Le libellé, la couleur et le mnémonique dont définis de façon standard. Le paramètre visibilité permet d'indiquer si le bouton
   * doit être visible ou non lors de l'affichage initial de la barre d'outils.
   */
  public void ajouterBouton(EnumBouton pTypeBouton, boolean pVisible) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    ajouterBouton(new SNBouton(pTypeBouton), pVisible);
  }

  /**
   * Supprimer un bouton de la barre de boutons.
   */
  public void supprimerBouton(SNBouton pSNBouton) {
    // Vérifier les paramètres
    if (pSNBouton == null) {
      throw new MessageErreurException("Le bouton à supprimer de la barre de boutons est invalide.");
    }

    // Supprimer le bouton de la liste correspondante
    switch (pSNBouton.getCategorie()) {
      case STANDARD:
        listeBoutonStandard.remove(pSNBouton);
        break;

      case VALIDATION:
        listeBoutonValidation.remove(pSNBouton);
        break;

      case ANNULATION:
        listeBoutonAnnulation.remove(pSNBouton);
        break;

      case IMAGE:
        listeBoutonImage.remove(pSNBouton);
        break;

      default:
        throw new MessageErreurException("La catégorie de boutons est inconnue.");
    }

    // Rafraîchir la barre de boutons
    rafraichirBouton();
  }

  /**
   * Supprimer tous les boutons.
   */
  public void supprimerTout() {
    listeBoutonStandard.clear();
    listeBoutonValidation.clear();
    listeBoutonAnnulation.clear();
    rafraichirBouton();
  }

  /**
   * Récupérer un bouton par son nom.
   */
  public SNBouton getBouton(String pNom) {
    for (SNBouton snBouton : listeBoutonValidation) {
      if (snBouton.getName().equals(pNom)) {
        return snBouton;
      }
    }
    for (SNBouton snBouton : listeBoutonAnnulation) {
      if (snBouton.getName().equals(pNom)) {
        return snBouton;
      }
    }
    for (SNBouton snBouton : listeBoutonStandard) {
      if (snBouton.getName().equals(pNom)) {
        return snBouton;
      }
    }
    for (SNBouton snBouton : listeBoutonImage) {
      if (snBouton.getName().equals(pNom)) {
        return snBouton;
      }
    }
    return null;
  }

  /**
   * Récupérer un bouton par son type.
   */
  public SNBouton getBouton(EnumBouton pTypeBouton) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    return getBouton(pTypeBouton.getTexte());
  }

  /**
   * Tester si un bouton est actif ou non.
   */
  public boolean isBoutonActif(String pNom) {
    SNBouton snBouton = getBouton(pNom);
    if (snBouton == null) {
      throw new MessageErreurException("Aucune bouton ne porte le nom suivant : " + pNom);
    }
    return snBouton.isActif();
  }

  /**
   * Tester si un bouton pré-configuré est actif ou non.
   */
  public boolean isBoutonActif(EnumBouton pTypeBouton) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    return isBoutonActif(pTypeBouton.getTexte());
  }

  /**
   * Activer un bouton.
   * Les boutons des catégories standards et annulation sont masqués. Les boutons de la catégorie image sont grisés.
   * Les boutons de la catégorie validation sont grisés s'il n'y a qu'un seul bouton de cette catégorie et sont masqués s'il y a plusieurs
   * boutons de cette catégorie.
   */
  public void activerBouton(String pNom, boolean pVisible) {
    SNBouton snBouton = getBouton(pNom);
    if (snBouton == null) {
      throw new MessageErreurException("Aucun bouton ne porte le nom suivant : " + pNom);
    }

    snBouton.activer(pVisible, listeBoutonValidation.size() > 1);
    rafraichirPageGauche();
    rafraichirMenuContextuel();
  }

  /**
   * Afficher ou cacher un bouton pré-configuré.
   * Les boutons des catégories standards et annulation sont masqués. Les boutons des catégories validation et image sont juste grisés.
   */
  public void activerBouton(EnumBouton pTypeBouton, boolean pVisible) {
    if (pTypeBouton == null) {
      throw new MessageErreurException("Le type de bouton est invalide.");
    }
    activerBouton(pTypeBouton.getTexte(), pVisible);
  }

  /**
   * Mode condensé.
   */
  public boolean isModeCondense() {
    return modeCondense;
  }

  /**
   * Mode navigation.
   */
  public boolean isModeNavigation() {
    return modeNavigation;
  }

  /**
   * Activer ou désactiver le mode navigation.
   * Le mode navigation fait apparaître des boutons permettant de parcourir le résultat d'une recherche. Les éléments suivants
   * apparaîssent à gauche de la berre d'outils : un bouton précédent pour afficher le résultat précédent, un compteur pour connaître
   * l'index courant et le nombre de résultats de la recherche, un bouton suivant pour afficher le résultat suivant.
   */
  public void setModeNavigation(boolean pModeNavigation) {
    if (modeNavigation == pModeNavigation) {
      return;
    }
    modeNavigation = pModeNavigation;

    // Créer les composants la première fois
    if (pnlNavigation == null) {
      // Initialiser le panel compteur
      // On souhaite qu'il ait la même dimension qu'un bouton.
      pnlNavigation = new JPanel(new GridBagLayout());
      pnlNavigation.setMinimumSize(SNCharteGraphique.TAILLE_BOUTON_STANDARD);
      pnlNavigation.setPreferredSize(SNCharteGraphique.TAILLE_BOUTON_STANDARD);
      pnlNavigation.setOpaque(false);
      add(pnlNavigation, new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.SOUTH, GridBagConstraints.NONE,
          new Insets(MARGE_EXTERNE, MARGE_EXTERNE, MARGE_EXTERNE, 0), 0, 0));

      // Ajouter le bouton navigation précédente
      btnNavigationPrecedent = new SNBouton(EnumBouton.NAVIGATION_PRECEDENT);
      ajouterBouton(btnNavigationPrecedent, true);
      pnlNavigation.add(btnNavigationPrecedent,
          new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

      // Ajouter le libellé avec le compteur
      lbCompteur = new JLabel("");
      lbCompteur.setFont(SNCharteGraphique.POLICE_TITRE);
      // lbCompteur.setMaximumSize(new Dimension(100, HAUTEUR));
      pnlNavigation.add(lbCompteur,
          new GridBagConstraints(1, 0, 1, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));

      // Initialiser le bouton suivant
      btnNavigationSuivant = new SNBouton(EnumBouton.NAVIGATION_SUIVANT);
      ajouterBouton(btnNavigationSuivant, true);
      pnlNavigation.add(btnNavigationSuivant,
          new GridBagConstraints(2, 0, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
    }

    pnlNavigation.setVisible(modeNavigation);
  }

  /**
   * Compteur maximum de la navigation.
   */
  public int getCompteurMax() {
    return compteurMax;
  }

  /**
   * Modifier le compteur maximum de la navigation
   */
  public void setCompteurMax(int pCompteurMax) {
    if (compteurMax == pCompteurMax) {
      return;
    }
    compteurMax = pCompteurMax;
    rafraichirNavigation();
  }

  /**
   * Compteur courant de la navigation.
   */
  public int getCompteurCourant() {
    return compteurCourant;
  }

  /**
   * Modifier le compteur courant de la navigation
   */
  public void setCompteurCourant(int pCompteurCourant) {
    if (compteurCourant == pCompteurCourant) {
      return;
    }
    compteurCourant = pCompteurCourant;
    rafraichirNavigation();
  }
}
