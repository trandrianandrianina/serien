/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton permettant de désélectionner toutes les entrées dans une liste à deux colonnes.
 */
public class SNBoutonDeselectionTotale extends SNBoutonIcone {
  private static final int TAILLE_IMAGE = 50;
  private static final String INFOBULLE = "Tout déselectionner";
  
  /**
   * Constructeur.
   */
  public SNBoutonDeselectionTotale() {
    super(SNCharteGraphique.ICONE_DESELECTION_TOTALE, TAILLE_IMAGE, INFOBULLE);
  }
}
