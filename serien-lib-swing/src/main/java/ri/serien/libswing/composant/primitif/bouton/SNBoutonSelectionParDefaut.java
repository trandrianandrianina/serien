/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.bouton;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton permettant de désélectionner toutes les entrées dans une liste à deux colonnes.
 */
public class SNBoutonSelectionParDefaut extends SNBoutonIcone {
  private static final int TAILLE_IMAGE = 50;
  private static final String INFOBULLE = "Remettre la sélection par défaut";
  
  /**
   * Constructeur.
   */
  public SNBoutonSelectionParDefaut() {
    super(SNCharteGraphique.ICONE_SELECTION_PAR_DEFAUT, TAILLE_IMAGE, INFOBULLE);
  }
}
