/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.composant.primitif.radiobouton;

import javax.swing.JRadioButton;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bouton radio avec la présentation standard.
 *
 * Ce composant remplace JRadioButton. Il est à utiliser pour tous les boutons radios des écrans.
 *
 * Caractéristiques graphiques :
 * - Fond transparent.
 * - Hauteur standard.
 * - Police standard.
 */
public class SNRadioButton extends JRadioButton {
  /**
   * Constructeur.
   */
  public SNRadioButton() {
    super();
    
    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    
    // Rendre le composant transparent
    setOpaque(false);

    // Fixer la police standard
    setFont(SNCharteGraphique.POLICE_STANDARD);
  }
}
