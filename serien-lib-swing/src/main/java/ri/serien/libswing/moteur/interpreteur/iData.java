/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.interpreteur;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;

/**
 * Interprétation des variables (à la JWalk)
 * ==> A faire:
 * ==> Voir comment faire quand les données sont modifiées sur un même format (listePanelData)
 * ==> Créer la classe DataPanel gérant les variables des panels ou screen voire des variables des scripts
 * ==> Voir si l'on peut optimiser
 */
public class iData {
  // Variables
  private char expression[] = null;
  private char tabResultat[] = null;
  private char tabNomVariable[] = null;
  private char tabParam[] = null;
  private char tabW[] = null;
  private HashMap<String, oRecord> listeRecord = null;

  private String msgErreur = null;

  /**
   * Constructeur de la classe
   */
  public iData() {
    tabResultat = new char[Constantes.TAILLE_BUFFER];
    tabNomVariable = new char[Constantes.TAILLE_BUFFER];
    tabParam = new char[Constantes.TAILLE_BUFFER];
    tabW = new char[Constantes.TAILLE_BUFFER];
  }

  /**
   * Initialise la liste des flux en cours
   * @param alisteFlux
   */
  public void setFlux(HashMap<String, oRecord> alisteRecord) {
    listeRecord = alisteRecord;
  }

  /**
   * Analyse de l'expression
   * @param valeur
   * @return
   */
  public int analyseExpressionBrut(String valeur) {
    String expressionOri = null;
    String nomVariable = null;
    String valeurVariable = null;
    int surVar = Constantes.FALSE;
    int surParam = Constantes.FALSE;
    int i = 0;
    int j = 0;
    int k = 0;

    if (valeur == null) {
      return -2;
    }

    // On convertie les caractères unicodes et
    // Est ce bien une variable panel dans le cas contraire on renvoi la valeur telle quelle
    // expressionOri = Constantes.code2unicode(valeur.trim());
    expressionOri = valeur; // valeur.trim();
    if (expressionOri.indexOf('@') == -1) {
      return -1;// expressionOri;
    }

    // On vérifie que l'expression n'a pas déjà été traité
    // Source de bug quand on reste sur le meme format mais qu'il y a des maj de donnée
    // style format en erreur le V03F reste à blanc car dejà traité
    // valeurVariable = ((String)listeDataPanel.get(expressionOri));
    // if (valeurVariable != null) return valeurVariable;

    // Sinon
    expression = new char[expressionOri.length()];
    expressionOri.getChars(0, expressionOri.length(), expression, 0);

    for (i = 0; i < expression.length; i++) {
      // On teste le début ou la fin d'une variable
      if (expression[i] == '@') {
        if (surVar == Constantes.FALSE) {
          surVar = Constantes.TRUE;
          continue;
        }
        else {
          if (surParam == Constantes.TRUE) {
            surParam = Constantes.FALSE;
            valeur = analyseVariable(tabParam, j - 1, valeurVariable);
            // valeur=" + valeur );
            if (valeur != null) {
              // valeur = valeur.trim();
              valeur.getChars(0, valeur.length(), tabResultat, k);
              k = k + valeur.length();
            }
          }
          else {
            nomVariable = new String(tabNomVariable, 0, j);
            valeurVariable = getValeur(nomVariable);
            if (valeurVariable != null) {
              // valeurVariable = valeurVariable.trim();
              valeurVariable.getChars(0, valeurVariable.length(), tabResultat, k);
              k = k + valeurVariable.length();
            }
          }
          j = 0;
          surVar = Constantes.FALSE;
          continue;
        }
      }
      // On teste la fin d'une variable
      else if ((expression[i] == '/') && (surParam == Constantes.FALSE)) {
        if (surVar == Constantes.TRUE) {
          surParam = Constantes.TRUE;
          nomVariable = new String(tabNomVariable, 0, j);
          valeurVariable = getValeur(nomVariable);
          j = 0;
          continue;
        }
      }

      if (surVar == Constantes.TRUE) {
        if (surParam == Constantes.FALSE) {
          tabNomVariable[j++] = expression[i];
        }
        else {
          tabParam[j++] = expression[i];
        }
      }
      else {
        tabResultat[k++] = expression[i];
      }
    }

    // valeurVariable = new String(tabResultat, 0, k);

    return k;
  }

  /**
   * Analyse de l'expression sans trim
   * @param valeur
   * @return
   */
  public String analyseExpressionWOTrim(String valeur) {
    int lg = analyseExpressionBrut(valeur);
    // On ne touche à rien car pas de @ détecté
    if (lg == -1) {
      return valeur;
    }
    else if (lg == -2) {
      return null;
    }
    else {
      return new String(tabResultat, 0, lg);
    }
  }

  /**
   * Analyse de l'expression avec trim à droite
   * @param valeur
   * @return
   */
  public String analyseExpressionTrimR(String valeur) {
    int lg = analyseExpressionBrut(valeur);
    // On ne touche à rien car pas de @ détecté
    if (lg == -1) {
      return valeur;
    }
    else if (lg == -2) {
      return null;
    }
    else {
      int i = 0;
      for (i = tabResultat.length - 1; i > 0; i--) {
        if ((tabResultat[i] != ' ') || (tabResultat[i] != '\t')) {
          break;
        }
      }
      return new String(tabResultat, 0, i);
    }
  }

  /**
   * Analyse de l'expression avec trim à droite et à gauche
   * @param valeur
   * @return
   */
  public String analyseExpression(String valeur) {
    int lg = analyseExpressionBrut(valeur);
    if (lg == -1) {
      return valeur.trim();
    }
    else if (lg == -2) {
      return null;
    }
    else {
      return new String(tabResultat, 0, lg).trim();
    }
  }

  /**
   * Analyse de l'expression (sans Trim sur variable retour) // TODO pas bo à corriger
   * @param valeur
   * @return
   *
   *         public String analyseExpression(String valeur)
   *         {
   *         String expressionOri=null;
   *         String nomVariable=null;
   *         String valeurVariable=null;
   *         int surVar=Constantes.FALSE, surParam=Constantes.FALSE;
   *         int i=0, j=0, k=0;
   *         
   *         
   *         if (valeur != null)
   *         {
   *         // On convertie les caractères unicodes et
   *         // Est ce bien une variable panel dans le cas contraire on renvoi la valeur telle quelle
   *         //expressionOri = Constantes.code2unicode(valeur.trim());
   *         expressionOri = valeur; //valeur.trim();
   *         if (expressionOri.indexOf('@') == -1) return expressionOri;
   *         
   *         // On vérifie que l'expression n'est pas déjà été traité
   *         // Source de bug quand on reste sur le meme format mais qu'il y a des maj de donnée
   *         // style format en erreur le V03F reste à blanc car dejà traité
   *         //valeurVariable = ((String)listeDataPanel.get(expressionOri));
   *         //if (valeurVariable != null) return valeurVariable;
   *         
   *         // Sinon
   *         expression = new char[expressionOri.length()];
   *         expressionOri.getChars(0, expressionOri.length(), expression, 0);
   *         
   *         for (i=0; i<expression.length; i++)
   *         {
   *         // On teste le début ou la fin d'une variable
   *         if (expression[i] == '@')
   *         if (surVar == Constantes.FALSE)
   *         {
   *         surVar = Constantes.TRUE;
   *         continue;
   *         }
   *         else
   *         {
   *         if (surParam == Constantes.TRUE)
   *         {
   *         surParam = Constantes.FALSE;
   *         valeur = analyseVariable(tabParam, j-1, valeurVariable);
   *         valeur=" + valeur );
   *         if (valeur != null)
   *         {
   *         valeur = valeur.trim();
   *         valeur.getChars(0, valeur.length(), tabResultat, k);
   *         k = k + valeur.length();
   *         }
   *         }
   *         else
   *         {
   *         nomVariable = new String(tabNomVariable, 0, j);
   *         valeurVariable = getValeur(nomVariable);
   *         if (valeurVariable != null)
   *         {
   *         valeurVariable = valeurVariable.trim();
   *         valeurVariable.getChars(0, valeurVariable.length(), tabResultat, k);
   *         k = k + valeurVariable.length();
   *         }
   *         }
   *         j = 0;
   *         surVar = Constantes.FALSE;
   *         continue;
   *         }
   *         else
   *         // On teste la fin d'une variable
   *         if ( (expression[i] == '/') && (surParam == Constantes.FALSE) )
   *         {
   *         if (surVar == Constantes.TRUE)
   *         {
   *         surParam = Constantes.TRUE;
   *         nomVariable = new String(tabNomVariable, 0, j);
   *         valeurVariable = getValeur(nomVariable);
   *         j = 0;
   *         continue;
   *         }
   *         }
   *
   *         if (surVar == Constantes.TRUE)
   *         if (surParam == Constantes.FALSE) tabNomVariable[j++] = expression[i];
   *         else tabParam[j++] = expression[i];
   *         else
   *         tabResultat[k++] = expression[i];
   *         }
   *
   *         // On met la valeur dans la HashMap
   *         valeurVariable = new String(tabResultat, 0, k);//.trim();
   *         // listeDataPanel.put(expressionOri, valeurVariable);
   *         }
   *
   *         if (valeurVariable == null) return null;
   *         else return valeurVariable;
   *         }
   */

  /**
   * Analyse de la variable
   * @param tabparam
   * @param indice
   * @param valeur
   * @return
   */
  public String analyseVariable(char[] tabparam, int indice, String valeur) {
    int i = 0;
    int j = 0;
    int occ = 0;
    int occabs;
    int debpos = 0;
    int finpos = 0;
    int pos = 0;
    int trouve = Constantes.FALSE;
    char element;
    boolean trim = false;
    String chaine = null;

    if (valeur == null) {
      return null;
    }
    // On récupére le caractère séparateur
    element = tabparam[indice];
    tabparam[indice] = ' ';
    trim = element == '\u00a8'; // Comme ^ mais en plus on trime la valeur trouvé
    if (trim) {
      element = '^';
    }
    indice--;

    // On teste la valeur d'élément car cela détermine la suite des opé
    // Variable de type @LD/0,30^@
    if (element == '^') {
      chaine = new String(tabparam, 0, indice + 1).trim();
      occ = chaine.indexOf(',');
      debpos = Integer.parseInt(chaine.substring(0, occ));
      // System.out.print("--> " + chaine + "|"+ occ + "|" + chaine.substring(0, occ) + "|" +
      // chaine.substring(occ+1)+"|");
      finpos = debpos + Integer.parseInt(chaine.substring(occ + 1));
      // System.out.print("--> " + debpos + " " + finpos);
    }
    // Variable de type @V07F/+1=@
    else {
      if (tabparam[0] == '+') {
        debpos = 1;
      }
      else {
        indice++;
      }

      // On récupère l'occurence du séparateur
      occ = Integer.parseInt(new String(tabparam, debpos, indice));
      occabs = Math.abs(occ);

      // On cherche dans la chaine
      valeur.getChars(0, valeur.length(), tabW, 0);
      j = 0;
      debpos = 0;
      finpos = 0;
      // On recherche le marqueur
      for (i = 0; (i < valeur.length()) && (trouve == Constantes.FALSE); i++) {
        if (tabW[i] == element) {
          j++;
        }
        if (j == occabs) {
          if (occ > 0) {
            debpos = i + 1;
            finpos = valeur.length();
          }
          else {
            debpos = 0;
            finpos = i;
          }
          trouve = Constantes.TRUE;
        }
      }

      // On recupère la partie qui nous interresse
      if (occ > 0) {
        pos = finpos;
        // à droite du symbole
        while (i < pos) {
          if (tabW[i] == ' ') {
            finpos = i;
          }
          else if (i == (pos - 1)) {
            finpos = i + 1;
          }
          if (tabW[i] == element) {
            break;
          }
          /* Spécifique pour les F1=... F2=...
          if (tabW[i] == 'F')
            if ( ((i+1) < finpos) && ( (tabW[i+1] == '0') || (tabW[i+1] == '1')
                || (tabW[i+1] == '2') || (tabW[i+1] == '3')
                || (tabW[i+1] == '4') || (tabW[i+1] == '5')
                || (tabW[i+1] == '6') || (tabW[i+1] == '7')
                || (tabW[i+1] == '8') || (tabW[i+1] == '9'))) finpos = i;
          if (tabW[i] == element) break;
          */
          i++;
        }
      }
      else {
        // à gauche du symbole
        while (i > 0) {
          if (tabW[i] == ' ') {
            debpos = i;
            break;
          }
          i--;
        }
      }
    }

    if ((valeur.length() < debpos) || (finpos == 0)) {
      return "";
    }
    else if (valeur.length() < finpos) {
      if (trim) {
        return valeur.substring(debpos).trim();
      }
      else {
        return valeur.substring(debpos);
      }
    }
    else if (trim) {
      return valeur.substring(debpos, finpos).trim();
    }
    else {
      return valeur.substring(debpos, finpos);
    }
  }

  /**
   * Retourne la valeur de la variable
   * @param nomVar
   * @return
   */
  private String getValeur(String nomVar) {
    if (nomVar == null) {
      return null;
    }

    // Cas particuliers: variable comme *DATE
    if (nomVar.equals("*DATE")) {
      return new SimpleDateFormat("dd/MM/yyyy").format(new Date());
    }

    // Sinon
    if (listeRecord == null) {
      return null;
    }

    // On recherche dans les variables venant du 5250
    for (Map.Entry<String, oRecord> entry : listeRecord.entrySet()) {
      if (entry.getValue().listeElement.containsKey(nomVar)) {
        // ((oData)entry.getValue().listeElement.get(nomVar)).getValeurToFrame()+"|");
        return ((oData) entry.getValue().listeElement.get(nomVar)).getValeurToFrame();
      }
    }

    return null;
  }

  /**
   * Retourne le message d'erreur
   */
  public String getMsgErreur() {
    String chaine;

    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";

    return chaine;
  }

  /**
   * Nettoyage des variables
   */
  public void dispose() {
    if (listeRecord != null) {
      listeRecord.clear();
    }
    listeRecord = null;
  }
}
