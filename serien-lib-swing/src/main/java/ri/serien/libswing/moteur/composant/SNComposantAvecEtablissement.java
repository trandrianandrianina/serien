/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.composant;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;

/**
 * SNComposant est la classe mère de tous les composants métiers (composant de sélection, de saisie ou d'affichage). Chaque composant
 * métier doit hériter de la classe SNComposant ou d'une de ses classes dérivées.
 *
 * Voici la description des classes SNComposant disponibles :
 *
 * - SNComposant : adapté pour les composants de saisie qui n'ont pas à interagir avec la base de données. Par exemple, le composant de
 * saisie d'une plage de dates.
 *
 * - SNComposantAvecSession : adapté pour les composants qui nécessitent un accès à la base de données mais qui n'ont pas besoin de
 * connaître l'établissement en cours. Par exemple, le composant de sélection d'un contact.
 *
 * - SNComposantAvecEtablissement : adapté pour les composants qui nécessitent un accès à la base de données et qui ont besoin de
 * connaître
 * l'établissement courant. Par exemple, le composant de sélection d'un client.
 *
 *
 * L'apport principal de la classe SNComposant est de mettre à disposition un système de notifications via l'interface
 * InterfaceSNComposantListener. La classe JPanel ne propose pas de notifications pertinentes pour informer la vue qu'une saisie a été
 * effectuée dans le composant. La méthode fireValueChanged() permet de prévenir les abonnés que le composant a changé de valeur.
 *
 */

abstract public class SNComposantAvecEtablissement extends SNComposantAvecSession {
  private IdEtablissement idEtablissement = null;
  private boolean forcerRechargement = false;
  
  /**
   * Constructeur par défaut.
   */
  public SNComposantAvecEtablissement() {
    setName("SNPanelComposantAvecEtablissement");
  }
  
  /**
   * Retourner l'identifiant de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Mettre à jour l'identifiant de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    if (!Constantes.equals(pIdEtablissement, idEtablissement)) {
      idEtablissement = pIdEtablissement;
      forcerRechargement = true;
    }
  }
  
  /**
   * Indique s'il faut effectuer un rechargement des données lors du prochain appel à la méthode charger().
   */
  public boolean isForcerRechargement() {
    return forcerRechargement;
  }
  
  /**
   * Demander le rechargement des données lors du prochain appel à la méthode charger().
   */
  public void setForcerRechargement(boolean pForcerRechargement) {
    forcerRechargement = pForcerRechargement;
  }
}
