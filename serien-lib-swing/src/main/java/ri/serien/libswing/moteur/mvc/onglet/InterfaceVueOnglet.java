/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc.onglet;

public interface InterfaceVueOnglet {
  /**
   * Initialiser les composants graphiques de l'écran.
   * Les données du modèle ne sont pas encore chargées à ce stade.
   */
  public void initialiserComposants();
  
  /**
   * Rafraichir les données de l'écran qui sont susceptibles de changer.
   * Cette méthode est appelées de nombreuses fois (à chaque saisie de valeurs).
   */
  public void rafraichir();
}
