/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.graphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;

import javax.swing.JToggleButton;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Ce composant est utilisé pour représenter visualement les sessions dans la barre de sessions.
 * Lorsque le la session est active c'est à dire affiché le bouton est de la même couleur que le fond du panel de la session.
 * Cet effet visuel donne l'impression qu'il s'agit d'un onglet.
 * Les boutons non actifs sont de la même couleur que celle de la barre de session par contre le texte à la couleur du fond du panel.
 */
public class SNBoutonSession extends JToggleButton implements Comparable<SNBoutonSession> {
  // Constantes
  private static final int MARGE_TEXTE = 10;
  private static final Color COULEUR_TEXTE_BOUTON_ACTIF = Color.BLACK;
  private static final Color COULEUR_TEXTE_BOUTON_INACTIF = SNCharteGraphique.COULEUR_FOND;
  public static final String PREFIXE = "tbtSession";
  
  // Variables
  private IdSession idSession = null;
  private String texteOriginal = null;
  private String texteLigne1 = null;
  private String texteLigne2 = null;
  private Character mnemonique = null;
  private String infobulle = null;
  private Dimension taille = new Dimension(SNBarreSession.LARGEUR_BOUTON, SNBarreSession.HAUTEUR_BOUTON);
  private boolean initialiserTexte = false;
  private int positionXOrigine = 0;
  private int positionXIntermediaire = 0;
  private Integer nouvelleCoordonneeX = null;
  private int demiLargeur = taille.width / 2;
  
  /**
   * Constructeur.
   */
  public SNBoutonSession() {
    super();
    initialiserComposant();
  }
  
  /**
   * Constructeur.
   */
  public SNBoutonSession(IdSession pIdSession, String pTexte, Character pMnemonique) {
    super();
    idSession = pIdSession;
    texteOriginal = pTexte;
    mnemonique = pMnemonique;
    initialiserComposant();
  }
  
  /**
   * Constructeur.
   */
  public SNBoutonSession(IdSession pIdSession, String pTexte, int pMnemonique) {
    this(pIdSession, pTexte, new Character((char) pMnemonique));
  }
  
  // -- Méthodes publiques
  
  /**
   * Modifie les dimensions du bouton.
   */
  public void modifierDimension(int pLargeur, int pHauteur) {
    if (pLargeur <= 0 || pHauteur <= 0 || (taille.getSize().width == pLargeur && taille.getSize().height == pHauteur)) {
      return;
    }
    
    taille = new Dimension(pLargeur, pHauteur);
    setMinimumSize(taille);
    setPreferredSize(taille);
    setMaximumSize(taille);
    
    // Calcul de la moitié de la largeur du bouton
    demiLargeur = getWidth() / 2;
  }
  
  /**
   * Redéfinition de la méthode qui dessine le composant.
   */
  @Override
  public void paintComponent(Graphics g) {
    // Le découpage du texte est effectué une seule fois lorsque le contexte graphique est bien initialisé
    if (!initialiserTexte) {
      decouperTexte(g, texteOriginal);
      initialiserTexte = true;
    }
    
    int w = getWidth();
    int h = getHeight();
    
    // Effet bouton sélectionné
    if (isSelected()) {
      // Le fond
      g.setColor(SNCharteGraphique.COULEUR_FOND);
      g.fillRect(0, 0, w, h);
      // La police
      g.setFont(SNCharteGraphique.POLICE_BOUTON_SESSION_ACTVF);
      g.setColor(COULEUR_TEXTE_BOUTON_ACTIF);
      ecrireTexte(g);
    }
    // Effet bouton non sélectionné
    else {
      // Le fond
      g.setColor(SNCharteGraphique.COULEUR_BARRE_SESSION);
      g.fillRect(0, 0, w, h);
      // Le séparateur vertical
      g.setColor(SNCharteGraphique.COULEUR_FOND);
      g.drawLine(w - 1, 0, w - 1, h);
      // La police
      g.setFont(SNCharteGraphique.POLICE_BOUTON_SESSION_NON_ACTIF);
      g.setColor(COULEUR_TEXTE_BOUTON_INACTIF);
      ecrireTexte(g);
    }
  }
  
  /**
   * Effacer les variables concernant le déplacement.
   */
  public void effacerVariables() {
    positionXOrigine = 0;
    positionXIntermediaire = 0;
    nouvelleCoordonneeX = null;
  }
  
  /**
   * Déplace le bouton via un drag'n drop.
   */
  public void deplacerBouton(MouseEvent pMouseEvent) {
    if (pMouseEvent == null) {
      return;
    }
    int position = pMouseEvent.getXOnScreen();
    
    // Calcul du déplacement en pixels depuis la dernière mesure
    int delta = position - positionXIntermediaire;
    // Ajout de l'abscisse du bouton par rapport à la position de la souris
    nouvelleCoordonneeX = new Integer(getX() + delta);
    setLocation(nouvelleCoordonneeX.intValue(), getY());
    // A chaque déplacement, les abscisses de l'ensemble des boutons est mémorisés afin d'avoir des valeurs fiables et non modifiées par
    // le système
    ((SNBarreSession) getParent()).stockerNouvelleAbscisse();
    // Mémorise la position pour le prochain calcul
    positionXIntermediaire = position;
  }
  
  /**
   * Détecte le début du déplacement du bouton via un drag'n drop.
   */
  public void demarrerDeplacerBouton(MouseEvent pMouseEvent) {
    if (pMouseEvent == null) {
      return;
    }
    // Initialisation des variables avant de démarrer le déplacement (de l'ensemble des boutons)
    ((SNBarreSession) getParent()).effacerNouvelleCoordonnee();
    
    // Modification du z-order afin que le composant reste toujours au dessus des autres lors du déplacement
    ((SNBarreSession) getParent()).modifierOrdreProfondeur(this);
    
    // Le bouton est sélectionné
    setSelected(true);
    // Mise à jour des nouvelles coordonnées
    positionXOrigine = pMouseEvent.getXOnScreen();
    positionXIntermediaire = positionXOrigine;
  }
  
  /**
   * Détecte la fin du déplacement du bouton.
   * A partir de ce moment, la liste des boutons est triée puis réaffichée pour prendre en compte le déplacement.
   */
  public void arreterDeplacerBouton(MouseEvent pMouseEvent) {
    if (pMouseEvent == null) {
      return;
    }
    int position = pMouseEvent.getXOnScreen();
    
    // Contrôle de la distance de déplacement, si elle est inférieure à la largeur du bouton le déplacement n'est pas effectué
    // car considéré comme accidentel
    int delta = Math.abs(position - positionXOrigine);
    if (delta < getWidth()) {
      // Annulation de toutes les modifications qui ont pu avoir lieu
      effacerVariables();
      ((SNBarreSession) getParent()).rafraichirBouton();
      return;
    }
    
    // Réorganisation de la liste des boutons de la barre de session
    ((SNBarreSession) getParent()).trierBouton();
  }
  
  /**
   * Compare cet objet avec un autre sur la position à la moitié du bouton sur l'axe des X (de la barre des boutons).
   * Utilisé pour trier la liste des boutons de la barre de session après un déplacement.
   */
  @Override
  public int compareTo(SNBoutonSession pBoutonSession) {
    if (getValeurDeTri() < pBoutonSession.getValeurDeTri()) {
      return -1;
    }
    else if (getValeurDeTri() > pBoutonSession.getValeurDeTri()) {
      return 1;
    }
    return 0;
  }
  
  // -- Méthodes privées
  
  /**
   * Configurer le bouton.
   */
  private void initialiserComposant() {
    // Afficher le curseur "main" lorsque la souris passe au dessus du bouton
    setCursor(SNCharteGraphique.CURSEUR_MAIN);
    
    // Définir le mnémonique
    if (mnemonique != null) {
      setMnemonic(mnemonique);
      setToolTipText(genererInfobulle());
    }
    
    // Définir la taille du bouton, fixe pour un bouton session si la résolution et le nombre de sessions le permettent
    setMinimumSize(taille);
    setPreferredSize(taille);
    setMaximumSize(taille);
    
    // Initialisation du texte (utile pour QFTest)
    if (texteOriginal != null) {
      setText(texteOriginal);
    }
    
    // Initialisation du nom du composant
    if (idSession != null) {
      setName(PREFIXE + idSession.getNumero());
    }
    
    // Initialise la méthode qui va détecter le drag'n drop
    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseDragged(MouseEvent e) {
        deplacerBouton(e);
      }
    });
    // Initialise les méthodes qui vont détecter le début et la fin du drag'ndrop
    addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        demarrerDeplacerBouton(e);
      }
      
      @Override
      public void mouseReleased(MouseEvent e) {
        arreterDeplacerBouton(e);
      }
    });
  }
  
  /**
   * Découpe le texte sur 1 ou 2 lignes en fonctions de la largeur initiale du bouton.
   */
  private void decouperTexte(final Graphics g, String pTexte) {
    if (g == null) {
      throw new MessageErreurException("Le contexte graphique n'est pas initialisé.");
    }
    pTexte = Constantes.normerTexte(pTexte);
    
    // Calcul de la largeur disponible pour le texte
    int largeurDisponible = SNBarreSession.LARGEUR_BOUTON - (MARGE_TEXTE * 2);
    
    // Si la largeur du texte à afficher est supérieure à la largeur disponible, le texte sera affiché sur deux lignes
    if (g.getFontMetrics().stringWidth(pTexte) > largeurDisponible) {
      int milieuTexte = pTexte.length() / 2;
      // Déplacement vers la gauche
      int i = milieuTexte;
      while (i > 0 && pTexte.charAt(i) != ' ') {
        i--;
      }
      String ligne1a = "";
      String ligne2a = "";
      // Dans le cas où aucun espace n'a été trouvé d'espace dans le texte
      if (i == 0) {
        ligne1a = pTexte;
      }
      // Dans le cas où un espace a bien été trouvé
      else {
        ligne1a = pTexte.substring(0, i);
        ligne2a = pTexte.substring(i + 1);
      }
      // Déplacement vers la droite
      int j = milieuTexte;
      while (j < pTexte.length() && pTexte.charAt(j) != ' ') {
        j++;
      }
      String ligne1b = pTexte.substring(0, j);
      String ligne2b = "";
      // Dans le cas où aucun espace n'a été trouvée
      if (j < pTexte.length()) {
        ligne2b = pTexte.substring(j + 1);
      }
      // Comparaison du découpage afin de récupérer les meilleures proportions
      int deltaa = g.getFontMetrics().stringWidth(ligne1a) - g.getFontMetrics().stringWidth(ligne2a);
      int deltab = g.getFontMetrics().stringWidth(ligne1b) - g.getFontMetrics().stringWidth(ligne2b);
      // Le découpage le plus proche de la moitié de la chaine est utilisé
      if (Math.abs(deltaa) < Math.abs(deltab)) {
        texteLigne1 = ligne1a;
        texteLigne2 = ligne2a;
      }
      else {
        texteLigne1 = ligne1b;
        texteLigne2 = ligne2b;
      }
    }
    else {
      texteLigne1 = pTexte;
      texteLigne2 = null;
    }
  }
  
  /**
   * Ecrit le texte dans le bouton sur 1 ou 2 lignes en fonctions de sa longueur.
   */
  private void ecrireTexte(final Graphics g) {
    int w = getWidth();
    int h = getHeight();
    
    // Ecriture du texte sur 2 lignes
    if (texteLigne2 != null) {
      // Calcul de l'espace entre les lignes (-2 pour un meilleur centrage vertical)
      int tailleEspace = ((h + g.getFontMetrics().getAscent()) / 3) - 2;
      g.drawString(texteLigne1, (w - g.getFontMetrics().stringWidth(texteLigne1)) / 2 + 1, tailleEspace);
      g.drawString(texteLigne2, (w - g.getFontMetrics().stringWidth(texteLigne2)) / 2 + 1, tailleEspace * 2);
    }
    else {
      g.drawString(texteLigne1, (w - g.getFontMetrics().stringWidth(texteLigne1)) / 2 + 1, (h + g.getFontMetrics().getAscent()) / 2 - 1);
    }
  }
  
  /**
   * Générer le texte d'infobulle qui rappelle le raccourci utilisable pour déclencher le bouton.
   */
  private String genererInfobulle() {
    if (mnemonique == null) {
      return "";
    }
    return "Ctrl + " + mnemonique;
  }
  
  /**
   * Calcul de l'abscisse à la moitié de la largeur du bouton dont l'axe est la barre de session.
   * Cette valeur va servir au tri de la liste qui contient l'ensemble des boutons de la barre de session.
   */
  private int getValeurDeTri() {
    if (nouvelleCoordonneeX == null) {
      return -1;
    }
    return nouvelleCoordonneeX.intValue() + demiLargeur;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne l'idSession associée au bouton.
   */
  public IdSession getIdSession() {
    return idSession;
  }
  
  public void setNouvelleCoordonneeX(Integer nouvelleCoordonneeX) {
    this.nouvelleCoordonneeX = nouvelleCoordonneeX;
  }
  
}
