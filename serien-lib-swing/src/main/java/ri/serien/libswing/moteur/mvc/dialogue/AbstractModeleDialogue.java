/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc.dialogue;

import java.util.Observable;
import java.util.Observer;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.InterfaceModele;

/**
 * Modèle pour une boîte de dialogue.
 *
 * Voir la documentation de InterfaceModele pour plus d'informations.
 */
public abstract class AbstractModeleDialogue extends Observable implements InterfaceModele {
  // Constantes
  private static final int NE_PAS_SORTIR = 0;
  private static final int SORTIE_AVEC_VALIDATION = 1;
  private static final int SORTIE_AVEC_ANNULATION = 2;
  private static final int SORTIE_AVEC_SUPPRESSION = 3;

  // Variables
  private final SessionBase session;
  private InterfaceCleVue cleVueEnfantActive = null;
  private String titreEcran = "";
  private int modeSortie = NE_PAS_SORTIR;
  private boolean donneesChargees = false;

  /**
   * Constructeur.
   */
  public AbstractModeleDialogue(SessionBase pSession) {
    session = pSession;
  }

  // -- Méthodes de InterfaceModele

  @Override
  abstract public void initialiserDonnees();

  @Override
  abstract public void chargerDonnees();

  @Override
  public final void rafraichir() {
    setChanged();
    notifyObservers();
  }

  @Override
  public final void abonnerVue(Observer pObserver) {
    addObserver(pObserver);
  }

  @Override
  public final InterfaceCleVue getCleVueEnfantActive() {
    return cleVueEnfantActive;
  }

  @Override
  public final void setCleVueEnfantActive(InterfaceCleVue pCleVueEnfantActive) {
    cleVueEnfantActive = pCleVueEnfantActive;
  }

  @Override
  public final boolean isDonneesChargees() {
    return donneesChargees;
  }

  @Override
  public final void setDonneesChargees(boolean donneesChargees) {
    this.donneesChargees = donneesChargees;
  }

  @Override
  public final boolean isModeSortie() {
    return modeSortie != NE_PAS_SORTIR;
  }

  @Override
  public final void setModeSortie(int pModeSortie) {
    modeSortie = pModeSortie;
  }

  @Override
  public final String getTitreEcran() {
    return titreEcran;
  }

  @Override
  public final void setTitreEcran(String pTitreEcran) {
    titreEcran = pTitreEcran;
  }

  // -- Méthodes publiques

  /**
   * Quitter la boite de dialogue en validant la saisie. Les données modifiées sont conservées.
   * Cette méthode fixe le mode de sortie avec validation et demande un rafraîchissement pour déclencher la fermeture de la vue.
   * Si la méthode est surchargéee, il est recommandé d'appelé cette méthode parente à la fin.
   */
  public void quitterAvecValidation() {
    modeSortie = SORTIE_AVEC_VALIDATION;
    rafraichir();
  }

  /**
   * Quitter la boite de dialogue an annulant la saisie. Les données modifiées ne sont pas conservées.
   * Cette méthode fixe le mode de sortie avec annulation et demande un rafraîchissement pour déclencher la fermeture de la vue.
   * Si la méthode est surchargéee, il est recommandé d'appelé cette méthode parente à la fin.
   */
  public void quitterAvecAnnulation() {
    modeSortie = SORTIE_AVEC_ANNULATION;
    rafraichir();
  }

  /**
   * Quitter la boite de dialogue an annulant la saisie. Les données modifiées seront supprimées de base de données.
   * Cette méthode fixe le mode de sortie avec suppression et demande un rafraîchissement pour déclencher la fermeture de la vue.
   * Si la méthode est surchargéee, il est recommandé d'appelé cette méthode parente à la fin.
   */
  public void quitterAvecSuppression() {
    modeSortie = SORTIE_AVEC_SUPPRESSION;
    rafraichir();
  }

  // -- Accesseurs

  /**
   * Tester si l'utilisateur est sortie de la boîte de dialogue en validant sa saisie.
   */
  public final boolean isSortieAvecValidation() {
    return modeSortie == SORTIE_AVEC_VALIDATION;
  }

  /**
   * Tester si l'utilisateur est sortie de la boîte de dialogue en annulant sa saisie.
   */
  public final boolean isSortieAvecAnnulation() {
    return modeSortie == SORTIE_AVEC_ANNULATION;
  }

  /**
   * Tester si l'utilisateur est sortie de la boîte de dialogue en supprimant l'objet de sa saisie.
   */
  public final boolean isSortieAvecSuppression() {
    return modeSortie == SORTIE_AVEC_SUPPRESSION;
  }

  /**
   * Session associée au modèle.
   */
  public final SessionBase getSession() {
    if (session == null) {
      throw new MessageErreurException("Aucune session n'est associée au modèle.");
    }
    return session;
  }

  /**
   * Identifiant de la session associée au modèle.
   */
  public final IdSession getIdSession() {
    if (session == null) {
      throw new MessageErreurException("Aucune session n'est associée au modèle.");
    }
    return session.getIdSession();
  }
}
