/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.interpreteur;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.ioGeneric;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oKey;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.SessionTransfert;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * Description du buffer qui transite entre le client & le serveur
 * ==> A faire:
 * ==> Gérer plus d'erreurs afin de blinder le code
 * ==> Améliorer setRecord
 * ==> Voir si les méthodes qui retourne le resultat de l'interprétation des indicateurs ne peut
 * ==> pas être remplacé avec oData
 */
public class FluxRecord {
  // Constantes
  public final static int OFFSET_ENTETE = 0;
  public final static int LONGUEUR_ENTETE = 89;
  public final static int OFFSET_TOUCHE = OFFSET_ENTETE + LONGUEUR_ENTETE;
  public final static int LONGUEUR_TOUCHE = 2;
  public final static int OFFSET_INDICATEUR = OFFSET_TOUCHE + LONGUEUR_TOUCHE;
  public final static int LONGUEUR_INDICATEUR = 99;
  public final static int OFFSET_DONNEE = OFFSET_INDICATEUR + LONGUEUR_INDICATEUR;
  public final static int OFFSET_DONNEE_MAX = Constantes.DATAQLONG - OFFSET_DONNEE;
  public final static int MAX_TAILLE_CACHE = 20;
  
  public final static char EXFMT = '1';
  public final static char WRITE = '0';
  public final static char WRITETEMPO = '2';
  
  // Variables
  private ArrayList<InformationsRecord> listeInfosRecord = new ArrayList<InformationsRecord>();
  private char touche = ' ';
  
  private SessionTransfert trfSession = null;
  private HashMap<String, String> translationtable = null;
  private String codepageServeur = System.getProperty("file.encoding");
  private ClassLoader loader = null;
  private StringBuffer buffer = new StringBuffer(Constantes.DATAQLONG);
  
  private static boolean init = false;
  private static char[] spaces = new char[Constantes.DATAQLONG];
  
  private InformationsRecord irecordcourant = null; // Contient le irecord en cours de traitement
  private InformationsRecord irecordExFmt = null; // Contient toujours le irecord qui a le WBLC = 1 (EXFMT)
  
  private IdSession idSession = null;
  private EnvUser envUser = null;
  private HashMap<Integer, SNPanelEcranRPG> listeoFrame = new HashMap<Integer, SNPanelEcranRPG>();
  private String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public FluxRecord(IdSession pIdSession, int pLbuffer, EnvUser pEnvUser) {
    idSession = pIdSession;
    envUser = pEnvUser;
    trfSession = envUser.getTransfertSession();
    translationtable = envUser.getTranslationTable();
    codepageServeur = envUser.getCodepageServeur();
    if (!init) {
      Arrays.fill(spaces, ' ');
      init = true;
    }
  }
  
  /**
   * Préparation du buffer à renvoyer au RPG(à partir du Record en cours)
   */
  private String setBuffer(boolean maj) {
    oData donnee = null;
    
    // Chargement du buffer avec les touches
    buffer.setCharAt(OFFSET_TOUCHE, touche);
    
    // Chargement du buffer avec les indicateurs si besoin (voir CA/CF)
    if (maj) {
      // Mise à blanc du buffer (on ne touche pas à l'entête, pas de raison)
      buffer.setLength(OFFSET_INDICATEUR);
      buffer.append(irecordExFmt.getIndicateurs()).append(spaces);
      
      // Chargement du buffer avec les données TODO ATTENTION vérifier l'ordre d'ecriture sinon ca va saigner !!
      for (Map.Entry<String, ioGeneric> entry : irecordExFmt.getDescriptionRecord().listeElement.entrySet()) {
        if (entry.getValue() instanceof oData) {
          donnee = (oData) entry.getValue();
          // Dépassement de la capacité du buffer/DataQ
          if (donnee.getOffset() > OFFSET_DONNEE_MAX) {
            continue;
          }
          buffer.replace(OFFSET_DONNEE + donnee.getOffset(), OFFSET_DONNEE + donnee.getOffset() + donnee.getLongueur(),
              donnee.getValeurToBuffer());
          // donnee.getNom()+"="+donnee.getValeurToBuffer()+"|" +donnee.getLongueur());
        }
      }
    }
    else {
      for (Map.Entry<String, ioGeneric> entry : irecordExFmt.getDescriptionRecord().listeElement.entrySet()) {
        if (entry.getValue() instanceof oData) {
          donnee = (oData) entry.getValue();
          if (donnee.getNom().equals("XXLIG") || donnee.getNom().equals("XXCOL")) {
            buffer.replace(OFFSET_DONNEE + donnee.getOffset(), OFFSET_DONNEE + donnee.getOffset() + donnee.getLongueur(),
                donnee.getValeurToBuffer());
            // donnee.getNom()+"="+donnee.getValeurToBuffer()+"|" +donnee.getLongueur());
          }
        }
      }
    }
    
    return buffer.toString().trim();
  }
  
  /**
   * Initialise les touches
   * @param tch
   */
  public void setTouches(char tch) {
    touche = tch;
  }
  
  /**
   * Initialise le Record en cours à partir du buffer réceptionné.
   */
  public int setRecord(String abuffer) {
    // Chargement des données
    buffer.setLength(OFFSET_ENTETE);
    buffer.append(abuffer);
    
    // Génération de la clé pour voir si le record existe dans la liste (Nom du format + '.' + Nom du format + '_' +
    // code du record, ex=VCGM06FM.VCGM06FM_A2)
    String classname = buffer.substring(38, 48).trim();
    classname = InformationsRecord.getPackage(classname) + '.' + classname + '.' + classname + '_' + buffer.substring(60, 79).trim();
    
    int idclassname = classname.hashCode();
    irecordcourant = found(idclassname, buffer.substring(15, 25).trim());
    if (irecordcourant == null) {
      irecordcourant = new InformationsRecord(idclassname, classname);
      listeInfosRecord.add(irecordcourant);
      irecordcourant.setVarFromEntete(buffer.toString());
      
      // Chargement de la description du record pour le découpage
      if (loader == null) {
        loader = Thread.currentThread().getContextClassLoader();
      }
      if (!irecordcourant.lectureFichierDAT(loader)) {
        return Constantes.ERREUR;
      }
    }
    else {
      irecordcourant.setVarFromEntete(buffer.toString());
    }
    
    // Récupération des touches de fonctions actives & des données ( Attention au trimage lors de l'envoi par Socket)
    irecordcourant.setVarFromCorps(translationtable);
    
    if (irecordcourant.isExFmt()) {
      irecordExFmt = irecordcourant;
    }
    
    return Constantes.OK;
  }
  
  /**
   * Retourne la racine du package (sans le nom de la classe)
   */
  public String getRacinePackage(String pNomBundle) {
    // On extrait le nom du format du bundle
    pNomBundle = Constantes.normerTexte(pNomBundle);
    String nomFormat = pNomBundle.substring(0, pNomBundle.indexOf('.'));
    // On contrôle s'il s'agit du format en cours
    if (irecordcourant.getNomFormat().equals(nomFormat)) {
      return irecordcourant.getRacinePackage();
    }
    // Dans le cas contraire on le recherche dans la liste
    InformationsRecord inforecord = found(nomFormat);
    if (inforecord != null) {
      return inforecord.getRacinePackage();
    }
    return "";
  }
  
  /**
   * @param idclassname
   * @param infoPrg
   * @return
   */
  @SuppressWarnings("rawtypes")
  public SNPanelEcranRPG getoFrame(int idclassname, EnvProgramClient infoPrg) {
    SNPanelEcranRPG ecran = listeoFrame.get(idclassname);
    if (ecran == null) {
      ArrayList<EnvProgramClient> parametreFrame = new ArrayList<EnvProgramClient>();
      try {
        // Préparation des paramètres
        parametreFrame.add(infoPrg);
        // Récupération de la classe donnée par fluxRpg.getClassName()
        // Class<?> classe = Class.forName (irecordcourant.getClassName(), false, loader);
        Class<?> classe = loader.loadClass(irecordcourant.getClassName());
        // Récupération du constructeur prenant en paramètre une ArrayList (ainsi on est pas limité en nombre de
        // paramètres)
        // Constructor constructeur = classe.getConstructor (new Class [] {Class.forName ("java.util.ArrayList")});
        Constructor<?> constructeur = classe.getConstructor(new Class[] { loader.loadClass("java.util.ArrayList") });
        ecran = (SNPanelEcranRPG) constructeur.newInstance(new Object[] { parametreFrame });
        ecran.setFlux(this);
        ecran.setIdClassName(idclassname);
        listeoFrame.put(idclassname, ecran);
        parametreFrame.clear();
      }
      catch (Exception e) {
        // La classe n'existe pas
        msgErreur = "[SessionPanel] Exception: " + e + Constantes.crlf + "[SessionPanel] Message: " + e.getMessage() + Constantes.crlf
            + "[SessionPanel] Stack:";
        for (int x = 0; x < e.getStackTrace().length; x++) {
          msgErreur += Constantes.crlf + e.getStackTrace()[x];
        }
        // msgErreur += "\nfluxRpg.getClassName():" + irecordcourant.getClassName() +" fluxRpg.getLoader():" +
        // irecordcourant.getLoader() + "\nParamètre:" + parametreFrame==null?"null":parametreFrame.size();
        msgErreur += "\nfluxRpg.getClassName():" + irecordcourant.getClassName() + " fluxRpg.getLoader():" + loader + "\nParamètre:"
            + parametreFrame.size();
        
        Trace.erreur(e, msgErreur);
      }
    }
    return ecran;
  }
  
  /**
   * Lève un indicateur donnée du buffer
   * @return
   */
  public void setOn(int indicateur) {
    irecordExFmt.getIndicateurs()[indicateur - 1] = '1';
  }
  
  /**
   * Baisse un indicateur donnée du buffer
   * @return
   */
  public void setOff(int indicateur) {
    irecordExFmt.getIndicateurs()[indicateur - 1] = '0';
  }
  
  /**
   * Retourne le oRecord du flux
   * @return
   */
  public oRecord getDescriptionRecord() {
    return irecordcourant.getDescriptionRecord();
  }
  
  /**
   * Retourne le nom du record du DSPF
   * @return
   */
  public String getRecordName() {
    return irecordcourant.getRecordName();
  }
  
  /**
   * Retourne le nom du format et du record du DSPF
   * @return
   */
  public String getFormatRecord() {
    return irecordcourant.getFormatRecord();
  }
  
  /**
   * Retourne le nom du classname pour le panel
   * @return
   */
  public String getClassName() {
    return irecordcourant.getClassName();
  }
  
  /**
   * Retourne le hashcode du classname pour le panel
   * @return
   */
  public int getIdClassName() {
    return irecordcourant.getIdClassName();
  }
  
  /**
   * Retourne le nom du format
   * @return
   */
  public String getNomFormat() {
    return irecordcourant.getNomFormat();
  }
  
  /**
   * Retourne les touches
   * @return
   */
  public char getTouches() {
    return touche;
  }
  
  /**
   * Retourne les indicateurs
   * @return
   */
  public char[] getIndicateurs() {
    return irecordcourant.getIndicateurs();
  }
  
  /**
   * Retourne le loader sur la jar du fichier du Record, null sinon
   * @return
   */
  public ClassLoader getLoader() {
    return loader;
  }
  
  /**
   * Retourne le nom du fichier du Record, null sinon
   * @return
   */
  public String getNomFichier() {
    return irecordExFmt.getNomFichierFMT();
  }
  
  /**
   * Retourne le nombre de ligne à clearer
   * @return
   */
  public int getNbrLigneCLR() {
    return irecordExFmt.getDescriptionRecord().getNbrLigneCLR();
  }
  
  /**
   * Retourne la valeur du code blocage envoyé par le RPG
   * @return
   */
  public char getBlocageEcran() {
    return irecordcourant.getBlocageEcran();
  }
  
  /**
   * Retourne si le panel sera affichable ou pas
   * @return
   */
  public boolean isPanelAffichage() {
    return !(irecordcourant.getBlocageEcran() == WRITE);
  }
  
  /**
   * Retourne le vecteur FMT
   */
  public String getVFmt() {
    return irecordcourant.getVFmt();
  }
  
  /**
   * Retourne 1 si le RPG détecte une erreur de saisie
   * @return
   */
  public char getErreurPRG() {
    return irecordcourant.getErreurPRG();
  }
  
  /**
   * Retourne le nom du programme RPG qui tourne
   * @return
   */
  public String getNomPrg() {
    return irecordcourant.getNomPrg();
  }
  
  /**
   * Retourne le nom du du fichier Aide
   * @return
   */
  public String getNomAide() {
    return irecordExFmt.getNomAide();
  }
  
  /**
   * Retourne le module
   * @return
   */
  public String getModule() {
    return irecordcourant.getModule();
  }
  
  /**
   * Retourne le résultat d'une interprétation des indicateurs
   * ATTENTION les indicateurs doivent être encadrés par des parenthèses ex: (70) AND (69) AND (N42)
   * @return
   */
  public int getEvaluationIndicateur(String cond) {
    // iIndicateur.evaluation(iIndicateur.tabconvert2notpol(cond)));
    return irecordExFmt.getEvaluationIndicateur(cond);
    // return irecordcourant.getEvaluationIndicateur(cond);
  }
  
  /**
   * Retourne le résultat d'une interprétation des indicateurs
   * ATTENTION les indicateurs doivent être encadrés par des parenthèses ex: (70) AND (69) AND (N42)
   * @return
   */
  public int getEvaluationIndicateur(int idclassname, String cond) {
    // iIndicateur.evaluation(iIndicateur.tabconvert2notpol(cond)));
    return found(idclassname).getEvaluationIndicateur(cond);
  }
  
  /**
   * Retourne le résultat d'une interprétation des indicateurs
   * ATTENTION les indicateurs doivent être encadrés par des parenthèses ex: (70) AND (69) AND (N42)
   * @return
   */
  public int getEvaluationIndicateur(int idclassname, String nomrecord, String cond) {
    // iIndicateur.evaluation(iIndicateur.tabconvert2notpol(cond)));
    return found(idclassname, nomrecord).getEvaluationIndicateur(cond);
  }
  
  /**
   * Retourne le mode en cours (création, modification, consultation, annulation, duplication ou indefini)
   * @return
   */
  public int getMode() {
    return irecordExFmt.mode;
  }
  
  /**
   * Recherche si on doit ou pas mettre à jour le buffer qui repart vers le serveur
   */
  private boolean isMajBuffer(String key) {
    // for (int i=listeKey.size(); --i>=0;)
    // (irecordExFmt.listeKey==null?"-1":irecordExFmt.listeKey.size()));
    for (int i = irecordExFmt.listeKey.size(); --i >= 0;) {
      if (irecordExFmt.listeKey.get(i).getKey().equals(key)) {
        // + listeKey.get(i).getNom());
        return irecordExFmt.listeKey.get(i).isMajData();
      }
    }
    return true;
  }
  
  /**
   * Renvoie le buffer au serveur.
   */
  public boolean sendBuffer(char indic[], char tch, String key) {
    boolean retour = false;
    try {
      touche = tch;
      if (indic != null) {
        irecordExFmt.setIndicateurs(indic);
      }
      String message = setBuffer(isMajBuffer(key));
      retour = ManagerServiceTechnique.envoyerMessageAuProgramme(idSession, message);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
    return retour;
  }
  
  /**
   * Nettoyage de la classe
   */
  public void dispose() {
    irecordcourant = null;
    irecordExFmt = null;
    
    // On dirait que l'on consomme plus de ram si on décommente ces 2 lignes
    for (int i = 0; i < listeInfosRecord.size(); i++) {
      listeInfosRecord.get(i).dispose();
    }
    listeInfosRecord.clear();
    
    // On libère les panels
    if (listeoFrame != null) {
      for (Entry<Integer, SNPanelEcranRPG> entry : listeoFrame.entrySet()) {
        if (entry.getValue() != null) {
          entry.getValue().dispose();
        }
      }
      listeoFrame.clear();
      listeoFrame = null;
    }
    // toServer = null;
    trfSession = null;
    translationtable = null;
  }
  
  /**
   * Retourne la liste des touches de fonctions actives pour le record en cours
   * @return
   */
  public ArrayList<oKey> getListeKey() {
    if (irecordExFmt == null || irecordExFmt.listeKey == null) {
      return new ArrayList<oKey>();
    }
    return irecordExFmt.listeKey;
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    // La récupération du message est à usage unique
    String chaine = msgErreur;
    if (irecordcourant != null) {
      chaine += "\n" + irecordcourant.getMsgErreur();
    }
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * @return codepageServeur
   */
  public String getCodepageServeur() {
    return codepageServeur;
  }
  
  /**
   * @param codepageServeur codepageServeur à définir
   */
  public void setCodepageServeur(String codepageServeur) {
    this.codepageServeur = codepageServeur;
  }
  
  /**
   * Recherche un élément dans la liste.
   */
  private InformationsRecord found(int idclassname, String nomrecord) {
    // for (int i=listeInfosRecord.size(); --i>=0;)
    // listeInfosRecord.get(i).getClassName() + " " + listeInfosRecord.get(i).getFormatRecord());
    for (int i = listeInfosRecord.size(); --i >= 0;) {
      if ((listeInfosRecord.get(i).getIdClassName() == idclassname) && listeInfosRecord.get(i).getRecordName().equals(nomrecord)) {
        // listeInfosRecord.get(i).getClassName() + " " + listeInfosRecord.get(i).getFormatRecord());
        return listeInfosRecord.get(i);
      }
    }
    return null;
  }
  
  /**
   * Recherche un élément dans la liste (le premier qu'il trouve puisque on cherche sur un seul critère).
   */
  private InformationsRecord found(int idclassname) {
    // for (int i=listeInfosRecord.size(); --i>=0;)
    // listeInfosRecord.get(i).getClassName() + " " + listeInfosRecord.get(i).getFormatRecord());
    if (irecordcourant.getIdClassName() == idclassname) {
      // + irecordcourant.getFormatRecord());
      return irecordcourant;
    }
    else if (irecordExFmt.getIdClassName() == idclassname) {
      // irecordExFmt.getFormatRecord());
      return irecordExFmt;
    }
    else {
      for (int i = listeInfosRecord.size(); --i >= 0;) {
        if (listeInfosRecord.get(i).getIdClassName() == idclassname) {
          // listeInfosRecord.get(i).getClassName() + " " + listeInfosRecord.get(i).getFormatRecord());
          return listeInfosRecord.get(i);
        }
      }
    }
    return null;
  }
  
  /**
   * Recherche un élément dans la liste.
   */
  private InformationsRecord found(String pNomFormat) {
    for (int i = listeInfosRecord.size(); --i >= 0;) {
      if (listeInfosRecord.get(i).getNomFormat().equals(pNomFormat)) {
        return listeInfosRecord.get(i);
      }
    }
    return null;
  }
  
  /**
   * Met à jour la liste des records pour un panel particulier.
   * @param listeRecord
   */
  public void majRecords(final HashMap<String, oRecord> listeRecord, int idclassname) {
    for (InformationsRecord inforecord : listeInfosRecord) {
      if (inforecord.getIdClassName() == idclassname) {
        listeRecord.put(inforecord.getRecordName(), inforecord.getDescriptionRecord());
      }
    }
  }
  
  /**
   * @param trfSession le trfSession à définir
   */
  public void setTrfSession(SessionTransfert trfSession) {
    this.trfSession = trfSession;
  }
  
}
