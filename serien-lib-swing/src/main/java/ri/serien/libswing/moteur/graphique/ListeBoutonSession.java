/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.graphique;

import java.util.ArrayList;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Liste de SNBoutonSession.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de bouton de session.
 */
public class ListeBoutonSession extends ArrayList<SNBoutonSession> {
  
  // -- Méthodes publiques
  
  /**
   * Sélectionne la session à partir de son id.
   */
  public void selectionnerSession(IdSession pIdSession) {
    if (pIdSession == null) {
      throw new MessageErreurException("L'identifiant de session est invalide.");
    }
    SNBoutonSession boutonSession = retrouverBoutonAvecIdSession(pIdSession);
    if (boutonSession == null) {
      throw new MessageErreurException("Le bouton à supprimer de la barre de sessions est invalide.");
    }
    
    // Sélectionne le bouton correspondant à la session active
    boutonSession.setSelected(true);
  }
  
  /**
   * Retourne le bouton à partir de l'id session.
   */
  public SNBoutonSession retrouverBoutonAvecIdSession(IdSession pIdSession) {
    if (pIdSession == null) {
      throw new MessageErreurException("L'identifiant de session est invalide.");
    }
    
    for (SNBoutonSession bouton : this) {
      if (bouton.getIdSession().equals(pIdSession)) {
        return bouton;
      }
    }
    return null;
  }
  
  /**
   * Efface les variables concernant le déplacement.
   */
  public void effacerNouvelleCoordonnee() {
    for (SNBoutonSession bouton : this) {
      bouton.effacerVariables();
    }
  }
  
  /**
   * Retourne l'index du bouton selectionné (session active).
   */
  public int getIndexBoutonSelectionne() {
    int index = 0;
    for (SNBoutonSession boutonSession : this) {
      if (boutonSession.isSelected()) {
        return index;
      }
      index++;
    }
    return -1;
  }
  
}
