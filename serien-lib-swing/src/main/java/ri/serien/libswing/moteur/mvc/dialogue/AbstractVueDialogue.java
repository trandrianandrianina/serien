/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc.dialogue;

import java.awt.Cursor;
import java.awt.Frame;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.mvc.ChargementDonnees;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.InterfaceModele;
import ri.serien.libswing.moteur.mvc.InterfaceVue;

/**
 * Vue de la boîte de dialogue de base de Série N.
 *
 * Toutes les boîtes de dialogue doivent hériter de cette classe afin de garantir une contruction et un fonctionnement standard.
 * L'affichage de la boîte de dialogue s'effectue en trois étapes : affichage immédiat des composants graphiques (sans leurs
 * données),chargement des données en dehors de la thread graphique, mise à jour des données après leur chargement. Le tout est protégé
 * pour traiter correctement les MessageErreurException et Exception.
 *
 * Les méthodes initialiserComposants et rafraichir sont à surcharger. Voir la documentation dans InterfaceVue
 */
public abstract class AbstractVueDialogue<M extends InterfaceModele> extends JDialog implements InterfaceVue<M>, Observer {
  private final M modele;
  private final boolean traceActive;
  private InterfaceVue<?> vueParent = null;
  private Map<Object, InterfaceVue<?>> listeVueEnfant = new HashMap<Object, InterfaceVue<?>>();
  private InterfaceVue<?> vueEnfantActive = null;
  private boolean composantInitialise = false;
  private boolean protectionReentrance = false;
  private boolean evenementsActifs = false;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes abstraites à implémenter
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Contructeur standard de la boîte de dialogue.
   *
   * Le logiciel cherche à trouver une fenêtre active pour rattacher la boîte de dialogue modale à une fenêtre parente. Avec ce
   * constructeur standard, les traces sont générées ce qui est souhaitable dans la majorité des cas.
   *
   * @param pModele Modèle associé à la vue.
   */
  public AbstractVueDialogue(M pModele) {
    this(pModele, true);
  }
  
  /**
   * Contructeur de la boîte de dialogue.
   * Le logiciel cherche à trouver une fenêtre active pour rattacher la boîte de dialogue modale à une fenêtre parente.
   *
   * @param pModele Modèle associé à la vue.
   * @param pTraceActive true=traces générées, false=pas de traces.
   */
  public AbstractVueDialogue(M pModele, boolean pTraceActive) {
    super(rechercherFenetreActive());
    
    // Vérifier que le modèle n'est pas nul
    if (pModele == null) {
      throw new MessageErreurException("Impossible de créer la vue car le modèle est invalide.");
    }
    
    // Modifier les informations de la vue
    modele = pModele;
    traceActive = pTraceActive;
    
    // Etre notifier lorsque le modèle est modifié (pour faire le rafraichissement)
    modele.abonnerVue(this);
    
    // Définir le logo de la barre de titre de la boîte de dialogue
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes abstraites à implémenter
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialiser les composants graphiques de la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public abstract void initialiserComposants();
  
  /**
   * Rafraichir les données de la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public abstract void rafraichir();
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes d'InterfaceVue
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Afficher la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void afficher() {
    try {
      // Vérifier que la méthode a bien été appelée dans la thread graphique
      if (Trace.isModeDebug() && !SwingUtilities.isEventDispatchThread()) {
        Trace.debugStackTrace();
        throw new MessageErreurException(
            "La méthode " + getClass().getSimpleName() + ".afficher() a été appelé en dehors de la thread graphique (EDT).");
      }
      
      // Afficher le curseur d'attente pendant le chargement des données
      // Le curseru standard sera remis lors du rafraichissement effectuée après le chargement des données
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
      
      // Indiquer au modèle que les données ne sont pas chargées.
      modele.setDonneesChargees(false);
      
      // Tracer
      if (traceActive) {
        Trace.debug(modele.getClass(), "initialiserDonnees");
      }
      
      // Initialiser les données du modèle
      // L'idée est de nettoyer rapidement les données avant le premier rafraîchissement et avant le chargement des données lourdes.
      modele.initialiserDonnees();
      
      // Initialiser les composants graphiques
      initialiserComposantsAvecEnfants();
      
      // Rafraîchir les données (les composants graphiques sont créés et les données sont rafraîchies à vide).
      // Le chargement des données n'est pas fait à ce stade. L'objectif est d'afficher les composants rapidement même s'ils sont
      // vides.
      rafraichirComposantsAvecEnfants();
      
      // Tracer
      if (traceActive) {
        Trace.debug(modele.getClass(), "chargerDonnees");
      }
      
      // Lancer le chargement des données de la vue (dans une thread à part)
      // Lorsque le chargement des données est terminé, la méthode notifier() est appelée afin de rafraichir les données
      ChargementDonnees chargementDonnees = new ChargementDonnees(modele);
      chargementDonnees.execute();
      
      // Afficher la boîte de dialogue (à faire à la fin car bloquant pour les boîtes de dialogues modales)
      setVisible(true);
    }
    catch (
    
    Exception e) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(e);
    }
  }
  
  /**
   * Initialiser les composants graphiques de la vue et des vues enfants.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public void initialiserComposantsAvecEnfants() {
    try {
      // Activer la protection contre la réentrance
      protectionReentrance = true;
      
      // Désactiver la gestion des évènements
      evenementsActifs = false;
      
      // Tracer
      if (traceActive) {
        Trace.debug(getClass(), "initialiserComposants");
      }
      
      // Appeler la méthode initialiserComposants de la vue
      initialiserComposants();
      
      // Nommer la fenêtre du nom de sa classe Vue (pour QFTest)
      // Cette étape doit être effectuée après initialiserComposants() car JFormDesigner donne le nom "this" au composant.
      setName(getClass().getSimpleName());
      
      // Initialiser les composants des vues enfants
      for (InterfaceVue<?> interfaceVue : listeVueEnfant.values()) {
        interfaceVue.initialiserComposantsAvecEnfants();
      }

      // Indiquer que le composant a été initialisé
      composantInitialise = true;
    }
    catch (Exception e) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(e);
    }
    finally {
      // Réactiver la gestion des évènements
      evenementsActifs = true;
      
      // Désactiver la protection contre la réentrance avant le chargement des données
      protectionReentrance = false;
    }
  }
  
  /**
   * Rafraîchir les données des composants graphiques.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void rafraichirComposants() {
    try {
      // Ne pas déclencher de rafraîchissement pour les vues enfantes car c'est la vue parente qui le fait
      if (vueParent != null) {
        return;
      }
      
      // Vérifier que la méthode a bien été appelée dans la thread graphique
      if (Trace.isModeDebug() && !SwingUtilities.isEventDispatchThread()) {
        Trace.debugStackTrace();
        throw new MessageErreurException(
            "La méthode " + getClass().getSimpleName() + ".rafraichir() a été appelé en dehors de la thread graphique (EDT).");
      }
      
      // Fermer la boîte de dialogue si un mode de sortie est activé
      if (modele.isModeSortie()) {
        cacher();
        return;
      }
      
      // Rafraîchir les données
      rafraichirComposantsAvecEnfants();
      
      // Afficher le curseur par défaut
      // Utile pour remettre le curseur standard suite au chargement des données
      setCursor(Cursor.getDefaultCursor());
    }
    catch (Exception exception) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Rafraichir les données des composants graphiques de la vue et des vues enfants.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public void rafraichirComposantsAvecEnfants() {
    try {
      // Activer la protection contre la réentrance
      if (Trace.isModeDebug() && protectionReentrance) {
        throw new MessageErreurException(
            "La méthode " + getClass().getSimpleName() + ".rafraichir() a été appelé en cours de rafraîchissement de l'écran. "
                + "Cela a provoqué une réeentrance. Protéger les méthodes évènementielles avec isEvenementsActifs() pour éviter cela.");
      }
      
      // Activer la protection contre la réentrance
      protectionReentrance = true;
      
      // Désactiver la gestion des évènements (avant initialiserComposants et rafraichir).
      evenementsActifs = false;
      
      // Tracer
      if (traceActive) {
        Trace.debug(getClass(), "rafraichir");
      }
      
      // Appeler la méthode rafraichir() de la vue
      rafraichir();
      
      // Rafraichir la vue enfant active
      // A faire après le rafraichissement de la vue parent car la vue parent peut changer la vue active lors de son rafraichissement
      InterfaceVue<?> vueEnfantActive = getVueEnfantActive();
      if (vueEnfantActive != null) {
        vueEnfantActive.rafraichirComposantsAvecEnfants();
      }
    }
    catch (Exception e) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(e);
    }
    finally {
      // Réactiver la gestion des évènements
      evenementsActifs = true;
      
      // Désactiver la protection contre la réentrance avant le chargement des données
      protectionReentrance = false;
    }
  }
  
  /**
   * Cacher la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void cacher() {
    setVisible(false);
    dispose();
  }
  
  /**
   * Modèle associé à la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final M getModele() {
    return modele;
  }
  
  /**
   * Ajouter une vue enfant à cette vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void ajouterVueEnfant(InterfaceCleVue pCle, InterfaceVue<?> pVueEnfant) {
    // Vérifier les paramètres
    if (pCle == null) {
      throw new MessageErreurException("Impossible d'ajouter la vue enfante car sa clé est invalide.");
    }
    if (pVueEnfant == null) {
      throw new MessageErreurException("Impossible d'ajouter la vue enfante car elle est invalide.");
    }
    if (listeVueEnfant.containsKey(pCle)) {
      throw new MessageErreurException(
          "Impossible d'ajouter la vue enfante car sa clé est déjà présente dans la liste des vues enfantes.");
    }
    if (listeVueEnfant.containsValue(pVueEnfant)) {
      throw new MessageErreurException("Impossible d'ajouter la vue enfante car elle est déjà présente dans la liste des vues enfantes.");
    }
    if (pVueEnfant.getModele() != getModele()) {
      throw new MessageErreurException("Impossible d'ajouter la vue enfante car son modèle est différent de celui de la vue parente.");
    }

    // Ajouter la vue enfant à la liste de la vue parent
    listeVueEnfant.put(pCle, pVueEnfant);
    
    // Renseigner la vue parent de l'enfant
    if (pVueEnfant.getVueParent() != this) {
      pVueEnfant.setVueParent(this);
    }
  }
  
  /**
   * Vue parente de la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final InterfaceVue<?> getVueParent() {
    return vueParent;
  }
  
  /**
   * Définir la vue parente d'une vue enfant.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void setVueParent(InterfaceVue<?> pVueParent) {
    // Vérifier les paramètres
    if (pVueParent == null) {
      throw new MessageErreurException("Impossible de renseigner la vue parente car elle est invalide.");
    }
    if (vueParent != null) {
      throw new MessageErreurException("Impossible de renseigner la vue parente car la vue enfante a déjà une vue parente.");
    }
    
    // Renseigner la vue parent de l'enfant
    vueParent = pVueParent;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes de l'interface Observer
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Mettre à jour les données de la vue.
   * Cette méthode est appelée lorsque les données du modèle évolue et qu'il notifie ces observateurs.
   * Si le mode de sortie est renseigné, la boîte de dialogue se ferme.
   */
  @Override
  public final void update(Observable o, Object arg) {
    rafraichirComposants();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rechercher une fenêtre active ou visible.
   *
   * @return Fenêtre active.
   */
  private static Frame rechercherFenetreActive() {
    Frame[] frames = JFrame.getFrames();
    for (Frame frame : frames) {
      if (frame.isActive()) {
        return frame;
      }
    }
    for (Frame frame : frames) {
      if (frame.isVisible()) {
        return frame;
      }
    }
    return null;
  }
  
  /**
   * Fournir la vue enfant active.
   *
   * L'information est détenue par le modèle. Cette méthode interroge le modèle pour récupérer la clé de la vue active et recherche
   * la vue correspondante à la clé dans la liste. Noter que la vue parente et les vues enfantes partagent le même modèle si
   * bien qu'il est possible que la vue active ne soit pas trouvée dans cette vue car elle serait dans une autre.
   *
   * @return Vue enfant active (null si aucune)
   */
  private final InterfaceVue<?> getVueEnfantActive() {
    // Récupérer l'identifiant de la vue active
    InterfaceCleVue interfaceCleVue = modele.getCleVueEnfantActive();
    if (interfaceCleVue == null) {
      return null;
    }

    // Récupérer l'interface de la vue (si présente dans cette vue et pas dans une autre)
    return listeVueEnfant.get(interfaceCleVue);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Indiquer si les évènements doivent être traités.
   *
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs && getModele().isDonneesChargees();
  }
  
  /**
   * Vérifier si les données du modèle sont chargées.
   *
   * Lors de l'affichage initial, la valeur est mise à false avant initialiserDonnees() et mise à true après chargerDonnees().
   * Lors des chargements ultérieurs de données, la valeur est mise à false avant l'appel à chargerDonnees() et remise à true lorsque
   * chargerDonnees() est terminé.
   *
   * @return true=les données du modèles sont chargées, false=les données du modèle ne soint pas chargées.
   */
  public final boolean isDonneesChargees() {
    return modele.isDonneesChargees();
  }
}
