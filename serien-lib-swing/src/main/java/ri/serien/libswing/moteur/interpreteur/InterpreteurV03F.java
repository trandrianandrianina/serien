/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.interpreteur;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

/**
 * Traiter les messages envoyés dans le champ V03F dans de nombreux programmes RPG.
 *
 * Le champ RPG V03F contient souvent des informations complémentaires concernant la vie de la fiche, par exemple "C:27.02.20
 * M:01.06.20" fourni la date de créatino et la date de modification. Les programmes RPG utilisent aussi le champ V03F pour envoyer
 * des informations complémentaires sur les erreurs.
 *
 * Comme nous ne sommes pas certains de la qualité des messages envoyés par les programmes RPG, il a été décidé d'ignorer tout sauf les
 * informations clairement identifiée dans cette classe. Le but final est d'avoir un message d'information le plus clair possible.
 */
public class InterpreteurV03F {
  // Formats de date
  private static final String REGEX_DATE_LONGUE = ":?([0-9]{2})(\\p{Punct}?)([0-9]{2})(\\p{Punct}?)([0-9]{2})";
  private static final String REGEX_DATE_COURTE = ":?([0-9]{2})(\\p{Punct}?)([0-9]{2})";
  
  // Date de création, côtés vente et achat
  private static final String REGEX_DATE_CREATION_LONGUE = "C:?" + REGEX_DATE_LONGUE;
  private static final String REGEX_DATE_CREATION_COURTE = "C:?" + REGEX_DATE_COURTE;
  
  // Date de modification, côtés vente et achat
  private static final String REGEX_DATE_MODIFICATION_LONGUE = "M:?" + REGEX_DATE_LONGUE;
  private static final String REGEX_DATE_MODIFICATION_COURTE = "M:?" + REGEX_DATE_COURTE;
  
  // Date de transfert, uniquement côté vente.
  private static final String REGEX_DATE_TRANSFERT_LONGUE = "T:" + REGEX_DATE_LONGUE;
  private static final String REGEX_DATE_TRANSFERT_COURTE = "T:" + REGEX_DATE_COURTE;
  
  // Date de validation, côtés vente et achat
  private static final String REGEX_DATE_VALIDATION_LONGUE = "H:?" + REGEX_DATE_LONGUE;
  private static final String REGEX_DATE_VALIDATION_COURTE = "H:?" + REGEX_DATE_COURTE;
  
  // Date de réservation, uniquement côté vente.
  private static final String REGEX_DATE_RESERVATION_LONGUE = "R:" + REGEX_DATE_LONGUE;
  private static final String REGEX_DATE_RESERVATION_COURTE = "R:" + REGEX_DATE_COURTE;
  
  // Date de réception, uniquement côté achat.
  private static final String REGEX_DATE_RECEPTION_LONGUE = "R" + REGEX_DATE_LONGUE;
  private static final String REGEX_DATE_RECEPTION_COURTE = "R" + REGEX_DATE_COURTE;
  
  // Date d'expédition, uniquement côté vente
  private static final String REGEX_DATE_EXPEDITION_LONGUE = "E:" + REGEX_DATE_LONGUE;
  private static final String REGEX_DATE_EXPEDITION_COURTE = "E:" + REGEX_DATE_COURTE;
  
  // Informations de facturation côté vente
  private static final String REGEX_INFORMATION_FACTURATION_VENTE_LONG = "F:" + REGEX_DATE_LONGUE + "/([0-9]{7})";
  private static final String REGEX_INFORMATION_FACTURATION_VENTE_COURT = "F:" + REGEX_DATE_COURTE + "/([0-9]{7})";
  private static final String REGEX_DATE_FACTURATION_LONGUE = "F:" + REGEX_DATE_LONGUE;
  private static final String REGEX_DATE_FACTURATION_COURTE = "F:" + REGEX_DATE_COURTE;
  
  // Informations de facturation côté achat
  private static final String REGEX_INFORMATION_FACTURATION_ACHAT = "F/([0-9]{6})";
  
  // La liste des codes d'options pour les boutons par exemple HOM COR ANN
  private static final String REGEX_LISTE_CODE_OPTION = "(?:[A-Z]{3}(\\s|$)){1,}";

  private static final String SEPARATION_MESSAGE_FORMATE = " - ";
  private static final String SEPARATION_DATE = "/";
  
  private String message = null;
  private String dateCreation = null;
  private String dateModification = null;
  private String dateTransfert = null;
  private String dateValidation = null;
  private String dateReservation = null;
  private String dateReception = null;
  private String dateExpedition = null;
  private String dateFacturation = null;
  private String numeroFacture = null;
  private String listeCodeOption = null;
  
  /**
   * Constructeur par défaut.
   */
  public InterpreteurV03F() {
  }
  
  /**
   * Constructeur prenant en entrée un message issu du champ V03F.
   * @param pMessage Message issu du champ V03F.
   */
  public InterpreteurV03F(String pMessage) {
    setMessage(pMessage);
  }
  
  /**
   * Message V03F non modifié.
   */
  public String getMessage() {
    return message;
  }
  
  /**
   * Modifier le message V03F.
   * @param pMessage Message V03F.
   */
  public void setMessage(String pMessage) {
    // Vérifier si la valeur a changé
    if (Constantes.equals(message, pMessage)) {
      return;
    }
    
    // Mettre à jour le nouveau message
    message = pMessage;
    
    // Rechercher les informations connues dans le message
    // Ce n'est pas grave s'il y a une erreur lors de la lecture de ce champ.
    try {
      extraireDateCreation();
      extraireDateModification();
      extraireDateTransfert();
      extraireDateValidation();
      extraireDateReservation();
      extraireDateExpedition();
      extraireDateReception();
      extraireInformationFacturation();
      extraireListeCodeOption();
    }
    catch (Exception e) {
      // Ajouter une erreur dans les traces
      Trace.erreur(e, "Erreur lors de l'interprétation du champs V03F");
    }
  }
  
  /**
   * Retourner une date présente dans le message.
   * Elles doivent être présente sous une des formes suivantes :
   * - Prefixe+JJ+Séparateur+MM+Séparateur+AAAA
   * - Prefixe+JJ+Séparateur+MM+Séparateur+AA
   *
   * @param pRegex Prefixe de la date (par exemple "C:").
   * @return Date.
   */
  private String extraireDate(String pRegex) {
    // Vérifier la présence d'un message
    if (message == null || message.isEmpty() || pRegex == null) {
      return null;
    }
    
    // Rechercher la date dans le message
    try {
      // Rechercher la date a u format
      Pattern pattern = Pattern.compile(pRegex);
      Matcher matcher = pattern.matcher(message);
      if (!matcher.find()) {
        return null;
      }
      
      // Traiter le cas d'une date longue
      if (matcher.groupCount() == 5) {
        // Extraire le jour
        String jour = matcher.group(1);
        
        // Extraire le mois
        String mois = matcher.group(3);
        
        // Extraire l'année et la covnertir en 4 chiffres
        Integer annee = Integer.parseInt(matcher.group(5));
        if (annee < 70) {
          annee += 2000;
        }
        else {
          annee += 1000;
        }
        
        // Construire la date
        return jour + SEPARATION_DATE + mois + SEPARATION_DATE + annee;
      }
      // Traiter le cas d'une date courte
      else if (matcher.groupCount() == 3) {
        // Extraire le jour
        String jour = matcher.group(1);
        
        // Extraire le mois
        String mois = matcher.group(3);
        
        // Construire la date
        return jour + SEPARATION_DATE + mois;
      }
      
    }
    catch (PatternSyntaxException pse) {
      Trace.alerte("Erreur lors de l'interprétation du message V03F : " + message);
    }
    
    return null;
  }
  
  /**
   * Retourner la date de création (si elle est présente dans le message).
   * Dans les fiches permanentes, le champ V03F contient 'C:' devant la date (exemple C:03.11.20).
   * Côté vente, le champ V03F contient 'C:' devant la date (exemple : C:28.09 E:28.09).
   * Côté achat, le champ V03F contient 'C' devant la date (exemple : C24.09 R24.09).
   */
  private void extraireDateCreation() {
    dateCreation = extraireDate(REGEX_DATE_CREATION_LONGUE);
    if (dateCreation == null) {
      dateCreation = extraireDate(REGEX_DATE_CREATION_COURTE);
    }
  }
  
  /**
   * Retourner la date de modification (si elle est présente dans le message).
   * Côté vente, le champ V03F contient 'M:' devant la date.
   * Côté achat, le champ V03F contient 'M' devant la date.
   */
  private void extraireDateModification() {
    dateModification = extraireDate(REGEX_DATE_MODIFICATION_LONGUE);
    if (dateModification == null) {
      dateModification = extraireDate(REGEX_DATE_MODIFICATION_COURTE);
    }
  }
  
  /**
   * Retourner la date de transfert (si elle est présente dans le message).
   * Côté vente, le champ V03F contient 'T:' devant la date.
   * Pas utilisé côté achat (jusqu'à preuve du contraire).
   */
  private void extraireDateTransfert() {
    dateTransfert = extraireDate(REGEX_DATE_TRANSFERT_LONGUE);
    if (dateTransfert == null) {
      dateTransfert = extraireDate(REGEX_DATE_TRANSFERT_COURTE);
    }
  }
  
  /**
   * Retourner la date de validation (si elle est présente dans le message).
   * Côté vente, le champ V03F contient 'H:' devant la date (exemple : H:28.09).
   * Côté achat, le champ V03F contient 'H' devant la date (exemple : H24.09).
   */
  private void extraireDateValidation() {
    dateValidation = extraireDate(REGEX_DATE_VALIDATION_LONGUE);
    if (dateValidation == null) {
      dateValidation = extraireDate(REGEX_DATE_VALIDATION_COURTE);
    }
  }
  
  /**
   * Retourner la date de réservation (si elle est présente dans le message).
   * Côté vente, le champ V03F cont ient 'R:' devant la date.
   * La lettre 'R' est utilisée pour la date de réception côté achat.
   */
  private void extraireDateReservation() {
    dateReservation = extraireDate(REGEX_DATE_RESERVATION_LONGUE);
    if (dateReservation == null) {
      dateReservation = extraireDate(REGEX_DATE_RESERVATION_COURTE);
    }
  }
  
  /**
   * Retourner la date de réception (si elle est présente dans le message).
   * Côté achat, le champ V03F contient 'R' devant la date (exemple : C24.09 R24.09).
   * La lettre 'R' est utilisée pour la date de réservation côté vente.
   */
  private void extraireDateReception() {
    dateReception = extraireDate(REGEX_DATE_RECEPTION_LONGUE);
    if (dateReception == null) {
      dateReception = extraireDate(REGEX_DATE_RECEPTION_COURTE);
    }
  }
  
  /**
   * Retourner la date d'expédition (si elle est présente dans le message).
   * Côté vente, le champ V03F contient 'E:' devant la date (exemple : C:28.09 E:28.09).
   * Pas utilisé côté achat (jusqu'à preuve du contraire).
   */
  private void extraireDateExpedition() {
    dateExpedition = extraireDate(REGEX_DATE_EXPEDITION_LONGUE);
    if (dateExpedition == null) {
      dateExpedition = extraireDate(REGEX_DATE_EXPEDITION_COURTE);
    }
  }
  
  /**
   * Extraire les informations de facturation présentes dans le message.
   * Elles doivent être présente sous une des formes suivantes :
   * - F:JJ+Séparateur+MM+Séparateur+AAAA/NNNNNNN
   * - F:JJ+Séparateur+MM+Séparateur+AA/NNNNNNN
   * - F:JJ+Séparateur+MM+Séparateur+AAAA
   * - F:JJ+Séparateur+MM+Séparateur+AA
   *
   * La lettre 'F:' suivi de la date et du numéro de facture est utilisée côté vente (exemple : C:28.09 F:28.09/0167031 C)
   * La lettre 'F' suivi du numéro de facture est utilisée côté achat (exemple : C25.09 R25.09 F/000003).
   */
  private void extraireInformationFacturation() {
    // Effacer les informations précédentes
    dateFacturation = null;
    numeroFacture = null;
    
    // Vérifier la présence d'un message
    if (message == null || message.isEmpty()) {
      return;
    }
    
    try {
      // Rechercher les informations de facturation avec la date au format long suivie du numéor de facture
      Pattern pattern = Pattern.compile(REGEX_INFORMATION_FACTURATION_VENTE_LONG);
      Matcher matcher = pattern.matcher(message);
      if (matcher.find() && matcher.groupCount() == 6) {
        // Extraire le jour
        String jour = matcher.group(1);
        
        // Extraire le mois
        String mois = matcher.group(3);
        
        // Extraire l'année et la covnertir en 4 ch iffres
        Integer annee = Integer.parseInt(matcher.group(5));
        if (annee < 70) {
          annee += 2000;
        }
        else {
          annee += 1000;
        }
        
        // Construire la date
        dateFacturation = jour + "/" + mois + "/" + annee;
        
        // Récupérer le numéro de facture
        numeroFacture = matcher.group(6);
        return;
      }
      
      // Rechercher les informations de facturation avec la date au format court suivie du numéro de facture
      pattern = Pattern.compile(REGEX_INFORMATION_FACTURATION_VENTE_COURT);
      matcher = pattern.matcher(message);
      if (matcher.find() && matcher.groupCount() == 4) {
        // Extraire le jour
        String jour = matcher.group(1);
        
        // Extraire le mois
        String mois = matcher.group(3);
        
        // Construire la date
        dateFacturation = jour + "/" + mois;
        
        // Récupérer le numéro de facture
        numeroFacture = matcher.group(4);
        return;
      }
      
      // Rechercher le numéro de facture sans date
      pattern = Pattern.compile(REGEX_INFORMATION_FACTURATION_ACHAT);
      matcher = pattern.matcher(message);
      if (matcher.find() && matcher.groupCount() == 1) {
        // Récupérer le numéro de facture
        numeroFacture = matcher.group(1);
        return;
      }
    }
    catch (PatternSyntaxException pse) {
      Trace.alerte("Erreur lors de l'interprétation du message V03F : " + message);
    }
    
    // Rechercher uniquement la date de facturation
    dateFacturation = extraireDate(REGEX_DATE_FACTURATION_LONGUE);
    if (dateFacturation == null) {
      dateFacturation = extraireDate(REGEX_DATE_FACTURATION_COURTE);
    }
  }
  
  /**
   * Retourne la liste des codes d'options des formats F1.
   * Côté trésorerie, le champ V03F soit :
   * Option incorrecte->HOM COR ATT ANN
   * Option incorrecte->COR ANN
   * Option incorrecte->COR ATT ANN
   * Options : COR ATT ANN
   * Options : HOM COR ATT ANN
   * Options : COR ANN
   */
  private void extraireListeCodeOption() {
    listeCodeOption = null;

    try {
      // Rechercher les codes de 3 lettres majuscules
      Pattern pattern = Pattern.compile(REGEX_LISTE_CODE_OPTION);
      Matcher matcher = pattern.matcher(message);
      if (matcher.find()) {
        listeCodeOption = matcher.group(0);
      }
    }
    catch (PatternSyntaxException pse) {
      Trace.alerte("Erreur lors de l'interprétation du message V03F : " + message);
    }
  }

  /**
   * Récupérer une version formatée du message V03F.
   * @return Message formaté.
   */
  public String getMessageFormate() {
    StringBuffer messageFormate = new StringBuffer("");
    completerMessage(messageFormate, "Créé le ", dateCreation);
    completerMessage(messageFormate, "Modifié le ", dateModification);
    completerMessage(messageFormate, "Transféré le ", dateTransfert);
    completerMessage(messageFormate, "Validé le ", dateValidation);
    completerMessage(messageFormate, "Réservé le ", dateReservation);
    completerMessage(messageFormate, "Expédié le ", dateExpedition);
    completerMessage(messageFormate, "Réceptionné le ", dateReception);
    completerMessage(messageFormate, "Facturé le ", dateFacturation);
    completerMessage(messageFormate, "Facture n°", numeroFacture);
    return messageFormate.toString();
  }
  
  /**
   * Compléter le message formaté avec un chaîne de caractères.
   * Si le message comportait déjà des informations, un caractère de transition '-' est ajouté. Le message n'est pas modifié si le
   * texte à ajouter est null ou vide.
   *
   * @param pMessage Message à compléter.
   * @param pTexte Chaîne à ajouter au message.
   */
  private void completerMessage(StringBuffer pMessage, String pTexte) {
    // Vérifier les paramètres
    if (pMessage == null || pTexte == null || pTexte.isEmpty()) {
      return;
    }
    
    // Ajouter un séparateur si nécessaire
    if (pMessage.length() > 0) {
      pMessage.append(SEPARATION_MESSAGE_FORMATE);
    }
    
    // Ajouter la chaîne supplémentaire
    pMessage.append(pTexte);
    
  }
  
  /**
   * Compléter le message formaté avec une texte précédé d'un préfixe.
   * Si le message comportait déjà des informations, un caractère de transition '-' est ajouté. Le message n'est pas modifié si le
   * texte ou la date est null ou vide.
   *
   * @param pMessage Message à compléter.
   * @param pPrefixe Préfixe à ajouter devant.
   * @param pTexte Date à ajouter.
   * @param pChaine Chaîne à ajouter au message.
   */
  private void completerMessage(StringBuffer pMessage, String pPrefixe, String pTexte) {
    // Vérifier les paramètres
    if (pMessage == null || pTexte == null) {
      return;
    }
    
    // Ajouter la date précédée du préfixe
    if (pPrefixe != null) {
      completerMessage(pMessage, pPrefixe + pTexte);
    }
    else {
      completerMessage(pMessage, pTexte);
    }
    
  }
  
  /**
   * Date de création issue du message V03F.
   * Sous forme de chaîne de caractères car il peut y avoir des dates sans années (JJ/MM).
   */
  public String getDateCreation() {
    return dateCreation;
  }
  
  /**
   * Date de modification issue du message V03F.
   * Sous forme de chaîne de caractères car il peut y avoir des dates sans années (JJ/MM).
   */
  public String getDateModification() {
    return dateModification;
  }
  
  /**
   * Date de dernier transfert issue du message V03F.
   * Sous forme de chaîne de caractères car il peut y avoir des dates sans années (JJ/MM).
   */
  public String getDateTransfert() {
    return dateTransfert;
  }
  
  /**
   * Date de validation issue du message V03F.
   * Sous forme de chaîne de caractères car il peut y avoir des dates sans années (JJ/MM).
   */
  public String getDateValidation() {
    return dateValidation;
  }
  
  /**
   * Date de réservation issue du message V03F.
   * Sous forme de chaîne de caractères car il peut y avoir des dates sans années (JJ/MM).
   */
  public String getDateReservation() {
    return dateReservation;
  }
  
  /**
   * Date de réception issue du message V03F.
   * Sous forme de chaîne de caractères car il peut y avoir des dates sans années (JJ/MM).
   */
  public String getDateReception() {
    return dateReception;
  }
  
  /**
   * Date d'expédition issue du message V03F.
   * Sous forme de chaîne de caractères car il peut y avoir des dates sans années (JJ/MM).
   */
  public String getDateExpedition() {
    return dateExpedition;
  }
  
  /**
   * Date de facturation issue du message V03F.
   * Sous forme de chaîne de caractères car il peut y avoir des dates sans années (JJ/MM).
   */
  public String getDateFacturation() {
    return dateFacturation;
  }
  
  /**
   * Chaine contenant la liste des codes options contenu dans le V03F.
   * @return
   */
  public String getListeCodeOption() {
    return listeCodeOption;
  }
}
