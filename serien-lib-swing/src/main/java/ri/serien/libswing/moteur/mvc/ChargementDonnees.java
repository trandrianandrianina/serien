/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc;

import javax.swing.SwingWorker;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;

/**
 * Charge les données en dehors de la thread graphique.
 * Les erreurs qui surviennent pendant le chargement des données sont affichées dans une boîte de dialogue (dans la thread graphique).
 */
public class ChargementDonnees extends SwingWorker<Exception, Integer> {
  private int tempsChargement = 0;
  private final InterfaceModele modele;

  public ChargementDonnees(InterfaceModele pModele) {
    modele = pModele;
  }

  @Override
  protected Exception doInBackground() throws Exception {
    // Charger les données
    try {
      modele.chargerDonnees();

      // Ajouter une attente artificielle à des fins de mise au point (attends TEMPS_CHARGEMENT en millisecondes)
      if (Trace.isModeDebug() && tempsChargement > 0 && tempsChargement < 60000) {
        try {
          Thread.sleep(tempsChargement);
        }
        catch (InterruptedException e) {
          // RAS
        }
      }

      return null;
    }
    catch (Exception e) {
      return e;
    }
  }

  @Override
  public void done() {
    try {
      // Récupération du résultat du chargement des données
      Exception e = get();

      // Indiquer au modèle que les données sont chargées.
      modele.setDonneesChargees(true);

      // Afficher le message d'erreur s'il y en a un
      if (e != null) {
        DialogueErreur.afficher(e);
      }

      // Rafraichir l'écran avec les données
      modele.rafraichir();
    }
    catch (Exception e) {
      // Il y a eu un problème pendant le rafraîchissement des données
      DialogueErreur.afficher(e);
    }
  }
}
