/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.composant;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.event.EventListenerList;

import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * SNComposant est la classe mère de tous les composants métiers (composant de sélection, de saisie ou d'affichage). Chaque composant
 * métier doit hériter de la classe SNComposant ou d'une de ses classes dérivées.
 *
 * Voici la description des classes SNComposant disponibles :
 *
 * - SNComposant : adapté pour les composants de saisie qui n'ont pas à interagir avec la base de données. Par exemple, le composant de
 * saisie d'une plage de dates.
 *
 * - SNComposantAvecSession : adapté pour les composants qui nécessitent un accès à la base de données mais qui n'ont pas besoin de
 * connaître l'établissement en cours. Par exemple, le composant de sélection d'un contact.
 *
 * - SNComposantAvecEtablissement : adapté pour les composants qui nécessitent un accès à la base de données et qui ont besoin de
 * connaître
 * l'établissement courant. Par exemple, le composant de sélection d'un client.
 *
 *
 * L'apport principal de la classe SNComposant est de mettre à disposition un système de notifications via l'interface
 * InterfaceSNComposantListener. La classe JPanel ne propose pas de notifications pertinentes pour informer la vue qu'une saisie a été
 * effectuée dans le composant. La méthode fireValueChanged() permet de prévenir les abonnés que le composant a changé de valeur.
 *
 */

abstract public class SNComposant extends JPanel {
  private final EventListenerList listeners = new EventListenerList();
  private boolean forcerRechargement = false;

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Constructeur par défaut.
   */
  public SNComposant() {
    setName("SNPanelComposant");
    setOpaque(false);
    setLayout(new BorderLayout());
    setFont(SNCharteGraphique.POLICE_STANDARD);
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Enregistrer un nouveau listener de type InterfaceSNComposantListener pour être notifié lorsque la valeur du composant change.
   */
  public void addSNComposantListener(InterfaceSNComposantListener listener) {
    listeners.add(InterfaceSNComposantListener.class, listener);
  }

  /**
   * Obtenir la liste des listeners de type InterfaceSNComposantListener.
   */
  public InterfaceSNComposantListener[] getSNComposantListener() {
    return listeners.getListeners(InterfaceSNComposantListener.class);
  }

  /**
   * Supprimer un listener de type InterfaceSNComposantListener.
   */
  public void removeSNComposantListener(InterfaceSNComposantListener aListener) {
    listeners.remove(InterfaceSNComposantListener.class, aListener);
  }
  
  /**
   * Indique s'il faut effectuer un rechargement des données lors du prochain appel à la méthode charger().
   */
  public boolean isForcerRechargement() {
    return forcerRechargement;
  }
  
  /**
   * Demander le rechargement des données lors du prochain appel à la méthode charger().
   */
  public void setForcerRechargement(boolean pForcerRechargement) {
    forcerRechargement = pForcerRechargement;
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes protégées
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Envoyer une notification pour signaler que la valeur du composant a changé.
   * La notification est envoyée à tous les listeners de type InterfaceSNComposantListener.
   */
  protected void fireValueChanged() {
    for (InterfaceSNComposantListener listener : getSNComposantListener()) {
      listener.valueChanged(new SNComposantEvent(this));
    }
  }
}
