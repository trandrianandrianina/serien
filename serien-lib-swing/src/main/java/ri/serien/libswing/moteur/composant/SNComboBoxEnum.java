/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.composant;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.UIManager;

import ri.serien.libcommun.commun.InterfaceEnumCritereSelection;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Classe abstraite des composants de sélection d'une valeur d'une Enum via une liste déroulante.
 *
 * Ce composant doit être utilisé pour tout affichage et sélection par liste déroulante d'une énumération dans le logiciel.
 * Tous les composants de sélection d'énumération de type liste déroulante doivent dériver de cette classe.
 *
 * Cette classe dérive elle-même de SNComposant, ce qui lui permet de bénéficier du mécanisme d'abonnement aux événements lorsque la
 * valeur du composant change.
 *
 * La taille du composant est par défaut celle définie dans SNCharteGraphique.
 *
 * Utilisation :
 * - Créer un nouveau composant de sélection qui étend SNComboBoxEnum.
 * - Implémenter les classes obligatoires.
 * - Utiliser charger(I[] pListeValeurEnum) pour charger les données.
 *
 * Voir un exemple d'implémentation dans SNStatutFlux.
 */
public abstract class SNComboBoxEnum<I extends InterfaceEnumCritereSelection> extends SNComposant {
  private I[] listeSelectionnable;
  private I enumSelection = null;
  private Object valeurPersisteEnumSelection = null;
  
  private static final String LIBELLE_AUCUN = "Aucun";
  private static final String LIBELLE_VIDE = " ";
  
  private boolean rafraichissementEnCours = false;
  private boolean tousAutorise = false;
  private boolean aucunAutorise = false;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Ne pas déclencher d'évènements ItemStateChanged pour chaque objet métier parcouru dans la liste déroulante avec le clavier.
   * Seul un appui sur la touche entrée ou un clic souris sur un objet métier déclenche l'évènement ItemStateChanged.
   * La propriété "ComboBox.noActionOnKeyNavigation" ne fonctionne qu'à partir de Java 1.8.
   */
  {
    UIManager.getLookAndFeelDefaults().put("ComboBox.noActionOnKeyNavigation", true);
  }
  
  /**
   * Constructeur par défaut.
   */
  public SNComboBoxEnum() {
    super();
    initComponents();
    
    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    if (SNCharteGraphique.TAILLE_COMPOSANT_SELECTION.width >= getWidth()) {
      setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    }
    else {
      setPreferredSize(new Dimension(getWidth(), SNCharteGraphique.TAILLE_COMPOSANT_SELECTION.height));
    }
    
    /**
     * Ne pas déclencher d'évènements ItemStateChanged pour chaque objet métier parcouru dans la liste déroulante avec le clavier.
     * Seul un appui sur la touche entrée ou un clic souris sur un objet métier déclenche l'évènement ItemStateChanged.
     * La propriété "ComboBox.noActionOnKeyNavigation" ne fonctionne qu'à partir de Java 1.8.
     */
    cbSelection.putClientProperty("ComboBox.noActionOnKeyNavigation", Boolean.TRUE);
    
    // Ajouter un évènements sur la liste déroulante
    cbSelection.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        cbSelectionItemStateChanged(e);
      }
    });
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Charger la liste des valeurs de l'énumération dans la combobox.
   * La liste n'est pas rechargée si les paramètres n'ont pas changés, sauf si un rafraîchissement explicite a été demandé lors de l'appel
   * de la méthode.
   * @param pListeValeurEnum La liste des valeurs de l'énumération.
   */
  public void charger(I[] pListeValeurEnum) {
    // Tester si la liste a changé.
    if (Constantes.equals(pListeValeurEnum, listeSelectionnable)) {
      return;
    }
    listeSelectionnable = pListeValeurEnum;
    cbSelection.removeAllItems();
    cbSelection.addItem(getLibelleAucuneSelection());
    for (I objetSelectionnable : listeSelectionnable) {
      cbSelection.addItem(objetSelectionnable);
    }
  }
  
  /**
   * Indiquer si la liste déroulante permet de sélectionner le choix "Tous".
   * Il n'est pas possible d'autoriser "Tous" ou "Aucun" simultanément.
   * @param pTousAutorise Si ce paramètre est à true, la liste déroulante propose le choix "Tous".
   */
  public void setTousAutorise(boolean pTousAutorise) {
    // Tester si la valeur a changée
    if (tousAutorise == pTousAutorise) {
      return;
    }
    
    // Mettre à jour la valeur
    tousAutorise = pTousAutorise;
    if (tousAutorise) {
      aucunAutorise = false;
    }
    
    // Rafraichir l'affichage de la liste déroulante pour ajouter ou supprimer le "Tous"
    rafraichirAffichageListeDeroulante();
  }
  
  /**
   * Indiquer si la liste déroulante permet de sélectionner le choix "Aucun".
   * Il n'est pas possible d'autoriser "Tous" ou "Aucun" simultanément.
   * @param pAucunAutorise Si ce paramètre est à true, la liste déroulante propose le choix "Aucun".
   */
  public void setAucunAutorise(boolean pAucunAutorise) {
    // Tester si la valeur a changé
    if (aucunAutorise == pAucunAutorise) {
      return;
    }
    
    // Mettre à jour la valeur
    aucunAutorise = pAucunAutorise;
    if (aucunAutorise) {
      tousAutorise = false;
    }
    
    // Rafraichir l'affichage de la liste déroulante pour ajouter ou supprimer le "Aucun"
    rafraichirAffichageListeDeroulante();
  }
  
  /**
   * Sélectionner une énumération dans la liste.
   * @param pEnumSelectionne L'énumération à sélectionner.
   */
  public void setSelection(I pEnumSelectionne) {
    // Contrôler que la valeur a changé.
    if (pEnumSelectionne == enumSelection) {
      return;
    }
    enumSelection = pEnumSelectionne;

    // Si l'énumération est à null, la valeur persistée est à null aussi.
    if (enumSelection == null) {
      valeurPersisteEnumSelection = null;
      return;
    }
    // Sinon on récupère la valeur persistée correspondant.
    valeurPersisteEnumSelection = enumSelection.getValeurPersiste();

    // Resélectionner l'objet sélectionné.
    rafraichirAffichageSelection();
  }
  
  /**
   * Retourner l'énumération sélectionnée (null si aucune sélection).
   * @return L'énumération sélectionnée.
   */
  public I getSelection() {
    return enumSelection;
  }
  
  /**
   * Sélectionner une enumération par sa valeur persistée.
   * @param pValeurPersiste La valeur persistée.
   */
  public void setValeurPersisteSelection(Object pValeurPersiste) {
    // Vérifier si la sélection a changé.
    if (Constantes.equals(pValeurPersiste, valeurPersisteEnumSelection)) {
      return;
    }
    
    enumSelection = (I) enumSelection.getEnumByValeurPersiste(pValeurPersiste);
    if (enumSelection == null) {
      valeurPersisteEnumSelection = null;
      return;
    }
    valeurPersisteEnumSelection = pValeurPersiste;

    // Resélectionner l'objet sélectionné.
    rafraichirAffichageSelection();
  }
  
  /**
   * Retourner la valeur persistée en base correspondant à l'enum sélectionnée.
   * @return
   */
  public Object getValeurPersisteSelection() {
    return valeurPersisteEnumSelection;
  }

  /**
   * Être notifié lorsque la sélection change.
   * Cette méthode est appelée lorsqu'une énumération est sélectionnée dans la liste déroulante.
   * Les classes enfants peuvent la surcharger afin d'intervenir lors de la sélection d'une énumération.
   */
  public void valueChanged() {
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraîchir le contenu de la liste déroulante avec les objets métiers de la liste.
   */
  private void rafraichirAffichageListeDeroulante() {
    // Bloquer les notifications du listener si on est en cours de chargement de la liste
    rafraichissementEnCours = true;
    
    // Mettre à jour la liste
    cbSelection.removeAllItems();
    
    // Ajouter la valeur "Tous" ou "Aucun" si c'est souhaité
    if (tousAutorise || aucunAutorise) {
      cbSelection.addItem(getLibelleAucuneSelection());
    }
    
    // Remplir la liste déroulante à partir de la liste d'objets
    if (listeSelectionnable != null) {
      for (I objetSelectionnable : listeSelectionnable) {
        cbSelection.addItem(objetSelectionnable);
      }
    }
    
    // Resélectionner l'objet sélectionné
    rafraichirAffichageSelection();
    
    // Réactiver les notifications
    rafraichissementEnCours = false;
  }
  
  /**
   * Rafraîchir l'affichage de l'objet métier sélectionné.
   */
  private void rafraichirAffichageSelection() {
    // Tester si aucun objet métier n'est sélectionné
    // Dans ce cas, il faut effacer la sélection
    if (enumSelection == null) {
      // Sélectionner "Tous" ou "Aucun" si cette valeur est autorisée
      if (tousAutorise || aucunAutorise) {
        cbSelection.setSelectedItem(getLibelleAucuneSelection());
      }
      else {
        cbSelection.setSelectedItem(null);
      }
    }
    // Dans ce dernier cas, un objet métier est sélectionné et il faut mettre à jour la liste déroulante en conséquence
    else {
      // Rechercher l'objet métier à sélectionner dans la liste
      I objetTrouve = null;
      if (listeSelectionnable != null) {
        for (I objetSelectionnable : listeSelectionnable) {
          if (objetSelectionnable != null && objetSelectionnable == enumSelection) {
            objetTrouve = objetSelectionnable;
          }
        }
      }
      // Sélectionner l'objet métier car il est présent dans la liste (cas normal)
      if (objetTrouve != null) {
        cbSelection.setSelectedItem(objetTrouve);
      }
    }
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // ---------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Retourner si la valeur "Tous" est autorisée à la sélection.
   * @return True si la liste déroulante permet de sélectionner le choix "Tous", false sinon.
   */
  public boolean isTousAutorise() {
    return tousAutorise;
  }
  
  /**
   * Retourner si la valeur "Aucun" est autorisée à la sélection.
   * @return True si la liste déroulante permet de sélectionner le choix "Aucun", false sinon.
   */
  public boolean isAucunAutorise() {
    return aucunAutorise;
  }
  
  /**
   * Retourner le libellé à employer lorsqu'aucun objet métier n'est sélectionné.
   * @return Le libellé à employer lorsqu'aucun objet métier n'est sélectionné.
   *         " " si rien n'a été défini.
   *         "Tous" si le composant est utilisé en tant que critère de filtrage.
   *         "Aucun" si le composant est utilisé en tant que composant de saisie.
   */
  private String getLibelleAucuneSelection() {
    if (tousAutorise) {
      // Retourner "Tous" si le composant est utilisé en tant que critère de filtrage.
      return SNCharteGraphique.TEXTE_TOUS;
    }
    else if (aucunAutorise) {
      // Retourner "Aucun" si le composant est utilisé en tant que composant de saisi.
      return LIBELLE_AUCUN;
    }
    else {
      // Rien par défaut
      return LIBELLE_VIDE;
    }
  }

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Traiter la sélection d'une nouvelle valeur dans la liste déroulante.
   *
   * Appelée lorsque :
   * - l'utilisateur sélectionne un objet dans la liste déroulante au clavier ou à la souris,
   * - la méthode setSelectedIndex ou setSelectedItem est appelée.
   *
   * Noter qu'on doit avoir : UIManager.getLookAndFeelDefaults().put("ComboBox.noActionOnKeyNavigation", true);
   * afin de pas déclencher d'évènements ItemStateChanged pour chaque objet métier parcouru dans la liste déroulante avec le clavier.
   */
  private void cbSelectionItemStateChanged(ItemEvent e) {
    try {
      // Ne rien faire si un rafraîchissement des valeurs de la liste déroulante est en cours
      if (rafraichissementEnCours) {
        return;
      }
      
      // Ne rien faire si ce n'est pas un évènement de sélection
      if (e.getStateChange() != ItemEvent.SELECTED) {
        return;
      }
      
      // Mémoriser l'objet métier sélectionné et son identifiant si la sélection est un objet métier
      Object objetSelectionne = cbSelection.getSelectedItem();
      if (objetSelectionne instanceof InterfaceEnumCritereSelection) {
        enumSelection = (I) objetSelectionne;
      }
      // Effacer la sélection si la sélection est la valeur "Tous" ou "Aucun"
      else if (objetSelectionne instanceof String && ((String) objetSelectionne).equals(getLibelleAucuneSelection())) {
        enumSelection = null;
      }
      
      // Notifier le composant lui-même
      valueChanged();
      
      // Notifier les listeners
      fireValueChanged();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes auto-générées
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    cbSelection = new SNComboBox();
    
    // ======== this ========
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
    
    // ---- cbSelection ----
    cbSelection.setPreferredSize(new Dimension(150, 29));
    cbSelection.setMaximumSize(new Dimension(150, 30));
    cbSelection.setMinimumSize(new Dimension(150, 30));
    cbSelection.setName("cbSelection");
    add(cbSelection,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNComboBox cbSelection;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
