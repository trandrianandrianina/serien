/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.graphique;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.image.OutilImage;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegrade;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Bandeau de titre situé en haut de tous les écrans du logiciel.
 *
 * Le bandeau comporte un titre à gauche et le logo de l'établissement à droite. Le bandeau est associé à un établissement via les
 * méthodes setIdEtablissement ou setCodeEtablissement().
 *
 * Il présente un fond dégradé de la couleur foncée à gauche vers la couleur claire à droite. Seule la couleur foncée est paramètrable via
 * la méthode setCouleurFoncee(). La couleur claire est déterminée automatiquement à partir de la couleur foncée. De même, la couleur du
 * titre (blanc ou noir) dépend uniquement de la luminosité de la couleur foncée du bandeau. Si la couleur foncée est sombre, le titre
 * est blanc. Si la couleur foncée est claire, le titre est noir.
 *
 */
public class SNBandeauTitre extends SNPanelDegrade {
  // Variables
  private String titre = null;
  private String sousTitre = null;
  private boolean capitaliserPremiereLettre = true;
  private IdEtablissement idEtablissement = null;
  private ImageIcon imageIcon = null;

  /**
   * Constructeur.
   */
  public SNBandeauTitre() {
    super();
    initComponents();
    setCouleurFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_GESCOM);
  }

  /**
   * Rafraichir l'affichage du composant graphique.
   */
  private void rafraichir() {
    rafraichirTitre();
    rafraichirSousTitre();
    rafraichirLogo();
  }

  /**
   * Rafraichir le titre.
   */
  private void rafraichirTitre() {
    // Modifier la couleur du titre en fonction de la couleur effective du bandeau
    if (OutilImage.isCouleurSombre(getCouleurFoncee())) {
      lbTitre.setForeground(Color.WHITE);
    }
    else {
      lbTitre.setForeground(Color.BLACK);
    }

    // Mettre la première lettre du titre en majuscules si c'est demandé
    String titreCorrige;
    if (titre != null && capitaliserPremiereLettre) {
      titreCorrige = Constantes.capitaliserPremiereLettre(titre);
    }
    else {
      titreCorrige = titre;
    }

    // Rafraîchir le titre
    if (titreCorrige != null) {
      lbTitre.setText(titreCorrige);
    }
    else {
      lbTitre.setText("");
    }
  }

  /**
   * Rafraichir le titre.
   */
  private void rafraichirSousTitre() {
    // Modifier la couleur du titre en fonction de la couleur effective du bandeau
    if (OutilImage.isCouleurSombre(getCouleurFoncee())) {
      lbSousTitre.setForeground(Color.WHITE);
    }
    else {
      lbSousTitre.setForeground(Color.BLACK);
    }

    // Modifier le sous-titre s'il vient d'un programme RPG
    String sousTitreCorrige = sousTitre;

    // Rafraîchir le sous-titre
    if (sousTitreCorrige != null) {
      lbSousTitre.setText(sousTitreCorrige);
    }
    else {
      lbSousTitre.setText("");
    }
  }

  /**
   * Rafraichir le logo.
   */
  private void rafraichirLogo() {
    // Rafraîchir le logo
    lbLogo.setIcon(imageIcon);
  }

  /**
   * Changer la couleur foncée du dégradé.
   */
  @Override
  public void setCouleurFoncee(Color pCouleurFoncee) {
    super.setCouleurFoncee(pCouleurFoncee);
    rafraichir();
  }

  /**
   * Titre du bandeau.
   * @return Titre.
   */
  public String getText() {
    return titre;
  }

  /**
   * Modifier le titre du bandeau.
   * @param pTitre Nouveau titre.
   */
  public void setText(String pTitre) {
    // Tester si la valeur a changé
    if (Constantes.equals(titre, pTitre)) {
      return;
    }

    // Modifier le titre
    titre = pTitre;
    rafraichir();
  }

  /**
   * Sous-titre du bandeau.
   */
  public String getSousTitre() {
    return sousTitre;
  }

  /**
   * Modifier le sous-titre du bandeau.
   * @param pSousTitre Nouveau sous-titre.
   */
  public void setSousTitre(String pSousTitre) {
    // Tester si la valeur a changé
    if (Constantes.equals(sousTitre, pSousTitre)) {
      return;
    }

    // Modifier le titre
    sousTitre = pSousTitre;
    rafraichir();
  }

  /**
   * Modifier le sous-titre du bandeau ainsi que son infobulle.
   * @param pSousTitre Nouveau sous-titre.
   */
  public void setSousTitre(String pSousTitre, String pInfoBulleSousTitre) {
    setSousTitre(pSousTitre);
    if (pInfoBulleSousTitre != null && !pInfoBulleSousTitre.isEmpty()) {
      pnlTitre.setToolTipText(pInfoBulleSousTitre);
    }
    else {
      pnlTitre.setToolTipText(null);
    }
  }

  /**
   * Vérifier si le titre est modifié de façon à avoir uniquement sa première lettre en majuscule.
   * @return true=titre modifié, false=titre non modifié.
   */
  public boolean isCapitaliserPremiereLettre() {
    return capitaliserPremiereLettre;
  }

  /**
   * Indiquer si le titre doit être modifié de façon à avoir uniquement sa première lettre en majuscule.
   *
   * Souvent, ce paramètre est mis à true pour les titres issus des écrans RPG car ceux-ci sont fournis entièrement en majuscules.
   * A l'inverse pour les écrans Java, ce paramètre est souveznt mis à false car le titre est correctement formaté et peut contenir
   * des identficiants d'objets métiers. Il n'est pas judicieux de modifier le titre dans ce cas.
   *
   * @param pCapitaliserPremiereLettre true=modifier le titre, false=ne pas modifier le titre.
   */
  public void setCapitaliserPremiereLettre(boolean pCapitaliserPremiereLettre) {
    // Tester si la valeur a changé
    if (capitaliserPremiereLettre == pCapitaliserPremiereLettre) {
      return;
    }

    // Modifier la valeur
    capitaliserPremiereLettre = pCapitaliserPremiereLettre;
    rafraichir();
  }

  /**
   * Retourner l'identfiant de l'établissement associé au bandeau de titre.
   * @return le logo.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }

  /**
   * Modifier l'établissement associé au bandeau de titre.
   * Cela a pour effet d'afficher le logo de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    // Tester si l'établissement a changé
    if (Constantes.equals(idEtablissement, pIdEtablissement)) {
      return;
    }

    // Mémoriser le nouvel établissement
    idEtablissement = pIdEtablissement;

    // Charger le logo de l'établissement
    if (idEtablissement != null) {
      // Récupérer l'image à afficher
      imageIcon = ManagerSessionClient.getInstance().chargerLogoEtablissement(idEtablissement.getCodeEtablissement());

      // Redimensionner le logo à la hauteur du bandeau de titre
      imageIcon = OutilImage.reduireIcone(imageIcon, getPreferredSize().width, getPreferredSize().height);
    }
    else {
      imageIcon = null;
    }

    // Rafraîchir l'affichage
    rafraichir();
  }

  /**
   * Modifier l'établissement associé au bandeau de titre.
   * Cela a pour effet d'afficher le logo de l'établissement.
   * Si on n'a pas de code établissement on n'affiche rien
   */
  public void setCodeEtablissement(String pCodeEtablissement) {
    if (pCodeEtablissement != null && !pCodeEtablissement.trim().isEmpty()) {
      setIdEtablissement(IdEtablissement.getInstance(pCodeEtablissement));
    }
    else {
      setIdEtablissement(null);
    }
  }

  public void setDateCreation(String pDateCreation) {

  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlTitre = new JPanel();
    lbTitre = new JLabel();
    lbSousTitre = new JLabel();
    lbLogo = new JLabel();

    // ======== this ========
    setPreferredSize(new Dimension(950, 55));
    setMinimumSize(new Dimension(0, 55));
    setMaximumSize(new Dimension(2147483647, 55));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 55, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };

    // ======== pnlTitre ========
    {
      pnlTitre.setBorder(new EmptyBorder(5, 15, 5, 5));
      pnlTitre.setOpaque(false);
      pnlTitre.setName("pnlTitre");
      pnlTitre.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlTitre.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlTitre.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlTitre.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
      ((GridBagLayout) pnlTitre.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };

      // ---- lbTitre ----
      lbTitre.setText("Titre");
      lbTitre.setFont(new Font(Font.SANS_SERIF, lbTitre.getFont().getStyle() | Font.BOLD, 20));
      lbTitre.setForeground(Color.white);
      lbTitre.setName("lbTitre");
      pnlTitre.add(lbTitre,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));

      // ---- lbSousTitre ----
      lbSousTitre.setFont(new Font("sansserif", Font.PLAIN, 11));
      lbSousTitre.setForeground(Color.white);
      lbSousTitre.setText("Sous-titre");
      lbSousTitre.setName("lbSousTitre");
      pnlTitre.add(lbSousTitre,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlTitre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 5), 0, 0));

    // ---- lbLogo ----
    lbLogo.setName("lbLogo");
    add(lbLogo,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlTitre;
  private JLabel lbTitre;
  private JLabel lbSousTitre;
  private JLabel lbLogo;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
