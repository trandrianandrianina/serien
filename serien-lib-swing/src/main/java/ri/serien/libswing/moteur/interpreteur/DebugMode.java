/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.interpreteur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.RowFilter;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.RowSpec;
import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioGeneric;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Fenêtre affichant des infos pour le débugage
 */
public class DebugMode extends JFrame {
  // Constantes

  private static final int LARGEUR_COURTE = 180;
  private static final int LARGEUR_GRANDE = 880;

  // Variables
  private String[] titlevalue = { "Variables", "Lg", "Valeurs envoyées par le RPG", "Valeurs envoyées au RPG" };
  private int[] largeurcol = { 70, 35, 300, 300 };
  private boolean large = false;
  private LinkedHashMap<String, DefaultTableModel> listeInformations = new LinkedHashMap<String, DefaultTableModel>();
  private LinkedHashMap<String, String[]> listeInfobulles = new LinkedHashMap<String, String[]>();
  private int compteur = 0;
  private TableRowSorter<TableModel> sorter = null; // Sorter liste des variables
  private Thread thread = null;
  private IdSession idSession = null;

  /**
   * Constructeur.
   */
  public DebugMode(String libelleprg, IdSession pIdSession) {
    initComponents();
    idSession = pIdSession;
    // initIndicateur(indicateurs);
    setSize(LARGEUR_COURTE, 530);

    monitoring();
    int x = Toolkit.getDefaultToolkit().getScreenSize().width - getWidth();
    int y = 0;
    setLocation(x, y);
    if (libelleprg != null) {
      l_libelleprg.setText(libelleprg);
    }
    setVisible(true);

    // Initialisation des infobulles de la liste
    t_Variables.addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        final Point p = e.getPoint();
        Runnable callMAJ = new Runnable() {
          @Override
          public void run() {
            if (listeInfobulles.size() == 0) {
              return;
            }
            int row = t_Variables.rowAtPoint(p);
            t_Variables.setToolTipText(listeInfobulles.get(cb_ListeRcd.getSelectedItem())[row]);
            repaint();
          }
        };
        if (SwingUtilities.isEventDispatchThread()) {
          callMAJ.run();
        }
        else {
          SwingUtilities.invokeLater(callMAJ);
        }
      }
    });
  }

  /**
   * Initialise les données avec un buffer entrant
   * @param afluxrpg
   */
  public void setFluxRpgEntrant(FluxRecord afluxrpg) {
    l_RPG.setText(afluxrpg.getNomPrg());
    l_DSPF.setText(afluxrpg.getNomFormat());
    if (controlCoherenceRecordAndBuffer(afluxrpg)) {
      l_Record.setForeground(Color.BLACK);
      l_Record.setText(afluxrpg.getRecordName());
    }
    else {
      l_Record.setForeground(Color.RED);
      l_Record.setText(afluxrpg.getRecordName() + " <--?!!?");
    }
    if (afluxrpg.getBlocageEcran() == FluxRecord.EXFMT) {
      l_WABLC.setText("EXFMT (" + afluxrpg.getBlocageEcran() + ")");
    }
    else if (afluxrpg.getBlocageEcran() == FluxRecord.WRITE) {
      l_WABLC.setText("WRITE (" + afluxrpg.getBlocageEcran() + ")");
    }
    else {
      l_WABLC.setText("" + afluxrpg.getBlocageEcran());
    }

    l_FMT.setText(afluxrpg.getVFmt());
    initIndicateur(afluxrpg.getIndicateurs());
    initVariablesEntrantes(afluxrpg);
  }

  /**
   * Initialise les données avec un buffer sortant
   * @param afluxrpg
   */
  public void setFluxRpgSortant(FluxRecord afluxrpg) {
    initVariablesSortantes(afluxrpg);
  }

  /**
   * Initialise le nom de la zone qui a le focus
   * @param nomZone
   */
  public void setZoneFocus(String nomZone) {
    l_Focus.setText(nomZone);
  }

  /**
   * Ajoute une touche dans la liste
   * @param touche
   *
   *          public void addTouche(String touche)
   *          {
   *          cb_ListeTouches.get
   *          cb_ListeTouches.addItem(touche);
   *          }
   */

  /**
   * Vide la liste des informations sur les variables
   */
  public void clear() {
    listeInformations.clear();
    listeInfobulles.clear();
    cb_ListeRcd.setModel(new DefaultComboBoxModel());
    t_Variables.setModel(new DefaultTableModel());
  }

  /**
   * Initialise le panel contenant la valeur des indicateurs
   * @param indicateurs
   */
  private void initIndicateur(char indicateurs[]) {
    if (indicateurs == null) {
      return;
    }

    Component composant = null;
    int cpt = 0;
    String chaine = null;
    for (int i = 0; i < p_Indicateur.getComponentCount(); i++) {
      composant = p_Indicateur.getComponent(i);
      if ((composant instanceof JLabel) && (composant.getName().startsWith("in"))) {
        chaine = composant.getName().substring(2);
        cpt = Integer.parseInt(chaine);
        ((JLabel) composant).setText(Character.toString(indicateurs[cpt - 1]));
        ((JLabel) composant).setToolTipText(chaine);
        if (((JLabel) composant).getText().equals("0")) {
          ((JLabel) composant).setForeground(Color.RED);
        }
        else {
          ((JLabel) composant).setForeground(Color.BLUE);
        }
      }
    }
  }

  /**
   * Charge les tableaux avec les flux entrants
   * @param afluxrpg
   */
  private void initVariablesEntrantes(FluxRecord afluxrpg) {
    int i = 0;
    int num = 0;
    oRecord record = afluxrpg.getDescriptionRecord();
    char[] indicateurs = afluxrpg.getIndicateurs();
    oData donnee = null;
    String[] tableau = null;
    ArrayList<String> listeVariables = new ArrayList<String>();
    ArrayList<String> listeValeurs = new ArrayList<String>();
    ArrayList<Integer> listeTaille = new ArrayList<Integer>();
    ArrayList<String> listeTooltip = new ArrayList<String>();

    // afluxrpg.getRecordName());
    // if (record != null)

    // Récupération des données du flux
    for (Map.Entry<String, ioGeneric> entry : record.listeElement.entrySet()) {
      if (entry.getValue() instanceof oData) {
        donnee = (oData) entry.getValue();
        // Dépassement de la capacité du buffer/DataQ
        if (donnee.getOffset() > (Constantes.DATAQLONG - 188)) {
          l_Warning.setText("Dépassement de la capacité du buffer/DataQ, variable hors buffer");
          continue;
        }
        listeVariables.add(donnee.getName());
        listeValeurs.add(donnee.getValeurToBuffer() + "|");
        listeTaille.add(donnee.getLongueur());
        listeTooltip.add("<html><b>" + donnee.getName() + ":</b><br>decimale=" + donnee.getDecimale() + "<br>isAlpha=" + donnee.isAlpha()
            + "<br>in_ctrl=" + (char) donnee.getInputCtrl() + "<br>out_edt=" + (char) donnee.getOutputEdt() + "<br>checkedit="
            + donnee.getCheckEdt() + "<br>ligne/colonne=" + donnee.getLigne() + "/" + donnee.getColonne() + "<br>visibility="
            + donnee.getVisibilityPol() + "<br>readonly=" + donnee.getReadOnlyPol() + "</html>");
      }
    }
    // Ajout des indicateurs
    for (i = 0; i < indicateurs.length; i++) {
      num = i + 1;
      listeVariables.add(num < 10 ? "*IN0" + num : "*IN" + num);
      if (indicateurs[i] == '1') {
        listeValeurs.add("*ON  (" + indicateurs[i] + ")");
      }
      else {
        listeValeurs.add("*OFF (" + indicateurs[i] + ")");
      }
      listeTaille.add(1);
      listeTooltip.add("Indicateur");
    }
    // Ajout de WABLC
    listeVariables.add("WABLC");
    if (afluxrpg.getBlocageEcran() == '1') {
      listeValeurs.add("EXFMT (1)");
    }
    else if (afluxrpg.getBlocageEcran() == '0') {
      listeValeurs.add("WRITE (0)");
    }
    else {
      listeValeurs.add("" + afluxrpg.getBlocageEcran());
    }
    listeTaille.add(1);
    listeTooltip.add("EXFMT/WRITE");
    // Ajout du vecteur FMT (WAFIC)
    listeVariables.add("WAFIC");
    listeValeurs.add(afluxrpg.getVFmt());
    listeTaille.add(20);
    listeTooltip.add("Vecteur FMT");

    // Conversion en tableau
    String[][] datavalue = new String[listeVariables.size()][4];
    String[] hostfield = new String[listeVariables.size()];
    for (i = 0; i < listeVariables.size(); i++) {
      datavalue[i][0] = listeVariables.get(i);
      datavalue[i][1] = "" + listeTaille.get(i);
      datavalue[i][2] = listeValeurs.get(i);
      hostfield[i] = listeTooltip.get(i);
    }

    // Ajout des données dans les listes
    String nomFmtRcd = record.getFormat() + "_" + record.getName() + " (" + ++compteur + ")";
    listeInformations.put(nomFmtRcd, new DefaultTableModel(datavalue, titlevalue));
    listeInfobulles.put(nomFmtRcd, hostfield);

    // On ne conserve que les 10 derniers flux
    try {
      int nbr = listeInformations.size() - 10;
      if (nbr > 0) {
        i = 0;
        tableau = new String[nbr];
        // On stocke les clés dans un tableau en vue de la suppression
        for (Map.Entry<String, DefaultTableModel> entry : listeInformations.entrySet()) {
          if (i < nbr) {
            tableau[i++] = entry.getKey();
          }
          else {
            break;
          }
        }
        // On supprime
        for (i = 0; i < tableau.length; i++) {
          listeInformations.remove(tableau[i]);
        }
      }
    }
    catch (Exception e) {
    }

    // Chargement de la combobox
    if (listeInformations.size() > 0) {
      tableau = new String[listeInformations.size()];
      i = 0;
      for (Map.Entry<String, DefaultTableModel> entry : listeInformations.entrySet()) {
        tableau[i++] = entry.getKey();
      }
      cb_ListeRcd.setModel(new DefaultComboBoxModel(tableau));
      cb_ListeRcd.setSelectedIndex(tableau.length - 1);
    }

    // Mise à jour si nécessaire (EXFMT)
    if (large && afluxrpg.isPanelAffichage()) {
      changeListeVariables(listeInformations.get(cb_ListeRcd.getSelectedItem()));
    }

    // On libère la mémoire
    listeVariables.clear();
    listeValeurs.clear();
    listeTaille.clear();
    listeTooltip.clear();
    listeVariables = null;
    listeValeurs = null;
    listeTaille = null;
    listeTooltip = null;
  }

  /**
   * Charge les tableaux avec le flux sortant
   * @param afluxrpg
   */
  private void initVariablesSortantes(FluxRecord afluxrpg) {
    oRecord record = afluxrpg.getDescriptionRecord();
    char[] indicateurs = afluxrpg.getIndicateurs();
    oData donnee = null;

    // On recherche le bon model
    String nomFmtRcd = record.getFormat() + "_" + record.getName() + " (" + compteur + ")";
    DefaultTableModel model = listeInformations.get(nomFmtRcd);
    if (model == null) {
      return;
    }

    // Récupération des données du flux
    int ligne = 0;
    for (Map.Entry<String, ioGeneric> entry : record.listeElement.entrySet()) {
      if (entry.getValue() instanceof oData) {
        donnee = (oData) entry.getValue();
        // Dépassement de la capacité du buffer/DataQ
        if (donnee.getOffset() > (Constantes.DATAQLONG - 188)) {
          continue;
        }
        model.setValueAt(donnee.getValeurToBuffer() + "|", ligne++, 3);
      }
    }
    // Ajout des indicateurs
    for (int i = 0; i < indicateurs.length; i++) {
      if (indicateurs[i] == '1') {
        model.setValueAt("*ON  (" + indicateurs[i] + ")", ligne++, 3);
      }
      else {
        model.setValueAt("*OFF (" + indicateurs[i] + ")", ligne++, 3);
      }
    }

    // Mise à jour si nécessaire (EXFMT)
    if (large && afluxrpg.isPanelAffichage()) {
      changeListeVariables(listeInformations.get(cb_ListeRcd.getSelectedItem()));
    }
  }

  /**
   * Change les informations de la jTable
   * @param model
   */
  private void changeListeVariables(final DefaultTableModel model) {
    // On met à jour la table dans le thread
    if (model == null) {
      return;
    }

    t_Variables.setModel(model);
    sorter = new TableRowSorter<TableModel>(model);
    t_Variables.setRowSorter(sorter);

    t_Variables.setIntercellSpacing(new Dimension(1, 1)); // Permet d'afficher les séparateurs sous Nimbus
    t_Variables.setShowVerticalLines(true);
    // On défini la largeur des colonnes
    TableColumnModel cm = t_Variables.getColumnModel();
    for (int k = 0; k < cm.getColumnCount(); k++) {
      cm.getColumn(k).setPreferredWidth(largeurcol[k]);
    }
  }

  /**
   * Filtre la liste sur les noms de variables
   */
  private void filtreVariables() {
    String text = tf_Filtre.getText().toUpperCase();

    if (text.length() == 0) {
      sorter.setRowFilter(null);
    }
    else {
      sorter.setRowFilter(RowFilter.regexFilter("^" + text, 0));
    }
  }

  /**
   * Affiche l'utilisation mémoire
   */
  private void monitoring() {
    thread = new Thread("Td:DebugMode-id=" + idSession) {
      final StringBuffer sb = new StringBuffer();

      @Override
      public void run() {
        do {
          try {
            sb.setLength(0);
            long totalmem = Runtime.getRuntime().totalMemory();
            sb.append(((totalmem - Runtime.getRuntime().freeMemory()) / (1024 * 1024))).append(" de ").append(totalmem / (1024 * 1024))
                .append(" Mo / ").append(Runtime.getRuntime().maxMemory() / (1024 * 1024)).append(" Mo max");
            // majRam(sb.toString());
            l_Monitor.setText(sb.toString());
            Thread.sleep(1000);
          }
          catch (InterruptedException ex) {
            Thread.currentThread().interrupt(); // Très important de réinterrompre
            break; // Sortie de la boucle infinie
          }
        }
        while (true);
      }
    };
    thread.setDaemon(true);
    thread.start();
  }

  /**
   * Vérifie la cohérence entre le buffer de données et le panel que l'on doit afficher
   * @param afluxrpg
   * @return
   */
  private boolean controlCoherenceRecordAndBuffer(FluxRecord afluxrpg) {
    String buffer = afluxrpg.getRecordName().substring(afluxrpg.getRecordName().length() - 2);
    String record = afluxrpg.getClassName().substring(afluxrpg.getClassName().length() - 2);

    if (!buffer.equals(record)) {
      if (buffer.equals("F1") || buffer.equals("F2")) {
        return true;
      }
      return false;
    }
    return true;
  }

  /**
   * Nettoyage de la mémoire
   */
  @Override
  public void dispose() {
    if (thread != null) {
      thread.interrupt();
      thread = null;
    }
    titlevalue = null;
    largeurcol = null;
    listeInformations.clear();
    listeInfobulles.clear();
    sorter = null;
    super.dispose();
  }

  /*
  private void majRam(final String texte)
  {
    Runnable code = new Runnable() {
      public void run()
      {
        l_Monitor.setText(texte);
      }
    };
    // On s'assure qu'il est bien envoyé dans l'EDT
    if (SwingUtilities.isEventDispatchThread())
      code.run();
    else
      SwingUtilities.invokeLater(code);
  }*/

  private void ckb_OnTopActionPerformed(ActionEvent e) {
    setAlwaysOnTop(ckb_OnTop.isSelected());
  }

  private void bt_VariablesActionPerformed(ActionEvent e) {
    if (large) {
      setSize(LARGEUR_COURTE, 530);
      bt_Variables.setText("Variables >>");
    }
    else {
      setSize(LARGEUR_GRANDE, 530);
      changeListeVariables(listeInformations.get(cb_ListeRcd.getSelectedItem()));
      bt_Variables.setText("Variables <<");
    }
    large = !large;
  }

  private void cb_ListeRcdActionPerformed(ActionEvent e) {
    changeListeVariables(listeInformations.get(cb_ListeRcd.getSelectedItem()));
  }

  private void bt_ViderActionPerformed(ActionEvent e) {
    clear();
  }

  private void tf_FiltreKeyReleased(KeyEvent e) {
    filtreVariables();
  }

  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_InfosGeneriques = new JPanel();
    RPG = new JLabel();
    DSPF = new JLabel();
    l_RPG = new JLabel();
    l_DSPF = new JLabel();
    l_Record = new JLabel();
    Record = new JLabel();
    ts_Indicateur = new JXTitledSeparator();
    p_Indicateur = new JPanel();
    x00 = new JLabel();
    x01 = new JLabel();
    x02 = new JLabel();
    x03 = new JLabel();
    x04 = new JLabel();
    x05 = new JLabel();
    x06 = new JLabel();
    x07 = new JLabel();
    x08 = new JLabel();
    x09 = new JLabel();
    y00 = new JLabel();
    y01 = new JLabel();
    y02 = new JLabel();
    y03 = new JLabel();
    y04 = new JLabel();
    y05 = new JLabel();
    y06 = new JLabel();
    y07 = new JLabel();
    y08 = new JLabel();
    y09 = new JLabel();
    in01 = new JLabel();
    in02 = new JLabel();
    in03 = new JLabel();
    in04 = new JLabel();
    in05 = new JLabel();
    in06 = new JLabel();
    in07 = new JLabel();
    in08 = new JLabel();
    in09 = new JLabel();
    in10 = new JLabel();
    in11 = new JLabel();
    in12 = new JLabel();
    in13 = new JLabel();
    in14 = new JLabel();
    in15 = new JLabel();
    in16 = new JLabel();
    in17 = new JLabel();
    in18 = new JLabel();
    in19 = new JLabel();
    in20 = new JLabel();
    in21 = new JLabel();
    in22 = new JLabel();
    in23 = new JLabel();
    in24 = new JLabel();
    in25 = new JLabel();
    in26 = new JLabel();
    in27 = new JLabel();
    in28 = new JLabel();
    in29 = new JLabel();
    in30 = new JLabel();
    in31 = new JLabel();
    in32 = new JLabel();
    in33 = new JLabel();
    in34 = new JLabel();
    in35 = new JLabel();
    in36 = new JLabel();
    in37 = new JLabel();
    in38 = new JLabel();
    in39 = new JLabel();
    in40 = new JLabel();
    in41 = new JLabel();
    in42 = new JLabel();
    in43 = new JLabel();
    in44 = new JLabel();
    in45 = new JLabel();
    in46 = new JLabel();
    in47 = new JLabel();
    in48 = new JLabel();
    in49 = new JLabel();
    in50 = new JLabel();
    in51 = new JLabel();
    in52 = new JLabel();
    in53 = new JLabel();
    in54 = new JLabel();
    in55 = new JLabel();
    in56 = new JLabel();
    in57 = new JLabel();
    in58 = new JLabel();
    in59 = new JLabel();
    in60 = new JLabel();
    in61 = new JLabel();
    in62 = new JLabel();
    in63 = new JLabel();
    in64 = new JLabel();
    in65 = new JLabel();
    in66 = new JLabel();
    in67 = new JLabel();
    in68 = new JLabel();
    in69 = new JLabel();
    in70 = new JLabel();
    in71 = new JLabel();
    in72 = new JLabel();
    in73 = new JLabel();
    in74 = new JLabel();
    in75 = new JLabel();
    in76 = new JLabel();
    in77 = new JLabel();
    in78 = new JLabel();
    in79 = new JLabel();
    in80 = new JLabel();
    in81 = new JLabel();
    in82 = new JLabel();
    in83 = new JLabel();
    in84 = new JLabel();
    in85 = new JLabel();
    in86 = new JLabel();
    in87 = new JLabel();
    in88 = new JLabel();
    in89 = new JLabel();
    in90 = new JLabel();
    in91 = new JLabel();
    in92 = new JLabel();
    in93 = new JLabel();
    in94 = new JLabel();
    in95 = new JLabel();
    in96 = new JLabel();
    in97 = new JLabel();
    in98 = new JLabel();
    in99 = new JLabel();
    ts_Options = new JXTitledSeparator();
    ckb_OnTop = new JCheckBox();
    ts_Focus = new JXTitledSeparator();
    l_Focus = new JLabel();
    WABLC = new JLabel();
    l_WABLC = new JLabel();
    l_libelleprg = new JLabel();
    bt_Variables = new JButton();
    p_Variables = new JPanel();
    panel2 = new JPanel();
    l_Warning = new JLabel();
    cb_ListeRcd = new JComboBox();
    bt_Vider = new JButton();
    l_LibelleFiltre = new JLabel();
    tf_Filtre = new JTextField();
    l_LibelleFMT = new JLabel();
    l_FMT = new JLabel();
    scrollPane1 = new JScrollPane();
    t_Variables = new JTable();
    l_Monitor = new JLabel();
    CellConstraints cc = new CellConstraints();
    
    // ======== this ========
    setTitle("D\u00e9bug");
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== p_InfosGeneriques ========
    {
      p_InfosGeneriques.setMinimumSize(new Dimension(170, 385));
      p_InfosGeneriques.setMaximumSize(new Dimension(170, 385));
      p_InfosGeneriques.setName("p_InfosGeneriques");
      
      // ---- RPG ----
      RPG.setText("RPG:");
      RPG.setFont(RPG.getFont().deriveFont(RPG.getFont().getStyle() | Font.BOLD));
      RPG.setName("RPG");
      
      // ---- DSPF ----
      DSPF.setText("DSPF:");
      DSPF.setFont(DSPF.getFont().deriveFont(DSPF.getFont().getStyle() | Font.BOLD));
      DSPF.setName("DSPF");
      
      // ---- l_RPG ----
      l_RPG.setText("text");
      l_RPG.setName("l_RPG");
      
      // ---- l_DSPF ----
      l_DSPF.setText("text");
      l_DSPF.setName("l_DSPF");
      
      // ---- l_Record ----
      l_Record.setText("text");
      l_Record.setName("l_Record");
      
      // ---- Record ----
      Record.setText("Record:");
      Record.setFont(Record.getFont().deriveFont(Record.getFont().getStyle() | Font.BOLD));
      Record.setName("Record");
      
      // ---- ts_Indicateur ----
      ts_Indicateur.setTitle("Valeur des indicateurs");
      ts_Indicateur.setName("ts_Indicateur");
      
      // ======== p_Indicateur ========
      {
        p_Indicateur.setName("p_Indicateur");
        p_Indicateur.setLayout(new FormLayout(
            new ColumnSpec[] { FormFactory.DEFAULT_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                FormFactory.DEFAULT_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                FormFactory.DEFAULT_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
                FormFactory.DEFAULT_COLSPEC, FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC },
            new RowSpec[] { FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC, FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC,
                FormFactory.LINE_GAP_ROWSPEC, FormFactory.DEFAULT_ROWSPEC }));
        
        // ---- x00 ----
        x00.setText("0");
        x00.setHorizontalAlignment(SwingConstants.CENTER);
        x00.setFont(x00.getFont().deriveFont(x00.getFont().getStyle() | Font.BOLD));
        x00.setName("x00");
        p_Indicateur.add(x00, cc.xy(3, 1));
        
        // ---- x01 ----
        x01.setText("1");
        x01.setHorizontalAlignment(SwingConstants.CENTER);
        x01.setFont(x01.getFont().deriveFont(x01.getFont().getStyle() | Font.BOLD));
        x01.setName("x01");
        p_Indicateur.add(x01, cc.xy(5, 1));
        
        // ---- x02 ----
        x02.setText("2");
        x02.setHorizontalAlignment(SwingConstants.CENTER);
        x02.setFont(x02.getFont().deriveFont(x02.getFont().getStyle() | Font.BOLD));
        x02.setName("x02");
        p_Indicateur.add(x02, cc.xy(7, 1));
        
        // ---- x03 ----
        x03.setText("3");
        x03.setHorizontalAlignment(SwingConstants.CENTER);
        x03.setFont(x03.getFont().deriveFont(x03.getFont().getStyle() | Font.BOLD));
        x03.setName("x03");
        p_Indicateur.add(x03, cc.xy(9, 1));
        
        // ---- x04 ----
        x04.setText("4");
        x04.setHorizontalAlignment(SwingConstants.CENTER);
        x04.setFont(x04.getFont().deriveFont(x04.getFont().getStyle() | Font.BOLD));
        x04.setName("x04");
        p_Indicateur.add(x04, cc.xy(11, 1));
        
        // ---- x05 ----
        x05.setText("5");
        x05.setHorizontalAlignment(SwingConstants.CENTER);
        x05.setFont(x05.getFont().deriveFont(x05.getFont().getStyle() | Font.BOLD));
        x05.setName("x05");
        p_Indicateur.add(x05, cc.xy(13, 1));
        
        // ---- x06 ----
        x06.setText("6");
        x06.setHorizontalAlignment(SwingConstants.CENTER);
        x06.setFont(x06.getFont().deriveFont(x06.getFont().getStyle() | Font.BOLD));
        x06.setName("x06");
        p_Indicateur.add(x06, cc.xy(15, 1));
        
        // ---- x07 ----
        x07.setText("7");
        x07.setHorizontalAlignment(SwingConstants.CENTER);
        x07.setFont(x07.getFont().deriveFont(x07.getFont().getStyle() | Font.BOLD));
        x07.setName("x07");
        p_Indicateur.add(x07, cc.xy(17, 1));
        
        // ---- x08 ----
        x08.setText("8");
        x08.setHorizontalAlignment(SwingConstants.CENTER);
        x08.setFont(x08.getFont().deriveFont(x08.getFont().getStyle() | Font.BOLD));
        x08.setName("x08");
        p_Indicateur.add(x08, cc.xy(19, 1));
        
        // ---- x09 ----
        x09.setText("9");
        x09.setHorizontalAlignment(SwingConstants.CENTER);
        x09.setFont(x09.getFont().deriveFont(x09.getFont().getStyle() | Font.BOLD));
        x09.setName("x09");
        p_Indicateur.add(x09, cc.xy(21, 1));
        
        // ---- y00 ----
        y00.setText("0");
        y00.setHorizontalAlignment(SwingConstants.CENTER);
        y00.setFont(y00.getFont().deriveFont(y00.getFont().getStyle() | Font.BOLD));
        y00.setName("y00");
        p_Indicateur.add(y00, cc.xy(1, 3));
        
        // ---- y01 ----
        y01.setText("1");
        y01.setHorizontalAlignment(SwingConstants.CENTER);
        y01.setFont(y01.getFont().deriveFont(y01.getFont().getStyle() | Font.BOLD));
        y01.setName("y01");
        p_Indicateur.add(y01, cc.xy(1, 5));
        
        // ---- y02 ----
        y02.setText("2");
        y02.setHorizontalAlignment(SwingConstants.CENTER);
        y02.setFont(y02.getFont().deriveFont(y02.getFont().getStyle() | Font.BOLD));
        y02.setName("y02");
        p_Indicateur.add(y02, cc.xy(1, 7));
        
        // ---- y03 ----
        y03.setText("3");
        y03.setHorizontalAlignment(SwingConstants.CENTER);
        y03.setFont(y03.getFont().deriveFont(y03.getFont().getStyle() | Font.BOLD));
        y03.setName("y03");
        p_Indicateur.add(y03, cc.xy(1, 9));
        
        // ---- y04 ----
        y04.setText("4");
        y04.setHorizontalAlignment(SwingConstants.CENTER);
        y04.setFont(y04.getFont().deriveFont(y04.getFont().getStyle() | Font.BOLD));
        y04.setName("y04");
        p_Indicateur.add(y04, cc.xy(1, 11));
        
        // ---- y05 ----
        y05.setText("5");
        y05.setHorizontalAlignment(SwingConstants.CENTER);
        y05.setFont(y05.getFont().deriveFont(y05.getFont().getStyle() | Font.BOLD));
        y05.setName("y05");
        p_Indicateur.add(y05, cc.xy(1, 13));
        
        // ---- y06 ----
        y06.setText("6");
        y06.setHorizontalAlignment(SwingConstants.CENTER);
        y06.setFont(y06.getFont().deriveFont(y06.getFont().getStyle() | Font.BOLD));
        y06.setName("y06");
        p_Indicateur.add(y06, cc.xy(1, 15));
        
        // ---- y07 ----
        y07.setText("7");
        y07.setHorizontalAlignment(SwingConstants.CENTER);
        y07.setFont(y07.getFont().deriveFont(y07.getFont().getStyle() | Font.BOLD));
        y07.setName("y07");
        p_Indicateur.add(y07, cc.xy(1, 17));
        
        // ---- y08 ----
        y08.setText("8");
        y08.setHorizontalAlignment(SwingConstants.CENTER);
        y08.setFont(y08.getFont().deriveFont(y08.getFont().getStyle() | Font.BOLD));
        y08.setName("y08");
        p_Indicateur.add(y08, cc.xy(1, 19));
        
        // ---- y09 ----
        y09.setText("9");
        y09.setHorizontalAlignment(SwingConstants.CENTER);
        y09.setFont(y09.getFont().deriveFont(y09.getFont().getStyle() | Font.BOLD));
        y09.setName("y09");
        p_Indicateur.add(y09, cc.xy(1, 21));
        
        // ---- in01 ----
        in01.setText("0");
        in01.setHorizontalAlignment(SwingConstants.CENTER);
        in01.setName("in01");
        p_Indicateur.add(in01, cc.xy(5, 3));
        
        // ---- in02 ----
        in02.setText("0");
        in02.setHorizontalAlignment(SwingConstants.CENTER);
        in02.setName("in02");
        p_Indicateur.add(in02, cc.xy(7, 3));
        
        // ---- in03 ----
        in03.setText("0");
        in03.setHorizontalAlignment(SwingConstants.CENTER);
        in03.setName("in03");
        p_Indicateur.add(in03, cc.xy(9, 3));
        
        // ---- in04 ----
        in04.setText("0");
        in04.setHorizontalAlignment(SwingConstants.CENTER);
        in04.setName("in04");
        p_Indicateur.add(in04, cc.xy(11, 3));
        
        // ---- in05 ----
        in05.setText("0");
        in05.setHorizontalAlignment(SwingConstants.CENTER);
        in05.setName("in05");
        p_Indicateur.add(in05, cc.xy(13, 3));
        
        // ---- in06 ----
        in06.setText("0");
        in06.setHorizontalAlignment(SwingConstants.CENTER);
        in06.setName("in06");
        p_Indicateur.add(in06, cc.xy(15, 3));
        
        // ---- in07 ----
        in07.setText("0");
        in07.setHorizontalAlignment(SwingConstants.CENTER);
        in07.setName("in07");
        p_Indicateur.add(in07, cc.xy(17, 3));
        
        // ---- in08 ----
        in08.setText("0");
        in08.setHorizontalAlignment(SwingConstants.CENTER);
        in08.setName("in08");
        p_Indicateur.add(in08, cc.xy(19, 3));
        
        // ---- in09 ----
        in09.setText("0");
        in09.setHorizontalAlignment(SwingConstants.CENTER);
        in09.setName("in09");
        p_Indicateur.add(in09, cc.xy(21, 3));
        
        // ---- in10 ----
        in10.setText("0");
        in10.setHorizontalAlignment(SwingConstants.CENTER);
        in10.setName("in10");
        p_Indicateur.add(in10, cc.xy(3, 5));
        
        // ---- in11 ----
        in11.setText("0");
        in11.setHorizontalAlignment(SwingConstants.CENTER);
        in11.setName("in11");
        p_Indicateur.add(in11, cc.xy(5, 5));
        
        // ---- in12 ----
        in12.setText("0");
        in12.setHorizontalAlignment(SwingConstants.CENTER);
        in12.setName("in12");
        p_Indicateur.add(in12, cc.xy(7, 5));
        
        // ---- in13 ----
        in13.setText("0");
        in13.setHorizontalAlignment(SwingConstants.CENTER);
        in13.setName("in13");
        p_Indicateur.add(in13, cc.xy(9, 5));
        
        // ---- in14 ----
        in14.setText("0");
        in14.setHorizontalAlignment(SwingConstants.CENTER);
        in14.setName("in14");
        p_Indicateur.add(in14, cc.xy(11, 5));
        
        // ---- in15 ----
        in15.setText("0");
        in15.setHorizontalAlignment(SwingConstants.CENTER);
        in15.setName("in15");
        p_Indicateur.add(in15, cc.xy(13, 5));
        
        // ---- in16 ----
        in16.setText("0");
        in16.setHorizontalAlignment(SwingConstants.CENTER);
        in16.setName("in16");
        p_Indicateur.add(in16, cc.xy(15, 5));
        
        // ---- in17 ----
        in17.setText("0");
        in17.setHorizontalAlignment(SwingConstants.CENTER);
        in17.setName("in17");
        p_Indicateur.add(in17, cc.xy(17, 5));
        
        // ---- in18 ----
        in18.setText("0");
        in18.setHorizontalAlignment(SwingConstants.CENTER);
        in18.setName("in18");
        p_Indicateur.add(in18, cc.xy(19, 5));
        
        // ---- in19 ----
        in19.setText("0");
        in19.setHorizontalAlignment(SwingConstants.CENTER);
        in19.setName("in19");
        p_Indicateur.add(in19, cc.xy(21, 5));
        
        // ---- in20 ----
        in20.setText("0");
        in20.setHorizontalAlignment(SwingConstants.CENTER);
        in20.setName("in20");
        p_Indicateur.add(in20, cc.xy(3, 7));
        
        // ---- in21 ----
        in21.setText("0");
        in21.setHorizontalAlignment(SwingConstants.CENTER);
        in21.setName("in21");
        p_Indicateur.add(in21, cc.xy(5, 7));
        
        // ---- in22 ----
        in22.setText("0");
        in22.setHorizontalAlignment(SwingConstants.CENTER);
        in22.setName("in22");
        p_Indicateur.add(in22, cc.xy(7, 7));
        
        // ---- in23 ----
        in23.setText("0");
        in23.setHorizontalAlignment(SwingConstants.CENTER);
        in23.setName("in23");
        p_Indicateur.add(in23, cc.xy(9, 7));
        
        // ---- in24 ----
        in24.setText("0");
        in24.setHorizontalAlignment(SwingConstants.CENTER);
        in24.setName("in24");
        p_Indicateur.add(in24, cc.xy(11, 7));
        
        // ---- in25 ----
        in25.setText("0");
        in25.setHorizontalAlignment(SwingConstants.CENTER);
        in25.setName("in25");
        p_Indicateur.add(in25, cc.xy(13, 7));
        
        // ---- in26 ----
        in26.setText("0");
        in26.setHorizontalAlignment(SwingConstants.CENTER);
        in26.setName("in26");
        p_Indicateur.add(in26, cc.xy(15, 7));
        
        // ---- in27 ----
        in27.setText("0");
        in27.setHorizontalAlignment(SwingConstants.CENTER);
        in27.setName("in27");
        p_Indicateur.add(in27, cc.xy(17, 7));
        
        // ---- in28 ----
        in28.setText("0");
        in28.setHorizontalAlignment(SwingConstants.CENTER);
        in28.setName("in28");
        p_Indicateur.add(in28, cc.xy(19, 7));
        
        // ---- in29 ----
        in29.setText("0");
        in29.setHorizontalAlignment(SwingConstants.CENTER);
        in29.setName("in29");
        p_Indicateur.add(in29, cc.xy(21, 7));
        
        // ---- in30 ----
        in30.setText("0");
        in30.setHorizontalAlignment(SwingConstants.CENTER);
        in30.setName("in30");
        p_Indicateur.add(in30, cc.xy(3, 9));
        
        // ---- in31 ----
        in31.setText("0");
        in31.setHorizontalAlignment(SwingConstants.CENTER);
        in31.setName("in31");
        p_Indicateur.add(in31, cc.xy(5, 9));
        
        // ---- in32 ----
        in32.setText("0");
        in32.setHorizontalAlignment(SwingConstants.CENTER);
        in32.setName("in32");
        p_Indicateur.add(in32, cc.xy(7, 9));
        
        // ---- in33 ----
        in33.setText("0");
        in33.setHorizontalAlignment(SwingConstants.CENTER);
        in33.setName("in33");
        p_Indicateur.add(in33, cc.xy(9, 9));
        
        // ---- in34 ----
        in34.setText("0");
        in34.setHorizontalAlignment(SwingConstants.CENTER);
        in34.setName("in34");
        p_Indicateur.add(in34, cc.xy(11, 9));
        
        // ---- in35 ----
        in35.setText("0");
        in35.setHorizontalAlignment(SwingConstants.CENTER);
        in35.setName("in35");
        p_Indicateur.add(in35, cc.xy(13, 9));
        
        // ---- in36 ----
        in36.setText("0");
        in36.setHorizontalAlignment(SwingConstants.CENTER);
        in36.setName("in36");
        p_Indicateur.add(in36, cc.xy(15, 9));
        
        // ---- in37 ----
        in37.setText("0");
        in37.setHorizontalAlignment(SwingConstants.CENTER);
        in37.setName("in37");
        p_Indicateur.add(in37, cc.xy(17, 9));
        
        // ---- in38 ----
        in38.setText("0");
        in38.setHorizontalAlignment(SwingConstants.CENTER);
        in38.setName("in38");
        p_Indicateur.add(in38, cc.xy(19, 9));
        
        // ---- in39 ----
        in39.setText("0");
        in39.setHorizontalAlignment(SwingConstants.CENTER);
        in39.setName("in39");
        p_Indicateur.add(in39, cc.xy(21, 9));
        
        // ---- in40 ----
        in40.setText("0");
        in40.setHorizontalAlignment(SwingConstants.CENTER);
        in40.setName("in40");
        p_Indicateur.add(in40, cc.xy(3, 11));
        
        // ---- in41 ----
        in41.setText("0");
        in41.setHorizontalAlignment(SwingConstants.CENTER);
        in41.setName("in41");
        p_Indicateur.add(in41, cc.xy(5, 11));
        
        // ---- in42 ----
        in42.setText("0");
        in42.setHorizontalAlignment(SwingConstants.CENTER);
        in42.setName("in42");
        p_Indicateur.add(in42, cc.xy(7, 11));
        
        // ---- in43 ----
        in43.setText("0");
        in43.setHorizontalAlignment(SwingConstants.CENTER);
        in43.setName("in43");
        p_Indicateur.add(in43, cc.xy(9, 11));
        
        // ---- in44 ----
        in44.setText("0");
        in44.setHorizontalAlignment(SwingConstants.CENTER);
        in44.setName("in44");
        p_Indicateur.add(in44, cc.xy(11, 11));
        
        // ---- in45 ----
        in45.setText("0");
        in45.setHorizontalAlignment(SwingConstants.CENTER);
        in45.setName("in45");
        p_Indicateur.add(in45, cc.xy(13, 11));
        
        // ---- in46 ----
        in46.setText("0");
        in46.setHorizontalAlignment(SwingConstants.CENTER);
        in46.setName("in46");
        p_Indicateur.add(in46, cc.xy(15, 11));
        
        // ---- in47 ----
        in47.setText("0");
        in47.setHorizontalAlignment(SwingConstants.CENTER);
        in47.setName("in47");
        p_Indicateur.add(in47, cc.xy(17, 11));
        
        // ---- in48 ----
        in48.setText("0");
        in48.setHorizontalAlignment(SwingConstants.CENTER);
        in48.setName("in48");
        p_Indicateur.add(in48, cc.xy(19, 11));
        
        // ---- in49 ----
        in49.setText("0");
        in49.setHorizontalAlignment(SwingConstants.CENTER);
        in49.setName("in49");
        p_Indicateur.add(in49, cc.xy(21, 11));
        
        // ---- in50 ----
        in50.setText("0");
        in50.setHorizontalAlignment(SwingConstants.CENTER);
        in50.setName("in50");
        p_Indicateur.add(in50, cc.xy(3, 13));
        
        // ---- in51 ----
        in51.setText("0");
        in51.setHorizontalAlignment(SwingConstants.CENTER);
        in51.setName("in51");
        p_Indicateur.add(in51, cc.xy(5, 13));
        
        // ---- in52 ----
        in52.setText("0");
        in52.setHorizontalAlignment(SwingConstants.CENTER);
        in52.setName("in52");
        p_Indicateur.add(in52, cc.xy(7, 13));
        
        // ---- in53 ----
        in53.setText("0");
        in53.setHorizontalAlignment(SwingConstants.CENTER);
        in53.setName("in53");
        p_Indicateur.add(in53, cc.xy(9, 13));
        
        // ---- in54 ----
        in54.setText("0");
        in54.setHorizontalAlignment(SwingConstants.CENTER);
        in54.setName("in54");
        p_Indicateur.add(in54, cc.xy(11, 13));
        
        // ---- in55 ----
        in55.setText("0");
        in55.setHorizontalAlignment(SwingConstants.CENTER);
        in55.setName("in55");
        p_Indicateur.add(in55, cc.xy(13, 13));
        
        // ---- in56 ----
        in56.setText("0");
        in56.setHorizontalAlignment(SwingConstants.CENTER);
        in56.setName("in56");
        p_Indicateur.add(in56, cc.xy(15, 13));
        
        // ---- in57 ----
        in57.setText("0");
        in57.setHorizontalAlignment(SwingConstants.CENTER);
        in57.setName("in57");
        p_Indicateur.add(in57, cc.xy(17, 13));
        
        // ---- in58 ----
        in58.setText("0");
        in58.setHorizontalAlignment(SwingConstants.CENTER);
        in58.setName("in58");
        p_Indicateur.add(in58, cc.xy(19, 13));
        
        // ---- in59 ----
        in59.setText("0");
        in59.setHorizontalAlignment(SwingConstants.CENTER);
        in59.setName("in59");
        p_Indicateur.add(in59, cc.xy(21, 13));
        
        // ---- in60 ----
        in60.setText("0");
        in60.setHorizontalAlignment(SwingConstants.CENTER);
        in60.setName("in60");
        p_Indicateur.add(in60, cc.xy(3, 15));
        
        // ---- in61 ----
        in61.setText("0");
        in61.setHorizontalAlignment(SwingConstants.CENTER);
        in61.setName("in61");
        p_Indicateur.add(in61, cc.xy(5, 15));
        
        // ---- in62 ----
        in62.setText("0");
        in62.setHorizontalAlignment(SwingConstants.CENTER);
        in62.setName("in62");
        p_Indicateur.add(in62, cc.xy(7, 15));
        
        // ---- in63 ----
        in63.setText("0");
        in63.setHorizontalAlignment(SwingConstants.CENTER);
        in63.setName("in63");
        p_Indicateur.add(in63, cc.xy(9, 15));
        
        // ---- in64 ----
        in64.setText("0");
        in64.setHorizontalAlignment(SwingConstants.CENTER);
        in64.setName("in64");
        p_Indicateur.add(in64, cc.xy(11, 15));
        
        // ---- in65 ----
        in65.setText("0");
        in65.setHorizontalAlignment(SwingConstants.CENTER);
        in65.setName("in65");
        p_Indicateur.add(in65, cc.xy(13, 15));
        
        // ---- in66 ----
        in66.setText("0");
        in66.setHorizontalAlignment(SwingConstants.CENTER);
        in66.setName("in66");
        p_Indicateur.add(in66, cc.xy(15, 15));
        
        // ---- in67 ----
        in67.setText("0");
        in67.setHorizontalAlignment(SwingConstants.CENTER);
        in67.setName("in67");
        p_Indicateur.add(in67, cc.xy(17, 15));
        
        // ---- in68 ----
        in68.setText("0");
        in68.setHorizontalAlignment(SwingConstants.CENTER);
        in68.setName("in68");
        p_Indicateur.add(in68, cc.xy(19, 15));
        
        // ---- in69 ----
        in69.setText("0");
        in69.setHorizontalAlignment(SwingConstants.CENTER);
        in69.setName("in69");
        p_Indicateur.add(in69, cc.xy(21, 15));
        
        // ---- in70 ----
        in70.setText("0");
        in70.setHorizontalAlignment(SwingConstants.CENTER);
        in70.setName("in70");
        p_Indicateur.add(in70, cc.xy(3, 17));
        
        // ---- in71 ----
        in71.setText("0");
        in71.setHorizontalAlignment(SwingConstants.CENTER);
        in71.setName("in71");
        p_Indicateur.add(in71, cc.xy(5, 17));
        
        // ---- in72 ----
        in72.setText("0");
        in72.setHorizontalAlignment(SwingConstants.CENTER);
        in72.setName("in72");
        p_Indicateur.add(in72, cc.xy(7, 17));
        
        // ---- in73 ----
        in73.setText("0");
        in73.setHorizontalAlignment(SwingConstants.CENTER);
        in73.setName("in73");
        p_Indicateur.add(in73, cc.xy(9, 17));
        
        // ---- in74 ----
        in74.setText("0");
        in74.setHorizontalAlignment(SwingConstants.CENTER);
        in74.setName("in74");
        p_Indicateur.add(in74, cc.xy(11, 17));
        
        // ---- in75 ----
        in75.setText("0");
        in75.setHorizontalAlignment(SwingConstants.CENTER);
        in75.setName("in75");
        p_Indicateur.add(in75, cc.xy(13, 17));
        
        // ---- in76 ----
        in76.setText("0");
        in76.setHorizontalAlignment(SwingConstants.CENTER);
        in76.setName("in76");
        p_Indicateur.add(in76, cc.xy(15, 17));
        
        // ---- in77 ----
        in77.setText("0");
        in77.setHorizontalAlignment(SwingConstants.CENTER);
        in77.setName("in77");
        p_Indicateur.add(in77, cc.xy(17, 17));
        
        // ---- in78 ----
        in78.setText("0");
        in78.setHorizontalAlignment(SwingConstants.CENTER);
        in78.setName("in78");
        p_Indicateur.add(in78, cc.xy(19, 17));
        
        // ---- in79 ----
        in79.setText("0");
        in79.setHorizontalAlignment(SwingConstants.CENTER);
        in79.setName("in79");
        p_Indicateur.add(in79, cc.xy(21, 17));
        
        // ---- in80 ----
        in80.setText("0");
        in80.setHorizontalAlignment(SwingConstants.CENTER);
        in80.setName("in80");
        p_Indicateur.add(in80, cc.xy(3, 19));
        
        // ---- in81 ----
        in81.setText("0");
        in81.setHorizontalAlignment(SwingConstants.CENTER);
        in81.setName("in81");
        p_Indicateur.add(in81, cc.xy(5, 19));
        
        // ---- in82 ----
        in82.setText("0");
        in82.setHorizontalAlignment(SwingConstants.CENTER);
        in82.setName("in82");
        p_Indicateur.add(in82, cc.xy(7, 19));
        
        // ---- in83 ----
        in83.setText("0");
        in83.setHorizontalAlignment(SwingConstants.CENTER);
        in83.setName("in83");
        p_Indicateur.add(in83, cc.xy(9, 19));
        
        // ---- in84 ----
        in84.setText("0");
        in84.setHorizontalAlignment(SwingConstants.CENTER);
        in84.setName("in84");
        p_Indicateur.add(in84, cc.xy(11, 19));
        
        // ---- in85 ----
        in85.setText("0");
        in85.setHorizontalAlignment(SwingConstants.CENTER);
        in85.setName("in85");
        p_Indicateur.add(in85, cc.xy(13, 19));
        
        // ---- in86 ----
        in86.setText("0");
        in86.setHorizontalAlignment(SwingConstants.CENTER);
        in86.setName("in86");
        p_Indicateur.add(in86, cc.xy(15, 19));
        
        // ---- in87 ----
        in87.setText("0");
        in87.setHorizontalAlignment(SwingConstants.CENTER);
        in87.setName("in87");
        p_Indicateur.add(in87, cc.xy(17, 19));
        
        // ---- in88 ----
        in88.setText("0");
        in88.setHorizontalAlignment(SwingConstants.CENTER);
        in88.setName("in88");
        p_Indicateur.add(in88, cc.xy(19, 19));
        
        // ---- in89 ----
        in89.setText("0");
        in89.setHorizontalAlignment(SwingConstants.CENTER);
        in89.setName("in89");
        p_Indicateur.add(in89, cc.xy(21, 19));
        
        // ---- in90 ----
        in90.setText("0");
        in90.setHorizontalAlignment(SwingConstants.CENTER);
        in90.setName("in90");
        p_Indicateur.add(in90, cc.xy(3, 21));
        
        // ---- in91 ----
        in91.setText("0");
        in91.setHorizontalAlignment(SwingConstants.CENTER);
        in91.setName("in91");
        p_Indicateur.add(in91, cc.xy(5, 21));
        
        // ---- in92 ----
        in92.setText("0");
        in92.setHorizontalAlignment(SwingConstants.CENTER);
        in92.setName("in92");
        p_Indicateur.add(in92, cc.xy(7, 21));
        
        // ---- in93 ----
        in93.setText("0");
        in93.setHorizontalAlignment(SwingConstants.CENTER);
        in93.setName("in93");
        p_Indicateur.add(in93, cc.xy(9, 21));
        
        // ---- in94 ----
        in94.setText("0");
        in94.setHorizontalAlignment(SwingConstants.CENTER);
        in94.setName("in94");
        p_Indicateur.add(in94, cc.xy(11, 21));
        
        // ---- in95 ----
        in95.setText("0");
        in95.setHorizontalAlignment(SwingConstants.CENTER);
        in95.setName("in95");
        p_Indicateur.add(in95, cc.xy(13, 21));
        
        // ---- in96 ----
        in96.setText("0");
        in96.setHorizontalAlignment(SwingConstants.CENTER);
        in96.setName("in96");
        p_Indicateur.add(in96, cc.xy(15, 21));
        
        // ---- in97 ----
        in97.setText("0");
        in97.setHorizontalAlignment(SwingConstants.CENTER);
        in97.setName("in97");
        p_Indicateur.add(in97, cc.xy(17, 21));
        
        // ---- in98 ----
        in98.setText("0");
        in98.setHorizontalAlignment(SwingConstants.CENTER);
        in98.setName("in98");
        p_Indicateur.add(in98, cc.xy(19, 21));
        
        // ---- in99 ----
        in99.setText("0");
        in99.setHorizontalAlignment(SwingConstants.CENTER);
        in99.setName("in99");
        p_Indicateur.add(in99, cc.xy(21, 21));
      }
      
      // ---- ts_Options ----
      ts_Options.setTitle("Options");
      ts_Options.setName("ts_Options");
      
      // ---- ckb_OnTop ----
      ckb_OnTop.setText("Toujours au-dessus");
      ckb_OnTop.setName("ckb_OnTop");
      ckb_OnTop.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          ckb_OnTopActionPerformed(e);
        }
      });
      
      // ---- ts_Focus ----
      ts_Focus.setTitle("Focus");
      ts_Focus.setName("ts_Focus");
      
      // ---- l_Focus ----
      l_Focus.setText("null");
      l_Focus.setName("l_Focus");
      
      // ---- WABLC ----
      WABLC.setText("WABLC:");
      WABLC.setFont(WABLC.getFont().deriveFont(WABLC.getFont().getStyle() | Font.BOLD));
      WABLC.setName("WABLC");
      
      // ---- l_WABLC ----
      l_WABLC.setText("text");
      l_WABLC.setName("l_WABLC");
      
      // ---- l_libelleprg ----
      l_libelleprg.setText("Libelle prg");
      l_libelleprg.setHorizontalAlignment(SwingConstants.CENTER);
      l_libelleprg.setForeground(Color.blue);
      l_libelleprg.setFont(l_libelleprg.getFont().deriveFont(l_libelleprg.getFont().getStyle() | Font.BOLD));
      l_libelleprg.setName("l_libelleprg");
      
      // ---- bt_Variables ----
      bt_Variables.setText("Variables >>");
      bt_Variables.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Variables.setFont(new Font("Courier New", Font.PLAIN, 12));
      bt_Variables.setName("bt_Variables");
      bt_Variables.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_VariablesActionPerformed(e);
        }
      });
      
      GroupLayout p_InfosGeneriquesLayout = new GroupLayout(p_InfosGeneriques);
      p_InfosGeneriques.setLayout(p_InfosGeneriquesLayout);
      p_InfosGeneriquesLayout
          .setHorizontalGroup(p_InfosGeneriquesLayout.createParallelGroup()
              .addGroup(p_InfosGeneriquesLayout.createSequentialGroup()
                  .addGroup(p_InfosGeneriquesLayout.createParallelGroup()
                      .addComponent(l_libelleprg, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                      .addGroup(p_InfosGeneriquesLayout.createSequentialGroup().addContainerGap()
                          .addGroup(p_InfosGeneriquesLayout.createParallelGroup().addGroup(p_InfosGeneriquesLayout.createSequentialGroup()
                              .addGroup(p_InfosGeneriquesLayout.createParallelGroup()
                                  .addComponent(RPG, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(DSPF, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(Record, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE).addComponent(WABLC))
                              .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                              .addGroup(p_InfosGeneriquesLayout.createParallelGroup()
                                  .addComponent(l_RPG, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(l_DSPF, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(l_Record, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(l_WABLC)))
                              .addComponent(ts_Indicateur, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
                              .addComponent(p_Indicateur, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                              .addComponent(ts_Focus, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
                              .addComponent(l_Focus, GroupLayout.PREFERRED_SIZE, 148, GroupLayout.PREFERRED_SIZE)
                              .addGroup(p_InfosGeneriquesLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                  .addComponent(bt_Variables, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE,
                                      GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                  .addComponent(ts_Options, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 156,
                                      Short.MAX_VALUE))
                              .addComponent(ckb_OnTop))))
                  .addContainerGap()));
      p_InfosGeneriquesLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] { l_Record, l_WABLC });
      p_InfosGeneriquesLayout.setVerticalGroup(p_InfosGeneriquesLayout.createParallelGroup().addGroup(p_InfosGeneriquesLayout
          .createSequentialGroup().addComponent(l_libelleprg).addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addGroup(p_InfosGeneriquesLayout.createParallelGroup().addComponent(RPG).addComponent(l_RPG)).addGap(5, 5, 5)
          .addGroup(p_InfosGeneriquesLayout.createParallelGroup().addComponent(DSPF).addComponent(l_DSPF)).addGap(5, 5, 5)
          .addGroup(p_InfosGeneriquesLayout.createParallelGroup().addComponent(Record).addComponent(l_Record))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addGroup(p_InfosGeneriquesLayout.createParallelGroup(GroupLayout.Alignment.BASELINE).addComponent(WABLC).addComponent(l_WABLC))
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(ts_Indicateur, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(p_Indicateur, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(ts_Focus, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(l_Focus)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
          .addComponent(ts_Options, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(ckb_OnTop)
          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(bt_Variables)
          .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
    }
    contentPane.add(p_InfosGeneriques, BorderLayout.WEST);
    
    // ======== p_Variables ========
    {
      p_Variables.setName("p_Variables");
      p_Variables.setLayout(new BorderLayout());
      
      // ======== panel2 ========
      {
        panel2.setPreferredSize(new Dimension(370, 50));
        panel2.setName("panel2");
        panel2.setLayout(null);
        
        // ---- l_Warning ----
        l_Warning.setForeground(Color.red);
        l_Warning.setFont(l_Warning.getFont().deriveFont(l_Warning.getFont().getStyle() | Font.BOLD));
        l_Warning.setText(" ");
        l_Warning.setName("l_Warning");
        panel2.add(l_Warning);
        l_Warning.setBounds(5, 0, 370, l_Warning.getPreferredSize().height);
        
        // ---- cb_ListeRcd ----
        cb_ListeRcd.setName("cb_ListeRcd");
        cb_ListeRcd.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            cb_ListeRcdActionPerformed(e);
          }
        });
        panel2.add(cb_ListeRcd);
        cb_ListeRcd.setBounds(5, 17, 175, 28);
        
        // ---- bt_Vider ----
        bt_Vider.setText("Tout vider");
        bt_Vider.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Vider.setToolTipText("Supprime tous les buffers m\u00e9moris\u00e9s");
        bt_Vider.setName("bt_Vider");
        bt_Vider.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_ViderActionPerformed(e);
          }
        });
        panel2.add(bt_Vider);
        bt_Vider.setBounds(new Rectangle(new Point(185, 17), bt_Vider.getPreferredSize()));
        
        // ---- l_LibelleFiltre ----
        l_LibelleFiltre.setText("Filtre");
        l_LibelleFiltre.setName("l_LibelleFiltre");
        panel2.add(l_LibelleFiltre);
        l_LibelleFiltre.setBounds(275, 23, 32, l_LibelleFiltre.getPreferredSize().height);
        
        // ---- tf_Filtre ----
        tf_Filtre.setName("tf_Filtre");
        tf_Filtre.addKeyListener(new KeyAdapter() {
          @Override
          public void keyReleased(KeyEvent e) {
            tf_FiltreKeyReleased(e);
          }
        });
        panel2.add(tf_Filtre);
        tf_Filtre.setBounds(310, 17, 92, tf_Filtre.getPreferredSize().height);
        
        // ---- l_LibelleFMT ----
        l_LibelleFMT.setText("Vecteur FMT:");
        l_LibelleFMT.setName("l_LibelleFMT");
        panel2.add(l_LibelleFMT);
        l_LibelleFMT.setBounds(new Rectangle(new Point(415, 23), l_LibelleFMT.getPreferredSize()));
        
        // ---- l_FMT ----
        l_FMT.setText("text");
        l_FMT.setFont(l_FMT.getFont().deriveFont(l_FMT.getFont().getStyle() | Font.BOLD));
        l_FMT.setName("l_FMT");
        panel2.add(l_FMT);
        l_FMT.setBounds(490, 23, 35, l_FMT.getPreferredSize().height);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      p_Variables.add(panel2, BorderLayout.NORTH);
      
      // ======== scrollPane1 ========
      {
        scrollPane1.setName("scrollPane1");
        
        // ---- t_Variables ----
        t_Variables.setModel(new DefaultTableModel());
        t_Variables.setFont(new Font("Courier New", Font.PLAIN, 12));
        t_Variables.setName("t_Variables");
        scrollPane1.setViewportView(t_Variables);
      }
      p_Variables.add(scrollPane1, BorderLayout.CENTER);
    }
    contentPane.add(p_Variables, BorderLayout.CENTER);
    
    // ---- l_Monitor ----
    l_Monitor.setText("text");
    l_Monitor.setHorizontalAlignment(SwingConstants.LEFT);
    l_Monitor.setName("l_Monitor");
    contentPane.add(l_Monitor, BorderLayout.SOUTH);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }

  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_InfosGeneriques;
  private JLabel RPG;
  private JLabel DSPF;
  private JLabel l_RPG;
  private JLabel l_DSPF;
  private JLabel l_Record;
  private JLabel Record;
  private JXTitledSeparator ts_Indicateur;
  private JPanel p_Indicateur;
  private JLabel x00;
  private JLabel x01;
  private JLabel x02;
  private JLabel x03;
  private JLabel x04;
  private JLabel x05;
  private JLabel x06;
  private JLabel x07;
  private JLabel x08;
  private JLabel x09;
  private JLabel y00;
  private JLabel y01;
  private JLabel y02;
  private JLabel y03;
  private JLabel y04;
  private JLabel y05;
  private JLabel y06;
  private JLabel y07;
  private JLabel y08;
  private JLabel y09;
  private JLabel in01;
  private JLabel in02;
  private JLabel in03;
  private JLabel in04;
  private JLabel in05;
  private JLabel in06;
  private JLabel in07;
  private JLabel in08;
  private JLabel in09;
  private JLabel in10;
  private JLabel in11;
  private JLabel in12;
  private JLabel in13;
  private JLabel in14;
  private JLabel in15;
  private JLabel in16;
  private JLabel in17;
  private JLabel in18;
  private JLabel in19;
  private JLabel in20;
  private JLabel in21;
  private JLabel in22;
  private JLabel in23;
  private JLabel in24;
  private JLabel in25;
  private JLabel in26;
  private JLabel in27;
  private JLabel in28;
  private JLabel in29;
  private JLabel in30;
  private JLabel in31;
  private JLabel in32;
  private JLabel in33;
  private JLabel in34;
  private JLabel in35;
  private JLabel in36;
  private JLabel in37;
  private JLabel in38;
  private JLabel in39;
  private JLabel in40;
  private JLabel in41;
  private JLabel in42;
  private JLabel in43;
  private JLabel in44;
  private JLabel in45;
  private JLabel in46;
  private JLabel in47;
  private JLabel in48;
  private JLabel in49;
  private JLabel in50;
  private JLabel in51;
  private JLabel in52;
  private JLabel in53;
  private JLabel in54;
  private JLabel in55;
  private JLabel in56;
  private JLabel in57;
  private JLabel in58;
  private JLabel in59;
  private JLabel in60;
  private JLabel in61;
  private JLabel in62;
  private JLabel in63;
  private JLabel in64;
  private JLabel in65;
  private JLabel in66;
  private JLabel in67;
  private JLabel in68;
  private JLabel in69;
  private JLabel in70;
  private JLabel in71;
  private JLabel in72;
  private JLabel in73;
  private JLabel in74;
  private JLabel in75;
  private JLabel in76;
  private JLabel in77;
  private JLabel in78;
  private JLabel in79;
  private JLabel in80;
  private JLabel in81;
  private JLabel in82;
  private JLabel in83;
  private JLabel in84;
  private JLabel in85;
  private JLabel in86;
  private JLabel in87;
  private JLabel in88;
  private JLabel in89;
  private JLabel in90;
  private JLabel in91;
  private JLabel in92;
  private JLabel in93;
  private JLabel in94;
  private JLabel in95;
  private JLabel in96;
  private JLabel in97;
  private JLabel in98;
  private JLabel in99;
  private JXTitledSeparator ts_Options;
  private JCheckBox ckb_OnTop;
  private JXTitledSeparator ts_Focus;
  private JLabel l_Focus;
  private JLabel WABLC;
  private JLabel l_WABLC;
  private JLabel l_libelleprg;
  private JButton bt_Variables;
  private JPanel p_Variables;
  private JPanel panel2;
  private JLabel l_Warning;
  private JComboBox cb_ListeRcd;
  private JButton bt_Vider;
  private JLabel l_LibelleFiltre;
  private JTextField tf_Filtre;
  private JLabel l_LibelleFMT;
  private JLabel l_FMT;
  private JScrollPane scrollPane1;
  private JTable t_Variables;
  private JLabel l_Monitor;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
