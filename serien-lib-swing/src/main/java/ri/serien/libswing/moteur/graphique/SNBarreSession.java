/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.graphique;

import java.awt.FlowLayout;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Collections;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Ce composant graphique correspond à la barre qui est affichée en bas de l'écran.
 * Cette barre contient les boutons qui sont associés aux sessions.
 */
public class SNBarreSession extends JPanel {
  // Constantes
  private static final int HAUTEUR_BARRE = 40;
  public static final int LARGEUR_BOUTON = 180;
  public static final int HAUTEUR_BOUTON = HAUTEUR_BARRE;

  // Variables
  private ButtonGroup btgBoutonSession = new ButtonGroup();
  private ListeBoutonSession listeBoutonSession = new ListeBoutonSession();

  /**
   * Constructeur.
   */
  public SNBarreSession() {
    super();
    initialiserComposant();
  }

  // -- Méthodes publiques

  /**
   * Ajouter un bouton à la barre de sessions.
   * Le bouton qui vient d'être ajouter est automatiquement actif c'est à dire que c'est cette session qui sera affichée.
   */
  public void ajouterBouton(SNBoutonSession pSNBoutonSession) {
    if (pSNBoutonSession == null) {
      throw new MessageErreurException("Le bouton à ajouter à la barre de sessions est invalide.");
    }

    // Ajouter le bouton à la liste
    listeBoutonSession.add(pSNBoutonSession);
    btgBoutonSession.add(pSNBoutonSession);
    pSNBoutonSession.setSelected(true);

    // Rafraîchir le composant
    rafraichirBouton();
  }

  /**
   * Supprimer un bouton à la barre de sessions.
   */
  public void supprimerBouton(IdSession pIdSession) {
    if (pIdSession == null) {
      throw new MessageErreurException("L'id session est invalide.");
    }
    SNBoutonSession boutonSession = listeBoutonSession.retrouverBoutonAvecIdSession(pIdSession);
    if (boutonSession == null) {
      throw new MessageErreurException("Le bouton à supprimer de la barre de sessions est invalide.");
    }

    // Controle si le bouton était sélectionné
    int indice = -1;
    if (boutonSession.isSelected()) {
      indice = listeBoutonSession.getIndexBoutonSelectionne();
    }

    // Supprimer le bouton à la liste
    listeBoutonSession.remove(boutonSession);
    btgBoutonSession.remove(boutonSession);
    // Si le bouton supprimé était sélectionné alors sélection du bouton juste avant celui qui vient d'être supprimé
    if (indice > -1 && !listeBoutonSession.isEmpty()) {
      // Si ce n'est pas le premier bouton alors décrémentation de l'indice
      if (indice > 0) {
        indice--;
      }
      listeBoutonSession.get(indice).setSelected(true);
    }

    // Rafraîchir le composant
    rafraichirBouton();
  }

  /**
   * Réorganise les boutons de la barre après un déplacement d'un des boutons.
   */
  public void trierBouton() {
    // La liste des boutons est triée sur l'ordre de positionnement sur l'axe des X (de gauche à droite)
    Collections.sort(listeBoutonSession);

    // Rafraîchir le composant
    rafraichirBouton();
  }

  /**
   * Réorganise les boutons sur l'axe des Z.
   * Celui qui a la valeur la plus petite est au-dessus des autres.
   * Attention cela peut avoir un impact sur les coordonnées X du bouton c'est pour cela que l'on stocke l'abscisse lors des déplacements.
   */
  public void modifierOrdreProfondeur(SNBoutonSession pBouton) {
    if (pBouton == null) {
      return;
    }

    int zorder = 1;
    for (SNBoutonSession snBoutonSession : listeBoutonSession) {
      if (snBoutonSession.equals(pBouton)) {
        setComponentZOrder(snBoutonSession, 0);
      }
      else {
        setComponentZOrder(snBoutonSession, zorder);
      }
    }
  }

  /**
   * Sélectionne la session à partir de son identifiant de session.
   */
  public void selectionnerSession(IdSession pIdSession) {
    if (listeBoutonSession == null) {
      return;
    }
    SNBoutonSession boutonSession = listeBoutonSession.retrouverBoutonAvecIdSession(pIdSession);
    if (boutonSession != null) {
      boutonSession.setSelected(true);
    }
  }

  /**
   * Efface les nouvelles coordonnées qui vont être calculée lors d'un déplacement.
   */
  public void effacerNouvelleCoordonnee() {
    if (listeBoutonSession == null) {
      return;
    }
    listeBoutonSession.effacerNouvelleCoordonnee();
  }

  /**
   * Rafraîchir l'ensemble des boutons.
   */
  public void rafraichirBouton() {
    // Contrôle la largeur des boutons
    verifierLargeurBouton(false);

    // Effacer tous les boutons
    removeAll();

    if (listeBoutonSession.size() > 0) {
      // Ajouter les boutons à la barre
      for (SNBoutonSession snBoutonSession : listeBoutonSession) {
        add(snBoutonSession);
      }
      setVisible(true);
    }
    else {
      // Il n'y a aucun bouton à afficher donc la barre est cachée
      setVisible(false);
    }

    // Forcer la rafraîchissement du composant
    revalidate();
  }

  /**
   * Permet de stocker provisoirement l'abscisse pendant un déplacement de bouton car les coordonnées orginales sont recalculées par le
   * système lors de l'arrêt du déplacement et donc on ne peut plus s'en servir pour trier les boutons.
   */
  public void stockerNouvelleAbscisse() {
    for (SNBoutonSession snBoutonSession : listeBoutonSession) {
      snBoutonSession.setNouvelleCoordonneeX(Integer.valueOf(snBoutonSession.getX()));
    }
  }

  // -- Méthodes privées

  /**
   * Configurer la barre de sessions.
   */
  private void initialiserComposant() {
    // Définir la couleur de fond
    setBackground(SNCharteGraphique.COULEUR_BARRE_SESSION);

    // Définir le layout
    setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));

    // L'évènement de redimensionnement pour recalculer la largeur des boutons de session
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentResized(ComponentEvent e) {
        verifierLargeurBouton(true);
      }
    });

  }

  /**
   * Vérifie que la largeur des boutons en fonction de la largeur de la barre de session.
   * Par défaut un bouton fait 180 pixels mais cette largeur peut être diminuée en fonction du nombre de boutons contenus dans
   * la barre et la largeur de cette barre.
   */
  private void verifierLargeurBouton(boolean pForcerMiseAJourGraphique) {
    int largeurBarreSession = getWidth();
    // Calcul de la largeur de l'ensemble des boutons avec la largeur idéale
    int largeurEnsembleBouton = LARGEUR_BOUTON * listeBoutonSession.size();
    // La largeur de l'ensemble des boutons est inférieure à la largeur de la barre de session donc il n'y a rien à faire
    if (largeurEnsembleBouton <= largeurBarreSession) {
      return;
    }

    // La largeur de l'ensemble des boutons est supérieure à la largeur de la barre de session donc il faut recalculer une largeur
    // plus petite pour les boutons
    int nouvelleLargeur = largeurBarreSession / listeBoutonSession.size();
    // Modification des dimensions de tous les boutons
    for (SNBoutonSession snBoutonSession : listeBoutonSession) {
      snBoutonSession.modifierDimension(nouvelleLargeur, HAUTEUR_BOUTON);
    }
    // La mise à jour graphique automatique n'est valable que dans certain cas: le redimensionnement par exemple
    if (pForcerMiseAJourGraphique) {
      revalidate();
    }
  }

}
