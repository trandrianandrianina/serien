/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.composant;

import java.awt.AWTEvent;

/**
 * Evènements émis par les composants SN pour notifier que la valeur du composant a changé.
 */
public class SNComposantEvent extends AWTEvent {
  
  /**
   * Constructeur standard.
   */
  public SNComposantEvent(Object pObjetSource) {
    super(pObjetSource, 0);
  }
}
