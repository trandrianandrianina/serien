/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.composant;

import java.util.EventListener;

/**
 * Interface pour être notifié des évènements issus de SNPanelComposant.
 */
public interface InterfaceSNComposantListener extends EventListener {
  /**
   * Evènement pour signaler que la valeur du composant a changé.
   */
  public void valueChanged(SNComposantEvent pSNComposantEvent);
}
