/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc.panel;

import java.awt.Cursor;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.mvc.ChargementDonnees;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.InterfaceModele;
import ri.serien.libswing.moteur.mvc.InterfaceVue;

/**
 * Vue associée à un composant JPanel.
 *
 * Une vue contient toujours un lien vers le modèle associé. C'est ce qui permet de lire les données du modèle pour rafraîchir l'affichage
 * et qui permet d'appeler les méthodes évênementiels du modèles lors d'une action graphique de l'utilisateur.
 *
 * La vue peut être utilisée seule, être enfante d'un vue parente ou au contraire contenir plusieurs vues enfantzs. Il est également
 * possible que la vue soit à la fois enfant d'une vue parente et contienne plusieurs vues enfants. Dans tous les cas, les méthodes
 * initialiserComposants() et rafraichir() sont à surcharger normalement.
 *
 * Pour une vue parent contenant une vue liste et une liste détail :
 * - Utiliser un CardLayout dans un JPanel de l'écran.
 * - Ajouter les vues au CardLayout dans la méthode initialiserComposants() :
 * Exemple : pnlContenu.add(getAbstractVueListe(), EnumModeListeDetail.LISTE.getCode());
 * - Gérer le permutation des vues dans la méthode Rafraichir() :
 * Exemple : cardLayout.show(pnlContenu, EnumModeListeDetail.LISTE.getCode());
 */
public abstract class AbstractVuePanel<M extends InterfaceModele> extends JPanel implements InterfaceVue<M>, Observer {
  private final M modele;
  private InterfaceVue<?> vueParent = null;
  private Map<Object, InterfaceVue<?>> listeVueEnfant = new HashMap<Object, InterfaceVue<?>>();
  private boolean composantInitialise = false;
  private boolean protectionReentrance = false;
  private boolean evenementsActifs = false;

  /**
   * Constructeur.
   *
   * @param pModele Modèle associé à la vue.
   */
  public AbstractVuePanel(M pModele) {
    super();

    // Vérifier que le modèle n'est pas nul
    if (pModele == null) {
      throw new MessageErreurException("Impossible de créer la vue car le modèle est invalide.");
    }

    // Modifier le modèle de la vue
    modele = pModele;

    // Etre notifier lorsque le modèle est modifié (pour faire le rafraichissement)
    modele.abonnerVue(this);
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes abstraites à implémenter
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Initialiser les composants graphiques de la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public abstract void initialiserComposants();

  /**
   * Rafraichir les données de la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public abstract void rafraichir();

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes d'InterfaceVue
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Afficher la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void afficher() {
    try {
      // Vérifier que la méthode a bien été appelée dans la thread graphique
      if (Trace.isModeDebug() && !SwingUtilities.isEventDispatchThread()) {
        Trace.debugStackTrace();
        throw new MessageErreurException(
            "La méthode " + getClass().getSimpleName() + ".afficher() a été appelé en dehors de la thread graphique (EDT).");
      }

      // Afficher le curseur d'attente
      // Le curseur standard sera remis lors du rafraichissement effectué après le chargement des données
      setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

      // Indiquer au modèle que les données ne sont pas chargées.
      modele.setDonneesChargees(false);

      // Tracer
      Trace.debug(modele.getClass(), "initialiserDonnees");

      // Initialiser les données du modèle
      // L'idée est de nettoyer rapidement les données avant le premier rafraîchissement et avant le chargement des données lourdes.
      modele.initialiserDonnees();

      // Initialiser les composants graphiques
      initialiserComposantsAvecEnfants();

      // Rafraîchir les données (les composants graphiques sont créés et les données sont rafraîchies à vide).
      // Le chargement des données n'est pas fait à ce stade. L'objectif est d'afficher les composants rapidement même s'ils sont vides.
      rafraichirComposantsAvecEnfants();

      // Tracer
      Trace.debug(modele.getClass(), "chargerDonnees");

      // Lancer le chargement des données (dans une thread à part)
      // Lorsque le chargement des données est terminé, la méthode notifier() est appelée afin de rafraichir les données
      ChargementDonnees chargementDonnees = new ChargementDonnees(modele);
      chargementDonnees.execute();

      // Afficher le panel
      setVisible(true);
    }
    catch (Exception e) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(e);
    }
  }

  /**
   * Initialiser les composants graphiques de la vue et des vues enfants.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public void initialiserComposantsAvecEnfants() {
    try {
      // Activer la protection contre la réentrance
      protectionReentrance = true;

      // Désactiver la gestion des évènements
      evenementsActifs = false;

      // Tracer
      Trace.debug(getClass(), "initialiserComposants");

      // Appeler la méthode initialiserComposants de la vue
      initialiserComposants();

      // Nommer la fenêtre du nom de sa classe Vue (pour QFTest)
      // Cette étape doit être effectuée après initialiserComposants() car JFormDesigner donne le nom "this" au composant.
      setName(getClass().getSimpleName());

      // Initialiser les composants des vues enfants
      for (InterfaceVue<?> interfaceVue : listeVueEnfant.values()) {
        interfaceVue.initialiserComposantsAvecEnfants();
      }
      
      // Indiquer que le composant a été initialisé
      composantInitialise = true;
    }
    catch (Exception e) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(e);
    }
    finally {
      // Réactiver la gestion des évènements
      evenementsActifs = true;

      // Désactiver la protection contre la réentrance avant le chargement des données
      protectionReentrance = false;
    }
  }

  /**
   * Rafraîchir les données des composants graphiques.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void rafraichirComposants() {
    try {
      // Ne pas déclencher de rafraîchissement pour les vues enfantes car c'est la vue parente qui le fait
      if (vueParent != null) {
        return;
      }

      // Vérifier que la méthode a bien été appelée dans la thread graphique
      if (Trace.isModeDebug() && !SwingUtilities.isEventDispatchThread()) {
        Trace.debugStackTrace();
        throw new MessageErreurException(
            "La méthode " + getClass().getSimpleName() + ".rafraichir() a été appelé en dehors de la thread graphique (EDT).");
      }

      // Rafraîchir les données
      rafraichirComposantsAvecEnfants();

      // Afficher le curseur par défaut
      // Utile pour remettre le curseur standard suite au chargement des données
      setCursor(Cursor.getDefaultCursor());
    }
    catch (Exception exception) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(exception);
    }
  }

  /**
   * Rafraichir les données des composants graphiques de la vue et des vues enfants.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public void rafraichirComposantsAvecEnfants() {
    try {
      // Initialiser les composants si ceux-ci ne l'ont pas encore été.
      // Le plus souvent, les composants des vues enfants sont initialisés avec ceux de la vue principale dans la méthode afficher().
      // Toutefois, si la vue enfant a été ajoutée de façon dynamique, après création de l'écran, elle n'aura pas été initialisée
      // et il faut le faire ici avant le premier rafraichissement.
      if (!composantInitialise) {
        initialiserComposantsAvecEnfants();
      }
      
      // Activer la protection contre la réentrance
      if (Trace.isModeDebug() && protectionReentrance) {
        throw new MessageErreurException(
            "La méthode " + getClass().getSimpleName() + ".rafraichir() a été appelé en cours de rafraîchissement de l'écran. "
                + "Cela a provoqué une réeentrance. Protéger les méthodes évènementielles avec isEvenementsActifs() pour éviter cela.");
      }

      // Activer la protection contre la réentrance
      protectionReentrance = true;

      // Désactiver la gestion des évènements (avant initialiserComposants et rafraichir).
      evenementsActifs = false;

      // Tracer
      Trace.debug(getClass(), "rafraichir");

      // Appeler la méthode rafraichir() de la vue
      rafraichir();

      // Rafraichir la vue enfant active
      // A faire après le rafraichissement de la vue parent car la vue parent peut changer la vue active lors de son rafraichissement
      InterfaceVue<?> vueEnfantActive = getVueEnfantActive();
      if (vueEnfantActive != null) {
        vueEnfantActive.rafraichirComposantsAvecEnfants();
      }
    }
    catch (Exception e) {
      setCursor(Cursor.getDefaultCursor());
      DialogueErreur.afficher(e);
    }
    finally {
      // Réactiver la gestion des évènements
      evenementsActifs = true;

      // Désactiver la protection contre la réentrance avant le chargement des données
      protectionReentrance = false;
    }
  }

  /**
   * Cacher la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void cacher() {
    setVisible(false);
  }

  /**
   * Modèle associé à la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final M getModele() {
    return modele;
  }

  /**
   * Ajouter une vue enfant à cette vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void ajouterVueEnfant(InterfaceCleVue pCle, InterfaceVue<?> pVueEnfant) {
    // Vérifier les paramètres
    if (pCle == null) {
      throw new MessageErreurException("Impossible d'ajouter la vue enfante car sa clé est invalide.");
    }
    if (pVueEnfant == null) {
      throw new MessageErreurException("Impossible d'ajouter la vue enfante car elle est invalide.");
    }
    if (listeVueEnfant.containsKey(pCle)) {
      throw new MessageErreurException(
          "Impossible d'ajouter la vue enfante car sa clé est déjà présente dans la liste des vues enfantes.");
    }
    if (listeVueEnfant.containsValue(pVueEnfant)) {
      throw new MessageErreurException("Impossible d'ajouter la vue enfante car elle est déjà présente dans la liste des vues enfantes.");
    }
    if (pVueEnfant.getModele() != getModele()) {
      throw new MessageErreurException("Impossible d'ajouter la vue enfante car son modèle est différent de celui de la vue parente.");
    }

    // Ajouter la vue enfant à la liste de la vue parent
    listeVueEnfant.put(pCle, pVueEnfant);

    // Renseigner la vue parent de l'enfant
    if (pVueEnfant.getVueParent() != this) {
      pVueEnfant.setVueParent(this);
    }
  }

  /**
   * Vue parente de la vue.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final InterfaceVue<?> getVueParent() {
    return vueParent;
  }

  /**
   * Définir la vue parente d'une vue enfant.
   * Voir InterfaceVue pour plus d'informations.
   */
  @Override
  public final void setVueParent(InterfaceVue<?> pVueParent) {
    // Vérifier les paramètres
    if (pVueParent == null) {
      throw new MessageErreurException("Impossible de renseigner la vue parente car elle est invalide.");
    }
    if (vueParent != null) {
      throw new MessageErreurException("Impossible de renseigner la vue parente car la vue enfante a déjà une vue parente.");
    }

    // Renseigner la vue parent de l'enfant
    vueParent = pVueParent;
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes de l'interface Observer
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Mettre à jour les données de la vue.
   * Cette méthode est appelée lorsque les données du modèle évolue et qu'il notifie ces observateurs.
   */
  @Override
  public final void update(Observable o, Object arg) {
    rafraichirComposants();
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Fournir la vue enfant active.
   *
   * L'information est détenue par le modèle. Cette méthode interroge le modèle pour récupérer la clé de la vue active et recherche
   * la vue correspondante à la clé dans la liste. Noter que la vue parente et les vues enfantes partagent le même modèle si
   * bien qu'il est possible que la vue active ne soit pas trouvée dans cette vue car elle serait dans une autre.
   *
   * @return Vue enfant active (null si aucune)
   */
  private InterfaceVue<?> getVueEnfantActive() {
    // Récupérer l'identifiant de la vue active
    InterfaceCleVue interfaceCleVue = modele.getCleVueEnfantActive();
    if (interfaceCleVue == null) {
      return null;
    }
    
    // Récupérer l'interface de la vue (si présente dans cette vue et pas dans une autre)
    return listeVueEnfant.get(interfaceCleVue);
  }

  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------

  /**
   * Indiquer si les évènements doivent être traités.
   *
   * @return true=traiter les évènements graphiques, false=ne pas traiter les évènements graphiques.
   */
  public final boolean isEvenementsActifs() {
    return evenementsActifs && getModele().isDonneesChargees();
  }

  /**
   * Vérifier si les données du modèle sont chargées.
   *
   * Lors de l'affichage initial, la valeur est mise à false avant initialiserDonnees() et mise à true après chargerDonnees().
   * Lors des chargements ultérieurs de données, la valeur est mise à false avant l'appel à chargerDonnees() et remise à true lorsque
   * chargerDonnees() est terminé.
   *
   * @return true=les données du modèles sont chargées, false=les données du modèle ne soint pas chargées.
   */
  public final boolean isDonneesChargees() {
    return modele.isDonneesChargees();
  }
}
