/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.composant.plage;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.composant.SNComposantAvecEtablissement;

/**
 * Classe abstraite des composants de plages de recherche.<br>
 * Tous les composants d'une plage de recherche doivent dériver de cette classe.<br>
 * Cette classe dérive de SNComposantAvecEtablissement ce qui lui permet de bénéficier du mécanisme d'abonnement aux évènements
 * lorsque la valeur du composant change.<br>
 * Un composant de début a une liaison avec un composant de fin et n'a pas de lien avec un composant de début. De même, un composant de
 * fin a une liaison avec un composant de début mais pas avec un composant de fin. De ce fait, un composant de plage ne peut pas être lié
 * avec un composant de début et de fin en même temps.<br>
 * Utilisation :<br>
 * - Créer un nouveau composant de plage qui étend SNComposantPlage.<br>
 * - Initialiser la liaison du composant plage depuis le composé plage.<br>
 * - Exploiter les données du composant lié dans le critère de recherche du composant.<br><br>
 * Voir un exemple d'implémention avec le composant SNFournisseur.
 */
public abstract class SNComposantPlage extends SNComposantAvecEtablissement {
  protected SNComposantPlage snComposantDebut = null;
  protected SNComposantPlage snComposantFin = null;

  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public SNComposantPlage() {
    super();
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Lier le composant en cours par un composant de début.
   * @param pSnComposantDebut Nouveau composant de début.
   */
  public void lierComposantDebut(SNComposantPlage pSnComposantDebut) {
    if (pSnComposantDebut == this) {
      throw new MessageErreurException("Impossible de lier un composant de plage à lui même.");
    }
    
    if (Constantes.equals(pSnComposantDebut, snComposantDebut)) {
      return;
    }
    
    // Défaire le lien avec l'ancien composant de début.
    SNComposantPlage ancienSnComposantPlageDebut = snComposantDebut;
    if (ancienSnComposantPlageDebut != null) {
      ancienSnComposantPlageDebut.lierComposantFin(null);
    }
    
    // Faire le Lien avec le nouveau composant de début.
    snComposantDebut = pSnComposantDebut;
    if (snComposantDebut != null) {
      snComposantDebut.lierComposantFin(this);
    }
  }
  
  /**
   * Lier le composant en cours par un composant de fin.
   * @param pSnComposantFin Nouveau composant de fin.
   */
  public void lierComposantFin(SNComposantPlage pSnComposantFin) {
    if (pSnComposantFin == this) {
      throw new MessageErreurException("Impossible de lier un composant de plage à lui même.");
    }
    
    if (Constantes.equals(pSnComposantFin, snComposantFin)) {
      return;
    }
    
    // Défaire le lien avec l'ancien composant de fin.
    SNComposantPlage ancienSnComposantPlageFin = snComposantFin;
    if (ancienSnComposantPlageFin != null) {
      ancienSnComposantPlageFin.lierComposantDebut(null);
    }
    
    // Faire le lien avec le nouveau composant de fin.
    snComposantFin = pSnComposantFin;
    if (snComposantFin != null) {
      snComposantFin.lierComposantDebut(this);
    }
  }
  
  /**
   * Identifier le composant de début d'une plage.<br>
   * Un composant de début possède un composant de fin qui lui est lié lors de son initialisation.
   * @return
   *         true si ce composant est le début d'une plage.<br>
   *         false si ce composant n'est pas le début d'une plage.
   */
  public boolean isComposantDeDebut() {
    return snComposantFin != null;
  }
  
  /**
   * Identifier le composant de fin d'une plage.<br>
   * Un composant de fin possède un composant de début qui lui est lié lors de son initialisation.
   * @return
   *         true si ce composant est la fin d'une plage.<br>
   *         false si ce composant n'est pas la fin d'une plage.
   */
  public boolean isComposantDeFin() {
    return snComposantDebut != null;
  }
}
