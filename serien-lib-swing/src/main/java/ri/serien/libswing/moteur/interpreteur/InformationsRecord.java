/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.interpreteur;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.iIndicator;
import ri.serien.libcommun.outils.composants.ioGeneric;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oKey;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libcommun.outils.fichier.GestionFichierDAText;

/**
 * Informations complètes pour un record
 */
public class InformationsRecord {
  // A mettre à jour dans Lexical si modification
  private final static int MODE_INDEFINI = 0;
  private final static int MODE_CREATION = 1;
  private final static int MODE_MODIFICATION = 2;
  private final static int MODE_CONSULTATION = 3;
  private final static int MODE_ANNULATION = 4;
  private final static int MODE_DUPLICATION = 5;
  
  public static final String NOMPACKAGE = "ri.serien.libecranrpg.";
  
  // Variables
  private oRecord descriptionRecord = null;
  public ArrayList<oKey> listeKey = new ArrayList<oKey>();
  private iIndicator iIndicateur = new iIndicator();
  public int mode = MODE_INDEFINI;
  
  private String messageID = null; // 5
  private String numjob = null; // 10
  private String nomRecord = null; // 10
  private String profil = null; // 10
  private String module = null; // 3
  private String nomProgramme = null; // 10
  private char blocageEcran = ' '; // 1 = 0:non bloquant non affiche, 1:bloquant affiche, 2:non bloquant affiche
  private char erreurPrg = '0'; // 1 = 1: erreur
  private String nomFichierPanel = null; // 20
  private String reserve = null; // 10
  
  private String nomFormat = null; // 10
  private char recordAlternatif = ' '; // 1
  private String vFmt = null;
  
  private char indicateurs[] = new char[FluxRecord.LONGUEUR_INDICATEUR];// 99
  
  private String nomPaquet = null;
  private String nomFichierAide = null;
  private String nomFichierFMT = null;
  private String classname = null;
  private int idclassname = -1;
  private String nompanel = null;
  private String nomFormatRecord = null;
  private String buffer = null;
  private String racinePackage = null;
  
  private String msgErreur = null;
  
  /**
   * Constructeur
   * @param idclassname
   * @param classname
   */
  public InformationsRecord(int idclassname, String classname) {
    this.idclassname = idclassname;
    this.classname = classname;
  }
  
  /**
   * Retourne le nom du package des écrans rpg
   * @param aNomFormat
   * @return
   */
  public static String getPackage(String aNomFormat) {
    return NOMPACKAGE + aNomFormat.substring(0, 4).toLowerCase();
  }
  
  /**
   * Récupération des informations dans l'entête
   * @param buffer
   */
  public void setVarFromEntete(String abuffer) {
    buffer = abuffer;
    
    nomFormat = buffer.substring(38, 48).trim(); // 10
    vFmt = buffer.substring(60, 79).trim(); // 19
    recordAlternatif = buffer.charAt(90); // 1
    
    messageID = buffer.substring(0, 5).trim(); // 5
    numjob = buffer.substring(5, 15).trim(); // 10
    nomRecord = buffer.substring(15, 25).trim(); // 10
    profil = buffer.substring(25, 35).trim(); // 10
    module = buffer.substring(35, 38).trim(); // 3
    nomProgramme = buffer.substring(48, 58).trim(); // 10
    blocageEcran = buffer.charAt(58); // 1
    erreurPrg = buffer.charAt(59); // 1
    
    racinePackage = getPackage(nomFormat);
    String racinepack = racinePackage.replace('.', '/');
    
    // Gestion du record alternatif style BMT, Negoce (on part du principe que l'on créer un nouveau package)
    if (recordAlternatif == ' ') {
      nompanel = nomFormat + '_' + vFmt;
      nomFichierFMT = racinepack + '/' + nomFormat + '/' + nomFormat + '_' + nomRecord + Constantes.EXT_BIN;
      nomFichierAide = racinepack + '/' + nomFormat + '/' + nomFormat + "_fr.htm";
      nomPaquet = nomFormat + ".jar";
    }
    else {
      nompanel = nomFormat + '_' + recordAlternatif + '_' + vFmt;
      nomFichierFMT = racinepack + '/' + nomFormat + '_' + recordAlternatif + '/' + nomFormat + '_' + recordAlternatif + '_' + nomRecord
          + Constantes.EXT_BIN;
      nomFichierAide = racinepack + '/' + nomFormat + '_' + recordAlternatif + '/' + nomFormat + '_' + recordAlternatif + "_fr.htm";
      nomPaquet = nomFormat + '_' + recordAlternatif + ".jar";
    }
    nomFormatRecord = nomFormat + nomRecord;
    reserve = buffer.substring(79, 89).trim(); // 10
    
    // Récupération des indicateurs du flux
    for (int i = indicateurs.length; --i >= 0;) {
      indicateurs[i] = buffer.charAt(FluxRecord.OFFSET_INDICATEUR + i);
      // On nettoie la liste des indicateurs au cas où
      if (indicateurs[i] == ' ') {
        indicateurs[i] = '0';
      }
    }
    iIndicateur.setIndicateur(indicateurs);
    
    // On initialise le mode
    if (indicateurs[50] == '1') {
      mode = MODE_CREATION;
    }
    else if (indicateurs[51] == '1') {
      mode = MODE_MODIFICATION;
    }
    else if (indicateurs[52] == '1') {
      mode = MODE_CONSULTATION;
    }
    else if (indicateurs[53] == '1') {
      mode = MODE_ANNULATION;
    }
    else if (indicateurs[56] == '1') {
      mode = MODE_DUPLICATION;
    }
    else {
      mode = MODE_INDEFINI;
    }
  }
  
  /**
   * Récupération des informations dans le corps
   */
  public void setVarFromCorps(HashMap<String, String> translationtable) {
    oData donnee = null;
    oKey touche = null;
    
    listeKey.clear();
    for (Map.Entry<String, ioGeneric> entry : descriptionRecord.listeElement.entrySet()) {
      if (entry.getValue() instanceof oKey) {
        touche = (oKey) entry.getValue();
        touche.setInterpreteurI(iIndicateur);
        listeKey.add(touche);
      }
      else if (entry.getValue() instanceof oData) {
        donnee = (oData) entry.getValue();
        donnee.setInterpreteurI(iIndicateur);
        // iIndicateur);
        donnee.setTranslationTable(translationtable);
        // Nécessaire pour la méthode de Lexical isIntoCurrentRecord pour éviter les ambiguités (faudra le stocker dans le DAT un jour)
        donnee.setNomFormat(nomFormat);
        if ((FluxRecord.OFFSET_DONNEE + donnee.getOffset()) > buffer.length()) {
          donnee.setValeurFromBuffer("");
        }
        else if ((FluxRecord.OFFSET_DONNEE + donnee.getOffset() + donnee.getLongueur()) > buffer.length()) {
          donnee.setValeurFromBuffer(buffer.substring(FluxRecord.OFFSET_DONNEE + donnee.getOffset()));
        }
        else {
          donnee.setValeurFromBuffer(buffer.substring(FluxRecord.OFFSET_DONNEE + donnee.getOffset(),
              FluxRecord.OFFSET_DONNEE + donnee.getOffset() + donnee.getLongueur()));
        }
      }
    }
    
  }
  
  /**
   * Lit le fichier DAT pour le Record en cours
   * @return
   */
  public boolean lectureFichierDAT(ClassLoader loader) {
    // Lecture du fichier
    GestionFichierDAText gfFichierFMT = new GestionFichierDAText(loader.getResource(getNomFichierFMT()));
    oRecord descriptionRecord = (oRecord) (gfFichierFMT.getObject());
    if (descriptionRecord == null) {
      // A améliorer
      msgErreur = "Problème avec fichier " + getNomFichierFMT() + Constantes.crlf + gfFichierFMT.getMsgErreur();
      Trace.erreur("Problème avec le fichier DAT " + getNomFichierFMT() + " : " + gfFichierFMT.getMsgErreur());
      return false;
    }
    setDescriptionRecord(descriptionRecord);
    gfFichierFMT.dispose();
    gfFichierFMT = null;
    
    return true;
  }
  
  /**
   * @param descriptionRecord the descriptionRecord to set
   */
  public void setDescriptionRecord(oRecord descriptionRecord) {
    this.descriptionRecord = descriptionRecord;
  }
  
  /**
   * @return the descriptionRecord
   */
  public oRecord getDescriptionRecord() {
    return descriptionRecord;
  }
  
  /**
   * @return the indicateurs
   */
  public char[] getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * @param indicateurs the indicateurs to set
   */
  public void setIndicateurs(char[] indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  public String getNomFichierFMT() {
    return nomFichierFMT;
  }
  
  /**
   * Retourne le nom du paquet (jar)
   * @return
   */
  public String getNomPaquet() {
    return nomPaquet;
  }
  
  /**
   * Retourne le nom du format
   * @return
   */
  public String getNomFormat() {
    return nomFormat;
  }
  
  /**
   * Retourne le nom du classname pour le panel
   * @return
   */
  public String getClassName() {
    if (classname == null) {
      if (recordAlternatif == ' ') {
        classname = nomFormat + "." + nompanel;
      }
      else {
        classname = nomFormat + '_' + recordAlternatif + "." + nompanel;
      }
    }
    
    return classname;
  }
  
  /**
   * Retourne le hashcode du classname pour le panel
   * @return
   */
  public int getIdClassName() {
    if (idclassname == -1) {
      idclassname = getClassName().hashCode();
    }
    return idclassname;
  }
  
  /**
   * Retourne le nom du record du DSPF
   * @return
   */
  public String getRecordName() {
    return nomRecord;
  }
  
  public String getFormatRecord() {
    return nomFormatRecord;
  }
  
  /**
   * Retourne le vecteur FMT
   */
  public String getVFmt() {
    return vFmt;
  }
  
  /**
   * Retourne le nom du programme RPG qui tourne
   * @return
   */
  public String getNomPrg() {
    return nomProgramme;
  }
  
  /**
   * Retourne 1 si le RPG détecte une erreur de saisie
   * @return
   */
  public char getErreurPRG() {
    return erreurPrg;
  }
  
  /**
   * Retourne le nom du du fichier Aide
   * @return
   */
  public String getNomAide() {
    return nomFichierAide;
  }
  
  /**
   * Retourne le module
   * @return
   */
  public String getModule() {
    return module;
  }
  
  public boolean isExFmt() {
    return blocageEcran == FluxRecord.EXFMT;
  }
  
  /**
   * Retourne la valeur du code blocage envoyé par le RPG
   * @return
   */
  public char getBlocageEcran() {
    return blocageEcran;
  }
  
  /**
   * Retourne le résultat d'une interprétation des indicateurs
   * ATTENTION les indicateurs doivent être encadrés par des parenthèses ex: (70) AND (69) AND (N42)
   * @return
   */
  public int getEvaluationIndicateur(String cond) {
    // iIndicateur.evaluation(iIndicateur.tabconvert2notpol(cond)));
    return iIndicateur.evaluation(iIndicateur.tabconvert2notpol(cond));
  }
  
  /**
   * Retourne la racine du package (sans le nom de la classe)
   */
  public String getRacinePackage() {
    return racinePackage;
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  public void dispose() {
    listeKey.clear();
    descriptionRecord = null;
    iIndicateur = null;
  }
}
