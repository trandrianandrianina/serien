/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.interpreteur;

import java.awt.Color;

import javax.swing.SwingUtilities;

import ri.serien.libcommun.exploitation.menu.EnumModuleMenu;
import ri.serien.libcommun.exploitation.menu.MenuDetail;
import ri.serien.libcommun.exploitation.menu.ProgrammeALancer;
import ri.serien.libcommun.outils.session.EnvProgram;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.moteur.SNCharteGraphique;

/**
 * Gestion de l'environnement du programme lancé sur l'I5
 * ==> A faire:
 * ==> -Attention avec les droits restreint en ASP sur EXP gestion du µ en paramètre 5
 * ==> -Faire un test sur la validité de la chaine pour le constructeur
 */
public class EnvProgramClient extends EnvProgram {
  // Variables
  private iData interpreteurD = new iData();
  private DebugMode debugmode = null;
  private Color CouleurBandeauFoncee = SNCharteGraphique.COULEUR_BARRE_TITRE_SPECIFIQUE;
  private MenuDetail pointDeMenu = null;
  private IdSession idSession = null;
  
  /**
   * Constructeur.
   */
  public EnvProgramClient() {
    super(ManagerSessionClient.getInstance().getEnvUser());
  }
  
  /**
   * Retourne l'interpréteur de données à la JWAlk.
   */
  public iData getInterpreteurD() {
    return interpreteurD;
  }
  
  /**
   * Initialise le flux venant du RPG pour le debug
   */
  public void setDebugFluxRpg(final FluxRecord afluxrpg, final String libelleprg, final boolean entrant) {
    if (ManagerSessionClient.getInstance().getEnvUser().isDebugStatus()) {
      if (debugmode == null) {
        debugmode = new DebugMode(libelleprg, idSession);
      }
      if (entrant) {
        debugmode.setFluxRpgEntrant(afluxrpg);
      }
      else {
        debugmode.setFluxRpgSortant(afluxrpg);
      }
    }
  }
  
  /**
   * Initialise le focus venant du RPG pour le debug
   */
  public void setDebugFocus(final String nomZone) {
    if (ManagerSessionClient.getInstance().getEnvUser().isDebugStatus()) {
      // Lancement du debug
      SwingUtilities.invokeLater(new Runnable() {
        @Override
        public void run() {
          if (debugmode == null) {
            debugmode = new DebugMode("", idSession);
          }
          debugmode.setZoneFocus(nomZone);
        }
      });
    }
  }
  
  /**
   * Retourne le debug mode
   * @return
   */
  public DebugMode getDebugMode() {
    return debugmode;
  }
  
  /**
   * Met à jour les infos à partir d'un point de menu.
   * @param pPointDeMenu
   * @return
   */
  public boolean setInfosFromMenu(MenuDetail pPointDeMenu) {
    pointDeMenu = pPointDeMenu;
    if (pointDeMenu == null || pointDeMenu.getProgrammeALancer() == null) {
      return false;
    }
    ProgrammeALancer programmeALancer = pointDeMenu.getProgrammeALancer();
    
    // Ajout du programme à lancer et de son pt de menu
    setProgram(programmeALancer.getMNPARG().trim());
    setPointMenu(pointDeMenu.retournerCodePointMenu(false));
    // Permet d'afficher le libellé personnalisé par l'utilisateur
    setLibMenu(pointDeMenu.retournerLibelle());
    // Ajout des paramètres
    if (!programmeALancer.getMNPAR1().trim().isEmpty()) {
      addParametre(programmeALancer.getMNPAR1(), false);
    }
    if (!programmeALancer.getMNPAR2().trim().isEmpty()) {
      addParametre(programmeALancer.getMNPAR2(), false);
    }
    if (!programmeALancer.getMNPAR3().trim().isEmpty()) {
      addParametre(programmeALancer.getMNPAR3(), false);
    }
    if (!programmeALancer.getMNPAR4().trim().isEmpty()) {
      addParametre(programmeALancer.getMNPAR4(), false);
    }
    if (!programmeALancer.getMNPAR5().trim().isEmpty()) {
      addParametre(programmeALancer.getMNPAR5(), false);
    }
    // Ajout des bib à mettre en ligne
    addBib("WEXPAS");
    // Les bibliothèques du module
    if (programmeALancer.getListeBibliotheque() != null) {
      for (int i = 0; i < programmeALancer.getListeBibliotheque().size(); i++) {
        addBib(programmeALancer.getListeBibliotheque().get(i));
      }
    }
    // Les bibliothèques spécifiques
    for (int i = 0; i < infoUser.getListeBibSpe().size(); i++) {
      addBib(infoUser.getListeBibSpe().get(i));
    }

    // Déterminer la couleur de fond du bandeau de titre
    if (pointDeMenu.getModule() == EnumModuleMenu.COMPTA.getCode()) {
      setCouleurBandeauFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_COMPTABILITE);
    }
    else if (pointDeMenu.getModule() == EnumModuleMenu.GESCOM.getCode()) {
      setCouleurBandeauFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_GESCOM);
    }
    else if (pointDeMenu.getModule() == EnumModuleMenu.EXPLOITATION.getCode()) {
      setCouleurBandeauFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_EXPLOITATION);
    }
    else {
      setCouleurBandeauFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_SPECIFIQUE);
    }

    return true;
  }
  
  /**
   * @return le couleurBandeauFoncee
   */
  public Color getCouleurBandeauFoncee() {
    return CouleurBandeauFoncee;
  }
  
  /**
   * @param couleurBandeauFoncee le couleurBandeauFoncee à définir
   */
  public void setCouleurBandeauFoncee(Color couleurBandeauFoncee) {
    CouleurBandeauFoncee = couleurBandeauFoncee;
  }

  /**
   * @return le ptmenu
   */
  public MenuDetail getPointDeMenu() {
    return pointDeMenu;
  }
  
  public IdSession getIdSession() {
    return idSession;
  }
  
  public void setIdSession(IdSession pIdSession) {
    idSession = pIdSession;
  }
  
  /**
   * Ferme la fenetre du debug mode
   *
   */
  public void closeDebug() {
    if (debugmode != null) {
      debugmode.dispose();
      debugmode = null;
    }
  }
  
  @Override
  public void dispose() {
    super.dispose();
    
    closeDebug();
    if (interpreteurD != null) {
      interpreteurD.dispose();
      interpreteurD = null;
    }
  }
}
