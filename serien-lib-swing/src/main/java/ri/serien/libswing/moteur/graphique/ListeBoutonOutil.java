/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.graphique;

import java.util.ArrayList;

import ri.serien.libswing.composant.primitif.bouton.SNBoutonIcone;

/**
 * Liste de SNBoutonIcone.
 * L'utilisation de cette classe est à priviliégier dès qu'on manipule une liste de bouton outil pour la barre de session.
 */
public class ListeBoutonOutil extends ArrayList<SNBoutonIcone> {

}
