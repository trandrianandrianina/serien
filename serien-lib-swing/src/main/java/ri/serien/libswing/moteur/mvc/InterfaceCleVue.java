/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc;

/**
 * Interface standard qui doit être implémentée par toutes les clés des vues dans l'architecture Modèle-Vue.
 *
 * Une vue est identifié par un indice ou un libellé au choix.
 */
public interface InterfaceCleVue {
  /**
   * Indice de la vue.
   *
   * L'indice est utile lorsque le changement de vue s'effectue via un indice comme avec des onglets :
   * Exemple : tbpOnglets.setSelectedIndex(interfaceCleVue.getIndex());
   *
   * @return Indice de la vue.
   */
  public Integer getIndex();

  /**
   * Libellé de la vue.
   *
   * Le libellé est utile lorsque le changement de vue s'effectue via un libellé comme avec des CardLayout :
   * Exemple : cardLayout.show(pnlContenu, interfaceCleVue.getLibelle());
   *
   * @return Libellé de la vue.
   */
  public String getLibelle();
}
