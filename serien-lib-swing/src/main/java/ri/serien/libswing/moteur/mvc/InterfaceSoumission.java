/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc;

/**
 * Interface standard d'un traitement qui sera traité en tâche de fond.
 * 
 * Une classe SwingWorkerAttente s'occupe l'affichage d'une boite boite de dialogue d'attente et en parallèle exécute le traitement
 * définit
 * dans la méthode.
 */
public interface InterfaceSoumission {
  /**
   * Demande l'exécution en tâche de fond.
   * 
   * Cette méthode est appelée par la classe SwingWorkerAttente.
   */
  public void executer();
}
