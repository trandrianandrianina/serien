/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.interpreteur;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.io.File;

import javax.swing.JPanel;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import ri.serien.libcommun.exploitation.dataarea.LdaSerien;
import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;
import ri.serien.libcommun.outils.session.AbstractSessionBase;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.Fading;
import ri.serien.libswing.moteur.SNCharteGraphique;

public abstract class SessionBase extends AbstractSessionBase {
  // Variables
  protected SessionBase thisSessionPanel = this;
  protected boolean boiteDialogue = false;
  protected JPanel panelPanel = null;
  
  protected EnvProgramClient envProgramClient = null;
  protected File captureEcranFile = null;
  protected int idPanelCourant = -1;
  protected boolean etatActif = true;
  protected Lexical lexique = null;
  
  // Indique s'il faut rechercher la base de données de l'utilisateur
  // private boolean rechargerCurlib = false;
  
  private Fading fading = null;
  private boolean modeDebug = false;
  private LdaSerien ldaSerien = null;
  
  // Paramètre provenant pour le point de menu à lancer
  private Object parametreMenu = null;
  
  /**
   * Constructeur pour les sessions RPG.
   */
  public SessionBase(EnvProgramClient pEnvProgramClient) {
    this();
    envProgramClient = pEnvProgramClient;
    modeDebug = ManagerSessionClient.getInstance().getEnvUser().isDebugStatus();
  }
  
  /**
   * Constructeur pour les session Java.
   */
  public SessionBase() {
    super();
    initialiserDonnees();
  }
  
  // -- Méthodes abstraites
  
  protected abstract void sessionAncestorAdded(AncestorEvent e);
  
  protected abstract void sessionAncestorRemoved(AncestorEvent e);
  
  public abstract void startConnexion();
  
  public abstract void setVisibleDialog(boolean visible);
  
  public abstract String getToolTipTxt(boolean selected);

  public abstract void rafraichirBaseDeDonnees();
  
  public abstract void fermerEcran();
  
  @Override
  public abstract void lancerPointMenu(IdEnregistrementMneMnp pIdEnregistrementMneMnp, Object pParametreMenu);
  
  // -- Méthodes publiques
  
  /**
   * Libère les ressources
   */
  public void dispose() {
    panelPanel.removeAll();
    thisSessionPanel = null;
    
    if (fading != null) {
      fading.dispose();
      fading = null;
    }

    if (envProgramClient != null) {
      envProgramClient.dispose();
      envProgramClient = null;
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise les données.
   */
  private void initialiserDonnees() {
    idSession = ManagerSessionClient.getInstance().creerSessionClient();
    modeDebug = ManagerSessionClient.getInstance().getEnvUser().isDebugStatus();
    
    panelPanel = new JPanel(new BorderLayout()) {
      /**
       * Méthode de dessin du panel
       */
      @Override
      protected void paintComponent(Graphics g) {
        super.paintComponent(g);
      }
    };
    fading = new Fading(panelPanel, true);
    
    // Initialisation de la couleur du fond en jaune
    panelPanel.setBackground(SNCharteGraphique.COULEUR_FOND);
    
    // Evènements
    panelPanel.addAncestorListener(new AncestorListener() {
      @Override
      public void ancestorMoved(AncestorEvent e) {
      }
      
      @Override
      public void ancestorAdded(AncestorEvent e) {
        sessionAncestorAdded(e);
      }
      
      @Override
      public void ancestorRemoved(AncestorEvent e) {
        sessionAncestorRemoved(e);
      }
    });
  }
  
  // -- Accesseurs
  
  public Lexical getLexique() {
    if (lexique == null) {
      lexique = new Lexical();
    }
    return lexique;
  }
  
  /**
   * @return le infoPrg
   */
  public EnvProgramClient getInfoPrg() {
    return envProgramClient;
  }
  
  /**
   * Retourne l'objet LDASERIEN qui sert à créer une DATAAREA nommé QTEMP/LDASERIEN et qui contient des informations
   * propre à Série N.
   * Aujourd'hui elle ne contient que le code écran utilisé dans les affectations d'impression
   */
  public LdaSerien getLdaSerien() {
    if (ldaSerien == null) {
      ldaSerien = new LdaSerien();
      ldaSerien.setCodeEcran(ManagerSessionClient.getInstance().getEnvUser().getDeviceImpression());
    }
    return ldaSerien;
  }
  
  public boolean isBoiteDialogue() {
    return boiteDialogue;
  }
  
  public void setBoiteDialogue(boolean boiteDialogue) {
    this.boiteDialogue = boiteDialogue;
  }
  
  public boolean isModeDebug() {
    return modeDebug;
  }
  
  /**
   * @return le etatActif
   */
  public boolean isEtatActif() {
    return etatActif;
  }
  
  /**
   * Retourne le numéro de la session.
   */
  public int getNumeroSession() {
    return idSession.getNumero();
  }
  
  /**
   * Retourne le nom du panel en cours
   * @return
   */
  public int getIdPanelCourant() {
    return idPanelCourant;
  }
  
  /**
   * Retourne le panel du menu.
   */
  public JPanel getPanel() {
    return panelPanel;
  }
  
  /**
   * Retourne les paramètres passés par le programme appelant.
   * Utilisé lors de l'utilisation de l'interface:
   * - lancerPointMenu(IdEnregistrementMneMnp pIdEnregistrementMneMnp, Object pParametreMenu);
   */
  public Object getParametreMenu() {
    return parametreMenu;
  }

  /**
   * Intialise les paramètres passés par le programme appelant.
   * Utilisé lors de l'utilisation de l'interface:
   * - lancerPointMenu(IdEnregistrementMneMnp pIdEnregistrementMneMnp, Object pParametreMenu);
   */
  public void setParametreMenu(Object pParametreMenu) {
    this.parametreMenu = pParametreMenu;
  }

}
