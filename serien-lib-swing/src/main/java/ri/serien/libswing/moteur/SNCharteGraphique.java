/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.KeyEvent;

import javax.swing.KeyStroke;

import ri.serien.libcommun.outils.image.IconeBouton;

/**
 * Cette classe utilitaire contient les constantes définissant la caharte graphique de l'interface.
 */
public class SNCharteGraphique {
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Dimensions standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Marge standard entre les composants.
   */
  public static final int MARGE_STANDARD = 5;
  
  /**
   * Marge externe entre les composants et le bord de la fenêtre.
   */
  public static final int MARGE_EXTERNE = 10;
  
  /**
   * Hauteur standard d'un composant.
   */
  public static final int HAUTEUR_STANDARD_COMPOSANT = 30;

  /**
   * Hauteur standard réduit d'un composant.
   */
  public static final int HAUTEUR_STANDARD_COMPOSANT_REDUIT = 22;

  /**
   * Largeur standard d'un bouton.
   */
  public static final int LARGEUR_STANDARD_BOUTON = 140;
  
  /**
   * Taille par défaut d'un composant de saisie basique.
   * Par exemple : saisie de texte, liste déroulante standard, ...
   */
  public static final Dimension TAILLE_COMPOSANT_SAISIE = new Dimension(100, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);

  /**
   * Taille réduit par défaut d'un composant de saisie basique.
   * Par exemple : saisie de texte, liste déroulante standard, ...
   */
  public static final Dimension TAILLE_COMPOSANT_SAISIE_REDUIT = new Dimension(80, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT_REDUIT);
  
  /**
   * Taille par défaut d'un composant de sélection d'un objet métier (comportant un libellé suivi de l'identifiant entre parenthèses).
   * Cette taille correspond à la taille minimum du composant. Dans la majorité des cas, le composant doit être configuré
   * pour prendre tout l'espace disponible en la largeur afin de pouvoir afficher toutes les tailles de libellés.
   * Par exemple : sélection d'un client, d'un aticle, ...
   */
  public static final Dimension TAILLE_COMPOSANT_SELECTION = new Dimension(200, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);

  /**
   * Taille par défaut des libellés des champs.
   */
  public static final Dimension TAILLE_COMPOSANT_LIBELLE = new Dimension(150, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);

  /**
   * Taille réduit des libellés des champs.
   */
  public static final Dimension TAILLE_COMPOSANT_LIBELLE_REDUIT = new Dimension(100, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT_REDUIT);

  /**
   * Taille d'un bouton standard.
   * Le bouton standard a une hauteur plus importante (50 pixels) que la hauteur habituelle des composants (30 pixels).
   */
  public static final Dimension TAILLE_BOUTON_STANDARD = new Dimension(LARGEUR_STANDARD_BOUTON, 50);
  
  /**
   * Taille d'un bouton léger.
   * Le bouton léger à la même hauteur que les autres composants.
   */
  public static final Dimension TAILLE_BOUTON_LEGER =
      new Dimension(LARGEUR_STANDARD_BOUTON, SNCharteGraphique.HAUTEUR_STANDARD_COMPOSANT);
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Couleurs de fond des écrans et boîtes de dialogue
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Couleur jaune pastel.
   * Utilisation : Couleur de fond de tous les écrans et boîte de dialogue (sauf écran de type PDF).
   */
  public static final Color COULEUR_FOND = new Color(239, 239, 222);
  
  /**
   * Couleur gris foncée pour les panneaux dégradés qui servent de fond aux écrans RPG v2.
   * Pour information, le fond gris est progressivement en vois de suppression.
   */
  public static final Color COULEUR_FOND_DEGRADE_GRIS_FONCE = new Color(130, 130, 130);
  
  /**
   * Trait à peine visible utilisé en mode débug pour afficher les bords des composants.
   */
  public static final Color COULEUR_DEBUG = new Color(0, 0, 0, 5);
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Couleurs de la barre de titre
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Couleur foncée pour le dégradé de couleurs de la barre de titre en gestion commerciale.
   * La couleur choisie est bleue foncée (comme le bleu foncé du logo RI).
   */
  public static final Color COULEUR_BARRE_TITRE_GESCOM = new Color(35, 93, 193);
  
  /**
   * Couleur foncée pour le dégradé de couleurs de la barre de titre en comptabilité.
   * La couleur choisie est verte foncée (comme le vert foncé du logo RI).
   */
  public static final Color COULEUR_BARRE_TITRE_COMPTABILITE = new Color(143, 179, 0);
  
  /**
   * Couleur foncée pour le dégradé de couleurs de la barre de titre en gestion commerciale.
   * La couleur choisie est violette foncée.
   */
  public static final Color COULEUR_BARRE_TITRE_EXPLOITATION = new Color(153, 0, 204);
  
  /**
   * Couleur foncée pour le dégradé de couleurs de la barre de titre pour les programmes spécifiques.
   * La couleur choisie est grise foncée.
   */
  public static final Color COULEUR_BARRE_TITRE_SPECIFIQUE = COULEUR_FOND_DEGRADE_GRIS_FONCE;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Couleurs de la barre de session
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Couleur de la barre de session.
   */
  public static final Color COULEUR_BARRE_SESSION = Color.DARK_GRAY;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Couleurs des composants de saisies
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Couleur de fond pour les champs éditable.
   */
  public static final Color COULEUR_FOND_CHAMP_EDITABLE = Color.WHITE;
  
  /**
   * Couleur de fond pour les champs grisé.
   */
  public static final Color COULEUR_FOND_CHAMP_DESACTIVE = new Color(243, 243, 236);
  
  /**
   * Couleur de fond pour les champs grisé.
   */
  public static final Color COULEUR_TEXTE_CHAMP_DESACTIVE = new Color(77, 77, 80);
  
  /**
   * Couleur de police des lignes traitées d'une liste.
   */
  public static final Color COULEUR_LIGNE_TRAITEE = new Color(140, 140, 140);
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Couleurs des boutons
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Couleur de fond des boutons oranges.
   * Utilisation: couleur de fond des boutons standards.
   */
  public static final Color COULEUR_BOUTON_ORANGE = new Color(171, 148, 79);
  
  /**
   * Couleur de fond des boutons verts.
   * Utilisation: couleur de fond des boutons de catégorie "Validation".
   */
  public static final Color COULEUR_BOUTON_VERT = new Color(109, 166, 126);
  
  /**
   * Couleur de fond des boutons rouges.
   * Utilisation: couleur de fond des boutons de catégorie "Annulation".
   */
  public static final Color COULEUR_BOUTON_ROUGE = new Color(177, 121, 116);
  
  /**
   * Couleur de fond des boutons contenant des images.
   */
  public static final Color COULEUR_BOUTON_IMAGE = new Color(238, 238, 210);
  
  /**
   * Couleur de fond du bord des boutons dégradés.
   * Ce sont les boutons permettant d'afficher des images.
   */
  public static final Color COULEUR_BOUTON_DEGRADE_BORD = new Color(95, 95, 95);
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Polices de caractères
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Police standard.
   * Utilisation: libellés des composants, valeurs des composants.
   */
  public static final Font POLICE_STANDARD = new Font("sansserif", Font.PLAIN, 14);
  
  /**
   * Police standard en mode réduit.
   * Utilisation: libellés des composants, valeurs des composants.
   */
  public static final Font POLICE_STANDARD_REDUIT = new Font("sansserif", Font.PLAIN, 11);
  
  /**
   * Police des titres.
   */
  public static final Font POLICE_TITRE = new Font("sansserif", Font.BOLD, 14);
  
  /**
   * Police des titres.
   */
  public static final Font POLICE_TITRE_REDUIT = new Font("sansserif", Font.BOLD, 11);
  
  /**
   * Police des tableaux.
   */
  public static final Font POLICE_TABLEAU = new Font("sansserif", Font.PLAIN, 12);
  public static final Font POLICE_TABLEAU_GRAS = new Font("sansserif", Font.BOLD, 12);
  
  /**
   * Police des boutons.
   * La police étant en gras, elle est lègérement plus petite que la police standard pour qu'elles paraissent de la même taille.
   * Utilisation: libellés des boutons des catégories "Standard", "Validation" et "Annulation".
   */
  public static final Font POLICE_BOUTON = new Font("sansserif", Font.BOLD, 13);
  
  /**
   * Police du bouton de session actif (contenu dans la barre de session).
   */
  public static final Font POLICE_BOUTON_SESSION_ACTVF = new Font("sansserif", Font.BOLD, 13);
  
  /**
   * Police du bouton de session non actif (contenu dans la barre de session).
   */
  public static final Font POLICE_BOUTON_SESSION_NON_ACTIF = new Font("sansserif", Font.PLAIN, 13);
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Textes standard
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Texte utilisé dans le slistes déroulantes pour indiquer que toutes les entrées sont sélectionnées.
   */
  public static final String TEXTE_TOUS = "Tous";
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Curseur souris
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Curseur main.
   * Utilisation : mise en évidence des composants cliquables.
   */
  public static final Cursor CURSEUR_MAIN = Cursor.getPredefinedCursor(Cursor.HAND_CURSOR);
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Touches
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Evènements sur touche entrée (Enter)
   */
  public static final KeyStroke TOUCHE_ENTREE = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
  
  /**
   * Evènements sur touche suppr. (Del.)
   */
  public static final KeyStroke TOUCHE_SUPPR = KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0);
  
  /**
   * Evènements sur touche Ech. (Esc.)
   */
  public static final KeyStroke TOUCHE_ECH = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
  
  /**
   * Evènements sur touche Tab.
   */
  public static final KeyStroke TOUCHE_TAB = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0);
  
  /**
   * Evènements sur touche Shift + Tab.
   */
  public static final KeyStroke TOUCHE_SHIFT_TAB = KeyStroke.getKeyStroke("shift TAB");
  
  /**
   * Evènements sur touche flèche vers le haut
   */
  public static final KeyStroke TOUCHE_HAUT = KeyStroke.getKeyStroke(KeyEvent.VK_UP, 0);
  
  /**
   * Evènements sur touche flèche vers le bas
   */
  public static final KeyStroke TOUCHE_BAS = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0);
  
  /**
   * Evènements sur touche flèche vers la gauche
   */
  public static final KeyStroke TOUCHE_GAUCHE = KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0);
  
  /**
   * Evènements sur touche flèche vers la droite
   */
  public static final KeyStroke TOUCHE_DROITE = KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0);
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Images
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Icône par défaut vide 16 x 16 en cas de problème avec une autre icône de taille 25 ou inférieurre.
   */
  public static final IconeBouton ICONE_DEFAUT_16 = new IconeBouton("images/defaut_16x16.png");
  
  /**
   * Icône par défaut vide 26 x 26 en cas de problème avec une autre icône de taille 26 ou supérieure.
   */
  public static final IconeBouton ICONE_DEFAUT_26 = new IconeBouton("images/defaut_26x26.png");
  
  /**
   * Icône d'affichage du détail (signe plus).
   */
  public static final IconeBouton ICONE_DETAIL = new IconeBouton("images/detail_16x16.png");
  
  /**
   * Icône pour obtenir une information complémentaire (petit i).
   */
  public static final IconeBouton ICONE_INFORMATION = new IconeBouton("images/information_16x16.png");
  
  /**
   * Icône pour lancer une recherche (loupe).
   */
  public static final IconeBouton ICONE_RECHERCHE = new IconeBouton("images/recherche_26x26.png");
  
  /**
   * Icône pour afficher un document (page de document).
   */
  public static final IconeBouton ICONE_DOCUMENT = new IconeBouton("images/document_26x26.png");
  
  /**
   * Icône pour afficher une aide (point d'interrogation).
   */
  public static final IconeBouton ICONE_AIDE = new IconeBouton("images/aide_26x26.png");
  
  /**
   * Icône pour envoyer un mail (courrier).
   */
  public static final IconeBouton ICONE_MAIL = new IconeBouton("images/mail_26x26.png");

  /**
   * Icône pour paramétrage (outils).
   */
  public static final IconeBouton ICONE_PARAMETRAGE = new IconeBouton("images/parametrage.png");
  
  /**
   * Icône pour afficher une notification.
   */
  public static final IconeBouton ICONE_NOTIFICATION_IMPORTANTE_ALIRE = new IconeBouton("images/notification-rouge_26x26.png");
  public static final IconeBouton ICONE_NOTIFICATION_ALIRE = new IconeBouton("images/notification-orange_26x26.png");
  public static final IconeBouton ICONE_NOTIFICATION = new IconeBouton("images/notification-bleu_26x26.png");

  /**
   * Icône pour sélection de date.
   */
  public static final IconeBouton ICONE_CALENDRIER = new IconeBouton("images/calendrier_26x26.png");

  /**
   * Icônes de sélection d'un élément (flèche vers la droite).
   */
  public static final IconeBouton ICONE_SELECTION_UNITAIRE = new IconeBouton("images/selection_unitaire_50x50.png");
  public static final IconeBouton ICONE_SELECTION_TOTALE = new IconeBouton("images/selection_totale_50x50.png");
  public static final IconeBouton ICONE_SELECTION_PAR_DEFAUT = new IconeBouton("images/selection_par_defaut_50x50.png");
  public static final IconeBouton ICONE_DESELECTION_UNITAIRE = new IconeBouton("images/deselection_unitaire_50x50.png");
  public static final IconeBouton ICONE_DESELECTION_TOTALE = new IconeBouton("images/deselection_totale_50x50.png");
}
