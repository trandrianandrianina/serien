/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc;

/**
 * Clé des vues des écrans de type liste/détail.
 *
 * Cette clé peut être utilisée en standard dans les écrans liste/détail dans lesquels on gère juste l'affichage de 2 vues : la vue
 * liste et la vue détail.
 */
public enum EnumCleVueListeDetail implements InterfaceCleVue {
  LISTE(0, "Liste"),
  DETAIL(1, "Détail");

  private final Integer index;
  private final String libelle;

  /**
   * Constructeur.
   *
   * @param pIndex Indice de la vue.
   * @param pLibelle Libellé de la vue.
   */
  EnumCleVueListeDetail(Integer pIndex, String pLibelle) {
    index = pIndex;
    libelle = pLibelle;
  }

  /**
   * Indice de la vue.
   * Voir documentation InterfaceCleVue.
   */
  @Override
  public Integer getIndex() {
    return index;
  }

  /**
   * Libellé de la vue.
   * Voir documentation InterfaceCleVue.
   */
  @Override
  public String getLibelle() {
    return libelle;
  }

  /**
   * Clé de la vue résumé dans une chaîne de caractères.
   */
  @Override
  public String toString() {
    return String.valueOf(libelle + "(" + index + ")");
  }
}
