/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.graphique;

import java.awt.FlowLayout;

import javax.swing.JPanel;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonIcone;

/**
 * Ce composant graphique correspond à la barre qui est affichée en bas de l'écran.
 * Cette barre contient les boutons qui sont associés aux sessions.
 */
public class SNBarreOutil extends JPanel {
  // Constantes
  private static final int HAUTEUR_BARRE = 40;
  public static final int LARGEUR_BOUTON_OUTIL = 26;
  public static final int LARGEUR_ESPACE_OUTIL = 5;
  public static final int HAUTEUR_BOUTON = HAUTEUR_BARRE;
  
  // Variables
  private ListeBoutonOutil listeBoutonOutil = new ListeBoutonOutil();
  
  /**
   * Constructeur.
   */
  public SNBarreOutil() {
    super();
    initialiserComposant();
  }
  
  // -- Méthodes publiques
  
  /**
   * Ajouter un bouton outil à la barre de sessions.
   * Le bouton qui vient d'être ajouter est automatiquement actif c'est à dire que c'est cette session qui sera affichée.
   */
  public void ajouterBouton(SNBoutonIcone pSNBoutonIcone) {
    if (pSNBoutonIcone == null) {
      throw new MessageErreurException("Le bouton outil à ajouter à la barre de sessions est invalide.");
    }
    
    // Ajouter le bouton à la liste
    listeBoutonOutil.add(pSNBoutonIcone);
    
    // Rafraîchir le composant
    rafraichir();
  }
  
  /**
   * Supprimer un bouton à la barre de sessions.
   */
  public void supprimerBouton(SNBoutonIcone pSNBoutonIcone) {
    if (pSNBoutonIcone == null) {
      throw new MessageErreurException("Le bouton outil est invalide.");
    }
    
    // Supprimer le bouton à la liste
    listeBoutonOutil.remove(pSNBoutonIcone);
    
    // Rafraîchir le composant
    rafraichir();
  }
  
  /**
   * Rafraîchir le composant.
   */
  public void rafraichir() {
    rafraichirBoutonOutil();
  }
  
  // -- Méthodes privées
  
  /**
   * Configurer la barre de sessions.
   */
  private void initialiserComposant() {
    // Définir le layout du panel des boutons outils
    setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
  }
  
  /**
   * Rafraîchir l'ensemble des boutons outils.
   */
  private void rafraichirBoutonOutil() {
    // Effacer tous les boutons de session
    removeAll();
    
    if (listeBoutonOutil.size() > 0) {
      // Ajouter les boutons à la barre
      for (SNBoutonIcone snBoutonOutil : listeBoutonOutil) {
        add(snBoutonOutil);
      }
    }

    // Forcer la rafraîchissement du panel
    revalidate();
  }

}
