/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc;

/**
 * Interface standard d'une vue écran.
 *
 * Une vue écran se charge d'afficher les données du modèle (c'est le rôles "Vue" du pattern MVC). Elle de modifie pas directement
 * sur les données du modèle car elle utilise les méthodes du modèle implémentant les différentes actions possibles sur les données.
 *
 * Lors de l'affichage initial d'un écran, les méthodes sont appelées dans l'ordre suivant :
 * 1- modele.initialiserDonnees()
 * 2- vue.initialiserComposants()
 * 3- vue.rafraichir()
 * 4- modele.chargerDonnees()
 * 5- vue.rafraichir()
 *
 * Des implémentations de cette interface sont disponibles : AbstractVueSession, AbstractVueDialogue.
 */
public interface InterfaceVue<InterfaceM extends InterfaceModele> {
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes à implémenter dans la vue finale.
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialiser les composants graphiques de la vue.
   *
   * Les données du modèle ne sont pas encore chargées à ce stade. L'objectif est d'afficher la boîte de dialogue très rapidement afin que
   * l'utilisateur ait une réponse visuelle immédiate après son action.
   *
   * En pratique, cette méthode appelle la méthode initCompoments de JFormDesigner. On peut ajouter manuellement des composants ou adapter
   * les propriétés des composants existants. C'est notamment là qu'on enregistre les raccourcis claviers.
   */
  public void initialiserComposants();
  
  /**
   * Rafraichir les données de la vue.
   *
   * Cette méthode est appelée à chaque fois que les données du modèle évoluent. L'objectif de cette méthode est de mettre à jour les
   * données visibles à l'écran en fonctions des données du modèle. Les composants graphiques peuvent devenir actifs ou inactifs,
   * éditables ou non éditables, visibles ou non visibles.
   *
   * La méthode rafraîchir() est appelée deux fois, une première fois après initialisation des données du modèle (initialiserDonnees) et
   * initialisation des composants graphiques (initialiserComposants), une seconde dois après le chargement des données du modèle
   * (chargerDonnees).
   */
  public void rafraichir();
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes implémentées dans les classes abstraites AbstractVuePanel et AbstractVueDialogue.
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Afficher la vue.
   *
   * C'est cette méthode qu'il faut appeler pour créer et afficher une vue. La méthode initialise les données du modèles, initialise les
   * composants graphiques, déclenche un premier affichage à vide de la vue, lance le chargement des données et déclenche un second
   * rafraichissement de la vue mais avec les données cette fois-ci. Elle vérifie que le traitement est bien effectué dans la thread
   * graphique (EDT).
   *
   * Si la vue contient des vues enfantes, les composants de la vue parente et de toutes les vues enfantes sont également initialisés.
   * Par contre, seule la vue parente et la vue enfante définie comme vue courante dans le modèle via getCleVueEnfantActive() sont
   * rafraichies.
   *
   * Exemple d'utilisation :
   * ModeleContact modele = new ModeleContact(this, getParametreMenu());
   * VueContact vue = new VueContact(modele);
   * panel.add(vue, BorderLayout.CENTER);
   * vue.afficher();
   */
  public void afficher();
  
  /**
   * Initialiser les composants graphiques de la vue et des vues enfants.
   *
   * Cette méthode est appelée en interne par la méthode afficher() pour initialiser les composants de la vue parent et de toutes les vues
   * enfants. Elle appelle individuellement les méthodes initialiserComposants() de chaque vue. De plus, elle affiche une information dans
   * les traces, renomme le composant graphique pour QFTest et désactive la gestion des évènements.
   */
  public void initialiserComposantsAvecEnfants();
  
  /**
   * Rafraîchir les données des composants graphiques.
   *
   * Cette méthode est la méthode principale pour rafraichir les données de la vue et de sa vue enfant active s'il y en a une.
   * Elle vérifie que le rafraichissement est bien effectué dans la thread graphique (EDT).
   *
   * Elle appelle les méthodes rafraîchir des vues :
   * - Si la vue est une vue sans parent ni sans enfant, c'est la méthode rafraichir de cette vue est appelée.
   * - Si la vue est une vue parente avec des vues enfants,seules la vue parente et la vue enfante définie comme vue courante dans le
   * modèle via getCleVueEnfantActive() sont rafraichies.
   * - Si la vue est une vue enfante, rien n'est rafraichi car c'est le vue parente qui gère cela.
   */
  public void rafraichirComposants();
  
  /**
   * Rafraichir les données des composants graphiques de la vue et des vues enfants.
   *
   * Cette méthode est appelée en interne par la méthode rafraichirComposants() pour rafraîchir les composants de la vue parent et de la
   * vue enfant active. Elle appelle individuellement les méthodes rafraichir() de chaque vue. De plus, elle affiche une information dans
   * les traces et désactive la gestion des évènements.
   */
  public void rafraichirComposantsAvecEnfants();
  
  /**
   * Cacher la vue.
   */
  public void cacher();
  
  /**
   * Modèle associé à la vue.
   *
   * @return Interface du modèle rattaché à la vue.
   */
  public InterfaceM getModele();
  
  /**
   * Ajouter une vue enfant à cette vue (qui fait office de vue parente).
   *
   * La vue enfant est associée à une clé qui permet de l'identifier. C'est cette clé qui est connu du modèle et permet de connaître la
   * vue active. Le modèle de la vue enfante doit être le modèle que celui de la vue parente.
   *
   * @param pCle Clé associé à la vue enfante.
   * @param pVueEnfant Vue enfant à ajouter à la vue parente
   */
  public void ajouterVueEnfant(InterfaceCleVue pCle, InterfaceVue<?> pVueEnfant);
  
  /**
   * Vue parente de la vue.
   *
   * @return Vue parente de la vue.
   */
  public InterfaceVue<?> getVueParent();
  
  /**
   * Modifier la vue parente de la vue.
   *
   * Ne pas utiliser cette méthode pour associer une vue enfant et une vue parente. Il faut utiliser la méthode ajouterVueEnfant().
   * La méthode ne permet pas de modifier la vue parente d'une vue enfant si celle-ci a déjà une vue parente. Cela retourne un message
   * d'erreur.
   *
   * @param pVueParent Vue parent à renseigner dans la vue enfant.
   */
  public void setVueParent(InterfaceVue<?> pVueParent);
}
