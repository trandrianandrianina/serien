/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.interpreteur;

import java.awt.AWTException;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.UIManager;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.DataFormat;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.composants.ioGeneric;
import ri.serien.libcommun.outils.composants.oData;
import ri.serien.libcommun.outils.composants.oKey;
import ri.serien.libcommun.outils.composants.oRecord;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierCSV;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.libcommun.outils.session.SessionTransfert;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composantrpg.autonome.CalculatriceSolde;
import ri.serien.libswing.composantrpg.autonome.ConsultDocFrame;
import ri.serien.libswing.composantrpg.autonome.ConsultDocPopup;
import ri.serien.libswing.composantrpg.autonome.FenetreIndependante;
import ri.serien.libswing.composantrpg.autonome.LaunchViewer;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.viewerHTML;

/**
 * Dictionnaire de fonctions équivalente de JWalk
 */
public class Lexical {
  // A mettre à jour dans FluxRecord si modification
  public final static int MODE_INDEFINI = 0;
  public final static int MODE_CREATION = 1;
  public final static int MODE_MODIFICATION = 2;
  public final static int MODE_CONSULTATION = 3;
  public final static int MODE_ANNULATION = 4;
  public final static int MODE_DUPLICATION = 5;
  
  // Variables
  private SNPanelEcranRPG panel = null;
  private SNPanelEcranRPG popupPerso = null; // Ne sert pas pour l'instant c'était pour la gestion des F4 dans les popup perso
  private FluxRecord fluxRpg = null;
  private String toucheEnvoyee = null;
  private static HashMap<String, Object> VariableGlobale = new HashMap<String, Object>();
  
  /**
   * Constructeur de la classe
   */
  public Lexical(SNPanelEcranRPG pnl, FluxRecord fluxrpg) {
    panel = pnl;
    fluxRpg = fluxrpg;
  }
  
  /**
   * Constructeur de la classe
   */
  public Lexical(SNPanelEcranRPG pnl) {
    panel = pnl;
  }
  
  /**
   * Constructeur de la classe
   */
  public Lexical() {
  }
  
  /**
   * Initialise le panel en cours
   *
   * @param pnl
   */
  public void setPanel(SNPanelEcranRPG pnl) {
    panel = pnl;
  }
  
  /**
   * Retourne le panel en cours
   *
   * @param pnl
   */
  public SNPanelEcranRPG getPanel() {
    return panel;
  }
  
  /**
   * Initialise le panel de la popup personnlisée en cours
   *
   * @param pnl
   */
  public void setPopupPerso(SNPanelEcranRPG popup) {
    popupPerso = popup;
  }
  
  /**
   * Initialise le databuffer en cours
   *
   * @param dtabuf
   */
  public void setDataBuffer(FluxRecord dtabuf) {
    fluxRpg = dtabuf;
  }
  
  /**
   * Retourne le chemin vers le fichier traduction
   */
  public String getPackageBundle(String pNomBundle) {
    if (pNomBundle == null) {
      return null;
    }
    // On contrôle le nom du bundle (en fonction des JFD il peut être différent:
    // soit juste le nom du projet soit le nom du package)
    // Problème avec les JFD contenant des onglets, ex: VGVM03FM_C3.
    if (pNomBundle.startsWith(InformationsRecord.NOMPACKAGE)) {
      return pNomBundle;
    }
    if (fluxRpg == null) {
      return InformationsRecord.getPackage(pNomBundle) + '.' + pNomBundle;
    }
    String nompackage = fluxRpg.getRacinePackage(pNomBundle);
    if (pNomBundle.startsWith(nompackage)) {
      return pNomBundle;
    }
    return nompackage + '.' + pNomBundle;
  }
  
  /**
   * Initialise le databuffer en cours
   *
   * @param dtabuf
   */
  public void setPanelDataBuffer(SNPanelEcranRPG pnl, FluxRecord dtabuf) {
    panel = pnl;
    fluxRpg = dtabuf;
  }
  
  /**
   * Rafraichit le panel
   */
  public void Refresh() {
    panel.repaint();
  }
  
  /**
   * Ferme un Panel (Popup ou panel dans le ClientSGM)
   */
  public void ClosePanel() {
    panel.getTopLevelAncestor().setVisible(false);
    // panel.getJDialog().setVisible(false);
    if (panel.getTopLevelAncestor() instanceof JFrame) {
      ((JFrame) (panel.getTopLevelAncestor())).dispose();
    }
  }
  
  /**
   * Ferme une popup Attention dangereux (utilisé dans le VGVX055F) à cause du
   * forceRenvoyable
   *
   * @param touche
   */
  public void ClosePopup(final String touche) {
    if (touche == null) {
      return;
    }
    new Thread() {
      @Override
      public void run() {
        int cpt = 20; // On patiente 10s max
        do {
          try {
            Thread.sleep(500);
          }
          catch (Exception e) {
          }
          cpt--;
          if (cpt == 0) {
            break;
          }
        }
        while (!panel.isRenvoyable());
        HostScreenSendKey(panel, touche, true);
      }
    }.start();
  }
  
  /**
   * Ferme l'application (Panel dans le ClientWEB) A améliorer car bourrin et pas
   * propre (voir oSessionPanel=setSession)
   */
  public void CloseApplication() {
    // ((Window)((JPanel)panel.getComposant()).getTopLevelAncestor()).dispose();
  }
  
  /**
   * Met à jour une donnée du buffer Attention à la casse du nom de la variable
   *
   * @param nomHostField
   * @param ligne
   * @param valeur
   */
  public void HostFieldPutData(String nomHostField, int ligne, String valeur) {
    if (fluxRpg == null || fluxRpg.getDescriptionRecord() == null || fluxRpg.getDescriptionRecord().listeElement == null) {
      return;
    }
    if (fluxRpg.getDescriptionRecord().listeElement.containsKey(nomHostField)) {
      oData HostField = (oData) (fluxRpg.getDescriptionRecord()).listeElement.get(nomHostField);
      if (HostField.getZoneType() == oData.SENS_ENTREE) {
        HostField.setValeurFromFrame(valeur);
        // panel.getMasterFrame().listeoDataFlux.put(HostField, panel.getMasterFrame());
      }
    }
  }
  
  /**
   * Retourne la valeur d'un HostField
   *
   * @param nomHostField
   * @return
   */
  public String HostFieldGetData(String nomHostField) {
    // Vérifier que le nom champ est valide
    if (nomHostField == null) {
      throw new MessageErreurException("Impossible de récupérer la valeur du champ car le nom du champ est invalide.");
    }
    
    // On cherche le oData dans les flux reçu par le panel
    for (Entry<String, oRecord> entry : panel.listeRecord.entrySet()) {
      if (entry.getValue().listeElement.containsKey(nomHostField)) {
        return ((oData) entry.getValue().listeElement.get(nomHostField)).getValeurToFrame();
      }
    }
    // Blanc sinon
    return "";
  }
  
  /**
   * Retourne la valeur numérique (formaté correctement) d'un HostField
   *
   * @param nomHostField
   * @return
   */
  public String HostFieldGetNumericData(String nomHostField) {
    oData hostField = getHostField(nomHostField);
    if (hostField != null) {
      DataFormat df = new DataFormat(hostField.getValeurToFrame(), '#', hostField.getLongueur(), hostField.getDecimale());
      if (df.isNumericPlus()) {
        return df.getEdtNumerique((char) hostField.getInputCtrl());
      }
      else {
        return hostField.getValeurToFrame();
      }
    }
    else {
      // Blanc sinon
      return "0";
    }
  }
  
  /**
   * Retourne la valeur numérique (formaté correctement) d'un String
   *
   * @param nomHostField
   * @return
   */
  public String HostFieldGetNumericString(String stringDeDepart, int decimales) {
    if (stringDeDepart != null) {
      DataFormat df = new DataFormat(stringDeDepart, '#', stringDeDepart.length(), decimales);
      if (df.isNumericPlus()) {
        return df.getEdtNumerique('O');
      }
      else {
        return stringDeDepart;
      }
    }
    else {
      // Blanc sinon
      return "0";
    }
  }
  
  /**
   * Retourne un HostField
   *
   * @param nomHostField
   * @return
   */
  public oData getHostField(String nomHostField) {
    oData hostField = null;
    // On cherche le oData dans les flux reçu par le panel
    for (Map.Entry<String, oRecord> entry : panel.listeRecord.entrySet()) {
      if (entry.getValue().listeElement.containsKey(nomHostField)) {
        hostField = ((oData) entry.getValue().listeElement.get(nomHostField));
        // panel + " " + panel.listeRecord);
      }
    }
    return hostField;
  }
  
  /**
   * Permet de vérifier si hostfield appartient au record courant (donc en EXFMT)
   * Important pour les zones éditables (principe du B1-F1 par exemple)
   *
   * @param hostfield
   * @return
   */
  public boolean isIntoCurrentRecord(oData hostfield) {
    if (hostfield == null) {
      return false;
    }
    return fluxRpg.getFormatRecord().equals(hostfield.getFormatRecord());
  }
  
  /**
   * Retourne une Key
   *
   * @param nomTouche
   * @return
   */
  public oKey getKey(String nomTouche) {
    oKey touche = null;
    
    // On cherche le oKey dans les flux reçu par le panel
    final ArrayList<oKey> listeKey = fluxRpg.getListeKey();
    for (int i = listeKey.size(); --i >= 0;) {
      // + listeKey.get(i).getParent());
      if (listeKey.get(i).getKey().equals(nomTouche)) {
        touche = listeKey.get(i);
        break;
      }
    }
    return touche;
  }
  
  /**
   * Retourne la valeur d'une variable de type @???@
   */
  public String PanelFieldGetData(String nomHostField) {
    return panel.getProgram().getInterpreteurD().analyseExpression(nomHostField);
  }
  
  /**
   * Envoi une touche de fonction
   *
   * @param pTouche
   */
  public void HostScreenSendKey(SNPanelEcranRPG pPanel, String pTouche) {
    HostScreenSendKey(pPanel, pTouche, pPanel.isCloseKey(pTouche));
  }
  
  /**
   * Envoi une touche de fonction (il faut supprimer son utilisation dans le PROJET_DVLP, enlever deprecated puis remplacer public par
   * private)
   */
  @Deprecated
  public void HostScreenSendKey(SNPanelEcranRPG pPanel, String pTouche, boolean pClose) {
    // On vérifie que la touche n'est pas une visiblity
    oKey kTouche = getKey(pTouche);
    if ((kTouche != null) && (kTouche.getEvalVisibility() == Constantes.OFF)) {
      return;
    }
    
    // Traitement normal
    char tch = ' ';
    char indicateur[] = fluxRpg.getIndicateurs();
    
    boolean bufferEnvoye = false;
    boolean renvoyable = pPanel.isRenvoyable();
    
    // On vérifie que l'on a bien cliqué sur le bon panel (anti-polioclic)
    if (panel.getFlux().getIdClassName() != getIdPanelCourant() || panel.getFlux().getBlocageEcran() != FluxRecord.EXFMT || !renvoyable) {
      return;
    }
    panel.setRenvoyable(false);
    // Permet de lancer le sablier pendant les transactions
    if (panel.getParent() != null) {
      panel.getParent().setCursor(new Cursor(Cursor.WAIT_CURSOR));
    }
    
    // Interpretation de la valeur de la touche si besoin
    pTouche = pPanel.getProgram().getInterpreteurD().analyseExpression(pTouche);
    toucheEnvoyee = pTouche;
    // Codage de la touche pour le buffer
    if (!pTouche.equals("ENTER")) {
      if (pTouche.equals("F1") || pTouche.equals("F1_AUTOMATIQUE")) {
        tch = 'A';
      }
      else if (pTouche.equals("F2")) {
        tch = 'B';
      }
      else if (pTouche.equals("F3")) {
        tch = 'C';
      }
      else if (pTouche.equals("F4")) {
        tch = 'D';
      }
      else if (pTouche.equals("F5")) {
        tch = 'E';
      }
      else if (pTouche.equals("F6")) {
        tch = 'F';
      }
      else if (pTouche.equals("F7")) {
        tch = 'G';
      }
      else if (pTouche.equals("F8")) {
        tch = 'H';
      }
      else if (pTouche.equals("F9")) {
        tch = 'I';
      }
      else if (pTouche.equals("F10")) {
        tch = 'J';
      }
      else if (pTouche.equals("F11")) {
        tch = 'K';
      }
      else if (pTouche.equals("F12")) {
        tch = 'L';
      }
      else if (pTouche.equals("F13")) {
        tch = 'M';
      }
      else if (pTouche.equals("F14")) {
        tch = 'N';
      }
      else if (pTouche.equals("F15")) {
        tch = 'P';
      }
      else if (pTouche.equals("F16")) {
        tch = 'Q';
      }
      else if (pTouche.equals("F17")) {
        tch = 'R';
      }
      else if (pTouche.equals("F18")) {
        tch = 'S';
      }
      else if (pTouche.equals("F19")) {
        tch = 'T';
      }
      else if (pTouche.equals("F20")) {
        tch = 'U';
      }
      else if (pTouche.equals("F21")) {
        tch = 'V';
      }
      else if (pTouche.equals("F22")) {
        tch = 'W';
      }
      else if (pTouche.equals("F23")) {
        tch = 'X';
      }
      else if (pTouche.equals("F24")) {
        tch = 'Y';
      }
      else if (pTouche.equals("PGUP")) {
        indicateur[90 - 1] = '1';
      }
      else if (pTouche.equals("PGDOWN")) {
        indicateur[89 - 1] = '1';
      }
    }
    
    // Mémorise la touche qui va être envoyé au programme RPG
    if (panel != null) {
      panel.setDerniereToucheEnvoyee(pTouche);
    }
    try {
      ((ioFrame) panel).getData();
      panel.getProgram().setDebugFluxRpg(panel.getFlux(), null, false);
      
      bufferEnvoye = fluxRpg.sendBuffer(indicateur, tch, pTouche);
      // Si le buffer n'a pas pu être envoyé
      if (!bufferEnvoye) {
        Trace.erreur("Le buffer n'a pas pu être envoyé. Explication : " + fluxRpg.getMsgErreur());
        // Le panel est à nouveau envoyable puisque le buffer n'a pas pu être envoyé
        panel.setRenvoyable(true);
        // Permet de remettre le curseur normal suite au problème
        panel.getParent().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        toucheEnvoyee = null;
        return;
      }
      
      // Si le buffer a été envoyé
      // Permet de fermer la boite de dialogue si popup pour certaines touches de fonction
      if (panel.isDialog() && pClose) {
        ClosePanel();
      }
    }
    catch (Exception exception) {
      // Le panel est à nouveau envoyable si le buffer n'a pas pu être envoyé
      if (!bufferEnvoye) {
        panel.setRenvoyable(true);
      }
      // Permet de remettre le curseur normal suite à cet arrêt
      panel.getParent().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
      DialogueErreur.afficher(exception);
    }
    toucheEnvoyee = null;
  }
  
  /**
   * Positionne virtuellement le curseur en position X & Y (modification de XXCOL & XXLIG dans le buffer de donnée)
   */
  public void HostCursorPut(int ligne, int colonne) {
    // Ligne
    HostFieldPutData("XXLIG", 0, Integer.toString(ligne));
    // Colonne
    HostFieldPutData("XXCOL", 0, Integer.toString(colonne));
    
    // On recherche le nom du HostField (car avec la nouvelle méthode ça évite de
    // tout modifier tous les panels à la
    // main dans l'immédiat)
    if ((fluxRpg.getDescriptionRecord()).listeElement.containsKey("XXZON")) {
      for (Map.Entry<String, ioGeneric> e : fluxRpg.getDescriptionRecord().listeElement.entrySet()) {
        if ((e.getValue() == null) || !(e.getValue() instanceof oData)) {
          continue;
        }
        oData HostField = (oData) e.getValue();
        if (HostField.cursorIsInto(ligne, colonne, fluxRpg.getDescriptionRecord().getDebLigneCLR())) {
          // Nom du format
          HostFieldPutData("XXFMT", 0, HostField.getRecord());
          // Nom de la zone
          HostFieldPutData("XXZON", 0, HostField.getName());
          break;
        }
      }
    }
  }
  
  /**
   * Positionne virtuellement le curseur en position X & Y par rapport au nom
   * d'une variable (modification de XXCOL & XXLIG dans le buffer de donnée)
   */
  public void HostCursorPut(String nomhostfield) {
    oData HostField = null;
    
    if (nomhostfield == null) {
      return;
    }
    
    // On recherche les variables ligne, colonne et la variable ref
    if (fluxRpg.getDescriptionRecord().listeElement.containsKey(nomhostfield)) {
      HostField = (oData) (fluxRpg.getDescriptionRecord()).listeElement.get(nomhostfield);
      if (HostField != null) {
        HostCursorPut(HostField.getLigne() + fluxRpg.getDescriptionRecord().getDebLigneCLR() - 1, HostField.getColonne());
      }
      // else
      
      // Nouvelle façon de faire le F4 (via méthode Henri)
      if (HostField != null && (fluxRpg.getDescriptionRecord()).listeElement.containsKey("XXZON")) {
        // Nom du format
        HostFieldPutData("XXFMT", 0, HostField.getRecord());
        // Nom de la zone
        HostFieldPutData("XXZON", 0, nomhostfield);
      }
    }
  }
  
  /**
   * Retourne si le Hostfield est en saisie ou non Attention à la casse du nom de
   * la variable
   *
   * @param nomHostField
   */
  public boolean isOutputHostField(String nomHostField) {
    oData HostField = null;
    if (fluxRpg.getDescriptionRecord().listeElement.containsKey(nomHostField)) {
      HostField = (oData) (fluxRpg.getDescriptionRecord()).listeElement.get(nomHostField);
      return !(HostField.getZoneType() == oData.SENS_ENTREE);
    }
    
    // Par défaut on répond non
    return false;
  }
  
  /**
   * Retourne si la condition est vérifié par rapport aux indicateurs
   *
   * @param condition
   * @return
   */
  public boolean isTrue(String cond) {
    return panel.isTrue(cond);
  }
  
  /**
   * Retourne si la condition est vérifié par rapport aux indicateurs
   *
   * @param condition
   * @return
   */
  public boolean isTrue(String cond, String nomrecord) {
    return panel.isTrue(cond, nomrecord);
  }
  
  /**
   * Retourne le mode en cours (création, modification, consultation, annulation,
   * duplication ou indefini)
   *
   * @return
   */
  public int getMode() {
    return fluxRpg.getMode();
  }
  
  /**
   * Retourne si la condition est vérifié par rapport aux indicateurs d'une oData
   * (variable SDA)
   *
   * @param nomHostField
   * @return
   */
  public boolean isPresent(String nomHostField) {
    if (nomHostField == null) {
      return false;
    }
    
    oData hostfield;
    for (Map.Entry<String, oRecord> entry : panel.listeRecord.entrySet()) {
      hostfield = (oData) entry.getValue().listeElement.get(nomHostField);
      if (hostfield != null) {
        return hostfield.getEvalVisibility() == Constantes.ON;
      }
    }
    return false;
  }
  
  /**
   * Lève un indicateur donnée du buffer
   *
   * @param indicacteur
   * @return
   */
  public void SetOn(int indicateur) {
    if ((indicateur >= 1) && (indicateur <= 99)) {
      fluxRpg.setOn(indicateur);
    }
  }
  
  /**
   * Baisse un indicateur donnée du buffer
   *
   * @param indicacteur
   * @return
   */
  public void SetOff(int indicateur) {
    if ((indicateur >= 1) && (indicateur <= 99)) {
      fluxRpg.setOff(indicateur);
    }
  }
  
  /**
   * Affiche l'aide en ligne (faire certainement une fenetre indépendante car pb
   * avec les popup modales)
   *
   * @return
   */
  public void WatchHelp(String ancre) {
    if (ancre == null) {
      ancre = "";
    }
    // Voir comment mieux le gérer (A améliorer avec une classe qui gère les aides à
    // part entière)
    try {
      final viewerHTML aide = new viewerHTML(fluxRpg.getLoader().getResource(fluxRpg.getNomAide()), false);
      aide.setInitialiseUrl("#" + ancre);
      FenetreIndependante gestionAide = new FenetreIndependante(null);
      gestionAide.setTitle("Aide en ligne");
      gestionAide.setPanelPrincipal(aide, 800, 600, true);
    }
    catch (Exception e) {
    }
  }
  
  /**
   * Retourne la traduction d'un texte
   *
   * @param expression
   * @return
   */
  public String TranslationTable(String expression) {
    String chaine = null;
    
    if (ManagerSessionClient.getInstance().getEnvUser().getTranslationTable() != null) {
      chaine = ManagerSessionClient.getInstance().getEnvUser().getTranslationTable().get(expression);
      if (chaine != null) {
        return chaine;
      }
    }
    return expression;
  }
  
  /**
   * Affiche un mailto
   *
   * @return
   */
  public void Mailto(String bib) {
    SessionTransfert sessionTrf = ManagerSessionClient.getInstance().getEnvUser().getTransfertSession();
    String fichier = Constantes.SEPARATEUR_DOSSIER_CHAR + "documents" + Constantes.SEPARATEUR_DOSSIER_CHAR
        + panel.getProgram().getInterpreteurD().analyseExpression(bib) + Constantes.SEPARATEUR_DOSSIER_CHAR + "mailto.txt";
    String nomDossierTempUser =
        ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
    String chaine = nomDossierTempUser + File.separatorChar + fichier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, '_');
    
    if (!new File(chaine).exists()) {
      sessionTrf.fichierArecup(fichier);
    }
    GestionFichierTexte gft = new GestionFichierTexte(chaine);
    chaine = gft.getContenuFichierTab()[0];
    try {
      Desktop.getDesktop().mail(new URI("mailto:" + chaine));
    }
    catch (IOException ex) {
    }
    catch (URISyntaxException ex) {
      JOptionPane.showMessageDialog(null, "Adresse email du destinataire incorrecte.", "MailTo", JOptionPane.ERROR_MESSAGE);
    }
  }
  
  /**
   * Récupère un fichier dans l'IFS de l'AS/400.
   */
  public String RecuperationFichier(String pCheminCompletIfs) {
    String nomDossierTempUser =
        ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath()
            + File.separatorChar;
    
    SessionTransfert sessionTrf = ManagerSessionClient.getInstance().getEnvUser().getTransfertSession();
    String chaine;
    if (panel == null) {
      chaine = pCheminCompletIfs;
    }
    else {
      chaine = panel.getProgram().getInterpreteurD().analyseExpression(pCheminCompletIfs);
    }
    // On élimine les doubles slash qui empêche de récupérer le fichier
    if (chaine == null) {
      return null;
    }
    
    chaine = chaine.replaceAll("//", "/");
    
    // On calcule l'index pour avoir juste le nom du fichier après la récupération
    int indexnomfichier = nomDossierTempUser.length() + chaine.lastIndexOf('/') + 1;
    
    // Récupération du fichier
    sessionTrf.fichierArecup(chaine);
    
    // Création du chemin complet de l'endoit où il est stocké sur le poste client
    chaine = nomDossierTempUser + chaine.replace('\\', ' ').replace('/', '_');
    
    // On renomme le fichier réceptionné afin d'éliminer le chemin de l'IFS
    File fichierrecu = new File(chaine);
    if (fichierrecu.exists()) {
      int compteur = 0;
      // On construit le nouveau nom du fichier
      FileNG nouvfichier = new FileNG(nomDossierTempUser + chaine.substring(indexnomfichier));
      String radical = nouvfichier.getNamewoExtension(false);
      String extension = nouvfichier.getExtension(true);
      boolean boucler;
      do {
        boucler = false;
        // Si le compteur est supérieur à 0 c'est que le fichier existe déja
        if (compteur > 0) {
          nouvfichier = new FileNG(nouvfichier.getParent() + File.separatorChar + radical + String.format("(%d)", compteur) + extension);
        }
        
        if (nouvfichier.exists()) {
          // On le supprime s'il existe déjà sinon on boucle dans la limite articielle de 256 fois
          boucler = !nouvfichier.delete();
        }
        if (!boucler) {
          // On renomme sinon on boucle
          boucler = !fichierrecu.renameTo(nouvfichier);
        }
        compteur++;
      }
      while (boucler && (compteur < 256));
      return nouvfichier.getAbsolutePath();
    }
    else {
      return null;
    }
  }
  
  /**
   * Affiche un document via son application associé
   *
   * @param fichier
   */
  public void AfficheDocument(String fichier) {
    try {
      if (fichier == null) {
        throw new MessageErreurException("Le document à afficher est invalide.");
      }
      LaunchViewer lv = new LaunchViewer(fichier);
      if (!lv.lancerViewer()) {
        throw new MessageErreurException(lv.getMsgErreur());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Convertit un fichier CSV en XLS
   */
  public String ConvertCSV2XLS(String fichier) {
    GestionFichierCSV gfcsv = new GestionFichierCSV(fichier);
    gfcsv.CSVtoXLS();
    
    return gfcsv.getNomFichier();
  }
  
  /**
   * Lance le viewer de doc
   *
   * @param obj
   */
  public JPopupMenu ViewerDoc(String chemin) {
    String[] listedoc = ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().listerDossier(chemin);
    if (listedoc == null) {
      return null;
    }
    
    ConsultDocPopup cdp = new ConsultDocPopup(listedoc, ManagerSessionClient.getInstance().getEnvUser());
    return cdp.getPopupMenu();
  }
  
  /**
   * Lance le viewer de doc à partir d'un chemin complet vers un fichier
   *
   * @param chemin
   * @param supprfic
   */
  public void ViewerDocDirect(String chemin, boolean supprfic) {
    // Transformation du chemin si besoin
    new ConsultDocFrame(panel.getProgram().getInterpreteurD().analyseExpression(chemin), ManagerSessionClient.getInstance().getEnvUser(),
        supprfic);
  }
  
  /**
   * Charger une image à partir de son nom.
   *
   * La gestion des images est situéz dansx le ManagerSessionClient mais le Lexical a cette variante permettant de substituer des
   * variables de type @???@ avec des champs issus des écrans RPG.
   *
   * @param pNomImage Nom de l'image à charger.
   * @param pImageSysteme true si c'est un image système, false si c'est une image client.
   * @param pRechercheLocale true=l'image est recherchée dans les dossiers locaux du poste de travail.
   * @return Image.
   */
  public ImageIcon chargerImage(String pNomImage, boolean pImageSysteme, boolean pRechercheLocale) {
    // Construire le nom de l'image à récupérer en remplaçant les éventuels champs RPG présents dans le nom
    // On interprète les variables de type @???@
    if (panel != null) {
      pNomImage = panel.getProgram().getInterpreteurD().analyseExpression(pNomImage);
    }
    
    // Charger l'image
    return ManagerSessionClient.getInstance().chargerImage(pNomImage, pImageSysteme, pRechercheLocale);
  }
  
  /**
   * Charger une image à partir de son nom (sans recherche locale).
   *
   * La gestion des images est situéz dansx le ManagerSessionClient mais le Lexical a cette variante permettant de substituer des
   * variables de type @???@ avec des champs issus des écrans RPG.
   *
   * @param pNomImage Nom de l'image à charger.
   * @param pImageSysteme true si c'est un image système, false si c'est une image client.
   * @param pRechercheLocale true=l'image est recherchée dans les dossiers locaux du poste de travail.
   * @return Image.
   */
  public ImageIcon chargerImage(String pNomImage, boolean pImageSysteme) {
    return chargerImage(pNomImage, pImageSysteme, false);
  }
  
  /**
   * On simule l'appuie sur le clavier afin de fermer l'appliridevthcation
   */
  public void StopClient() {
    Robot robot;
    try {
      robot = new Robot();
      robot.keyPress(KeyEvent.VK_ALT);
      robot.keyPress(KeyEvent.VK_Q);
      robot.keyRelease(KeyEvent.VK_Q);
      robot.keyRelease(KeyEvent.VK_ALT);
    }
    catch (AWTException e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Permet de tester l'existence d'une icone afin de cacher ou non le bouton (ex
   * V07F)
   *
   * @param composant
   * @param image
   */
  public void setVisibilityButton(JComponent composant, String image) {
    ImageIcon icone = chargerImage(Constantes.DOSSIER_IMAGE + "/" + image, true);
    if (icone != null) {
      ((JButton) composant).setIcon(icone);
    }
    else {
      composant.setVisible(false);
    }
  }
  
  /**
   * Retourne une propriété du UI manager en cours
   *
   * @param apropriete
   * @return
   */
  public Object getSystemDefaultColor(String apropriete) {
    return UIManager.get(apropriete);
  }
  
  /**
   * Retourne l'index d'un element d'un tableau de String
   *
   * @param atab
   * @param achaine
   * @return
   */
  public int getIndexTableau(String achaine, String[] atab) {
    if ((atab == null) || (achaine == null)) {
      return 0;
    }
    achaine = achaine.trim();
    for (int i = 0; i < atab.length; i++) {
      if (atab[i].trim().equalsIgnoreCase(achaine)) {
        return i;
      }
    }
    return 0;
  }
  
  /**
   * Retourne l'indice correspondant à une hostconversion (utilisé notamment par
   * les XRiComboBox)
   *
   * @param anomData
   * @param avalue
   * @return
   */
  public int getIndice(String anomData, String[] avalue) {
    int i = 0;
    String valeur = HostFieldGetData(anomData);
    
    // On commence par les tops
    for (i = 0; i < avalue.length; i++) {
      if (avalue[i].trim().equals(valeur)) {
        return i;
      }
    }
    return 0;
  }
  
  /**
   * Supprime toute les variables
   */
  public void clearVariableGlobale() {
    VariableGlobale.clear();
  }
  
  /**
   * Ajoute ou modifie une variable globale
   *
   * @param variable
   * @param valeur
   */
  public void addVariableGlobale(String variable, Object valeur) {
    VariableGlobale.put(variable, valeur);
  }
  
  /**
   * Supprime une variable
   *
   * @param variable
   */
  public void removeVariableGlobale(String variable) {
    VariableGlobale.remove(variable);
  }
  
  /**
   * Retourne la valeur d'une variable
   *
   * @param variable
   */
  public Object getValeurVariableGlobale(String variable) {
    return VariableGlobale.get(variable);
  }
  
  /**
   * Ajoute ou modifie une variable globale d'une session en cours
   */
  public void addVariableGlobaleFromSession(String variable, Object valeur) {
    VariableGlobale.put(getNumeroSession() + Constantes.SEPARATEUR_CHAINE_CHAR + variable, valeur);
  }
  
  /**
   * Supprime une variable de la session en cours
   *
   * @param variable
   */
  public void removeVariableGlobaleFromSession(String variable) {
    VariableGlobale.remove(getNumeroSession() + Constantes.SEPARATEUR_CHAINE_CHAR + variable);
  }
  
  /**
   * Retourne la valeur d'une variable de la session en cours
   *
   * @param variable
   */
  public Object getValeurVariableGlobaleFromSession(String variable) {
    return VariableGlobale.get(getNumeroSession() + Constantes.SEPARATEUR_CHAINE_CHAR + variable);
  }
  
  /**
   * Vérifie que la police existe bien dans le cas contraire on retourne une
   * police équivalente
   *
   * @param font
   * @return
   */
  public String getFont4List(String font) {
    if (!ManagerSessionClient.getInstance().getEnvUser().isFontPresent(font)) {
      return "Monospaced";
    }
    else {
      return font;
    }
  }
  
  /**
   * Lance une calculatrice de Solde partiel en mode Singleton
   *
   * @param head
   * @param liste
   * @param indexs
   *
   */
  public void openCalculatriceSP(String[][] head, ArrayList<String> liste, int[] indexs) {
    CalculatriceSolde.getInstance(head, liste, indexs);
  }
  
  /**
   * ferme la calculatrice de Solde partiel
   */
  public void closeCalculatriceSP() {
    CalculatriceSolde.fermerCalculatriceSolde();
  }
  
  /**
   * Convertir une chaine de type C:??.?? M:??.?? Permet de tester la présence
   * d'un pattern dans une chaine de caractères pour le V03F (c'est un cas
   * particulier)
   *
   * @param pMessage
   */
  public static String convertirMessageAvecDate(String pMessage) {
    if (pMessage == null) {
      return null;
    }
    
    final String[] listeRegex = { "C:([0-9]{2})(\\p{Punct}?)[0-9]{2}", "M:([0-9]{2})(\\p{Punct}?)[0-9]{2}",
        "T:([0-9]{2})(\\p{Punct}?)[0-9]{2}", "H:([0-9]{2})(\\p{Punct}?)[0-9]{2}", "R:([0-9]{2})(\\p{Punct}?)[0-9]{2}",
        "F:([0-9]{2})(\\p{Punct}?)[0-9]{2}", "E:([0-9]{2})(\\p{Punct}?)[0-9]{2}" };
    final String[] listeTraduction =
        { "Créé le ", "Modifié le ", "Dernier transfert ", "Validé le ", "Reservé le ", "Facturé le ", "Expédié le " };
    
    for (int i = 0; i < listeRegex.length; i++) {
      try {
        // Pattern d'expressions régulières
        Pattern pattern = Pattern.compile(listeRegex[i]);
        
        // Le comparateur de chaine <=> pattern
        Matcher matcher = pattern.matcher(pMessage);
        if (matcher.find()) {
          pMessage = pMessage.replace(listeRegex[i].substring(0, 2), listeTraduction[i]);
        }
      }
      catch (PatternSyntaxException pse) {
        Trace.erreur(pse, "");
      }
    }
    
    return pMessage;
  }
  
  /**
   * Convertit le V01F en combobox de fonctions principales ( interro, créa,
   * modif...etc)
   *
   * @param chaine
   * @param combo
   */
  public void getConvertV01F(String chaine, JComboBox combo) {
    
    String expReg = "^(\\p{Upper}{1})(/{1})([\\p{Upper}]+)";
    String[] tabValeurs = null; // le tableau de fonction courante / fonctions possibles
    String tabFctCourante = null;
    String[] tabFctPossibles = null;
    Pattern p = null; // Pattern d'expressions régulières
    Matcher m = null; // Le comparateur de chaine <=> pattern
    DefaultComboBoxModel comboModel;
    
    try {
      p = Pattern.compile(expReg);
      m = p.matcher(chaine);
      comboModel = new DefaultComboBoxModel();
      if (m.find()) {
        // découpage de la string en sous-tableaux
        tabValeurs = chaine.split("/");
        tabFctCourante = tabValeurs[0];
        char[] tabCharFonctions = tabValeurs[1].trim().toCharArray();
        tabFctPossibles = new String[tabCharFonctions.length];
        String[] listeLibelle = { "Création", "Modification", "Interrogation", "Annulation", "Duplication" };
        String lettre = null;
        
        // decoupage des donctions possibles
        for (int j = 0; j < tabCharFonctions.length; j++) {
          lettre = Character.toString(tabCharFonctions[j]);
          tabFctPossibles[j] = lettre;
        }
        
        // rajouter les fonctions à la combo box
        int indice = 0;
        int nbElements = 0;
        for (int i = 0; i < tabFctPossibles.length; i++) {
          if (tabFctPossibles[i].trim().equals("C")) {
            comboModel.addElement(listeLibelle[0]);
            nbElements++;
            if (tabFctCourante.trim().equals("C")) {
              indice = nbElements - 1;
            }
          }
          else if (tabFctPossibles[i].trim().equals("M")) {
            comboModel.addElement(listeLibelle[1]);
            nbElements++;
            if (tabFctCourante.trim().equals("M")) {
              indice = nbElements - 1;
            }
          }
          else if (tabFctPossibles[i].trim().equals("I")) {
            comboModel.addElement(listeLibelle[2]);
            nbElements++;
            if (tabFctCourante.trim().equals("I")) {
              indice = nbElements - 1;
            }
          }
          else if (tabFctPossibles[i].trim().equals("A")) {
            comboModel.addElement(listeLibelle[3]);
            nbElements++;
            if (tabFctCourante.trim().equals("A")) {
              indice = nbElements - 1;
            }
          }
          else if (tabFctPossibles[i].trim().equals("D")) {
            comboModel.addElement(listeLibelle[4]);
            nbElements++;
            if (tabFctCourante.trim().equals("D")) {
              indice = nbElements - 1;
            }
          }
        }
        combo.setModel(comboModel);
        
        // sélectionner la bonne fonction
        try {
          combo.setSelectedIndex(indice);
        }
        catch (NullPointerException pe) {
          Trace.erreur(pe, "");
        }
      }
      else {
        comboModel.addElement(chaine);
        combo.setModel(comboModel);
      }
    }
    catch (PatternSyntaxException pse) {
      Trace.erreur(pse, "");
    }
  }
  
  /**
   * envoie le HostScreenSendKey (correspondant à Interro, Création,
   * Modification...etc) en fonction de la sélection
   *
   * @param lexique
   * @param comboBox1
   * @param etat le paramètre "panel" n'est pas génial faudra revoir ça un
   *          jour (peut être)
   */
  public boolean evenementielV01F(JComboBox comboBox1, Lexical lexique, boolean etat) {
    if (etat) {
      if (comboBox1.getSelectedItem().toString().equals("Création")) {
        lexique.HostScreenSendKey(panel, "F13");
      }
      else if (comboBox1.getSelectedItem().toString().equals("Modification")) {
        lexique.HostScreenSendKey(panel, "F14");
      }
      else if (comboBox1.getSelectedItem().toString().equals("Interrogation")) {
        lexique.HostScreenSendKey(panel, "F15");
      }
      else if (comboBox1.getSelectedItem().toString().equals("Annulation")) {
        lexique.HostScreenSendKey(panel, "F16");
      }
      else if (comboBox1.getSelectedItem().toString().equals("Duplication")) {
        lexique.HostScreenSendKey(panel, "F18");
      }
      etat = false;
    }
    else {
      etat = true;
    }
    
    return etat;
  }
  
  /**
   * Cette méthode permet d'ouvrir une URL passée en paramètre dans le navigateur
   * par défaut de l'utilisateur.
   *
   * @param URL
   *
   */
  public void ouvrirURL(String URL) {
    
    // On vérifie que la classe Desktop soit bien supportée :
    if (Desktop.isDesktopSupported()) {
      // On récupère l'instance du desktop :
      Desktop desktop = Desktop.getDesktop();
      
      // On vérifie que la fonction browse est bien supportée :
      if (desktop.isSupported(Desktop.Action.BROWSE)) {
        if (!URL.equals("")) {
          // Vérifier la présence d'un http:// ou d'un https://
          // +++++++++++++++++++++++++++++++++
          Pattern p = null; // Pattern d'expressions régulières
          Matcher m = null; // Le comparateur de chaine <=> pattern
          String expReg = "http://https://|";
          String complement = "";
          try {
            p = Pattern.compile(expReg);
            m = p.matcher(URL);
            if (!m.find()) {
              complement = expReg;
            }
          }
          catch (PatternSyntaxException pse) {
            Trace.erreur(pse, "");
          }
          
          // Ouvrir l'URL néttoyée dans un navigateur +++++++++++++++++++++++++
          try {
            URI monURI = new URI(complement + URL.trim());
            desktop.browse(monURI);
          }
          catch (IOException ex) {
            throw new MessageErreurException("La page spécifiée est introuvable.");
          }
          catch (URISyntaxException exp) {
            throw new MessageErreurException("Erreur de syntaxe dans votre URL.");
          }
        }
      }
    }
  }
  
  /**
   * Teste si une URL est valide
   */
  public boolean isValidURL(String url) {
    
    URL u = null;
    
    try {
      u = new URL(url);
    }
    catch (MalformedURLException e) {
      return false;
    }
    
    try {
      u.toURI();
    }
    catch (URISyntaxException e) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Cette méthode permet d'ouvrir un dossier avec l'explorateur et de le créer si
   * on le souhaite
   *
   * @param URL
   *
   */
  public void ouvrirExplorer(String dossier, boolean create) {
    // if (!System.getProperty("os.name").equalsIgnoreCase("windows")) return;
    
    if (dossier == null) {
      return;
    }
    
    // On vérifie que la classe Desktop soit bien supportée :
    if (Desktop.isDesktopSupported()) {
      // On récupère l'instance du desktop :
      Desktop desktop = Desktop.getDesktop();
      
      // On vérifie que la fonction browse est bien supportée :
      if (desktop.isSupported(Desktop.Action.BROWSE)) {
        try {
          File fdossier = new File(dossier);
          if ((create) && (!fdossier.exists())) {
            fdossier.mkdirs();
          }
          desktop.open(fdossier);
        }
        catch (IOException e) {
          Trace.erreur(e, "");
        }
      }
    }
  }
  
  /**
   * Cette méthode permet d'ouvrir un dossier avec l'explorateur s'il existe
   *
   * @param URL
   *
   */
  public void ouvrirExplorer(String dossier) {
    ouvrirExplorer(dossier, false);
  }
  
  /**
   * Supprime les fichiers d'un dossier
   *
   * @param chemin
   */
  public void viderDossier(String chemin) {
    if (chemin == null) {
      return;
    }
    
    File dossier = new File(chemin);
    if (!dossier.exists()) {
      return;
    }
    
    if (dossier.isDirectory()) {
      File[] fichier = dossier.listFiles();
      for (int i = 0; fichier != null && i < fichier.length; i++) {
        fichier[i].delete();
        // viderDossier(children[i].getAbsolutePath());
      }
    }
  }
  
  /**
   * Formate la variable globale pour l'export tableur (à destination du VEXP0AFM)
   *
   * @param nom
   */
  public void setNomFichierTableur(String nom) {
    addVariableGlobaleFromSession("NOMFIC_POUR_VEXP0A", nom);
  }
  
  /**
   * Retourne le nom du fichier pour l'export tableur (à destination du VEXP0AFM)
   */
  public String getNomFichierTableur() {
    return (String) getValeurVariableGlobaleFromSession("NOMFIC_POUR_VEXP0A");
  }
  
  /**
   * Retourne la touche que l'on a actioné pour le HostScreenSendKey
   *
   * @return
   */
  public String getToucheEnvoyee() {
    return toucheEnvoyee;
  }
  
  /**
   * Permet de faire défiler les données de la liste avec la molette de la souris
   *
   * @param table
   * @param wheelRotation le paramètre "panel" n'est pas génial faudra revoir ça
   *          un jour (peut être)
   */
  public void defileListe(JTable table, int wheelRotation) {
    if (!isPresent("WSUIS")) {
      return;
    }
    
    table.clearSelection();
    if (wheelRotation < 0) {
      HostFieldPutData("WSUIS", 0, "P");
    }
    else if (wheelRotation > 0) {
      HostFieldPutData("WSUIS", 0, "S");
    }
    HostScreenSendKey(panel, "ENTER");
  }
  
  /**
   * Permet de faire défiler les données avec la molette de la souris
   *
   * @param wheelRotation Même qu'au dessus mais sans notion de table, quand on
   *          veut faire défiler un écran équipé du WSUIS
   */
  public void defileListe(int wheelRotation) {
    if (!isPresent("WSUIS")) {
      return;
    }
    
    if (wheelRotation < 0) {
      HostFieldPutData("WSUIS", 0, "P");
    }
    else if (wheelRotation > 0) {
      HostFieldPutData("WSUIS", 0, "S");
    }
    HostScreenSendKey(panel, "ENTER");
  }
  
  /**
   * Libération de la mémoire
   */
  public void dispose() {
    panel = null;
    fluxRpg = null;
    popupPerso = null;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne le numéro de la session du panel en cours.
   */
  private int getNumeroSession() {
    if (panel != null && panel.getSession() != null) {
      return panel.getSession().getNumeroSession();
    }
    return 0;
  }
  
  /**
   * Retourne l'id du panel en cours.
   */
  private int getIdPanelCourant() {
    if (panel != null && panel.getSession() != null) {
      return panel.getSession().getIdPanelCourant();
    }
    return 0;
  }
  
  /**
   * Retourne la liste des variables globales.
   */
  public static HashMap<String, Object> getVariableGlobale() {
    return VariableGlobale;
  }
  
}
