/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc;

import java.util.Observer;

/**
 * Interface d'un modèle écran.
 *
 * Un modèle écran centralise les données destinées à être affichées par une vue ainsi que la logique métier lorsque des actions sont
 * effectuées (à ce titre, il condense les rôles "Modèle" et "Contrôleur" du pattern MVC). Il n'a pas à intervenir directement sur la vue.
 * Le modèle met à jour ses données, notifie la vue (via la méthode rafraichir) et celle-ci se met à jour.
 *
 * Lors de l'affichage initial d'un écran, les méthodes sont appelées dans l'ordre suivant :
 * 1- modele.initialiserDonnees()
 * 2- vue.initialiserComposants()
 * 3- vue.rafraichir()
 * 4- modele.chargerDonnees()
 * 5- vue.rafraichir()
 *
 * Plusieurs abstractions de cette interface sont disponibles :
 * - AbstractModeleDialogue : à utiliser pour les JDialog.
 * - AbstractModelPanel : à utiliser pour les JPanel.
 */
public interface InterfaceModele {
  /**
   * Initialiser les données avant la phase de chargement des données.
   *
   * Cela permet de nettoyer rapidement les données pour le rafraîchissement de la vue effectué avant le chargement des données.
   * Cette opération doit être rapide pour ne pas bloquer l'interface graphique. Les opérations longues doivent être effectuées dans la
   * méthode chargerDonnees().
   */
  public void initialiserDonnees();
  
  /**
   * Charger les données du modèle.
   *
   * Cette méthode est appelée lorsqu'une vue est affichée pour la première fois. Elle doit donc pouvoir être appelée plusieurs fois de
   * suite sans erreurs. Elle est appelée entre le premier rafraîchissement de la vue (sans données) et le second rafraîchissement
   * (avec données). Son rôle est de charger les données qui sont nécessaires à la vue mais qui sont longues à charger.
   *
   * Cette méthode est appelée en dehors de la thread graphique pour ne pas geler l'interface graphique. Il est interdit d'effectuer
   * des opérations graphiques dans cette méthode sous peine de créer des plantages aléatoire dans le logiciel (en particulier,
   * d'appeler la méthode rafraîchir). Les exceptions générées dans cette méthode sont récupérées pour être affichée dans une boîte de
   * dialogue à partir de la thread graphique.
   */
  public void chargerDonnees();
  
  /**
   * Rafraîchir les vues associées au modèle.
   *
   * Cette méthode est à appeler à chaque fois que le modèle souhaite déclencher un rafraîchissement des données. Elle déclenche un
   * rafraîchissement de toutes les vues qui se sont abonnées via la méthode abonnverVue().
   *
   * Il est indispensable que cette méthode soit appelée à partir de la thread graphique.
   */
  public void rafraichir();
  
  /**
   * Abonner une vue aux notifications de rafraîchissement du modèle.
   *
   * La vue ajoutée via cette méthode sera rafraîchie à chaque appel de la méthode rafraichir().
   */
  public void abonnerVue(Observer pObserver);

  /**
   * Clé de la vue enfant active.
   *
   * La clé d'une vue est un enum qui doit implémenter l'interface InterfaceCleVue. L'interface expose les méthode getIndex() et
   * getLibelle(). L'indice est utile lorsque le changement de vue s'effectue via un indice comme avec des onglets. Le libellé est utile
   * lorsque le changement de vue s'effectue via un libellé comme avec des CardLayout.
   *
   * @return Clé de la vue enfant active.
   */
  public InterfaceCleVue getCleVueEnfantActive();
  
  /**
   * Modifier la clé de la vue enfant active.
   *
   * La clé est un enum qui doit implémenter l'interface InterfaceCleVue. Chaque entrée de l'enum correspond à une vue enfant.
   * Il existe un enum standard EnumCleVueListeDetail qui contient deux valeurs : LISTE et DETAIL. Cet enum standard est utile pour les
   * écrans liste et détail qu'on souhaite gérer avec ces deux vues.
   *
   * @param pCleVueEnfantActive Clé de la vue active.
   */
  public void setCleVueEnfantActive(InterfaceCleVue pCleVueEnfantActive);
  
  /**
   * Vérifier si les données du modèle ont été chargées.
   *
   * La valeur est mise à false avant initialiserDonnees() et mise à true après chargerDonnees().
   *
   * @return true=les données dont chargées, false=les données ne sont pas chargées.
   */
  public boolean isDonneesChargees();
  
  /**
   * Indiquer si les données du modèle ont été chargées.
   *
   * Cette méthode est utilisée par le moteur. Il n'est pas recommandé de l'appeler dans les écrans.
   *
   * @param pDonneesChargees true=indiquer que les données sont chargées, false=indiquer que les données ne sont pas chargées.
   */
  public void setDonneesChargees(boolean pDonneesChargees);
  
  /**
   * Vérifier si le modèle a demandé la sortie de la vue.
   * Pour une vue de type boîte de dialogue, cela signifie que la boite de dialogue va se fermer.
   */
  public boolean isModeSortie();
  
  /**
   * Modifier le mode de sortie.
   */
  public void setModeSortie(int modeSortie);
  
  /**
   * Titre de l'écran.
   *
   * Si ce modèle à un parent, c'est le titre du parent qui est retourné. Ainsi, dans une hiérarchie de modèle, c'est toujours le titre
   * du modèle en haut de la hiérarchie qui est retourné.
   */
  public String getTitreEcran();
  
  /**
   * Modifier le titre de l'écran.
   *
   * Si ce modèle à un parent, c'est le titre du parent qui est modifié. Ainsi, dans une hiérarchie de modèle, c'est toujours le titre
   * du modèle en haut de la hiérarchie qui est modifié.
   */
  public void setTitreEcran(String pTitreEcran);
}
