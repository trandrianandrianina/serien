/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.composant;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JTextField;
import javax.swing.UIManager;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Classe abstraite des composants de sélection via une liste déroulante.
 *
 * Ce composant doit être utilisé pour tout affichage et sélection par liste déroulante d'un objet métier dans le logiciel.
 * Tous les composants de sélection de type liste déroulante doivent dériver de cette classe.
 *
 * Cette classe dérive elle même de SNComposantAvecEtablissement ce qui lui permet de bénéficier du mécanisme d'abonnement aux évènements
 * lorsque la valeur du composant change.
 *
 * La taille est par défaut à 320 car c'est un bon compris qui a été utilisé dans le RGVX08FM_A4.
 *
 * Utilisation :
 * - Créer un nouveau composant de sélection qui étend SNComboBoxObjetMetier.
 * - Implémenter les classes obligatoires.
 * - Utiliser charger(boolean pRafraichir) pour charger les données intelligemment. Pas de recharge de données si la configuration n'a pas
 * changé (rafraichir = false). Il est possible de forcer le rechargement via le paramètre pRafraichir.
 *
 * Il existe également un mode où le texte saisi dans le composant peut être récupéré via la méthode getTexteSaisi(), il s'agit du mode
 * saisi qui s'active via la méthode setModeSaisi() par défaut ce mode est inactif. Ce mode est utilisé dans la classe VueDetailCheque.
 *
 * Voir un exemple d'implémentation dans SNMagasin
 */
public abstract class SNComboBoxObjetMetier<I extends AbstractId, O extends AbstractClasseMetier, L extends ListeClasseMetier>
    extends SNComposantAvecEtablissement {
  private static final String LIBELLE_INEXISTANT = "Inexistant";
  
  private L liste = null;
  private List<O> listeSelectionnnable = null;
  private boolean tousAutorise = false;
  private boolean aucunAutorise = false;
  private I idSelection = null;
  private O selection = null;
  private boolean rafraichissementEnCours = false;
  private SNComboBoxObjetMetier<I, O, L> snComposantDebut = null;
  private SNComboBoxObjetMetier<I, O, L> snComposantFin = null;
  private boolean modeSaisiAutorise = false;
  private String texteSaisi = null;
  
  /**
   * Ne pas déclencher d'évènements ItemStateChanged pour chaque objet métier parcouru dans la liste déroulante avec le clavier.
   * Seul un appui sur la touche entrée ou un clic souris sur un objet métier déclenche l'évènement ItemStateChanged.
   * La propriété "ComboBox.noActionOnKeyNavigation" ne fonctionne qu'à partir de Java 1.8.
   * La propriété "JComboBox.isTableCellEditor" fonctionne sur Java 1.6.
   */
  {
    UIManager.getLookAndFeelDefaults().put("ComboBox.noActionOnKeyNavigation", true);
  }
  
  /**
   * Constructeur par défaut.
   */
  public SNComboBoxObjetMetier() {
    super();
    initComponents();
    
    // Fixer la taille standard du composant
    setMinimumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    setMaximumSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    if (SNCharteGraphique.TAILLE_COMPOSANT_SELECTION.width >= getWidth()) {
      setPreferredSize(SNCharteGraphique.TAILLE_COMPOSANT_SELECTION);
    }
    else {
      setPreferredSize(new Dimension(getWidth(), SNCharteGraphique.TAILLE_COMPOSANT_SELECTION.height));
    }
    
    /**
     * Ne pas déclencher d'évènements ItemStateChanged pour chaque objet métier parcouru dans la liste déroulante avec le clavier.
     * Seul un appui sur la touche entrée ou un clic souris sur un objet métier déclenche l'évènement ItemStateChanged.
     * La propriété "ComboBox.noActionOnKeyNavigation" ne fonctionne qu'à partir de Java 1.8.
     * La propriété "JComboBox.isTableCellEditor" fonctionne sur Java 1.6.
     */
    cbSelection.putClientProperty("JComboBox.isTableCellEditor", Boolean.TRUE);
    
    // Configurer le TextField
    final JTextField tfSelection = (JTextField) cbSelection.getEditor().getEditorComponent();
    tfSelection.setDisabledTextColor(SNCharteGraphique.COULEUR_TEXTE_CHAMP_DESACTIVE);
    
    // Ajouter un évènement sur le gain et la perte de focus
    tfSelection.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        tfSelectionFocusGained(e);
      }
      
      @Override
      public void focusLost(FocusEvent e) {
        tfSelectionFocusLost(e);
      }
    });
    
    // Ajouter un évènements sur la liste déroulante
    cbSelection.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        cbSelectionItemStateChanged(e);
      }
    });
    
    // Ajouter un évènement pour filtrer la liste des objets métiers au fûr et à mesure de la saisie
    tfSelection.addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent ke) {
        cbSelectionKeyPressed(ke, tfSelection.getText());
      }
      
      @Override
      public void keyReleased(KeyEvent ke) {
        cbSelectionKeyReleased(ke, tfSelection.getText());
      }
    });
  }
  
  /**
   * Méthode appelée lorsque la liste déroulante gagne le focus.
   * Sélectionner l'ensemble du texte lorsque le composant récupère le focus
   */
  private void tfSelectionFocusGained(FocusEvent e) {
    try {
      final JTextField tfSelection = (JTextField) cbSelection.getEditor().getEditorComponent();
      tfSelection.selectAll();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Méthode appelée lorsque la liste déroulante perd le focus.
   * Rafraîchir la liste des objets métiers affichés lorsque la liste déroulante perd le focus
   * C'est utile lorsque l'utilisateur saisit un filtre qui restreint la liste des objets métiers affichés. Lorsque l'utilisateur
   * quitte la liste déroulante, on remet la liste non filtrée.
   */
  private void tfSelectionFocusLost(FocusEvent e) {
    try {
      // Rafraîchir la liste déroulante
      rafraichirAffichageListeDeroulante();
      
      // Si le mode saisi est actif et que le texte saisi n'est pas vide alors l'évènement valueChanges est déclenché
      if (modeSaisiAutorise && !Constantes.normerTexte(texteSaisi).isEmpty()) {
        // Notifier le composant lui-même
        valueChanged();
        
        // Notifier les listeners
        fireValueChanged();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Méthode appelée lorsqu'une nouvelle valeur est sélectionnée dans la liste déroulante.
   *
   * Appelée lorsque :
   * - l'utilisateur sélectionne un objet dans la liste déroulante au clavier ou à la souris,
   * - la méthode setSelectedIndex ou setSelectedItem est appelée.
   *
   * Noter qu'on doit avoir : UIManager.getLookAndFeelDefaults().put("ComboBox.noActionOnKeyNavigation", true);
   * afin de pas déclencher d'évènements ItemStateChanged pour chaque objet métier parcouru dans la liste déroulante avec le clavier.
   */
  private void cbSelectionItemStateChanged(ItemEvent e) {
    try {
      // Ne rien faire si un rafraîchissement des valeurs de la liste déroulante est en cours
      if (rafraichissementEnCours) {
        return;
      }
      
      // Ne rien faire si ce n'est pas un évènement de sélection
      if (e.getStateChange() != ItemEvent.SELECTED) {
        return;
      }
      
      // Mémoriser l'objet métier sélectionné et son identifiant si la sélection est un objet métier
      Object object = cbSelection.getSelectedItem();
      if (object instanceof AbstractClasseMetier<?>) {
        selection = (O) object;
        idSelection = (I) selection.getId();
      }
      // Effacer la sélection si la sélection est la valeur "Tous" ou "Aucun"
      else if (object instanceof String && ((String) object).equals(getLibelleAucuneSelection())) {
        selection = null;
        idSelection = null;
      }
      // Effacer la sélection dans les autres cas car c'est une saisie de filtre qui est en cours
      else {
        selection = null;
        idSelection = null;
        
        // Sortir de suite car on ne souhaite pas envoyer de notifications sur une saisie de filtre en cours
        return;
      }
      
      // Rafraîchir la liste sélectionnable du composant début
      if (snComposantDebut != null) {
        snComposantDebut.rafraichirListeSelectionnable();
      }
      
      // Rafraîchir la liste sélectionnable du composant fin
      if (snComposantFin != null) {
        snComposantFin.rafraichirListeSelectionnable();
      }
      
      // Notifier le composant lui-même
      valueChanged();
      
      // Notifier les listeners
      fireValueChanged();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Etre notifié lorsque la sélection change.
   * Cette méthode est appelée lorsqu'un objet métier est sélectionné dans la liste déroulante.
   * Les classes enfants peuvent la surcharger afin d'intervenir lors de la sélection d'un objet métier.
   */
  public void valueChanged() {
  }
  
  /**
   * Tester si la touche appuyée est à traiter.
   */
  private boolean isToucheATraiter(KeyEvent ke) {
    return ke.getKeyCode() != KeyEvent.VK_DOWN && ke.getKeyCode() != KeyEvent.VK_UP & ke.getKeyCode() != KeyEvent.VK_LEFT
        && ke.getKeyCode() != KeyEvent.VK_RIGHT && ke.getKeyCode() != KeyEvent.VK_HOME && ke.getKeyCode() != KeyEvent.VK_SHIFT
        && ke.getKeyCode() != KeyEvent.VK_ESCAPE && ke.getKeyCode() != KeyEvent.VK_ENTER;
  }
  
  /**
   * Méthode appelée lorsqu'une touche est enfoncée dans la zone de texte de la liste déroulante.
   * Effacer le texte présent dans la liste déroulante lorsque l'utilisateur commence une saisie.
   * On détecte que l'utilisateur commence une saisie car un caractère est saisie et qu'il y a une sélection en cours.
   */
  private void cbSelectionKeyPressed(KeyEvent ke, String pSaisie) {
    try {
      // Tester si la touche saisie est à traiter
      if (!isToucheATraiter(ke)) {
        return;
      }
      
      // Tester s'il y a une sélection en cours
      // Si c'est le cas, on désectionne l'entrée actuellement sélectionnée et on notifie les composants potentiellement concernés
      if (idSelection != null || selection != null || Constantes.equals(cbSelection.getSelectedItem(), getLibelleAucuneSelection())) {
        // Effacer la sélection en cours
        idSelection = null;
        selection = null;
        
        // Rafraîchir la liste sélectionnable du composant début
        if (snComposantDebut != null) {
          snComposantDebut.rafraichirListeSelectionnable();
        }
        
        // Rafraîchir la liste sélectionnable du composant fin
        if (snComposantFin != null) {
          snComposantFin.rafraichirListeSelectionnable();
        }
        
        // Notifier le composant lui-même
        valueChanged();
        
        // Notifier les listeners
        fireValueChanged();
        
        // Effacer le texte affiché dans la liste déroulante pour permettre l'affichage de ce qui va être saisi
        if (!modeSaisiAutorise) {
          cbSelection.setSelectedItem(null);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Méthode appelée lorsqu'une touche est relachée dans la zone de texte de la liste déroulante.
   * Gère le filtrage de l'affichage des objets métiers dans la liste déroulante en fonction du texte saisi.
   * Ce filtrage est purement visuel pour l'affichage. La liste des objets métiers reste inchangée.
   */
  private void cbSelectionKeyReleased(KeyEvent ke, String pSaisie) {
    try {
      // Récupération du texte saisi
      if (modeSaisiAutorise) {
        texteSaisi = pSaisie;
      }
      
      // Rafraîchir la liste déroulante si la touche ECHAP est appuyée
      if (ke.getKeyCode() == KeyEvent.VK_ESCAPE) {
        rafraichirAffichageListeDeroulante();
        return;
      }
      
      // Tester si la touche saisie est à traiter
      if (!isToucheATraiter(ke)) {
        return;
      }
      
      // Normaliser et séparer le texte saisie
      // Si le texte saisie comporte des espaces, chaque mot saisi est recherché
      String saisieNormalisee = pSaisie.toUpperCase();
      saisieNormalisee = Normalizer.normalize(saisieNormalisee, Normalizer.Form.NFD);
      saisieNormalisee = saisieNormalisee.replaceAll("[^\\x00-\\x7F]", "");
      String[] listeMotSaisie = saisieNormalisee.split(" ");
      
      // Filtrer les objets métiers à partir des différents mots saisis dans la recherche
      List<O> listeObjetMetierVisible = listeSelectionnnable;
      for (String motSaisi : listeMotSaisie) {
        listeObjetMetierVisible = filtrerListeObjetMetier(listeObjetMetierVisible, motSaisi);
      }
      
      // Mettre à jour la liste déroulante avec la liste des objets métiers à afficher.
      cbSelection.setModel(new DefaultComboBoxModel(listeObjetMetierVisible.toArray()));
      
      // Mettre à jour le texte en cours de saisie dans la liste déroulante
      cbSelection.setSelectedItem(pSaisie);
      
      // Afficher ou masquer la popup suivant la présence d'entrée dans la liste sélectionnable
      if (listeObjetMetierVisible.size() == 0) {
        cbSelection.hidePopup();
      }
      else {
        cbSelection.showPopup();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Filtrer la liste des objets métiers en ne conservant que ceux possédant le mot saisi.
   *
   * L'objet métier est conservé si l'un des mots du libellé ou de l'identifiant commence par le texte à rechercher. Cela marche aussi
   * pour des identifiants comportant des espaces qui sont considérés comme plusieurs mots. De plus, comme l'identifiant est affiché avec
   * des parenthèses dans la liste déroulante, l'utilisateur peut être tenté de saisir des parenthèses autour de l'identfifant lors de sa
   * recherche. Ce cas de figure est également pris en compte.
   *
   * Les textes sont normalisés pour faciliter les recherches : tout en majuscules et sans accents.
   */
  private List<O> filtrerListeObjetMetier(List<O> pListeObjetMetier, String pMotSaisi) {
    // Créer la liste contenant le résultat
    List<O> listeObjetMetierFiltre = new ArrayList<O>();
    
    // Parcourir les objets métiers afin de filtrer sur le mot fourni
    for (Object object : pListeObjetMetier) {
      O objectMetier = (O) object;
      String texte = "";
      
      // Ajouter au texte à scanner le libellé de l'objet métier normalisé
      String texteObjetMetier = objectMetier.getTexteNormalise();
      if (texteObjetMetier != null) {
        texte += texteObjetMetier;
      }
      
      // Ajouter au texte à scanner l'identifiant de l'objet métier (avec et sans parenthèses)
      // On procède comme cela car il faut également séparer l'identifiant en mots si celui-ci est composé d'espaces
      String texteId = objectMetier.getId().getTexte();
      if (texteId != null) {
        texte += " " + texteId;
        texte += " (" + texteId + ")";
      }
      
      // Séparer le texte à scanner en mots
      String[] listeMot = texte.split(" ");
      
      // Ajouter l'objet métier à la liste si l'un de ses mots commence par le mot saisi
      for (String mot : listeMot) {
        if (mot.startsWith(pMotSaisi)) {
          listeObjetMetierFiltre.add(objectMetier);
          break;
        }
      }
    }
    
    return listeObjetMetierFiltre;
  }
  
  /**
   * Rafraîchir l'affichage de l'objet métier sélectionné.
   */
  private void rafraichirAffichageSelection() {
    // Tester si aucun objet métier n'est sélectionné
    // Dans ce cas, il faut effacer la sélection
    if (idSelection == null) {
      // Sélectionner "Tous" ou "Aucun" si cette valeur est autorisée
      if (tousAutorise || aucunAutorise) {
        cbSelection.setSelectedItem(getLibelleAucuneSelection());
      }
      else {
        // Ne rien sélectionner dans le cas contraire si le mode saisi est inactif
        if (!modeSaisiAutorise) {
          cbSelection.setSelectedItem(null);
        }
        // Afficher le texte saisi si le mode saisi est actif
        else {
          cbSelection.setSelectedItem(texteSaisi);
        }
      }
    }
    // Dans ce dernier cas, un objet métier est sélectionné et il faut mettre à jour la liste déroulante en conséquence
    else {
      // Rechercher l'objet métier à sélectionner dans la liste
      O objetMetierTrouve = null;
      if (listeSelectionnnable != null) {
        for (O objetMetier : listeSelectionnnable) {
          if (objetMetier != null && objetMetier.getId().equals(idSelection)) {
            objetMetierTrouve = objetMetier;
          }
        }
      }
      
      // Sélectionner l'objet métier car il est présent dans la liste (cas normal)
      if (objetMetierTrouve != null) {
        cbSelection.setSelectedItem(objetMetierTrouve);
      }
      // Afficher un texte particulier si l'objet métier n'a pas été trouvé dans la liste et si le mode saisi est incatif
      // Ce cas se rencontre si on doit afficher un champ issu de la base de données et que la personnalisation correspondante a été
      // effacée. Par exemple, un écran contient la valeur "RC" comme représentant mais le représentant "RC" n'existe plus.
      else if (!modeSaisiAutorise) {
        cbSelection.setSelectedItem(LIBELLE_INEXISTANT + " (" + idSelection.toString() + ")");
      }
    }
  }
  
  /**
   * Mettre à jour le contenu de la liste déroulante avec les objets métiers de la liste.
   */
  private void rafraichirAffichageListeDeroulante() {
    // Bloquer les notifications du listener si on est en cours de chargement de la liste
    rafraichissementEnCours = true;
    
    // Mettre à jour la liste
    cbSelection.removeAllItems();
    
    // Ajouter la valeur "Tous" ou "Aucun" si c'est souhaité
    if (tousAutorise || aucunAutorise) {
      cbSelection.addItem(getLibelleAucuneSelection());
    }
    
    // Remplir la liste déroulante à partir de la liste d'objets
    if (listeSelectionnnable != null) {
      for (O objetMetier : listeSelectionnnable) {
        cbSelection.addItem(objetMetier);
      }
    }
    
    // Resélectionner l'objet sélectionné
    rafraichirAffichageSelection();
    
    // Réactiver les notifications
    rafraichissementEnCours = false;
  }
  
  /**
   * Rafraichir la liste des objets métiers en ne conservant que ceux que l'on souhaite pouvoir sélectionner.
   * Surcharger la méthode isSelectionnable() pour réduire la liste des objets sélectionnables.
   */
  public void rafraichirListeSelectionnable() {
    // Construire la liste des objets métiers à afficher
    listeSelectionnnable = new ArrayList<O>();
    if (liste != null) {
      for (Object object : liste) {
        O objetMetier = (O) object;
        
        // Ne pas sélectionner les objets nulls
        if (objetMetier == null) {
          continue;
        }
        
        // Ne pas sélectionner les objets métiers dont l'identifiant est inférieur à l'objet métier de début
        if (snComposantDebut != null && snComposantDebut.getIdSelection() != null
            && objetMetier.getId().compareTo(snComposantDebut.getIdSelection()) < 0) {
          continue;
        }
        
        // Ne pas sélectionner les objets métiers dont l'identifiant est supérieur à l'objet métier de fin
        if (snComposantFin != null && snComposantFin.getIdSelection() != null
            && objetMetier.getId().compareTo(snComposantFin.getIdSelection()) > 0) {
          continue;
        }
        
        // Demander à la classe enfant si l'objet est sélectionnable
        if (isSelectionnable(objetMetier)) {
          listeSelectionnnable.add(objetMetier);
        }
      }
    }
    
    // Supprimer la sélection s'il ne fait pas partie de la liste
    if (idSelection != null && !isSelectionnable(idSelection)) {
      idSelection = null;
      selection = null;
    }
    
    // Rafraîchir le contenu de la liste déroulante
    rafraichirAffichageListeDeroulante();
  }
  
  /**
   * Charger la liste des objets métiers.
   * La liste n'est pas rechargée si les paramètres n'ont pas changés, sauf si un rafraîchissement explicite a été demandé lors de l'appel
   * de la méthode.
   */
  public void charger(boolean pForcerRafraichissement, L plisteObjetMetier) {
    // Ne pas rafraîchir la liste si elle est déjà chargée (sauf si explicitement demandé)
    if (liste != null && !pForcerRafraichissement && !isForcerRechargement()) {
      return;
    }
    setForcerRechargement(false);
    
    // Vérifier que la session et l'établissement sont renseignés
    // Ces informations peuvent ne pas être renseignées lorsque les composants d'un écran sont rafraichis très tôt, avant même le
    // chargement des données. Dans ce cas, le composant doit retourner une liste vide.
    // Nous ne testons pas la nullité de l'identifiant établissement car cet identifiant peut, dans de rares cas, ne pas avoir de sens
    // pour un objet métier (exemple : civilités)
    if (getIdSession() != null) {
      // Charger la liste des objets
      liste = (L) plisteObjetMetier.charger(getIdSession(), getIdEtablissement());
    }
    else {
      liste = null;
    }
    
    // Effacer la sélection en cours
    idSelection = null;
    selection = null;
    
    // Réduire la liste des objets métiers à ceux que l'on souhaite conserver
    // Le filtrage effectue également le rafraichissement de la liste déroulante
    rafraichirListeSelectionnable();
  }
  
  /**
   * Retourner la liste de objets métiers de la liste déroulante.
   *
   * Tous les objets métiers de la liste sont retournés, y compris ceux qui sont éventuellement masqués car il ne sont pas sélectionnable.
   * Noter qu'il ne faut pas modifier le contenu de cette liste car les modifications seront mal prise en dcompte par la liste déroulante.
   *
   * @return Liste des objets métiers.
   */
  protected L getListe() {
    return liste;
  }
  
  /**
   * Modifier la liste des objets métiers de la liste déroulante.
   *
   * La liste des objets métiers de la liste déroulante est mise à jour. Seuls les objets sélectionnables seront affichés.
   *
   * @param pListeObjetMetier Nouvelle liste d'objets métiers.
   */
  public void setListe(L pListeObjetMetier) {
    if (Constantes.equals(liste, pListeObjetMetier)) {
      return;
    }
    liste = pListeObjetMetier;
    
    // Effacer la sélection en cours
    idSelection = null;
    selection = null;
    
    // Réduire la liste des objets métiers à ceux que l'on souhaite conserver
    // Le filtrage effectue également le rafraichissement de la liste déroulante
    rafraichirListeSelectionnable();
  }

  /**
   * Ajouter un objet métier à la liste des obejst métiers gérés par la liste déroulante.
   *
   * La liste des objets métiers de la liste déroulante est mise à jour. Seuls les objets sélectionnables seront affichés.
   *
   * @param pListeObjetMetier Nouvelle liste d'objets métiers.
   */
  public void ajouter(O pObjetMetier) {
    // Vérifier les paramètres
    if (pObjetMetier == null) {
      return;
    }

    // Ne pas ajouter deux fois le même objet métier dans la liste
    if (liste.contains(pObjetMetier)) {
      return;
    }

    // Ajouter l'objet métier à la liste
    liste.add(pObjetMetier);
    
    // Réduire la liste des objets métiers à ceux que l'on souhaite conserver
    // Le filtrage effectue également le rafraichissement de la liste déroulante
    rafraichirListeSelectionnable();
  }

  /**
   * Activer ou désactiver le composant.
   */
  @Override
  public void setEnabled(boolean pEnabled) {
    super.setEnabled(pEnabled);
    cbSelection.setEnabled(pEnabled);
  }
  
  /**
   * Indiquer si un objet métier est sélectionnable dans la liste déroulante.
   *
   * L'implémentation par défaut de cette méthode retourne tout le temps "true" si bien que tous les objets métiers chargés sont
   * sélectionnables et donc affichés dans la liste déroulante. Les classes dérivées peuvent surcharger cette méthode pour indiquer
   * les objets métiers sélectionnables suivant leur propres règles de gestion . Ceux qui ne le sont pas ne seront pas présent dans
   * la liste déroulante.
   *
   * Exemple dans la classe Famille.
   */
  public boolean isSelectionnable(O pObjetmetier) {
    return true;
  }
  
  /**
   * Indiquer si un objet métier est sélectionnable dans la liste déroulante en fournissant son identifiant.
   * Il est sélectionnable s'il n'a pas été filtré fonctionnellement.
   */
  public boolean isSelectionnable(I pIdObjetMetier) {
    // Rechercher l'objet métier dans la liste
    if (listeSelectionnnable != null) {
      for (O objetMetier : listeSelectionnnable) {
        if (objetMetier != null && objetMetier.getId().equals(pIdObjetMetier)) {
          return true;
        }
      }
    }
    
    return false;
  }
  
  /**
   * Retourner l'objet métier sélectionné (null si aucune sélection).
   * Il est préférable d'utiliser getIdSelection() dans la mesure du possible. En effet, si l'objet métier à afficher n'existe plus
   * dans la base de données, le composant ne connaîtra que son identifiant. La méthode getIdSelection() retournera donc un
   * identifiant alors que la méthode getSelection() retournera null.
   * Si vous devez utiliser getSelection(), il faut gérer convenablement le cas ou null est retourné.
   */
  public O getSelection() {
    return selection;
  }
  
  /**
   * Retourner l'identifiant de l'objet métier sélectionné (null si aucune sélection).
   */
  public I getIdSelection() {
    return idSelection;
  }
  
  /**
   * Sélectionner un objet métier.
   * La méyhode récupére l'idenfiant de l'objet métier et utilise la sélection par identifiant pour effectuer la sélection.
   */
  public void setSelection(O pSelection) {
    if (pSelection != null) {
      setIdSelection((I) pSelection.getId());
    }
    else {
      setIdSelection(null);
    }
  }
  
  /**
   * Sélectionner un objet métier par son identifiant.
   */
  public void setIdSelection(I pIdSelection) {
    // Vérifier si la sélection a changée
    if (Constantes.equals(pIdSelection, idSelection)) {
      return;
    }
    
    // Effacer la sélection du composant de début si sa sélection courante est supérieure à celle-ci
    if (pIdSelection != null && snComposantDebut != null && snComposantDebut.getIdSelection() != null
        && pIdSelection.compareTo(snComposantDebut.getIdSelection()) < 0) {
      snComposantDebut.setIdSelection(null);
    }
    
    // Effacer la sélection du composant de fin si sa sélection courante est inférieure à celle-ci
    if (pIdSelection != null && snComposantFin != null && snComposantFin.getIdSelection() != null
        && pIdSelection.compareTo(snComposantFin.getIdSelection()) > 0) {
      snComposantFin.setIdSelection(null);
    }
    
    // Rechercher l'objet métier à sélectionner dans la liste des objets sélectionnables
    O objetMetierTrouve = null;
    if (listeSelectionnnable != null) {
      for (O objetMetier : listeSelectionnnable) {
        if (objetMetier != null && objetMetier.getId().equals(pIdSelection)) {
          objetMetierTrouve = objetMetier;
        }
      }
    }
    
    // Mémoriser l'objet métier en tant que sélection s'il est présent dans la liste des objets métiers sélectionnables.
    if (objetMetierTrouve != null) {
      idSelection = pIdSelection;
      selection = objetMetierTrouve;
    }
    // Tester si l'objet métier est dans la liste complète des objets métiers.
    // Dans ce cas, l'objet est connu mais il n'est pas dans la liste des objets sélectionnables, cela ressemble à une
    // anomalie fonctionnelle. On corrige le coup en ne sélectionnant rien.
    else if (liste != null && liste.contains(pIdSelection)) {
      idSelection = null;
      selection = null;
    }
    // L'objet n'est pas connu du tout, il a peut-être été supprimé et seul son identifiant est référencé dans d'autres objets métiers.
    // On sélectionne l'identifiant de cet objet métier pour afficher quelque chose à l'écran (et ne pas perdre l'information).
    else {
      idSelection = pIdSelection;
      selection = null;
    }
    
    // Rafraichir l'affichage de la sélection
    rafraichirAffichageSelection();
  }
  
  /**
   * true si la liste déroulante permet de sélectionner le choix "Tous".
   */
  public boolean isTousAutorise() {
    return tousAutorise;
  }
  
  /**
   * Indique si la liste déroulante permet de sélectionner le choix "Tous".
   * Il n'est pas possible d'autoriser "Tous" ou "Aucun" simultanément.
   * @param pTousAutorise Si ce paramètre est à true, la liste déroulante propose le choix "Tous".
   */
  public void setTousAutorise(boolean pTousAutorise) {
    // Tester si la valeur a changée
    if (tousAutorise == pTousAutorise) {
      return;
    }
    
    // Mettre à jour la valeur
    tousAutorise = pTousAutorise;
    if (tousAutorise) {
      aucunAutorise = false;
    }
    
    // Rafraichir l'affichage de la liste déroulante pour ajouter ou supprimer le "Tous"
    rafraichirAffichageListeDeroulante();
  }
  
  /**
   * true si la liste déroulante permet de sélectionner le choix "Aucun".
   */
  public boolean isAucunAutorise() {
    return aucunAutorise;
  }
  
  /**
   * Indique si la liste déroulante permet de sélectionner le choix "Aucun".
   * Il n'est pas possible d'autoriser "Tous" ou "Aucun" simultanément.
   * @param pAucunAutorise Si ce paramètre est à true, la liste déroulante propose le choix "Aucun".
   */
  public void setAucunAutorise(boolean pAucunAutorise) {
    // Tester si la valeur a changée
    if (aucunAutorise == pAucunAutorise) {
      return;
    }
    
    // Mettre à jour la valeur
    aucunAutorise = pAucunAutorise;
    if (aucunAutorise) {
      tousAutorise = false;
    }
    
    // Rafraichir l'affichage de la liste déroulante pour ajouter ou supprimer le "Aucun"
    rafraichirAffichageListeDeroulante();
  }
  
  /**
   * Retourner le libellé à employer lorsqu'aucun objet métier n'est sélectionné.
   * - " " si le composant fait partie d'une plage.
   * - "Tous" si le composant est utilisé en tant que critère de filtrage.
   * - "Aucun" si le composant est utilisé en tant que composant de saisi.
   */
  private String getLibelleAucuneSelection() {
    // Retourner un libellé " " si le composant fait partie d'une plage
    // " " pour que la ligne soit de hauteur normale dans la liste déroulante
    if (snComposantDebut != null || snComposantFin != null) {
      return " ";
    }
    else if (tousAutorise) {
      // Retourner "Tous" si le composant est utilisé en tant que critère de filtrage.
      return SNCharteGraphique.TEXTE_TOUS;
    }
    else if (aucunAutorise) {
      // Retourner "Aucun" si le composant est utilisé en tant que composant de saisi.
      return "Aucun";
    }
    else {
      // Rien par défaut
      return " ";
    }
  }
  
  /**
   * Retourner le composant début associé à ce composant.
   */
  public SNComboBoxObjetMetier<I, O, L> getComposantDebut() {
    return snComposantDebut;
  }
  
  /**
   * Lier un composant de début dans le cadre d'une plage de composants.
   */
  public void lierComposantDebut(SNComboBoxObjetMetier<I, O, L> psnComposantDebut) {
    // Tester si c'est nous-même
    if (psnComposantDebut == this) {
      throw new MessageErreurException("Impossible de lier un composant à lui-même");
    }
    
    // Tester si la valeur a changée
    if (Constantes.equals(psnComposantDebut, snComposantDebut)) {
      return;
    }
    
    // Défaire le lien de l'ancien composant début
    // Attention : snComposantDebut est mis à null avant appel pour éviter une boucle infinie
    SNComboBoxObjetMetier<I, O, L> snAncienComposantDebut = snComposantDebut;
    snComposantDebut = null;
    if (snAncienComposantDebut != null) {
      snAncienComposantDebut.lierComposantFin(null);
    }
    
    // Mettre à jour la valeur
    snComposantDebut = psnComposantDebut;
    
    // Faire le lien dans l'autre sens sur l'autre composant
    if (snComposantDebut != null) {
      snComposantDebut.lierComposantFin(this);
    }
    
    // Rafraichir la liste des composants sélectionnable
    rafraichirListeSelectionnable();
  }
  
  /**
   * Retourner le composant fin associé à ce composant.
   */
  public SNComboBoxObjetMetier<I, O, L> getComposantFin() {
    return snComposantFin;
  }
  
  /**
   * Lier un composant de fin dans le cadre d'une plage de composants.
   */
  public void lierComposantFin(SNComboBoxObjetMetier<I, O, L> psnComposantFin) {
    // Tester si c'est nous-même
    if (psnComposantFin == this) {
      throw new MessageErreurException("Impossible de lier un composant à lui-même");
    }
    
    // Tester si la valeur a changée
    if (Constantes.equals(psnComposantFin, snComposantFin)) {
      return;
    }
    
    // Défaire le lien de l'ancien composant fin
    // Attention : snComposantFin est mis à null avant appel pour éviter une boucle infinie
    SNComboBoxObjetMetier<I, O, L> snAncienComposantFin = snComposantFin;
    snComposantFin = null;
    if (snAncienComposantFin != null) {
      snAncienComposantFin.lierComposantDebut(null);
    }
    
    // Mettre à jour la valeur
    snComposantFin = psnComposantFin;
    
    // Faire le lien dans l'autre sens sur l'autre composant
    if (snComposantFin != null) {
      snComposantFin.lierComposantDebut(this);
    }
    
    // Rafraichir la liste des composants sélectionnable
    rafraichirListeSelectionnable();
  }
  
  /**
   * Renvoyer le nombre d'objets métiers chargés par le composant et sélectionnables
   */
  public int getNombreObjetMetierSelectionnnable() {
    return listeSelectionnnable.size();
  }
  
  /**
   * Activer le mode saisie.
   */
  public void setModeSaisiAutorise(boolean modeSaisiAutorise) {
    this.modeSaisiAutorise = modeSaisiAutorise;
  }
  
  /**
   * Récupération du texte saisi en mode saisi.
   */
  public String getTexteSaisi() {
    if (modeSaisiAutorise) {
      return texteSaisi;
    }
    return null;
  }

  /**
   * Force l'affichage d'un texte.
   * Dans certains cas rare, il est nécessaire d'afficher un texte dans le composant. Comme par exemple, dans le cas où un identifiant n'a
   * pas été trouvé mais un libellé est présent est doit absoluement être affiché (cas du snPays pae exemple).
   */
  protected void setForceTexte(String pTexteForce) {
    if (pTexteForce == null) {
      return;
    }
    cbSelection.setSelectedItem(pTexteForce);
  }
  
  /**
   * Sélectionner un objet métier à partir de la valeur d'un champ RPG.
   * Cette méthode est à surcharger car son implémentation dépend de l'objet métier.
   */
  abstract public void setSelectionParChampRPG(Lexical pLexical, String pNomChampRPG);
  
  /**
   * Renseigner le champ RPG correspondant à l'objet métier sélectionné.
   * Cette méthode est à surcharger car son implémentation dépend de l'objet métier.
   */
  abstract public void renseignerChampRPG(Lexical pLexical, String pNomChampRPG);
  
  /**
   * Code JFormDesigner.
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    cbSelection = new SNComboBox();
    
    // ======== this ========
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
    
    // ---- cbSelection ----
    cbSelection.setEditable(true);
    cbSelection.setMaximumRowCount(15);
    cbSelection.setName("cbSelection");
    add(cbSelection,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNComboBox cbSelection;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
