/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.mvc.panel;

import java.util.Observable;
import java.util.Observer;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.InterfaceModele;

/**
 * Modèle associé à une vue JPanel.
 *
 * Voir la documentation de InterfaceModele pour plus d'informations.
 */
public abstract class AbstractModelePanel extends Observable implements InterfaceModele {
  // Variables
  private final SessionBase session;
  private InterfaceCleVue cleVueEnfantActive = null;
  private String titreEcran = "";
  private boolean donneesChargees = false;

  /**
   * Constructeur.
   */
  public AbstractModelePanel(SessionBase pSession) {
    session = pSession;
  }

  // Méthodes de InterfaceModele

  @Override
  abstract public void initialiserDonnees();

  @Override
  abstract public void chargerDonnees();

  @Override
  public final void rafraichir() {
    setChanged();
    notifyObservers();
  }

  @Override
  public final void abonnerVue(Observer pObserver) {
    addObserver(pObserver);
  }

  @Override
  public final InterfaceCleVue getCleVueEnfantActive() {
    return cleVueEnfantActive;
  }

  @Override
  public final void setCleVueEnfantActive(InterfaceCleVue pCleVueEnfantActive) {
    cleVueEnfantActive = pCleVueEnfantActive;
  }

  @Override
  public final boolean isDonneesChargees() {
    return donneesChargees;
  }

  @Override
  public final void setDonneesChargees(boolean pDonneesChargees) {
    donneesChargees = pDonneesChargees;
  }

  @Override
  public final boolean isModeSortie() {
    return false;
  }

  @Override
  public final void setModeSortie(int modeSortie) {
  }

  @Override
  public final String getTitreEcran() {
    return titreEcran;
  }

  @Override
  public final void setTitreEcran(String pTitreEcran) {
    titreEcran = pTitreEcran;
  }

  /**
   * Session associée au modèle.
   */
  public final SessionBase getSession() {
    if (session == null) {
      throw new MessageErreurException("Aucune session n'est associée au modèle.");
    }
    return session;
  }

  /**
   * Identifiant de la session associée au modèle.
   */
  public IdSession getIdSession() {
    if (session == null) {
      throw new MessageErreurException("Aucune session n'est associée au modèle.");
    }
    return session.getIdSession();
  }
}
