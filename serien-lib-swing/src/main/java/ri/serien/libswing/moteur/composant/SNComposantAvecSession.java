/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libswing.moteur.composant;

import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libswing.moteur.interpreteur.SessionBase;

/**
 * SNComposant est la classe mère de tous les composants métiers (composant de sélection, de saisie ou d'affichage). Chaque composant
 * métier doit hériter de la classe SNComposant ou d'une de ses classes dérivées.
 * 
 * Voici la description des classes SNComposant disponibles :
 * 
 * - SNComposant : adapté pour les composants de saisie qui n'ont pas à interagir avec la base de données. Par exemple, le composant de
 * saisie d'une plage de dates.
 * 
 * - SNComposantAvecSession : adapté pour les composants qui nécessitent un accès à la base de données mais qui n'ont pas besoin de
 * connaître l'établissement en cours. Par exemple, le composant de sélection d'un contact.
 * 
 * - SNComposantAvecEtablissement : adapté pour les composants qui nécessitent un accès à la base de données et qui ont besoin de
 * connaître
 * l'établissement courant. Par exemple, le composant de sélection d'un client.
 * 
 * 
 * L'apport principal de la classe SNComposant est de mettre à disposition un système de notifications via l'interface
 * InterfaceSNComposantListener. La classe JPanel ne propose pas de notifications pertinentes pour informer la vue qu'une saisie a été
 * effectuée dans le composant. La méthode fireValueChanged() permet de prévenir les abonnés que le composant a changé de valeur.
 * 
 */

abstract public class SNComposantAvecSession extends SNComposant {
  private SessionBase session = null;
  
  /**
   * Constructeur par défaut.
   */
  public SNComposantAvecSession() {
    setName("SNPanelComposantAvecSession");
  }
  
  /**
   * Retourner l'identifiant de la session.
   */
  public IdSession getIdSession() {
    if (session == null) {
      return null;
    }
    return session.getIdSession();
  }
  
  /**
   * Retourner la session
   */
  public SessionBase getSession() {
    return session;
  }
  
  /**
   * Modifier la session.
   */
  public void setSession(SessionBase pSession) {
    session = pSession;
  }
}
