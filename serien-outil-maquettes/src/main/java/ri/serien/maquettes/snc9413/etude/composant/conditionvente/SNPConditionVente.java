/*
 * Created by JFormDesigner on Tue Dec 07 16:33:18 CET 2021
 */

package ri.serien.maquettes.snc9413.etude.composant.conditionvente;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;
import ri.serien.maquettes.snc9413.etude.metier.EnumOrigine;
import ri.serien.maquettes.snc9413.etude.metier.condition.EnumCategorieConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.condition.EnumTypeConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.formulecalculprix.FormulePrixFP;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreConditionVente;

/**
 * Composant permettant d'afficher toutes les informations d'un tarif.
 */
public class SNPConditionVente extends SNPanel {
  // Variables
  private ParametreConditionVente parametreConditionVente = null;
  
  /**
   * Constructeur.
   */
  public SNPConditionVente() {
    super();
    initComponents();
  }
  
  // -- Méthodes publiques
  
  /**
   * Modifier les paramètres de la condition de ventes.
   */
  public void modifierParametreConditionVente(ParametreConditionVente pParametreConditionVente) {
    if (pParametreConditionVente == null) {
      return;
    }
    
    parametreConditionVente = pParametreConditionVente;
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichissement des données du composant.
   */
  private void rafraichir() {
    rafraichirOrigine();
    rafraichirCode();
    rafraichirDateDebutValidite();
    rafraichirDateFinValidite();
    rafraichirCodeDevise();
    rafraichirCategorie();
    rafraichirType();
    rafraichirTypeGratuit();
    rafraichirQuantiteMinimale();
    rafraichirPrixBase();
    rafraichirPrixNet();
    rafraichirRemiseEnValeur();
    rafraichirAjoutEnValeur();
    rafraichirRemiseEnPourcentage();
    rafraichirModeApplicationRemise();
    rafraichirCoefficientMultiplicateur();
    rafraichirCodeFormulePrix();
    rafraichirColonneTarif();
  }
  
  /**
   * Rafraichit l'origine de la condition de vente.
   */
  private void rafraichirOrigine() {
    if (parametreConditionVente == null || parametreConditionVente.getOrigine() == null) {
      tfPOrigine.setText("");
      return;
    }
    EnumOrigine origine = parametreConditionVente.getOrigine();
    tfPOrigine.setText(origine.getLibelle());
  }
  
  /**
   * Rafraichit le code.
   */
  private void rafraichirCode() {
    if (parametreConditionVente == null || parametreConditionVente.getCode() == null) {
      tfPCode.setText("");
      return;
    }
    String code = parametreConditionVente.getCode();
    tfPCode.setText(code);
  }
  
  /**
   * Rafraichit la date de début de validité.
   */
  private void rafraichirDateDebutValidite() {
    if (parametreConditionVente == null || parametreConditionVente.getDateDebutValidite() == null) {
      tfPDateDebutValidite.setText("");
      return;
    }
    Date date = parametreConditionVente.getDateDebutValidite();
    tfPDateDebutValidite.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
  }
  
  /**
   * Rafraichit la date de fin de validité.
   */
  private void rafraichirDateFinValidite() {
    if (parametreConditionVente == null || parametreConditionVente.getDateFinValidite() == null) {
      tfPDateFinValidite.setText("");
      return;
    }
    Date date = parametreConditionVente.getDateFinValidite();
    tfPDateFinValidite.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
  }
  
  /**
   * Rafraichit le code devise.
   */
  private void rafraichirCodeDevise() {
    if (parametreConditionVente == null || parametreConditionVente.getCodeDevise() == null) {
      tfPCodeDevise.setText("");
      return;
    }
    String code = parametreConditionVente.getCodeDevise();
    tfPCodeDevise.setText(code);
  }
  
  /**
   * Rafraichit la catégorie.
   */
  private void rafraichirCategorie() {
    if (parametreConditionVente == null || parametreConditionVente.getCategorie() == null) {
      tfPCategorie.setText("");
      return;
    }
    EnumCategorieConditionVente categorie = parametreConditionVente.getCategorie();
    tfPCategorie.setText(categorie.getLibelle());
  }
  
  /**
   * Rafraichit le type.
   */
  private void rafraichirType() {
    if (parametreConditionVente == null || parametreConditionVente.getType() == null) {
      tfPType.setText("");
      return;
    }
    EnumTypeConditionVente type = parametreConditionVente.getType();
    tfPType.setText(type.getLibelle());
  }
  
  /**
   * Rafraichit le type gratuit.
   */
  private void rafraichirTypeGratuit() {
    if (parametreConditionVente == null) {
      tfPTypeGratuit.setText("");
      return;
    }
    if (parametreConditionVente.isTypeGratuit()) {
      tfPTypeGratuit.setText("Oui");
    }
    else {
      tfPTypeGratuit.setText("Non");
    }
  }
  
  /**
   * Rafraichit la quantité minimale.
   */
  private void rafraichirQuantiteMinimale() {
    if (parametreConditionVente == null || parametreConditionVente.getQuantiteMinimale() == null) {
      tfPQuantiteMinimale.setText("");
      return;
    }
    BigDecimal quantite = parametreConditionVente.getQuantiteMinimale();
    tfPQuantiteMinimale.setText(quantite.toString());
  }
  
  /**
   * Rafraichit le prix de base.
   */
  private void rafraichirPrixBase() {
    if (parametreConditionVente == null || parametreConditionVente.getPrixBaseHT() == null) {
      tfPPrixBaseHT.setMontant("");
      return;
    }
    BigDecimal valeur = parametreConditionVente.getPrixBaseHT();
    tfPPrixBaseHT.setMontant(valeur);
  }
  
  /**
   * Rafraichit le prix net.
   */
  private void rafraichirPrixNet() {
    if (parametreConditionVente == null || parametreConditionVente.getPrixNetHT() == null) {
      tfPPrixNetHT.setMontant("");
      return;
    }
    BigDecimal valeur = parametreConditionVente.getPrixNetHT();
    tfPPrixNetHT.setMontant(valeur);
  }
  
  /**
   * Rafraichit la remise en valeur.
   */
  private void rafraichirRemiseEnValeur() {
    if (parametreConditionVente == null || parametreConditionVente.getRemiseEnValeur() == null) {
      tfPRemiseEnValeur.setMontant("");
      return;
    }
    BigDecimal valeur = parametreConditionVente.getRemiseEnValeur();
    tfPRemiseEnValeur.setMontant(valeur);
  }
  
  /**
   * Rafraichit l'ajout en valeur.
   */
  private void rafraichirAjoutEnValeur() {
    if (parametreConditionVente == null || parametreConditionVente.getAjoutEnValeur() == null) {
      tfPAjoutEnValeur.setMontant("");
      return;
    }
    BigDecimal valeur = parametreConditionVente.getAjoutEnValeur();
    tfPAjoutEnValeur.setMontant(valeur);
  }
  
  /**
   * Rafraichit les remises en pourcentage.
   */
  private void rafraichirRemiseEnPourcentage() {
    if (parametreConditionVente == null || parametreConditionVente.getListeRemiseEnPourcentage() == null
        || parametreConditionVente.getListeRemiseEnPourcentage().isEmpty()) {
      tfPPourcentageRemise1.setText("");
      tfPPourcentageRemise2.setText("");
      tfPPourcentageRemise3.setText("");
      tfPPourcentageRemise4.setText("");
      tfPPourcentageRemise5.setText("");
      tfPPourcentageRemise6.setText("");
      return;
    }
    
    List<BigDecimal> listeValeur = parametreConditionVente.getListeRemiseEnPourcentage();
    tfPPourcentageRemise1.setText(listeValeur.get(0).toString());
    if (listeValeur.size() > 1) {
      tfPPourcentageRemise2.setText(listeValeur.get(1).toString());
    }
    if (listeValeur.size() > 2) {
      tfPPourcentageRemise3.setText(listeValeur.get(2).toString());
    }
    if (listeValeur.size() > 3) {
      tfPPourcentageRemise4.setText(listeValeur.get(3).toString());
    }
    if (listeValeur.size() > 4) {
      tfPPourcentageRemise5.setText(listeValeur.get(4).toString());
    }
    if (listeValeur.size() > 5) {
      tfPPourcentageRemise6.setText(listeValeur.get(5).toString());
    }
  }
  
  /**
   * Rafraichit le mode d'application des remises.
   */
  private void rafraichirModeApplicationRemise() {
    if (parametreConditionVente == null) {
      tfPModeApplicationRemise.setText("");
      return;
    }
    if (parametreConditionVente.isModeRemiseEnCascade()) {
      tfPModeApplicationRemise.setText("En cascade");
    }
    else {
      tfPModeApplicationRemise.setText("En ajout");
    }
  }
  
  /**
   * Rafraichit le coefficient multiplicateur.
   */
  private void rafraichirCoefficientMultiplicateur() {
    if (parametreConditionVente == null || parametreConditionVente.getCoefficientMultiplicateur() == null) {
      tfPCoefficientMultiplicateur.setText("");
      return;
    }
    BigDecimal valeur = parametreConditionVente.getCoefficientMultiplicateur();
    tfPCoefficientMultiplicateur.setText(valeur.toString());
  }
  
  /**
   * Rafraichit le code de la formule prix du paramètre FP.
   */
  private void rafraichirCodeFormulePrix() {
    if (parametreConditionVente == null || parametreConditionVente.getFormulePrixFP() == null) {
      tfPCodeFormulePrixFP.setText("");
      return;
    }
    FormulePrixFP formulePrix = parametreConditionVente.getFormulePrixFP();
    tfPCodeFormulePrixFP.setText(formulePrix.getCodeFormulePrix());
  }
  
  /**
   * Rafraichit la colonne tarif.
   */
  private void rafraichirColonneTarif() {
    if (parametreConditionVente == null || parametreConditionVente.getColonneTarif() == null) {
      tfPColonneTarif.setText("");
      return;
    }
    ColonneTarif colonneTarif = parametreConditionVente.getColonneTarif();
    tfPColonneTarif.setText(colonneTarif.getNumero().toString());
  }
  
  // -- Méthodes événementielles
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbPOrigine = new SNLabelChamp();
    tfPOrigine = new SNTexte();
    lbPCode = new SNLabelChamp();
    tfPCode = new SNTexte();
    lbPDateDebutValidite = new SNLabelChamp();
    tfPDateDebutValidite = new SNTexte();
    lbPDateFinValidite = new SNLabelChamp();
    tfPDateFinValidite = new SNTexte();
    lbPCodeDevise = new SNLabelChamp();
    tfPCodeDevise = new SNTexte();
    lbPCategorie = new SNLabelChamp();
    tfPCategorie = new SNTexte();
    lbPType = new SNLabelChamp();
    tfPType = new SNTexte();
    lbPTypeGratuit = new SNLabelChamp();
    tfPTypeGratuit = new SNTexte();
    lbPQuantiteMinimale = new SNLabelChamp();
    tfPQuantiteMinimale = new SNTexte();
    lbPPrixBaseHT = new SNLabelChamp();
    tfPPrixBaseHT = new SNMontant();
    lbPPrixNetHT = new SNLabelChamp();
    tfPPrixNetHT = new SNMontant();
    lbPRemiseEnValeur = new SNLabelChamp();
    tfPRemiseEnValeur = new SNMontant();
    lbPAjoutEnValeur = new SNLabelChamp();
    tfPAjoutEnValeur = new SNMontant();
    lbPCoefficientMultiplicateur2 = new SNLabelChamp();
    pnlPPourcentageRemise = new SNPanel();
    tfPPourcentageRemise1 = new SNTexte();
    tfPPourcentageRemise2 = new SNTexte();
    tfPPourcentageRemise3 = new SNTexte();
    tfPPourcentageRemise4 = new SNTexte();
    tfPPourcentageRemise5 = new SNTexte();
    tfPPourcentageRemise6 = new SNTexte();
    lbPModeApplicationRemise = new SNLabelChamp();
    tfPModeApplicationRemise = new SNTexte();
    lbPCoefficientMultiplicateur = new SNLabelChamp();
    tfPCoefficientMultiplicateur = new SNTexte();
    lbPCodeFormulePrixFP = new SNLabelChamp();
    tfPCodeFormulePrixFP = new SNTexte();
    lbPColonneTarif = new SNLabelChamp();
    tfPColonneTarif = new SNTexte();
    
    // ======== this ========
    setMinimumSize(new Dimension(400, 405));
    setPreferredSize(new Dimension(400, 405));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights =
        new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    
    // ---- lbPOrigine ----
    lbPOrigine.setModeReduit(true);
    lbPOrigine.setText("Origine");
    lbPOrigine.setMinimumSize(new Dimension(130, 22));
    lbPOrigine.setPreferredSize(new Dimension(130, 22));
    lbPOrigine.setName("lbPOrigine");
    add(lbPOrigine,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPOrigine ----
    tfPOrigine.setModeReduit(true);
    tfPOrigine.setEnabled(false);
    tfPOrigine.setName("tfPOrigine");
    add(tfPOrigine,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCode ----
    lbPCode.setModeReduit(true);
    lbPCode.setText("Code");
    lbPCode.setMinimumSize(new Dimension(30, 22));
    lbPCode.setPreferredSize(new Dimension(30, 22));
    lbPCode.setName("lbPCode");
    add(lbPCode,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPCode ----
    tfPCode.setModeReduit(true);
    tfPCode.setEnabled(false);
    tfPCode.setName("tfPCode");
    add(tfPCode,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPDateDebutValidite ----
    lbPDateDebutValidite.setModeReduit(true);
    lbPDateDebutValidite.setText("Date d\u00e9but validit\u00e9");
    lbPDateDebutValidite.setMinimumSize(new Dimension(30, 22));
    lbPDateDebutValidite.setPreferredSize(new Dimension(30, 22));
    lbPDateDebutValidite.setName("lbPDateDebutValidite");
    add(lbPDateDebutValidite,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPDateDebutValidite ----
    tfPDateDebutValidite.setModeReduit(true);
    tfPDateDebutValidite.setEnabled(false);
    tfPDateDebutValidite.setName("tfPDateDebutValidite");
    add(tfPDateDebutValidite,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPDateFinValidite ----
    lbPDateFinValidite.setModeReduit(true);
    lbPDateFinValidite.setText("Date fin validit\u00e9");
    lbPDateFinValidite.setMinimumSize(new Dimension(30, 22));
    lbPDateFinValidite.setPreferredSize(new Dimension(30, 22));
    lbPDateFinValidite.setName("lbPDateFinValidite");
    add(lbPDateFinValidite,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPDateFinValidite ----
    tfPDateFinValidite.setModeReduit(true);
    tfPDateFinValidite.setEnabled(false);
    tfPDateFinValidite.setName("tfPDateFinValidite");
    add(tfPDateFinValidite,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCodeDevise ----
    lbPCodeDevise.setModeReduit(true);
    lbPCodeDevise.setText("Code devise");
    lbPCodeDevise.setMinimumSize(new Dimension(30, 22));
    lbPCodeDevise.setPreferredSize(new Dimension(30, 22));
    lbPCodeDevise.setName("lbPCodeDevise");
    add(lbPCodeDevise,
        new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPCodeDevise ----
    tfPCodeDevise.setModeReduit(true);
    tfPCodeDevise.setEnabled(false);
    tfPCodeDevise.setName("tfPCodeDevise");
    add(tfPCodeDevise,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCategorie ----
    lbPCategorie.setModeReduit(true);
    lbPCategorie.setText("Cat\u00e9gorie");
    lbPCategorie.setMinimumSize(new Dimension(30, 22));
    lbPCategorie.setPreferredSize(new Dimension(30, 22));
    lbPCategorie.setName("lbPCategorie");
    add(lbPCategorie,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPCategorie ----
    tfPCategorie.setModeReduit(true);
    tfPCategorie.setEnabled(false);
    tfPCategorie.setName("tfPCategorie");
    add(tfPCategorie,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPType ----
    lbPType.setModeReduit(true);
    lbPType.setText("Type");
    lbPType.setMinimumSize(new Dimension(30, 22));
    lbPType.setPreferredSize(new Dimension(30, 22));
    lbPType.setName("lbPType");
    add(lbPType,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPType ----
    tfPType.setModeReduit(true);
    tfPType.setEnabled(false);
    tfPType.setName("tfPType");
    add(tfPType,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPTypeGratuit ----
    lbPTypeGratuit.setModeReduit(true);
    lbPTypeGratuit.setText("Type gratuit");
    lbPTypeGratuit.setMinimumSize(new Dimension(30, 22));
    lbPTypeGratuit.setPreferredSize(new Dimension(30, 22));
    lbPTypeGratuit.setName("lbPTypeGratuit");
    add(lbPTypeGratuit,
        new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPTypeGratuit ----
    tfPTypeGratuit.setModeReduit(true);
    tfPTypeGratuit.setEnabled(false);
    tfPTypeGratuit.setName("tfPTypeGratuit");
    add(tfPTypeGratuit,
        new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPQuantiteMinimale ----
    lbPQuantiteMinimale.setModeReduit(true);
    lbPQuantiteMinimale.setText("Quantite minimale");
    lbPQuantiteMinimale.setMinimumSize(new Dimension(30, 22));
    lbPQuantiteMinimale.setPreferredSize(new Dimension(30, 22));
    lbPQuantiteMinimale.setName("lbPQuantiteMinimale");
    add(lbPQuantiteMinimale,
        new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPQuantiteMinimale ----
    tfPQuantiteMinimale.setModeReduit(true);
    tfPQuantiteMinimale.setEnabled(false);
    tfPQuantiteMinimale.setName("tfPQuantiteMinimale");
    add(tfPQuantiteMinimale,
        new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPPrixBaseHT ----
    lbPPrixBaseHT.setModeReduit(true);
    lbPPrixBaseHT.setText("Prix Base HT");
    lbPPrixBaseHT.setMinimumSize(new Dimension(30, 22));
    lbPPrixBaseHT.setPreferredSize(new Dimension(30, 22));
    lbPPrixBaseHT.setName("lbPPrixBaseHT");
    add(lbPPrixBaseHT,
        new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPPrixBaseHT ----
    tfPPrixBaseHT.setModeReduit(true);
    tfPPrixBaseHT.setEnabled(false);
    tfPPrixBaseHT.setName("tfPPrixBaseHT");
    add(tfPPrixBaseHT,
        new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPPrixNetHT ----
    lbPPrixNetHT.setModeReduit(true);
    lbPPrixNetHT.setText("Prix Net HT");
    lbPPrixNetHT.setMinimumSize(new Dimension(30, 22));
    lbPPrixNetHT.setPreferredSize(new Dimension(30, 22));
    lbPPrixNetHT.setName("lbPPrixNetHT");
    add(lbPPrixNetHT,
        new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPPrixNetHT ----
    tfPPrixNetHT.setModeReduit(true);
    tfPPrixNetHT.setEnabled(false);
    tfPPrixNetHT.setName("tfPPrixNetHT");
    add(tfPPrixNetHT, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPRemiseEnValeur ----
    lbPRemiseEnValeur.setModeReduit(true);
    lbPRemiseEnValeur.setText("Remise en valeur");
    lbPRemiseEnValeur.setMinimumSize(new Dimension(30, 22));
    lbPRemiseEnValeur.setPreferredSize(new Dimension(30, 22));
    lbPRemiseEnValeur.setName("lbPRemiseEnValeur");
    add(lbPRemiseEnValeur,
        new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPRemiseEnValeur ----
    tfPRemiseEnValeur.setModeReduit(true);
    tfPRemiseEnValeur.setEnabled(false);
    tfPRemiseEnValeur.setName("tfPRemiseEnValeur");
    add(tfPRemiseEnValeur, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPAjoutEnValeur ----
    lbPAjoutEnValeur.setModeReduit(true);
    lbPAjoutEnValeur.setText("Ajout en valeur");
    lbPAjoutEnValeur.setMinimumSize(new Dimension(30, 22));
    lbPAjoutEnValeur.setPreferredSize(new Dimension(30, 22));
    lbPAjoutEnValeur.setName("lbPAjoutEnValeur");
    add(lbPAjoutEnValeur,
        new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPAjoutEnValeur ----
    tfPAjoutEnValeur.setModeReduit(true);
    tfPAjoutEnValeur.setEnabled(false);
    tfPAjoutEnValeur.setName("tfPAjoutEnValeur");
    add(tfPAjoutEnValeur, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCoefficientMultiplicateur2 ----
    lbPCoefficientMultiplicateur2.setModeReduit(true);
    lbPCoefficientMultiplicateur2.setText("Remises en %");
    lbPCoefficientMultiplicateur2.setMinimumSize(new Dimension(30, 22));
    lbPCoefficientMultiplicateur2.setPreferredSize(new Dimension(30, 22));
    lbPCoefficientMultiplicateur2.setName("lbPCoefficientMultiplicateur2");
    add(lbPCoefficientMultiplicateur2,
        new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ======== pnlPPourcentageRemise ========
    {
      pnlPPourcentageRemise.setMinimumSize(new Dimension(270, 22));
      pnlPPourcentageRemise.setPreferredSize(new Dimension(270, 22));
      pnlPPourcentageRemise.setName("pnlPPourcentageRemise");
      pnlPPourcentageRemise.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPPourcentageRemise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlPPourcentageRemise.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlPPourcentageRemise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlPPourcentageRemise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      
      // ---- tfPPourcentageRemise1 ----
      tfPPourcentageRemise1.setEnabled(false);
      tfPPourcentageRemise1.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise1.setModeReduit(true);
      tfPPourcentageRemise1.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise1.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise1.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise1.setName("tfPPourcentageRemise1");
      pnlPPourcentageRemise.add(tfPPourcentageRemise1,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise2 ----
      tfPPourcentageRemise2.setEnabled(false);
      tfPPourcentageRemise2.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise2.setModeReduit(true);
      tfPPourcentageRemise2.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise2.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise2.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise2.setName("tfPPourcentageRemise2");
      pnlPPourcentageRemise.add(tfPPourcentageRemise2,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise3 ----
      tfPPourcentageRemise3.setEnabled(false);
      tfPPourcentageRemise3.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise3.setModeReduit(true);
      tfPPourcentageRemise3.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise3.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise3.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise3.setName("tfPPourcentageRemise3");
      pnlPPourcentageRemise.add(tfPPourcentageRemise3,
          new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise4 ----
      tfPPourcentageRemise4.setEnabled(false);
      tfPPourcentageRemise4.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise4.setModeReduit(true);
      tfPPourcentageRemise4.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise4.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise4.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise4.setName("tfPPourcentageRemise4");
      pnlPPourcentageRemise.add(tfPPourcentageRemise4,
          new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise5 ----
      tfPPourcentageRemise5.setEnabled(false);
      tfPPourcentageRemise5.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise5.setModeReduit(true);
      tfPPourcentageRemise5.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise5.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise5.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise5.setName("tfPPourcentageRemise5");
      pnlPPourcentageRemise.add(tfPPourcentageRemise5,
          new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise6 ----
      tfPPourcentageRemise6.setEnabled(false);
      tfPPourcentageRemise6.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise6.setModeReduit(true);
      tfPPourcentageRemise6.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise6.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise6.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise6.setName("tfPPourcentageRemise6");
      pnlPPourcentageRemise.add(tfPPourcentageRemise6,
          new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPPourcentageRemise,
        new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPModeApplicationRemise ----
    lbPModeApplicationRemise.setModeReduit(true);
    lbPModeApplicationRemise.setText("Mode application remises");
    lbPModeApplicationRemise.setMinimumSize(new Dimension(30, 22));
    lbPModeApplicationRemise.setPreferredSize(new Dimension(30, 22));
    lbPModeApplicationRemise.setName("lbPModeApplicationRemise");
    add(lbPModeApplicationRemise,
        new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPModeApplicationRemise ----
    tfPModeApplicationRemise.setModeReduit(true);
    tfPModeApplicationRemise.setEnabled(false);
    tfPModeApplicationRemise.setName("tfPModeApplicationRemise");
    add(tfPModeApplicationRemise, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCoefficientMultiplicateur ----
    lbPCoefficientMultiplicateur.setModeReduit(true);
    lbPCoefficientMultiplicateur.setText("Coefficient Multiplicateur");
    lbPCoefficientMultiplicateur.setMinimumSize(new Dimension(30, 22));
    lbPCoefficientMultiplicateur.setPreferredSize(new Dimension(30, 22));
    lbPCoefficientMultiplicateur.setName("lbPCoefficientMultiplicateur");
    add(lbPCoefficientMultiplicateur,
        new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPCoefficientMultiplicateur ----
    tfPCoefficientMultiplicateur.setModeReduit(true);
    tfPCoefficientMultiplicateur.setEnabled(false);
    tfPCoefficientMultiplicateur.setName("tfPCoefficientMultiplicateur");
    add(tfPCoefficientMultiplicateur, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCodeFormulePrixFP ----
    lbPCodeFormulePrixFP.setModeReduit(true);
    lbPCodeFormulePrixFP.setText("Code formule");
    lbPCodeFormulePrixFP.setMinimumSize(new Dimension(30, 22));
    lbPCodeFormulePrixFP.setPreferredSize(new Dimension(30, 22));
    lbPCodeFormulePrixFP.setName("lbPCodeFormulePrixFP");
    add(lbPCodeFormulePrixFP,
        new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPCodeFormulePrixFP ----
    tfPCodeFormulePrixFP.setModeReduit(true);
    tfPCodeFormulePrixFP.setEnabled(false);
    tfPCodeFormulePrixFP.setName("tfPCodeFormulePrixFP");
    add(tfPCodeFormulePrixFP, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPColonneTarif ----
    lbPColonneTarif.setModeReduit(true);
    lbPColonneTarif.setText("Colonne Tarif");
    lbPColonneTarif.setMinimumSize(new Dimension(30, 22));
    lbPColonneTarif.setPreferredSize(new Dimension(30, 22));
    lbPColonneTarif.setName("lbPColonneTarif");
    add(lbPColonneTarif,
        new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPColonneTarif ----
    tfPColonneTarif.setModeReduit(true);
    tfPColonneTarif.setEnabled(false);
    tfPColonneTarif.setName("tfPColonneTarif");
    add(tfPColonneTarif, new GridBagConstraints(1, 17, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbPOrigine;
  private SNTexte tfPOrigine;
  private SNLabelChamp lbPCode;
  private SNTexte tfPCode;
  private SNLabelChamp lbPDateDebutValidite;
  private SNTexte tfPDateDebutValidite;
  private SNLabelChamp lbPDateFinValidite;
  private SNTexte tfPDateFinValidite;
  private SNLabelChamp lbPCodeDevise;
  private SNTexte tfPCodeDevise;
  private SNLabelChamp lbPCategorie;
  private SNTexte tfPCategorie;
  private SNLabelChamp lbPType;
  private SNTexte tfPType;
  private SNLabelChamp lbPTypeGratuit;
  private SNTexte tfPTypeGratuit;
  private SNLabelChamp lbPQuantiteMinimale;
  private SNTexte tfPQuantiteMinimale;
  private SNLabelChamp lbPPrixBaseHT;
  private SNMontant tfPPrixBaseHT;
  private SNLabelChamp lbPPrixNetHT;
  private SNMontant tfPPrixNetHT;
  private SNLabelChamp lbPRemiseEnValeur;
  private SNMontant tfPRemiseEnValeur;
  private SNLabelChamp lbPAjoutEnValeur;
  private SNMontant tfPAjoutEnValeur;
  private SNLabelChamp lbPCoefficientMultiplicateur2;
  private SNPanel pnlPPourcentageRemise;
  private SNTexte tfPPourcentageRemise1;
  private SNTexte tfPPourcentageRemise2;
  private SNTexte tfPPourcentageRemise3;
  private SNTexte tfPPourcentageRemise4;
  private SNTexte tfPPourcentageRemise5;
  private SNTexte tfPPourcentageRemise6;
  private SNLabelChamp lbPModeApplicationRemise;
  private SNTexte tfPModeApplicationRemise;
  private SNLabelChamp lbPCoefficientMultiplicateur;
  private SNTexte tfPCoefficientMultiplicateur;
  private SNLabelChamp lbPCodeFormulePrixFP;
  private SNTexte tfPCodeFormulePrixFP;
  private SNLabelChamp lbPColonneTarif;
  private SNTexte tfPColonneTarif;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
