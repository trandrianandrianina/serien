/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.condition;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreArticle;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreConditionVente;

/**
 * Cette regroupe l'ensemble des paramètres des conditions de vente et des traitements.
 */
public class ListeConditionVente extends ArrayList<ConditionVente> {
  // Variables
  private ConditionVente meilleureConditionVente = null;
  
  /**
   * Constructeur.
   */
  public ListeConditionVente(boolean pModeNegoce, List<ParametreConditionVente> pListeParametreConditionVente,
      ParametreArticle pParametreArticle, int pNombreDecimale) {
    if (pListeParametreConditionVente == null || pListeParametreConditionVente.isEmpty()) {
      throw new MessageErreurException("La liste des paramètres des conditions de vente est invalide.");
    }
    
    // Création de la liste des conditions de vente à partir de la liste des paramètres
    for (ParametreConditionVente parametreConditionVente : pListeParametreConditionVente) {
      ConditionVente conditionVente = new ConditionVente(pModeNegoce, parametreConditionVente, pParametreArticle, pNombreDecimale);
      add(conditionVente);
    }
  }
  
  // -- Méthodes publiques
  
  /**
   * Calcule le prix de base HT et le prix net HT pour toutes les conditions de ventes listées.
   * 
   * @param pColonneTarifAUtiliser
   * @param pListePrixVenteHT
   * @param pValeurPS305
   * @param pListeRemiseEnPourcentageSaisie peut être null
   * @param pCoefficientMultiplicateurSaisi peut être null
   */
  public void calculerPrixVenteConditionVente(ColonneTarif pColonneTarifAUtiliser, List<BigDecimal> pListePrixVenteHT,
      Boolean pValeurPS305, List<BigDecimal> pListeRemiseEnPourcentageSaisie, BigDecimal pCoefficientMultiplicateurSaisi) {
    meilleureConditionVente = null;
    if (isEmpty()) {
      return;
    }
    
    // Parcours de toutes les conditions de ventes
    for (ConditionVente conditionVente : this) {
      // Calculer le prix de base HT
      conditionVente.calculerPrixBaseHT(pListePrixVenteHT, pValeurPS305);
      // Calculer le prix net HT
      conditionVente.calculerPrixNetHT(pColonneTarifAUtiliser, pListeRemiseEnPourcentageSaisie, pCoefficientMultiplicateurSaisi,
          pListePrixVenteHT, pValeurPS305);
    }
  }
  
  /**
   * Détermine la condition de vente qui a le meilleur prix de vente.
   */
  public void determinerMeilleurPrixVente() {
    meilleureConditionVente = null;
    if (isEmpty()) {
      return;
    }
    
    meilleureConditionVente = get(0);
    BigDecimal meilleurPrixNetHT = meilleureConditionVente.getPrixNetHT();
    if (size() > 1) {
      // Parcours de toutes les conditions de ventes afin de rechercher le meilleur prix net HT
      for (int i = 1; i < size(); i++) {
        BigDecimal prixNetHT = get(i).getPrixNetHT();
        if (meilleurPrixNetHT.compareTo(prixNetHT) > 0) {
          meilleureConditionVente = get(i);
        }
      }
    }
  }
  
  /**
   * Retourne la condition de vente à partir de son code.
   * @return
   */
  public ConditionVente getConditionVenteAvecCode(String pCode) {
    if (Constantes.normerTexte(pCode).isEmpty()) {
      return null;
    }
    
    // Parcours de toutes les conditions de ventes
    for (ConditionVente conditionVente : this) {
      if (Constantes.equals(conditionVente.getCode(), pCode)) {
        return conditionVente;
      }
    }
    return null;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la meilleure condition de vente.
   * @return
   */
  public ConditionVente getMeilleureConditionVente() {
    return meilleureConditionVente;
  }
}
