/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.formulecalculprix;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les types possibles pour les formules de calcul de prix du paramètre 'FP'.
 */
public enum EnumTypeOperateur {
  ADDITION('+', "Addition"),
  SOUSTRACTION('-', "Soustraction");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeOperateur(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumTypeOperateur valueOfByCode(Character pCode) {
    for (EnumTypeOperateur value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code de l'opérateur de la formule de calcul est invalide : " + pCode);
  }
  
}
