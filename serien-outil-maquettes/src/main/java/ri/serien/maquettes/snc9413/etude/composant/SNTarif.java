/*
 * Created by JFormDesigner on Tue Dec 07 16:33:18 CET 2021
 */

package ri.serien.maquettes.snc9413.etude.composant;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;

/**
 * Composant permettant d'afficher toutes les informations d'un tarif.
 */
public class SNTarif extends SNPanel {
  // Variables
  private int maxColonne = TarifArticle.NOMBRE_COLONNES_MODE_CLASSIQUE;
  private List<BigDecimal> listePrixColonne = null;
  private List<SNMontant> listeComposantPrixColonne = null;
  
  /**
   * Constructeur.
   */
  public SNTarif() {
    super();
    initComponents();
    
    // Initialisation de la liste des composants
    listeComposantPrixColonne = new ArrayList<SNMontant>();
    listeComposantPrixColonne.add(lbPrixColonne1);
    listeComposantPrixColonne.add(lbPrixColonne2);
    listeComposantPrixColonne.add(lbPrixColonne3);
    listeComposantPrixColonne.add(lbPrixColonne4);
    listeComposantPrixColonne.add(lbPrixColonne5);
    listeComposantPrixColonne.add(lbPrixColonne6);
    listeComposantPrixColonne.add(lbPrixColonne7);
    listeComposantPrixColonne.add(lbPrixColonne8);
    listeComposantPrixColonne.add(lbPrixColonne9);
    listeComposantPrixColonne.add(lbPrixColonne10);
  }
  
  // -- Méthodes publiques
  
  /**
   * Modifier la liste des prix HT des colonnes du tarif.
   */
  public void modifierListePrixColonne(List<BigDecimal> pListePrixHTColonne) {
    if (pListePrixHTColonne == null || pListePrixHTColonne.isEmpty()) {
      return;
    }
    if (pListePrixHTColonne.size() > maxColonne) {
      throw new MessageErreurException("La liste contient trop d'éléments, le nombre maximum est " + maxColonne + '.');
    }
    if (listeComposantPrixColonne.size() < pListePrixHTColonne.size()) {
      throw new MessageErreurException("Il y a moins de composants que d'éléments fournis dans la liste des prix colonnes.");
    }
    // Si les listes sont identiques alors pas de modification
    if (Constantes.equals(pListePrixHTColonne, listePrixColonne)) {
      return;
    }
    
    listePrixColonne = pListePrixHTColonne;
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichissement des données du composant.
   */
  private void rafraichir() {
    rafraichirPrixColonne();
  }
  
  /**
   * Rafraichit les prix HT des colonnes.
   */
  private void rafraichirPrixColonne() {
    // Effacement de toutes les valeurs
    for (SNMontant composantPrixColonne : listeComposantPrixColonne) {
      composantPrixColonne.setMontant("");
    }
    // Contrôle le contenu de la liste des prix colonnes
    if (listePrixColonne == null || listePrixColonne.isEmpty()) {
      return;
    }
    // Mise à jour des valeurs à partir de la liste des prix
    for (int i = 0; i < listePrixColonne.size(); i++) {
      BigDecimal prix = listePrixColonne.get(i);
      listeComposantPrixColonne.get(i).setMontant(prix);
    }
  }
  
  // -- Méthodes événementielles
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbTitreColonne = new SNLabelTitre();
    lbTitrePrixHT = new SNLabelTitre();
    lbColonne1 = new SNLabelTitre();
    lbPrixColonne1 = new SNMontant();
    lbColonne2 = new SNLabelTitre();
    lbPrixColonne2 = new SNMontant();
    lbColonne3 = new SNLabelTitre();
    lbPrixColonne3 = new SNMontant();
    lbColonne4 = new SNLabelTitre();
    lbPrixColonne4 = new SNMontant();
    lbColonne5 = new SNLabelTitre();
    lbPrixColonne5 = new SNMontant();
    lbColonne6 = new SNLabelTitre();
    lbPrixColonne6 = new SNMontant();
    lbColonne7 = new SNLabelTitre();
    lbPrixColonne7 = new SNMontant();
    lbColonne8 = new SNLabelTitre();
    lbPrixColonne8 = new SNMontant();
    lbColonne9 = new SNLabelTitre();
    lbPrixColonne9 = new SNMontant();
    lbColonne10 = new SNLabelTitre();
    lbPrixColonne10 = new SNMontant();
    
    // ======== this ========
    setMinimumSize(new Dimension(150, 255));
    setPreferredSize(new Dimension(150, 255));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    
    // ---- lbTitreColonne ----
    lbTitreColonne.setModeReduit(true);
    lbTitreColonne.setText("Col.");
    lbTitreColonne.setHorizontalTextPosition(SwingConstants.CENTER);
    lbTitreColonne.setMinimumSize(new Dimension(30, 22));
    lbTitreColonne.setPreferredSize(new Dimension(30, 22));
    lbTitreColonne.setHorizontalAlignment(SwingConstants.CENTER);
    lbTitreColonne.setName("lbTitreColonne");
    add(lbTitreColonne,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbTitrePrixHT ----
    lbTitrePrixHT.setModeReduit(true);
    lbTitrePrixHT.setText("Prix HT");
    lbTitrePrixHT.setHorizontalTextPosition(SwingConstants.CENTER);
    lbTitrePrixHT.setMinimumSize(new Dimension(80, 22));
    lbTitrePrixHT.setPreferredSize(new Dimension(80, 22));
    lbTitrePrixHT.setHorizontalAlignment(SwingConstants.CENTER);
    lbTitrePrixHT.setName("lbTitrePrixHT");
    add(lbTitrePrixHT,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne1 ----
    lbColonne1.setModeReduit(true);
    lbColonne1.setText("1");
    lbColonne1.setMinimumSize(new Dimension(30, 22));
    lbColonne1.setPreferredSize(new Dimension(30, 22));
    lbColonne1.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne1.setName("lbColonne1");
    add(lbColonne1,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbPrixColonne1 ----
    lbPrixColonne1.setModeReduit(true);
    lbPrixColonne1.setEnabled(false);
    lbPrixColonne1.setName("lbPrixColonne1");
    add(lbPrixColonne1,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne2 ----
    lbColonne2.setModeReduit(true);
    lbColonne2.setText("2");
    lbColonne2.setMinimumSize(new Dimension(30, 22));
    lbColonne2.setPreferredSize(new Dimension(30, 22));
    lbColonne2.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne2.setName("lbColonne2");
    add(lbColonne2,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbPrixColonne2 ----
    lbPrixColonne2.setModeReduit(true);
    lbPrixColonne2.setEnabled(false);
    lbPrixColonne2.setName("lbPrixColonne2");
    add(lbPrixColonne2,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne3 ----
    lbColonne3.setModeReduit(true);
    lbColonne3.setText("3");
    lbColonne3.setMinimumSize(new Dimension(30, 22));
    lbColonne3.setPreferredSize(new Dimension(30, 22));
    lbColonne3.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne3.setName("lbColonne3");
    add(lbColonne3,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbPrixColonne3 ----
    lbPrixColonne3.setModeReduit(true);
    lbPrixColonne3.setEnabled(false);
    lbPrixColonne3.setName("lbPrixColonne3");
    add(lbPrixColonne3,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne4 ----
    lbColonne4.setModeReduit(true);
    lbColonne4.setText("4");
    lbColonne4.setMinimumSize(new Dimension(30, 22));
    lbColonne4.setPreferredSize(new Dimension(30, 22));
    lbColonne4.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne4.setName("lbColonne4");
    add(lbColonne4,
        new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbPrixColonne4 ----
    lbPrixColonne4.setModeReduit(true);
    lbPrixColonne4.setEnabled(false);
    lbPrixColonne4.setName("lbPrixColonne4");
    add(lbPrixColonne4,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne5 ----
    lbColonne5.setModeReduit(true);
    lbColonne5.setText("5");
    lbColonne5.setMinimumSize(new Dimension(30, 22));
    lbColonne5.setPreferredSize(new Dimension(30, 22));
    lbColonne5.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne5.setName("lbColonne5");
    add(lbColonne5,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbPrixColonne5 ----
    lbPrixColonne5.setModeReduit(true);
    lbPrixColonne5.setEnabled(false);
    lbPrixColonne5.setName("lbPrixColonne5");
    add(lbPrixColonne5,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne6 ----
    lbColonne6.setModeReduit(true);
    lbColonne6.setText("6");
    lbColonne6.setMinimumSize(new Dimension(30, 22));
    lbColonne6.setPreferredSize(new Dimension(30, 22));
    lbColonne6.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne6.setName("lbColonne6");
    add(lbColonne6,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbPrixColonne6 ----
    lbPrixColonne6.setModeReduit(true);
    lbPrixColonne6.setEnabled(false);
    lbPrixColonne6.setName("lbPrixColonne6");
    add(lbPrixColonne6,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne7 ----
    lbColonne7.setModeReduit(true);
    lbColonne7.setText("7");
    lbColonne7.setMinimumSize(new Dimension(30, 22));
    lbColonne7.setPreferredSize(new Dimension(30, 22));
    lbColonne7.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne7.setName("lbColonne7");
    add(lbColonne7,
        new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbPrixColonne7 ----
    lbPrixColonne7.setModeReduit(true);
    lbPrixColonne7.setEnabled(false);
    lbPrixColonne7.setName("lbPrixColonne7");
    add(lbPrixColonne7,
        new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne8 ----
    lbColonne8.setModeReduit(true);
    lbColonne8.setText("8");
    lbColonne8.setMinimumSize(new Dimension(30, 22));
    lbColonne8.setPreferredSize(new Dimension(30, 22));
    lbColonne8.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne8.setName("lbColonne8");
    add(lbColonne8,
        new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbPrixColonne8 ----
    lbPrixColonne8.setModeReduit(true);
    lbPrixColonne8.setEnabled(false);
    lbPrixColonne8.setName("lbPrixColonne8");
    add(lbPrixColonne8,
        new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne9 ----
    lbColonne9.setModeReduit(true);
    lbColonne9.setText("9");
    lbColonne9.setMinimumSize(new Dimension(30, 22));
    lbColonne9.setPreferredSize(new Dimension(30, 22));
    lbColonne9.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne9.setName("lbColonne9");
    add(lbColonne9,
        new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 2), 0, 0));
    
    // ---- lbPrixColonne9 ----
    lbPrixColonne9.setModeReduit(true);
    lbPrixColonne9.setEnabled(false);
    lbPrixColonne9.setName("lbPrixColonne9");
    add(lbPrixColonne9,
        new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 1, 0), 0, 0));
    
    // ---- lbColonne10 ----
    lbColonne10.setModeReduit(true);
    lbColonne10.setText("10");
    lbColonne10.setMinimumSize(new Dimension(30, 22));
    lbColonne10.setPreferredSize(new Dimension(30, 22));
    lbColonne10.setHorizontalAlignment(SwingConstants.CENTER);
    lbColonne10.setName("lbColonne10");
    add(lbColonne10,
        new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 2), 0, 0));
    
    // ---- lbPrixColonne10 ----
    lbPrixColonne10.setModeReduit(true);
    lbPrixColonne10.setEnabled(false);
    lbPrixColonne10.setName("lbPrixColonne10");
    add(lbPrixColonne10,
        new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelTitre lbTitreColonne;
  private SNLabelTitre lbTitrePrixHT;
  private SNLabelTitre lbColonne1;
  private SNMontant lbPrixColonne1;
  private SNLabelTitre lbColonne2;
  private SNMontant lbPrixColonne2;
  private SNLabelTitre lbColonne3;
  private SNMontant lbPrixColonne3;
  private SNLabelTitre lbColonne4;
  private SNMontant lbPrixColonne4;
  private SNLabelTitre lbColonne5;
  private SNMontant lbPrixColonne5;
  private SNLabelTitre lbColonne6;
  private SNMontant lbPrixColonne6;
  private SNLabelTitre lbColonne7;
  private SNMontant lbPrixColonne7;
  private SNLabelTitre lbColonne8;
  private SNMontant lbPrixColonne8;
  private SNLabelTitre lbColonne9;
  private SNMontant lbPrixColonne9;
  private SNLabelTitre lbColonne10;
  private SNMontant lbPrixColonne10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
