/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.ihm;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;
import ri.serien.maquettes.snc9413.etude.composant.conditionvente.SNPConditionVente;
import ri.serien.maquettes.snc9413.etude.composant.documentvente.SNPDocumentVente;
import ri.serien.maquettes.snc9413.etude.composant.tarif.SNPTarif;
import ri.serien.maquettes.snc9413.etude.metier.CalculPrixVente;
import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;
import ri.serien.maquettes.snc9413.etude.metier.EnumOrigine;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreConditionVente;

/**
 * Vue de la boîte de dialogue "Calcul Prix de vente".
 */
public class DialogueDetailCalculPrixVente extends AbstractVueDialogue<ModeleDialogueCalculPrixVente> {
  // Constantes
  private static final String TITRE = "Détail du calcul du prix de vente";
  
  /**
   * Contructeur.
   */
  public DialogueDetailCalculPrixVente(ModeleDialogueCalculPrixVente pModele) {
    super(pModele);
  }
  
  /**
   * Initialiser les composants graphiques de la boîte de dialogue.
   */
  @Override
  public void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichissement des données à l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirTitre();
    
    // -- Paramètres
    
    // Paramètres généraux
    rafraichirPBaseDeDonnees();
    rafraichirPModeNegoce();
    rafraichirPCodeEtablissement();
    rafraichirPCodeDeviseDG();
    rafraichirPColonneTarifPS105();
    rafraichirPValeurPS305();
    rafraichirPCodeCNVStandardDG();
    rafraichirPCodeCNVPromoDG();
    rafraichirPCodeCNVQuantitativeDG();
    rafraichirPIgnoreCNVQuantitativeDG();
    rafraichirPEmboitageCNVDG();
    rafraichirPRechercherCNVFournisseurDG();
    rafraichirPDateApplicationCNVDG();
    rafraichirPMoisEnCoursDG();
    rafraichirPCalculPrixNetCol1DG();
    
    // Paramètres article
    rafraichirPCodeArticle();
    rafraichirPPumpHT();
    rafraichirPPrixRevientHT();
    rafraichirPPrixRevientStandardHT();
    rafraichirPIdentifiantFournisseur();
    rafraichirPRattachementCNV();
    rafraichirPGroupeFamille();
    rafraichirPSousFamille();
    
    // Paramètres client
    rafraichirPCodeClient();
    rafraichirPCodeDeviseClient();
    rafraichirPClientTTC();
    rafraichirPColonneTarifClient();
    rafraichirPRemiseClient();
    rafraichirPCodeCNVClient();
    rafraichirPCodeCNVPromoClient();
    rafraichirPCentraleAchatClient();
    rafraichirPIgnorerCNVDGClient();
    rafraichirPIgnorerCNVQuantitativeClient();
    
    // Paramètres tarif
    rafraichirPDateApplication();
    rafraichirPOrigineDateApplication();
    rafraichirPListeTarifArticle();
    rafraichirPCodeDeviseTarif();
    
    // Paramètres condition de ventes
    rafraichirPCodeConditionVente();
    
    // -- Calcul
    
    rafraichirDevise();
    
    // Les colonnes tarifs
    rafraichirColonneTarifSaisie();
    rafraichirColonneTarifDocumentVente();
    rafraichirColonneTarifConditionVente();
    rafraichirColonneTarif1Client();
    rafraichirColonneTarifPS105();
    rafraichirColonneTarif();
    
    // Prix de base HT
    rafraichirPrixBaseHTSaisi();
    rafraichirPrixBaseHTCNV();
    rafraichirPrixBaseHTPump();
    rafraichirPrixBaseHTPrixRevient();
    rafraichirPrixBaseHTColonneTarif();
    rafraichirPrixBaseHT();
    
    // Remise
    rafraichirPourcentageRemiseSaisie();
    rafraichirPourcentageRemiseClient();
    rafraichirPourcentageRemiseCNV();
    rafraichirPourcentageRemise();
    
    // Prix net HT
    rafraichirPrixNetHTSaisi();
    rafraichirPrixNetHTBase();
    rafraichirPrixNetNTCalcule();
    rafraichirPrixNetHTCNV();
    rafraichirPrixNetHT();
    
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit le titre de l'application.
   */
  private void rafraichirTitre() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      setTitle(TITRE);
      return;
    }
    
    String titre = TITRE;
    setTitle(titre);
  }
  
  // -- Paramètres généraux
  
  /**
   * Rafraichit le nom de la base de données (paramètres généraux).
   */
  private void rafraichirPBaseDeDonnees() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPBaseDeDonnees.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreGeneral().getNomBaseDeDonnees();
    if (valeur == null) {
      tfPBaseDeDonnees.setText("");
    }
    else {
      tfPBaseDeDonnees.setText(valeur);
    }
  }
  
  /**
   * Rafraichit le mode négoce (paramètres généraux).
   */
  private void rafraichirPModeNegoce() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPModeNegoce.setText("");
      return;
    }
    
    boolean valeur = prixVente.getParametrePrixVente().getParametreGeneral().isModeNegoce();
    if (valeur) {
      tfPModeNegoce.setText("Actif");
    }
    else {
      tfPModeNegoce.setText("Non actif");
    }
  }
  
  /**
   * Rafraichit le code établissement (paramètres généraux).
   */
  private void rafraichirPCodeEtablissement() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPCodeEtablissement.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreGeneral().getCodeEtablissement();
    if (valeur == null) {
      tfPCodeEtablissement.setText("");
    }
    else {
      tfPCodeEtablissement.setText(valeur);
    }
  }
  
  /**
   * Rafraichit le code devise de la DG (paramètres généraux).
   */
  private void rafraichirPCodeDeviseDG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPCodeDeviseDG.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreGeneral().getCodeDevise();
    if (valeur == null) {
      tfPCodeDeviseDG.setText("");
    }
    else {
      tfPCodeDeviseDG.setText(valeur);
    }
  }
  
  /**
   * Rafraichit la colonne de la PS105 (paramètres généraux).
   */
  private void rafraichirPColonneTarifPS105() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPColonnePS105.setText("");
      return;
    }
    
    ColonneTarif valeur = prixVente.getParametrePrixVente().getParametreGeneral().getColonnePS105();
    if (valeur == null) {
      tfPColonnePS105.setText("");
    }
    else {
      tfPColonnePS105.setText(valeur.getNumero().toString());
    }
  }
  
  /**
   * Rafraichit la valeur de la PS305 (paramètres généraux).
   */
  private void rafraichirPValeurPS305() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPRechercheColonnePrecedente.setText("");
      return;
    }
    
    Boolean valeur = prixVente.getParametrePrixVente().getParametreGeneral().getChercherColonnePrecedente();
    if (valeur == null) {
      tfPRechercheColonnePrecedente.setText("");
    }
    else {
      if (valeur.booleanValue()) {
        tfPRechercheColonnePrecedente.setText("Oui");
      }
      else {
        tfPRechercheColonnePrecedente.setText("Non");
      }
    }
  }
  
  /**
   * Rafraichit le code condition de vente standard de la DG (paramètres généraux).
   */
  private void rafraichirPCodeCNVStandardDG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPCodeCNVStandard.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreGeneral().getCodeCNVStandardDG();
    if (valeur == null) {
      tfPCodeCNVStandard.setText("");
    }
    else {
      tfPCodeCNVStandard.setText(valeur);
    }
  }
  
  /**
   * Rafraichit le code condition de vente promo de la DG (paramètres généraux).
   */
  private void rafraichirPCodeCNVPromoDG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPCodeCNVPromo.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreGeneral().getCodeCNVPromoDG();
    if (valeur == null) {
      tfPCodeCNVPromo.setText("");
    }
    else {
      tfPCodeCNVPromo.setText(valeur);
    }
  }
  
  /**
   * Rafraichit le code condition de vente quantitative de la DG (paramètres généraux).
   */
  private void rafraichirPCodeCNVQuantitativeDG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPCodeCNVQuantitative.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreGeneral().getCodeCNVQuantitativeDG();
    if (valeur == null) {
      tfPCodeCNVQuantitative.setText("");
    }
    else {
      tfPCodeCNVQuantitative.setText(valeur);
    }
  }
  
  /**
   * Rafraichit si la condition de vente quantitative doit être ignorée (paramètres généraux).
   */
  private void rafraichirPIgnoreCNVQuantitativeDG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPIgnoreCNVQuantitative.setText("");
      return;
    }
    
    Boolean valeur = prixVente.getParametrePrixVente().getParametreGeneral().getIgnoreCNVQuantitative();
    if (valeur == null) {
      tfPIgnoreCNVQuantitative.setText("");
    }
    else if (valeur.booleanValue()) {
      tfPIgnoreCNVQuantitative.setText("Oui");
    }
    else {
      tfPIgnoreCNVQuantitative.setText("Non");
    }
  }
  
  /**
   * Rafraichit l'activation de l'emboitage des conditions de vente (paramètres généraux).
   */
  private void rafraichirPEmboitageCNVDG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPEmboitageCNVDG.setText("");
      return;
    }
    
    Boolean valeur = prixVente.getParametrePrixVente().getParametreGeneral().getActiveEmboitageCNV();
    if (valeur == null) {
      tfPEmboitageCNVDG.setText("");
    }
    else if (valeur.booleanValue()) {
      tfPEmboitageCNVDG.setText("Oui");
    }
    else {
      tfPEmboitageCNVDG.setText("Non");
    }
  }
  
  /**
   * Rafraichit l'activation de la recherche de conditions de vente sur les éléments d'achats (paramètres généraux).
   * Note : à affiner
   */
  private void rafraichirPRechercherCNVFournisseurDG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPCNVFournisseurDG.setText("");
      return;
    }
    
    Character valeur = prixVente.getParametrePrixVente().getParametreGeneral().getActiveRechercheCNVFournisseur();
    if (valeur == null) {
      tfPCNVFournisseurDG.setText("");
      return;
    }
    switch (valeur) {
      case ' ':
        tfPCNVFournisseurDG.setText("Non");
        break;
      case '1':
        tfPCNVFournisseurDG.setText("Rattachement sur CNA");
        break;
      case '2':
        tfPCNVFournisseurDG.setText("Fournisseur");
        break;
      default:
        tfPCNVFournisseurDG.setText("");
        break;
    }
  }
  
  /**
   * Rafraichit quelle date d'application pour les conditions de vente est choisi (paramètres généraux).
   */
  private void rafraichirPDateApplicationCNVDG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPDateApplicationCNVDG.setText("");
      return;
    }
    
    Character valeur = prixVente.getParametrePrixVente().getParametreGeneral().getOrigineDateApplicationCNV();
    if (valeur == null) {
      tfPDateApplicationCNVDG.setText("");
      return;
    }
    switch (valeur) {
      case ' ':
        tfPDateApplicationCNVDG.setText("Date création");
        break;
      case '1':
        tfPDateApplicationCNVDG.setText("Date livraison");
        break;
      case '2':
        tfPDateApplicationCNVDG.setText("Date souhaitée");
        break;
      default:
        tfPDateApplicationCNVDG.setText("");
        break;
    }
  }
  
  /**
   * Rafraichit la date du mois en cours de la DG (paramètres généraux).
   */
  private void rafraichirPMoisEnCoursDG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPMoisEnCoursDG.setText("");
      return;
    }
    
    Date date = prixVente.getParametrePrixVente().getParametreGeneral().getMoisEnCours();
    if (date == null) {
      tfPMoisEnCoursDG.setText("");
    }
    else {
      tfPMoisEnCoursDG.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
    }
  }
  
  /**
   * Rafraichit si le calcul du prix net se fait à partir de la colonne tarif 1 (paramètres généraux).
   */
  private void rafraichirPCalculPrixNetCol1DG() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreGeneral() == null) {
      tfPCalculPrixNetCol1DG.setText("");
      return;
    }
    
    Boolean valeur = prixVente.getParametrePrixVente().getParametreGeneral().getActiveCalculPrixNetAvecColonne1();
    if (valeur == null) {
      tfPCalculPrixNetCol1DG.setText("");
    }
    else if (valeur.booleanValue()) {
      tfPCalculPrixNetCol1DG.setText("Oui");
    }
    else {
      tfPCalculPrixNetCol1DG.setText("Non");
    }
  }
  
  // -- Paramètres article
  
  /**
   * Rafraichit le code article (paramètres article).
   */
  private void rafraichirPCodeArticle() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreArticle() == null) {
      tfPCodeArticle.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreArticle().getCodeArticle();
    if (valeur == null) {
      tfPCodeArticle.setText("");
    }
    else {
      tfPCodeArticle.setText(valeur);
    }
  }
  
  /**
   * Rafraichit le pump HT (paramètres article).
   */
  private void rafraichirPPumpHT() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreArticle() == null) {
      tfPPumpHTArticle.setMontant("");
      return;
    }
    
    BigDecimal valeur = prixVente.getParametrePrixVente().getParametreArticle().getPumpHTArticle();
    if (valeur == null) {
      tfPPumpHTArticle.setMontant("");
    }
    else {
      tfPPumpHTArticle.setMontant(valeur);
    }
  }
  
  /**
   * Rafraichit le prix de revient HT (paramètres article).
   */
  private void rafraichirPPrixRevientHT() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreArticle() == null) {
      tfPPrixDeRevientHT.setMontant("");
      return;
    }
    
    BigDecimal valeur = prixVente.getParametrePrixVente().getParametreArticle().getPrixDeRevientHT();
    if (valeur == null) {
      tfPPrixDeRevientHT.setMontant("");
    }
    else {
      tfPPrixDeRevientHT.setMontant(valeur);
    }
  }
  
  /**
   * Rafraichit le prix de revient standard HT (paramètres article).
   */
  private void rafraichirPPrixRevientStandardHT() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreArticle() == null) {
      tfPPrixDeRevientHTStandardCNA.setMontant("");
      return;
    }
    
    BigDecimal valeur = prixVente.getParametrePrixVente().getParametreArticle().getPrixDeRevientHTStandardCNA();
    if (valeur == null) {
      tfPPrixDeRevientHTStandardCNA.setMontant("");
    }
    else {
      tfPPrixDeRevientHTStandardCNA.setMontant(valeur);
    }
  }
  
  /**
   * Rafraichit l'identifiant du fournisseur (paramètres article).
   */
  private void rafraichirPIdentifiantFournisseur() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreArticle() == null) {
      tfPIdentifiantFournisseur.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreArticle().getIdentifiantFournisseur();
    if (valeur == null) {
      tfPIdentifiantFournisseur.setText("");
    }
    else {
      tfPIdentifiantFournisseur.setText(valeur);
    }
  }
  
  /**
   * Rafraichit les rattacheements aux conditions de vente (paramètres article).
   */
  private void rafraichirPRattachementCNV() {
    CalculPrixVente calculPrixVente = getModele().getCalculPrixVente();
    if (calculPrixVente == null || calculPrixVente.getParametrePrixVente() == null
        || calculPrixVente.getParametrePrixVente().getParametreArticle() == null) {
      tfPRattachementCNV.setText("");
      tfPRattachementCNV1.setText("");
      tfPRattachementCNV2.setText("");
      tfPRattachementCNV3.setText("");
      return;
    }
    
    List<String> listeValeur = calculPrixVente.getParametrePrixVente().getParametreArticle().getListeCodeRattachementCNV();
    if (listeValeur == null || listeValeur.isEmpty()) {
      tfPRattachementCNV.setText("");
      tfPRattachementCNV1.setText("");
      tfPRattachementCNV2.setText("");
      tfPRattachementCNV3.setText("");
    }
    else {
      tfPRattachementCNV.setText(listeValeur.get(0).toString());
      if (listeValeur.size() > 1) {
        tfPRattachementCNV1.setText(listeValeur.get(1).toString());
      }
      if (listeValeur.size() > 2) {
        tfPRattachementCNV2.setText(listeValeur.get(2).toString());
      }
      if (listeValeur.size() > 3) {
        tfPRattachementCNV3.setText(listeValeur.get(3).toString());
      }
    }
  }
  
  /**
   * Rafraichit le groupe famille de l'article (paramètres article).
   */
  private void rafraichirPGroupeFamille() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreArticle() == null) {
      tfPGroupeFamille.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreArticle().getGroupeFamille();
    if (valeur == null) {
      tfPGroupeFamille.setText("");
    }
    else {
      tfPGroupeFamille.setText(valeur);
    }
  }
  
  /**
   * Rafraichit la sous-famille de l'article (paramètres article).
   */
  private void rafraichirPSousFamille() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreArticle() == null) {
      tfPSousFamille.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreArticle().getSousFamille();
    if (valeur == null) {
      tfPSousFamille.setText("");
    }
    else {
      tfPSousFamille.setText(valeur);
    }
  }
  
  // -- Paramètres client
  
  /**
   * Rafraichit le code client (paramètres client).
   */
  private void rafraichirPCodeClient() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPIdentifiantClient.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreClient().getIdentifiantClient();
    if (valeur == null) {
      tfPIdentifiantClient.setText("");
    }
    else {
      tfPIdentifiantClient.setText(valeur);
    }
  }
  
  /**
   * Rafraichit le code devise client (paramètres client).
   */
  private void rafraichirPCodeDeviseClient() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPCodeDeviseClient.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreClient().getCodeDevise();
    if (valeur == null) {
      tfPCodeDeviseClient.setText("");
    }
    else {
      tfPCodeDeviseClient.setText(valeur);
    }
  }
  
  /**
   * Rafraichit le type de client TTC ou HT (paramètres client).
   */
  private void rafraichirPClientTTC() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPClientTTC.setText("");
      return;
    }
    
    Boolean valeur = prixVente.getParametrePrixVente().getParametreClient().getTTC();
    if (valeur) {
      tfPClientTTC.setText("");
    }
    else {
      if (valeur.booleanValue()) {
        tfPClientTTC.setText("TTC");
      }
      else {
        tfPClientTTC.setText("HT");
      }
    }
  }
  
  /**
   * Rafraichit la colonne tarif du client (paramètres client).
   */
  private void rafraichirPColonneTarifClient() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPColonneTarifClient.setText("");
      return;
    }
    
    ColonneTarif valeur = prixVente.getParametrePrixVente().getParametreClient().getColonneTarif();
    if (valeur == null) {
      tfPColonneTarifClient.setText("");
    }
    else {
      tfPColonneTarifClient.setText(valeur.getNumero().toString());
    }
  }
  
  /**
   * Rafraichit les remises du client (paramètres client).
   */
  private void rafraichirPRemiseClient() {
    CalculPrixVente calculPrixVente = getModele().getCalculPrixVente();
    if (calculPrixVente == null || calculPrixVente.getParametrePrixVente() == null
        || calculPrixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPPourcentageRemise1Client.setText("");
      tfPPourcentageRemise2Client.setText("");
      tfPPourcentageRemise3Client.setText("");
      return;
    }
    
    List<BigDecimal> listeValeur = calculPrixVente.getParametrePrixVente().getParametreClient().getListeRemise();
    if (listeValeur == null || listeValeur.isEmpty()) {
      tfPPourcentageRemise1Client.setText("");
      tfPPourcentageRemise2Client.setText("");
      tfPPourcentageRemise3Client.setText("");
    }
    else {
      tfPPourcentageRemise1Client.setText(listeValeur.get(0).toString());
      if (listeValeur.size() > 1) {
        tfPPourcentageRemise2Client.setText(listeValeur.get(1).toString());
      }
      if (listeValeur.size() > 2) {
        tfPPourcentageRemise3Client.setText(listeValeur.get(2).toString());
      }
    }
  }
  
  /**
   * Rafraichit le code de la condition de vente du client (paramètres client).
   */
  private void rafraichirPCodeCNVClient() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPCodeCNVClient.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreClient().getCodeConditionVente();
    if (valeur == null) {
      tfPCodeCNVClient.setText("");
    }
    else {
      tfPCodeCNVClient.setText(valeur);
    }
  }
  
  /**
   * Rafraichit le code de la condition de vente promo du client (paramètres client).
   */
  private void rafraichirPCodeCNVPromoClient() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPCodeCNVPromoClient.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreClient().getCodeConditionVentePromo();
    if (valeur == null) {
      tfPCodeCNVPromoClient.setText("");
    }
    else {
      tfPCodeCNVPromoClient.setText(valeur);
    }
  }
  
  /**
   * Rafraichit les numéros des centrales d'achat du client (paramètres client).
   */
  private void rafraichirPCentraleAchatClient() {
    CalculPrixVente calculPrixVente = getModele().getCalculPrixVente();
    if (calculPrixVente == null || calculPrixVente.getParametrePrixVente() == null
        || calculPrixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPNumeroCentraleAchat1Client.setText("");
      tfPNumeroCentraleAchat2Client.setText("");
      tfPNumeroCentraleAchat3Client.setText("");
      return;
    }
    
    List<Integer> listeValeur = calculPrixVente.getParametrePrixVente().getParametreClient().getListeNumeroCentraleAchat();
    if (listeValeur == null || listeValeur.isEmpty()) {
      tfPNumeroCentraleAchat1Client.setText("");
      tfPNumeroCentraleAchat2Client.setText("");
      tfPNumeroCentraleAchat3Client.setText("");
    }
    else {
      tfPNumeroCentraleAchat1Client.setText(listeValeur.get(0).toString());
      if (listeValeur.size() > 1) {
        tfPNumeroCentraleAchat2Client.setText(listeValeur.get(1).toString());
      }
      if (listeValeur.size() > 2) {
        tfPNumeroCentraleAchat3Client.setText(listeValeur.get(2).toString());
      }
    }
  }
  
  /**
   * Rafraichit si les conditions de la DG doivent être ignorées (paramètres client).
   */
  private void rafraichirPIgnorerCNVDGClient() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPIgnorerCNVDGClient.setText("");
      return;
    }
    
    boolean valeur = prixVente.getParametrePrixVente().getParametreClient().isNePasUtiliserConditionVenteDG();
    if (valeur) {
      tfPIgnorerCNVDGClient.setText("Oui");
    }
    else {
      tfPIgnorerCNVDGClient.setText("Non");
    }
  }
  
  /**
   * Rafraichit si les conditions quantitatives doivent être ignorées (paramètres client).
   */
  private void rafraichirPIgnorerCNVQuantitativeClient() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null
        || prixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPIgnorerCNVQuantitativeClient.setText("");
      return;
    }
    
    boolean valeur = prixVente.getParametrePrixVente().getParametreClient().isNePasUtiliserConditionVenteQuantitative();
    if (valeur) {
      tfPIgnorerCNVQuantitativeClient.setText("Oui");
    }
    else {
      tfPIgnorerCNVQuantitativeClient.setText("Non");
    }
  }
  
  // -- Paramètres tarif
  
  /**
   * Rafraichit la date d'application (paramètres tarif).
   */
  private void rafraichirPDateApplication() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null || prixVente.getParametrePrixVente().getParametreTarif() == null) {
      tfPDateApplication.setText("");
      return;
    }
    
    Date date = prixVente.getParametrePrixVente().getParametreTarif().getDateApplication();
    if (date == null) {
      tfPDateApplication.setText("");
    }
    else {
      tfPDateApplication.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
    }
  }
  
  /**
   * Rafraichit l'origine de la date d'application (paramètres tarif).
   */
  private void rafraichirPOrigineDateApplication() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null || prixVente.getParametrePrixVente().getParametreTarif() == null) {
      tfPOrigineDateApplication.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreTarif().getOrigineDate();
    if (valeur == null) {
      tfPOrigineDateApplication.setText("");
    }
    else {
      tfPOrigineDateApplication.setText(Constantes.normerTexte(valeur));
    }
  }
  
  /**
   * Rafraichit la liste des tarifs (paramètres tarif).
   */
  private void rafraichirPListeTarifArticle() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null || prixVente.getParametrePrixVente().getParametreTarif() == null) {
      return;
    }
    
    List<BigDecimal> listePrixVente = prixVente.getParametrePrixVente().getParametreTarif().getListePrixVenteHT();
    if (listePrixVente == null || listePrixVente.isEmpty()) {
      return;
    }
    snTarif.modifierListePrixColonne(listePrixVente);
  }
  
  /**
   * Rafraichit le code devise tarif (paramètres tarif).
   */
  private void rafraichirPCodeDeviseTarif() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null || prixVente.getParametrePrixVente() == null || prixVente.getParametrePrixVente().getParametreTarif() == null) {
      tfPCodeDeviseTarif.setText("");
      return;
    }
    
    String valeur = prixVente.getParametrePrixVente().getParametreTarif().getCodeDevise();
    if (valeur == null) {
      tfPCodeDeviseTarif.setText("");
    }
    else {
      tfPCodeDeviseTarif.setText(valeur);
    }
  }
  
  /**
   * Rafraichit la liste des conditions de ventes (paramètres conditions de ventes).
   */
  private void rafraichirPCodeConditionVente() {
    cbListeCodeCNV.removeAllItems();
    CalculPrixVente calculPrixVente = getModele().getCalculPrixVente();
    if (calculPrixVente == null || calculPrixVente.getParametrePrixVente() == null
        || calculPrixVente.getParametrePrixVente().getListeParametreConditionVente() == null
        || calculPrixVente.getParametrePrixVente().getListeParametreConditionVente().isEmpty()) {
      return;
    }
    
    // Chargement de la combobox avec les codes des conditions de ventes
    List<ParametreConditionVente> liste = calculPrixVente.getParametrePrixVente().getListeParametreConditionVente();
    String codeConditionVenteSelectionnee = calculPrixVente.getCodeConditionVenteSelectionnee();
    ParametreConditionVente parametreConditionVenteSelectionnee = null;
    cbListeCodeCNV.addItem("Non sélectionnée");
    for (ParametreConditionVente parametreConditionVente : liste) {
      cbListeCodeCNV.addItem(parametreConditionVente.getCode());
      if (Constantes.equals(codeConditionVenteSelectionnee, parametreConditionVente.getCode())) {
        parametreConditionVenteSelectionnee = parametreConditionVente;
      }
    }
    
    // Mise à jour des paramètres de la CNV sélectionnée
    if (codeConditionVenteSelectionnee != null) {
      cbListeCodeCNV.setSelectedItem(codeConditionVenteSelectionnee);
    }
    pnlPConditionVente.modifierParametreConditionVente(parametreConditionVenteSelectionnee);
  }
  
  // -- Calcul
  
  /**
   * Rafraichit la devise.
   */
  private void rafraichirDevise() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfCodeDevise.setText("");
      lbOrigineDevise.setMessage(Message.getMessageNormal(""));
      return;
    }
    
    String codeDevise = prixVente.getCodeDevise();
    if (codeDevise == null) {
      tfCodeDevise.setText("");
    }
    else {
      tfCodeDevise.setText(codeDevise);
    }
    String origine = Constantes.normerTexte(prixVente.getOrigineDevise());
    if (origine.isEmpty()) {
      lbOrigineDevise.setMessage(Message.getMessageNormal("Inconnue"));
    }
    else {
      lbOrigineDevise.setMessage(Message.getMessageNormal(origine));
    }
  }
  
  /**
   * Rafraichit la colonne tarif saisie.
   */
  private void rafraichirColonneTarifSaisie() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfColonneTarifSaisie.setText("");
      return;
    }
    
    ColonneTarif colonne = prixVente.getColonneTarifSaisie();
    if (colonne == null) {
      tfColonneTarifSaisie.setText("");
    }
    else {
      tfColonneTarifSaisie.setText(colonne.getNumero().toString());
    }
  }
  
  /**
   * Rafraichit la colonne tarif du document de vente.
   */
  private void rafraichirColonneTarifDocumentVente() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfColonneTarifDocumentVente.setText("");
      return;
    }
    
    ColonneTarif colonne = prixVente.getColonneTarifDocumentVente();
    if (colonne == null) {
      tfColonneTarifDocumentVente.setText("");
    }
    else {
      tfColonneTarifDocumentVente.setText(colonne.getNumero().toString());
    }
  }
  
  /**
   * Rafraichit la colonne tarif de la condition de vente.
   */
  private void rafraichirColonneTarifConditionVente() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfColonneTarifConditionVente.setText("");
      return;
    }
    
    ColonneTarif colonne = prixVente.getColonneTarifConditionVente();
    if (colonne == null) {
      tfColonneTarifConditionVente.setText("");
    }
    else {
      tfColonneTarifConditionVente.setText(colonne.getNumero().toString());
    }
  }
  
  /**
   * Rafraichit la colonne tarif 1 du client.
   */
  private void rafraichirColonneTarif1Client() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfColonneTarifClient.setText("");
      return;
    }
    
    ColonneTarif colonne = prixVente.getColonneTarif1Client();
    if (colonne == null) {
      tfColonneTarifClient.setText("");
    }
    else {
      tfColonneTarifClient.setText(colonne.getNumero().toString());
    }
  }
  
  /**
   * Rafraichit la colonne tarif de la PS105.
   */
  private void rafraichirColonneTarifPS105() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfColonneTarifPS105.setText("");
      return;
    }
    
    ColonneTarif colonne = prixVente.getColonneTarifPS105();
    if (colonne == null) {
      tfColonneTarifPS105.setText("");
    }
    else {
      tfColonneTarifPS105.setText(colonne.getNumero().toString());
    }
  }
  
  /**
   * Rafraichit la colonne tarif utilisée.
   */
  private void rafraichirColonneTarif() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfColonneTarif.setText("");
      lbOrigineColonneTarif.setText("");
      return;
    }
    
    ColonneTarif colonne = prixVente.getColonneTarifAUtiliser();
    if (colonne == null) {
      tfColonneTarif.setText("");
      lbOrigineColonneTarif.setText("");
    }
    else {
      tfColonneTarif.setText(colonne.getNumero().toString());
      lbOrigineColonneTarif.setText("Colonne " + colonne.getOrigine().getLibelle().toLowerCase());
    }
  }
  
  /**
   * Rafraichit le prix de base HT saisi.
   */
  private void rafraichirPrixBaseHTSaisi() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixBaseHTSaisi.setMontant("");
      return;
    }
    
    BigDecimal valeur = prixVente.getPrixBaseHTSaisi();
    if (valeur == null) {
      tfPrixBaseHTSaisi.setMontant("");
    }
    else {
      tfPrixBaseHTSaisi.setMontant(valeur.toString());
    }
  }
  
  /**
   * Rafraichit le prix de base HT provenant de la condition de vente.
   */
  private void rafraichirPrixBaseHTCNV() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixBaseHTCNV.setText("");
      return;
    }
    
    BigDecimal prixBaseHT = prixVente.getPrixBaseHTCNV();
    if (prixBaseHT == null) {
      tfPrixBaseHTCNV.setText("");
    }
    else {
      tfPrixBaseHTCNV.setText(prixBaseHT.toString());
    }
    String origine = prixVente.getCodeMeilleureConditionVente();
    if (origine == null) {
      lbCodeCNVPrixBase.setText("");
    }
    else {
      lbCodeCNVPrixBase.setText(origine);
    }
  }
  
  /**
   * Rafraichit le prix de base HT provenant du PUMP.
   */
  private void rafraichirPrixBaseHTPump() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixBaseHTPump.setText("");
      return;
    }
    
    BigDecimal prixBaseHT = prixVente.getPrixBaseHTPump();
    if (prixBaseHT == null) {
      tfPrixBaseHTPump.setText("");
    }
    else {
      tfPrixBaseHTPump.setText(prixBaseHT.toString());
    }
  }
  
  /**
   * Rafraichit le prix de base HT provenant du pris de revient.
   */
  private void rafraichirPrixBaseHTPrixRevient() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixBaseHTPrixRevient.setText("");
      return;
    }
    
    BigDecimal prixBaseHT = prixVente.getPrixBaseHTPrixRevient();
    if (prixBaseHT == null) {
      tfPrixBaseHTPrixRevient.setText("");
    }
    else {
      tfPrixBaseHTPrixRevient.setText(prixBaseHT.toString());
    }
    // TODO l'origine PRV ou PRS
  }
  
  /**
   * Rafraichit le prix de base HT de la colonne tarif.
   */
  private void rafraichirPrixBaseHTColonneTarif() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixBaseHTColonneTarif.setText("");
      return;
    }
    
    BigDecimal prixBaseHT = prixVente.getPrixBaseHTColonneTarif();
    if (prixBaseHT == null) {
      tfPrixBaseHTColonneTarif.setText("");
    }
    else {
      tfPrixBaseHTColonneTarif.setText(prixBaseHT.toString());
    }
  }
  
  /**
   * Rafraichit le prix de base HT.
   */
  private void rafraichirPrixBaseHT() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixBaseHT.setText("");
      lbOriginePrixBaseHT.setText("");
      return;
    }
    
    BigDecimal prixBaseHT = prixVente.getPrixBaseHT();
    if (prixBaseHT == null) {
      tfPrixBaseHT.setText("");
    }
    else {
      tfPrixBaseHT.setText(prixBaseHT.toString());
    }
    
    String origine = prixVente.getOriginePrixBaseHT();
    if (origine == null) {
      lbOriginePrixBaseHT.setText("");
    }
    else {
      lbOriginePrixBaseHT.setText(prixVente.getOriginePrixBaseHT());
    }
  }
  
  /**
   * Rafraichit le pourcentage de remise saisi.
   */
  private void rafraichirPourcentageRemiseSaisie() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPourcentageRemiseSaisi.setText("");
      return;
    }
    
    BigDecimal valeur = prixVente.getPourcentageRemiseSaisi();
    if (valeur == null) {
      tfPourcentageRemiseSaisi.setText("");
    }
    else {
      tfPourcentageRemiseSaisi.setText(valeur.toString());
    }
  }
  
  /**
   * Rafraichit le pourcentage de remise client.
   */
  private void rafraichirPourcentageRemiseClient() {
    CalculPrixVente calculPrixVente = getModele().getCalculPrixVente();
    if (calculPrixVente == null || calculPrixVente.getParametrePrixVente() == null
        || calculPrixVente.getParametrePrixVente().getParametreClient() == null) {
      tfPourcentageRemise1Client.setText("");
      tfPourcentageRemise2Client.setText("");
      tfPourcentageRemise3Client.setText("");
      return;
    }
    
    List<BigDecimal> listeValeur = calculPrixVente.getListePourcentageRemiseClient();
    if (listeValeur == null || listeValeur.isEmpty()) {
      tfPourcentageRemise1Client.setText("");
      tfPourcentageRemise2Client.setText("");
      tfPourcentageRemise3Client.setText("");
    }
    else {
      tfPourcentageRemise1Client.setText(listeValeur.get(0).toString());
      if (listeValeur.size() > 1) {
        tfPourcentageRemise2Client.setText(listeValeur.get(1).toString());
      }
      if (listeValeur.size() > 2) {
        tfPourcentageRemise3Client.setText(listeValeur.get(2).toString());
      }
    }
  }
  
  /**
   * Rafraichit le pourcentage de remise CNV.
   */
  private void rafraichirPourcentageRemiseCNV() {
    CalculPrixVente calculPrixVente = getModele().getCalculPrixVente();
    if (calculPrixVente == null || calculPrixVente.getParametrePrixVente() == null
        || calculPrixVente.getParametrePrixVente().getListeParametreConditionVente() == null) {
      tfPourcentageRemise1CNV.setText("");
      tfPourcentageRemise2CNV.setText("");
      tfPourcentageRemise3CNV.setText("");
      tfPourcentageRemise4CNV.setText("");
      tfPourcentageRemise5CNV.setText("");
      tfPourcentageRemise6CNV.setText("");
      return;
    }
    
    List<BigDecimal> listeValeur = calculPrixVente.getListePourcentageRemiseCNV();
    if (listeValeur == null || listeValeur.isEmpty()) {
      tfPourcentageRemise1CNV.setText("");
      tfPourcentageRemise2CNV.setText("");
      tfPourcentageRemise3CNV.setText("");
      tfPourcentageRemise4CNV.setText("");
      tfPourcentageRemise5CNV.setText("");
      tfPourcentageRemise6CNV.setText("");
    }
    else {
      tfPourcentageRemise1CNV.setText(listeValeur.get(0).toString());
      if (listeValeur.size() > 1) {
        tfPourcentageRemise2CNV.setText(listeValeur.get(1).toString());
      }
      if (listeValeur.size() > 2) {
        tfPourcentageRemise3CNV.setText(listeValeur.get(2).toString());
      }
      if (listeValeur.size() > 3) {
        tfPourcentageRemise4CNV.setText(listeValeur.get(3).toString());
      }
      if (listeValeur.size() > 4) {
        tfPourcentageRemise5CNV.setText(listeValeur.get(4).toString());
      }
      if (listeValeur.size() > 5) {
        tfPourcentageRemise6CNV.setText(listeValeur.get(5).toString());
      }
    }
  }
  
  /**
   * Rafraichit le pourcentage de remise à appliquer.
   */
  private void rafraichirPourcentageRemise() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPourcentageRemise1.setText("");
      tfPourcentageRemise2.setText("");
      tfPourcentageRemise3.setText("");
      tfPourcentageRemise4.setText("");
      tfPourcentageRemise5.setText("");
      tfPourcentageRemise6.setText("");
      lbOriginePourcentageRemise.setText("");
      return;
    }
    
    List<BigDecimal> listeValeur = prixVente.getListePourcentageRemise();
    if (listeValeur == null || listeValeur.isEmpty()) {
      tfPourcentageRemise1.setText("");
      tfPourcentageRemise2.setText("");
      tfPourcentageRemise3.setText("");
      tfPourcentageRemise4.setText("");
      tfPourcentageRemise5.setText("");
      tfPourcentageRemise6.setText("");
    }
    else {
      tfPourcentageRemise1.setText(listeValeur.get(0).toString());
      if (listeValeur.size() > 1) {
        tfPourcentageRemise2.setText(listeValeur.get(1).toString());
      }
      if (listeValeur.size() > 2) {
        tfPourcentageRemise3.setText(listeValeur.get(2).toString());
      }
      if (listeValeur.size() > 3) {
        tfPourcentageRemise4.setText(listeValeur.get(3).toString());
      }
      if (listeValeur.size() > 4) {
        tfPourcentageRemise5.setText(listeValeur.get(4).toString());
      }
      if (listeValeur.size() > 5) {
        tfPourcentageRemise6.setText(listeValeur.get(5).toString());
      }
    }
    // Origine de la remise
    EnumOrigine origine = prixVente.getOriginePourcentageRemise();
    if (origine == null) {
      lbOriginePourcentageRemise.setText("");
    }
    else {
      lbOriginePourcentageRemise.setText("Remise " + origine.getLibelle().toLowerCase());
    }
  }
  
  /**
   * Rafraichit le prix net HT saisi.
   */
  private void rafraichirPrixNetHTSaisi() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixNetHTSaisi.setMontant("");
      return;
    }
    
    BigDecimal valeur = prixVente.getPrixNetHTSaisi();
    if (valeur == null) {
      tfPrixNetHTSaisi.setMontant("");
    }
    else {
      tfPrixNetHTSaisi.setMontant(valeur.toString());
    }
  }
  
  /**
   * Rafraichit le prix net HT de la colonne tarif.
   */
  private void rafraichirPrixNetHTBase() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixNetHTColonneTarif.setText("");
      return;
    }
    
    BigDecimal prixBaseHT = prixVente.getPrixNetHTColonneTarif();
    if (prixBaseHT == null) {
      tfPrixNetHTColonneTarif.setText("");
    }
    else {
      tfPrixNetHTColonneTarif.setText(prixBaseHT.toString());
    }
  }
  
  /**
   * Rafraichit le prix net HT calculé.
   */
  private void rafraichirPrixNetNTCalcule() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixNetHTCalcule.setText("");
      return;
    }
    
    BigDecimal prixBaseHT = prixVente.getPrixNetHTCalcule();
    if (prixBaseHT == null) {
      tfPrixNetHTCalcule.setText("");
    }
    else {
      tfPrixNetHTCalcule.setText(prixBaseHT.toString());
    }
  }
  
  /**
   * Rafraichit le prix net HT de la condition de vente.
   */
  private void rafraichirPrixNetHTCNV() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixNetHTCNV.setText("");
      return;
    }
    
    BigDecimal prixBaseHT = prixVente.getPrixNetHTCNV();
    if (prixBaseHT == null) {
      tfPrixNetHTCNV.setText("");
    }
    else {
      tfPrixNetHTCNV.setText(prixBaseHT.toString());
    }
    String origine = prixVente.getCodeMeilleureConditionVente();
    if (origine == null) {
      lbCodeCNVPrixNet.setText("");
    }
    else {
      lbCodeCNVPrixNet.setText(origine);
    }
    
  }
  
  /**
   * Rafraichit le prix net HT.
   */
  private void rafraichirPrixNetHT() {
    CalculPrixVente prixVente = getModele().getCalculPrixVente();
    if (prixVente == null) {
      tfPrixNetHT.setText("");
      lbOriginePrixNetHT.setText("");
      return;
    }
    
    BigDecimal prixNetHT = prixVente.getPrixNetHT();
    if (prixNetHT == null) {
      tfPrixNetHT.setText("");
    }
    else {
      tfPrixNetHT.setText(prixNetHT.toString());
    }
    
    String origine = prixVente.getOriginePrixBaseHT();
    if (origine == null) {
      lbOriginePrixNetHT.setText("");
    }
    else {
      lbOriginePrixNetHT.setText(prixVente.getOriginePrixNetHT());
    }
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfColonneTarifSaisieFocusLost(FocusEvent e) {
    try {
      getModele().modifierColonneSaisie(tfColonneTarifSaisie.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfPrixBaseHTSaisiValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierPrixBaseHTSaisi(tfPrixBaseHTSaisi.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfPrixNetHTSaisiValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierPrixNetHTSaisi(tfPrixNetHTSaisi.getMontant());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfPourcentageRemiseSaisiFocusLost(FocusEvent e) {
    try {
      getModele().modifierPourcentageRemiseSaisi(tfPourcentageRemiseSaisi.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbListeCodeCNVItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierCNVSelectionne((String) cbListeCodeCNV.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    scrpContenu = new JScrollPane();
    pnlContenu = new SNPanelContenu();
    pnlParametrePrixVente1 = new SNPanel();
    pnlParametreGeneral = new SNPanelTitre();
    lbPBaseDeDonnees = new SNLabelChamp();
    tfPBaseDeDonnees = new SNTexte();
    lbPCodeDeviseDG = new SNLabelChamp();
    tfPCodeDeviseDG = new SNTexte();
    lbPModeNegoce = new SNLabelChamp();
    tfPModeNegoce = new SNTexte();
    lbPMoisEnCoursDG = new SNLabelChamp();
    tfPMoisEnCoursDG = new SNTexte();
    lbPCodeEtablissement = new SNLabelChamp();
    tfPCodeEtablissement = new SNTexte();
    lbPCalculPrixNetCol1DG = new SNLabelChamp();
    tfPCalculPrixNetCol1DG = new SNTexte();
    lbPColonnePS105 = new SNLabelChamp();
    tfPColonnePS105 = new SNTexte();
    lbPRechercheColonnePrecedente = new SNLabelChamp();
    tfPRechercheColonnePrecedente = new SNTexte();
    lbPCodeCNVStandard = new SNLabelChamp();
    tfPCodeCNVStandard = new SNTexte();
    lbPCodeCNVPromo = new SNLabelChamp();
    tfPCodeCNVPromo = new SNTexte();
    lbPCodeCNVQuantitative = new SNLabelChamp();
    tfPCodeCNVQuantitative = new SNTexte();
    lbPIgnoreCNVQuantitative = new SNLabelChamp();
    tfPIgnoreCNVQuantitative = new SNTexte();
    lbPEmboitageCNVDG = new SNLabelChamp();
    tfPEmboitageCNVDG = new SNTexte();
    lbPCNVFournisseurDG = new SNLabelChamp();
    tfPCNVFournisseurDG = new SNTexte();
    lbPDateApplicationCNVDG = new SNLabelChamp();
    tfPDateApplicationCNVDG = new SNTexte();
    pnlParametreTarif = new SNPanelTitre();
    lbPDateApplication = new SNLabelChamp();
    tfPDateApplication = new SNTexte();
    snTarif = new SNPTarif();
    lbPOrigineDateApplication = new SNLabelChamp();
    tfPOrigineDateApplication = new SNTexte();
    lbPCodeDeviseTarif = new SNLabelChamp();
    tfPCodeDeviseTarif = new SNTexte();
    pnlParametreConditionVente = new SNPanelTitre();
    lbPFiltreListeCNV = new SNLabelChamp();
    chkFiltreListeCNV = new JCheckBox();
    lbPCodeCNV = new SNLabelChamp();
    cbListeCodeCNV = new SNComboBox();
    pnlPConditionVente = new SNPConditionVente();
    pnlParametrePrixVente2 = new SNPanel();
    pnlParametreDocumentVente = new SNPanelTitre();
    pnlPDocumentVente = new SNPDocumentVente();
    pnlParametreClient = new SNPanelTitre();
    lbPIdentifiantClient = new SNLabelChamp();
    tfPIdentifiantClient = new SNTexte();
    lbPCodeDeviseClient = new SNLabelChamp();
    tfPCodeDeviseClient = new SNTexte();
    lbPClientTTC = new SNLabelChamp();
    tfPClientTTC = new SNTexte();
    lbPColonneTarifCLient = new SNLabelChamp();
    tfPColonneTarifClient = new SNTexte();
    lbPPourcentageRemiseClient = new SNLabelChamp();
    pnlPPourcentageRemiseClient = new SNPanel();
    tfPPourcentageRemise1Client = new SNTexte();
    tfPPourcentageRemise2Client = new SNTexte();
    tfPPourcentageRemise3Client = new SNTexte();
    lbPCodeCNVClient = new SNLabelChamp();
    tfPCodeCNVClient = new SNTexte();
    lbPCodeCNVPromoClient = new SNLabelChamp();
    tfPCodeCNVPromoClient = new SNTexte();
    lbPNumeroCentraleAchatClient = new SNLabelChamp();
    pnlPNumeroCentraleAchatClient = new SNPanel();
    tfPNumeroCentraleAchat1Client = new SNTexte();
    tfPNumeroCentraleAchat2Client = new SNTexte();
    tfPNumeroCentraleAchat3Client = new SNTexte();
    lbPIgnorerCNVDGClient = new SNLabelChamp();
    tfPIgnorerCNVDGClient = new SNTexte();
    lbPIgnorerCNVQuantitativeClient = new SNLabelChamp();
    tfPIgnorerCNVQuantitativeClient = new SNTexte();
    pnlParametreArticle = new SNPanelTitre();
    lbPCodeArticle = new SNLabelChamp();
    tfPCodeArticle = new SNTexte();
    lbPPumpHTArticle = new SNLabelChamp();
    tfPPumpHTArticle = new SNMontant();
    lbPPrixDeRevientHT = new SNLabelChamp();
    tfPPrixDeRevientHT = new SNMontant();
    lbPPrixDeRevientHTStandardCNA = new SNLabelChamp();
    tfPPrixDeRevientHTStandardCNA = new SNMontant();
    lbPIdentifiantFournisseur = new SNLabelChamp();
    tfPIdentifiantFournisseur = new SNTexte();
    lbPRattachementCNV = new SNLabelChamp();
    pnlPRattachementCNV = new SNPanel();
    tfPRattachementCNV = new SNTexte();
    tfPRattachementCNV1 = new SNTexte();
    tfPRattachementCNV2 = new SNTexte();
    tfPRattachementCNV3 = new SNTexte();
    lbPGroupeFamille = new SNLabelChamp();
    tfPGroupeFamille = new SNTexte();
    lbPSousFamille = new SNLabelChamp();
    tfPSousFamille = new SNTexte();
    pnlCalcul = new SNPanelTitre();
    pnInformationGenerale = new SNPanelTitre();
    lbCodeDevise = new SNLabelChamp();
    tfCodeDevise = new SNTexte();
    lbOrigineDevise = new SNMessage();
    pnlColonneTarif = new SNPanelTitre();
    lbColonneTarifSaisie = new SNLabelChamp();
    tfColonneTarifSaisie = new SNTexte();
    lbColonneTarifDocumentVente = new SNLabelChamp();
    tfColonneTarifDocumentVente = new SNTexte();
    lbColonneTarifConditionVente = new SNLabelChamp();
    tfColonneTarifConditionVente = new SNTexte();
    lbColonneTarifClient = new SNLabelChamp();
    tfColonneTarifClient = new SNTexte();
    lbColonneTarifPS105 = new SNLabelChamp();
    tfColonneTarifPS105 = new SNTexte();
    lbColonneTarif = new SNMessage();
    tfColonneTarif = new SNTexte();
    lbOrigineColonneTarif = new SNMessage();
    pnlPrixBase = new SNPanelTitre();
    lbPrixBaseHTSaisi = new SNLabelChamp();
    tfPrixBaseHTSaisi = new SNMontant();
    lbPrixBaseHTCNV = new SNLabelChamp();
    tfPrixBaseHTCNV = new SNTexte();
    lbCodeCNVPrixBase = new SNMessage();
    lbPrixBaseHTPump = new SNLabelChamp();
    tfPrixBaseHTPump = new SNTexte();
    lbPrixBaseHTPrixRevient = new SNLabelChamp();
    tfPrixBaseHTPrixRevient = new SNTexte();
    lbOriginePrixBasePrixRevient = new SNMessage();
    lbPrixBaseHTColonneTarif = new SNLabelChamp();
    tfPrixBaseHTColonneTarif = new SNTexte();
    lbPrixBaseHT = new SNMessage();
    tfPrixBaseHT = new SNTexte();
    lbOriginePrixBaseHT = new SNMessage();
    pnlRemise = new SNPanelTitre();
    lbPourcentageRemiseSaisi = new SNLabelChamp();
    pnlPourcentageRemiseSaisi = new SNPanel();
    tfPourcentageRemiseSaisi = new SNTexte();
    lbPourcentageRemiseClient = new SNLabelChamp();
    pnlPourcentageRemiseClient = new SNPanel();
    tfPourcentageRemise1Client = new SNTexte();
    tfPourcentageRemise2Client = new SNTexte();
    tfPourcentageRemise3Client = new SNTexte();
    lbPourcentageRemiseCNV = new SNLabelChamp();
    pnlPPourcentageRemise = new SNPanel();
    tfPourcentageRemise1CNV = new SNTexte();
    tfPourcentageRemise2CNV = new SNTexte();
    tfPourcentageRemise3CNV = new SNTexte();
    tfPourcentageRemise4CNV = new SNTexte();
    tfPourcentageRemise5CNV = new SNTexte();
    tfPourcentageRemise6CNV = new SNTexte();
    lbPourcentageRemise = new SNMessage();
    pnlPourcentageRemise = new SNPanel();
    tfPourcentageRemise1 = new SNTexte();
    tfPourcentageRemise2 = new SNTexte();
    tfPourcentageRemise3 = new SNTexte();
    tfPourcentageRemise4 = new SNTexte();
    tfPourcentageRemise5 = new SNTexte();
    tfPourcentageRemise6 = new SNTexte();
    lbOriginePourcentageRemise = new SNMessage();
    pnPrixNet = new SNPanelTitre();
    lbPrixNetHTSaisi = new SNLabelChamp();
    tfPrixNetHTSaisi = new SNMontant();
    lbPrixNetHTColonneTarif = new SNLabelChamp();
    tfPrixNetHTColonneTarif = new SNTexte();
    lbPrixNetHTCalcule = new SNLabelChamp();
    tfPrixNetHTCalcule = new SNTexte();
    lbPrixNetHTCNV = new SNLabelChamp();
    tfPrixNetHTCNV = new SNTexte();
    lbCodeCNVPrixNet = new SNMessage();
    lbPrixNetHT = new SNMessage();
    tfPrixNetHT = new SNTexte();
    lbOriginePrixNetHT = new SNMessage();
    pnlCalcul2 = new SNPanelTitre();
    pnInformationGenerale2 = new SNPanelTitre();
    lbCodeDevise2 = new SNLabelChamp();
    tfCodeDevise2 = new SNTexte();
    lbOrigineDevise2 = new SNMessage();
    pnlColonneTarif2 = new SNPanelTitre();
    lbColonneTarifSaisie2 = new SNLabelChamp();
    tfColonneTarifSaisie2 = new SNTexte();
    lbColonneTarifDocumentVente2 = new SNLabelChamp();
    tfColonneTarifDocumentVente2 = new SNTexte();
    lbColonneTarifConditionVente2 = new SNLabelChamp();
    tfColonneTarifConditionVente2 = new SNTexte();
    lbColonneTarifClient2 = new SNLabelChamp();
    tfColonneTarifClient2 = new SNTexte();
    lbColonneTarifPS106 = new SNLabelChamp();
    tfColonneTarifPS106 = new SNTexte();
    lbColonneTarif2 = new SNMessage();
    tfColonneTarif2 = new SNTexte();
    lbOrigineColonneTarif2 = new SNMessage();
    pnlPrixBase2 = new SNPanelTitre();
    lbPrixBaseHTSaisi2 = new SNLabelChamp();
    tfPrixBaseHTSaisi2 = new SNMontant();
    lbPrixBaseHTCNV2 = new SNLabelChamp();
    tfPrixBaseHTCNV2 = new SNTexte();
    lbCodeCNVPrixBase2 = new SNMessage();
    lbPrixBaseHTPump2 = new SNLabelChamp();
    tfPrixBaseHTPump2 = new SNTexte();
    lbPrixBaseHTPrixRevient2 = new SNLabelChamp();
    tfPrixBaseHTPrixRevient2 = new SNTexte();
    lbOriginePrixBasePrixRevient2 = new SNMessage();
    lbPrixBaseHTColonneTarif2 = new SNLabelChamp();
    tfPrixBaseHTColonneTarif2 = new SNTexte();
    lbPrixBaseHT2 = new SNMessage();
    tfPrixBaseHT2 = new SNTexte();
    lbOriginePrixBaseHT2 = new SNMessage();
    pnlRemise2 = new SNPanelTitre();
    lbPourcentageRemiseSaisi2 = new SNLabelChamp();
    pnlPourcentageRemiseSaisi2 = new SNPanel();
    tfPourcentageRemiseSaisi2 = new SNTexte();
    lbPourcentageRemiseClient2 = new SNLabelChamp();
    pnlPourcentageRemiseClient2 = new SNPanel();
    tfPourcentageRemise1Client2 = new SNTexte();
    tfPourcentageRemise2Client2 = new SNTexte();
    tfPourcentageRemise3Client2 = new SNTexte();
    lbPourcentageRemiseCNV2 = new SNLabelChamp();
    pnlPPourcentageRemise2 = new SNPanel();
    tfPourcentageRemise1CNV2 = new SNTexte();
    tfPourcentageRemise2CNV2 = new SNTexte();
    tfPourcentageRemise3CNV2 = new SNTexte();
    tfPourcentageRemise4CNV2 = new SNTexte();
    tfPourcentageRemise5CNV2 = new SNTexte();
    tfPourcentageRemise6CNV2 = new SNTexte();
    lbPourcentageRemise2 = new SNMessage();
    pnlPourcentageRemise2 = new SNPanel();
    tfPourcentageRemise7 = new SNTexte();
    tfPourcentageRemise8 = new SNTexte();
    tfPourcentageRemise9 = new SNTexte();
    tfPourcentageRemise10 = new SNTexte();
    tfPourcentageRemise11 = new SNTexte();
    tfPourcentageRemise12 = new SNTexte();
    lbOriginePourcentageRemise2 = new SNMessage();
    pnPrixNet2 = new SNPanelTitre();
    lbPrixNetHTSaisi2 = new SNLabelChamp();
    tfPrixNetHTSaisi2 = new SNMontant();
    lbPrixNetHTColonneTarif2 = new SNLabelChamp();
    tfPrixNetHTColonneTarif2 = new SNTexte();
    lbPrixNetHTCalcule2 = new SNLabelChamp();
    tfPrixNetHTCalcule2 = new SNTexte();
    lbPrixNetHTCNV2 = new SNLabelChamp();
    tfPrixNetHTCNV2 = new SNTexte();
    lbCodeCNVPrixNet2 = new SNMessage();
    lbPrixNetHT2 = new SNMessage();
    tfPrixNetHT2 = new SNTexte();
    lbOriginePrixNetHT2 = new SNMessage();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("D\u00e9tail du calcul du prix de vente");
    setBackground(new Color(238, 238, 210));
    setModal(true);
    setMinimumSize(new Dimension(1740, 1080));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBorder(null);
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setOpaque(true);
      pnlPrincipal.setPreferredSize(new Dimension(1200, 1140));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== scrpContenu ========
      {
        scrpContenu.setBackground(new Color(238, 238, 210));
        scrpContenu.setOpaque(true);
        scrpContenu.setName("scrpContenu");
        
        // ======== pnlContenu ========
        {
          pnlContenu.setBackground(new Color(238, 238, 210));
          pnlContenu.setOpaque(true);
          pnlContenu.setMinimumSize(new Dimension(640, 800));
          pnlContenu.setPreferredSize(new Dimension(1200, 1080));
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlParametrePrixVente1 ========
          {
            pnlParametrePrixVente1.setPreferredSize(new Dimension(430, 1080));
            pnlParametrePrixVente1.setMinimumSize(new Dimension(430, 1080));
            pnlParametrePrixVente1.setName("pnlParametrePrixVente1");
            pnlParametrePrixVente1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlParametrePrixVente1.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlParametrePrixVente1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlParametrePrixVente1.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlParametrePrixVente1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlParametreGeneral ========
            {
              pnlParametreGeneral.setBackground(new Color(238, 238, 210));
              pnlParametreGeneral.setOpaque(false);
              pnlParametreGeneral.setPreferredSize(new Dimension(210, 300));
              pnlParametreGeneral.setMinimumSize(new Dimension(280, 300));
              pnlParametreGeneral.setTitre("Param\u00e8tres g\u00e9n\u00e9raux");
              pnlParametreGeneral.setModeReduit(true);
              pnlParametreGeneral.setBorder(
                  new CompoundBorder(BorderFactory.createEmptyBorder(), new TitledBorder(null, "Param\u00e8tres g\u00e9n\u00e9raux",
                      TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 11))));
              pnlParametreGeneral.setName("pnlParametreGeneral");
              pnlParametreGeneral.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlParametreGeneral.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlParametreGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlParametreGeneral.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlParametreGeneral.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPBaseDeDonnees ----
              lbPBaseDeDonnees.setText("Base de donn\u00e9es");
              lbPBaseDeDonnees.setModeReduit(true);
              lbPBaseDeDonnees.setMinimumSize(new Dimension(130, 22));
              lbPBaseDeDonnees.setPreferredSize(new Dimension(130, 22));
              lbPBaseDeDonnees.setName("lbPBaseDeDonnees");
              pnlParametreGeneral.add(lbPBaseDeDonnees, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPBaseDeDonnees ----
              tfPBaseDeDonnees.setEnabled(false);
              tfPBaseDeDonnees.setModeReduit(true);
              tfPBaseDeDonnees.setName("tfPBaseDeDonnees");
              pnlParametreGeneral.add(tfPBaseDeDonnees, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPCodeDeviseDG ----
              lbPCodeDeviseDG.setText("Code devise");
              lbPCodeDeviseDG.setModeReduit(true);
              lbPCodeDeviseDG.setName("lbPCodeDeviseDG");
              pnlParametreGeneral.add(lbPCodeDeviseDG, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeDeviseDG ----
              tfPCodeDeviseDG.setEnabled(false);
              tfPCodeDeviseDG.setModeReduit(true);
              tfPCodeDeviseDG.setName("tfPCodeDeviseDG");
              pnlParametreGeneral.add(tfPCodeDeviseDG, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPModeNegoce ----
              lbPModeNegoce.setText("Mode n\u00e9goce");
              lbPModeNegoce.setModeReduit(true);
              lbPModeNegoce.setName("lbPModeNegoce");
              pnlParametreGeneral.add(lbPModeNegoce, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPModeNegoce ----
              tfPModeNegoce.setEnabled(false);
              tfPModeNegoce.setModeReduit(true);
              tfPModeNegoce.setName("tfPModeNegoce");
              pnlParametreGeneral.add(tfPModeNegoce, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPMoisEnCoursDG ----
              lbPMoisEnCoursDG.setText("Mois en cours");
              lbPMoisEnCoursDG.setModeReduit(true);
              lbPMoisEnCoursDG.setName("lbPMoisEnCoursDG");
              pnlParametreGeneral.add(lbPMoisEnCoursDG, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPMoisEnCoursDG ----
              tfPMoisEnCoursDG.setEnabled(false);
              tfPMoisEnCoursDG.setModeReduit(true);
              tfPMoisEnCoursDG.setName("tfPMoisEnCoursDG");
              pnlParametreGeneral.add(tfPMoisEnCoursDG, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPCodeEtablissement ----
              lbPCodeEtablissement.setText("Code \u00e9tablissement");
              lbPCodeEtablissement.setModeReduit(true);
              lbPCodeEtablissement.setName("lbPCodeEtablissement");
              pnlParametreGeneral.add(lbPCodeEtablissement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeEtablissement ----
              tfPCodeEtablissement.setEnabled(false);
              tfPCodeEtablissement.setModeReduit(true);
              tfPCodeEtablissement.setName("tfPCodeEtablissement");
              pnlParametreGeneral.add(tfPCodeEtablissement, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPCalculPrixNetCol1DG ----
              lbPCalculPrixNetCol1DG.setText("Calcul prix net col 1");
              lbPCalculPrixNetCol1DG.setModeReduit(true);
              lbPCalculPrixNetCol1DG.setName("lbPCalculPrixNetCol1DG");
              pnlParametreGeneral.add(lbPCalculPrixNetCol1DG, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCalculPrixNetCol1DG ----
              tfPCalculPrixNetCol1DG.setEnabled(false);
              tfPCalculPrixNetCol1DG.setModeReduit(true);
              tfPCalculPrixNetCol1DG.setName("tfPCalculPrixNetCol1DG");
              pnlParametreGeneral.add(tfPCalculPrixNetCol1DG, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPColonnePS105 ----
              lbPColonnePS105.setText("Colonne PS105");
              lbPColonnePS105.setModeReduit(true);
              lbPColonnePS105.setName("lbPColonnePS105");
              pnlParametreGeneral.add(lbPColonnePS105, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPColonnePS105 ----
              tfPColonnePS105.setEnabled(false);
              tfPColonnePS105.setModeReduit(true);
              tfPColonnePS105.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPColonnePS105.setName("tfPColonnePS105");
              pnlParametreGeneral.add(tfPColonnePS105, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPRechercheColonnePrecedente ----
              lbPRechercheColonnePrecedente.setText("Colonne pr\u00e9c\u00e9dente");
              lbPRechercheColonnePrecedente.setModeReduit(true);
              lbPRechercheColonnePrecedente.setName("lbPRechercheColonnePrecedente");
              pnlParametreGeneral.add(lbPRechercheColonnePrecedente, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPRechercheColonnePrecedente ----
              tfPRechercheColonnePrecedente.setEnabled(false);
              tfPRechercheColonnePrecedente.setModeReduit(true);
              tfPRechercheColonnePrecedente.setMaximumSize(new Dimension(80, 22));
              tfPRechercheColonnePrecedente.setMinimumSize(new Dimension(80, 22));
              tfPRechercheColonnePrecedente.setPreferredSize(new Dimension(80, 22));
              tfPRechercheColonnePrecedente.setName("tfPRechercheColonnePrecedente");
              pnlParametreGeneral.add(tfPRechercheColonnePrecedente, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPCodeCNVStandard ----
              lbPCodeCNVStandard.setText("Code CNV standard");
              lbPCodeCNVStandard.setModeReduit(true);
              lbPCodeCNVStandard.setName("lbPCodeCNVStandard");
              pnlParametreGeneral.add(lbPCodeCNVStandard, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeCNVStandard ----
              tfPCodeCNVStandard.setEnabled(false);
              tfPCodeCNVStandard.setModeReduit(true);
              tfPCodeCNVStandard.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPCodeCNVStandard.setName("tfPCodeCNVStandard");
              pnlParametreGeneral.add(tfPCodeCNVStandard, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPCodeCNVPromo ----
              lbPCodeCNVPromo.setText("Code CNV promo");
              lbPCodeCNVPromo.setModeReduit(true);
              lbPCodeCNVPromo.setName("lbPCodeCNVPromo");
              pnlParametreGeneral.add(lbPCodeCNVPromo, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeCNVPromo ----
              tfPCodeCNVPromo.setEnabled(false);
              tfPCodeCNVPromo.setModeReduit(true);
              tfPCodeCNVPromo.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPCodeCNVPromo.setName("tfPCodeCNVPromo");
              pnlParametreGeneral.add(tfPCodeCNVPromo, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPCodeCNVQuantitative ----
              lbPCodeCNVQuantitative.setText("Code CNV quantitative");
              lbPCodeCNVQuantitative.setModeReduit(true);
              lbPCodeCNVQuantitative.setName("lbPCodeCNVQuantitative");
              pnlParametreGeneral.add(lbPCodeCNVQuantitative, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeCNVQuantitative ----
              tfPCodeCNVQuantitative.setEnabled(false);
              tfPCodeCNVQuantitative.setModeReduit(true);
              tfPCodeCNVQuantitative.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPCodeCNVQuantitative.setName("tfPCodeCNVQuantitative");
              pnlParametreGeneral.add(tfPCodeCNVQuantitative, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPIgnoreCNVQuantitative ----
              lbPIgnoreCNVQuantitative.setText("Ignorer CNV quantitative");
              lbPIgnoreCNVQuantitative.setModeReduit(true);
              lbPIgnoreCNVQuantitative.setName("lbPIgnoreCNVQuantitative");
              pnlParametreGeneral.add(lbPIgnoreCNVQuantitative, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPIgnoreCNVQuantitative ----
              tfPIgnoreCNVQuantitative.setEnabled(false);
              tfPIgnoreCNVQuantitative.setModeReduit(true);
              tfPIgnoreCNVQuantitative.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPIgnoreCNVQuantitative.setName("tfPIgnoreCNVQuantitative");
              pnlParametreGeneral.add(tfPIgnoreCNVQuantitative, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPEmboitageCNVDG ----
              lbPEmboitageCNVDG.setText("Activer emboitage CNV");
              lbPEmboitageCNVDG.setModeReduit(true);
              lbPEmboitageCNVDG.setName("lbPEmboitageCNVDG");
              pnlParametreGeneral.add(lbPEmboitageCNVDG, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPEmboitageCNVDG ----
              tfPEmboitageCNVDG.setEnabled(false);
              tfPEmboitageCNVDG.setModeReduit(true);
              tfPEmboitageCNVDG.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPEmboitageCNVDG.setName("tfPEmboitageCNVDG");
              pnlParametreGeneral.add(tfPEmboitageCNVDG, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPCNVFournisseurDG ----
              lbPCNVFournisseurDG.setText("Rechercher CNV fournisseur");
              lbPCNVFournisseurDG.setModeReduit(true);
              lbPCNVFournisseurDG.setName("lbPCNVFournisseurDG");
              pnlParametreGeneral.add(lbPCNVFournisseurDG, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCNVFournisseurDG ----
              tfPCNVFournisseurDG.setEnabled(false);
              tfPCNVFournisseurDG.setModeReduit(true);
              tfPCNVFournisseurDG.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPCNVFournisseurDG.setName("tfPCNVFournisseurDG");
              pnlParametreGeneral.add(tfPCNVFournisseurDG, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPDateApplicationCNVDG ----
              lbPDateApplicationCNVDG.setText("Date application CNV");
              lbPDateApplicationCNVDG.setModeReduit(true);
              lbPDateApplicationCNVDG.setName("lbPDateApplicationCNVDG");
              pnlParametreGeneral.add(lbPDateApplicationCNVDG, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPDateApplicationCNVDG ----
              tfPDateApplicationCNVDG.setEnabled(false);
              tfPDateApplicationCNVDG.setModeReduit(true);
              tfPDateApplicationCNVDG.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPDateApplicationCNVDG.setName("tfPDateApplicationCNVDG");
              pnlParametreGeneral.add(tfPDateApplicationCNVDG, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
            }
            pnlParametrePrixVente1.add(pnlParametreGeneral, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ======== pnlParametreTarif ========
            {
              pnlParametreTarif.setBackground(new Color(238, 238, 210));
              pnlParametreTarif.setOpaque(false);
              pnlParametreTarif.setPreferredSize(new Dimension(280, 290));
              pnlParametreTarif.setMinimumSize(new Dimension(240, 290));
              pnlParametreTarif.setTitre("Param\u00e8tres tarif");
              pnlParametreTarif.setModeReduit(true);
              pnlParametreTarif.setBorder(new CompoundBorder(BorderFactory.createEmptyBorder(), new TitledBorder(null,
                  "Param\u00e8tres tarif", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 11))));
              pnlParametreTarif.setName("pnlParametreTarif");
              pnlParametreTarif.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlParametreTarif.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlParametreTarif.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlParametreTarif.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlParametreTarif.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPDateApplication ----
              lbPDateApplication.setText("Date d'application");
              lbPDateApplication.setModeReduit(true);
              lbPDateApplication.setMinimumSize(new Dimension(130, 22));
              lbPDateApplication.setPreferredSize(new Dimension(130, 22));
              lbPDateApplication.setName("lbPDateApplication");
              pnlParametreTarif.add(lbPDateApplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPDateApplication ----
              tfPDateApplication.setEnabled(false);
              tfPDateApplication.setModeReduit(true);
              tfPDateApplication.setMinimumSize(new Dimension(80, 22));
              tfPDateApplication.setPreferredSize(new Dimension(80, 22));
              tfPDateApplication.setName("tfPDateApplication");
              pnlParametreTarif.add(tfPDateApplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- snTarif ----
              snTarif.setName("snTarif");
              pnlParametreTarif.add(snTarif, new GridBagConstraints(2, 0, 1, 3, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPOrigineDateApplication ----
              lbPOrigineDateApplication.setText("Origine date");
              lbPOrigineDateApplication.setModeReduit(true);
              lbPOrigineDateApplication.setName("lbPOrigineDateApplication");
              pnlParametreTarif.add(lbPOrigineDateApplication, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPOrigineDateApplication ----
              tfPOrigineDateApplication.setEnabled(false);
              tfPOrigineDateApplication.setModeReduit(true);
              tfPOrigineDateApplication.setName("tfPOrigineDateApplication");
              pnlParametreTarif.add(tfPOrigineDateApplication, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbPCodeDeviseTarif ----
              lbPCodeDeviseTarif.setText("Code devise");
              lbPCodeDeviseTarif.setModeReduit(true);
              lbPCodeDeviseTarif.setVerticalAlignment(SwingConstants.TOP);
              lbPCodeDeviseTarif.setName("lbPCodeDeviseTarif");
              pnlParametreTarif.add(lbPCodeDeviseTarif, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeDeviseTarif ----
              tfPCodeDeviseTarif.setEnabled(false);
              tfPCodeDeviseTarif.setModeReduit(true);
              tfPCodeDeviseTarif.setName("tfPCodeDeviseTarif");
              pnlParametreTarif.add(tfPCodeDeviseTarif, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 3), 0, 0));
            }
            pnlParametrePrixVente1.add(pnlParametreTarif, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ======== pnlParametreConditionVente ========
            {
              pnlParametreConditionVente.setBackground(new Color(238, 238, 210));
              pnlParametreConditionVente.setOpaque(false);
              pnlParametreConditionVente.setPreferredSize(new Dimension(430, 480));
              pnlParametreConditionVente.setMinimumSize(new Dimension(430, 480));
              pnlParametreConditionVente.setTitre("Param\u00e8tres condition vente");
              pnlParametreConditionVente.setModeReduit(true);
              pnlParametreConditionVente.setBorder(
                  new CompoundBorder(BorderFactory.createEmptyBorder(), new TitledBorder(null, "Param\u00e8tres condition vente",
                      TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 11))));
              pnlParametreConditionVente.setName("pnlParametreConditionVente");
              pnlParametreConditionVente.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlParametreConditionVente.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlParametreConditionVente.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlParametreConditionVente.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlParametreConditionVente.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPFiltreListeCNV ----
              lbPFiltreListeCNV.setText("Filtre CNV");
              lbPFiltreListeCNV.setModeReduit(true);
              lbPFiltreListeCNV.setMinimumSize(new Dimension(130, 22));
              lbPFiltreListeCNV.setPreferredSize(new Dimension(130, 22));
              lbPFiltreListeCNV.setMaximumSize(new Dimension(130, 22));
              lbPFiltreListeCNV.setName("lbPFiltreListeCNV");
              pnlParametreConditionVente.add(lbPFiltreListeCNV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- chkFiltreListeCNV ----
              chkFiltreListeCNV.setText("Afficher toutes les conditions de vente");
              chkFiltreListeCNV.setFont(chkFiltreListeCNV.getFont().deriveFont(chkFiltreListeCNV.getFont().getSize() - 1f));
              chkFiltreListeCNV.setToolTipText(
                  "Si coch\u00e9 alors affiche toutes les conditions de vente.\nSinon affiche seulement les conditions de vente utilis\u00e9es dans le calcul du prix de vente final.");
              chkFiltreListeCNV.setName("chkFiltreListeCNV");
              pnlParametreConditionVente.add(chkFiltreListeCNV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPCodeCNV ----
              lbPCodeCNV.setText("Code CNV");
              lbPCodeCNV.setModeReduit(true);
              lbPCodeCNV.setMinimumSize(new Dimension(130, 22));
              lbPCodeCNV.setPreferredSize(new Dimension(130, 22));
              lbPCodeCNV.setMaximumSize(new Dimension(130, 22));
              lbPCodeCNV.setName("lbPCodeCNV");
              pnlParametreConditionVente.add(lbPCodeCNV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- cbListeCodeCNV ----
              cbListeCodeCNV.setModeReduit(true);
              cbListeCodeCNV.setName("cbListeCodeCNV");
              cbListeCodeCNV.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                  cbListeCodeCNVItemStateChanged(e);
                }
              });
              pnlParametreConditionVente.add(cbListeCodeCNV, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- pnlPConditionVente ----
              pnlPConditionVente.setMinimumSize(new Dimension(250, 400));
              pnlPConditionVente.setPreferredSize(new Dimension(250, 400));
              pnlPConditionVente.setName("pnlPConditionVente");
              pnlParametreConditionVente.add(pnlPConditionVente, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlParametrePrixVente1.add(pnlParametreConditionVente, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlParametrePrixVente1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlParametrePrixVente2 ========
          {
            pnlParametrePrixVente2.setMinimumSize(new Dimension(440, 1080));
            pnlParametrePrixVente2.setPreferredSize(new Dimension(440, 1080));
            pnlParametrePrixVente2.setName("pnlParametrePrixVente2");
            pnlParametrePrixVente2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlParametrePrixVente2.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlParametrePrixVente2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlParametrePrixVente2.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlParametrePrixVente2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlParametreDocumentVente ========
            {
              pnlParametreDocumentVente.setBackground(new Color(238, 238, 210));
              pnlParametreDocumentVente.setOpaque(false);
              pnlParametreDocumentVente.setPreferredSize(new Dimension(440, 620));
              pnlParametreDocumentVente.setMinimumSize(new Dimension(440, 620));
              pnlParametreDocumentVente.setTitre("Param\u00e8tres document  vente");
              pnlParametreDocumentVente.setModeReduit(true);
              pnlParametreDocumentVente.setBorder(
                  new CompoundBorder(BorderFactory.createEmptyBorder(), new TitledBorder(null, "Param\u00e8tres document  vente",
                      TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 11))));
              pnlParametreDocumentVente.setName("pnlParametreDocumentVente");
              pnlParametreDocumentVente.setLayout(new BorderLayout());
              
              // ---- pnlPDocumentVente ----
              pnlPDocumentVente.setName("pnlPDocumentVente");
              pnlParametreDocumentVente.add(pnlPDocumentVente, BorderLayout.CENTER);
            }
            pnlParametrePrixVente2.add(pnlParametreDocumentVente, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ======== pnlParametreClient ========
            {
              pnlParametreClient.setBackground(new Color(238, 238, 210));
              pnlParametreClient.setOpaque(false);
              pnlParametreClient.setPreferredSize(new Dimension(200, 255));
              pnlParametreClient.setMinimumSize(new Dimension(200, 255));
              pnlParametreClient.setTitre("Param\u00e8tres client");
              pnlParametreClient.setModeReduit(true);
              pnlParametreClient.setBorder(new CompoundBorder(BorderFactory.createEmptyBorder(), new TitledBorder(null,
                  "Param\u00e8tres client", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 11))));
              pnlParametreClient.setName("pnlParametreClient");
              pnlParametreClient.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlParametreClient.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlParametreClient.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlParametreClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlParametreClient.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPIdentifiantClient ----
              lbPIdentifiantClient.setText("Identifiant client");
              lbPIdentifiantClient.setModeReduit(true);
              lbPIdentifiantClient.setMinimumSize(new Dimension(140, 22));
              lbPIdentifiantClient.setPreferredSize(new Dimension(140, 22));
              lbPIdentifiantClient.setMaximumSize(new Dimension(140, 22));
              lbPIdentifiantClient.setName("lbPIdentifiantClient");
              pnlParametreClient.add(lbPIdentifiantClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPIdentifiantClient ----
              tfPIdentifiantClient.setEnabled(false);
              tfPIdentifiantClient.setModeReduit(true);
              tfPIdentifiantClient.setName("tfPIdentifiantClient");
              pnlParametreClient.add(tfPIdentifiantClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPCodeDeviseClient ----
              lbPCodeDeviseClient.setText("Code devise");
              lbPCodeDeviseClient.setModeReduit(true);
              lbPCodeDeviseClient.setMinimumSize(new Dimension(140, 22));
              lbPCodeDeviseClient.setPreferredSize(new Dimension(140, 22));
              lbPCodeDeviseClient.setName("lbPCodeDeviseClient");
              pnlParametreClient.add(lbPCodeDeviseClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeDeviseClient ----
              tfPCodeDeviseClient.setEnabled(false);
              tfPCodeDeviseClient.setModeReduit(true);
              tfPCodeDeviseClient.setName("tfPCodeDeviseClient");
              pnlParametreClient.add(tfPCodeDeviseClient, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPClientTTC ----
              lbPClientTTC.setText("Type de client");
              lbPClientTTC.setModeReduit(true);
              lbPClientTTC.setName("lbPClientTTC");
              pnlParametreClient.add(lbPClientTTC, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPClientTTC ----
              tfPClientTTC.setEnabled(false);
              tfPClientTTC.setModeReduit(true);
              tfPClientTTC.setName("tfPClientTTC");
              pnlParametreClient.add(tfPClientTTC, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPColonneTarifCLient ----
              lbPColonneTarifCLient.setText("Colonne tarif");
              lbPColonneTarifCLient.setModeReduit(true);
              lbPColonneTarifCLient.setName("lbPColonneTarifCLient");
              pnlParametreClient.add(lbPColonneTarifCLient, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPColonneTarifClient ----
              tfPColonneTarifClient.setEnabled(false);
              tfPColonneTarifClient.setModeReduit(true);
              tfPColonneTarifClient.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPColonneTarifClient.setName("tfPColonneTarifClient");
              pnlParametreClient.add(tfPColonneTarifClient, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPPourcentageRemiseClient ----
              lbPPourcentageRemiseClient.setText("% Remises client");
              lbPPourcentageRemiseClient.setModeReduit(true);
              lbPPourcentageRemiseClient.setPreferredSize(new Dimension(110, 22));
              lbPPourcentageRemiseClient.setMinimumSize(new Dimension(110, 22));
              lbPPourcentageRemiseClient.setName("lbPPourcentageRemiseClient");
              pnlParametreClient.add(lbPPourcentageRemiseClient, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ======== pnlPPourcentageRemiseClient ========
              {
                pnlPPourcentageRemiseClient.setMinimumSize(new Dimension(140, 22));
                pnlPPourcentageRemiseClient.setPreferredSize(new Dimension(140, 22));
                pnlPPourcentageRemiseClient.setName("pnlPPourcentageRemiseClient");
                pnlPPourcentageRemiseClient.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPPourcentageRemiseClient.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlPPourcentageRemiseClient.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPPourcentageRemiseClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPPourcentageRemiseClient.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPPourcentageRemise1Client ----
                tfPPourcentageRemise1Client.setEnabled(false);
                tfPPourcentageRemise1Client.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPPourcentageRemise1Client.setModeReduit(true);
                tfPPourcentageRemise1Client.setMaximumSize(new Dimension(40, 22));
                tfPPourcentageRemise1Client.setMinimumSize(new Dimension(40, 22));
                tfPPourcentageRemise1Client.setPreferredSize(new Dimension(40, 22));
                tfPPourcentageRemise1Client.setName("tfPPourcentageRemise1Client");
                pnlPPourcentageRemiseClient.add(tfPPourcentageRemise1Client, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPPourcentageRemise2Client ----
                tfPPourcentageRemise2Client.setEnabled(false);
                tfPPourcentageRemise2Client.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPPourcentageRemise2Client.setModeReduit(true);
                tfPPourcentageRemise2Client.setMaximumSize(new Dimension(40, 22));
                tfPPourcentageRemise2Client.setMinimumSize(new Dimension(40, 22));
                tfPPourcentageRemise2Client.setPreferredSize(new Dimension(40, 22));
                tfPPourcentageRemise2Client.setName("tfPPourcentageRemise2Client");
                pnlPPourcentageRemiseClient.add(tfPPourcentageRemise2Client, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPPourcentageRemise3Client ----
                tfPPourcentageRemise3Client.setEnabled(false);
                tfPPourcentageRemise3Client.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPPourcentageRemise3Client.setModeReduit(true);
                tfPPourcentageRemise3Client.setMaximumSize(new Dimension(40, 22));
                tfPPourcentageRemise3Client.setMinimumSize(new Dimension(40, 22));
                tfPPourcentageRemise3Client.setPreferredSize(new Dimension(40, 22));
                tfPPourcentageRemise3Client.setName("tfPPourcentageRemise3Client");
                pnlPPourcentageRemiseClient.add(tfPPourcentageRemise3Client, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlParametreClient.add(pnlPPourcentageRemiseClient, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPCodeCNVClient ----
              lbPCodeCNVClient.setText("Code CNV");
              lbPCodeCNVClient.setModeReduit(true);
              lbPCodeCNVClient.setMinimumSize(new Dimension(130, 22));
              lbPCodeCNVClient.setPreferredSize(new Dimension(130, 22));
              lbPCodeCNVClient.setMaximumSize(new Dimension(130, 22));
              lbPCodeCNVClient.setName("lbPCodeCNVClient");
              pnlParametreClient.add(lbPCodeCNVClient, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeCNVClient ----
              tfPCodeCNVClient.setEnabled(false);
              tfPCodeCNVClient.setModeReduit(true);
              tfPCodeCNVClient.setName("tfPCodeCNVClient");
              pnlParametreClient.add(tfPCodeCNVClient, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPCodeCNVPromoClient ----
              lbPCodeCNVPromoClient.setText("Code CNV promo");
              lbPCodeCNVPromoClient.setModeReduit(true);
              lbPCodeCNVPromoClient.setMinimumSize(new Dimension(130, 22));
              lbPCodeCNVPromoClient.setPreferredSize(new Dimension(130, 22));
              lbPCodeCNVPromoClient.setMaximumSize(new Dimension(130, 22));
              lbPCodeCNVPromoClient.setName("lbPCodeCNVPromoClient");
              pnlParametreClient.add(lbPCodeCNVPromoClient, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeCNVPromoClient ----
              tfPCodeCNVPromoClient.setEnabled(false);
              tfPCodeCNVPromoClient.setModeReduit(true);
              tfPCodeCNVPromoClient.setName("tfPCodeCNVPromoClient");
              pnlParametreClient.add(tfPCodeCNVPromoClient, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPNumeroCentraleAchatClient ----
              lbPNumeroCentraleAchatClient.setText("Num\u00e9ro centrales d'achat");
              lbPNumeroCentraleAchatClient.setModeReduit(true);
              lbPNumeroCentraleAchatClient.setPreferredSize(new Dimension(110, 22));
              lbPNumeroCentraleAchatClient.setMinimumSize(new Dimension(110, 22));
              lbPNumeroCentraleAchatClient.setName("lbPNumeroCentraleAchatClient");
              pnlParametreClient.add(lbPNumeroCentraleAchatClient, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ======== pnlPNumeroCentraleAchatClient ========
              {
                pnlPNumeroCentraleAchatClient.setMinimumSize(new Dimension(140, 22));
                pnlPNumeroCentraleAchatClient.setPreferredSize(new Dimension(140, 22));
                pnlPNumeroCentraleAchatClient.setName("pnlPNumeroCentraleAchatClient");
                pnlPNumeroCentraleAchatClient.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPNumeroCentraleAchatClient.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlPNumeroCentraleAchatClient.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPNumeroCentraleAchatClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPNumeroCentraleAchatClient.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPNumeroCentraleAchat1Client ----
                tfPNumeroCentraleAchat1Client.setEnabled(false);
                tfPNumeroCentraleAchat1Client.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPNumeroCentraleAchat1Client.setModeReduit(true);
                tfPNumeroCentraleAchat1Client.setMaximumSize(new Dimension(40, 22));
                tfPNumeroCentraleAchat1Client.setMinimumSize(new Dimension(40, 22));
                tfPNumeroCentraleAchat1Client.setPreferredSize(new Dimension(40, 22));
                tfPNumeroCentraleAchat1Client.setName("tfPNumeroCentraleAchat1Client");
                pnlPNumeroCentraleAchatClient.add(tfPNumeroCentraleAchat1Client, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPNumeroCentraleAchat2Client ----
                tfPNumeroCentraleAchat2Client.setEnabled(false);
                tfPNumeroCentraleAchat2Client.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPNumeroCentraleAchat2Client.setModeReduit(true);
                tfPNumeroCentraleAchat2Client.setMaximumSize(new Dimension(40, 22));
                tfPNumeroCentraleAchat2Client.setMinimumSize(new Dimension(40, 22));
                tfPNumeroCentraleAchat2Client.setPreferredSize(new Dimension(40, 22));
                tfPNumeroCentraleAchat2Client.setName("tfPNumeroCentraleAchat2Client");
                pnlPNumeroCentraleAchatClient.add(tfPNumeroCentraleAchat2Client, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPNumeroCentraleAchat3Client ----
                tfPNumeroCentraleAchat3Client.setEnabled(false);
                tfPNumeroCentraleAchat3Client.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPNumeroCentraleAchat3Client.setModeReduit(true);
                tfPNumeroCentraleAchat3Client.setMaximumSize(new Dimension(40, 22));
                tfPNumeroCentraleAchat3Client.setMinimumSize(new Dimension(40, 22));
                tfPNumeroCentraleAchat3Client.setPreferredSize(new Dimension(40, 22));
                tfPNumeroCentraleAchat3Client.setName("tfPNumeroCentraleAchat3Client");
                pnlPNumeroCentraleAchatClient.add(tfPNumeroCentraleAchat3Client, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlParametreClient.add(pnlPNumeroCentraleAchatClient, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPIgnorerCNVDGClient ----
              lbPIgnorerCNVDGClient.setText("Ignorer CNV de la DG");
              lbPIgnorerCNVDGClient.setModeReduit(true);
              lbPIgnorerCNVDGClient.setPreferredSize(new Dimension(110, 22));
              lbPIgnorerCNVDGClient.setMinimumSize(new Dimension(110, 22));
              lbPIgnorerCNVDGClient.setName("lbPIgnorerCNVDGClient");
              pnlParametreClient.add(lbPIgnorerCNVDGClient, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPIgnorerCNVDGClient ----
              tfPIgnorerCNVDGClient.setEnabled(false);
              tfPIgnorerCNVDGClient.setModeReduit(true);
              tfPIgnorerCNVDGClient.setName("tfPIgnorerCNVDGClient");
              pnlParametreClient.add(tfPIgnorerCNVDGClient, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPIgnorerCNVQuantitativeClient ----
              lbPIgnorerCNVQuantitativeClient.setText("Ignorer CNV quantitatives");
              lbPIgnorerCNVQuantitativeClient.setModeReduit(true);
              lbPIgnorerCNVQuantitativeClient.setPreferredSize(new Dimension(110, 22));
              lbPIgnorerCNVQuantitativeClient.setMinimumSize(new Dimension(110, 22));
              lbPIgnorerCNVQuantitativeClient.setName("lbPIgnorerCNVQuantitativeClient");
              pnlParametreClient.add(lbPIgnorerCNVQuantitativeClient, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPIgnorerCNVQuantitativeClient ----
              tfPIgnorerCNVQuantitativeClient.setEnabled(false);
              tfPIgnorerCNVQuantitativeClient.setModeReduit(true);
              tfPIgnorerCNVQuantitativeClient.setName("tfPIgnorerCNVQuantitativeClient");
              pnlParametreClient.add(tfPIgnorerCNVQuantitativeClient, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
                  GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlParametrePrixVente2.add(pnlParametreClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ======== pnlParametreArticle ========
            {
              pnlParametreArticle.setBackground(new Color(238, 238, 210));
              pnlParametreArticle.setOpaque(false);
              pnlParametreArticle.setPreferredSize(new Dimension(200, 210));
              pnlParametreArticle.setMinimumSize(new Dimension(200, 210));
              pnlParametreArticle.setTitre("Param\u00e8tres article");
              pnlParametreArticle.setModeReduit(true);
              pnlParametreArticle.setBorder(new CompoundBorder(BorderFactory.createEmptyBorder(), new TitledBorder(null,
                  "Param\u00e8tres article", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 11))));
              pnlParametreArticle.setName("pnlParametreArticle");
              pnlParametreArticle.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlParametreArticle.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlParametreArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlParametreArticle.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlParametreArticle.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPCodeArticle ----
              lbPCodeArticle.setText("Code article");
              lbPCodeArticle.setModeReduit(true);
              lbPCodeArticle.setMinimumSize(new Dimension(140, 22));
              lbPCodeArticle.setPreferredSize(new Dimension(140, 22));
              lbPCodeArticle.setName("lbPCodeArticle");
              pnlParametreArticle.add(lbPCodeArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPCodeArticle ----
              tfPCodeArticle.setEnabled(false);
              tfPCodeArticle.setModeReduit(true);
              tfPCodeArticle.setName("tfPCodeArticle");
              pnlParametreArticle.add(tfPCodeArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPPumpHTArticle ----
              lbPPumpHTArticle.setModeReduit(true);
              lbPPumpHTArticle.setText("Pump HTArticle");
              lbPPumpHTArticle.setMinimumSize(new Dimension(30, 22));
              lbPPumpHTArticle.setPreferredSize(new Dimension(30, 22));
              lbPPumpHTArticle.setName("lbPPumpHTArticle");
              pnlParametreArticle.add(lbPPumpHTArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPPumpHTArticle ----
              tfPPumpHTArticle.setModeReduit(true);
              tfPPumpHTArticle.setEnabled(false);
              tfPPumpHTArticle.setName("tfPPumpHTArticle");
              pnlParametreArticle.add(tfPPumpHTArticle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPPrixDeRevientHT ----
              lbPPrixDeRevientHT.setModeReduit(true);
              lbPPrixDeRevientHT.setText("PRV HT");
              lbPPrixDeRevientHT.setMinimumSize(new Dimension(30, 22));
              lbPPrixDeRevientHT.setPreferredSize(new Dimension(30, 22));
              lbPPrixDeRevientHT.setName("lbPPrixDeRevientHT");
              pnlParametreArticle.add(lbPPrixDeRevientHT, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPPrixDeRevientHT ----
              tfPPrixDeRevientHT.setModeReduit(true);
              tfPPrixDeRevientHT.setEnabled(false);
              tfPPrixDeRevientHT.setName("tfPPrixDeRevientHT");
              pnlParametreArticle.add(tfPPrixDeRevientHT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPPrixDeRevientHTStandardCNA ----
              lbPPrixDeRevientHTStandardCNA.setModeReduit(true);
              lbPPrixDeRevientHTStandardCNA.setText("PRS HT");
              lbPPrixDeRevientHTStandardCNA.setMinimumSize(new Dimension(30, 22));
              lbPPrixDeRevientHTStandardCNA.setPreferredSize(new Dimension(30, 22));
              lbPPrixDeRevientHTStandardCNA.setName("lbPPrixDeRevientHTStandardCNA");
              pnlParametreArticle.add(lbPPrixDeRevientHTStandardCNA, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPPrixDeRevientHTStandardCNA ----
              tfPPrixDeRevientHTStandardCNA.setModeReduit(true);
              tfPPrixDeRevientHTStandardCNA.setEnabled(false);
              tfPPrixDeRevientHTStandardCNA.setName("tfPPrixDeRevientHTStandardCNA");
              pnlParametreArticle.add(tfPPrixDeRevientHTStandardCNA, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPIdentifiantFournisseur ----
              lbPIdentifiantFournisseur.setText("Num\u00e9ro fournisseur");
              lbPIdentifiantFournisseur.setModeReduit(true);
              lbPIdentifiantFournisseur.setName("lbPIdentifiantFournisseur");
              pnlParametreArticle.add(lbPIdentifiantFournisseur, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPIdentifiantFournisseur ----
              tfPIdentifiantFournisseur.setEnabled(false);
              tfPIdentifiantFournisseur.setModeReduit(true);
              tfPIdentifiantFournisseur.setName("tfPIdentifiantFournisseur");
              pnlParametreArticle.add(tfPIdentifiantFournisseur, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPRattachementCNV ----
              lbPRattachementCNV.setText("Rattachements CNV");
              lbPRattachementCNV.setModeReduit(true);
              lbPRattachementCNV.setMinimumSize(new Dimension(140, 22));
              lbPRattachementCNV.setPreferredSize(new Dimension(140, 22));
              lbPRattachementCNV.setName("lbPRattachementCNV");
              pnlParametreArticle.add(lbPRattachementCNV, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ======== pnlPRattachementCNV ========
              {
                pnlPRattachementCNV.setMinimumSize(new Dimension(220, 22));
                pnlPRattachementCNV.setPreferredSize(new Dimension(220, 22));
                pnlPRattachementCNV.setName("pnlPRattachementCNV");
                pnlPRattachementCNV.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPRattachementCNV.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlPRattachementCNV.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPRattachementCNV.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPRattachementCNV.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPRattachementCNV ----
                tfPRattachementCNV.setEnabled(false);
                tfPRattachementCNV.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPRattachementCNV.setModeReduit(true);
                tfPRattachementCNV.setMaximumSize(new Dimension(50, 22));
                tfPRattachementCNV.setMinimumSize(new Dimension(50, 22));
                tfPRattachementCNV.setPreferredSize(new Dimension(50, 22));
                tfPRattachementCNV.setName("tfPRattachementCNV");
                pnlPRattachementCNV.add(tfPRattachementCNV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPRattachementCNV1 ----
                tfPRattachementCNV1.setEnabled(false);
                tfPRattachementCNV1.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPRattachementCNV1.setModeReduit(true);
                tfPRattachementCNV1.setMaximumSize(new Dimension(50, 22));
                tfPRattachementCNV1.setMinimumSize(new Dimension(50, 22));
                tfPRattachementCNV1.setPreferredSize(new Dimension(50, 22));
                tfPRattachementCNV1.setName("tfPRattachementCNV1");
                pnlPRattachementCNV.add(tfPRattachementCNV1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPRattachementCNV2 ----
                tfPRattachementCNV2.setEnabled(false);
                tfPRattachementCNV2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPRattachementCNV2.setModeReduit(true);
                tfPRattachementCNV2.setMaximumSize(new Dimension(50, 22));
                tfPRattachementCNV2.setMinimumSize(new Dimension(50, 22));
                tfPRattachementCNV2.setPreferredSize(new Dimension(50, 22));
                tfPRattachementCNV2.setName("tfPRattachementCNV2");
                pnlPRattachementCNV.add(tfPRattachementCNV2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPRattachementCNV3 ----
                tfPRattachementCNV3.setEnabled(false);
                tfPRattachementCNV3.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPRattachementCNV3.setModeReduit(true);
                tfPRattachementCNV3.setMaximumSize(new Dimension(50, 22));
                tfPRattachementCNV3.setMinimumSize(new Dimension(50, 22));
                tfPRattachementCNV3.setPreferredSize(new Dimension(50, 22));
                tfPRattachementCNV3.setName("tfPRattachementCNV3");
                pnlPRattachementCNV.add(tfPRattachementCNV3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlParametreArticle.add(pnlPRattachementCNV, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPGroupeFamille ----
              lbPGroupeFamille.setText("Groupe et famille");
              lbPGroupeFamille.setModeReduit(true);
              lbPGroupeFamille.setMinimumSize(new Dimension(140, 22));
              lbPGroupeFamille.setPreferredSize(new Dimension(140, 22));
              lbPGroupeFamille.setName("lbPGroupeFamille");
              pnlParametreArticle.add(lbPGroupeFamille, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPGroupeFamille ----
              tfPGroupeFamille.setEnabled(false);
              tfPGroupeFamille.setModeReduit(true);
              tfPGroupeFamille.setName("tfPGroupeFamille");
              pnlParametreArticle.add(tfPGroupeFamille, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              
              // ---- lbPSousFamille ----
              lbPSousFamille.setText("Sous famille");
              lbPSousFamille.setModeReduit(true);
              lbPSousFamille.setMinimumSize(new Dimension(140, 22));
              lbPSousFamille.setPreferredSize(new Dimension(140, 22));
              lbPSousFamille.setName("lbPSousFamille");
              pnlParametreArticle.add(lbPSousFamille, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPSousFamille ----
              tfPSousFamille.setEnabled(false);
              tfPSousFamille.setModeReduit(true);
              tfPSousFamille.setName("tfPSousFamille");
              pnlParametreArticle.add(tfPSousFamille, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlParametrePrixVente2.add(pnlParametreArticle, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlParametrePrixVente2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlCalcul ========
          {
            pnlCalcul.setMinimumSize(new Dimension(410, 850));
            pnlCalcul.setPreferredSize(new Dimension(430, 850));
            pnlCalcul.setOpaque(false);
            pnlCalcul.setTitre("D\u00e9tail du calcul de la CNV s\u00e9lectionn\u00e9e");
            pnlCalcul.setName("pnlCalcul");
            pnlCalcul.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCalcul.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlCalcul.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCalcul.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCalcul.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnInformationGenerale ========
            {
              pnInformationGenerale.setMinimumSize(new Dimension(320, 75));
              pnInformationGenerale.setPreferredSize(new Dimension(320, 150));
              pnInformationGenerale.setTitre("Information g\u00e9n\u00e9rale");
              pnInformationGenerale.setModeReduit(true);
              pnInformationGenerale.setName("pnInformationGenerale");
              pnInformationGenerale.setLayout(new GridBagLayout());
              ((GridBagLayout) pnInformationGenerale.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnInformationGenerale.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnInformationGenerale.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnInformationGenerale.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbCodeDevise ----
              lbCodeDevise.setText("Code devise");
              lbCodeDevise.setModeReduit(true);
              lbCodeDevise.setName("lbCodeDevise");
              pnInformationGenerale.add(lbCodeDevise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfCodeDevise ----
              tfCodeDevise.setEnabled(false);
              tfCodeDevise.setModeReduit(true);
              tfCodeDevise.setName("tfCodeDevise");
              pnInformationGenerale.add(tfCodeDevise, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbOrigineDevise ----
              lbOrigineDevise.setText("Origine devise");
              lbOrigineDevise.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOrigineDevise.setName("lbOrigineDevise");
              pnInformationGenerale.add(lbOrigineDevise, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul.add(pnInformationGenerale, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlColonneTarif ========
            {
              pnlColonneTarif.setBackground(new Color(238, 238, 210));
              pnlColonneTarif.setOpaque(false);
              pnlColonneTarif.setTitre("Colonne tarif");
              pnlColonneTarif.setModeReduit(true);
              pnlColonneTarif.setPreferredSize(new Dimension(313, 170));
              pnlColonneTarif.setName("pnlColonneTarif");
              pnlColonneTarif.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlColonneTarif.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlColonneTarif.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlColonneTarif.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlColonneTarif.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbColonneTarifSaisie ----
              lbColonneTarifSaisie.setText("Colonne saisie");
              lbColonneTarifSaisie.setModeReduit(true);
              lbColonneTarifSaisie.setName("lbColonneTarifSaisie");
              pnlColonneTarif.add(lbColonneTarifSaisie, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifSaisie ----
              tfColonneTarifSaisie.setModeReduit(true);
              tfColonneTarifSaisie.setHorizontalAlignment(SwingConstants.TRAILING);
              tfColonneTarifSaisie.setName("tfColonneTarifSaisie");
              tfColonneTarifSaisie.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                  tfColonneTarifSaisieFocusLost(e);
                }
              });
              pnlColonneTarif.add(tfColonneTarifSaisie, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarifDocumentVente ----
              lbColonneTarifDocumentVente.setText("Document vente");
              lbColonneTarifDocumentVente.setModeReduit(true);
              lbColonneTarifDocumentVente.setName("lbColonneTarifDocumentVente");
              pnlColonneTarif.add(lbColonneTarifDocumentVente, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifDocumentVente ----
              tfColonneTarifDocumentVente.setEnabled(false);
              tfColonneTarifDocumentVente.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarifDocumentVente.setModeReduit(true);
              tfColonneTarifDocumentVente.setName("tfColonneTarifDocumentVente");
              pnlColonneTarif.add(tfColonneTarifDocumentVente, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarifConditionVente ----
              lbColonneTarifConditionVente.setText("Condition de vente");
              lbColonneTarifConditionVente.setModeReduit(true);
              lbColonneTarifConditionVente.setName("lbColonneTarifConditionVente");
              pnlColonneTarif.add(lbColonneTarifConditionVente, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifConditionVente ----
              tfColonneTarifConditionVente.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarifConditionVente.setModeReduit(true);
              tfColonneTarifConditionVente.setEnabled(false);
              tfColonneTarifConditionVente.setName("tfColonneTarifConditionVente");
              pnlColonneTarif.add(tfColonneTarifConditionVente, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarifClient ----
              lbColonneTarifClient.setText("Client");
              lbColonneTarifClient.setModeReduit(true);
              lbColonneTarifClient.setName("lbColonneTarifClient");
              pnlColonneTarif.add(lbColonneTarifClient, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifClient ----
              tfColonneTarifClient.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarifClient.setEnabled(false);
              tfColonneTarifClient.setModeReduit(true);
              tfColonneTarifClient.setName("tfColonneTarifClient");
              pnlColonneTarif.add(tfColonneTarifClient, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarifPS105 ----
              lbColonneTarifPS105.setText("PS105");
              lbColonneTarifPS105.setModeReduit(true);
              lbColonneTarifPS105.setName("lbColonneTarifPS105");
              pnlColonneTarif.add(lbColonneTarifPS105, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifPS105 ----
              tfColonneTarifPS105.setEnabled(false);
              tfColonneTarifPS105.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarifPS105.setModeReduit(true);
              tfColonneTarifPS105.setName("tfColonneTarifPS105");
              pnlColonneTarif.add(tfColonneTarifPS105, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarif ----
              lbColonneTarif.setText("Colonne tarif");
              lbColonneTarif.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbColonneTarif.setName("lbColonneTarif");
              pnlColonneTarif.add(lbColonneTarif, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfColonneTarif ----
              tfColonneTarif.setEnabled(false);
              tfColonneTarif.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarif.setModeReduit(true);
              tfColonneTarif.setName("tfColonneTarif");
              pnlColonneTarif.add(tfColonneTarif, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbOrigineColonneTarif ----
              lbOrigineColonneTarif.setText("Origine colonne");
              lbOrigineColonneTarif.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOrigineColonneTarif.setName("lbOrigineColonneTarif");
              pnlColonneTarif.add(lbOrigineColonneTarif, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul.add(pnlColonneTarif, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlPrixBase ========
            {
              pnlPrixBase.setTitre("Prix de base HT");
              pnlPrixBase.setMinimumSize(new Dimension(300, 190));
              pnlPrixBase.setPreferredSize(new Dimension(422, 170));
              pnlPrixBase.setModeReduit(true);
              pnlPrixBase.setName("pnlPrixBase");
              pnlPrixBase.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrixBase.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlPrixBase.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPrixBase.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPrixBase.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPrixBaseHTSaisi ----
              lbPrixBaseHTSaisi.setText("Prix base saisi");
              lbPrixBaseHTSaisi.setModeReduit(true);
              lbPrixBaseHTSaisi.setName("lbPrixBaseHTSaisi");
              pnlPrixBase.add(lbPrixBaseHTSaisi, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTSaisi ----
              tfPrixBaseHTSaisi.setModeReduit(true);
              tfPrixBaseHTSaisi.setMinimumSize(new Dimension(80, 22));
              tfPrixBaseHTSaisi.setPreferredSize(new Dimension(80, 22));
              tfPrixBaseHTSaisi.setName("tfPrixBaseHTSaisi");
              tfPrixBaseHTSaisi.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  tfPrixBaseHTSaisiValueChanged(e);
                }
              });
              pnlPrixBase.add(tfPrixBaseHTSaisi, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixBaseHTCNV ----
              lbPrixBaseHTCNV.setText("Prix base CNV");
              lbPrixBaseHTCNV.setModeReduit(true);
              lbPrixBaseHTCNV.setName("lbPrixBaseHTCNV");
              pnlPrixBase.add(lbPrixBaseHTCNV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTCNV ----
              tfPrixBaseHTCNV.setEnabled(false);
              tfPrixBaseHTCNV.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHTCNV.setModeReduit(true);
              tfPrixBaseHTCNV.setName("tfPrixBaseHTCNV");
              pnlPrixBase.add(tfPrixBaseHTCNV, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbCodeCNVPrixBase ----
              lbCodeCNVPrixBase.setText("Code CNV");
              lbCodeCNVPrixBase.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbCodeCNVPrixBase.setName("lbCodeCNVPrixBase");
              pnlPrixBase.add(lbCodeCNVPrixBase, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPrixBaseHTPump ----
              lbPrixBaseHTPump.setText("Prix base PUMP");
              lbPrixBaseHTPump.setModeReduit(true);
              lbPrixBaseHTPump.setName("lbPrixBaseHTPump");
              pnlPrixBase.add(lbPrixBaseHTPump, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTPump ----
              tfPrixBaseHTPump.setEnabled(false);
              tfPrixBaseHTPump.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHTPump.setModeReduit(true);
              tfPrixBaseHTPump.setName("tfPrixBaseHTPump");
              pnlPrixBase.add(tfPrixBaseHTPump, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixBaseHTPrixRevient ----
              lbPrixBaseHTPrixRevient.setText("Prix base PR");
              lbPrixBaseHTPrixRevient.setModeReduit(true);
              lbPrixBaseHTPrixRevient.setName("lbPrixBaseHTPrixRevient");
              pnlPrixBase.add(lbPrixBaseHTPrixRevient, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTPrixRevient ----
              tfPrixBaseHTPrixRevient.setEnabled(false);
              tfPrixBaseHTPrixRevient.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHTPrixRevient.setModeReduit(true);
              tfPrixBaseHTPrixRevient.setName("tfPrixBaseHTPrixRevient");
              pnlPrixBase.add(tfPrixBaseHTPrixRevient, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbOriginePrixBasePrixRevient ----
              lbOriginePrixBasePrixRevient.setText("PRV ou PRS");
              lbOriginePrixBasePrixRevient.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOriginePrixBasePrixRevient.setName("lbOriginePrixBasePrixRevient");
              pnlPrixBase.add(lbOriginePrixBasePrixRevient, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPrixBaseHTColonneTarif ----
              lbPrixBaseHTColonneTarif.setText("Prix colonne 1");
              lbPrixBaseHTColonneTarif.setModeReduit(true);
              lbPrixBaseHTColonneTarif.setPreferredSize(new Dimension(110, 22));
              lbPrixBaseHTColonneTarif.setMinimumSize(new Dimension(110, 22));
              lbPrixBaseHTColonneTarif.setName("lbPrixBaseHTColonneTarif");
              pnlPrixBase.add(lbPrixBaseHTColonneTarif, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTColonneTarif ----
              tfPrixBaseHTColonneTarif.setEnabled(false);
              tfPrixBaseHTColonneTarif.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHTColonneTarif.setModeReduit(true);
              tfPrixBaseHTColonneTarif.setName("tfPrixBaseHTColonneTarif");
              pnlPrixBase.add(tfPrixBaseHTColonneTarif, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixBaseHT ----
              lbPrixBaseHT.setText("Prix base HT");
              lbPrixBaseHT.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbPrixBaseHT.setName("lbPrixBaseHT");
              pnlPrixBase.add(lbPrixBaseHT, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPrixBaseHT ----
              tfPrixBaseHT.setEnabled(false);
              tfPrixBaseHT.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHT.setModeReduit(true);
              tfPrixBaseHT.setName("tfPrixBaseHT");
              pnlPrixBase.add(tfPrixBaseHT, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbOriginePrixBaseHT ----
              lbOriginePrixBaseHT.setText("Origine prix base");
              lbOriginePrixBaseHT.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOriginePrixBaseHT.setName("lbOriginePrixBaseHT");
              pnlPrixBase.add(lbOriginePrixBaseHT, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul.add(pnlPrixBase, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlRemise ========
            {
              pnlRemise.setMinimumSize(new Dimension(400, 160));
              pnlRemise.setPreferredSize(new Dimension(350, 160));
              pnlRemise.setTitre("Pourcentage remise");
              pnlRemise.setModeReduit(true);
              pnlRemise.setName("pnlRemise");
              pnlRemise.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemise.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlRemise.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemise.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPourcentageRemiseSaisi ----
              lbPourcentageRemiseSaisi.setText("% Remise saisi");
              lbPourcentageRemiseSaisi.setModeReduit(true);
              lbPourcentageRemiseSaisi.setPreferredSize(new Dimension(110, 22));
              lbPourcentageRemiseSaisi.setMinimumSize(new Dimension(110, 22));
              lbPourcentageRemiseSaisi.setName("lbPourcentageRemiseSaisi");
              pnlRemise.add(lbPourcentageRemiseSaisi, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ======== pnlPourcentageRemiseSaisi ========
              {
                pnlPourcentageRemiseSaisi.setMinimumSize(new Dimension(100, 22));
                pnlPourcentageRemiseSaisi.setPreferredSize(new Dimension(100, 22));
                pnlPourcentageRemiseSaisi.setName("pnlPourcentageRemiseSaisi");
                pnlPourcentageRemiseSaisi.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPourcentageRemiseSaisi.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlPourcentageRemiseSaisi.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPourcentageRemiseSaisi.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPourcentageRemiseSaisi.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPourcentageRemiseSaisi ----
                tfPourcentageRemiseSaisi.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemiseSaisi.setModeReduit(true);
                tfPourcentageRemiseSaisi.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemiseSaisi.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemiseSaisi.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemiseSaisi.setName("tfPourcentageRemiseSaisi");
                tfPourcentageRemiseSaisi.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    tfPourcentageRemiseSaisiFocusLost(e);
                  }
                });
                pnlPourcentageRemiseSaisi.add(tfPourcentageRemiseSaisi, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              }
              pnlRemise.add(pnlPourcentageRemiseSaisi, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPourcentageRemiseClient ----
              lbPourcentageRemiseClient.setText("% Remises client");
              lbPourcentageRemiseClient.setModeReduit(true);
              lbPourcentageRemiseClient.setPreferredSize(new Dimension(110, 22));
              lbPourcentageRemiseClient.setMinimumSize(new Dimension(110, 22));
              lbPourcentageRemiseClient.setName("lbPourcentageRemiseClient");
              pnlRemise.add(lbPourcentageRemiseClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ======== pnlPourcentageRemiseClient ========
              {
                pnlPourcentageRemiseClient.setMinimumSize(new Dimension(140, 22));
                pnlPourcentageRemiseClient.setPreferredSize(new Dimension(140, 22));
                pnlPourcentageRemiseClient.setName("pnlPourcentageRemiseClient");
                pnlPourcentageRemiseClient.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPourcentageRemiseClient.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlPourcentageRemiseClient.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPourcentageRemiseClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPourcentageRemiseClient.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPourcentageRemise1Client ----
                tfPourcentageRemise1Client.setEnabled(false);
                tfPourcentageRemise1Client.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise1Client.setModeReduit(true);
                tfPourcentageRemise1Client.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise1Client.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise1Client.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise1Client.setName("tfPourcentageRemise1Client");
                pnlPourcentageRemiseClient.add(tfPourcentageRemise1Client, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise2Client ----
                tfPourcentageRemise2Client.setEnabled(false);
                tfPourcentageRemise2Client.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise2Client.setModeReduit(true);
                tfPourcentageRemise2Client.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise2Client.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise2Client.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise2Client.setName("tfPourcentageRemise2Client");
                pnlPourcentageRemiseClient.add(tfPourcentageRemise2Client, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise3Client ----
                tfPourcentageRemise3Client.setEnabled(false);
                tfPourcentageRemise3Client.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise3Client.setModeReduit(true);
                tfPourcentageRemise3Client.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise3Client.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise3Client.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise3Client.setName("tfPourcentageRemise3Client");
                pnlPourcentageRemiseClient.add(tfPourcentageRemise3Client, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlRemise.add(pnlPourcentageRemiseClient, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPourcentageRemiseCNV ----
              lbPourcentageRemiseCNV.setText("% Remises CNV");
              lbPourcentageRemiseCNV.setModeReduit(true);
              lbPourcentageRemiseCNV.setPreferredSize(new Dimension(110, 22));
              lbPourcentageRemiseCNV.setMinimumSize(new Dimension(110, 22));
              lbPourcentageRemiseCNV.setName("lbPourcentageRemiseCNV");
              pnlRemise.add(lbPourcentageRemiseCNV, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ======== pnlPPourcentageRemise ========
              {
                pnlPPourcentageRemise.setMinimumSize(new Dimension(270, 22));
                pnlPPourcentageRemise.setPreferredSize(new Dimension(270, 22));
                pnlPPourcentageRemise.setName("pnlPPourcentageRemise");
                pnlPPourcentageRemise.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPPourcentageRemise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlPPourcentageRemise.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPPourcentageRemise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPPourcentageRemise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPourcentageRemise1CNV ----
                tfPourcentageRemise1CNV.setEnabled(false);
                tfPourcentageRemise1CNV.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise1CNV.setModeReduit(true);
                tfPourcentageRemise1CNV.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise1CNV.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise1CNV.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise1CNV.setName("tfPourcentageRemise1CNV");
                pnlPPourcentageRemise.add(tfPourcentageRemise1CNV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise2CNV ----
                tfPourcentageRemise2CNV.setEnabled(false);
                tfPourcentageRemise2CNV.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise2CNV.setModeReduit(true);
                tfPourcentageRemise2CNV.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise2CNV.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise2CNV.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise2CNV.setName("tfPourcentageRemise2CNV");
                pnlPPourcentageRemise.add(tfPourcentageRemise2CNV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise3CNV ----
                tfPourcentageRemise3CNV.setEnabled(false);
                tfPourcentageRemise3CNV.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise3CNV.setModeReduit(true);
                tfPourcentageRemise3CNV.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise3CNV.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise3CNV.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise3CNV.setName("tfPourcentageRemise3CNV");
                pnlPPourcentageRemise.add(tfPourcentageRemise3CNV, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise4CNV ----
                tfPourcentageRemise4CNV.setEnabled(false);
                tfPourcentageRemise4CNV.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise4CNV.setModeReduit(true);
                tfPourcentageRemise4CNV.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise4CNV.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise4CNV.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise4CNV.setName("tfPourcentageRemise4CNV");
                pnlPPourcentageRemise.add(tfPourcentageRemise4CNV, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise5CNV ----
                tfPourcentageRemise5CNV.setEnabled(false);
                tfPourcentageRemise5CNV.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise5CNV.setModeReduit(true);
                tfPourcentageRemise5CNV.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise5CNV.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise5CNV.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise5CNV.setName("tfPourcentageRemise5CNV");
                pnlPPourcentageRemise.add(tfPourcentageRemise5CNV, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise6CNV ----
                tfPourcentageRemise6CNV.setEnabled(false);
                tfPourcentageRemise6CNV.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise6CNV.setModeReduit(true);
                tfPourcentageRemise6CNV.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise6CNV.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise6CNV.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise6CNV.setName("tfPourcentageRemise6CNV");
                pnlPPourcentageRemise.add(tfPourcentageRemise6CNV, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlRemise.add(pnlPPourcentageRemise, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPourcentageRemise ----
              lbPourcentageRemise.setText("Pourcentage");
              lbPourcentageRemise.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbPourcentageRemise.setName("lbPourcentageRemise");
              pnlRemise.add(lbPourcentageRemise, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ======== pnlPourcentageRemise ========
              {
                pnlPourcentageRemise.setMinimumSize(new Dimension(140, 22));
                pnlPourcentageRemise.setPreferredSize(new Dimension(140, 22));
                pnlPourcentageRemise.setName("pnlPourcentageRemise");
                pnlPourcentageRemise.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPourcentageRemise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlPourcentageRemise.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPourcentageRemise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPourcentageRemise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPourcentageRemise1 ----
                tfPourcentageRemise1.setEnabled(false);
                tfPourcentageRemise1.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise1.setModeReduit(true);
                tfPourcentageRemise1.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise1.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise1.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise1.setName("tfPourcentageRemise1");
                pnlPourcentageRemise.add(tfPourcentageRemise1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise2 ----
                tfPourcentageRemise2.setEnabled(false);
                tfPourcentageRemise2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise2.setModeReduit(true);
                tfPourcentageRemise2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise2.setName("tfPourcentageRemise2");
                pnlPourcentageRemise.add(tfPourcentageRemise2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise3 ----
                tfPourcentageRemise3.setEnabled(false);
                tfPourcentageRemise3.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise3.setModeReduit(true);
                tfPourcentageRemise3.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise3.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise3.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise3.setName("tfPourcentageRemise3");
                pnlPourcentageRemise.add(tfPourcentageRemise3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise4 ----
                tfPourcentageRemise4.setEnabled(false);
                tfPourcentageRemise4.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise4.setModeReduit(true);
                tfPourcentageRemise4.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise4.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise4.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise4.setName("tfPourcentageRemise4");
                pnlPourcentageRemise.add(tfPourcentageRemise4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise5 ----
                tfPourcentageRemise5.setEnabled(false);
                tfPourcentageRemise5.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise5.setModeReduit(true);
                tfPourcentageRemise5.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise5.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise5.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise5.setName("tfPourcentageRemise5");
                pnlPourcentageRemise.add(tfPourcentageRemise5, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise6 ----
                tfPourcentageRemise6.setEnabled(false);
                tfPourcentageRemise6.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise6.setModeReduit(true);
                tfPourcentageRemise6.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise6.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise6.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise6.setName("tfPourcentageRemise6");
                pnlPourcentageRemise.add(tfPourcentageRemise6, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlRemise.add(pnlPourcentageRemise, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbOriginePourcentageRemise ----
              lbOriginePourcentageRemise.setText("Origine remise");
              lbOriginePourcentageRemise.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOriginePourcentageRemise.setName("lbOriginePourcentageRemise");
              pnlRemise.add(lbOriginePourcentageRemise, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul.add(pnlRemise, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnPrixNet ========
            {
              pnPrixNet.setMinimumSize(new Dimension(300, 170));
              pnPrixNet.setPreferredSize(new Dimension(300, 170));
              pnPrixNet.setTitre("Prix net HT");
              pnPrixNet.setModeReduit(true);
              pnPrixNet.setName("pnPrixNet");
              pnPrixNet.setLayout(new GridBagLayout());
              ((GridBagLayout) pnPrixNet.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnPrixNet.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnPrixNet.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnPrixNet.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPrixNetHTSaisi ----
              lbPrixNetHTSaisi.setText("Prix net saisi");
              lbPrixNetHTSaisi.setModeReduit(true);
              lbPrixNetHTSaisi.setPreferredSize(new Dimension(110, 22));
              lbPrixNetHTSaisi.setMinimumSize(new Dimension(110, 22));
              lbPrixNetHTSaisi.setName("lbPrixNetHTSaisi");
              pnPrixNet.add(lbPrixNetHTSaisi, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixNetHTSaisi ----
              tfPrixNetHTSaisi.setModeReduit(true);
              tfPrixNetHTSaisi.setMinimumSize(new Dimension(80, 22));
              tfPrixNetHTSaisi.setPreferredSize(new Dimension(80, 22));
              tfPrixNetHTSaisi.setName("tfPrixNetHTSaisi");
              tfPrixNetHTSaisi.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  tfPrixNetHTSaisiValueChanged(e);
                }
              });
              pnPrixNet.add(tfPrixNetHTSaisi, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixNetHTColonneTarif ----
              lbPrixNetHTColonneTarif.setText("Prix net colonne tarif");
              lbPrixNetHTColonneTarif.setModeReduit(true);
              lbPrixNetHTColonneTarif.setPreferredSize(new Dimension(110, 22));
              lbPrixNetHTColonneTarif.setMinimumSize(new Dimension(110, 22));
              lbPrixNetHTColonneTarif.setName("lbPrixNetHTColonneTarif");
              pnPrixNet.add(lbPrixNetHTColonneTarif, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixNetHTColonneTarif ----
              tfPrixNetHTColonneTarif.setEnabled(false);
              tfPrixNetHTColonneTarif.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixNetHTColonneTarif.setModeReduit(true);
              tfPrixNetHTColonneTarif.setName("tfPrixNetHTColonneTarif");
              pnPrixNet.add(tfPrixNetHTColonneTarif, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixNetHTCalcule ----
              lbPrixNetHTCalcule.setText("Prix net calcul\u00e9");
              lbPrixNetHTCalcule.setModeReduit(true);
              lbPrixNetHTCalcule.setPreferredSize(new Dimension(110, 22));
              lbPrixNetHTCalcule.setMinimumSize(new Dimension(110, 22));
              lbPrixNetHTCalcule.setName("lbPrixNetHTCalcule");
              pnPrixNet.add(lbPrixNetHTCalcule, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixNetHTCalcule ----
              tfPrixNetHTCalcule.setEnabled(false);
              tfPrixNetHTCalcule.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixNetHTCalcule.setModeReduit(true);
              tfPrixNetHTCalcule.setName("tfPrixNetHTCalcule");
              pnPrixNet.add(tfPrixNetHTCalcule, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixNetHTCNV ----
              lbPrixNetHTCNV.setText("Prix net CNV");
              lbPrixNetHTCNV.setModeReduit(true);
              lbPrixNetHTCNV.setPreferredSize(new Dimension(110, 22));
              lbPrixNetHTCNV.setMinimumSize(new Dimension(110, 22));
              lbPrixNetHTCNV.setName("lbPrixNetHTCNV");
              pnPrixNet.add(lbPrixNetHTCNV, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixNetHTCNV ----
              tfPrixNetHTCNV.setEnabled(false);
              tfPrixNetHTCNV.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixNetHTCNV.setModeReduit(true);
              tfPrixNetHTCNV.setName("tfPrixNetHTCNV");
              pnPrixNet.add(tfPrixNetHTCNV, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbCodeCNVPrixNet ----
              lbCodeCNVPrixNet.setText("Code CNV");
              lbCodeCNVPrixNet.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbCodeCNVPrixNet.setName("lbCodeCNVPrixNet");
              pnPrixNet.add(lbCodeCNVPrixNet, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPrixNetHT ----
              lbPrixNetHT.setText("Prix net HT");
              lbPrixNetHT.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbPrixNetHT.setName("lbPrixNetHT");
              pnPrixNet.add(lbPrixNetHT, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPrixNetHT ----
              tfPrixNetHT.setEnabled(false);
              tfPrixNetHT.setModeReduit(true);
              tfPrixNetHT.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPrixNetHT.setName("tfPrixNetHT");
              pnPrixNet.add(tfPrixNetHT, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbOriginePrixNetHT ----
              lbOriginePrixNetHT.setText("Origine prix net");
              lbOriginePrixNetHT.setPreferredSize(new Dimension(100, 22));
              lbOriginePrixNetHT.setMinimumSize(new Dimension(100, 22));
              lbOriginePrixNetHT.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOriginePrixNetHT.setName("lbOriginePrixNetHT");
              pnPrixNet.add(lbOriginePrixNetHT, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul.add(pnPrixNet, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlCalcul, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlCalcul2 ========
          {
            pnlCalcul2.setMinimumSize(new Dimension(410, 800));
            pnlCalcul2.setPreferredSize(new Dimension(430, 950));
            pnlCalcul2.setOpaque(false);
            pnlCalcul2.setTitre("D\u00e9tail du calcul du prix final");
            pnlCalcul2.setName("pnlCalcul2");
            pnlCalcul2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCalcul2.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlCalcul2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCalcul2.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCalcul2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnInformationGenerale2 ========
            {
              pnInformationGenerale2.setMinimumSize(new Dimension(320, 75));
              pnInformationGenerale2.setPreferredSize(new Dimension(320, 150));
              pnInformationGenerale2.setTitre("Information g\u00e9n\u00e9rale");
              pnInformationGenerale2.setModeReduit(true);
              pnInformationGenerale2.setName("pnInformationGenerale2");
              pnInformationGenerale2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnInformationGenerale2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnInformationGenerale2.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnInformationGenerale2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnInformationGenerale2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbCodeDevise2 ----
              lbCodeDevise2.setText("Code devise");
              lbCodeDevise2.setModeReduit(true);
              lbCodeDevise2.setName("lbCodeDevise2");
              pnInformationGenerale2.add(lbCodeDevise2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfCodeDevise2 ----
              tfCodeDevise2.setEnabled(false);
              tfCodeDevise2.setModeReduit(true);
              tfCodeDevise2.setName("tfCodeDevise2");
              pnInformationGenerale2.add(tfCodeDevise2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbOrigineDevise2 ----
              lbOrigineDevise2.setText("Origine devise");
              lbOrigineDevise2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOrigineDevise2.setName("lbOrigineDevise2");
              pnInformationGenerale2.add(lbOrigineDevise2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul2.add(pnInformationGenerale2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlColonneTarif2 ========
            {
              pnlColonneTarif2.setBackground(new Color(238, 238, 210));
              pnlColonneTarif2.setOpaque(false);
              pnlColonneTarif2.setTitre("Colonne tarif");
              pnlColonneTarif2.setModeReduit(true);
              pnlColonneTarif2.setPreferredSize(new Dimension(313, 170));
              pnlColonneTarif2.setName("pnlColonneTarif2");
              pnlColonneTarif2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlColonneTarif2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlColonneTarif2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlColonneTarif2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlColonneTarif2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbColonneTarifSaisie2 ----
              lbColonneTarifSaisie2.setText("Colonne saisie");
              lbColonneTarifSaisie2.setModeReduit(true);
              lbColonneTarifSaisie2.setName("lbColonneTarifSaisie2");
              pnlColonneTarif2.add(lbColonneTarifSaisie2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifSaisie2 ----
              tfColonneTarifSaisie2.setModeReduit(true);
              tfColonneTarifSaisie2.setHorizontalAlignment(SwingConstants.TRAILING);
              tfColonneTarifSaisie2.setName("tfColonneTarifSaisie2");
              tfColonneTarifSaisie2.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                  tfColonneTarifSaisieFocusLost(e);
                }
              });
              pnlColonneTarif2.add(tfColonneTarifSaisie2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarifDocumentVente2 ----
              lbColonneTarifDocumentVente2.setText("Document vente");
              lbColonneTarifDocumentVente2.setModeReduit(true);
              lbColonneTarifDocumentVente2.setName("lbColonneTarifDocumentVente2");
              pnlColonneTarif2.add(lbColonneTarifDocumentVente2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifDocumentVente2 ----
              tfColonneTarifDocumentVente2.setEnabled(false);
              tfColonneTarifDocumentVente2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarifDocumentVente2.setModeReduit(true);
              tfColonneTarifDocumentVente2.setName("tfColonneTarifDocumentVente2");
              pnlColonneTarif2.add(tfColonneTarifDocumentVente2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarifConditionVente2 ----
              lbColonneTarifConditionVente2.setText("Condition de vente");
              lbColonneTarifConditionVente2.setModeReduit(true);
              lbColonneTarifConditionVente2.setName("lbColonneTarifConditionVente2");
              pnlColonneTarif2.add(lbColonneTarifConditionVente2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifConditionVente2 ----
              tfColonneTarifConditionVente2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarifConditionVente2.setModeReduit(true);
              tfColonneTarifConditionVente2.setEnabled(false);
              tfColonneTarifConditionVente2.setName("tfColonneTarifConditionVente2");
              pnlColonneTarif2.add(tfColonneTarifConditionVente2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarifClient2 ----
              lbColonneTarifClient2.setText("Client");
              lbColonneTarifClient2.setModeReduit(true);
              lbColonneTarifClient2.setName("lbColonneTarifClient2");
              pnlColonneTarif2.add(lbColonneTarifClient2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifClient2 ----
              tfColonneTarifClient2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarifClient2.setEnabled(false);
              tfColonneTarifClient2.setModeReduit(true);
              tfColonneTarifClient2.setName("tfColonneTarifClient2");
              pnlColonneTarif2.add(tfColonneTarifClient2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarifPS106 ----
              lbColonneTarifPS106.setText("PS105");
              lbColonneTarifPS106.setModeReduit(true);
              lbColonneTarifPS106.setName("lbColonneTarifPS106");
              pnlColonneTarif2.add(lbColonneTarifPS106, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfColonneTarifPS106 ----
              tfColonneTarifPS106.setEnabled(false);
              tfColonneTarifPS106.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarifPS106.setModeReduit(true);
              tfColonneTarifPS106.setName("tfColonneTarifPS106");
              pnlColonneTarif2.add(tfColonneTarifPS106, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbColonneTarif2 ----
              lbColonneTarif2.setText("Colonne tarif");
              lbColonneTarif2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbColonneTarif2.setName("lbColonneTarif2");
              pnlColonneTarif2.add(lbColonneTarif2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfColonneTarif2 ----
              tfColonneTarif2.setEnabled(false);
              tfColonneTarif2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfColonneTarif2.setModeReduit(true);
              tfColonneTarif2.setName("tfColonneTarif2");
              pnlColonneTarif2.add(tfColonneTarif2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbOrigineColonneTarif2 ----
              lbOrigineColonneTarif2.setText("Origine colonne");
              lbOrigineColonneTarif2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOrigineColonneTarif2.setName("lbOrigineColonneTarif2");
              pnlColonneTarif2.add(lbOrigineColonneTarif2, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul2.add(pnlColonneTarif2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlPrixBase2 ========
            {
              pnlPrixBase2.setTitre("Prix de base HT");
              pnlPrixBase2.setMinimumSize(new Dimension(300, 190));
              pnlPrixBase2.setPreferredSize(new Dimension(422, 170));
              pnlPrixBase2.setModeReduit(true);
              pnlPrixBase2.setName("pnlPrixBase2");
              pnlPrixBase2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrixBase2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlPrixBase2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPrixBase2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPrixBase2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPrixBaseHTSaisi2 ----
              lbPrixBaseHTSaisi2.setText("Prix base saisi");
              lbPrixBaseHTSaisi2.setModeReduit(true);
              lbPrixBaseHTSaisi2.setName("lbPrixBaseHTSaisi2");
              pnlPrixBase2.add(lbPrixBaseHTSaisi2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTSaisi2 ----
              tfPrixBaseHTSaisi2.setModeReduit(true);
              tfPrixBaseHTSaisi2.setMinimumSize(new Dimension(80, 22));
              tfPrixBaseHTSaisi2.setPreferredSize(new Dimension(80, 22));
              tfPrixBaseHTSaisi2.setName("tfPrixBaseHTSaisi2");
              tfPrixBaseHTSaisi2.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  tfPrixBaseHTSaisiValueChanged(e);
                }
              });
              pnlPrixBase2.add(tfPrixBaseHTSaisi2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixBaseHTCNV2 ----
              lbPrixBaseHTCNV2.setText("Prix base CNV");
              lbPrixBaseHTCNV2.setModeReduit(true);
              lbPrixBaseHTCNV2.setName("lbPrixBaseHTCNV2");
              pnlPrixBase2.add(lbPrixBaseHTCNV2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTCNV2 ----
              tfPrixBaseHTCNV2.setEnabled(false);
              tfPrixBaseHTCNV2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHTCNV2.setModeReduit(true);
              tfPrixBaseHTCNV2.setName("tfPrixBaseHTCNV2");
              pnlPrixBase2.add(tfPrixBaseHTCNV2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbCodeCNVPrixBase2 ----
              lbCodeCNVPrixBase2.setText("Code CNV");
              lbCodeCNVPrixBase2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbCodeCNVPrixBase2.setName("lbCodeCNVPrixBase2");
              pnlPrixBase2.add(lbCodeCNVPrixBase2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPrixBaseHTPump2 ----
              lbPrixBaseHTPump2.setText("Prix base PUMP");
              lbPrixBaseHTPump2.setModeReduit(true);
              lbPrixBaseHTPump2.setName("lbPrixBaseHTPump2");
              pnlPrixBase2.add(lbPrixBaseHTPump2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTPump2 ----
              tfPrixBaseHTPump2.setEnabled(false);
              tfPrixBaseHTPump2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHTPump2.setModeReduit(true);
              tfPrixBaseHTPump2.setName("tfPrixBaseHTPump2");
              pnlPrixBase2.add(tfPrixBaseHTPump2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixBaseHTPrixRevient2 ----
              lbPrixBaseHTPrixRevient2.setText("Prix base PR");
              lbPrixBaseHTPrixRevient2.setModeReduit(true);
              lbPrixBaseHTPrixRevient2.setName("lbPrixBaseHTPrixRevient2");
              pnlPrixBase2.add(lbPrixBaseHTPrixRevient2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTPrixRevient2 ----
              tfPrixBaseHTPrixRevient2.setEnabled(false);
              tfPrixBaseHTPrixRevient2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHTPrixRevient2.setModeReduit(true);
              tfPrixBaseHTPrixRevient2.setName("tfPrixBaseHTPrixRevient2");
              pnlPrixBase2.add(tfPrixBaseHTPrixRevient2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbOriginePrixBasePrixRevient2 ----
              lbOriginePrixBasePrixRevient2.setText("PRV ou PRS");
              lbOriginePrixBasePrixRevient2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOriginePrixBasePrixRevient2.setName("lbOriginePrixBasePrixRevient2");
              pnlPrixBase2.add(lbOriginePrixBasePrixRevient2, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPrixBaseHTColonneTarif2 ----
              lbPrixBaseHTColonneTarif2.setText("Prix colonne 1");
              lbPrixBaseHTColonneTarif2.setModeReduit(true);
              lbPrixBaseHTColonneTarif2.setPreferredSize(new Dimension(110, 22));
              lbPrixBaseHTColonneTarif2.setMinimumSize(new Dimension(110, 22));
              lbPrixBaseHTColonneTarif2.setName("lbPrixBaseHTColonneTarif2");
              pnlPrixBase2.add(lbPrixBaseHTColonneTarif2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixBaseHTColonneTarif2 ----
              tfPrixBaseHTColonneTarif2.setEnabled(false);
              tfPrixBaseHTColonneTarif2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHTColonneTarif2.setModeReduit(true);
              tfPrixBaseHTColonneTarif2.setName("tfPrixBaseHTColonneTarif2");
              pnlPrixBase2.add(tfPrixBaseHTColonneTarif2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixBaseHT2 ----
              lbPrixBaseHT2.setText("Prix base HT");
              lbPrixBaseHT2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbPrixBaseHT2.setName("lbPrixBaseHT2");
              pnlPrixBase2.add(lbPrixBaseHT2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPrixBaseHT2 ----
              tfPrixBaseHT2.setEnabled(false);
              tfPrixBaseHT2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixBaseHT2.setModeReduit(true);
              tfPrixBaseHT2.setName("tfPrixBaseHT2");
              pnlPrixBase2.add(tfPrixBaseHT2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbOriginePrixBaseHT2 ----
              lbOriginePrixBaseHT2.setText("Origine prix base");
              lbOriginePrixBaseHT2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOriginePrixBaseHT2.setName("lbOriginePrixBaseHT2");
              pnlPrixBase2.add(lbOriginePrixBaseHT2, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul2.add(pnlPrixBase2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlRemise2 ========
            {
              pnlRemise2.setMinimumSize(new Dimension(400, 160));
              pnlRemise2.setPreferredSize(new Dimension(350, 160));
              pnlRemise2.setTitre("Pourcentage remise");
              pnlRemise2.setModeReduit(true);
              pnlRemise2.setName("pnlRemise2");
              pnlRemise2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemise2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlRemise2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemise2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemise2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPourcentageRemiseSaisi2 ----
              lbPourcentageRemiseSaisi2.setText("% Remise saisi");
              lbPourcentageRemiseSaisi2.setModeReduit(true);
              lbPourcentageRemiseSaisi2.setPreferredSize(new Dimension(110, 22));
              lbPourcentageRemiseSaisi2.setMinimumSize(new Dimension(110, 22));
              lbPourcentageRemiseSaisi2.setName("lbPourcentageRemiseSaisi2");
              pnlRemise2.add(lbPourcentageRemiseSaisi2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ======== pnlPourcentageRemiseSaisi2 ========
              {
                pnlPourcentageRemiseSaisi2.setMinimumSize(new Dimension(100, 22));
                pnlPourcentageRemiseSaisi2.setPreferredSize(new Dimension(100, 22));
                pnlPourcentageRemiseSaisi2.setName("pnlPourcentageRemiseSaisi2");
                pnlPourcentageRemiseSaisi2.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPourcentageRemiseSaisi2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlPourcentageRemiseSaisi2.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPourcentageRemiseSaisi2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPourcentageRemiseSaisi2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPourcentageRemiseSaisi2 ----
                tfPourcentageRemiseSaisi2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemiseSaisi2.setModeReduit(true);
                tfPourcentageRemiseSaisi2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemiseSaisi2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemiseSaisi2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemiseSaisi2.setName("tfPourcentageRemiseSaisi2");
                tfPourcentageRemiseSaisi2.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    tfPourcentageRemiseSaisiFocusLost(e);
                  }
                });
                pnlPourcentageRemiseSaisi2.add(tfPourcentageRemiseSaisi2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              }
              pnlRemise2.add(pnlPourcentageRemiseSaisi2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPourcentageRemiseClient2 ----
              lbPourcentageRemiseClient2.setText("% Remises client");
              lbPourcentageRemiseClient2.setModeReduit(true);
              lbPourcentageRemiseClient2.setPreferredSize(new Dimension(110, 22));
              lbPourcentageRemiseClient2.setMinimumSize(new Dimension(110, 22));
              lbPourcentageRemiseClient2.setName("lbPourcentageRemiseClient2");
              pnlRemise2.add(lbPourcentageRemiseClient2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ======== pnlPourcentageRemiseClient2 ========
              {
                pnlPourcentageRemiseClient2.setMinimumSize(new Dimension(140, 22));
                pnlPourcentageRemiseClient2.setPreferredSize(new Dimension(140, 22));
                pnlPourcentageRemiseClient2.setName("pnlPourcentageRemiseClient2");
                pnlPourcentageRemiseClient2.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPourcentageRemiseClient2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlPourcentageRemiseClient2.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPourcentageRemiseClient2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPourcentageRemiseClient2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPourcentageRemise1Client2 ----
                tfPourcentageRemise1Client2.setEnabled(false);
                tfPourcentageRemise1Client2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise1Client2.setModeReduit(true);
                tfPourcentageRemise1Client2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise1Client2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise1Client2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise1Client2.setName("tfPourcentageRemise1Client2");
                pnlPourcentageRemiseClient2.add(tfPourcentageRemise1Client2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise2Client2 ----
                tfPourcentageRemise2Client2.setEnabled(false);
                tfPourcentageRemise2Client2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise2Client2.setModeReduit(true);
                tfPourcentageRemise2Client2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise2Client2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise2Client2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise2Client2.setName("tfPourcentageRemise2Client2");
                pnlPourcentageRemiseClient2.add(tfPourcentageRemise2Client2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise3Client2 ----
                tfPourcentageRemise3Client2.setEnabled(false);
                tfPourcentageRemise3Client2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise3Client2.setModeReduit(true);
                tfPourcentageRemise3Client2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise3Client2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise3Client2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise3Client2.setName("tfPourcentageRemise3Client2");
                pnlPourcentageRemiseClient2.add(tfPourcentageRemise3Client2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlRemise2.add(pnlPourcentageRemiseClient2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPourcentageRemiseCNV2 ----
              lbPourcentageRemiseCNV2.setText("% Remises CNV");
              lbPourcentageRemiseCNV2.setModeReduit(true);
              lbPourcentageRemiseCNV2.setPreferredSize(new Dimension(110, 22));
              lbPourcentageRemiseCNV2.setMinimumSize(new Dimension(110, 22));
              lbPourcentageRemiseCNV2.setName("lbPourcentageRemiseCNV2");
              pnlRemise2.add(lbPourcentageRemiseCNV2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ======== pnlPPourcentageRemise2 ========
              {
                pnlPPourcentageRemise2.setMinimumSize(new Dimension(270, 22));
                pnlPPourcentageRemise2.setPreferredSize(new Dimension(270, 22));
                pnlPPourcentageRemise2.setName("pnlPPourcentageRemise2");
                pnlPPourcentageRemise2.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPPourcentageRemise2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlPPourcentageRemise2.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPPourcentageRemise2.getLayout()).columnWeights =
                    new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPPourcentageRemise2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPourcentageRemise1CNV2 ----
                tfPourcentageRemise1CNV2.setEnabled(false);
                tfPourcentageRemise1CNV2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise1CNV2.setModeReduit(true);
                tfPourcentageRemise1CNV2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise1CNV2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise1CNV2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise1CNV2.setName("tfPourcentageRemise1CNV2");
                pnlPPourcentageRemise2.add(tfPourcentageRemise1CNV2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise2CNV2 ----
                tfPourcentageRemise2CNV2.setEnabled(false);
                tfPourcentageRemise2CNV2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise2CNV2.setModeReduit(true);
                tfPourcentageRemise2CNV2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise2CNV2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise2CNV2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise2CNV2.setName("tfPourcentageRemise2CNV2");
                pnlPPourcentageRemise2.add(tfPourcentageRemise2CNV2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise3CNV2 ----
                tfPourcentageRemise3CNV2.setEnabled(false);
                tfPourcentageRemise3CNV2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise3CNV2.setModeReduit(true);
                tfPourcentageRemise3CNV2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise3CNV2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise3CNV2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise3CNV2.setName("tfPourcentageRemise3CNV2");
                pnlPPourcentageRemise2.add(tfPourcentageRemise3CNV2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise4CNV2 ----
                tfPourcentageRemise4CNV2.setEnabled(false);
                tfPourcentageRemise4CNV2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise4CNV2.setModeReduit(true);
                tfPourcentageRemise4CNV2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise4CNV2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise4CNV2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise4CNV2.setName("tfPourcentageRemise4CNV2");
                pnlPPourcentageRemise2.add(tfPourcentageRemise4CNV2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise5CNV2 ----
                tfPourcentageRemise5CNV2.setEnabled(false);
                tfPourcentageRemise5CNV2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise5CNV2.setModeReduit(true);
                tfPourcentageRemise5CNV2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise5CNV2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise5CNV2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise5CNV2.setName("tfPourcentageRemise5CNV2");
                pnlPPourcentageRemise2.add(tfPourcentageRemise5CNV2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise6CNV2 ----
                tfPourcentageRemise6CNV2.setEnabled(false);
                tfPourcentageRemise6CNV2.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise6CNV2.setModeReduit(true);
                tfPourcentageRemise6CNV2.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise6CNV2.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise6CNV2.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise6CNV2.setName("tfPourcentageRemise6CNV2");
                pnlPPourcentageRemise2.add(tfPourcentageRemise6CNV2, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlRemise2.add(pnlPPourcentageRemise2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPourcentageRemise2 ----
              lbPourcentageRemise2.setText("Pourcentage");
              lbPourcentageRemise2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbPourcentageRemise2.setName("lbPourcentageRemise2");
              pnlRemise2.add(lbPourcentageRemise2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ======== pnlPourcentageRemise2 ========
              {
                pnlPourcentageRemise2.setMinimumSize(new Dimension(140, 22));
                pnlPourcentageRemise2.setPreferredSize(new Dimension(140, 22));
                pnlPourcentageRemise2.setName("pnlPourcentageRemise2");
                pnlPourcentageRemise2.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPourcentageRemise2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlPourcentageRemise2.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPourcentageRemise2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPourcentageRemise2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPourcentageRemise7 ----
                tfPourcentageRemise7.setEnabled(false);
                tfPourcentageRemise7.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise7.setModeReduit(true);
                tfPourcentageRemise7.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise7.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise7.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise7.setName("tfPourcentageRemise7");
                pnlPourcentageRemise2.add(tfPourcentageRemise7, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise8 ----
                tfPourcentageRemise8.setEnabled(false);
                tfPourcentageRemise8.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise8.setModeReduit(true);
                tfPourcentageRemise8.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise8.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise8.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise8.setName("tfPourcentageRemise8");
                pnlPourcentageRemise2.add(tfPourcentageRemise8, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise9 ----
                tfPourcentageRemise9.setEnabled(false);
                tfPourcentageRemise9.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise9.setModeReduit(true);
                tfPourcentageRemise9.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise9.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise9.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise9.setName("tfPourcentageRemise9");
                pnlPourcentageRemise2.add(tfPourcentageRemise9, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise10 ----
                tfPourcentageRemise10.setEnabled(false);
                tfPourcentageRemise10.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise10.setModeReduit(true);
                tfPourcentageRemise10.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise10.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise10.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise10.setName("tfPourcentageRemise10");
                pnlPourcentageRemise2.add(tfPourcentageRemise10, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise11 ----
                tfPourcentageRemise11.setEnabled(false);
                tfPourcentageRemise11.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise11.setModeReduit(true);
                tfPourcentageRemise11.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise11.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise11.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise11.setName("tfPourcentageRemise11");
                pnlPourcentageRemise2.add(tfPourcentageRemise11, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPourcentageRemise12 ----
                tfPourcentageRemise12.setEnabled(false);
                tfPourcentageRemise12.setHorizontalAlignment(SwingConstants.RIGHT);
                tfPourcentageRemise12.setModeReduit(true);
                tfPourcentageRemise12.setMaximumSize(new Dimension(40, 22));
                tfPourcentageRemise12.setMinimumSize(new Dimension(40, 22));
                tfPourcentageRemise12.setPreferredSize(new Dimension(40, 22));
                tfPourcentageRemise12.setName("tfPourcentageRemise12");
                pnlPourcentageRemise2.add(tfPourcentageRemise12, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlRemise2.add(pnlPourcentageRemise2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbOriginePourcentageRemise2 ----
              lbOriginePourcentageRemise2.setText("Origine remise");
              lbOriginePourcentageRemise2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOriginePourcentageRemise2.setName("lbOriginePourcentageRemise2");
              pnlRemise2.add(lbOriginePourcentageRemise2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul2.add(pnlRemise2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnPrixNet2 ========
            {
              pnPrixNet2.setMinimumSize(new Dimension(300, 170));
              pnPrixNet2.setPreferredSize(new Dimension(300, 170));
              pnPrixNet2.setTitre("Prix net HT");
              pnPrixNet2.setModeReduit(true);
              pnPrixNet2.setName("pnPrixNet2");
              pnPrixNet2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnPrixNet2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnPrixNet2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnPrixNet2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnPrixNet2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPrixNetHTSaisi2 ----
              lbPrixNetHTSaisi2.setText("Prix net saisi");
              lbPrixNetHTSaisi2.setModeReduit(true);
              lbPrixNetHTSaisi2.setPreferredSize(new Dimension(110, 22));
              lbPrixNetHTSaisi2.setMinimumSize(new Dimension(110, 22));
              lbPrixNetHTSaisi2.setName("lbPrixNetHTSaisi2");
              pnPrixNet2.add(lbPrixNetHTSaisi2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixNetHTSaisi2 ----
              tfPrixNetHTSaisi2.setModeReduit(true);
              tfPrixNetHTSaisi2.setMinimumSize(new Dimension(80, 22));
              tfPrixNetHTSaisi2.setPreferredSize(new Dimension(80, 22));
              tfPrixNetHTSaisi2.setName("tfPrixNetHTSaisi2");
              tfPrixNetHTSaisi2.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  tfPrixNetHTSaisiValueChanged(e);
                }
              });
              pnPrixNet2.add(tfPrixNetHTSaisi2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixNetHTColonneTarif2 ----
              lbPrixNetHTColonneTarif2.setText("Prix net colonne tarif");
              lbPrixNetHTColonneTarif2.setModeReduit(true);
              lbPrixNetHTColonneTarif2.setPreferredSize(new Dimension(110, 22));
              lbPrixNetHTColonneTarif2.setMinimumSize(new Dimension(110, 22));
              lbPrixNetHTColonneTarif2.setName("lbPrixNetHTColonneTarif2");
              pnPrixNet2.add(lbPrixNetHTColonneTarif2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixNetHTColonneTarif2 ----
              tfPrixNetHTColonneTarif2.setEnabled(false);
              tfPrixNetHTColonneTarif2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixNetHTColonneTarif2.setModeReduit(true);
              tfPrixNetHTColonneTarif2.setName("tfPrixNetHTColonneTarif2");
              pnPrixNet2.add(tfPrixNetHTColonneTarif2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixNetHTCalcule2 ----
              lbPrixNetHTCalcule2.setText("Prix net calcul\u00e9");
              lbPrixNetHTCalcule2.setModeReduit(true);
              lbPrixNetHTCalcule2.setPreferredSize(new Dimension(110, 22));
              lbPrixNetHTCalcule2.setMinimumSize(new Dimension(110, 22));
              lbPrixNetHTCalcule2.setName("lbPrixNetHTCalcule2");
              pnPrixNet2.add(lbPrixNetHTCalcule2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixNetHTCalcule2 ----
              tfPrixNetHTCalcule2.setEnabled(false);
              tfPrixNetHTCalcule2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixNetHTCalcule2.setModeReduit(true);
              tfPrixNetHTCalcule2.setName("tfPrixNetHTCalcule2");
              pnPrixNet2.add(tfPrixNetHTCalcule2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbPrixNetHTCNV2 ----
              lbPrixNetHTCNV2.setText("Prix net CNV");
              lbPrixNetHTCNV2.setModeReduit(true);
              lbPrixNetHTCNV2.setPreferredSize(new Dimension(110, 22));
              lbPrixNetHTCNV2.setMinimumSize(new Dimension(110, 22));
              lbPrixNetHTCNV2.setName("lbPrixNetHTCNV2");
              pnPrixNet2.add(lbPrixNetHTCNV2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- tfPrixNetHTCNV2 ----
              tfPrixNetHTCNV2.setEnabled(false);
              tfPrixNetHTCNV2.setHorizontalAlignment(SwingConstants.RIGHT);
              tfPrixNetHTCNV2.setModeReduit(true);
              tfPrixNetHTCNV2.setName("tfPrixNetHTCNV2");
              pnPrixNet2.add(tfPrixNetHTCNV2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 3), 0, 0));
              
              // ---- lbCodeCNVPrixNet2 ----
              lbCodeCNVPrixNet2.setText("Code CNV");
              lbCodeCNVPrixNet2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbCodeCNVPrixNet2.setName("lbCodeCNVPrixNet2");
              pnPrixNet2.add(lbCodeCNVPrixNet2, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 3, 0), 0, 0));
              
              // ---- lbPrixNetHT2 ----
              lbPrixNetHT2.setText("Prix net HT");
              lbPrixNetHT2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbPrixNetHT2.setName("lbPrixNetHT2");
              pnPrixNet2.add(lbPrixNetHT2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- tfPrixNetHT2 ----
              tfPrixNetHT2.setEnabled(false);
              tfPrixNetHT2.setModeReduit(true);
              tfPrixNetHT2.setHorizontalAlignment(SwingConstants.TRAILING);
              tfPrixNetHT2.setName("tfPrixNetHT2");
              pnPrixNet2.add(tfPrixNetHT2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
              
              // ---- lbOriginePrixNetHT2 ----
              lbOriginePrixNetHT2.setText("Origine prix net");
              lbOriginePrixNetHT2.setPreferredSize(new Dimension(100, 22));
              lbOriginePrixNetHT2.setMinimumSize(new Dimension(100, 22));
              lbOriginePrixNetHT2.setFont(new Font("sansserif", Font.PLAIN, 11));
              lbOriginePrixNetHT2.setName("lbOriginePrixNetHT2");
              pnPrixNet2.add(lbOriginePrixNetHT2, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCalcul2.add(pnPrixNet2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlContenu.add(pnlCalcul2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        scrpContenu.setViewportView(pnlContenu);
      }
      pnlPrincipal.add(scrpContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private JScrollPane scrpContenu;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlParametrePrixVente1;
  private SNPanelTitre pnlParametreGeneral;
  private SNLabelChamp lbPBaseDeDonnees;
  private SNTexte tfPBaseDeDonnees;
  private SNLabelChamp lbPCodeDeviseDG;
  private SNTexte tfPCodeDeviseDG;
  private SNLabelChamp lbPModeNegoce;
  private SNTexte tfPModeNegoce;
  private SNLabelChamp lbPMoisEnCoursDG;
  private SNTexte tfPMoisEnCoursDG;
  private SNLabelChamp lbPCodeEtablissement;
  private SNTexte tfPCodeEtablissement;
  private SNLabelChamp lbPCalculPrixNetCol1DG;
  private SNTexte tfPCalculPrixNetCol1DG;
  private SNLabelChamp lbPColonnePS105;
  private SNTexte tfPColonnePS105;
  private SNLabelChamp lbPRechercheColonnePrecedente;
  private SNTexte tfPRechercheColonnePrecedente;
  private SNLabelChamp lbPCodeCNVStandard;
  private SNTexte tfPCodeCNVStandard;
  private SNLabelChamp lbPCodeCNVPromo;
  private SNTexte tfPCodeCNVPromo;
  private SNLabelChamp lbPCodeCNVQuantitative;
  private SNTexte tfPCodeCNVQuantitative;
  private SNLabelChamp lbPIgnoreCNVQuantitative;
  private SNTexte tfPIgnoreCNVQuantitative;
  private SNLabelChamp lbPEmboitageCNVDG;
  private SNTexte tfPEmboitageCNVDG;
  private SNLabelChamp lbPCNVFournisseurDG;
  private SNTexte tfPCNVFournisseurDG;
  private SNLabelChamp lbPDateApplicationCNVDG;
  private SNTexte tfPDateApplicationCNVDG;
  private SNPanelTitre pnlParametreTarif;
  private SNLabelChamp lbPDateApplication;
  private SNTexte tfPDateApplication;
  private SNPTarif snTarif;
  private SNLabelChamp lbPOrigineDateApplication;
  private SNTexte tfPOrigineDateApplication;
  private SNLabelChamp lbPCodeDeviseTarif;
  private SNTexte tfPCodeDeviseTarif;
  private SNPanelTitre pnlParametreConditionVente;
  private SNLabelChamp lbPFiltreListeCNV;
  private JCheckBox chkFiltreListeCNV;
  private SNLabelChamp lbPCodeCNV;
  private SNComboBox cbListeCodeCNV;
  private SNPConditionVente pnlPConditionVente;
  private SNPanel pnlParametrePrixVente2;
  private SNPanelTitre pnlParametreDocumentVente;
  private SNPDocumentVente pnlPDocumentVente;
  private SNPanelTitre pnlParametreClient;
  private SNLabelChamp lbPIdentifiantClient;
  private SNTexte tfPIdentifiantClient;
  private SNLabelChamp lbPCodeDeviseClient;
  private SNTexte tfPCodeDeviseClient;
  private SNLabelChamp lbPClientTTC;
  private SNTexte tfPClientTTC;
  private SNLabelChamp lbPColonneTarifCLient;
  private SNTexte tfPColonneTarifClient;
  private SNLabelChamp lbPPourcentageRemiseClient;
  private SNPanel pnlPPourcentageRemiseClient;
  private SNTexte tfPPourcentageRemise1Client;
  private SNTexte tfPPourcentageRemise2Client;
  private SNTexte tfPPourcentageRemise3Client;
  private SNLabelChamp lbPCodeCNVClient;
  private SNTexte tfPCodeCNVClient;
  private SNLabelChamp lbPCodeCNVPromoClient;
  private SNTexte tfPCodeCNVPromoClient;
  private SNLabelChamp lbPNumeroCentraleAchatClient;
  private SNPanel pnlPNumeroCentraleAchatClient;
  private SNTexte tfPNumeroCentraleAchat1Client;
  private SNTexte tfPNumeroCentraleAchat2Client;
  private SNTexte tfPNumeroCentraleAchat3Client;
  private SNLabelChamp lbPIgnorerCNVDGClient;
  private SNTexte tfPIgnorerCNVDGClient;
  private SNLabelChamp lbPIgnorerCNVQuantitativeClient;
  private SNTexte tfPIgnorerCNVQuantitativeClient;
  private SNPanelTitre pnlParametreArticle;
  private SNLabelChamp lbPCodeArticle;
  private SNTexte tfPCodeArticle;
  private SNLabelChamp lbPPumpHTArticle;
  private SNMontant tfPPumpHTArticle;
  private SNLabelChamp lbPPrixDeRevientHT;
  private SNMontant tfPPrixDeRevientHT;
  private SNLabelChamp lbPPrixDeRevientHTStandardCNA;
  private SNMontant tfPPrixDeRevientHTStandardCNA;
  private SNLabelChamp lbPIdentifiantFournisseur;
  private SNTexte tfPIdentifiantFournisseur;
  private SNLabelChamp lbPRattachementCNV;
  private SNPanel pnlPRattachementCNV;
  private SNTexte tfPRattachementCNV;
  private SNTexte tfPRattachementCNV1;
  private SNTexte tfPRattachementCNV2;
  private SNTexte tfPRattachementCNV3;
  private SNLabelChamp lbPGroupeFamille;
  private SNTexte tfPGroupeFamille;
  private SNLabelChamp lbPSousFamille;
  private SNTexte tfPSousFamille;
  private SNPanelTitre pnlCalcul;
  private SNPanelTitre pnInformationGenerale;
  private SNLabelChamp lbCodeDevise;
  private SNTexte tfCodeDevise;
  private SNMessage lbOrigineDevise;
  private SNPanelTitre pnlColonneTarif;
  private SNLabelChamp lbColonneTarifSaisie;
  private SNTexte tfColonneTarifSaisie;
  private SNLabelChamp lbColonneTarifDocumentVente;
  private SNTexte tfColonneTarifDocumentVente;
  private SNLabelChamp lbColonneTarifConditionVente;
  private SNTexte tfColonneTarifConditionVente;
  private SNLabelChamp lbColonneTarifClient;
  private SNTexte tfColonneTarifClient;
  private SNLabelChamp lbColonneTarifPS105;
  private SNTexte tfColonneTarifPS105;
  private SNMessage lbColonneTarif;
  private SNTexte tfColonneTarif;
  private SNMessage lbOrigineColonneTarif;
  private SNPanelTitre pnlPrixBase;
  private SNLabelChamp lbPrixBaseHTSaisi;
  private SNMontant tfPrixBaseHTSaisi;
  private SNLabelChamp lbPrixBaseHTCNV;
  private SNTexte tfPrixBaseHTCNV;
  private SNMessage lbCodeCNVPrixBase;
  private SNLabelChamp lbPrixBaseHTPump;
  private SNTexte tfPrixBaseHTPump;
  private SNLabelChamp lbPrixBaseHTPrixRevient;
  private SNTexte tfPrixBaseHTPrixRevient;
  private SNMessage lbOriginePrixBasePrixRevient;
  private SNLabelChamp lbPrixBaseHTColonneTarif;
  private SNTexte tfPrixBaseHTColonneTarif;
  private SNMessage lbPrixBaseHT;
  private SNTexte tfPrixBaseHT;
  private SNMessage lbOriginePrixBaseHT;
  private SNPanelTitre pnlRemise;
  private SNLabelChamp lbPourcentageRemiseSaisi;
  private SNPanel pnlPourcentageRemiseSaisi;
  private SNTexte tfPourcentageRemiseSaisi;
  private SNLabelChamp lbPourcentageRemiseClient;
  private SNPanel pnlPourcentageRemiseClient;
  private SNTexte tfPourcentageRemise1Client;
  private SNTexte tfPourcentageRemise2Client;
  private SNTexte tfPourcentageRemise3Client;
  private SNLabelChamp lbPourcentageRemiseCNV;
  private SNPanel pnlPPourcentageRemise;
  private SNTexte tfPourcentageRemise1CNV;
  private SNTexte tfPourcentageRemise2CNV;
  private SNTexte tfPourcentageRemise3CNV;
  private SNTexte tfPourcentageRemise4CNV;
  private SNTexte tfPourcentageRemise5CNV;
  private SNTexte tfPourcentageRemise6CNV;
  private SNMessage lbPourcentageRemise;
  private SNPanel pnlPourcentageRemise;
  private SNTexte tfPourcentageRemise1;
  private SNTexte tfPourcentageRemise2;
  private SNTexte tfPourcentageRemise3;
  private SNTexte tfPourcentageRemise4;
  private SNTexte tfPourcentageRemise5;
  private SNTexte tfPourcentageRemise6;
  private SNMessage lbOriginePourcentageRemise;
  private SNPanelTitre pnPrixNet;
  private SNLabelChamp lbPrixNetHTSaisi;
  private SNMontant tfPrixNetHTSaisi;
  private SNLabelChamp lbPrixNetHTColonneTarif;
  private SNTexte tfPrixNetHTColonneTarif;
  private SNLabelChamp lbPrixNetHTCalcule;
  private SNTexte tfPrixNetHTCalcule;
  private SNLabelChamp lbPrixNetHTCNV;
  private SNTexte tfPrixNetHTCNV;
  private SNMessage lbCodeCNVPrixNet;
  private SNMessage lbPrixNetHT;
  private SNTexte tfPrixNetHT;
  private SNMessage lbOriginePrixNetHT;
  private SNPanelTitre pnlCalcul2;
  private SNPanelTitre pnInformationGenerale2;
  private SNLabelChamp lbCodeDevise2;
  private SNTexte tfCodeDevise2;
  private SNMessage lbOrigineDevise2;
  private SNPanelTitre pnlColonneTarif2;
  private SNLabelChamp lbColonneTarifSaisie2;
  private SNTexte tfColonneTarifSaisie2;
  private SNLabelChamp lbColonneTarifDocumentVente2;
  private SNTexte tfColonneTarifDocumentVente2;
  private SNLabelChamp lbColonneTarifConditionVente2;
  private SNTexte tfColonneTarifConditionVente2;
  private SNLabelChamp lbColonneTarifClient2;
  private SNTexte tfColonneTarifClient2;
  private SNLabelChamp lbColonneTarifPS106;
  private SNTexte tfColonneTarifPS106;
  private SNMessage lbColonneTarif2;
  private SNTexte tfColonneTarif2;
  private SNMessage lbOrigineColonneTarif2;
  private SNPanelTitre pnlPrixBase2;
  private SNLabelChamp lbPrixBaseHTSaisi2;
  private SNMontant tfPrixBaseHTSaisi2;
  private SNLabelChamp lbPrixBaseHTCNV2;
  private SNTexte tfPrixBaseHTCNV2;
  private SNMessage lbCodeCNVPrixBase2;
  private SNLabelChamp lbPrixBaseHTPump2;
  private SNTexte tfPrixBaseHTPump2;
  private SNLabelChamp lbPrixBaseHTPrixRevient2;
  private SNTexte tfPrixBaseHTPrixRevient2;
  private SNMessage lbOriginePrixBasePrixRevient2;
  private SNLabelChamp lbPrixBaseHTColonneTarif2;
  private SNTexte tfPrixBaseHTColonneTarif2;
  private SNMessage lbPrixBaseHT2;
  private SNTexte tfPrixBaseHT2;
  private SNMessage lbOriginePrixBaseHT2;
  private SNPanelTitre pnlRemise2;
  private SNLabelChamp lbPourcentageRemiseSaisi2;
  private SNPanel pnlPourcentageRemiseSaisi2;
  private SNTexte tfPourcentageRemiseSaisi2;
  private SNLabelChamp lbPourcentageRemiseClient2;
  private SNPanel pnlPourcentageRemiseClient2;
  private SNTexte tfPourcentageRemise1Client2;
  private SNTexte tfPourcentageRemise2Client2;
  private SNTexte tfPourcentageRemise3Client2;
  private SNLabelChamp lbPourcentageRemiseCNV2;
  private SNPanel pnlPPourcentageRemise2;
  private SNTexte tfPourcentageRemise1CNV2;
  private SNTexte tfPourcentageRemise2CNV2;
  private SNTexte tfPourcentageRemise3CNV2;
  private SNTexte tfPourcentageRemise4CNV2;
  private SNTexte tfPourcentageRemise5CNV2;
  private SNTexte tfPourcentageRemise6CNV2;
  private SNMessage lbPourcentageRemise2;
  private SNPanel pnlPourcentageRemise2;
  private SNTexte tfPourcentageRemise7;
  private SNTexte tfPourcentageRemise8;
  private SNTexte tfPourcentageRemise9;
  private SNTexte tfPourcentageRemise10;
  private SNTexte tfPourcentageRemise11;
  private SNTexte tfPourcentageRemise12;
  private SNMessage lbOriginePourcentageRemise2;
  private SNPanelTitre pnPrixNet2;
  private SNLabelChamp lbPrixNetHTSaisi2;
  private SNMontant tfPrixNetHTSaisi2;
  private SNLabelChamp lbPrixNetHTColonneTarif2;
  private SNTexte tfPrixNetHTColonneTarif2;
  private SNLabelChamp lbPrixNetHTCalcule2;
  private SNTexte tfPrixNetHTCalcule2;
  private SNLabelChamp lbPrixNetHTCNV2;
  private SNTexte tfPrixNetHTCNV2;
  private SNMessage lbCodeCNVPrixNet2;
  private SNMessage lbPrixNetHT2;
  private SNTexte tfPrixNetHT2;
  private SNMessage lbOriginePrixNetHT2;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
