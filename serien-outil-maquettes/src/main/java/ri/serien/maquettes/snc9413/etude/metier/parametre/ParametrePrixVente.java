/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.parametre;

import java.util.List;

/**
 * Classe regroupant tous les paramètres nécessaires au calcul du prix de vente d'un article.
 */
public class ParametrePrixVente {
  // Variables
  private ParametreGeneral parametreGeneral = null;
  private ParametreArticle parametreArticle = null;
  private ParametreClient parametreClient = null;
  private ParametreTarif parametreTarif = null;
  private ParametreDocumentVente parametreDocumentVente = null;
  private List<ParametreConditionVente> listeParametreConditionVente = null;
  
  // -- Accesseurs
  
  /**
   * Retourne les paramètres contenant les informations générales.
   * @return
   */
  public ParametreGeneral getParametreGeneral() {
    return parametreGeneral;
  }
  
  /**
   * Modifie les paramètres contenant les informations générales.
   * @param pParametreGeneral
   */
  public void setParametreGeneral(ParametreGeneral pParametreGeneral) {
    this.parametreGeneral = pParametreGeneral;
  }
  
  /**
   * Retourne les paramètres contenant les informations de l'article.
   * @return
   */
  public ParametreArticle getParametreArticle() {
    return parametreArticle;
  }
  
  /**
   * Modifie les paramètres contenant les informations de l'article.
   * @param pParametreArticle
   */
  public void setParametreArticle(ParametreArticle pParametreArticle) {
    this.parametreArticle = pParametreArticle;
  }
  
  /**
   * Retourne les paramètres contenant les informations du client.
   * @return
   */
  public ParametreClient getParametreClient() {
    return parametreClient;
  }
  
  /**
   * Modifie les paramètres contenant les informations du client.
   * @param pParametreClient
   */
  public void setParametreClient(ParametreClient pParametreClient) {
    this.parametreClient = pParametreClient;
  }
  
  /**
   * Retourne les paramètres contenant les informations du tarif article.
   * @return
   */
  public ParametreTarif getParametreTarif() {
    return parametreTarif;
  }
  
  /**
   * Retourne les paramètres contenant les informations du tarif article.
   * @param pParametreTarif
   */
  public void setParametreTarif(ParametreTarif pParametreTarif) {
    this.parametreTarif = pParametreTarif;
  }
  
  /**
   * Retourne les paramètres contenant les informations du document de vente.
   * @return
   */
  public ParametreDocumentVente getParametreDocumentVente() {
    return parametreDocumentVente;
  }
  
  /**
   * Modifie les paramètres contenant les informations du document de vente.
   * @param pParametreDocumentVente
   */
  public void setParametreDocumentVente(ParametreDocumentVente pParametreDocumentVente) {
    this.parametreDocumentVente = pParametreDocumentVente;
  }
  
  /**
   * Retourne la liste des paramètres contenant les informations des conditions de vente.
   * @return
   */
  public List<ParametreConditionVente> getListeParametreConditionVente() {
    return listeParametreConditionVente;
  }
  
  /**
   * Modifie la liste des paramètres contenant les informations des conditions de vente.
   * @param pListeParametreConditionVente
   */
  public void setListeParametreConditionVente(List<ParametreConditionVente> pListeParametreConditionVente) {
    this.listeParametreConditionVente = pListeParametreConditionVente;
  }
  
}
