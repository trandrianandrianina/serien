/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.parametre;

import java.math.BigDecimal;
import java.util.List;

/**
 * Classe regroupant tous les paramètres de l' article pour le calcul du prix de vente.
 */
public class ParametreArticle {
  // Variables
  private String codeArticle = null;
  // Nécessaire si une condition de vente est de type coefficient ou pourcentage afin de déterminer son prix de base HT
  // Source de la valeur :
  // Si T1VAL = 98 alors il faut récupérer le PUMP sinon reste à null
  private BigDecimal pumpHTArticle = null;
  // Source de la valeur :
  // Si T1VAL = 99 alors il faut récupérer le PRV sinon reste à null
  private BigDecimal prixDeRevientHT = null;
  // Utilisé pour déterminer le prix de base de la CNV en mode Négoce sinon reste à null
  // Source de la valeur :
  // Si T1VAL = 99 et mode négoce et l'article a un fournisseur alors il faut récupérer le PRS de la CNA sinon reste null
  private BigDecimal prixDeRevientHTStandardCNA = null;
  // Indique si la condition est de type gratuit
  // Si TCTCD = 'N' et que le champ T1FPR commence par '*TG' alors condition de type gratuit
  // TODO avoir si ce n'est pas plus judicieux d'utiliser le champ T1TCD avec la valeur 'G' pour définir type gratuit
  private String identifiantFournisseur = null;
  private List<String> listeCodeRattachementCNV = null;
  private String groupeFamille = null;
  private String sousFamille = null;
  
  // -- Accesseurs
  
  /**
   * Retourne le code de l'article.
   * @return
   */
  public String getCodeArticle() {
    return codeArticle;
  }
  
  /**
   * Initialise le code de l'article.
   * @param pCodeArticle
   */
  public void setCodeArticle(String pCodeArticle) {
    this.codeArticle = pCodeArticle;
  }
  
  /**
   * Retourne le pump HT de l'article.
   * @return
   */
  public BigDecimal getPumpHTArticle() {
    return pumpHTArticle;
  }
  
  /**
   * Initialise le pump HT de l'article.
   * @param pPumpHTArticle
   */
  public void setPumpHTArticle(BigDecimal pPumpHTArticle) {
    this.pumpHTArticle = pPumpHTArticle;
  }
  
  /**
   * Retourne le prix de revient HT de l'article.
   * @return
   */
  public BigDecimal getPrixDeRevientHT() {
    return prixDeRevientHT;
  }
  
  /**
   * Initialise le prix de revient HT de l'article.
   * @param pPrixDeRevientHT
   */
  public void setPrixDeRevientHT(BigDecimal pPrixDeRevientHT) {
    this.prixDeRevientHT = pPrixDeRevientHT;
  }
  
  /**
   * Retourne le prix de revient standard HT de l'article de la CNA.
   * @return
   */
  public BigDecimal getPrixDeRevientHTStandardCNA() {
    return prixDeRevientHTStandardCNA;
  }
  
  /**
   * Initialise le prix de revient standard HT de l'article de la CNA (utilisé en mode négoce uniquement).
   * @param pPrixDeRevientHTStandardCNA
   */
  public void setPrixDeRevientHTStandardCNA(BigDecimal pPrixDeRevientHTStandardCNA) {
    this.prixDeRevientHTStandardCNA = pPrixDeRevientHTStandardCNA;
  }
  
  /**
   * Retourne l'identifiant fournisseur (déjà formaté).
   * @return
   */
  public String getIdentifiantFournisseur() {
    return identifiantFournisseur;
  }
  
  /**
   * Initialise l'identifiant fournisseur (déjà formaté).
   * @param pIdentifiantFournisseur
   */
  public void setIdentifiantFournisseur(String pIdentifiantFournisseur) {
    this.identifiantFournisseur = pIdentifiantFournisseur;
  }
  
  /**
   * Retourne la liste des rattachement aux conditions de vente.
   * @return
   */
  public List<String> getListeCodeRattachementCNV() {
    return listeCodeRattachementCNV;
  }
  
  /**
   * Initialise la liste des rattachement aux conditions de vente.
   * @param pListeCodeRattachementCNV
   */
  public void setListeCodeRattachementCNV(List<String> pListeCodeRattachementCNV) {
    this.listeCodeRattachementCNV = pListeCodeRattachementCNV;
  }
  
  /**
   * Retourne le groupe famille de l'article.
   * @return
   */
  public String getGroupeFamille() {
    return groupeFamille;
  }
  
  /**
   * Initialise le groupe famille de l'article.
   * @param pGroupeFamille
   */
  public void setGroupeFamille(String pGroupeFamille) {
    this.groupeFamille = pGroupeFamille;
  }
  
  /**
   * Retourne la sous-famille de l'article.
   * @return
   */
  public String getSousFamille() {
    return sousFamille;
  }
  
  /**
   * Initialise la sous-famille de l'article.
   * @param pSousFamille
   */
  public void setSousFamille(String pSousFamille) {
    this.sousFamille = pSousFamille;
  }
  
}
