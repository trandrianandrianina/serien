/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.parametre;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;
import ri.serien.maquettes.snc9413.etude.metier.EnumOrigine;
import ri.serien.maquettes.snc9413.etude.metier.condition.EnumCategorieConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.condition.EnumTypeConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.formulecalculprix.FormulePrixFP;

/**
 * Classe regroupant tous les paramètres des conditions de vente pour le calcul du prix de vente.
 */
public class ParametreConditionVente {
  // Variables
  // Origine de la CNV : DG, client, ...
  private EnumOrigine origine = null;
  private String code = null;
  private Date dateDebutValidite = null;
  private Date dateFinValidite = null;
  private String codeDevise = null;
  private EnumCategorieConditionVente categorie = null;
  private EnumTypeConditionVente type = null;
  private BigDecimal quantiteMinimale = null;
  // Source de la valeur : T1VAL car elle sert à tout et n'importe quoi (peut faire doublon avec certaines variables comme prixBaseHT,
  // prixNetHT, ...)
  private BigDecimal t1val = null;
  // Source de la valeur :
  // Si T1TCD = 'B' (type CNV) alors T1VAL contient un prix de base
  private BigDecimal prixBaseHT = null;
  // Source de la valeur :
  // Si T1TCD = 'N' (type CNV) T1VAL contient un prix net
  private BigDecimal prixNetHT = null;
  // Source de la valeur :
  // Si T1TCD = + ou - (type CNV) alors T1VAL contient une remise en valeur
  private BigDecimal remiseEnValeur = null;
  private BigDecimal ajoutEnValeur = null;
  private List<BigDecimal> listeRemiseEnPourcentage = null;
  private boolean modeRemiseEnCascade = true;
  private BigDecimal coefficientMultiplicateur = null;
  // Source de la valeur :
  // Paramètre 'FP'
  private FormulePrixFP formulePrixFP = null;
  // Source de la valeur :
  // Ce numéro de colonne tarif est stockée en base dans le champ T1VAL sous certaines conditions (tout ça pour éviter un REC...)
  // Si T1TCD = 'K' pour coeff ou 'R' pour remise alors T1VAL est nulle ou contient une colonne de tarif. Donc pour imposer une colonne
  // de tarif avec une CNV sans coeff ni remise, on utilise T1TCD = 'K' et T1COE = 1 et T1REM = 0
  private ColonneTarif colonneTarif = null;
  private boolean typeGratuit = false;
  
  /**
   * Constructeur.
   */
  public ParametreConditionVente(EnumOrigine pOrigine) {
    origine = pOrigine;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne l'origine des paramètres de la CNV.
   * @return
   */
  public EnumOrigine getOrigine() {
    return origine;
  }
  
  /**
   * Retourne le code de la condition de vente.
   * @return
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Initialise le code de la condition de vente.
   * @param code
   */
  public void setCode(String code) {
    this.code = code;
  }
  
  /**
   * Retourne la date du début de la validité de la condition de vente.
   * @return
   */
  public Date getDateDebutValidite() {
    return dateDebutValidite;
  }
  
  /**
   * Initialise la date du début de la validité de la condition de vente.
   * @param dateDebutValidite
   */
  public void setDateDebutValidite(Date dateDebutValidite) {
    this.dateDebutValidite = dateDebutValidite;
  }
  
  /**
   * Retourne la date de fin de la validité de la condition de vente.
   * @return
   */
  public Date getDateFinValidite() {
    return dateFinValidite;
  }
  
  /**
   * Initialise la date de fin de la validité de la condition de vente.
   * @param dateFinValidite
   */
  public void setDateFinValidite(Date dateFinValidite) {
    this.dateFinValidite = dateFinValidite;
  }
  
  /**
   * Retourne le code de la devise.
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Initialise le code de la devise.
   * @param codeDevise
   */
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  /**
   * Retourne la catégorie de la condition de vente.
   * @return
   */
  public EnumCategorieConditionVente getCategorie() {
    return categorie;
  }
  
  /**
   * Initialise la catégorie de la condition de vente.
   * @param categorie
   */
  public void setCategorie(EnumCategorieConditionVente categorie) {
    this.categorie = categorie;
  }
  
  /**
   * Retourne la quantité minimale d'article pour activer la condition.
   * @return
   */
  public BigDecimal getQuantiteMinimale() {
    return quantiteMinimale;
  }
  
  /**
   * Initialise la quantité minimale d'article pour activer la condition.
   * @param quantiteMinimale
   */
  public void setQuantiteMinimale(BigDecimal quantiteMinimale) {
    this.quantiteMinimale = quantiteMinimale;
  }
  
  /**
   * Retourne la valeur de T1VAL.
   * @return
   */
  public BigDecimal getT1val() {
    return t1val;
  }
  
  /**
   * Initialise la valeur de T1VAL.
   * @param t1val
   */
  public void setT1val(BigDecimal t1val) {
    this.t1val = t1val;
  }
  
  /**
   * Retourne le type de la condition de vente.
   * @return
   */
  public EnumTypeConditionVente getType() {
    return type;
  }
  
  /**
   * Initialise le type de la condition de vente.
   * @param type
   */
  public void setType(EnumTypeConditionVente type) {
    this.type = type;
  }
  
  /**
   * Retourne le prix de base HT si le type de la CNV vaut 'B'.
   * @return
   */
  public BigDecimal getPrixBaseHT() {
    return prixBaseHT;
  }
  
  /**
   * Initialise le prix de base HT si le type de la CNV vaut 'B'.
   * @param prixBaseHT
   */
  public void setPrixBaseHT(BigDecimal prixBaseHT) {
    this.prixBaseHT = prixBaseHT;
  }
  
  /**
   * Retourne le prix net HT si le type de la CNV vaut 'N'.
   * @return
   */
  public BigDecimal getPrixNetHT() {
    return prixNetHT;
  }
  
  /**
   * Initialise le prix net HT si le type de la CNV vaut 'N'.
   * @param prixNetHT
   */
  public void setPrixNetHT(BigDecimal prixNetHT) {
    this.prixNetHT = prixNetHT;
  }
  
  /**
   * Retourne la remise en valeur si le type de la CNV vaut + ou -.
   * @return
   */
  public BigDecimal getRemiseEnValeur() {
    return remiseEnValeur;
  }
  
  /**
   * Initialise la remise en valeur si le type de la CNV vaut + ou -.
   * @param remiseEnValeur
   */
  public void setRemiseEnValeur(BigDecimal remiseEnValeur) {
    this.remiseEnValeur = remiseEnValeur;
  }
  
  /**
   * Retourne l'ajout en valeur.
   * @return
   */
  public BigDecimal getAjoutEnValeur() {
    return ajoutEnValeur;
  }
  
  /**
   * Initialise l'ajout en valeur.
   * @param ajoutEnValeur
   */
  public void setAjoutEnValeur(BigDecimal ajoutEnValeur) {
    this.ajoutEnValeur = ajoutEnValeur;
  }
  
  /**
   * Retourne la liste des remises à appliquer.
   * @return
   */
  public List<BigDecimal> getListeRemiseEnPourcentage() {
    return listeRemiseEnPourcentage;
  }
  
  /**
   * Initialise la liste des remises à appliquer.
   * @param pListeRemise
   */
  public void setListeRemiseEnPourcentage(List<BigDecimal> pListeRemise) {
    this.listeRemiseEnPourcentage = pListeRemise;
  }
  
  /**
   * Retourne le mode d'application des remises.
   * @return
   */
  public boolean isModeRemiseEnCascade() {
    return modeRemiseEnCascade;
  }
  
  /**
   * Initialise le mode d'application des remises.
   * @param modeRemiseEnCascade
   */
  public void setModeRemiseEnCascade(boolean modeRemiseEnCascade) {
    this.modeRemiseEnCascade = modeRemiseEnCascade;
  }
  
  /**
   * Retourne le coefficient multiplicateur.
   * @return
   */
  public BigDecimal getCoefficientMultiplicateur() {
    return coefficientMultiplicateur;
  }
  
  /**
   * Initialise le coefficient multiplicateur.
   * @param pCoefficientMultiplicateur
   */
  public void setCoefficientMultiplicateur(BigDecimal pCoefficientMultiplicateur) {
    this.coefficientMultiplicateur = pCoefficientMultiplicateur;
  }
  
  /**
   * Retourne la formule du calcul du prix.
   * @return
   */
  public FormulePrixFP getFormulePrixFP() {
    return formulePrixFP;
  }
  
  /**
   * Initialise la formule du calcul du prix.
   * @param pFormulePrixFP
   */
  public void setFormulePrixFP(FormulePrixFP pFormulePrixFP) {
    this.formulePrixFP = pFormulePrixFP;
  }
  
  /**
   * Retourne la colonne tarif de la condition de vente.
   * @return
   */
  public ColonneTarif getColonneTarif() {
    return colonneTarif;
  }
  
  /**
   * Initialise la colonne tarif de la condition de vente.
   * @param pColonneTarif
   */
  public void setColonneTarif(ColonneTarif pColonneTarif) {
    this.colonneTarif = pColonneTarif;
  }
  
  /**
   * Retourne si la condition est de type gratuit.
   * @return
   */
  public boolean isTypeGratuit() {
    return typeGratuit;
  }
  
  /**
   * Initialise la condition en type gratuit.
   * @param typeGratuit
   */
  public void setTypeGratuit(boolean typeGratuit) {
    this.typeGratuit = typeGratuit;
  }
  
}
