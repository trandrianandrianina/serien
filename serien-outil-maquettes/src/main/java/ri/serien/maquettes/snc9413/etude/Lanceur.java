
package ri.serien.maquettes.snc9413.etude;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.EnumTypeSession;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerClientRMI;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceSession;
import ri.serien.maquettes.snc9413.etude.ihm.DialogueDetailCalculPrixVente;
import ri.serien.maquettes.snc9413.etude.ihm.ModeleDialogueCalculPrixVente;
import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;
import ri.serien.maquettes.snc9413.etude.metier.EnumOrigine;
import ri.serien.maquettes.snc9413.etude.metier.condition.EnumCategorieConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.condition.EnumTypeConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreArticle;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreClient;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreGeneral;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametrePrixVente;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreTarif;

/**
 * ATTENTION le profil doit avoir la meme base de données qu'ici. A voir pour corriger cela plus tard.
 * Jeu d'essai :
 * - client 225
 */
public class Lanceur {
  // Constantes
  private final String DATE_JOUR = "Date du jour";
  private final String DATE_COMMANDE = "Date commande";
  private final String DATE_LIVRAISON = "Date de livraison";
  
  // Variables
  private IdClientDesktop idClientDesktop = null;
  private IdSession idSession = null;
  
  // Jeu d'essai 1 : Client 225 de la FM911 article PLC04102
  /*
  private String nomBdd = "FM911";
  private String codeEtablissement = "ALL";
  private String codeDevise = "EUR";
  private int numeroClient = 225;
  private String codeArticle = "PLC04102";
  */
  // Jeu d'essai 2 : Client 3646 de la FM567 article LAF05005 avec remise client
  /*
  private String nomBdd = "FM567";
  private String codeEtablissement = "HER";
  private String codeDevise = "EUR";
  private int numeroClient = 3646;
  private String codeArticle = "LAF05005";*/
  // Jeu d'essai 3 : Client 3645 de la FM567 article LAF05005 avec condition client remise
  private String nomBdd = "FM567";
  private String codeEtablissement = "HER";
  private String codeDevise = "EUR";
  private int numeroClient = 3645;
  private String codeArticle = "LAF05005";
  
  // -- Méthodes publiques
  
  public void executer() {
    // Force le look&Feel Nimbus
    Constantes.forcerLookAndFeelNimbus();
    
    // Initialisation de la connexion avec le serveur Série N pour le chargement des données
    initialiserConnexion();
    
    // Création et chargement des paramètres nécessaire au calcul
    ParametrePrixVente parametrePrixVente = chargerDonnees();
    
    // Affichage de la boite de dialogue
    ModeleDialogueCalculPrixVente modele = new ModeleDialogueCalculPrixVente(parametrePrixVente);
    DialogueDetailCalculPrixVente vue = new DialogueDetailCalculPrixVente(modele);
    vue.afficher();
  }
  
  // -- Méthodes privées
  
  /**
   * Initialisation de la connexion au serveur Série N.
   */
  private void initialiserConnexion() {
    // Connexion au serveur
    EnvUser envUser = new EnvUser();
    envUser.setProfil("RIDEVSV");
    envUser.setMotDePasse("GAR1972");
    BDD baseDeDonnees = new BDD(IdBibliotheque.getInstance(nomBdd), 'O');
    envUser.setCurlib(baseDeDonnees);
    envUser.setServeurSGM("SRP_DEV");
    envUser.setPortSGM(6690);
    envUser.getDossierDesktop().setDossierRacine("C:\\Users\\Stéphane\\serien");
    envUser.setIdSousEnvironnement(IdSousEnvironnement.getInstance("Defaut"));
    ManagerSessionClient.getInstance().setEnvUser(envUser);
    ManagerClientRMI.configurerServeur(envUser.getServeurSGM(), envUser.getPortSGM());
    
    idClientDesktop = ManagerServiceSession.creerClientDesktop();
    envUser.setCurlib(baseDeDonnees);
    // ManagerServiceSession.connecterClientDesktop(idClientDesktop, envUser);
    ManagerSessionClient.getInstance().getEnvUser().setIdClientDesktop(idClientDesktop);
    ManagerSessionClient.getInstance().getEnvUser().getDossierServeur()
        .setDossierRacine(ManagerServiceSession.getNomDossierRacineServeur());
    if (!ManagerServiceSession.connecterClientDesktop(ManagerSessionClient.getInstance().getEnvUser().getIdClientDesktop(),
        ManagerSessionClient.getInstance().getEnvUser())) {
      throw new MessageErreurException("Utilisateur ou mot de passe incorrect.");
    }
    
    // Récupération des données utilisateurs mise à jour lors de la connexion au serveur
    envUser = ManagerServiceSession.getEnvUser(ManagerSessionClient.getInstance().getEnvUser().getIdClientDesktop());
    ManagerSessionClient.getInstance().setEnvUser(envUser);
    
    // Appel des services
    idSession = ManagerSessionClient.getInstance().creerSessionClient();
    ManagerServiceSession.ouvrirSession(idSession, EnumTypeSession.SESSION_INCONNUE, null);
  }
  
  /**
   * Charge les données nécessaires.
   * A faire :
   * - Voir comment charger la devise
   * - Que faire de la 2° colonne tarif dans la fiche client
   * - Faire chargement CNV
   * - Faire chargement du document de vente
   * @return
   * @throws ParseException
   */
  private ParametrePrixVente chargerDonnees() {
    ParametrePrixVente parametrePrixVente = new ParametrePrixVente();
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(codeEtablissement);
    ManagerSessionClient.getInstance().setIdEtablissement(idSession, idEtablissement);
    
    // Paramètres généraux
    ParametreGeneral parametreGeneral = new ParametreGeneral();
    parametreGeneral.setNomBaseDeDonnees(nomBdd);
    parametreGeneral.setCodeEtablissement(idEtablissement.getCodeEtablissement());
    parametreGeneral.setCodeDevise(codeDevise);
    boolean ps287 = ManagerSessionClient.getInstance().isParametreSystemeActif(idSession, EnumParametreSysteme.PS287);
    parametreGeneral.setModeNegoce(ps287);
    // Chargement de la PS105 qui contient le numéro de colonne tarif
    Character ps105 = ManagerSessionClient.getInstance().getValeurParametreSysteme(idSession, EnumParametreSysteme.PS105);
    ColonneTarif colonneTarifPS105 = null;
    if (ps105 != null) {
      colonneTarifPS105 =
          new ColonneTarif(Constantes.convertirTexteEnInteger(ps105.toString()), EnumOrigine.PS105, parametreGeneral.isModeNegoce());
    }
    // Chargement de la PS305
    Boolean ps305 = Boolean.valueOf(ManagerSessionClient.getInstance().isParametreSystemeActif(idSession, EnumParametreSysteme.PS305));
    parametreGeneral.setColonnePS105(colonneTarifPS105);
    parametreGeneral.setChercherColonnePrecedente(ps305);
    
    parametrePrixVente.setParametreGeneral(parametreGeneral);
    
    // Paramètres article
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, codeArticle);
    ParametreArticle parametreArticle = new ParametreArticle();
    parametreArticle.setCodeArticle(idArticle.getCodeArticle());
    parametrePrixVente.setParametreArticle(parametreArticle);
    
    // Paramètres client
    ParametreClient parametreClient = new ParametreClient();
    IdClient idClient = IdClient.getInstance(idEtablissement, numeroClient, 0);
    if (idClient == null) {
      throw new MessageErreurException("L'identifiant du client est invalide.");
    }
    Client client = ManagerServiceClient.chargerClient(idSession, idClient);
    if (client == null) {
      throw new MessageErreurException("Le client n'a pas pu être chargé.");
    }
    parametreClient.setIdentifiantClient(idClient.toString());
    parametreClient.setTTC(client.isFactureEnTTC());
    // Le code devise
    String codeDevise = client.getCodeDevise();
    if (!Constantes.normerTexte(codeDevise).isEmpty()) {
      parametreClient.setCodeDevise(codeDevise);
    }
    // La colonne client
    ColonneTarif colonneClient = null;
    int colonne = client.getNumeroColonneTarif();
    if (colonne > 1) {
      colonneClient = new ColonneTarif(Integer.valueOf(colonne), EnumOrigine.CLIENT, parametreGeneral.isModeNegoce());
    }
    parametreClient.setColonneTarif(colonneClient);
    // Les remises clients
    parametreClient.ajouterRemise(client.getRemise1());
    parametreClient.ajouterRemise(client.getRemise2());
    parametreClient.ajouterRemise(client.getRemise3());
    // Exclusion CNV de la DG
    if (client.getNonUtilisationDGCNV() == null || client.getNonUtilisationDGCNV().charValue() == ' ') {
      parametreClient.setNePasUtiliserConditionVenteDG(Boolean.FALSE);
    }
    else {
      parametreClient.setNePasUtiliserConditionVenteDG(Boolean.TRUE);
    }
    parametrePrixVente.setParametreClient(parametreClient);
    
    // Paramètres tarif
    Date dateApplication = new Date();
    /*
    try {
      dateApplication = new SimpleDateFormat("dd/MM/yyyy").parse("01/10/2020");
    }
    catch (ParseException e) {
      dateApplication = new Date();
    }*/
    String origineDate = DATE_JOUR;
    ParametreTarif parametreTarif = new ParametreTarif();
    parametreTarif.setDateApplication(dateApplication);
    parametreTarif.setOrigineDate(origineDate);
    
    // Article test (5.27€ HT pour le client 225)
    TarifArticle tarifArticle = ManagerServiceArticle.chargerTarifArticle(idSession, idArticle, parametreGeneral.getCodeDevise(),
        dateApplication, parametreClient.getTTC().booleanValue());
    if (tarifArticle == null) {
      throw new MessageErreurException("Le tarif de l'article n'a pas pu être chargé.");
    }
    List<BigDecimal> listeTarif = new ArrayList<BigDecimal>();
    listeTarif.add(tarifArticle.getPrixVente01());
    listeTarif.add(tarifArticle.getPrixVente02());
    listeTarif.add(tarifArticle.getPrixVente03());
    listeTarif.add(tarifArticle.getPrixVente04());
    listeTarif.add(tarifArticle.getPrixVente05());
    listeTarif.add(tarifArticle.getPrixVente06());
    if (!parametreGeneral.isModeNegoce()) {
      listeTarif.add(tarifArticle.getPrixVente07());
      listeTarif.add(tarifArticle.getPrixVente08());
      listeTarif.add(tarifArticle.getPrixVente09());
      listeTarif.add(tarifArticle.getPrixVente10());
    }
    parametreTarif.setListeTarif(listeTarif);
    parametrePrixVente.setParametreTarif(parametreTarif);
    
    // Paramètre CNV
    ParametreConditionVente parametreConditionVente = new ParametreConditionVente(EnumOrigine.CLIENT);
    parametreConditionVente.setCode("003645"); // T1CNV
    parametreConditionVente.setCategorie(EnumCategorieConditionVente.NORMALE);
    parametreConditionVente.setType(EnumTypeConditionVente.REMISE_EN_POURCENTAGE);
    Date date = null;
    try {
      SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy", Locale.FRANCE);
      date = formatter.parse("17-21-2021");
    }
    catch (Exception e) {
      date = new Date();
    }
    parametreConditionVente.setDateDebutValidite(date);
    parametreConditionVente.setDateFinValidite(date);
    List<BigDecimal> listeRemise = new ArrayList<BigDecimal>();
    listeRemise.add(new BigDecimal("13.00"));
    listeRemise.add(BigDecimal.ZERO);
    listeRemise.add(BigDecimal.ZERO);
    listeRemise.add(BigDecimal.ZERO);
    listeRemise.add(BigDecimal.ZERO);
    listeRemise.add(BigDecimal.ZERO);
    parametreConditionVente.setListeRemiseEnPourcentage(listeRemise);
    parametreConditionVente.setT1val(BigDecimal.ZERO);
    
    List<ParametreConditionVente> listeCNV = new ArrayList<ParametreConditionVente>();
    listeCNV.add(parametreConditionVente);
    parametrePrixVente.setListeParametreConditionVente(listeCNV);
    
    return parametrePrixVente;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne l'identifiant du client desktop.
   * @return
   */
  public IdClientDesktop getIdClientDesktop() {
    return idClientDesktop;
  }
  
  // -- Programme principal
  
  public static void main(String[] args) {
    System.setSecurityManager(null);
    
    Lanceur lanceur = new Lanceur();
    lanceur.executer();
    
    // Déconnexion du serveur
    if (lanceur.getIdClientDesktop() != null) {
      ManagerServiceSession.detruireClientDesktop(lanceur.getIdClientDesktop());
    }
    // Ferme l'application
    System.exit(0);
  }
  
}
