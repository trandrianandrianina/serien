/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.vente.prixvente.OutilCalculPrix;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.maquettes.snc9413.etude.metier.condition.ConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.condition.ListeConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreArticle;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreClient;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreConditionVente;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreDocumentVente;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreGeneral;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametrePrixVente;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreTarif;

/**
 * Cette classe contient toutes les opérations nécessaires au calcul du prix de vente d'un article.
 * Tous les paramètres fournis à cette suffisent à effectuer ce calcul, aucun accès en base n'est et ne doit être effectué dans cette
 * classe.
 * A faire :
 * - Que faire de la 2° colonne tarif dans la fiche client ?
 */
public class CalculPrixVente {
  // Constantes
  public static final int NOMBRE_DECIMALE_MONTANT = 2;
  
  // Variables
  private ParametrePrixVente parametrePrixVente = null;
  
  private String nomBaseDeDonnees = null;
  private boolean modeNegoce = false;
  private String codeEtablissement = null;
  private String codeDevise = null;
  private String origineDevise = null;
  private Boolean valeurPS305 = null;
  private String codeClient = null;
  private boolean ttc = false;
  private String codeArticle = null;
  private Date dateApplication = null;
  private String origineDateApplication = null;
  private List<BigDecimal> listePrixVenteHT = null;
  private String codeDocumentVente = null;
  
  // Les conditions de ventes
  private ListeConditionVente listeConditionVente = null;
  private String codeMeilleureConditionVente = null;
  private String codeConditionVenteSelectionnee = null;
  
  // Liste des colonnes tarifs possibles
  private ColonneTarif colonneTarifSaisie = null;
  private ColonneTarif colonneTarifDocumentVente = null;
  private ColonneTarif colonneTarifConditionVente = null;
  private ColonneTarif colonneTarifClient = null;
  private ColonneTarif colonneTarifPS105 = null;
  private ColonneTarif colonneTarifUn = null;
  private ColonneTarif colonneTarifAUtiliser = null;
  
  // Les prix de base
  private BigDecimal prixBaseHTSaisi = null;
  private BigDecimal prixBaseHTCNV = null;
  private BigDecimal prixBaseHTPump = null;
  private BigDecimal prixBaseHTPrixRevient = null;
  private BigDecimal prixBaseHTColonneTarif = null;
  private BigDecimal prixBaseHT = null;
  private String originePrixBaseHT = null;
  
  // Les remises
  private List<BigDecimal> listePourcentageRemiseSaisi = new ArrayList<BigDecimal>();
  private List<BigDecimal> listePourcentageRemiseClient = null;
  private List<BigDecimal> listePourcentageRemiseCNV = null;
  private List<BigDecimal> listePourcentageRemise = new ArrayList<BigDecimal>();
  private EnumOrigine originePourcentageRemise = null;
  
  // Le coefficient multiplicateur
  private BigDecimal coefficientMultiplicateurSaisi = null;
  
  // Les prix net
  private BigDecimal prixNetHTSaisi = null;
  private BigDecimal prixNetHTColonneTarif = null;
  private BigDecimal prixNetHTCalcule = null;
  private BigDecimal prixNetHTCNV = null;
  private BigDecimal prixNetHT = null;
  private String originePrixNetHT = null;
  
  /**
   * Constructeur.
   */
  public CalculPrixVente(ParametrePrixVente pParametrePrixVente) {
    parametrePrixVente = pParametrePrixVente;
    
    // Charge toutes les données nécessaires à la détermination du prix de vente
    initialiser();
  }
  
  // -- Méthodes publiques
  
  /**
   * Calcule le prix de vente.
   */
  public void calculer() {
    // Détermine parmi toutes les colonnes tarifs collectées laquelle doit être utilisée (hors conditions de ventes)
    determinerColonneTarif();
    
    // Calcul des prix de base HT et des prix net HT pour toutes les conditions de ventes
    calculerPrixVenteConditionVente();
    
    // Détermine parmi les prix de base HT lequel doit être utilisé
    calculerPrixBaseHTColonneUn();
    determinerPrixBaseHT();
    
    // Détermine le pourcentage de remise
    determinerPourcentageRemise();
    
    // Détermine parmi les prix net HT lequel doit être utilisé
    calculerPrixNetHTAvecColonneTarifDeduite();
    calculerPrixNetHTCalcule();
    determinerPrixNetHT();
    
  }
  
  /**
   * Modifie le numéro de la colonne saisie.
   * @param pNumeroColonne
   */
  public void modifierColonneSaisie(Integer pNumeroColonne) {
    if (pNumeroColonne == null) {
      colonneTarifSaisie = null;
      calculer();
      return;
    }
    // Si la valeur saisie est égale à la valeur déjà enregistrée, il n'y a rien à faire
    if (colonneTarifSaisie != null && colonneTarifSaisie.getNumero().compareTo(pNumeroColonne) == 0) {
      return;
    }
    colonneTarifSaisie = new ColonneTarif(pNumeroColonne, EnumOrigine.SAISI, modeNegoce);
    calculer();
  }
  
  /**
   * Modifie le prix de base HT saisi.
   * @param pPrixBaseHTSaisi
   */
  public void modifierPrixBaseHTSaisi(BigDecimal pPrixBaseHTSaisi) {
    if (pPrixBaseHTSaisi == null) {
      prixBaseHTSaisi = null;
      calculer();
      return;
    }
    // Si la valeur saisie est égale à la valeur déjà enregistrée, il n'y a rien à faire
    if (prixBaseHTSaisi != null && prixBaseHTSaisi.compareTo(pPrixBaseHTSaisi) == 0) {
      return;
    }
    prixBaseHTSaisi = controlerValeurMontant(pPrixBaseHTSaisi);
    calculer();
  }
  
  /**
   * Modifie le prix net HT saisi.
   * @param pPrixNetHTSaisi
   */
  public void modifierPrixNetHTSaisi(BigDecimal pPrixNetHTSaisi) {
    if (pPrixNetHTSaisi == null) {
      prixNetHTSaisi = null;
      calculer();
      return;
    }
    // Si la valeur saisie est égale à la valeur déjà enregistrée, il n'y a rien à faire
    if (prixNetHTSaisi != null && prixNetHTSaisi.compareTo(pPrixNetHTSaisi) == 0) {
      return;
    }
    prixNetHTSaisi = controlerValeurMontant(pPrixNetHTSaisi);
    calculer();
  }
  
  /**
   * Modifie le pourcentage de remise saisi.
   * @param pPourcentageRemise
   */
  public void modifierPourcentageRemiseSaisi(BigDecimal pPourcentageRemise) {
    if (pPourcentageRemise == null) {
      listePourcentageRemiseSaisi.clear();
      calculer();
      return;
    }
    // Si la valeur saisie est égale à la valeur déjà enregistrée, il n'y a rien à faire
    if (listePourcentageRemiseSaisi != null && listePourcentageRemiseSaisi.size() > 0
        && listePourcentageRemiseSaisi.get(0).compareTo(pPourcentageRemise) == 0) {
      return;
    }
    listePourcentageRemiseSaisi.clear();
    listePourcentageRemiseSaisi.add(controlerValeurPourcentage(pPourcentageRemise));
    calculer();
  }
  
  /**
   * Modifie la CNV sélectionnée.
   * @param pPourcentageRemise
   */
  public void modifierConditionVenteSelectionnee(String pCodeCNV) {
    if (pCodeCNV == null) {
      codeConditionVenteSelectionnee = null;
      calculer();
      return;
    }
    // Si la valeur saisie est égale à la valeur déjà enregistrée, il n'y a rien à faire
    if (Constantes.equals(codeConditionVenteSelectionnee, pCodeCNV)) {
      return;
    }
    codeConditionVenteSelectionnee = pCodeCNV;
    // Initialiser variables spécifiques
    ConditionVente conditionVente = listeConditionVente.getConditionVenteAvecCode(pCodeCNV);
    if (conditionVente != null) {
      listePourcentageRemiseCNV = conditionVente.getParametreConditionVente().getListeRemiseEnPourcentage();
    }
    calculer();
  }
  
  /**
   * Retourne le prix net HT à partir d'un numéro de colonne.
   * Gère la conversion colonne/liste (index 1/0) et prend en compte la PS305.
   * @return
   */
  public static BigDecimal retournerPrixVenteAvecColonne(Integer pNumeroColonne, List<BigDecimal> pListePrixVenteHT,
      Boolean pValeurPS305) {
    if (pNumeroColonne == null) {
      throw new MessageErreurException("Le numéro de la colonne est invalide, il doit être compris entre 1 et 10 maximum.");
    }
    if (pListePrixVenteHT == null || pListePrixVenteHT.isEmpty()) {
      throw new MessageErreurException("La liste des prix de vente du tarif de l'article est invalide.");
    }
    if (pNumeroColonne.intValue() <= 0) {
      return null;
    }
    
    // Contrôle s'il faut parcourir les colonnes précédentes si le prix net de la colonne en cours vaut 0
    boolean chercherColonnePrecedente = false;
    if (pValeurPS305 != null && pValeurPS305.booleanValue()) {
      chercherColonnePrecedente = true;
    }
    
    // Récupération du prix net à partir de la colonne déterminée (parmi les colonnes des différentes sources)
    BigDecimal prixNetHT = pListePrixVenteHT.get(pNumeroColonne.intValue() - 1);
    if (prixNetHT == null) {
      prixNetHT = BigDecimal.ZERO;
    }
    // Si la PS305 n'est pas activée alors le prix net est retourné tel quel
    if (!chercherColonnePrecedente) {
      return prixNetHT;
    }
    // Si la PS305 est activée et que le prix net vaut 0 alors parcourt des colonnes précédentes du tarif jusqu'à trouver un prix net > 0
    if (prixNetHT.intValue() == 0) {
      for (int indexColonne = pNumeroColonne.intValue() - 2; indexColonne > 0; indexColonne--) {
        prixNetHT = pListePrixVenteHT.get(indexColonne);
        if (prixNetHT == null) {
          prixNetHT = BigDecimal.ZERO;
        }
        if (prixNetHT.intValue() > 0) {
          return prixNetHT;
        }
      }
    }
    return prixNetHT;
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise les valeurs à partir du paramètre.
   */
  private void initialiser() {
    if (parametrePrixVente == null) {
      throw new MessageErreurException("Les paramètres pour calculer le prix de vente de l'article sont invalides.");
    }
    
    // Les paramètres généraux
    ParametreGeneral parametreGeneral = parametrePrixVente.getParametreGeneral();
    if (parametreGeneral == null) {
      throw new MessageErreurException("Les paramètres généraux ne sont pas valides.");
    }
    nomBaseDeDonnees = parametreGeneral.getNomBaseDeDonnees();
    modeNegoce = parametreGeneral.isModeNegoce();
    codeEtablissement = parametreGeneral.getCodeEtablissement();
    codeDevise = parametreGeneral.getCodeDevise();
    origineDevise = "Description générale";
    colonneTarifPS105 = parametreGeneral.getColonnePS105();
    valeurPS305 = parametreGeneral.getChercherColonnePrecedente();
    
    // Les paramètres article
    ParametreArticle parametreArticle = parametrePrixVente.getParametreArticle();
    if (parametreArticle == null) {
      throw new MessageErreurException("Les paramètres articles ne sont pas valides.");
    }
    codeArticle = parametreArticle.getCodeArticle();
    
    // Les paramètres client
    ParametreClient parametreClient = parametrePrixVente.getParametreClient();
    if (parametreClient == null) {
      throw new MessageErreurException("Les paramètres client ne sont pas valides.");
    }
    codeClient = parametreClient.getIdentifiantClient();
    if (!Constantes.normerTexte(parametreClient.getCodeDevise()).isEmpty()) {
      codeDevise = parametreClient.getCodeDevise();
      origineDevise = "Client";
    }
    if (parametreClient.getTTC() != null) {
      ttc = parametreClient.getTTC().booleanValue();
    }
    colonneTarifClient = parametreClient.getColonneTarif();
    listePourcentageRemiseClient = parametreClient.getListeRemise();
    
    // Les paramètres tarif
    ParametreTarif parametreTarif = parametrePrixVente.getParametreTarif();
    if (parametreTarif == null) {
      throw new MessageErreurException("Les paramètres tarif ne sont pas valides.");
    }
    dateApplication = parametreTarif.getDateApplication();
    origineDateApplication = parametreTarif.getOrigineDate();
    listePrixVenteHT = parametreTarif.getListePrixVenteHT();
    
    // Les paramètres document de vente
    ParametreDocumentVente parametreDocumentVente = parametrePrixVente.getParametreDocumentVente();
    if (parametreDocumentVente != null) {
      codeDocumentVente = parametreDocumentVente.getCodeConditionVente();
      colonneTarifDocumentVente = parametreDocumentVente.getColonneTarif();
    }
    
    // La liste des paramètres des conditions de vente
    List<ParametreConditionVente> listeParametreConditionVente = parametrePrixVente.getListeParametreConditionVente();
    if (listeParametreConditionVente != null && !listeParametreConditionVente.isEmpty()) {
      listeConditionVente = new ListeConditionVente(modeNegoce, listeParametreConditionVente, parametreArticle, NOMBRE_DECIMALE_MONTANT);
    }
    
    // Autres initialisation
    colonneTarifUn = new ColonneTarif(Integer.valueOf(1), EnumOrigine.COLONNE_UN, modeNegoce);
    
    // Les champs saisis sont mis à null
    colonneTarifSaisie = null;
    prixBaseHTSaisi = null;
    prixNetHTSaisi = null;
    listePourcentageRemiseSaisi.clear();
  }
  
  /**
   * Détermine la colonne tarif permettant de récupérer le prix de vente.
   */
  private void determinerColonneTarif() {
    // La colonne saisie par l'utilisateur est prioritaire
    if (colonneTarifSaisie != null) {
      colonneTarifAUtiliser = colonneTarifSaisie;
      return;
    }
    // La colonne saisie dans la ligne du document de vente si le prix de vente concerne un document de vente
    if (codeDocumentVente != null && colonneTarifDocumentVente != null) {
      colonneTarifAUtiliser = colonneTarifDocumentVente;
      return;
    }
    // La colonne du client
    if (colonneTarifClient != null) {
      colonneTarifAUtiliser = colonneTarifClient;
      return;
    }
    // La colonne paramétrée dans la PS105
    if (colonneTarifPS105 != null) {
      colonneTarifAUtiliser = colonneTarifPS105;
      return;
    }
    // En dernier recours c'est la colonne 1 qui est utilisée
    colonneTarifAUtiliser = colonneTarifUn;
  }
  
  /**
   * Calcule le prix de base HT et le prix net NT pour toutes les conditions de ventes listées.
   */
  private void calculerPrixVenteConditionVente() {
    if (listeConditionVente == null || listeConditionVente.isEmpty()) {
      return;
    }
    
    // Calcule tous les prix net de toutes les conditions de vente
    listeConditionVente.calculerPrixVenteConditionVente(colonneTarifAUtiliser, listePrixVenteHT, valeurPS305, listePourcentageRemiseSaisi,
        coefficientMultiplicateurSaisi);
    // Balaye toutes les conditions de vente afin de trouver celle qui offre le meilleur prix de vente
    listeConditionVente.determinerMeilleurPrixVente();
    
    // Initialisation des variables de travail
    codeMeilleureConditionVente = null;
    prixBaseHTCNV = null;
    prixNetHTCNV = null;
    ConditionVente meilleureConditionVente = listeConditionVente.getMeilleureConditionVente();
    if (meilleureConditionVente != null) {
      codeMeilleureConditionVente = meilleureConditionVente.getParametreConditionVente().getCode() + " ("
          + meilleureConditionVente.getParametreConditionVente().getOrigine().getLibelle() + ')';
      prixBaseHTCNV = meilleureConditionVente.getPrixBaseHT();
      prixNetHTCNV = meilleureConditionVente.getPrixNetHT();
    }
  }
  
  /**
   * Calcule le prix base HT à partir colonne un.
   */
  private void calculerPrixBaseHTColonneUn() {
    if (colonneTarifUn == null) {
      return;
    }
    prixBaseHTColonneTarif = retournerPrixVenteAvecColonne(colonneTarifUn.getNumero(), listePrixVenteHT, valeurPS305);
  }
  
  /**
   * Détermine le prix de base HT à utiliser.
   */
  private void determinerPrixBaseHT() {
    // Le prix de base saisi par l'utilisateur est prioritaire sur les autres
    if (prixBaseHTSaisi != null) {
      prixBaseHT = prixBaseHTSaisi;
      originePrixBaseHT = "Prix base saisi";
      return;
    }
    
    // Prix base colonne 1
    prixBaseHT = prixBaseHTColonneTarif;
    originePrixBaseHT = "Prix colonne 1";
  }
  
  /**
   * Détermine la meilleure remise à appliquer.
   */
  private void determinerPourcentageRemise() {
    // Le pourcentage saisi est prioritaire
    if (listePourcentageRemiseSaisi != null && !listePourcentageRemiseSaisi.isEmpty()) {
      listePourcentageRemise = listePourcentageRemiseSaisi;
      originePourcentageRemise = EnumOrigine.SAISI;
      return;
    }
    
    // Les pourcentages du client
    listePourcentageRemise = listePourcentageRemiseClient;
    originePourcentageRemise = EnumOrigine.CLIENT;
    
    // Les pourcentages de la CNV sélectionnée (pour l'instant)
    if (codeConditionVenteSelectionnee != null) {
      listePourcentageRemise = listePourcentageRemiseCNV;
      originePourcentageRemise = EnumOrigine.CONDITION_VENTE;
    }
  }
  
  /**
   * Retourne le prix net HT à partir de la colonne tarif déduite.
   */
  private void calculerPrixNetHTAvecColonneTarifDeduite() {
    if (colonneTarifAUtiliser == null) {
      throw new MessageErreurException("Le numéro de la colonne tarif déduite est invalide.");
    }
    prixNetHTColonneTarif = retournerPrixVenteAvecColonne(colonneTarifAUtiliser.getNumero(), listePrixVenteHT, valeurPS305);
  }
  
  /**
   * Calcule le prix net HT calculé.
   * Les remises sont calculées en cascades car dans la fiche client il n'y a pas de top pour le définir et en saisi il n'y en a qu'une.
   */
  private void calculerPrixNetHTCalcule() {
    // Le prix net calculé avec le pourcentage de la remise à utiliser (saisie ou client)
    if (listePourcentageRemise != null && !listePourcentageRemise.isEmpty()) {
      prixNetHTCalcule =
          FormulePrixNet.calculerAvecListeRemiseEnPourcentage(prixBaseHT, listePourcentageRemise, null, NOMBRE_DECIMALE_MONTANT, true);
    }
  }
  
  /**
   * Détermine le meilleur prix net HT parmi l'ensemble des prix net HT disponibles.
   */
  private void determinerPrixNetHT() {
    // Contrôle du prix de base
    if (prixBaseHT == null) {
      throw new MessageErreurException("Le prix de base HT est invalide.");
    }
    
    // Détermination du meilleur prix net HT
    // Si le prix net a été saisi alors c'est lui qui est pris en compte
    if (prixNetHTSaisi != null) {
      prixNetHT = prixNetHTSaisi;
      originePrixNetHT = "Prix net saisi";
      return;
    }
    
    // Sinon comparaison des prix net et le plus petit gagne car c'est le meilleur prix
    // Prix de base
    prixNetHT = prixNetHTColonneTarif;
    originePrixNetHT = "Prix net colonne " + colonneTarifAUtiliser.getNumero();
    // Prix net calculé
    if (prixNetHTCalcule != null && prixNetHTCalcule.compareTo(prixNetHT) < 0) {
      prixNetHT = prixNetHTCalcule;
      originePrixNetHT = "Prix net calculé";
    }
    // Prix net des CNV
    if (prixNetHTCNV != null && prixNetHTCNV.compareTo(prixNetHT) < 0 || codeConditionVenteSelectionnee != null) {
      prixNetHT = prixNetHTCNV;
      originePrixNetHT = "Prix net CNV";
    }
  }
  
  /**
   * Contrôle la validité d'un montant.
   * @return
   */
  private BigDecimal controlerValeurMontant(BigDecimal pMontant) {
    if (pMontant == null) {
      return null;
    }
    if (pMontant.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Le montant doit être supérieur ou égal à zéro.");
    }
    return pMontant;
  }
  
  /**
   * Contrôle la validité d'un pourcentage.
   * @return
   */
  private BigDecimal controlerValeurPourcentage(BigDecimal pPourcentage) {
    if (pPourcentage == null) {
      return null;
    }
    if (pPourcentage.compareTo(BigDecimal.ZERO) < 0 || pPourcentage.compareTo(Constantes.VALEUR_CENT) > 0) {
      throw new MessageErreurException("Le pourcentage doit être supérieur ou égal à zéro et inférieur ou égal à 100.");
    }
    return pPourcentage.setScale(OutilCalculPrix.NOMBRE_DECIMALE_POURCENTAGE, RoundingMode.HALF_UP);
  }
  
  // -- Accesseurs
  
  /**
   * Retourne les paramètres du prix de vente.
   * @return
   */
  public ParametrePrixVente getParametrePrixVente() {
    return parametrePrixVente;
  }
  
  /**
   * Retourne le code de la devise.
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Retourne l'origine de la devise.
   * @return
   */
  public String getOrigineDevise() {
    return origineDevise;
  }
  
  /**
   * Retourne la colonne saisie.
   * @return
   */
  public ColonneTarif getColonneTarifSaisie() {
    return colonneTarifSaisie;
  }
  
  /**
   * Retourne la colonne du document de vente.
   * @return
   */
  public ColonneTarif getColonneTarifDocumentVente() {
    return colonneTarifDocumentVente;
  }
  
  /**
   * Retourne la colonne de la condition de vente.
   * @return
   */
  public ColonneTarif getColonneTarifConditionVente() {
    return colonneTarifConditionVente;
  }
  
  /**
   * Retourne la colonne tarif 1 du client.
   * @return
   */
  public ColonneTarif getColonneTarif1Client() {
    return colonneTarifClient;
  }
  
  /**
   * Retourne la colonne tarif de la PS105.
   * @return
   */
  public ColonneTarif getColonneTarifPS105() {
    return colonneTarifPS105;
  }
  
  /**
   * Retourne la colonne tarif Un.
   * @return
   */
  public ColonneTarif getColonneTarifUn() {
    return colonneTarifUn;
  }
  
  /**
   * Retourne la colonne tarif à utiliser.
   * @return
   */
  public ColonneTarif getColonneTarifAUtiliser() {
    return colonneTarifAUtiliser;
  }
  
  /**
   * Retourne le prix de base HT saisi.
   * @return
   */
  public BigDecimal getPrixBaseHTSaisi() {
    return prixBaseHTSaisi;
  }
  
  /**
   * Retourne le prix de base HT de la condition de vente.
   * @return
   */
  public BigDecimal getPrixBaseHTCNV() {
    return prixBaseHTCNV;
  }
  
  /**
   * Retourne le prix de base Ht est le pump de l'article.
   * @return
   */
  public BigDecimal getPrixBaseHTPump() {
    return prixBaseHTPump;
  }
  
  /**
   * Retourne le prix de base HT est le prix de revient de l'article.
   * @return
   */
  public BigDecimal getPrixBaseHTPrixRevient() {
    return prixBaseHTPrixRevient;
  }
  
  /**
   * Retourne le prix de base HT est la colonne tarif.
   * @return
   */
  public BigDecimal getPrixBaseHTColonneTarif() {
    return prixBaseHTColonneTarif;
  }
  
  /**
   * Retourne le prix de base HT à utiliser.
   * @return
   */
  public BigDecimal getPrixBaseHT() {
    return prixBaseHT;
  }
  
  /**
   * Retourne l'origine du prix de base HT à utiliser.
   * @return
   */
  public String getOriginePrixBaseHT() {
    return originePrixBaseHT;
  }
  
  /**
   * Retourne le pourcentage de remise saisi.
   * @return
   */
  public BigDecimal getPourcentageRemiseSaisi() {
    if (listePourcentageRemiseSaisi != null && !listePourcentageRemiseSaisi.isEmpty()) {
      return listePourcentageRemiseSaisi.get(0);
    }
    return null;
  }
  
  /**
   * Retourne la liste des pourcentages de remises client.
   * @return
   */
  public List<BigDecimal> getListePourcentageRemiseClient() {
    return listePourcentageRemiseClient;
  }
  
  /**
   * Retourne la liste des pourcentages de remises CNV.
   * @return
   */
  public List<BigDecimal> getListePourcentageRemiseCNV() {
    return listePourcentageRemiseCNV;
  }
  
  /**
   * Retourne la liste des pourcentages de remises à appliquer.
   * @return
   */
  public List<BigDecimal> getListePourcentageRemise() {
    return listePourcentageRemise;
  }
  
  /**
   * Retourne l'origine du pourcentage de remise à appliquer.
   * @return
   */
  public EnumOrigine getOriginePourcentageRemise() {
    return originePourcentageRemise;
  }
  
  /**
   * Retourne le prix net HT saisi.
   * @return
   */
  public BigDecimal getPrixNetHTSaisi() {
    return prixNetHTSaisi;
  }
  
  public BigDecimal getPrixNetHTColonneTarif() {
    return prixNetHTColonneTarif;
  }
  
  /**
   * Retourne le prix net calculé HT.
   * @return
   */
  public BigDecimal getPrixNetHTCalcule() {
    return prixNetHTCalcule;
  }
  
  /**
   * Retourne le meilleur prix net HT des conditions de vente.
   * @return
   */
  public BigDecimal getPrixNetHTCNV() {
    return prixNetHTCNV;
  }
  
  /**
   * Retourne le meilleur prix net HT.
   * @return
   */
  public BigDecimal getPrixNetHT() {
    return prixNetHT;
  }
  
  /**
   * Retourne l'origine du prix net HT à utiliser.
   * @return
   */
  public String getOriginePrixNetHT() {
    return originePrixNetHT;
  }
  
  /**
   * Retourne le code de la meilleure condition de ventes.
   * @return
   */
  public String getCodeMeilleureConditionVente() {
    return codeMeilleureConditionVente;
  }
  
  /**
   * Retourne le code de la condition de ventes qui a été sélectionnée.
   * @return
   */
  public String getCodeConditionVenteSelectionnee() {
    return codeConditionVenteSelectionnee;
  }
}
