/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.parametre;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Classe regroupant tous les paramètres du tarif article pour le calcul du prix de vente.
 */
public class ParametreTarif {
  // Variables
  private Date dateApplication = null;
  private String origineDate = null;
  private List<BigDecimal> listePrixVenteHT = null;
  private String codeDevise = null;
  
  // -- Accesseurs
  
  /**
   * Retourne la date d'application du tarif.
   * @return
   */
  public Date getDateApplication() {
    return dateApplication;
  }
  
  /**
   * Initialise la date d'application du tarif.
   * @param pDateApplication
   */
  public void setDateApplication(Date pDateApplication) {
    this.dateApplication = pDateApplication;
  }
  
  /**
   * Retourne l'origine de la date d'application.
   * @return
   */
  public String getOrigineDate() {
    return origineDate;
  }
  
  /**
   * Initialise l'origine de la date d'application.
   * @param pOrigineDate
   */
  public void setOrigineDate(String pOrigineDate) {
    this.origineDate = pOrigineDate;
  }
  
  /**
   * Retourne la liste des prix de vente des colonnes.
   * @return
   */
  public List<BigDecimal> getListePrixVenteHT() {
    return listePrixVenteHT;
  }
  
  /**
   * Initialise la liste des prix de vente des colonnes.
   * @param pListePrixVenteHT
   */
  public void setListeTarif(List<BigDecimal> pListePrixVenteHT) {
    this.listePrixVenteHT = pListePrixVenteHT;
  }
  
  /**
   * Retourne le code devise.
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Initialise le code devise.
   * @param pCodeDevise
   */
  public void setCodeDevise(String pCodeDevise) {
    this.codeDevise = pCodeDevise;
  }
  
}
