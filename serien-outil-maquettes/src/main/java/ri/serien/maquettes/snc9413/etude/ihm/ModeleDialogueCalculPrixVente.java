/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.ihm;

import java.math.BigDecimal;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;
import ri.serien.maquettes.snc9413.etude.metier.CalculPrixVente;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametrePrixVente;

/**
 * Modèle de la boîte de dialogue de calcul des prix de ventes.
 */
public class ModeleDialogueCalculPrixVente extends AbstractModeleDialogue {
  // Variables
  private CalculPrixVente calculPrixVente = null;
  
  /**
   * Constructeur.
   */
  public ModeleDialogueCalculPrixVente(ParametrePrixVente pParametrePrixVente) {
    super(null);
    calculPrixVente = new CalculPrixVente(pParametrePrixVente);
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  /**
   * Charge les données nécessaires.
   */
  @Override
  public void chargerDonnees() {
    calculPrixVente.calculer();
  }
  
  /**
   * Modifie le numméro de la colonne saisie.
   * 
   * @param pNumeroColonne
   */
  public void modifierColonneSaisie(String pNumeroColonne) {
    if (calculPrixVente == null) {
      return;
    }
    Integer numeroColonne = Constantes.convertirTexteEnInteger(pNumeroColonne);
    // Si le numéro de colonne est inférieure ou égale à 0 alors sa valeur est mise à null
    if (numeroColonne.intValue() <= 0) {
      numeroColonne = null;
    }
    calculPrixVente.modifierColonneSaisie(numeroColonne);
    rafraichir();
  }
  
  /**
   * Modifie le prix de base HT saisi.
   * 
   * @param pPrixBaseHT
   */
  public void modifierPrixBaseHTSaisi(BigDecimal pPrixBaseHT) {
    if (calculPrixVente == null) {
      return;
    }
    calculPrixVente.modifierPrixBaseHTSaisi(pPrixBaseHT);
    rafraichir();
  }
  
  /**
   * Modifie le prix net HT saisi.
   * 
   * @param pPrixNetHT
   */
  public void modifierPrixNetHTSaisi(BigDecimal pPrixNetHT) {
    if (calculPrixVente == null) {
      return;
    }
    calculPrixVente.modifierPrixNetHTSaisi(pPrixNetHT);
    rafraichir();
  }
  
  /**
   * Modifie le pourcentage de remise saisi.
   * 
   * @param pPourcentageRemise
   */
  public void modifierPourcentageRemiseSaisi(String pPourcentageRemise) {
    if (calculPrixVente == null) {
      return;
    }
    BigDecimal pourcentage = Constantes.convertirTexteEnBigDecimal(pPourcentageRemise);
    // Si le pourcentage de remise est inférieur ou égale à 0 ou supérieur à 100 alors sa valeur est mise à null
    if (pourcentage.compareTo(BigDecimal.ZERO) <= 0 || pourcentage.compareTo(Constantes.VALEUR_CENT) > 0) {
      pourcentage = null;
    }
    calculPrixVente.modifierPourcentageRemiseSaisi(pourcentage);
    rafraichir();
  }
  
  /**
   * Modifie la condition de ventes sélectionnée.
   * 
   * @param pCodeCNV
   */
  public void modifierCNVSelectionne(String pCodeCNV) {
    if (calculPrixVente == null) {
      return;
    }
    if (Constantes.equals(pCodeCNV, calculPrixVente.getCodeConditionVenteSelectionnee())) {
      return;
    }
    calculPrixVente.modifierConditionVenteSelectionnee(pCodeCNV);
    rafraichir();
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  /**
   * Retourne le calcul du prix de vente.
   * @return
   */
  public CalculPrixVente getCalculPrixVente() {
    return calculPrixVente;
  }
  
}
