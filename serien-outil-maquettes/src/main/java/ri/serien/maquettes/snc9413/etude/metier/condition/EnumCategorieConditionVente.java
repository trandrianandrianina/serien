/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.condition;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des catégories des conditions de vente.
 * TODO
 * Mettre à jour la classe ri.serien.libcommun.gescom.commun.client.Negociation qui utilise un type String pour la catégorie.
 */
public enum EnumCategorieConditionVente {
  NORMALE(' ', "Normale"),
  QUANTITATIVE('Q', "Quantitative"),
  DEROGATION('D', "Dérogation"),
  FLASH('F', "Flash"),
  NEGOCIE('N', "Négociée"),
  CHANTIER('H', "Chantier"),
  AFFAIRE('E', "Affaire"),
  CLIENT('C', "Client"),
  ZONE_GEOGRAPHIQUE('g', "Zone géographique"),
  PAYS('p', "Pays"),
  CATEGORIE_CLIENT('c', "Catégorie client"),
  CANAL('K', "Canal");
  // ???('z', "???") <- A préciser
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumCategorieConditionVente(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(code);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumCategorieConditionVente valueOfByCode(Character pCode) {
    for (EnumCategorieConditionVente value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le code de la catégorie de la condition de vente est invalide : " + pCode);
  }
  
}
