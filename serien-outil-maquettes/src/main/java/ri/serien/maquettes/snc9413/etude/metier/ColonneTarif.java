/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier;

import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe contient les informations qui décrivzent une colonne tarif.
 */
public class ColonneTarif {
  // Variables
  private boolean modeNegoce = false;
  // Le numéro réel de la colonne (dont la valeur commence à 1)
  private Integer numero = null;
  private EnumOrigine origine = null;
  
  /**
   * Constructeur.
   */
  public ColonneTarif(Integer pNumeroColonne, EnumOrigine pOrigine, boolean pModeNegoce) {
    modeNegoce = pModeNegoce;
    numero = controlerNumero(pNumeroColonne, modeNegoce);
    if (pOrigine == null) {
      throw new MessageErreurException("L'origine de la colonne tarif est invalide.");
    }
    origine = pOrigine;
  }
  
  // -- Méthodes publiques
  
  /**
   * Contrôle la validité d'une colonne tarif.
   * @return
   */
  public static Integer controlerNumero(Integer pNumeroColonne, boolean pModeNegoce) {
    if (pNumeroColonne == null) {
      throw new MessageErreurException("Le numéro de la colonne tarif est invalide.");
    }
    if (pNumeroColonne.intValue() <= 0) {
      throw new MessageErreurException("Le numéro de la colonne tarif doit être supérieur à 0.");
    }
    if (pModeNegoce) {
      if (pNumeroColonne <= TarifArticle.NOMBRE_COLONNES_MODE_NEGOCE) {
        return pNumeroColonne;
      }
      throw new MessageErreurException(
          "Le numéro de la colonne tarif doit être inférieur au égal à " + TarifArticle.NOMBRE_COLONNES_MODE_NEGOCE + '.');
    }
    else {
      if (pNumeroColonne > 0 && pNumeroColonne <= TarifArticle.NOMBRE_COLONNES_MODE_CLASSIQUE) {
        return pNumeroColonne;
      }
      throw new MessageErreurException(
          "Le numéro de la colonne tarif doit être inférieur au égal à " + TarifArticle.NOMBRE_COLONNES_MODE_CLASSIQUE + '.');
    }
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le numéro de la colonne.
   * @return
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Retourne l'origine de la colonne.
   * @return
   */
  public EnumOrigine getOrigine() {
    return origine;
  }
}
