/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.formulecalculprix;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Cette classe décrit une ligne d'une formule de calcul de prix du paramètre 'FP'.
 * Ces lignes seront stockées dans une liste dont l'ordre est important puisqu'il s'agit d'une formule.
 */
public class LigneFormulePrix {
  // Variables
  private String libelle = null;
  // Dans le programme SGVMFP si l'opérateur n'est pas + alors c'est -, à voir lors du chargement des données depuis les paramètres
  private EnumTypeOperateur typeOperateur = null;
  private EnumTypeValeur typeValeur = null;
  private BigDecimal valeur = null;
  
  // Faire un constructeur qui contrôle les 2 types afin qu'ils ne soient jamais null
  
  // -- Méthodes publiques
  
  /**
   * Applique la formule de la ligne.
   * @param pPrixNetHTLignePrecedente
   * @param pPrixBaseHT
   * @param pNombreDecimale
   * @return
   */
  public BigDecimal calculer(BigDecimal pPrixNetHTLignePrecedente, BigDecimal pPrixBaseHT, int pNombreDecimale) {
    if (pPrixBaseHT == null) {
      return null;
    }
    if (pPrixNetHTLignePrecedente == null) {
      pPrixNetHTLignePrecedente = pPrixBaseHT;
    }
    if (valeur == null || valeur.intValue() == 0) {
      return pPrixNetHTLignePrecedente.setScale(pNombreDecimale, RoundingMode.HALF_UP);
    }
    
    // Calculs
    BigDecimal resultat = BigDecimal.ZERO;
    // Application de la valeur
    switch (typeValeur) {
      // Valeur à ajouter
      case VALEUR:
        resultat = pPrixBaseHT.add(valeur).setScale(pNombreDecimale, RoundingMode.HALF_UP);
        break;
      
      // Valeur de type coefficient
      case COEFFICIENT:
        resultat = pPrixBaseHT.multiply(valeur).setScale(pNombreDecimale, RoundingMode.HALF_UP);
        break;
    }
    // Application de l'opérateur
    switch (typeOperateur) {
      // Addition du résultat précédent au prix net provenant du calcul de la ligne précédente
      case ADDITION:
        resultat = pPrixNetHTLignePrecedente.add(resultat).setScale(pNombreDecimale, RoundingMode.HALF_UP);
        break;
      // Soustraction du résultat précédent au prix net provenant du calcul de la ligne précédente
      case SOUSTRACTION:
        resultat = pPrixNetHTLignePrecedente.subtract(resultat).setScale(pNombreDecimale, RoundingMode.HALF_UP);
        break;
    }
    
    return resultat.setScale(pNombreDecimale, RoundingMode.HALF_UP);
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le libellé correspondant à la valeur.
   * @return
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Initialise le libellé.
   * @param libelle
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  /**
   * Retourne le type de l'opérateur.
   * @return
   */
  public EnumTypeOperateur getTypeOperateur() {
    return typeOperateur;
  }
  
  /**
   * Initialise le type de l'opérateur.
   * @param typeOperateur
   */
  public void setTypeOperateur(EnumTypeOperateur typeOperateur) {
    this.typeOperateur = typeOperateur;
  }
  
  /**
   * Retourne le type de la valeur.
   * @return
   */
  public EnumTypeValeur getTypeValeur() {
    return typeValeur;
  }
  
  /**
   * Initialise le type de la valeur.
   * @param typeValeur
   */
  public void setTypeValeur(EnumTypeValeur typeValeur) {
    this.typeValeur = typeValeur;
  }
  
  /**
   * Retourne la valeur.
   * @return
   */
  public BigDecimal getValeur() {
    return valeur;
  }
  
  /**
   * Initialise la valeur.
   * @param valeur
   */
  public void setValeur(BigDecimal valeur) {
    this.valeur = valeur;
  }
  
}
