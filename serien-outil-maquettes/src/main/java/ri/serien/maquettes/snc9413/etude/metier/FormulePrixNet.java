/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

import ri.serien.libcommun.outils.Constantes;

/**
 * Cette classe regroupe tous les calculs d'un prix net à partir de différents paramètres.
 * L'ensemble des méthodes ne retournent jamais une exception, les difféents contrôles doivent permettre d'éviter toutes erreurs.
 */
public class FormulePrixNet {
  
  // -- Méthodes publiques
  
  /**
   * Calcule le prix net HT à partir d'une remise en pourcentage.
   * 
   * @param pPrixBaseHT
   * @param pRemiseEnPourcentage
   * @param pNombreDecimale
   * @return
   */
  public static BigDecimal calculerAvecRemiseEnPourcentage(BigDecimal pPrixBaseHT, BigDecimal pRemiseEnPourcentage, int pNombreDecimale) {
    return calculerRemiseEnPourcentage(pPrixBaseHT, pRemiseEnPourcentage, pNombreDecimale);
  }
  
  /**
   * Calcule le prix net HT à partir d'une remise en valeur.
   * 
   * @param pPrixBaseHT
   * @param pRemiseEnValeur
   * @param pNombreDecimale
   * @return
   */
  public static BigDecimal calculerAvecRemiseEnValeur(BigDecimal pPrixBaseHT, BigDecimal pRemiseEnValeur, int pNombreDecimale) {
    if (pPrixBaseHT == null) {
      return null;
    }
    if (pRemiseEnValeur == null) {
      return pPrixBaseHT.setScale(pNombreDecimale, RoundingMode.HALF_UP);
    }
    return pPrixBaseHT.subtract(pRemiseEnValeur).setScale(pNombreDecimale, RoundingMode.HALF_UP);
  }
  
  /**
   * Calcule le prix net HT à partir d'un ajout en valeur.
   * 
   * @param pPrixBaseHT
   * @param pAjoutEnValeur
   * @param pNombreDecimale
   * @return
   */
  public static BigDecimal calculerAvecAjoutEnValeur(BigDecimal pPrixBaseHT, BigDecimal pAjoutEnValeur, int pNombreDecimale) {
    if (pPrixBaseHT == null) {
      return null;
    }
    if (pAjoutEnValeur == null) {
      return pPrixBaseHT.setScale(pNombreDecimale, RoundingMode.HALF_UP);
    }
    return pPrixBaseHT.subtract(pAjoutEnValeur).setScale(pNombreDecimale, RoundingMode.HALF_UP);
  }
  
  /**
   * Calcule le prix net HT à partir d'une liste de remise en pourcentage et d'un coefficient multiplicateur.
   *
   * @param pPrixBaseHT
   * @param pListeRemiseEnPourcentage
   * @param pCoefficientMultiplicateur
   * @param pNombreDecimale
   * @param pModeCascade sinon en ajout
   * @return
   */
  public static BigDecimal calculerAvecListeRemiseEnPourcentage(BigDecimal pPrixBaseHT, List<BigDecimal> pListeRemiseEnPourcentage,
      BigDecimal pCoefficientMultiplicateur, int pNombreDecimale, boolean pModeCascade) {
    if (pPrixBaseHT == null) {
      return null;
    }
    if (pListeRemiseEnPourcentage == null || pListeRemiseEnPourcentage.isEmpty()) {
      return calculerAvecCoefficientMultiplicateur(pPrixBaseHT, pCoefficientMultiplicateur, pNombreDecimale);
    }
    
    BigDecimal valeurHT = pPrixBaseHT;
    // Calcul des remises en cascade
    if (pModeCascade) {
      for (BigDecimal remise : pListeRemiseEnPourcentage) {
        valeurHT = calculerRemiseEnPourcentage(valeurHT, remise, pNombreDecimale);
      }
      return calculerAvecCoefficientMultiplicateur(valeurHT, pCoefficientMultiplicateur, pNombreDecimale);
    }
    
    // Calcul des remises en ajout
    BigDecimal valeurRemiseTotale = BigDecimal.ZERO.setScale(pNombreDecimale);
    for (BigDecimal remiseEnPourcentage : pListeRemiseEnPourcentage) {
      valeurHT = calculerRemiseEnPourcentage(pPrixBaseHT, remiseEnPourcentage, pNombreDecimale);
      valeurRemiseTotale = valeurRemiseTotale.add(valeurHT);
    }
    valeurHT = pPrixBaseHT.subtract(valeurRemiseTotale).setScale(pNombreDecimale, RoundingMode.HALF_UP);
    return calculerAvecCoefficientMultiplicateur(valeurHT, pCoefficientMultiplicateur, pNombreDecimale);
  }
  
  /**
   * Calcule le prix net HT à partir d'un coefficient multiplicateur.
   *
   * @param pPrixBaseHT
   * @param pCoefficientMultiplicateur
   * @param pNombreDecimale
   * @return
   */
  public static BigDecimal calculerAvecCoefficientMultiplicateur(BigDecimal pPrixBaseHT, BigDecimal pCoefficientMultiplicateur,
      int pNombreDecimale) {
    if (pPrixBaseHT == null) {
      return null;
    }
    if (pCoefficientMultiplicateur == null) {
      return pPrixBaseHT.setScale(pNombreDecimale, RoundingMode.HALF_UP);
    }
    return pPrixBaseHT.multiply(pCoefficientMultiplicateur).setScale(pNombreDecimale, RoundingMode.HALF_UP);
  }
  
  // -- Méthodes privées
  
  /**
   * Calcule un prix à partir d'une remise en pourcentage.
   * 
   * @param pPrix
   * @param pRemisePourcentage
   * @return
   */
  private static BigDecimal calculerRemiseEnPourcentage(BigDecimal pPrix, BigDecimal pRemisePourcentage, int pNombreDecimale) {
    if (pPrix == null) {
      return null;
    }
    if (pRemisePourcentage == null || pRemisePourcentage.intValue() == 0) {
      return pPrix.setScale(pNombreDecimale, RoundingMode.HALF_UP);
    }
    
    BigDecimal coefficient = BigDecimal.ONE.subtract(pRemisePourcentage.divide(Constantes.VALEUR_CENT, MathContext.DECIMAL64));
    return pPrix.multiply(coefficient).setScale(pNombreDecimale, RoundingMode.HALF_UP);
  }
}
