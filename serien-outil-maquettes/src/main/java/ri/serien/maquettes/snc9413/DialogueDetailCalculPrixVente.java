/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue "Message d'information".
 * Elle permet d'afficher des messages simples à l'utilisateur. Celui-ci peut uniquement cliquer sur un bouton "Fermer" pour fermer
 * la boîte de dialogue.
 */
public class DialogueDetailCalculPrixVente extends AbstractVueDialogue<ModeleDialogueCalculPrixVente> {
  
  // Variables
  private static String BOUTON_OUI = "Oui";
  private static String BOUTON_NON = "Non";
  
  /**
   * Contructeur.
   */
  public DialogueDetailCalculPrixVente(ModeleDialogueCalculPrixVente pModele) {
    super(pModele);
  }
  
  /**
   * Afficher la boîte de dialogue avec un message.
   * A utiliser uniquement si la variante avec Component n'est pas utilisable.
   */
  public static void afficher(String pMessage) {
    ModeleDialogueCalculPrixVente modele = new ModeleDialogueCalculPrixVente();
    DialogueDetailCalculPrixVente vue = new DialogueDetailCalculPrixVente(modele);
    modele.modifierMessage(pMessage);
    vue.afficher();
  }
  
  /**
   * Afficher la boîte de dialogue avec un message sans subir le formatage Html par défaut.
   * A utiliser uniquement si la variante avec Component n'est pas utilisable.
   */
  public static void afficherTexteNonCentre(String pMessage) {
    ModeleDialogueCalculPrixVente modele = new ModeleDialogueCalculPrixVente();
    DialogueDetailCalculPrixVente vue = new DialogueDetailCalculPrixVente(modele);
    modele.setCentrerTexte(false);
    modele.modifierMessage(pMessage);
    vue.afficher();
  }
  
  /**
   * Initialiser les composants graphiques de la boîte de dialogue.
   */
  @Override
  public void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_OUI, 'o', true);
    snBarreBouton.ajouterBouton(BOUTON_NON, 'n', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Rafraichissement des données à l'écran.
   */
  @Override
  public void rafraichir() {
  }
  
  // Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    sNLabelChamp1 = new SNLabelChamp();
    sNTexte1 = new SNTexte();
    sNLabelChamp2 = new SNLabelChamp();
    sNTexte2 = new SNTexte();
    sNLabelMessage1 = new SNLabelTitre();
    sNTexte3 = new SNTexte();
    sNLabelChamp3 = new SNLabelChamp();
    sNTexte4 = new SNTexte();
    sNLabelChamp4 = new SNLabelChamp();
    sNTexte5 = new SNTexte();
    sNLabelChamp5 = new SNLabelChamp();
    sNTexte6 = new SNTexte();
    sNLabelMessage2 = new SNLabelTitre();
    sNTexte7 = new SNTexte();
    sNLabelChamp6 = new SNLabelChamp();
    sNTexte8 = new SNTexte();
    sNLabelChamp7 = new SNLabelChamp();
    sNTexte9 = new SNTexte();
    sNLabelChamp8 = new SNLabelChamp();
    sNTexte10 = new SNTexte();
    sNLabelChamp9 = new SNLabelChamp();
    sNTexte11 = new SNTexte();
    sNLabelMessage3 = new SNLabelTitre();
    sNTexte12 = new SNTexte();
    sNLabelChamp10 = new SNLabelChamp();
    sNTexte13 = new SNTexte();
    sNLabelChamp11 = new SNLabelChamp();
    sNTexte14 = new SNTexte();
    sNLabelChamp12 = new SNLabelChamp();
    sNTexte15 = new SNTexte();
    sNLabelChamp13 = new SNLabelChamp();
    sNTexte16 = new SNTexte();
    sNLabelMessage4 = new SNLabelTitre();
    sNTexte17 = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("D\u00e9tail du calcul du prix de vente");
    setBackground(new Color(238, 238, 210));
    setResizable(false);
    setModal(true);
    setMinimumSize(new Dimension(350, 750));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBorder(null);
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setOpaque(true);
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setOpaque(false);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- sNLabelChamp1 ----
        sNLabelChamp1.setText("Colonne tarif client");
        sNLabelChamp1.setName("sNLabelChamp1");
        pnlContenu.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte1 ----
        sNTexte1.setEnabled(false);
        sNTexte1.setName("sNTexte1");
        pnlContenu.add(sNTexte1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp2 ----
        sNLabelChamp2.setText("Colonne tarif saisi");
        sNLabelChamp2.setName("sNLabelChamp2");
        pnlContenu.add(sNLabelChamp2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte2 ----
        sNTexte2.setEnabled(false);
        sNTexte2.setName("sNTexte2");
        pnlContenu.add(sNTexte2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelMessage1 ----
        sNLabelMessage1.setText("Colonne tarif");
        sNLabelMessage1.setImportanceMessage(EnumImportanceMessage.MOYEN);
        sNLabelMessage1.setName("sNLabelMessage1");
        pnlContenu.add(sNLabelMessage1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte3 ----
        sNTexte3.setEnabled(false);
        sNTexte3.setName("sNTexte3");
        pnlContenu.add(sNTexte3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp3 ----
        sNLabelChamp3.setText("Prix base tarif");
        sNLabelChamp3.setName("sNLabelChamp3");
        pnlContenu.add(sNLabelChamp3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte4 ----
        sNTexte4.setEnabled(false);
        sNTexte4.setName("sNTexte4");
        pnlContenu.add(sNTexte4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp4 ----
        sNLabelChamp4.setText("Prix base CNV");
        sNLabelChamp4.setName("sNLabelChamp4");
        pnlContenu.add(sNLabelChamp4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte5 ----
        sNTexte5.setEnabled(false);
        sNTexte5.setName("sNTexte5");
        pnlContenu.add(sNTexte5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp5 ----
        sNLabelChamp5.setText("Prix de base saisi");
        sNLabelChamp5.setName("sNLabelChamp5");
        pnlContenu.add(sNLabelChamp5, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte6 ----
        sNTexte6.setEnabled(false);
        sNTexte6.setName("sNTexte6");
        pnlContenu.add(sNTexte6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelMessage2 ----
        sNLabelMessage2.setText("Prix base");
        sNLabelMessage2.setImportanceMessage(EnumImportanceMessage.MOYEN);
        sNLabelMessage2.setName("sNLabelMessage2");
        pnlContenu.add(sNLabelMessage2, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte7 ----
        sNTexte7.setEnabled(false);
        sNTexte7.setName("sNTexte7");
        pnlContenu.add(sNTexte7, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp6 ----
        sNLabelChamp6.setText("Coefficient CNV");
        sNLabelChamp6.setName("sNLabelChamp6");
        pnlContenu.add(sNLabelChamp6, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte8 ----
        sNTexte8.setEnabled(false);
        sNTexte8.setName("sNTexte8");
        pnlContenu.add(sNTexte8, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp7 ----
        sNLabelChamp7.setText("Pourcentage remise CNV");
        sNLabelChamp7.setPreferredSize(new Dimension(200, 30));
        sNLabelChamp7.setName("sNLabelChamp7");
        pnlContenu.add(sNLabelChamp7, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte9 ----
        sNTexte9.setEnabled(false);
        sNTexte9.setName("sNTexte9");
        pnlContenu.add(sNTexte9, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp8 ----
        sNLabelChamp8.setText("Mode cumul remise");
        sNLabelChamp8.setName("sNLabelChamp8");
        pnlContenu.add(sNLabelChamp8, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte10 ----
        sNTexte10.setEnabled(false);
        sNTexte10.setName("sNTexte10");
        pnlContenu.add(sNTexte10, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp9 ----
        sNLabelChamp9.setText("Pourcentage remise saisi");
        sNLabelChamp9.setName("sNLabelChamp9");
        pnlContenu.add(sNLabelChamp9, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte11 ----
        sNTexte11.setEnabled(false);
        sNTexte11.setName("sNTexte11");
        pnlContenu.add(sNTexte11, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelMessage3 ----
        sNLabelMessage3.setText("Pourcentage remise");
        sNLabelMessage3.setImportanceMessage(EnumImportanceMessage.MOYEN);
        sNLabelMessage3.setName("sNLabelMessage3");
        pnlContenu.add(sNLabelMessage3, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte12 ----
        sNTexte12.setEnabled(false);
        sNTexte12.setName("sNTexte12");
        pnlContenu.add(sNTexte12, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp10 ----
        sNLabelChamp10.setText("Prix net base");
        sNLabelChamp10.setName("sNLabelChamp10");
        pnlContenu.add(sNLabelChamp10, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte13 ----
        sNTexte13.setEnabled(false);
        sNTexte13.setName("sNTexte13");
        pnlContenu.add(sNTexte13, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp11 ----
        sNLabelChamp11.setText("Prix net calcul\u00e9");
        sNLabelChamp11.setName("sNLabelChamp11");
        pnlContenu.add(sNLabelChamp11, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte14 ----
        sNTexte14.setEnabled(false);
        sNTexte14.setName("sNTexte14");
        pnlContenu.add(sNTexte14, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp12 ----
        sNLabelChamp12.setText("Prix net CNV");
        sNLabelChamp12.setName("sNLabelChamp12");
        pnlContenu.add(sNLabelChamp12, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte15 ----
        sNTexte15.setEnabled(false);
        sNTexte15.setName("sNTexte15");
        pnlContenu.add(sNTexte15, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp13 ----
        sNLabelChamp13.setText("Prix net saisi");
        sNLabelChamp13.setName("sNLabelChamp13");
        pnlContenu.add(sNLabelChamp13, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte16 ----
        sNTexte16.setEnabled(false);
        sNTexte16.setName("sNTexte16");
        pnlContenu.add(sNTexte16, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelMessage4 ----
        sNLabelMessage4.setText("Prix net");
        sNLabelMessage4.setImportanceMessage(EnumImportanceMessage.MOYEN);
        sNLabelMessage4.setName("sNLabelMessage4");
        pnlContenu.add(sNLabelMessage4, new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNTexte17 ----
        sNTexte17.setEnabled(false);
        sNTexte17.setName("sNTexte17");
        pnlContenu.add(sNTexte17, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNLabelChamp sNLabelChamp1;
  private SNTexte sNTexte1;
  private SNLabelChamp sNLabelChamp2;
  private SNTexte sNTexte2;
  private SNLabelTitre sNLabelMessage1;
  private SNTexte sNTexte3;
  private SNLabelChamp sNLabelChamp3;
  private SNTexte sNTexte4;
  private SNLabelChamp sNLabelChamp4;
  private SNTexte sNTexte5;
  private SNLabelChamp sNLabelChamp5;
  private SNTexte sNTexte6;
  private SNLabelTitre sNLabelMessage2;
  private SNTexte sNTexte7;
  private SNLabelChamp sNLabelChamp6;
  private SNTexte sNTexte8;
  private SNLabelChamp sNLabelChamp7;
  private SNTexte sNTexte9;
  private SNLabelChamp sNLabelChamp8;
  private SNTexte sNTexte10;
  private SNLabelChamp sNLabelChamp9;
  private SNTexte sNTexte11;
  private SNLabelTitre sNLabelMessage3;
  private SNTexte sNTexte12;
  private SNLabelChamp sNLabelChamp10;
  private SNTexte sNTexte13;
  private SNLabelChamp sNLabelChamp11;
  private SNTexte sNTexte14;
  private SNLabelChamp sNLabelChamp12;
  private SNTexte sNTexte15;
  private SNLabelChamp sNLabelChamp13;
  private SNTexte sNTexte16;
  private SNLabelTitre sNLabelMessage4;
  private SNTexte sNTexte17;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
