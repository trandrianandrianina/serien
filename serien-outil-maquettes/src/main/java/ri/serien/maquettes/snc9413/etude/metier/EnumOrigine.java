/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des origines possibles d'un prix de vente (base ou net), d'un coefficient, d'une remise, ...
 */
public enum EnumOrigine {
  SAISI(0, "Saisi"),
  DOCUMENT_VENTE(1, "Document de vente"),
  CONDITION_VENTE(2, "Condition de vente"),
  TARIF(3, "Tarif"),
  CLIENT(4, "Client"),
  PS105(5, "PS105"),
  COLONNE_UN(6, "1"),
  DESCRIPTION_GENERALE(7, "Description générale");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrigine(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(numero);
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumOrigine valueOfByCode(Integer pNumero) {
    for (EnumOrigine value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("L'origine du prix de vente est invalide : " + pNumero);
  }
  
}
