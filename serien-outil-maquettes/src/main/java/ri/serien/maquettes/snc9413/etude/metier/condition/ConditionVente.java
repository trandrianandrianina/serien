/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.condition;

import java.math.BigDecimal;
import java.util.List;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.maquettes.snc9413.etude.metier.CalculPrixVente;
import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;
import ri.serien.maquettes.snc9413.etude.metier.EnumOrigine;
import ri.serien.maquettes.snc9413.etude.metier.FormulePrixNet;
import ri.serien.maquettes.snc9413.etude.metier.StockagePrixVente;
import ri.serien.maquettes.snc9413.etude.metier.formulecalculprix.FormulePrixFP;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreArticle;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreConditionVente;

public class ConditionVente {
  // Constantes
  private static final BigDecimal UTILISER_PUMP = new BigDecimal(98);
  private static final BigDecimal UTILISER_PRV = new BigDecimal(99);
  
  // Variables
  private boolean modeNegoce = false;
  private ParametreConditionVente parametreConditionVente = null;
  private StockagePrixVente stockagePrixVente = null;
  private ColonneTarif colonneTarif = null;
  private EnumOrigine origineRemiseEnPourcentage = null;
  private EnumOrigine origineCoefficientMultiplicateur = null;
  private BigDecimal pumpHTArticle = null;
  private BigDecimal prixDeRevientHT = null;
  private BigDecimal prixDeRevientHTStandardCNA = null;
  
  /**
   * Constructeur.
   */
  public ConditionVente(boolean pModeNegoce, ParametreConditionVente pParametreConditionVente, ParametreArticle pParametreArticle,
      int pNombreDecimale) {
    if (pParametreConditionVente == null) {
      throw new MessageErreurException("Les paramètres de la condition de vente sont invalides.");
    }
    modeNegoce = pModeNegoce;
    parametreConditionVente = pParametreConditionVente;
    stockagePrixVente = new StockagePrixVente(pNombreDecimale);
    
    // Paramètres provenant des paramètres de l'article
    if (pParametreArticle != null) {
      pumpHTArticle = pParametreArticle.getPumpHTArticle();
      prixDeRevientHT = pParametreArticle.getPrixDeRevientHT();
      prixDeRevientHTStandardCNA = pParametreArticle.getPrixDeRevientHTStandardCNA();
    }
  }
  
  // -- Méthodes publiques
  
  /**
   * Calcule le prix de base HT de cette condition.
   * Attention l'ordre de la séquence dans la recherche est primordial.
   * @return
   */
  public void calculerPrixBaseHT(List<BigDecimal> pListePrixVenteHT, Boolean pValeurPS305) {
    EnumTypeConditionVente typeConditionVente = parametreConditionVente.getType();
    
    // Si la condition est de type prix de base
    if (typeConditionVente != null && typeConditionVente == EnumTypeConditionVente.PRIX_DE_BASE) {
      BigDecimal prixBaseHT = parametreConditionVente.getPrixBaseHT();
      if (prixBaseHT == null) {
        throw new MessageErreurException(
            "Le prix de base HT de la condition de vente " + parametreConditionVente.getCode() + " est invalide.");
      }
      setPrixBaseHT(prixBaseHT);
      setOriginePrixBase(EnumOrigine.CONDITION_VENTE);
      return;
    }
    
    // Si la condition indique qu'il faut utiliser le PUMP ou le PRV/PRS
    if (typeConditionVente != null
        && (typeConditionVente == EnumTypeConditionVente.COEFFICIENT
            || typeConditionVente == EnumTypeConditionVente.REMISE_EN_POURCENTAGE)
        || typeConditionVente == EnumTypeConditionVente.FORMULE_PRIX) {
      // Le contenu de la variable va définir quelle valeur utiliser pour initialiser le prix de base
      BigDecimal t1val = parametreConditionVente.getT1val();
      if (t1val == null) {
        t1val = BigDecimal.ZERO;
      }
      
      // Utilise le Pump (reste encore à affiner)
      if (t1val.compareTo(UTILISER_PUMP) == 0) {
        setPrixBaseHT(pumpHTArticle);
        setOriginePrixBase(EnumOrigine.CONDITION_VENTE);
        return;
      }
      // Utilise le PRV ou le PRS (reste encore à affiner)
      if (t1val.compareTo(UTILISER_PRV) == 0) {
        // En mode négoce si le PRS est initialisé alors il est utilisé comme prix de base
        if (modeNegoce && prixDeRevientHTStandardCNA != null) {
          setPrixBaseHT(prixDeRevientHTStandardCNA);
          setOriginePrixBase(EnumOrigine.CONDITION_VENTE);
          return;
        }
        // Sinon c'est le prix de revient
        if (prixDeRevientHT != null) {
          setPrixBaseHT(prixDeRevientHT);
          setOriginePrixBase(EnumOrigine.CONDITION_VENTE);
          return;
        }
      }
      // La valeur contient la colonne tarif à utiliser
      if (t1val.intValue() > 0 && t1val.intValue() <= 10) {
        colonneTarif = new ColonneTarif(t1val.intValue(), EnumOrigine.CONDITION_VENTE, modeNegoce);
      }
    }
    
    // Sinon il faut utiliser la colonne tarif
    // Sinon c'est la colonne passée en paramètre
    if (colonneTarif == null) {
      colonneTarif = new ColonneTarif(Integer.valueOf(1), EnumOrigine.TARIF, modeNegoce);
    }
    // Récupère le prix de base HT à partir de la colonne déterminé
    BigDecimal prixBaseHT = CalculPrixVente.retournerPrixVenteAvecColonne(colonneTarif.getNumero(), pListePrixVenteHT, pValeurPS305);
    setPrixBaseHT(prixBaseHT);
    setOriginePrixBase(EnumOrigine.TARIF);
  }
  
  /**
   * Calcule le prix net HT de cette condition.
   * Attention l'ordre de la séquence dans la recherche est primordial.
   * @param pListeRemiseEnPourcentageSaisie
   * @param pCoefficientMultiplicateurSaisi
   */
  public void calculerPrixNetHT(ColonneTarif pColonneTarif, List<BigDecimal> pListeRemiseEnPourcentageSaisie,
      BigDecimal pCoefficientMultiplicateurSaisi, List<BigDecimal> pListePrixVenteHT, Boolean pValeurPS305) {
    EnumTypeConditionVente typeConditionVente = parametreConditionVente.getType();
    
    // Si la condition est de type prix net
    if (typeConditionVente != null && typeConditionVente == EnumTypeConditionVente.PRIX_NET) {
      BigDecimal prixNetHT = parametreConditionVente.getPrixNetHT();
      if (prixNetHT == null) {
        throw new MessageErreurException(
            "Le prix net HT de la condition de vente " + parametreConditionVente.getCode() + " est invalide.");
      }
      setPrixNetHT(parametreConditionVente.getPrixNetHT());
      setOriginePrixNet(EnumOrigine.CONDITION_VENTE);
      origineRemiseEnPourcentage = null;
      origineCoefficientMultiplicateur = null;
      return;
    }
    
    // Si la condition est de type gratuit
    if (parametreConditionVente.isTypeGratuit()) {
      setPrixNetHT(BigDecimal.ZERO);
      setOriginePrixNet(EnumOrigine.CONDITION_VENTE);
      origineRemiseEnPourcentage = null;
      origineCoefficientMultiplicateur = null;
      return;
    }
    
    // Si la condition est de type remise en pourcentage
    if (typeConditionVente != null && typeConditionVente == EnumTypeConditionVente.REMISE_EN_POURCENTAGE) {
      // Les remises peuvent avoir été saisie
      List<BigDecimal> listeRemiseEnPourcentage = parametreConditionVente.getListeRemiseEnPourcentage();
      origineRemiseEnPourcentage = EnumOrigine.CONDITION_VENTE;
      if (pListeRemiseEnPourcentageSaisie != null && !pListeRemiseEnPourcentageSaisie.isEmpty()) {
        listeRemiseEnPourcentage = pListeRemiseEnPourcentageSaisie;
        origineRemiseEnPourcentage = EnumOrigine.SAISI;
      }
      // Le coefficient peut avoir été saisi
      BigDecimal coefficientMultiplicateur = parametreConditionVente.getCoefficientMultiplicateur();
      origineCoefficientMultiplicateur = EnumOrigine.CONDITION_VENTE;
      if (pCoefficientMultiplicateurSaisi != null && pCoefficientMultiplicateurSaisi.intValue() != 0) {
        coefficientMultiplicateur = pCoefficientMultiplicateurSaisi;
        origineCoefficientMultiplicateur = EnumOrigine.SAISI;
      }
      // Calcul du prix net HT avec remise en cascade ou en ajout
      BigDecimal prixNetHT = FormulePrixNet.calculerAvecListeRemiseEnPourcentage(getPrixBaseHT(), listeRemiseEnPourcentage,
          coefficientMultiplicateur, getNombreDecimale(), parametreConditionVente.isModeRemiseEnCascade());
      setPrixNetHT(prixNetHT);
      setOriginePrixNet(null);
      return;
    }
    
    // Si la condition est de type coefficient
    if (typeConditionVente != null && typeConditionVente == EnumTypeConditionVente.COEFFICIENT) {
      // Le coefficient peut avoir été saisi
      BigDecimal coefficientMultiplicateur = parametreConditionVente.getCoefficientMultiplicateur();
      origineCoefficientMultiplicateur = EnumOrigine.CONDITION_VENTE;
      if (pCoefficientMultiplicateurSaisi != null && pCoefficientMultiplicateurSaisi.intValue() != 0) {
        coefficientMultiplicateur = pCoefficientMultiplicateurSaisi;
        origineCoefficientMultiplicateur = EnumOrigine.SAISI;
      }
      // Calcul du prix net HT
      BigDecimal prixNetHT =
          FormulePrixNet.calculerAvecCoefficientMultiplicateur(getPrixBaseHT(), coefficientMultiplicateur, getNombreDecimale());
      setPrixNetHT(prixNetHT);
      setOriginePrixNet(null);
      return;
    }
    
    // Si la condition est de type formule
    if (typeConditionVente != null && typeConditionVente == EnumTypeConditionVente.FORMULE_PRIX) {
      FormulePrixFP formulePrixFP = parametreConditionVente.getFormulePrixFP();
      if (formulePrixFP == null) {
        throw new MessageErreurException("La formule à appliquer est invalide.");
      }
      // Calcul du prix net HT à l'aide de la formule
      BigDecimal prixNetHT = formulePrixFP.calculerPrixNetHT(getPrixBaseHT(), getNombreDecimale());
      setPrixNetHT(prixNetHT);
      setOriginePrixNet(null);
      origineRemiseEnPourcentage = null;
      origineCoefficientMultiplicateur = null;
      return;
    }
    
    // Si la condition est de type remise en valeur
    if (typeConditionVente != null && typeConditionVente == EnumTypeConditionVente.REMISE_EN_VALEUR) {
      BigDecimal remiseEnValeur = parametreConditionVente.getRemiseEnValeur();
      if (remiseEnValeur == null) {
        throw new MessageErreurException("La remise en valeur est invalide.");
      }
      BigDecimal prixNetHT = FormulePrixNet.calculerAvecRemiseEnValeur(getPrixBaseHT(), remiseEnValeur, getNombreDecimale());
      setPrixNetHT(prixNetHT);
      setOriginePrixNet(null);
      origineRemiseEnPourcentage = null;
      origineCoefficientMultiplicateur = null;
      return;
    }
    
    // Si la condition est de type ajout en valeur
    if (typeConditionVente != null && typeConditionVente == EnumTypeConditionVente.AJOUT_EN_VALEUR) {
      BigDecimal ajoutEnValeur = parametreConditionVente.getAjoutEnValeur();
      if (ajoutEnValeur == null) {
        throw new MessageErreurException("L'ajout en valeur est invalide.");
      }
      BigDecimal prixNetHT = FormulePrixNet.calculerAvecAjoutEnValeur(getPrixBaseHT(), ajoutEnValeur, getNombreDecimale());
      setPrixNetHT(prixNetHT);
      setOriginePrixNet(null);
      origineRemiseEnPourcentage = null;
      origineCoefficientMultiplicateur = null;
      return;
    }
    
    // Sinon il faut utiliser la colonne tarif
    // L'ordre hiérarchique est important
    // Si la colonne a été saisie ou vient du document de vente alors c'est celle-ci qui est utilisée
    if (pColonneTarif != null
        && (pColonneTarif.getOrigine() == EnumOrigine.SAISI || pColonneTarif.getOrigine() == EnumOrigine.DOCUMENT_VENTE)) {
      colonneTarif = pColonneTarif;
    }
    // Sinon c'est la colonne de la condition de vente
    else if (parametreConditionVente.getColonneTarif() != null) {
      colonneTarif = parametreConditionVente.getColonneTarif();
    }
    // Sinon c'est la colonne passée en paramètre
    else if (colonneTarif == null && pColonneTarif != null) {
      colonneTarif = pColonneTarif;
    }
    if (colonneTarif == null) {
      throw new MessageErreurException("La colonne tarif est invalide.");
    }
    // Récupère le prix de base HT à partir de la colonne déterminé
    BigDecimal prixBaseHT = CalculPrixVente.retournerPrixVenteAvecColonne(colonneTarif.getNumero(), pListePrixVenteHT, pValeurPS305);
    setPrixBaseHT(prixBaseHT);
    setOriginePrixBase(EnumOrigine.TARIF);
    
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le code de la condition de vente.
   * 
   * @return
   */
  public String getCode() {
    return parametreConditionVente.getCode();
  }
  
  /**
   * Initialiser les paramètres de la condition de vente.
   * @return
   */
  public ParametreConditionVente getParametreConditionVente() {
    return parametreConditionVente;
  }
  
  /**
   * Retourne le nombre de décimales à utiliser.
   * @return
   */
  public int getNombreDecimale() {
    return stockagePrixVente.getNombreDecimale();
  }
  
  /**
   * Retourne le prix de base HT.
   * @return
   */
  public BigDecimal getPrixBaseHT() {
    return stockagePrixVente.getPrixBaseHT();
  }
  
  /**
   * Initialise le prix de base HT.
   * @return
   */
  public void setPrixBaseHT(BigDecimal prixBaseHT) {
    stockagePrixVente.setPrixBaseHT(prixBaseHT);
  }
  
  /**
   * Retourne le prix net HT.
   * @return
   */
  public BigDecimal getPrixNetHT() {
    return stockagePrixVente.getPrixNetHT();
  }
  
  /**
   * Initialise le prix net HT.
   * @return
   */
  public void setPrixNetHT(BigDecimal prixNetHT) {
    stockagePrixVente.setPrixNetHT(prixNetHT);
  }
  
  /**
   * Retourne l'origine du prix de base.
   * @return
   */
  public EnumOrigine getOriginePrixBase() {
    return stockagePrixVente.getOriginePrixBase();
  }
  
  /**
   * Initialise l'origine du prix de base.
   * @param originePrixBase
   */
  public void setOriginePrixBase(EnumOrigine originePrixBase) {
    stockagePrixVente.setOriginePrixBase(originePrixBase);
  }
  
  /**
   * Retourne l'origine du prix net.
   * @return
   */
  public EnumOrigine getOriginePrixNet() {
    return stockagePrixVente.getOriginePrixNet();
  }
  
  /**
   * Initialise l'origine du prix net.
   * @param originePrixNet
   */
  public void setOriginePrixNet(EnumOrigine originePrixNet) {
    stockagePrixVente.setOriginePrixNet(originePrixNet);
  }
  
}
