/*
 * Created by JFormDesigner on Tue Dec 07 16:33:18 CET 2021
 */

package ri.serien.maquettes.snc9413.etude.composant.documentvente;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;
import ri.serien.maquettes.snc9413.etude.metier.parametre.ParametreDocumentVente;

/**
 * Composant permettant d'afficher tous les paramètres d'un document de vente (entête et ligne).
 */
public class SNPDocumentVente extends SNPanel {
  // Variables
  private ParametreDocumentVente parametreDocumentVente = null;
  
  /**
   * Constructeur.
   */
  public SNPDocumentVente() {
    super();
    initComponents();
  }
  
  // -- Méthodes publiques
  
  /**
   * Modifier les paramètres du document de ventes.
   */
  public void modifierParametreConditionVente(ParametreDocumentVente pParametreDocumentVente) {
    if (pParametreDocumentVente == null) {
      return;
    }
    
    parametreDocumentVente = pParametreDocumentVente;
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichissement des données du composant.
   */
  private void rafraichir() {
    // Paramètres de l'entête
    rafraichirIdentifiant();
    rafraichirCodeConditionVente();
    rafraichirColonneTarif();
    rafraichirRemiseEnPourcentage();
    rafraichirModeApplicationRemise();
    rafraichirBaseApplicationRemise();
    rafraichirDateLivraisonPrevue();
    rafraichirDateLivraisonSouhaitee();
    rafraichirDateExpeditionCommande();
    rafraichirCodeDevise();
    
    // Paramètres de la ligne
    rafraichirTopColonneTarifLigne();
    rafraichirColonneTarifLigne();
    rafraichirTopPrixBaseLigne();
    rafraichirPrixBaseHTLigne();
    rafraichirTopCoefficientLigne();
    rafraichirCoefficientLigne();
    rafraichirTopRemiseLigne();
    rafraichirRemiseEnPourcentageLigne();
    rafraichirModeApplicationRemiseLigne();
    rafraichirBaseApplicationRemiseLigne();
    rafraichirTopPrixNetLigne();
    rafraichirPrixNetHTLigne();
    rafraichirTopPrixCalculeLigne();
    rafraichirPrixCalculeHTLigne();
    rafraichirPrixGarantiLigne();
    rafraichirComplementLibelleLigne();
  }
  
  // -- Paramètres de l'entête
  
  /**
   * Rafraichit l'identifiant du document de vente.
   */
  private void rafraichirIdentifiant() {
    if (parametreDocumentVente == null || parametreDocumentVente.getIdentifiant() == null) {
      tfPIdentifiant.setText("");
      return;
    }
    String identifiant = parametreDocumentVente.getIdentifiant();
    tfPIdentifiant.setText(identifiant);
  }
  
  /**
   * Rafraichit le code de la condition de vente.
   */
  private void rafraichirCodeConditionVente() {
    if (parametreDocumentVente == null || parametreDocumentVente.getCodeConditionVente() == null) {
      tfPCodeCNV.setText("");
      return;
    }
    String code = parametreDocumentVente.getCodeConditionVente();
    tfPCodeCNV.setText(code);
  }
  
  /**
   * Rafraichit la colonne tarif.
   */
  private void rafraichirColonneTarif() {
    if (parametreDocumentVente == null || parametreDocumentVente.getColonneTarif() == null) {
      tfPColonneTarif.setText("");
      return;
    }
    ColonneTarif colonneTarif = parametreDocumentVente.getColonneTarif();
    tfPColonneTarif.setText(colonneTarif.getNumero().toString());
  }
  
  /**
   * Rafraichit les remises en pourcentage.
   */
  private void rafraichirRemiseEnPourcentage() {
    if (parametreDocumentVente == null || parametreDocumentVente.getListeRemiseEnPourcentage() == null
        || parametreDocumentVente.getListeRemiseEnPourcentage().isEmpty()) {
      tfPPourcentageRemise1.setText("");
      tfPPourcentageRemise2.setText("");
      tfPPourcentageRemise3.setText("");
      tfPPourcentageRemise4.setText("");
      tfPPourcentageRemise5.setText("");
      tfPPourcentageRemise6.setText("");
      return;
    }
    
    List<BigPercentage> listeValeur = parametreDocumentVente.getListeRemiseEnPourcentage();
    tfPPourcentageRemise1.setText(listeValeur.get(0).toString());
    if (listeValeur.size() > 1) {
      tfPPourcentageRemise2.setText(listeValeur.get(1).toString());
    }
    if (listeValeur.size() > 2) {
      tfPPourcentageRemise3.setText(listeValeur.get(2).toString());
    }
    if (listeValeur.size() > 3) {
      tfPPourcentageRemise4.setText(listeValeur.get(3).toString());
    }
    if (listeValeur.size() > 4) {
      tfPPourcentageRemise5.setText(listeValeur.get(4).toString());
    }
    if (listeValeur.size() > 5) {
      tfPPourcentageRemise6.setText(listeValeur.get(5).toString());
    }
  }
  
  /**
   * Rafraichit le mode d'application des remises.
   */
  private void rafraichirModeApplicationRemise() {
    if (parametreDocumentVente == null) {
      tfPModeApplicationRemise.setText("");
      return;
    }
    if (parametreDocumentVente.isModeRemiseEnCascade()) {
      tfPModeApplicationRemise.setText("En cascade");
    }
    else {
      tfPModeApplicationRemise.setText("En ajout");
    }
  }
  
  /**
   * Rafraichit la base d'application des remises.
   */
  private void rafraichirBaseApplicationRemise() {
    if (parametreDocumentVente == null) {
      tfPBaseApplicationRemise.setText("");
      return;
    }
    if (parametreDocumentVente.isBaseRemisePrixUnitaire()) {
      tfPBaseApplicationRemise.setText("Sur le prix unitaire");
    }
    else {
      tfPBaseApplicationRemise.setText("Sur le montant de la ligne");
    }
  }
  
  /**
   * Rafraichit la date de livraison prévue.
   */
  private void rafraichirDateLivraisonPrevue() {
    if (parametreDocumentVente == null) {
      tfPDateLivraisonPrevue.setText("");
      return;
    }
    Date date = parametreDocumentVente.getDateLivraisonPrevue();
    tfPDateLivraisonPrevue.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
  }
  
  /**
   * Rafraichit la date de livraison souhaitée.
   */
  private void rafraichirDateLivraisonSouhaitee() {
    if (parametreDocumentVente == null) {
      tfPDateLivraisonSouhaitee.setText("");
      return;
    }
    Date date = parametreDocumentVente.getDateLivraisonSouhaitee();
    tfPDateLivraisonSouhaitee.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
  }
  
  /**
   * Rafraichit la date d'expédition de la commande.
   */
  private void rafraichirDateExpeditionCommande() {
    if (parametreDocumentVente == null) {
      tfPDateExpeditionCommande.setText("");
      return;
    }
    Date date = parametreDocumentVente.getDateExpeditionCommande();
    tfPDateExpeditionCommande.setText(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, date));
  }
  
  /**
   * Rafraichit le code devise.
   */
  private void rafraichirCodeDevise() {
    if (parametreDocumentVente == null || parametreDocumentVente.getCodeDevise() == null) {
      tfPCodeDevise.setText("");
      return;
    }
    String code = parametreDocumentVente.getCodeDevise();
    tfPCodeDevise.setText(code);
  }
  
  // -- Paramètres de la ligne
  
  /**
   * Rafraichit le top de la colonne tarif de la ligne.
   */
  private void rafraichirTopColonneTarifLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getTopColonneTarifLigne() == null) {
      tfPTopColonneTarifLigne.setText("");
      return;
    }
    Integer top = parametreDocumentVente.getTopColonneTarifLigne();
    tfPTopColonneTarifLigne.setText(top.toString());
  }
  
  /**
   * Rafraichit la colonne tarif de la ligne.
   */
  private void rafraichirColonneTarifLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getColonneTarifLigne() == null) {
      tfPColonneTarifLigne.setText("");
      return;
    }
    ColonneTarif colonneTarif = parametreDocumentVente.getColonneTarifLigne();
    tfPColonneTarifLigne.setText(colonneTarif.getNumero().toString());
  }
  
  /**
   * Rafraichit le top du prix de base de la ligne.
   */
  private void rafraichirTopPrixBaseLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getTopPrixBaseSaisiLigne() == null) {
      tfPTopPrixBaseLigne.setText("");
      return;
    }
    Integer top = parametreDocumentVente.getTopPrixBaseSaisiLigne();
    tfPTopPrixBaseLigne.setText(top.toString());
  }
  
  /**
   * Rafraichit le prix de base HT de la ligne.
   */
  private void rafraichirPrixBaseHTLigne() {
    if (parametreDocumentVente == null) {
      tfPPrixBaseHTLigne.setMontant("");
      return;
    }
    BigDecimal valeur = parametreDocumentVente.getPrixBaseHTSaisiLigne();
    tfPPrixBaseHTLigne.setMontant(valeur);
  }
  
  /**
   * Rafraichit le top du coefficient de la ligne.
   */
  private void rafraichirTopCoefficientLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getTopCoefficientSaisiLigne() == null) {
      tfPTopCoefficientLigne.setText("");
      return;
    }
    Integer top = parametreDocumentVente.getTopCoefficientSaisiLigne();
    tfPTopCoefficientLigne.setText(top.toString());
  }
  
  /**
   * Rafraichit le coeffcient multiplicateur de la ligne.
   */
  private void rafraichirCoefficientLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getCoefficientSaisiLigne() == null) {
      tfPCoefficientMultiplicateurLigne.setText("");
      return;
    }
    BigDecimal valeur = parametreDocumentVente.getCoefficientSaisiLigne();
    tfPCoefficientMultiplicateurLigne.setText(valeur.toString());
  }
  
  /**
   * Rafraichit le top des remises de la ligne.
   */
  private void rafraichirTopRemiseLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getTopRemiseSaisieLigne() == null) {
      tfPTopRemiseLigne.setText("");
      return;
    }
    Integer top = parametreDocumentVente.getTopRemiseSaisieLigne();
    tfPTopRemiseLigne.setText(top.toString());
  }
  
  /**
   * Rafraichit les remises en pourcentage de la ligne.
   */
  private void rafraichirRemiseEnPourcentageLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getListeRemiseEnPourcentageSaisieLigne() == null
        || parametreDocumentVente.getListeRemiseEnPourcentageSaisieLigne().isEmpty()) {
      tfPPourcentageRemise1.setText("");
      tfPPourcentageRemise2.setText("");
      tfPPourcentageRemise3.setText("");
      tfPPourcentageRemise4.setText("");
      tfPPourcentageRemise5.setText("");
      tfPPourcentageRemise6.setText("");
      return;
    }
    
    List<BigPercentage> listeValeur = parametreDocumentVente.getListeRemiseEnPourcentageSaisieLigne();
    tfPPourcentageRemise1Ligne.setText(listeValeur.get(0).toString());
    if (listeValeur.size() > 1) {
      tfPPourcentageRemise2Ligne.setText(listeValeur.get(1).toString());
    }
    if (listeValeur.size() > 2) {
      tfPPourcentageRemise3Ligne.setText(listeValeur.get(2).toString());
    }
    if (listeValeur.size() > 3) {
      tfPPourcentageRemise4Ligne.setText(listeValeur.get(3).toString());
    }
    if (listeValeur.size() > 4) {
      tfPPourcentageRemise5Ligne.setText(listeValeur.get(4).toString());
    }
    if (listeValeur.size() > 5) {
      tfPPourcentageRemise6Ligne.setText(listeValeur.get(5).toString());
    }
  }
  
  /**
   * Rafraichit le mode d'application des remises de la ligne.
   */
  private void rafraichirModeApplicationRemiseLigne() {
    if (parametreDocumentVente == null) {
      tfPModeApplicationRemiseLigne.setText("");
      return;
    }
    if (parametreDocumentVente.isModeRemiseEnCascadeLigne()) {
      tfPModeApplicationRemiseLigne.setText("En cascade");
    }
    else {
      tfPModeApplicationRemiseLigne.setText("En ajout");
    }
  }
  
  /**
   * Rafraichit la base d'application des remises de la ligne.
   */
  private void rafraichirBaseApplicationRemiseLigne() {
    if (parametreDocumentVente == null) {
      tfPBaseApplicationRemiseLigne.setText("");
      return;
    }
    if (parametreDocumentVente.isBaseRemisePrixUnitaireLigne()) {
      tfPBaseApplicationRemiseLigne.setText("Sur le prix unitaire");
    }
    else {
      tfPBaseApplicationRemiseLigne.setText("Sur le montant de la ligne");
    }
  }
  
  /**
   * Rafraichit le top du prix net de la ligne.
   */
  private void rafraichirTopPrixNetLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getTopPrixNetSaisiLigne() == null) {
      tfPTopPrixNetLigne.setText("");
      return;
    }
    Integer top = parametreDocumentVente.getTopPrixNetSaisiLigne();
    tfPTopPrixNetLigne.setText(top.toString());
  }
  
  /**
   * Rafraichit le prix net HT de la ligne.
   */
  private void rafraichirPrixNetHTLigne() {
    if (parametreDocumentVente == null) {
      tfPPrixNetHTLigne.setMontant("");
      return;
    }
    BigDecimal valeur = parametreDocumentVente.getPrixNetHTSaisiLigne();
    tfPPrixNetHTLigne.setMontant(valeur);
  }
  
  /**
   * Rafraichit le top du prix calculé de la ligne.
   */
  private void rafraichirTopPrixCalculeLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getTopPrixCalculeSaisiLigne() == null) {
      tfPTopPrixCalculeLigne.setText("");
      return;
    }
    Integer top = parametreDocumentVente.getTopPrixCalculeSaisiLigne();
    tfPTopPrixCalculeLigne.setText(top.toString());
  }
  
  /**
   * Rafraichit le prix calculé HT de la ligne.
   */
  private void rafraichirPrixCalculeHTLigne() {
    if (parametreDocumentVente == null) {
      tfPPrixCalculeHTLigne.setMontant("");
      return;
    }
    BigDecimal valeur = parametreDocumentVente.getPrixCalculeHTSaisiLigne();
    tfPPrixCalculeHTLigne.setMontant(valeur);
  }
  
  /**
   * Rafraichit si le prix est garanti à la commande de la ligne.
   */
  private void rafraichirPrixGarantiLigne() {
    if (parametreDocumentVente == null) {
      tfPPrixGarantiLigne.setText("");
      return;
    }
    if (parametreDocumentVente.isPrixGarantiCommande()) {
      tfPPrixGarantiLigne.setText("Oui");
    }
    else {
      tfPPrixGarantiLigne.setText("Non");
    }
  }
  
  /**
   * Rafraichit le complément du libellé de la ligne.
   */
  private void rafraichirComplementLibelleLigne() {
    if (parametreDocumentVente == null || parametreDocumentVente.getComplementLibelle() == null) {
      tfPComplementLibelleLigne.setText("");
      return;
    }
    String libelle = parametreDocumentVente.getComplementLibelle();
    tfPComplementLibelleLigne.setText(libelle);
  }
  
  // -- Méthodes événementielles
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbPIdentifiant = new SNLabelChamp();
    tfPIdentifiant = new SNTexte();
    lbPCodeCNV = new SNLabelChamp();
    tfPCodeCNV = new SNTexte();
    lbPColonneTarif = new SNLabelChamp();
    tfPColonneTarif = new SNTexte();
    lbPCoefficientMultiplicateur2 = new SNLabelChamp();
    pnlPPourcentageRemise = new SNPanel();
    tfPPourcentageRemise1 = new SNTexte();
    tfPPourcentageRemise2 = new SNTexte();
    tfPPourcentageRemise3 = new SNTexte();
    tfPPourcentageRemise4 = new SNTexte();
    tfPPourcentageRemise5 = new SNTexte();
    tfPPourcentageRemise6 = new SNTexte();
    lbPModeApplicationRemise = new SNLabelChamp();
    tfPModeApplicationRemise = new SNTexte();
    lbPBaseApplicationRemise = new SNLabelChamp();
    tfPBaseApplicationRemise = new SNTexte();
    lbPDateLivraisonPrevue = new SNLabelChamp();
    tfPDateLivraisonPrevue = new SNTexte();
    lbPDateLivraisonSouhaitee = new SNLabelChamp();
    tfPDateLivraisonSouhaitee = new SNTexte();
    lbPDateExpeditionCommande = new SNLabelChamp();
    tfPDateExpeditionCommande = new SNTexte();
    lbPCodeDevise = new SNLabelChamp();
    tfPCodeDevise = new SNTexte();
    separator1 = new JSeparator();
    lbPTopColonneTarifLigne = new SNLabelChamp();
    tfPTopColonneTarifLigne = new SNTexte();
    lbPColonneTarifLigne = new SNLabelChamp();
    tfPColonneTarifLigne = new SNTexte();
    lbPTopPrixBaseLigne = new SNLabelChamp();
    tfPTopPrixBaseLigne = new SNTexte();
    lbPPrixBaseHTLigne = new SNLabelChamp();
    tfPPrixBaseHTLigne = new SNMontant();
    lbPTopCoefficientLigne = new SNLabelChamp();
    tfPTopCoefficientLigne = new SNTexte();
    lbPCoefficientMultiplicateurLigne = new SNLabelChamp();
    tfPCoefficientMultiplicateurLigne = new SNTexte();
    lbPTopRemiseLigne = new SNLabelChamp();
    tfPTopRemiseLigne = new SNTexte();
    lbPRemiseLigne = new SNLabelChamp();
    pnlPPourcentageRemiseLigne = new SNPanel();
    tfPPourcentageRemise1Ligne = new SNTexte();
    tfPPourcentageRemise2Ligne = new SNTexte();
    tfPPourcentageRemise3Ligne = new SNTexte();
    tfPPourcentageRemise4Ligne = new SNTexte();
    tfPPourcentageRemise5Ligne = new SNTexte();
    tfPPourcentageRemise6Ligne = new SNTexte();
    lbPModeApplicationremiseLigne = new SNLabelChamp();
    tfPModeApplicationRemiseLigne = new SNTexte();
    lbPBaseApplicationRemiseLigne = new SNLabelChamp();
    tfPBaseApplicationRemiseLigne = new SNTexte();
    lbPTopPrixNetLigne = new SNLabelChamp();
    tfPTopPrixNetLigne = new SNTexte();
    lbPPrixNetHTLigne = new SNLabelChamp();
    tfPPrixNetHTLigne = new SNMontant();
    lbPTopPrixCalculeLigne = new SNLabelChamp();
    tfPTopPrixCalculeLigne = new SNTexte();
    lbPPrixCalculeHTLigne = new SNLabelChamp();
    tfPPrixCalculeHTLigne = new SNMontant();
    lbPPrixGarantiLigne = new SNLabelChamp();
    tfPPrixGarantiLigne = new SNTexte();
    lbPComplementLibelleLigne = new SNLabelChamp();
    tfPComplementLibelleLigne = new SNTexte();
    
    // ======== this ========
    setMinimumSize(new Dimension(420, 580));
    setPreferredSize(new Dimension(420, 590));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) getLayout()).rowHeights =
        new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
        0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    
    // ---- lbPIdentifiant ----
    lbPIdentifiant.setModeReduit(true);
    lbPIdentifiant.setText("Identifiant");
    lbPIdentifiant.setMinimumSize(new Dimension(140, 22));
    lbPIdentifiant.setPreferredSize(new Dimension(140, 22));
    lbPIdentifiant.setMaximumSize(new Dimension(140, 22));
    lbPIdentifiant.setName("lbPIdentifiant");
    add(lbPIdentifiant,
        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPIdentifiant ----
    tfPIdentifiant.setModeReduit(true);
    tfPIdentifiant.setEnabled(false);
    tfPIdentifiant.setName("tfPIdentifiant");
    add(tfPIdentifiant,
        new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCodeCNV ----
    lbPCodeCNV.setModeReduit(true);
    lbPCodeCNV.setText("Code CNV");
    lbPCodeCNV.setMinimumSize(new Dimension(130, 22));
    lbPCodeCNV.setPreferredSize(new Dimension(130, 22));
    lbPCodeCNV.setMaximumSize(new Dimension(130, 22));
    lbPCodeCNV.setName("lbPCodeCNV");
    add(lbPCodeCNV,
        new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPCodeCNV ----
    tfPCodeCNV.setModeReduit(true);
    tfPCodeCNV.setEnabled(false);
    tfPCodeCNV.setName("tfPCodeCNV");
    add(tfPCodeCNV,
        new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPColonneTarif ----
    lbPColonneTarif.setModeReduit(true);
    lbPColonneTarif.setText("Colonne tarif");
    lbPColonneTarif.setMinimumSize(new Dimension(30, 22));
    lbPColonneTarif.setPreferredSize(new Dimension(30, 22));
    lbPColonneTarif.setName("lbPColonneTarif");
    add(lbPColonneTarif,
        new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPColonneTarif ----
    tfPColonneTarif.setModeReduit(true);
    tfPColonneTarif.setEnabled(false);
    tfPColonneTarif.setName("tfPColonneTarif");
    add(tfPColonneTarif,
        new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCoefficientMultiplicateur2 ----
    lbPCoefficientMultiplicateur2.setModeReduit(true);
    lbPCoefficientMultiplicateur2.setText("Remises en %");
    lbPCoefficientMultiplicateur2.setMinimumSize(new Dimension(30, 22));
    lbPCoefficientMultiplicateur2.setPreferredSize(new Dimension(30, 22));
    lbPCoefficientMultiplicateur2.setName("lbPCoefficientMultiplicateur2");
    add(lbPCoefficientMultiplicateur2,
        new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ======== pnlPPourcentageRemise ========
    {
      pnlPPourcentageRemise.setMinimumSize(new Dimension(270, 22));
      pnlPPourcentageRemise.setPreferredSize(new Dimension(270, 22));
      pnlPPourcentageRemise.setName("pnlPPourcentageRemise");
      pnlPPourcentageRemise.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPPourcentageRemise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlPPourcentageRemise.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlPPourcentageRemise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlPPourcentageRemise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      
      // ---- tfPPourcentageRemise1 ----
      tfPPourcentageRemise1.setEnabled(false);
      tfPPourcentageRemise1.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise1.setModeReduit(true);
      tfPPourcentageRemise1.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise1.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise1.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise1.setName("tfPPourcentageRemise1");
      pnlPPourcentageRemise.add(tfPPourcentageRemise1,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise2 ----
      tfPPourcentageRemise2.setEnabled(false);
      tfPPourcentageRemise2.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise2.setModeReduit(true);
      tfPPourcentageRemise2.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise2.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise2.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise2.setName("tfPPourcentageRemise2");
      pnlPPourcentageRemise.add(tfPPourcentageRemise2,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise3 ----
      tfPPourcentageRemise3.setEnabled(false);
      tfPPourcentageRemise3.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise3.setModeReduit(true);
      tfPPourcentageRemise3.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise3.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise3.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise3.setName("tfPPourcentageRemise3");
      pnlPPourcentageRemise.add(tfPPourcentageRemise3,
          new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise4 ----
      tfPPourcentageRemise4.setEnabled(false);
      tfPPourcentageRemise4.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise4.setModeReduit(true);
      tfPPourcentageRemise4.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise4.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise4.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise4.setName("tfPPourcentageRemise4");
      pnlPPourcentageRemise.add(tfPPourcentageRemise4,
          new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise5 ----
      tfPPourcentageRemise5.setEnabled(false);
      tfPPourcentageRemise5.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise5.setModeReduit(true);
      tfPPourcentageRemise5.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise5.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise5.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise5.setName("tfPPourcentageRemise5");
      pnlPPourcentageRemise.add(tfPPourcentageRemise5,
          new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise6 ----
      tfPPourcentageRemise6.setEnabled(false);
      tfPPourcentageRemise6.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise6.setModeReduit(true);
      tfPPourcentageRemise6.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise6.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise6.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise6.setName("tfPPourcentageRemise6");
      pnlPPourcentageRemise.add(tfPPourcentageRemise6,
          new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPPourcentageRemise,
        new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPModeApplicationRemise ----
    lbPModeApplicationRemise.setModeReduit(true);
    lbPModeApplicationRemise.setText("Mode application");
    lbPModeApplicationRemise.setMinimumSize(new Dimension(30, 22));
    lbPModeApplicationRemise.setPreferredSize(new Dimension(30, 22));
    lbPModeApplicationRemise.setName("lbPModeApplicationRemise");
    add(lbPModeApplicationRemise,
        new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPModeApplicationRemise ----
    tfPModeApplicationRemise.setModeReduit(true);
    tfPModeApplicationRemise.setEnabled(false);
    tfPModeApplicationRemise.setName("tfPModeApplicationRemise");
    add(tfPModeApplicationRemise,
        new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPBaseApplicationRemise ----
    lbPBaseApplicationRemise.setModeReduit(true);
    lbPBaseApplicationRemise.setText("Base remise");
    lbPBaseApplicationRemise.setMinimumSize(new Dimension(30, 22));
    lbPBaseApplicationRemise.setPreferredSize(new Dimension(30, 22));
    lbPBaseApplicationRemise.setName("lbPBaseApplicationRemise");
    add(lbPBaseApplicationRemise,
        new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPBaseApplicationRemise ----
    tfPBaseApplicationRemise.setModeReduit(true);
    tfPBaseApplicationRemise.setEnabled(false);
    tfPBaseApplicationRemise.setName("tfPBaseApplicationRemise");
    add(tfPBaseApplicationRemise,
        new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPDateLivraisonPrevue ----
    lbPDateLivraisonPrevue.setModeReduit(true);
    lbPDateLivraisonPrevue.setText("Date livraison pr\u00e9vue");
    lbPDateLivraisonPrevue.setMinimumSize(new Dimension(30, 22));
    lbPDateLivraisonPrevue.setPreferredSize(new Dimension(30, 22));
    lbPDateLivraisonPrevue.setName("lbPDateLivraisonPrevue");
    add(lbPDateLivraisonPrevue,
        new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPDateLivraisonPrevue ----
    tfPDateLivraisonPrevue.setModeReduit(true);
    tfPDateLivraisonPrevue.setEnabled(false);
    tfPDateLivraisonPrevue.setName("tfPDateLivraisonPrevue");
    add(tfPDateLivraisonPrevue,
        new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPDateLivraisonSouhaitee ----
    lbPDateLivraisonSouhaitee.setModeReduit(true);
    lbPDateLivraisonSouhaitee.setText("Date livraison souhait\u00e9e");
    lbPDateLivraisonSouhaitee.setMinimumSize(new Dimension(30, 22));
    lbPDateLivraisonSouhaitee.setPreferredSize(new Dimension(30, 22));
    lbPDateLivraisonSouhaitee.setName("lbPDateLivraisonSouhaitee");
    add(lbPDateLivraisonSouhaitee,
        new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPDateLivraisonSouhaitee ----
    tfPDateLivraisonSouhaitee.setModeReduit(true);
    tfPDateLivraisonSouhaitee.setEnabled(false);
    tfPDateLivraisonSouhaitee.setName("tfPDateLivraisonSouhaitee");
    add(tfPDateLivraisonSouhaitee,
        new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPDateExpeditionCommande ----
    lbPDateExpeditionCommande.setModeReduit(true);
    lbPDateExpeditionCommande.setText("Date exp\u00e9dition commande");
    lbPDateExpeditionCommande.setMinimumSize(new Dimension(30, 22));
    lbPDateExpeditionCommande.setPreferredSize(new Dimension(30, 22));
    lbPDateExpeditionCommande.setName("lbPDateExpeditionCommande");
    add(lbPDateExpeditionCommande,
        new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPDateExpeditionCommande ----
    tfPDateExpeditionCommande.setModeReduit(true);
    tfPDateExpeditionCommande.setEnabled(false);
    tfPDateExpeditionCommande.setName("tfPDateExpeditionCommande");
    add(tfPDateExpeditionCommande,
        new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCodeDevise ----
    lbPCodeDevise.setModeReduit(true);
    lbPCodeDevise.setText("Code devise");
    lbPCodeDevise.setMinimumSize(new Dimension(30, 22));
    lbPCodeDevise.setPreferredSize(new Dimension(30, 22));
    lbPCodeDevise.setName("lbPCodeDevise");
    add(lbPCodeDevise,
        new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPCodeDevise ----
    tfPCodeDevise.setModeReduit(true);
    tfPCodeDevise.setEnabled(false);
    tfPCodeDevise.setName("tfPCodeDevise");
    add(tfPCodeDevise,
        new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- separator1 ----
    separator1.setName("separator1");
    add(separator1, new GridBagConstraints(0, 10, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPTopColonneTarifLigne ----
    lbPTopColonneTarifLigne.setModeReduit(true);
    lbPTopColonneTarifLigne.setText("Top colonne tarif");
    lbPTopColonneTarifLigne.setMinimumSize(new Dimension(30, 22));
    lbPTopColonneTarifLigne.setPreferredSize(new Dimension(30, 22));
    lbPTopColonneTarifLigne.setName("lbPTopColonneTarifLigne");
    add(lbPTopColonneTarifLigne,
        new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPTopColonneTarifLigne ----
    tfPTopColonneTarifLigne.setModeReduit(true);
    tfPTopColonneTarifLigne.setEnabled(false);
    tfPTopColonneTarifLigne.setName("tfPTopColonneTarifLigne");
    add(tfPTopColonneTarifLigne, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPColonneTarifLigne ----
    lbPColonneTarifLigne.setModeReduit(true);
    lbPColonneTarifLigne.setText("Colonne tarif");
    lbPColonneTarifLigne.setMinimumSize(new Dimension(30, 22));
    lbPColonneTarifLigne.setPreferredSize(new Dimension(30, 22));
    lbPColonneTarifLigne.setName("lbPColonneTarifLigne");
    add(lbPColonneTarifLigne,
        new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPColonneTarifLigne ----
    tfPColonneTarifLigne.setModeReduit(true);
    tfPColonneTarifLigne.setEnabled(false);
    tfPColonneTarifLigne.setName("tfPColonneTarifLigne");
    add(tfPColonneTarifLigne, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPTopPrixBaseLigne ----
    lbPTopPrixBaseLigne.setModeReduit(true);
    lbPTopPrixBaseLigne.setText("Top prix de base");
    lbPTopPrixBaseLigne.setMinimumSize(new Dimension(30, 22));
    lbPTopPrixBaseLigne.setPreferredSize(new Dimension(30, 22));
    lbPTopPrixBaseLigne.setName("lbPTopPrixBaseLigne");
    add(lbPTopPrixBaseLigne,
        new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPTopPrixBaseLigne ----
    tfPTopPrixBaseLigne.setModeReduit(true);
    tfPTopPrixBaseLigne.setEnabled(false);
    tfPTopPrixBaseLigne.setName("tfPTopPrixBaseLigne");
    add(tfPTopPrixBaseLigne, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPPrixBaseHTLigne ----
    lbPPrixBaseHTLigne.setModeReduit(true);
    lbPPrixBaseHTLigne.setText("Prix de base HT");
    lbPPrixBaseHTLigne.setMinimumSize(new Dimension(30, 22));
    lbPPrixBaseHTLigne.setPreferredSize(new Dimension(30, 22));
    lbPPrixBaseHTLigne.setName("lbPPrixBaseHTLigne");
    add(lbPPrixBaseHTLigne,
        new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPPrixBaseHTLigne ----
    tfPPrixBaseHTLigne.setModeReduit(true);
    tfPPrixBaseHTLigne.setEnabled(false);
    tfPPrixBaseHTLigne.setName("tfPPrixBaseHTLigne");
    add(tfPPrixBaseHTLigne, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPTopCoefficientLigne ----
    lbPTopCoefficientLigne.setModeReduit(true);
    lbPTopCoefficientLigne.setText("Top coefficient");
    lbPTopCoefficientLigne.setMinimumSize(new Dimension(30, 22));
    lbPTopCoefficientLigne.setPreferredSize(new Dimension(30, 22));
    lbPTopCoefficientLigne.setName("lbPTopCoefficientLigne");
    add(lbPTopCoefficientLigne,
        new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPTopCoefficientLigne ----
    tfPTopCoefficientLigne.setModeReduit(true);
    tfPTopCoefficientLigne.setEnabled(false);
    tfPTopCoefficientLigne.setName("tfPTopCoefficientLigne");
    add(tfPTopCoefficientLigne, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPCoefficientMultiplicateurLigne ----
    lbPCoefficientMultiplicateurLigne.setModeReduit(true);
    lbPCoefficientMultiplicateurLigne.setText("Coefficient Multiplicateur");
    lbPCoefficientMultiplicateurLigne.setMinimumSize(new Dimension(30, 22));
    lbPCoefficientMultiplicateurLigne.setPreferredSize(new Dimension(30, 22));
    lbPCoefficientMultiplicateurLigne.setName("lbPCoefficientMultiplicateurLigne");
    add(lbPCoefficientMultiplicateurLigne,
        new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPCoefficientMultiplicateurLigne ----
    tfPCoefficientMultiplicateurLigne.setModeReduit(true);
    tfPCoefficientMultiplicateurLigne.setEnabled(false);
    tfPCoefficientMultiplicateurLigne.setName("tfPCoefficientMultiplicateurLigne");
    add(tfPCoefficientMultiplicateurLigne, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
        GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPTopRemiseLigne ----
    lbPTopRemiseLigne.setModeReduit(true);
    lbPTopRemiseLigne.setText("Top remises");
    lbPTopRemiseLigne.setMinimumSize(new Dimension(30, 22));
    lbPTopRemiseLigne.setPreferredSize(new Dimension(30, 22));
    lbPTopRemiseLigne.setName("lbPTopRemiseLigne");
    add(lbPTopRemiseLigne,
        new GridBagConstraints(0, 17, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPTopRemiseLigne ----
    tfPTopRemiseLigne.setModeReduit(true);
    tfPTopRemiseLigne.setEnabled(false);
    tfPTopRemiseLigne.setName("tfPTopRemiseLigne");
    add(tfPTopRemiseLigne, new GridBagConstraints(1, 17, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPRemiseLigne ----
    lbPRemiseLigne.setModeReduit(true);
    lbPRemiseLigne.setText("Remises en %");
    lbPRemiseLigne.setMinimumSize(new Dimension(30, 22));
    lbPRemiseLigne.setPreferredSize(new Dimension(30, 22));
    lbPRemiseLigne.setName("lbPRemiseLigne");
    add(lbPRemiseLigne,
        new GridBagConstraints(0, 18, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ======== pnlPPourcentageRemiseLigne ========
    {
      pnlPPourcentageRemiseLigne.setMinimumSize(new Dimension(270, 22));
      pnlPPourcentageRemiseLigne.setPreferredSize(new Dimension(270, 22));
      pnlPPourcentageRemiseLigne.setName("pnlPPourcentageRemiseLigne");
      pnlPPourcentageRemiseLigne.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPPourcentageRemiseLigne.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlPPourcentageRemiseLigne.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlPPourcentageRemiseLigne.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlPPourcentageRemiseLigne.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      
      // ---- tfPPourcentageRemise1Ligne ----
      tfPPourcentageRemise1Ligne.setEnabled(false);
      tfPPourcentageRemise1Ligne.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise1Ligne.setModeReduit(true);
      tfPPourcentageRemise1Ligne.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise1Ligne.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise1Ligne.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise1Ligne.setName("tfPPourcentageRemise1Ligne");
      pnlPPourcentageRemiseLigne.add(tfPPourcentageRemise1Ligne,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise2Ligne ----
      tfPPourcentageRemise2Ligne.setEnabled(false);
      tfPPourcentageRemise2Ligne.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise2Ligne.setModeReduit(true);
      tfPPourcentageRemise2Ligne.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise2Ligne.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise2Ligne.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise2Ligne.setName("tfPPourcentageRemise2Ligne");
      pnlPPourcentageRemiseLigne.add(tfPPourcentageRemise2Ligne,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise3Ligne ----
      tfPPourcentageRemise3Ligne.setEnabled(false);
      tfPPourcentageRemise3Ligne.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise3Ligne.setModeReduit(true);
      tfPPourcentageRemise3Ligne.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise3Ligne.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise3Ligne.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise3Ligne.setName("tfPPourcentageRemise3Ligne");
      pnlPPourcentageRemiseLigne.add(tfPPourcentageRemise3Ligne,
          new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise4Ligne ----
      tfPPourcentageRemise4Ligne.setEnabled(false);
      tfPPourcentageRemise4Ligne.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise4Ligne.setModeReduit(true);
      tfPPourcentageRemise4Ligne.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise4Ligne.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise4Ligne.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise4Ligne.setName("tfPPourcentageRemise4Ligne");
      pnlPPourcentageRemiseLigne.add(tfPPourcentageRemise4Ligne,
          new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise5Ligne ----
      tfPPourcentageRemise5Ligne.setEnabled(false);
      tfPPourcentageRemise5Ligne.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise5Ligne.setModeReduit(true);
      tfPPourcentageRemise5Ligne.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise5Ligne.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise5Ligne.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise5Ligne.setName("tfPPourcentageRemise5Ligne");
      pnlPPourcentageRemiseLigne.add(tfPPourcentageRemise5Ligne,
          new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- tfPPourcentageRemise6Ligne ----
      tfPPourcentageRemise6Ligne.setEnabled(false);
      tfPPourcentageRemise6Ligne.setHorizontalAlignment(SwingConstants.RIGHT);
      tfPPourcentageRemise6Ligne.setModeReduit(true);
      tfPPourcentageRemise6Ligne.setMaximumSize(new Dimension(40, 22));
      tfPPourcentageRemise6Ligne.setMinimumSize(new Dimension(40, 22));
      tfPPourcentageRemise6Ligne.setPreferredSize(new Dimension(40, 22));
      tfPPourcentageRemise6Ligne.setName("tfPPourcentageRemise6Ligne");
      pnlPPourcentageRemiseLigne.add(tfPPourcentageRemise6Ligne,
          new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPPourcentageRemiseLigne,
        new GridBagConstraints(1, 18, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPModeApplicationremiseLigne ----
    lbPModeApplicationremiseLigne.setModeReduit(true);
    lbPModeApplicationremiseLigne.setText("Mode application");
    lbPModeApplicationremiseLigne.setMinimumSize(new Dimension(30, 22));
    lbPModeApplicationremiseLigne.setPreferredSize(new Dimension(30, 22));
    lbPModeApplicationremiseLigne.setName("lbPModeApplicationremiseLigne");
    add(lbPModeApplicationremiseLigne,
        new GridBagConstraints(0, 19, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPModeApplicationRemiseLigne ----
    tfPModeApplicationRemiseLigne.setModeReduit(true);
    tfPModeApplicationRemiseLigne.setEnabled(false);
    tfPModeApplicationRemiseLigne.setName("tfPModeApplicationRemiseLigne");
    add(tfPModeApplicationRemiseLigne, new GridBagConstraints(1, 19, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPBaseApplicationRemiseLigne ----
    lbPBaseApplicationRemiseLigne.setModeReduit(true);
    lbPBaseApplicationRemiseLigne.setText("Base remise");
    lbPBaseApplicationRemiseLigne.setMinimumSize(new Dimension(30, 22));
    lbPBaseApplicationRemiseLigne.setPreferredSize(new Dimension(30, 22));
    lbPBaseApplicationRemiseLigne.setName("lbPBaseApplicationRemiseLigne");
    add(lbPBaseApplicationRemiseLigne,
        new GridBagConstraints(0, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPBaseApplicationRemiseLigne ----
    tfPBaseApplicationRemiseLigne.setModeReduit(true);
    tfPBaseApplicationRemiseLigne.setEnabled(false);
    tfPBaseApplicationRemiseLigne.setName("tfPBaseApplicationRemiseLigne");
    add(tfPBaseApplicationRemiseLigne,
        new GridBagConstraints(1, 20, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPTopPrixNetLigne ----
    lbPTopPrixNetLigne.setModeReduit(true);
    lbPTopPrixNetLigne.setText("Top prix net");
    lbPTopPrixNetLigne.setMinimumSize(new Dimension(30, 22));
    lbPTopPrixNetLigne.setPreferredSize(new Dimension(30, 22));
    lbPTopPrixNetLigne.setName("lbPTopPrixNetLigne");
    add(lbPTopPrixNetLigne,
        new GridBagConstraints(0, 21, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPTopPrixNetLigne ----
    tfPTopPrixNetLigne.setModeReduit(true);
    tfPTopPrixNetLigne.setEnabled(false);
    tfPTopPrixNetLigne.setName("tfPTopPrixNetLigne");
    add(tfPTopPrixNetLigne, new GridBagConstraints(1, 21, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPPrixNetHTLigne ----
    lbPPrixNetHTLigne.setModeReduit(true);
    lbPPrixNetHTLigne.setText("Prix net HT");
    lbPPrixNetHTLigne.setMinimumSize(new Dimension(30, 22));
    lbPPrixNetHTLigne.setPreferredSize(new Dimension(30, 22));
    lbPPrixNetHTLigne.setName("lbPPrixNetHTLigne");
    add(lbPPrixNetHTLigne,
        new GridBagConstraints(0, 22, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPPrixNetHTLigne ----
    tfPPrixNetHTLigne.setModeReduit(true);
    tfPPrixNetHTLigne.setEnabled(false);
    tfPPrixNetHTLigne.setName("tfPPrixNetHTLigne");
    add(tfPPrixNetHTLigne, new GridBagConstraints(1, 22, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPTopPrixCalculeLigne ----
    lbPTopPrixCalculeLigne.setModeReduit(true);
    lbPTopPrixCalculeLigne.setText("Top prix calcul\u00e9");
    lbPTopPrixCalculeLigne.setMinimumSize(new Dimension(30, 22));
    lbPTopPrixCalculeLigne.setPreferredSize(new Dimension(30, 22));
    lbPTopPrixCalculeLigne.setName("lbPTopPrixCalculeLigne");
    add(lbPTopPrixCalculeLigne,
        new GridBagConstraints(0, 23, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPTopPrixCalculeLigne ----
    tfPTopPrixCalculeLigne.setModeReduit(true);
    tfPTopPrixCalculeLigne.setEnabled(false);
    tfPTopPrixCalculeLigne.setName("tfPTopPrixCalculeLigne");
    add(tfPTopPrixCalculeLigne, new GridBagConstraints(1, 23, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPPrixCalculeHTLigne ----
    lbPPrixCalculeHTLigne.setModeReduit(true);
    lbPPrixCalculeHTLigne.setText("Prix calcul\u00e9 HT");
    lbPPrixCalculeHTLigne.setMinimumSize(new Dimension(30, 22));
    lbPPrixCalculeHTLigne.setPreferredSize(new Dimension(30, 22));
    lbPPrixCalculeHTLigne.setName("lbPPrixCalculeHTLigne");
    add(lbPPrixCalculeHTLigne,
        new GridBagConstraints(0, 24, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPPrixCalculeHTLigne ----
    tfPPrixCalculeHTLigne.setModeReduit(true);
    tfPPrixCalculeHTLigne.setEnabled(false);
    tfPPrixCalculeHTLigne.setName("tfPPrixCalculeHTLigne");
    add(tfPPrixCalculeHTLigne, new GridBagConstraints(1, 24, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPPrixGarantiLigne ----
    lbPPrixGarantiLigne.setModeReduit(true);
    lbPPrixGarantiLigne.setText("Prix garanti \u00e0 la commande");
    lbPPrixGarantiLigne.setMinimumSize(new Dimension(30, 22));
    lbPPrixGarantiLigne.setPreferredSize(new Dimension(30, 22));
    lbPPrixGarantiLigne.setName("lbPPrixGarantiLigne");
    add(lbPPrixGarantiLigne,
        new GridBagConstraints(0, 25, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPPrixGarantiLigne ----
    tfPPrixGarantiLigne.setModeReduit(true);
    tfPPrixGarantiLigne.setEnabled(false);
    tfPPrixGarantiLigne.setName("tfPPrixGarantiLigne");
    add(tfPPrixGarantiLigne, new GridBagConstraints(1, 25, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ---- lbPComplementLibelleLigne ----
    lbPComplementLibelleLigne.setModeReduit(true);
    lbPComplementLibelleLigne.setText("Compl\u00e9ment de libell\u00e9");
    lbPComplementLibelleLigne.setMinimumSize(new Dimension(140, 22));
    lbPComplementLibelleLigne.setPreferredSize(new Dimension(140, 22));
    lbPComplementLibelleLigne.setMaximumSize(new Dimension(140, 22));
    lbPComplementLibelleLigne.setName("lbPComplementLibelleLigne");
    add(lbPComplementLibelleLigne,
        new GridBagConstraints(0, 26, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 3), 0, 0));
    
    // ---- tfPComplementLibelleLigne ----
    tfPComplementLibelleLigne.setModeReduit(true);
    tfPComplementLibelleLigne.setEnabled(false);
    tfPComplementLibelleLigne.setName("tfPComplementLibelleLigne");
    add(tfPComplementLibelleLigne,
        new GridBagConstraints(1, 26, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNLabelChamp lbPIdentifiant;
  private SNTexte tfPIdentifiant;
  private SNLabelChamp lbPCodeCNV;
  private SNTexte tfPCodeCNV;
  private SNLabelChamp lbPColonneTarif;
  private SNTexte tfPColonneTarif;
  private SNLabelChamp lbPCoefficientMultiplicateur2;
  private SNPanel pnlPPourcentageRemise;
  private SNTexte tfPPourcentageRemise1;
  private SNTexte tfPPourcentageRemise2;
  private SNTexte tfPPourcentageRemise3;
  private SNTexte tfPPourcentageRemise4;
  private SNTexte tfPPourcentageRemise5;
  private SNTexte tfPPourcentageRemise6;
  private SNLabelChamp lbPModeApplicationRemise;
  private SNTexte tfPModeApplicationRemise;
  private SNLabelChamp lbPBaseApplicationRemise;
  private SNTexte tfPBaseApplicationRemise;
  private SNLabelChamp lbPDateLivraisonPrevue;
  private SNTexte tfPDateLivraisonPrevue;
  private SNLabelChamp lbPDateLivraisonSouhaitee;
  private SNTexte tfPDateLivraisonSouhaitee;
  private SNLabelChamp lbPDateExpeditionCommande;
  private SNTexte tfPDateExpeditionCommande;
  private SNLabelChamp lbPCodeDevise;
  private SNTexte tfPCodeDevise;
  private JSeparator separator1;
  private SNLabelChamp lbPTopColonneTarifLigne;
  private SNTexte tfPTopColonneTarifLigne;
  private SNLabelChamp lbPColonneTarifLigne;
  private SNTexte tfPColonneTarifLigne;
  private SNLabelChamp lbPTopPrixBaseLigne;
  private SNTexte tfPTopPrixBaseLigne;
  private SNLabelChamp lbPPrixBaseHTLigne;
  private SNMontant tfPPrixBaseHTLigne;
  private SNLabelChamp lbPTopCoefficientLigne;
  private SNTexte tfPTopCoefficientLigne;
  private SNLabelChamp lbPCoefficientMultiplicateurLigne;
  private SNTexte tfPCoefficientMultiplicateurLigne;
  private SNLabelChamp lbPTopRemiseLigne;
  private SNTexte tfPTopRemiseLigne;
  private SNLabelChamp lbPRemiseLigne;
  private SNPanel pnlPPourcentageRemiseLigne;
  private SNTexte tfPPourcentageRemise1Ligne;
  private SNTexte tfPPourcentageRemise2Ligne;
  private SNTexte tfPPourcentageRemise3Ligne;
  private SNTexte tfPPourcentageRemise4Ligne;
  private SNTexte tfPPourcentageRemise5Ligne;
  private SNTexte tfPPourcentageRemise6Ligne;
  private SNLabelChamp lbPModeApplicationremiseLigne;
  private SNTexte tfPModeApplicationRemiseLigne;
  private SNLabelChamp lbPBaseApplicationRemiseLigne;
  private SNTexte tfPBaseApplicationRemiseLigne;
  private SNLabelChamp lbPTopPrixNetLigne;
  private SNTexte tfPTopPrixNetLigne;
  private SNLabelChamp lbPPrixNetHTLigne;
  private SNMontant tfPPrixNetHTLigne;
  private SNLabelChamp lbPTopPrixCalculeLigne;
  private SNTexte tfPTopPrixCalculeLigne;
  private SNLabelChamp lbPPrixCalculeHTLigne;
  private SNMontant tfPPrixCalculeHTLigne;
  private SNLabelChamp lbPPrixGarantiLigne;
  private SNTexte tfPPrixGarantiLigne;
  private SNLabelChamp lbPComplementLibelleLigne;
  private SNTexte tfPComplementLibelleLigne;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
