/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.parametre;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;

/**
 * Classe regroupant tous les paramètres du client pour le calcul du prix de vente.
 * 
 * TODO
 * La deuxième colonne tarif du client :
 * C'est bien ça le champs FACTA du param. 'FA' peut valoir 2. Dans ce cas, la ligne est chiffrée avec la 2ème colonne de la fiche client.
 * La PS n°105 permet aussi de définir une colonne par défaut pour le chiffrage. Cette PS est utilisée pour les clients qui renseignent le
 * PRV dans la colonne n°1. Le colonne 2 est alors le prix de base calculé par PRV (colonne 1) x 1er coef. du tarif.
 */
public class ParametreClient {
  // Variables
  private String identifiantClient = null;
  private Boolean ttc = null;
  private String codeDevise = null;
  private ColonneTarif colonneTarif = null;
  private List<BigDecimal> listeRemise = new ArrayList<BigDecimal>();
  private String codeConditionVente = null;
  private String codeConditionVentePromo = null;
  private List<Integer> listeNumeroCentraleAchat = null;
  // Initialisée à partir du champ CLIN4 du PGVMCLIM
  private boolean nePasUtiliserConditionVenteDG = false;
  private boolean nePasUtiliserConditionVenteQuantitative = false;
  
  // -- Accesseurs
  
  /**
   * Retourne l'identifiant du client sous forme de chaine de caractères (000000-000).
   * @return
   */
  public String getIdentifiantClient() {
    return identifiantClient;
  }
  
  /**
   * Initialise l'identifiant du client sous forme de chaine de caractères (000000-000).
   * @param pIdentifiantClient
   */
  public void setIdentifiantClient(String pIdentifiantClient) {
    this.identifiantClient = pIdentifiantClient;
  }
  
  /**
   * Retourne si le client est facturé en TTC.
   * @return
   */
  public Boolean getTTC() {
    return ttc;
  }
  
  /**
   * Initialise si le client est facturé en TTC.
   * @param ttc
   */
  public void setTTC(Boolean ttc) {
    this.ttc = ttc;
  }
  
  /**
   * Retourne le code devise du client.
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Initialise le code devise du client.
   * @param codeDevise
   */
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  /**
   * Retourne la première colonne tarif du client.
   * @return
   */
  public ColonneTarif getColonneTarif() {
    return colonneTarif;
  }
  
  /**
   * Initialise la première colonne tarif du client.
   * @return
   */
  public void setColonneTarif(ColonneTarif pColonneTarif) {
    this.colonneTarif = pColonneTarif;
  }
  
  /**
   * Retourne la liste des remises du client.
   * @return
   */
  public List<BigDecimal> getListeRemise() {
    return listeRemise;
  }
  
  /**
   * Ajoute une remise à la liste des remises du client.
   * @param listeRemise
   */
  public void ajouterRemise(BigDecimal pRemise) {
    listeRemise.add(pRemise);
  }
  
  /**
   * Retourne le code condition de vente du client.
   * @return
   */
  public String getCodeConditionVente() {
    return codeConditionVente;
  }
  
  /**
   * Initialise le code condition de vente du client.
   * @param pCodeConditionVente
   */
  public void setCodeConditionVente(String pCodeConditionVente) {
    this.codeConditionVente = pCodeConditionVente;
  }
  
  /**
   * Retourne le code condition de vente promo du client.
   * @return
   */
  public String getCodeConditionVentePromo() {
    return codeConditionVentePromo;
  }
  
  /**
   * Initialise le code condition de vente promo du client.
   * @param pCodeConditionVentePromo
   */
  public void setCodeConditionVentePromo(String pCodeConditionVentePromo) {
    this.codeConditionVentePromo = pCodeConditionVentePromo;
  }
  
  /**
   * Retourne la liste des numéros des centrales d'achat du client.
   * @return
   */
  public List<Integer> getListeNumeroCentraleAchat() {
    return listeNumeroCentraleAchat;
  }
  
  /**
   * Initialise la liste des numéros des centrales d'achat du client.
   * @param pListeNumeroCentraleAchat
   */
  public void setListeNumeroCentraleAchat(List<Integer> pListeNumeroCentraleAchat) {
    this.listeNumeroCentraleAchat = pListeNumeroCentraleAchat;
  }
  
  /**
   * Retourne si la condition de vente de la DG doit être ignoré pour ce client.
   * @return
   */
  public boolean isNePasUtiliserConditionVenteDG() {
    return nePasUtiliserConditionVenteDG;
  }
  
  /**
   * Initialise si la condition de vente de la DG doit être ignoré pour ce client.
   * @param pNePasUtiliserConditionVenteDG
   */
  public void setNePasUtiliserConditionVenteDG(boolean pNePasUtiliserConditionVenteDG) {
    this.nePasUtiliserConditionVenteDG = pNePasUtiliserConditionVenteDG;
  }
  
  /**
   * Retourne si les conditions de vente quantitatives doivent être ignorées pour ce client.
   * @return
   */
  public boolean isNePasUtiliserConditionVenteQuantitative() {
    return nePasUtiliserConditionVenteQuantitative;
  }
  
  /**
   * Initialise si les conditions de vente quantitatives doivent être ignorées pour ce client.
   * @param pNePasUtiliserConditionVenteQuantitative
   */
  public void setNePasUtiliserConditionVenteQuantitative(boolean pNePasUtiliserConditionVenteQuantitative) {
    this.nePasUtiliserConditionVenteQuantitative = pNePasUtiliserConditionVenteQuantitative;
  }
  
}
