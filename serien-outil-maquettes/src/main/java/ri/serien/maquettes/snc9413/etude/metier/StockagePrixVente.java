/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier;

import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe stocke le prix de base et le prix net.
 * Aucun calcul n'est effectué dans cette classe, c'est du stockage pur.
 */
public class StockagePrixVente {
  // Variables
  private int nombreDecimale = CalculPrixVente.NOMBRE_DECIMALE_MONTANT;
  private BigDecimal prixBaseHT = null;
  private EnumOrigine originePrixBase = null;
  private BigDecimal prixNetHT = null;
  private EnumOrigine originePrixNet = null;
  
  /**
   * Constructeur.
   */
  public StockagePrixVente(int pNombreDecimale) {
    if (pNombreDecimale < 0) {
      throw new MessageErreurException("Le nombre de décimales passé en paramètre est invalide.");
    }
    nombreDecimale = pNombreDecimale;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le nombre de décimales.
   * @return
   */
  public int getNombreDecimale() {
    return nombreDecimale;
  }
  
  /**
   * Retourne le prix de base HT.
   * @return
   */
  public BigDecimal getPrixBaseHT() {
    return prixBaseHT;
  }
  
  /**
   * Initialise le prix de base HT.
   * @return
   */
  public void setPrixBaseHT(BigDecimal pPrixBaseHT) {
    if (pPrixBaseHT == null) {
      prixBaseHT = null;
      return;
    }
    prixBaseHT = pPrixBaseHT.setScale(nombreDecimale, RoundingMode.HALF_UP);
  }
  
  /**
   * Retourne le prix net HT.
   * @return
   */
  public BigDecimal getPrixNetHT() {
    return prixNetHT;
  }
  
  /**
   * Initialise le prix net HT.
   * @return
   */
  public void setPrixNetHT(BigDecimal pPrixNetHT) {
    if (pPrixNetHT == null) {
      prixNetHT = null;
      return;
    }
    prixNetHT = pPrixNetHT.setScale(nombreDecimale, RoundingMode.HALF_UP);
  }
  
  /**
   * Retourne l'origine du prix de base.
   * @return
   */
  public EnumOrigine getOriginePrixBase() {
    return originePrixBase;
  }
  
  /**
   * Initialise l'origine du prix de base.
   * @param originePrixBase
   */
  public void setOriginePrixBase(EnumOrigine originePrixBase) {
    this.originePrixBase = originePrixBase;
  }
  
  /**
   * Retourne l'origine du prix net.
   * @return
   */
  public EnumOrigine getOriginePrixNet() {
    return originePrixNet;
  }
  
  /**
   * Initialise l'origine du prix net.
   * @param originePrixNet
   */
  public void setOriginePrixNet(EnumOrigine originePrixNet) {
    this.originePrixNet = originePrixNet;
  }
  
}
