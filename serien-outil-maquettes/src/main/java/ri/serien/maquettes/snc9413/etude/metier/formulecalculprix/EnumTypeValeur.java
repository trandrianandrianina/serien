/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.formulecalculprix;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste les types de valeurs possibles dans les formules de calcul de prix du paramètre 'FP'.
 */
public enum EnumTypeValeur {
  COEFFICIENT(0, "Coefficient"),
  VALEUR(1, "Valeur");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeValeur(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(numero);
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumTypeValeur valueOfByCode(Integer pNumero) {
    for (EnumTypeValeur value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de la valeur est invalide : " + pNumero);
  }
  
}
