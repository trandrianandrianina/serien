/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.parametre;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;

/**
 * Classe regroupant tous les paramètres du document de vente pour le calcul du prix de vente.
 */
public class ParametreDocumentVente {
  // Variables entête
  private String identifiant = null;
  private String codeConditionVente = null;
  private ColonneTarif colonneTarif = null;
  private List<BigPercentage> listeRemiseEnPourcentage = null;
  // Remise en cascade ou en ajout
  private boolean modeRemiseEnCascade = true;
  // Base sur laquelle la remise s'applique (prix unitaire ou montant de la ligne)
  private boolean baseRemisePrixUnitaire = true;
  private Date dateLivraisonPrevue = null;
  private Date dateLivraisonSouhaitee = null;
  private Date dateExpeditionCommande = null;
  private String codeDevise = null;
  
  // Variables ligne
  private Integer topColonneTarifLigne = null;
  private ColonneTarif colonneTarifLigne = null;
  private Integer topPrixBaseSaisiLigne = null;
  private BigDecimal prixBaseHTSaisiLigne = null;
  private Integer topCoefficientSaisiLigne = null;
  private BigDecimal coefficientSaisiLigne = null;
  private Integer topRemiseSaisieLigne = null;
  private List<BigPercentage> listeRemiseEnPourcentageSaisieLigne = null;
  // Remise en cascade ou en ajout
  private boolean modeRemiseEnCascadeLigne = true;
  // Base sur laquelle la remise s'applique (prix unitaire ou montant de la ligne)
  private boolean baseRemisePrixUnitaireLigne = true;
  private Integer topPrixNetSaisiLigne = null;
  private BigDecimal prixNetHTSaisiLigne = null;
  private Integer topPrixCalculeSaisiLigne = null;
  private BigDecimal prixCalculeHTSaisiLigne = null;
  private boolean prixGarantiCommande = false;
  private String complementLibelle = null;
  
  // -- Accesseurs
  
  /**
   * Retourne l'identifiant du document de vente sous forme de chaine de caractères.
   * @return
   */
  public String getIdentifiant() {
    return identifiant;
  }
  
  /**
   * Initialise l'identifiant du document de vente sous forme de chaine de caractères.
   * @param pIdentifiant
   */
  public void setIdentifiant(String pIdentifiant) {
    this.identifiant = pIdentifiant;
  }
  
  /**
   * Retourne le code de la condition de vente sous forme de chaine de caractères.
   * @return
   */
  public String getCodeConditionVente() {
    return codeConditionVente;
  }
  
  /**
   * Initialise le code de la condition de vente sous forme de chaine de caractères.
   * @param pCodeCNV
   */
  public void setCodeConditionVente(String pCodeCNV) {
    this.codeConditionVente = pCodeCNV;
  }
  
  /**
   * Retourne la colonne tarif de l'entête du documment de vente.
   * @return
   */
  public ColonneTarif getColonneTarif() {
    return colonneTarif;
  }
  
  /**
   * Initialise la colonne tarif de l'entête du documment de vente.
   * @param pColonneTarif
   */
  public void setColonneTarif(ColonneTarif pColonneTarif) {
    this.colonneTarif = pColonneTarif;
  }
  
  /**
   * Retourne la liste des remises en pourcentage.
   * @return
   */
  public List<BigPercentage> getListeRemiseEnPourcentage() {
    return listeRemiseEnPourcentage;
  }
  
  /**
   * Initialise la liste des remises en pourcentage.
   * @param pListeRemiseEnPourcentage
   */
  public void setListeRemiseEnPourcentage(List<BigPercentage> pListeRemiseEnPourcentage) {
    this.listeRemiseEnPourcentage = pListeRemiseEnPourcentage;
  }
  
  /**
   * Retourne le mode d'application des remises.
   * @return
   */
  public boolean isModeRemiseEnCascade() {
    return modeRemiseEnCascade;
  }
  
  /**
   * Initialise le mode d'application des remises.
   * @param pModeRemiseEnCascade
   */
  public void setModeRemiseEnCascade(boolean pModeRemiseEnCascade) {
    this.modeRemiseEnCascade = pModeRemiseEnCascade;
  }
  
  /**
   * Retourne la base sur laquelle les remises s'appliquent (prix untaire ou montant ligne).
   * @return
   */
  public boolean isBaseRemisePrixUnitaire() {
    return baseRemisePrixUnitaire;
  }
  
  /**
   * Initialise la base sur laquelle les remises s'appliquent (prix untaire ou montant ligne).
   * @param pBaseRemisePrixUnitaire
   */
  public void setBaseRemisePrixUnitaire(boolean pBaseRemisePrixUnitaire) {
    this.baseRemisePrixUnitaire = pBaseRemisePrixUnitaire;
  }
  
  /**
   * Retourne la date de livraison préve.
   * @return
   */
  public Date getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  /**
   * Initialise la date de livraison prévue.
   * @param pDateLivraisonPrevue
   */
  public void setDateLivraisonPrevue(Date pDateLivraisonPrevue) {
    this.dateLivraisonPrevue = pDateLivraisonPrevue;
  }
  
  /**
   * Retourne la date de livraison souhaitée.
   * @return
   */
  public Date getDateLivraisonSouhaitee() {
    return dateLivraisonSouhaitee;
  }
  
  /**
   * Initialise la date de livraison souhaitée.
   * @param pDateLivraisonSouhaitee
   */
  public void setDateLivraisonSouhaitee(Date pDateLivraisonSouhaitee) {
    this.dateLivraisonSouhaitee = pDateLivraisonSouhaitee;
  }
  
  /**
   * Retourne la date d'expédition de la commande.
   * @return
   */
  public Date getDateExpeditionCommande() {
    return dateExpeditionCommande;
  }
  
  /**
   * Initialise la date d'expédition de la commande.
   * @param pDateExpeditionCommande
   */
  public void setDateExpeditionCommande(Date pDateExpeditionCommande) {
    this.dateExpeditionCommande = pDateExpeditionCommande;
  }
  
  /**
   * Retourne le code de la devise.
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Initialise le code de la devise.
   * @param codeDevise
   */
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  /**
   * Retourne le top de la colonne tarif de la ligne.
   * @return
   */
  public Integer getTopColonneTarifLigne() {
    return topColonneTarifLigne;
  }
  
  /**
   * Initialise le top de la colonne tarif de la ligne.
   * @param pTopColonneTarifLigne
   */
  public void setTopColonneTarifLigne(Integer pTopColonneTarifLigne) {
    this.topColonneTarifLigne = pTopColonneTarifLigne;
  }
  
  /**
   * Retourne la colonne tarif de la ligne du documment de vente.
   * @return
   */
  public ColonneTarif getColonneTarifLigne() {
    return colonneTarifLigne;
  }
  
  /**
   * Initialise la colonne tarif de la ligne du documment de vente.
   * @param pColonneTarifLigne
   */
  public void setColonneTarifLigne(ColonneTarif pColonneTarifLigne) {
    this.colonneTarifLigne = pColonneTarifLigne;
  }
  
  /**
   * Retourne le top du prix de base saisi de la ligne.
   * @return
   */
  public Integer getTopPrixBaseSaisiLigne() {
    return topPrixBaseSaisiLigne;
  }
  
  /**
   * Initialise le top du prix de base saisi de la ligne.
   * @param pTopPrixBaseLigne
   */
  public void setTopPrixBaseSaisiLigne(Integer pTopPrixBaseLigne) {
    this.topPrixBaseSaisiLigne = pTopPrixBaseLigne;
  }
  
  /**
   * Retourne le prix de base HT saisi de la ligne.
   * @return
   */
  public BigDecimal getPrixBaseHTSaisiLigne() {
    return prixBaseHTSaisiLigne;
  }
  
  /**
   * Initialise le prix de base HT saisi de la ligne.
   * @param pPrixBaseHTLigne
   */
  public void setPrixBaseHTSaisiLigne(BigDecimal pPrixBaseHTLigne) {
    this.prixBaseHTSaisiLigne = pPrixBaseHTLigne;
  }
  
  /**
   * Retourne le top du coefficient saisi de la ligne.
   * @return
   */
  public Integer getTopCoefficientSaisiLigne() {
    return topCoefficientSaisiLigne;
  }
  
  /**
   * Initialise le top du coefficient saisi de la ligne.
   * @param pTopCoefficientLigne
   */
  public void setTopCoefficientSaisiLigne(Integer pTopCoefficientLigne) {
    this.topCoefficientSaisiLigne = pTopCoefficientLigne;
  }
  
  /**
   * Retourne le coeffcient multiplicateur saisi de la ligne.
   * @return
   */
  public BigDecimal getCoefficientSaisiLigne() {
    return coefficientSaisiLigne;
  }
  
  /**
   * Initialise le coeffcient multiplicateur saisi de la ligne.
   * @param pCoefficientLigne
   */
  public void setCoefficientSaisiLigne(BigDecimal pCoefficientLigne) {
    this.coefficientSaisiLigne = pCoefficientLigne;
  }
  
  /**
   * Retourne le top des remises saisies de la ligne.
   * @return
   */
  public Integer getTopRemiseSaisieLigne() {
    return topRemiseSaisieLigne;
  }
  
  /**
   * Initialise le top des remises saisies de la ligne.
   * @param pTopRemiseSaisieLigne
   */
  public void setTopRemiseSaisieLigne(Integer pTopRemiseSaisieLigne) {
    this.topRemiseSaisieLigne = pTopRemiseSaisieLigne;
  }
  
  /**
   * Retourne la liste des remises saisies de la ligne.
   * @return
   */
  public List<BigPercentage> getListeRemiseEnPourcentageSaisieLigne() {
    return listeRemiseEnPourcentageSaisieLigne;
  }
  
  /**
   * Initialise la liste des remises saisies de la ligne.
   * @param pListeRemiseSaisieLigne
   */
  public void setListeRemiseEnPourcentageSaisieLigne(List<BigPercentage> pListeRemiseSaisieLigne) {
    this.listeRemiseEnPourcentageSaisieLigne = pListeRemiseSaisieLigne;
  }
  
  /**
   * Retourne le mode d'application des remises de la ligne.
   * @return
   */
  public boolean isModeRemiseEnCascadeLigne() {
    return modeRemiseEnCascadeLigne;
  }
  
  /**
   * Initialise le mode d'application des remises de la ligne.
   * @param pModeRemiseEnCascade
   */
  public void setModeRemiseEnCascadeLigne(boolean pModeRemiseEnCascade) {
    this.modeRemiseEnCascadeLigne = pModeRemiseEnCascade;
  }
  
  /**
   * Retourne la base sur laquelle les remises s'appliquent (prix untaire ou montant ligne) de la ligne.
   * @return
   */
  public boolean isBaseRemisePrixUnitaireLigne() {
    return baseRemisePrixUnitaireLigne;
  }
  
  /**
   * Initialise la base sur laquelle les remises s'appliquent (prix untaire ou montant ligne) de la ligne.
   * @param pBaseRemisePrixUnitaire
   */
  public void setBaseRemisePrixUnitaireLigne(boolean pBaseRemisePrixUnitaire) {
    this.baseRemisePrixUnitaireLigne = pBaseRemisePrixUnitaire;
  }
  
  /**
   * Retourne le top du prix net saisi de la ligne.
   * @return
   */
  public Integer getTopPrixNetSaisiLigne() {
    return topPrixNetSaisiLigne;
  }
  
  /**
   * Initialise le top du prix net saisi de la ligne.
   * @param pTopPrixNetLigne
   */
  public void setTopPrixNetSaisiLigne(Integer pTopPrixNetLigne) {
    this.topPrixNetSaisiLigne = pTopPrixNetLigne;
  }
  
  /**
   * Retourne le prix net HT saisi de la ligne.
   * @return
   */
  public BigDecimal getPrixNetHTSaisiLigne() {
    return prixNetHTSaisiLigne;
  }
  
  /**
   * Initialise le prix net HT saisi de la ligne.
   * @param pPrixNetHTLigne
   */
  public void setPrixNetHTSaisiLigne(BigDecimal pPrixNetHTLigne) {
    this.prixNetHTSaisiLigne = pPrixNetHTLigne;
  }
  
  /**
   * Retourne le top du prix calculé saisi de la ligne.
   * @return
   */
  public Integer getTopPrixCalculeSaisiLigne() {
    return topPrixCalculeSaisiLigne;
  }
  
  /**
   * Initialise le top du prix calculé saisi de la ligne.
   * @param pTopPrixCalculeLigne
   */
  public void setTopPrixCalculeSaisiLigne(Integer pTopPrixCalculeLigne) {
    this.topPrixCalculeSaisiLigne = pTopPrixCalculeLigne;
  }
  
  /**
   * Retourne le prix calculé HT saisi de la ligne.
   * @return
   */
  public BigDecimal getPrixCalculeHTSaisiLigne() {
    return prixCalculeHTSaisiLigne;
  }
  
  /**
   * Initialise le prix calculé HT saisi de la ligne.
   * @param pPrixCalculeHTLigne
   */
  public void setPrixCalculeHTSaisiLigne(BigDecimal pPrixCalculeHTLigne) {
    this.prixNetHTSaisiLigne = pPrixCalculeHTLigne;
  }
  
  /**
   * Retourne si le prix est garanti à la commande de la ligne.
   * @return
   */
  public boolean isPrixGarantiCommande() {
    return prixGarantiCommande;
  }
  
  /**
   * Initialise si le prix est garanti à la commande de la ligne.
   * @param pPrixGarantiCommande
   */
  public void setPrixGarantiCommande(boolean pPrixGarantiCommande) {
    this.prixGarantiCommande = pPrixGarantiCommande;
  }
  
  /**
   * Retourne le complément de libellé de la ligne.
   * @return
   */
  public String getComplementLibelle() {
    return complementLibelle;
  }
  
  /**
   * Initialise le complément de libellé de la ligne.
   * @param pComplementLibelle
   */
  public void setComplementLibelle(String pComplementLibelle) {
    this.complementLibelle = pComplementLibelle;
  }
  
}
