/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Liste des origines possibles d'une colonne tarif.
 * La liste est hiérarchisée dans l'ordre de recherche.
 */
public enum EnumOrigineColonneTarif {
  SAISIE(0, "Saisie"),
  DOCUMENT_VENTE(1, "Document de vente"),
  CONDITION_VENTE(2, "Condition de vente"),
  CLIENT(3, "Fiche client"),
  PS105(4, "PS105"),
  UN(5, "Colonne 1");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumOrigineColonneTarif(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numero sous lequel la valeur est persistée en base de données.
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associé au nom de la variable.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(numero);
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumOrigineColonneTarif valueOfByCode(Integer pNumero) {
    for (EnumOrigineColonneTarif value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("L'origine de la colonne tarif est invalide : " + pNumero);
  }
  
}
