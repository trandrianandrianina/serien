/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.parametre;

import java.util.Date;

import ri.serien.maquettes.snc9413.etude.metier.ColonneTarif;

/**
 * Classe regroupant tous les paramètres généraux pour le calcul du prix de vente.
 */
public class ParametreGeneral {
  // Variables
  private String nomBaseDeDonnees = null;
  // Grâce à la PS287 qui si elle vaut 1 alors le mode négoce est activé
  private boolean modeNegoce = false;
  private String codeEtablissement = null;
  private String codeDevise = null;
  private String codeCNVStandardDG = null;
  private String codeCNVPromoDG = null;
  private String codeCNVQuantitativeDG = null;
  private Date moisEnCours = null;
  // La PS105 peut contenir un numéro de colonne tarif à utiliser
  private ColonneTarif colonnePS105 = null;
  // La PS305 indique qu'il faut rechercher le prix dans la colonne précédente si le prix de l'actuelle vaut 0 (PS305)
  private Boolean chercherColonnePrecedente = null;
  // Prise en compte des conditions de vente quantitatives (PS047)
  private Boolean ignoreCNVQuantitative = null;
  // Indicateur du choix de la date d'application de la condition de vente (PS119)
  private Character origineDateApplicationCNV = null;
  // « Emboîtage » condition centrale (PS216)
  private Boolean activeEmboitageCNV = null;
  // Recherche de condition de vente sur éléments d'achat (Code regroupement et fournisseur) (PS253)
  private Character activeRechercheCNVFournisseur = null;
  // Calcul prix net à partir tarif colonne 1 (PS316)
  private Boolean activeCalculPrixNetAvecColonne1 = null;
  
  // -- Accesseurs
  
  /**
   * Retourne le nom de la base de données.
   * @return
   */
  public String getNomBaseDeDonnees() {
    return nomBaseDeDonnees;
  }
  
  /**
   * Initialise le nom de la base données.
   * @param nomBaseDeDonnees
   */
  public void setNomBaseDeDonnees(String nomBaseDeDonnees) {
    this.nomBaseDeDonnees = nomBaseDeDonnees;
  }
  
  /**
   * Indique si le mode négoce est actif.
   * @return
   */
  public boolean isModeNegoce() {
    return modeNegoce;
  }
  
  /**
   * Initialise si le mode négoce doit être activé.
   * @param modeNegoce
   */
  public void setModeNegoce(boolean modeNegoce) {
    this.modeNegoce = modeNegoce;
  }
  
  /**
   * Retourne le code établissement.
   * @return
   */
  public String getCodeEtablissement() {
    return codeEtablissement;
  }
  
  /**
   * Initialise le code établissement.
   * @param codeEtablissement
   */
  public void setCodeEtablissement(String codeEtablissement) {
    this.codeEtablissement = codeEtablissement;
  }
  
  /**
   * Retourne le code la devise de la DG.
   * @return
   */
  public String getCodeDevise() {
    return codeDevise;
  }
  
  /**
   * Initialise le code de la devise de la DG.
   * @param codeDevise
   */
  public void setCodeDevise(String codeDevise) {
    this.codeDevise = codeDevise;
  }
  
  /**
   * Retourne le code de la condition de vente standard de la DG.
   * @return
   */
  public String getCodeCNVStandardDG() {
    return codeCNVStandardDG;
  }
  
  /**
   * Initialiser le code de la condition de vente standard de la DG.
   * @param pCodeCNVStandardDG
   */
  public void setCodeCNVStandardDG(String pCodeCNVStandardDG) {
    this.codeCNVStandardDG = pCodeCNVStandardDG;
  }
  
  /**
   * Retourne le code de la condition de vente promo de la DG.
   * @return
   */
  public String getCodeCNVPromoDG() {
    return codeCNVPromoDG;
  }
  
  /**
   * Initialise le code de la condition de vente promo de la DG.
   * @param pCodeCNVPromoDG
   */
  public void setCodeCNVPromoDG(String pCodeCNVPromoDG) {
    this.codeCNVPromoDG = pCodeCNVPromoDG;
  }
  
  /**
   * Retourne le code de la condition de vente quantitative de la DG.
   * @return
   */
  public String getCodeCNVQuantitativeDG() {
    return codeCNVQuantitativeDG;
  }
  
  /**
   * Initialise le code de la condition de vente quantitative de la DG.
   * @param pCodeCNVQuantitativeDG
   */
  public void setCodeCNVQuantitativeDG(String pCodeCNVQuantitativeDG) {
    this.codeCNVQuantitativeDG = pCodeCNVQuantitativeDG;
  }
  
  /**
   * Retourne le mois en cours de la DG (date du 1° du mois).
   * @return
   */
  public Date getMoisEnCours() {
    return moisEnCours;
  }
  
  /**
   * Initialise le mois en cours de la DG (date du 1° du mois).
   * @param pMoisEnCours
   */
  public void setMoisEnCours(Date pMoisEnCours) {
    this.moisEnCours = pMoisEnCours;
  }
  
  /**
   * Retourne la colonne stockée dans la PS105.
   * @return
   */
  public ColonneTarif getColonnePS105() {
    return colonnePS105;
  }
  
  /**
   * Initilise la colonne stockée dans la PS105.
   * @param pColonnePS105
   */
  public void setColonnePS105(ColonneTarif pColonnePS105) {
    this.colonnePS105 = pColonnePS105;
  }
  
  /**
   * Retourne si les colonnes précédentes doivent être balayées si le prix vaut 0 (PS305).
   * @return
   */
  public Boolean getChercherColonnePrecedente() {
    return chercherColonnePrecedente;
  }
  
  /**
   * Initialise si les colonnes précédentes doivent être balayées si le prix vaut 0 (PS305).
   * @param ps305
   */
  public void setChercherColonnePrecedente(Boolean ps305) {
    this.chercherColonnePrecedente = ps305;
  }
  
  /**
   * Retourne si les CNV quantitatives sont prise en compte (PS047).
   * @return
   */
  public Boolean getIgnoreCNVQuantitative() {
    return ignoreCNVQuantitative;
  }
  
  /**
   * Initialise si les CNV quantitatives sont prise en compte (PS047).
   * @param pIgnoreCNVQuantitative
   */
  public void setIgnoreCNVQuantitativePS047(Boolean pIgnoreCNVQuantitative) {
    this.ignoreCNVQuantitative = pIgnoreCNVQuantitative;
  }
  
  /**
   * Retourner le choix de la date d'application des conditions de vente (PS119).
   * @return
   */
  public Character getOrigineDateApplicationCNV() {
    return origineDateApplicationCNV;
  }
  
  /**
   * Initialiser le choix de la date d'application des conditions de vente (PS119).
   * @param pOrigineDateApplicationCNV
   */
  public void setOrigineDateApplicationCNV(Character pOrigineDateApplicationCNV) {
    this.origineDateApplicationCNV = pOrigineDateApplicationCNV;
  }
  
  /**
   * Retourne si l'emboitage des CNV est actif (PS216).
   * @return
   */
  public Boolean getActiveEmboitageCNV() {
    return activeEmboitageCNV;
  }
  
  /**
   * Initialise si l'emboitage des CNV est actif (PS216).
   * @param pActiveEmboitageCNV
   */
  public void setActiveEmboitageCNV(Boolean pActiveEmboitageCNV) {
    this.activeEmboitageCNV = pActiveEmboitageCNV;
  }
  
  /**
   * Retourne si la recherche des conditions de vente sur fournisseur est actif (PS253).
   * @return
   */
  public Character getActiveRechercheCNVFournisseur() {
    return activeRechercheCNVFournisseur;
  }
  
  /**
   * Initialise si la recherche des conditions de vente sur fournisseur est actif (PS253).
   * @param pActiveRechercheCNVFournisseur
   */
  public void setActiveRechercheCNVFournisseur(Character pActiveRechercheCNVFournisseur) {
    this.activeRechercheCNVFournisseur = pActiveRechercheCNVFournisseur;
  }
  
  /**
   * Retourne si le calcul du prix net est actif (PS316).
   * @return
   */
  public Boolean getActiveCalculPrixNetAvecColonne1() {
    return activeCalculPrixNetAvecColonne1;
  }
  
  /**
   * Initialise si le calcul du prix net est actif (PS316).
   * @param pActiveCalculPrixNetAvecColonne1
   */
  public void setActiveCalculPrixNetAvecColonne1(Boolean pActiveCalculPrixNetAvecColonne1) {
    this.activeCalculPrixNetAvecColonne1 = pActiveCalculPrixNetAvecColonne1;
  }
  
}
