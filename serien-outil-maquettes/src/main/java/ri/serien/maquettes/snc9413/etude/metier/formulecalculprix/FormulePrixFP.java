/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9413.etude.metier.formulecalculprix;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

/**
 * Cette classe contient une liste de lignes de formule de calcul de prix du paramètre 'FP'.
 */
public class FormulePrixFP extends ArrayList<LigneFormulePrix> {
  // Variables
  private String codeFormulePrix = null;
  
  /**
   * Applique la formule sur le prix de base HT en paramètre afin de calculer le prix net HT.
   * 
   * @param pPrixBaseHT
   * @param pNombreDecimale
   * @return
   */
  public BigDecimal calculerPrixNetHT(BigDecimal pPrixBaseHT, int pNombreDecimale) {
    if (pPrixBaseHT == null) {
      return null;
    }
    if (isEmpty()) {
      return pPrixBaseHT.setScale(pNombreDecimale, RoundingMode.HALF_UP);
    }
    
    // Application de la formule
    BigDecimal prixNetHT = pPrixBaseHT;
    for (LigneFormulePrix ligne : this) {
      prixNetHT = ligne.calculer(prixNetHT, pPrixBaseHT, pNombreDecimale);
    }
    return prixNetHT;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le code de la formule du calcul du prix.
   * @return
   */
  public String getCodeFormulePrix() {
    return codeFormulePrix;
  }
  
  /**
   * Initialise le code de la formule du calcul du prix.
   * @param pCodeFormulePrix
   */
  public void setCodeFormulePrix(String pCodeFormulePrix) {
    this.codeFormulePrix = pCodeFormulePrix;
  }
  
}
