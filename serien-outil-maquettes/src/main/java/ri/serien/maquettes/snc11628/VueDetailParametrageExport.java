/*
 * Created by JFormDesigner on Wed Oct 19 12:03:30 CEST 2022
 */

package ri.serien.maquettes.snc11628;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.SpinnerNumberModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.desktop.metier.exploitation.export.ModeleExport;
import ri.serien.libswing.composant.metier.exploitation.utilisateur.snutilisateur.SNUtilisateur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDeselectionTotale;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDeselectionUnitaire;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonSelectionParDefaut;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonSelectionTotale;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonSelectionUnitaire;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * @author User #8
 */
public class VueDetailParametrageExport extends AbstractVuePanel<ModeleExport> {
  
  public VueDetailParametrageExport(ModeleExport pModele) {
    super(pModele);
    initComponents();
    // XXX Auto-generated constructor stub
  }
  
  @Override
  public void initialiserComposants() {
    // XXX Auto-generated method stub
    
  }
  
  @Override
  public void rafraichir() {
    // XXX Auto-generated method stub
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    sNPanelFond1 = new SNPanelFond();
    sNBarreBouton1 = new SNBarreBouton();
    sNBandeauTitre1 = new SNBandeauTitre();
    sNPanelContenu1 = new SNPanelContenu();
    tabbedPane1 = new JTabbedPane();
    pnlGeneral = new SNPanel();
    sNPanel10 = new SNPanel();
    sNPanel13 = new SNPanel();
    sNPanel11 = new SNPanel();
    sNLabelChamp1 = new SNLabelChamp();
    sNComboBox1 = new SNComboBox();
    sNLabelChamp5 = new SNLabelChamp();
    sNUtilisateur1 = new SNUtilisateur();
    sNLabelChamp6 = new SNLabelChamp();
    scrollPane3 = new JScrollPane();
    textArea1 = new JTextArea();
    pnlContenu = new SNPanelTitre();
    sNPanel8 = new SNPanel();
    xRiCheckBox1 = new XRiCheckBox();
    xRiCheckBox5 = new XRiCheckBox();
    xRiCheckBox2 = new XRiCheckBox();
    xRiCheckBox6 = new XRiCheckBox();
    xRiCheckBox3 = new XRiCheckBox();
    xRiCheckBox7 = new XRiCheckBox();
    xRiCheckBox4 = new XRiCheckBox();
    sNPanel14 = new SNPanel();
    sNLabelChamp11 = new SNLabelChamp();
    spinner1 = new JSpinner();
    sNLabelTitre4 = new SNMessage();
    sNPanel12 = new SNPanel();
    sNLabelChamp4 = new SNLabelChamp();
    sNEtablissement2 = new SNEtablissement();
    sNLabelChamp7 = new SNLabelChamp();
    sNDate1 = new SNDate();
    sNLabelChamp8 = new SNLabelChamp();
    sNDate2 = new SNDate();
    sNLabelChamp9 = new SNLabelChamp();
    sNDate3 = new SNDate();
    pnlChampAExporter = new SNPanelContenu();
    snSelectionChampMetier = new SNPanel();
    sNLabelTitre1 = new SNLabelTitre();
    sNLabelTitre2 = new SNLabelTitre();
    spGauche = new JScrollPane();
    tblGauche = new NRiTable();
    pnlBoutonCentral = new SNPanel();
    snBoutonSelectionUnitaire = new SNBoutonSelectionUnitaire();
    snBoutonDeselectionUnitaire = new SNBoutonDeselectionUnitaire();
    snBoutonSelectionTotale = new SNBoutonSelectionTotale();
    snBoutonDeselectionTotale = new SNBoutonDeselectionTotale();
    snBoutonSelectionParDefaut = new SNBoutonSelectionParDefaut();
    spDroite = new JScrollPane();
    tblDroite = new NRiTable();
    pnlFiltre = new SNPanel();
    sNPanel5 = new SNPanel();
    sNLabelChamp10 = new SNLabelChamp();
    sNComboBox2 = new SNComboBox();
    sNPanel3 = new SNPanel();
    sNLabelChamp2 = new SNLabelChamp();
    sNEtablissement1 = new SNEtablissement();
    pnlPrevisualisation = new JScrollPane();
    nRiTable2 = new NRiTable();
    sNPanel6 = new SNPanel();
    
    // ======== this ========
    setName("this");
    setLayout(new GridLayout());
    
    // ======== sNPanelFond1 ========
    {
      sNPanelFond1.setName("sNPanelFond1");
      sNPanelFond1.setLayout(new BorderLayout());
      
      // ---- sNBarreBouton1 ----
      sNBarreBouton1.setName("sNBarreBouton1");
      sNPanelFond1.add(sNBarreBouton1, BorderLayout.SOUTH);
      
      // ---- sNBandeauTitre1 ----
      sNBandeauTitre1.setText("Modification d'un export");
      sNBandeauTitre1.setName("sNBandeauTitre1");
      sNPanelFond1.add(sNBandeauTitre1, BorderLayout.NORTH);
      
      // ======== sNPanelContenu1 ========
      {
        sNPanelContenu1.setName("sNPanelContenu1");
        sNPanelContenu1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== tabbedPane1 ========
        {
          tabbedPane1.setFont(new Font("sansserif", Font.PLAIN, 14));
          tabbedPane1.setName("tabbedPane1");
          
          // ======== pnlGeneral ========
          {
            pnlGeneral.setName("pnlGeneral");
            pnlGeneral.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGeneral.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlGeneral.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGeneral.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
            
            // ======== sNPanel10 ========
            {
              sNPanel10.setName("sNPanel10");
              sNPanel10.setLayout(new GridLayout(1, 2));
              
              // ======== sNPanel13 ========
              {
                sNPanel13.setName("sNPanel13");
                sNPanel13.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel13.getLayout()).columnWidths = new int[] { 0, 0 };
                ((GridBagLayout) sNPanel13.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) sNPanel13.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
                ((GridBagLayout) sNPanel13.getLayout()).rowWeights = new double[] { 1.0, 1.0, 1.0E-4 };
                
                // ======== sNPanel11 ========
                {
                  sNPanel11.setName("sNPanel11");
                  sNPanel11.setLayout(new GridBagLayout());
                  ((GridBagLayout) sNPanel11.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                  ((GridBagLayout) sNPanel11.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
                  ((GridBagLayout) sNPanel11.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                  ((GridBagLayout) sNPanel11.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
                  
                  // ---- sNLabelChamp1 ----
                  sNLabelChamp1.setText("Objet \u00e0 exporter");
                  sNLabelChamp1.setName("sNLabelChamp1");
                  sNPanel11.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- sNComboBox1 ----
                  sNComboBox1.setModel(new DefaultComboBoxModel<>(new String[] { "Groupe d'articles", "Famille d'articles",
                      "Sous-familles d'articles", "Commandes de ventes", "Factures de ventes", "Commandes d'achats" }));
                  sNComboBox1.setName("sNComboBox1");
                  sNPanel11.add(sNComboBox1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- sNLabelChamp5 ----
                  sNLabelChamp5.setText("Utilisateur");
                  sNLabelChamp5.setName("sNLabelChamp5");
                  sNPanel11.add(sNLabelChamp5, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                  
                  // ---- sNUtilisateur1 ----
                  sNUtilisateur1.setEnabled(false);
                  sNUtilisateur1.setName("sNUtilisateur1");
                  sNPanel11.add(sNUtilisateur1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- sNLabelChamp6 ----
                  sNLabelChamp6.setText("Description");
                  sNLabelChamp6.setName("sNLabelChamp6");
                  sNPanel11.add(sNLabelChamp6, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  
                  // ======== scrollPane3 ========
                  {
                    scrollPane3.setName("scrollPane3");
                    
                    // ---- textArea1 ----
                    textArea1.setLineWrap(true);
                    textArea1.setFont(new Font("sansserif", Font.PLAIN, 14));
                    textArea1.setName("textArea1");
                    scrollPane3.setViewportView(textArea1);
                  }
                  sNPanel11.add(scrollPane3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                sNPanel13.add(sNPanel11, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
                
                // ======== pnlContenu ========
                {
                  pnlContenu.setBackground(new Color(238, 238, 210));
                  pnlContenu.setOpaque(false);
                  pnlContenu.setTitre("Planification");
                  pnlContenu.setName("pnlContenu");
                  pnlContenu.setLayout(new GridBagLayout());
                  ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
                  ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                  ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
                  ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                  
                  // ======== sNPanel8 ========
                  {
                    sNPanel8.setName("sNPanel8");
                    sNPanel8.setLayout(new GridBagLayout());
                    ((GridBagLayout) sNPanel8.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                    ((GridBagLayout) sNPanel8.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
                    ((GridBagLayout) sNPanel8.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
                    ((GridBagLayout) sNPanel8.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                    
                    // ---- xRiCheckBox1 ----
                    xRiCheckBox1.setText("Lundi");
                    xRiCheckBox1.setFont(new Font("sansserif", Font.PLAIN, 14));
                    xRiCheckBox1.setMinimumSize(new Dimension(100, 30));
                    xRiCheckBox1.setPreferredSize(new Dimension(150, 30));
                    xRiCheckBox1.setName("xRiCheckBox1");
                    sNPanel8.add(xRiCheckBox1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                    
                    // ---- xRiCheckBox5 ----
                    xRiCheckBox5.setText("Vendredi");
                    xRiCheckBox5.setFont(new Font("sansserif", Font.PLAIN, 14));
                    xRiCheckBox5.setMinimumSize(new Dimension(100, 30));
                    xRiCheckBox5.setPreferredSize(new Dimension(150, 30));
                    xRiCheckBox5.setName("xRiCheckBox5");
                    sNPanel8.add(xRiCheckBox5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                    
                    // ---- xRiCheckBox2 ----
                    xRiCheckBox2.setText("Mardi");
                    xRiCheckBox2.setFont(new Font("sansserif", Font.PLAIN, 14));
                    xRiCheckBox2.setMinimumSize(new Dimension(100, 30));
                    xRiCheckBox2.setPreferredSize(new Dimension(150, 30));
                    xRiCheckBox2.setName("xRiCheckBox2");
                    sNPanel8.add(xRiCheckBox2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                    
                    // ---- xRiCheckBox6 ----
                    xRiCheckBox6.setText("Samedi");
                    xRiCheckBox6.setFont(new Font("sansserif", Font.PLAIN, 14));
                    xRiCheckBox6.setMinimumSize(new Dimension(100, 30));
                    xRiCheckBox6.setPreferredSize(new Dimension(150, 30));
                    xRiCheckBox6.setName("xRiCheckBox6");
                    sNPanel8.add(xRiCheckBox6, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                    
                    // ---- xRiCheckBox3 ----
                    xRiCheckBox3.setText("Mercredi");
                    xRiCheckBox3.setFont(new Font("sansserif", Font.PLAIN, 14));
                    xRiCheckBox3.setMinimumSize(new Dimension(100, 30));
                    xRiCheckBox3.setPreferredSize(new Dimension(150, 30));
                    xRiCheckBox3.setName("xRiCheckBox3");
                    sNPanel8.add(xRiCheckBox3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                    
                    // ---- xRiCheckBox7 ----
                    xRiCheckBox7.setText("Dimanche");
                    xRiCheckBox7.setFont(new Font("sansserif", Font.PLAIN, 14));
                    xRiCheckBox7.setMinimumSize(new Dimension(100, 30));
                    xRiCheckBox7.setPreferredSize(new Dimension(150, 30));
                    xRiCheckBox7.setName("xRiCheckBox7");
                    sNPanel8.add(xRiCheckBox7, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                    
                    // ---- xRiCheckBox4 ----
                    xRiCheckBox4.setText("Jeudi");
                    xRiCheckBox4.setFont(new Font("sansserif", Font.PLAIN, 14));
                    xRiCheckBox4.setMinimumSize(new Dimension(100, 30));
                    xRiCheckBox4.setPreferredSize(new Dimension(150, 30));
                    xRiCheckBox4.setName("xRiCheckBox4");
                    sNPanel8.add(xRiCheckBox4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                    
                    // ======== sNPanel14 ========
                    {
                      sNPanel14.setName("sNPanel14");
                      sNPanel14.setLayout(new GridBagLayout());
                      ((GridBagLayout) sNPanel14.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                      ((GridBagLayout) sNPanel14.getLayout()).rowHeights = new int[] { 0, 0 };
                      ((GridBagLayout) sNPanel14.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                      ((GridBagLayout) sNPanel14.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                      
                      // ---- sNLabelChamp11 ----
                      sNLabelChamp11.setText("Heure de d\u00e9but");
                      sNLabelChamp11.setName("sNLabelChamp11");
                      sNPanel14.add(sNLabelChamp11, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                          GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                      
                      // ---- spinner1 ----
                      spinner1.setFont(new Font("sansserif", Font.PLAIN, 14));
                      spinner1.setModel(new SpinnerNumberModel(0, 0, 24, 1));
                      spinner1.setName("spinner1");
                      sNPanel14.add(spinner1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                          GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                    }
                    sNPanel8.add(sNPanel14, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                        GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                  }
                  pnlContenu.add(sNPanel8, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- sNLabelTitre4 ----
                  sNLabelTitre4.setText("Dossier d'exportation : /documents/fmdev/exports");
                  sNLabelTitre4.setFont(new Font("sansserif", Font.PLAIN, 14));
                  sNLabelTitre4.setName("sNLabelTitre4");
                  pnlContenu.add(sNLabelTitre4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
                }
                sNPanel13.add(pnlContenu, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              sNPanel10.add(sNPanel13);
              
              // ======== sNPanel12 ========
              {
                sNPanel12.setName("sNPanel12");
                sNPanel12.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel12.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) sNPanel12.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
                ((GridBagLayout) sNPanel12.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) sNPanel12.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- sNLabelChamp4 ----
                sNLabelChamp4.setText("Etablissement");
                sNLabelChamp4.setName("sNLabelChamp4");
                sNPanel12.add(sNLabelChamp4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNEtablissement2 ----
                sNEtablissement2.setName("sNEtablissement2");
                sNPanel12.add(sNEtablissement2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- sNLabelChamp7 ----
                sNLabelChamp7.setText("Date de cr\u00e9ation");
                sNLabelChamp7.setName("sNLabelChamp7");
                sNPanel12.add(sNLabelChamp7, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNDate1 ----
                sNDate1.setEnabled(false);
                sNDate1.setName("sNDate1");
                sNPanel12.add(sNDate1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- sNLabelChamp8 ----
                sNLabelChamp8.setText("Date de modification");
                sNLabelChamp8.setName("sNLabelChamp8");
                sNPanel12.add(sNLabelChamp8, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNDate2 ----
                sNDate2.setEnabled(false);
                sNDate2.setName("sNDate2");
                sNPanel12.add(sNDate2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- sNLabelChamp9 ----
                sNLabelChamp9.setText("Date de dernier export");
                sNLabelChamp9.setName("sNLabelChamp9");
                sNPanel12.add(sNLabelChamp9, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- sNDate3 ----
                sNDate3.setEnabled(false);
                sNDate3.setName("sNDate3");
                sNPanel12.add(sNDate3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              sNPanel10.add(sNPanel12);
            }
            pnlGeneral.add(sNPanel10, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
          }
          tabbedPane1.addTab("G\u00e9n\u00e9ral", pnlGeneral);
          
          // ======== pnlChampAExporter ========
          {
            pnlChampAExporter.setName("pnlChampAExporter");
            pnlChampAExporter.setLayout(new BorderLayout());
            
            // ======== snSelectionChampMetier ========
            {
              snSelectionChampMetier.setName("snSelectionChampMetier");
              snSelectionChampMetier.setLayout(new GridBagLayout());
              ((GridBagLayout) snSelectionChampMetier.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) snSelectionChampMetier.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) snSelectionChampMetier.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) snSelectionChampMetier.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              
              // ---- sNLabelTitre1 ----
              sNLabelTitre1.setText("Donn\u00e9es non export\u00e9es");
              sNLabelTitre1.setName("sNLabelTitre1");
              snSelectionChampMetier.add(sNLabelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNLabelTitre2 ----
              sNLabelTitre2.setText("Donn\u00e9es export\u00e9es");
              sNLabelTitre2.setName("sNLabelTitre2");
              snSelectionChampMetier.add(sNLabelTitre2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== spGauche ========
              {
                spGauche.setName("spGauche");
                
                // ---- tblGauche ----
                tblGauche.setModel(new DefaultTableModel(new Object[][] { { "Quantit\u00e9 maximum", null }, { "Sp\u00e9cial", null },
                    { "Assujetti \u00e0 la taxe parafiscale", null }, { "Saisie des lots \u00e0 la commande", null },
                    { "Affichage dispo", null }, { "Exclus web", null }, { "Arrondi", null }, { "N\u00b0CO ventes taxables TVA1", null },
                    { "N\u00b0CO ventes taxables TVA2", null }, { "N\u00b0CO ventes taxables TVA3", null },
                    { "N\u00b0CO ventes taxables TVA4", null }, { "N\u00b0CO ventes taxables TVA5", null },
                    { "N\u00b0CO ventes taxables TVA6", null }, { "N\u00b0CO ventes export CEE", null },
                    { "N\u00b0CO ventes export", null }, { "N\u00b0CO ventes assimil\u00e9es export", null }, },
                    new String[] { "Libell\u00e9", "Description" }) {
                  Class<?>[] columnTypes = new Class<?>[] { String.class, String.class };
                  
                  @Override
                  public Class<?> getColumnClass(int columnIndex) {
                    return columnTypes[columnIndex];
                  }
                });
                {
                  TableColumnModel cm = tblGauche.getColumnModel();
                  cm.getColumn(0).setMinWidth(220);
                  cm.getColumn(0).setMaxWidth(220);
                  cm.getColumn(0).setPreferredWidth(220);
                }
                tblGauche.setFont(new Font("sansserif", Font.PLAIN, 14));
                tblGauche.setMinimumSize(new Dimension(160, 560));
                tblGauche.setPreferredSize(new Dimension(360, 560));
                tblGauche.setAutoCreateRowSorter(true);
                tblGauche.setBackground(Color.white);
                tblGauche.setShowHorizontalLines(true);
                tblGauche.setShowVerticalLines(true);
                tblGauche.setSelectionForeground(Color.white);
                tblGauche.setGridColor(new Color(204, 204, 204));
                tblGauche.setRowHeight(20);
                tblGauche.setEnabled(false);
                tblGauche.setName("tblGauche");
                spGauche.setViewportView(tblGauche);
              }
              snSelectionChampMetier.add(spGauche, new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ======== pnlBoutonCentral ========
              {
                pnlBoutonCentral.setName("pnlBoutonCentral");
                pnlBoutonCentral.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlBoutonCentral.getLayout()).columnWidths = new int[] { 0, 0 };
                ((GridBagLayout) pnlBoutonCentral.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlBoutonCentral.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
                ((GridBagLayout) pnlBoutonCentral.getLayout()).rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
                
                // ---- snBoutonSelectionUnitaire ----
                snBoutonSelectionUnitaire.setName("snBoutonSelectionUnitaire");
                pnlBoutonCentral.add(snBoutonSelectionUnitaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snBoutonDeselectionUnitaire ----
                snBoutonDeselectionUnitaire.setName("snBoutonDeselectionUnitaire");
                pnlBoutonCentral.add(snBoutonDeselectionUnitaire, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snBoutonSelectionTotale ----
                snBoutonSelectionTotale.setName("snBoutonSelectionTotale");
                pnlBoutonCentral.add(snBoutonSelectionTotale, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snBoutonDeselectionTotale ----
                snBoutonDeselectionTotale.setName("snBoutonDeselectionTotale");
                pnlBoutonCentral.add(snBoutonDeselectionTotale, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- snBoutonSelectionParDefaut ----
                snBoutonSelectionParDefaut.setName("snBoutonSelectionParDefaut");
                pnlBoutonCentral.add(snBoutonSelectionParDefaut, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              }
              snSelectionChampMetier.add(pnlBoutonCentral, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ======== spDroite ========
              {
                spDroite.setBackground(Color.white);
                spDroite.setName("spDroite");
                
                // ---- tblDroite ----
                tblDroite
                    .setModel(
                        new DefaultTableModel(
                            new Object[][] { { "Etablissement", "Etablissement du groupe d'articles" },
                                { "Code", "Code du groupe d'articles" }, { "Libell\u00e9", "Libell\u00e9 du groupe d'articles" },
                                { "Type de fiche article", null }, { "Code TVA", "Code TVA appliqu\u00e9 aux articles du groupe" },
                                { "Unit\u00e9 de vente", "Unit\u00e9 de vente param\u00e9tr\u00e9e pour les articles du groupe" },
                                { "Unit\u00e9 de stock", null }, { "Gestionnaire produit", null }, { "R\u00e9f\u00e9rence tarif", null },
                                { "Coefficient de vente", "Coefficient de vente des articles du groupe" },
                                { "Coefficient de prix de revient",
                                    "Coefficient de prix de revient appliqu\u00e9 aux articles du groupe" },
                                { "Pourcentage minimum marge", "Pourcentage minimal de marge pour les articles du groupe" }, },
                            new String[] { "Libell\u00e9", "Description" }) {
                          Class<?>[] columnTypes = new Class<?>[] { String.class, String.class };
                          
                          @Override
                          public Class<?> getColumnClass(int columnIndex) {
                            return columnTypes[columnIndex];
                          }
                        });
                {
                  TableColumnModel cm = tblDroite.getColumnModel();
                  cm.getColumn(0).setMinWidth(220);
                  cm.getColumn(0).setMaxWidth(220);
                  cm.getColumn(0).setPreferredWidth(220);
                }
                tblDroite.setFont(new Font("sansserif", Font.PLAIN, 14));
                tblDroite.setMinimumSize(new Dimension(160, 560));
                tblDroite.setPreferredSize(new Dimension(360, 560));
                tblDroite.setAutoCreateRowSorter(true);
                tblDroite.setBackground(Color.white);
                tblDroite.setShowHorizontalLines(true);
                tblDroite.setShowVerticalLines(true);
                tblDroite.setSelectionForeground(Color.white);
                tblDroite.setGridColor(new Color(204, 204, 204));
                tblDroite.setRowHeight(20);
                tblDroite.setEnabled(false);
                tblDroite.setName("tblDroite");
                spDroite.setViewportView(tblDroite);
              }
              snSelectionChampMetier.add(spDroite, new GridBagConstraints(2, 1, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlChampAExporter.add(snSelectionChampMetier, BorderLayout.CENTER);
          }
          tabbedPane1.addTab("Donn\u00e9es \u00e0 exporter", pnlChampAExporter);
          
          // ======== pnlFiltre ========
          {
            pnlFiltre.setName("pnlFiltre");
            pnlFiltre.setLayout(new GridLayout(1, 2));
            
            // ======== sNPanel5 ========
            {
              sNPanel5.setName("sNPanel5");
              sNPanel5.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel5.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) sNPanel5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- sNLabelChamp10 ----
              sNLabelChamp10.setText("Etat");
              sNLabelChamp10.setName("sNLabelChamp10");
              sNPanel5.add(sNLabelChamp10, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNComboBox2 ----
              sNComboBox2.setModel(new DefaultComboBoxModel<>(new String[] { "Tous", "Non annul\u00e9s", "Annul\u00e9s" }));
              sNComboBox2.setName("sNComboBox2");
              sNPanel5.add(sNComboBox2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlFiltre.add(sNPanel5);
            
            // ======== sNPanel3 ========
            {
              sNPanel3.setName("sNPanel3");
              sNPanel3.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel3.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel3.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) sNPanel3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- sNLabelChamp2 ----
              sNLabelChamp2.setText("Etablissement");
              sNLabelChamp2.setName("sNLabelChamp2");
              sNPanel3.add(sNLabelChamp2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNEtablissement1 ----
              sNEtablissement1.setEnabled(false);
              sNEtablissement1.setName("sNEtablissement1");
              sNPanel3.add(sNEtablissement1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlFiltre.add(sNPanel3);
          }
          tabbedPane1.addTab("Crit\u00e8res de filtrage", pnlFiltre);
          
          // ======== pnlPrevisualisation ========
          {
            pnlPrevisualisation.setName("pnlPrevisualisation");
            
            // ---- nRiTable2 ----
            nRiTable2.setModel(new DefaultTableModel(
                new Object[][] { { "ALL", "A", "Gros oeuvre" }, { "ALL", "B", "Environnement plein air" },
                    { "ALL", "C", "Assainissement" }, { "ALL", "D", "Couverture" }, { "ALL", "E", "Isolation et cloison" },
                    { "ALL", "F", "Bois et panneaux" }, { "ALL", "G", "Menuiserie" }, { "ALL", "H", "Carrelage - salle de bain" },
                    { "ALL", "I", "Cuisine" }, { "ALL", "J", "Plomberie" }, { "ALL", "K", "Chauffage et traitement d'air" },
                    { "ALL", "L", "Electricit\u00e9" }, { "ALL", "M", "Peinture et droguerie" }, { "ALL", "N", "Outillage" }, },
                new String[] { "Etablissement", "Code", "Libell\u00e9" }) {
              Class<?>[] columnTypes = new Class<?>[] { String.class, String.class, String.class };
              
              @Override
              public Class<?> getColumnClass(int columnIndex) {
                return columnTypes[columnIndex];
              }
            });
            {
              TableColumnModel cm = nRiTable2.getColumnModel();
              cm.getColumn(0).setMinWidth(100);
              cm.getColumn(0).setMaxWidth(100);
              cm.getColumn(0).setPreferredWidth(100);
              cm.getColumn(1).setMaxWidth(60);
              cm.getColumn(1).setPreferredWidth(60);
            }
            nRiTable2.setFont(new Font("sansserif", Font.PLAIN, 14));
            nRiTable2.setMinimumSize(new Dimension(160, 560));
            nRiTable2.setPreferredSize(new Dimension(360, 560));
            nRiTable2.setAutoCreateRowSorter(true);
            nRiTable2.setBackground(Color.white);
            nRiTable2.setShowHorizontalLines(true);
            nRiTable2.setShowVerticalLines(true);
            nRiTable2.setSelectionForeground(Color.white);
            nRiTable2.setGridColor(new Color(204, 204, 204));
            nRiTable2.setRowHeight(20);
            nRiTable2.setEnabled(false);
            nRiTable2.setName("nRiTable2");
            pnlPrevisualisation.setViewportView(nRiTable2);
          }
          tabbedPane1.addTab("Pr\u00e9visualisation de l'export", pnlPrevisualisation);
        }
        sNPanelContenu1.add(tabbedPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelFond1.add(sNPanelContenu1, BorderLayout.CENTER);
    }
    add(sNPanelFond1);
    
    // ======== sNPanel6 ========
    {
      sNPanel6.setVisible(false);
      sNPanel6.setName("sNPanel6");
      sNPanel6.setLayout(new GridBagLayout());
      ((GridBagLayout) sNPanel6.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) sNPanel6.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) sNPanel6.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) sNPanel6.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond sNPanelFond1;
  private SNBarreBouton sNBarreBouton1;
  private SNBandeauTitre sNBandeauTitre1;
  private SNPanelContenu sNPanelContenu1;
  private JTabbedPane tabbedPane1;
  private SNPanel pnlGeneral;
  private SNPanel sNPanel10;
  private SNPanel sNPanel13;
  private SNPanel sNPanel11;
  private SNLabelChamp sNLabelChamp1;
  private SNComboBox sNComboBox1;
  private SNLabelChamp sNLabelChamp5;
  private SNUtilisateur sNUtilisateur1;
  private SNLabelChamp sNLabelChamp6;
  private JScrollPane scrollPane3;
  private JTextArea textArea1;
  private SNPanelTitre pnlContenu;
  private SNPanel sNPanel8;
  private XRiCheckBox xRiCheckBox1;
  private XRiCheckBox xRiCheckBox5;
  private XRiCheckBox xRiCheckBox2;
  private XRiCheckBox xRiCheckBox6;
  private XRiCheckBox xRiCheckBox3;
  private XRiCheckBox xRiCheckBox7;
  private XRiCheckBox xRiCheckBox4;
  private SNPanel sNPanel14;
  private SNLabelChamp sNLabelChamp11;
  private JSpinner spinner1;
  private SNMessage sNLabelTitre4;
  private SNPanel sNPanel12;
  private SNLabelChamp sNLabelChamp4;
  private SNEtablissement sNEtablissement2;
  private SNLabelChamp sNLabelChamp7;
  private SNDate sNDate1;
  private SNLabelChamp sNLabelChamp8;
  private SNDate sNDate2;
  private SNLabelChamp sNLabelChamp9;
  private SNDate sNDate3;
  private SNPanelContenu pnlChampAExporter;
  private SNPanel snSelectionChampMetier;
  private SNLabelTitre sNLabelTitre1;
  private SNLabelTitre sNLabelTitre2;
  private JScrollPane spGauche;
  private NRiTable tblGauche;
  private SNPanel pnlBoutonCentral;
  private SNBoutonSelectionUnitaire snBoutonSelectionUnitaire;
  private SNBoutonDeselectionUnitaire snBoutonDeselectionUnitaire;
  private SNBoutonSelectionTotale snBoutonSelectionTotale;
  private SNBoutonDeselectionTotale snBoutonDeselectionTotale;
  private SNBoutonSelectionParDefaut snBoutonSelectionParDefaut;
  private JScrollPane spDroite;
  private NRiTable tblDroite;
  private SNPanel pnlFiltre;
  private SNPanel sNPanel5;
  private SNLabelChamp sNLabelChamp10;
  private SNComboBox sNComboBox2;
  private SNPanel sNPanel3;
  private SNLabelChamp sNLabelChamp2;
  private SNEtablissement sNEtablissement1;
  private JScrollPane pnlPrevisualisation;
  private NRiTable nRiTable2;
  private SNPanel sNPanel6;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
