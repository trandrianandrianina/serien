/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9858;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.EnumCategorieDate;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SGPM80FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  private ArrayList<EnumCategorieDate> listeEnumCategorieDate = new ArrayList<EnumCategorieDate>();
  
  public SGPM80FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    snFamilleDebut.lierComposantFin(snFamilleFin);
    snSousFamilleDebut.lierComposantFin(snSousFamilleFin);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Chargement de la liste des valeurs de l'EnumDatePreEnregister
    for (int i = 0; i < EnumCategorieDate.values().length; i++) {
      listeEnumCategorieDate.add(EnumCategorieDate.values()[i]);
    }
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCorus.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation des composants
    chargerComposantMagasin();
    chargerArticle();
    
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin1.renseignerChampRPG(lexique, "WMAG");
    
    // Famille
    snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
    snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
    // Sous Famille
    snSousFamilleDebut.renseignerChampRPG(lexique, "SFADEB");
    snSousFamilleFin.renseignerChampRPG(lexique, "SFAFIN");
    
    // Article
    snArticleDebut.renseignerChampRPG(lexique, "ARTDEB");
    snArticleFin.renseignerChampRPG(lexique, "ARTFIN");
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge le composant magasin
   */
  private void chargerComposantMagasin() {
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setTousAutorise(true);
    snMagasin1.charger(true);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
  }
  
  /**
   * Charge les famille
   */
  private void chargerFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  /**
   * Charge les sous famille
   */
  private void chargerSousFamille() {
    snSousFamilleDebut.setSession(getSession());
    snSousFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleDebut.setTousAutorise(true);
    snSousFamilleDebut.charger(false);
    snSousFamilleDebut.setSelectionParChampRPG(lexique, "SFADEB");
    
    snSousFamilleFin.setSession(getSession());
    snSousFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleFin.setTousAutorise(true);
    snSousFamilleFin.charger(false);
    snSousFamilleFin.setSelectionParChampRPG(lexique, "SFAFIN");
  }
  
  /**
   * Charge les articles
   */
  private void chargerArticle() {
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "ARTDEB");
    
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "ARTFIN");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      
      chargerComposantMagasin();
      chargerFamille();
      chargerSousFamille();
      chargerArticle();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelectionArticle = new SNPanelTitre();
    lbFamilleDebut = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFamilleFin = new SNLabelChamp();
    pnlSelectionDeBon = new SNPanel();
    snFamilleFin = new SNFamille();
    lbSousFamilleDebut = new SNLabelChamp();
    snSousFamilleDebut = new SNSousFamille();
    lbSousFamilleFin = new SNLabelChamp();
    snSousFamilleFin = new SNSousFamille();
    lbArticleDebut = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticleFin = new SNLabelChamp();
    snArticleFin = new SNArticle();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCorus = new SNTexte();
    pnlOptionTraitement = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbDate = new SNLabelChamp();
    DATTRT = new XRiCalendrier();
    snBarreBouton = new SNBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(960, 640));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("LOCTP");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelectionArticle ========
          {
            pnlCritereSelectionArticle.setTitre("Crit\u00e8res de s\u00e9lection articles compos\u00e9s");
            pnlCritereSelectionArticle.setName("pnlCritereSelectionArticle");
            pnlCritereSelectionArticle.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbFamilleDebut ----
            lbFamilleDebut.setText("Famille de d\u00e9but");
            lbFamilleDebut.setMaximumSize(new Dimension(175, 30));
            lbFamilleDebut.setMinimumSize(new Dimension(175, 30));
            lbFamilleDebut.setPreferredSize(new Dimension(175, 30));
            lbFamilleDebut.setName("lbFamilleDebut");
            pnlCritereSelectionArticle.add(lbFamilleDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleDebut ----
            snFamilleDebut.setName("snFamilleDebut");
            pnlCritereSelectionArticle.add(snFamilleDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilleFin ----
            lbFamilleFin.setText("Famille de fin");
            lbFamilleFin.setMaximumSize(new Dimension(175, 30));
            lbFamilleFin.setMinimumSize(new Dimension(175, 30));
            lbFamilleFin.setPreferredSize(new Dimension(175, 30));
            lbFamilleFin.setName("lbFamilleFin");
            pnlCritereSelectionArticle.add(lbFamilleFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlSelectionDeBon ========
            {
              pnlSelectionDeBon.setName("pnlSelectionDeBon");
              pnlSelectionDeBon.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- snFamilleFin ----
              snFamilleFin.setName("snFamilleFin");
              pnlSelectionDeBon.add(snFamilleFin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelectionArticle.add(pnlSelectionDeBon, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousFamilleDebut ----
            lbSousFamilleDebut.setText("Sous-famille de d\u00e9but");
            lbSousFamilleDebut.setMaximumSize(new Dimension(175, 30));
            lbSousFamilleDebut.setMinimumSize(new Dimension(175, 30));
            lbSousFamilleDebut.setPreferredSize(new Dimension(175, 30));
            lbSousFamilleDebut.setName("lbSousFamilleDebut");
            pnlCritereSelectionArticle.add(lbSousFamilleDebut, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSousFamilleDebut ----
            snSousFamilleDebut.setName("snSousFamilleDebut");
            pnlCritereSelectionArticle.add(snSousFamilleDebut, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousFamilleFin ----
            lbSousFamilleFin.setText("Sous-famille de fin");
            lbSousFamilleFin.setMaximumSize(new Dimension(175, 30));
            lbSousFamilleFin.setMinimumSize(new Dimension(175, 30));
            lbSousFamilleFin.setPreferredSize(new Dimension(175, 30));
            lbSousFamilleFin.setName("lbSousFamilleFin");
            pnlCritereSelectionArticle.add(lbSousFamilleFin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSousFamilleFin ----
            snSousFamilleFin.setName("snSousFamilleFin");
            pnlCritereSelectionArticle.add(snSousFamilleFin, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticleDebut ----
            lbArticleDebut.setText("Article de d\u00e9but");
            lbArticleDebut.setMinimumSize(new Dimension(175, 30));
            lbArticleDebut.setMaximumSize(new Dimension(175, 30));
            lbArticleDebut.setPreferredSize(new Dimension(175, 30));
            lbArticleDebut.setName("lbArticleDebut");
            pnlCritereSelectionArticle.add(lbArticleDebut, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticleDebut ----
            snArticleDebut.setName("snArticleDebut");
            pnlCritereSelectionArticle.add(snArticleDebut, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticleFin ----
            lbArticleFin.setText("Article de fin");
            lbArticleFin.setMinimumSize(new Dimension(175, 30));
            lbArticleFin.setMaximumSize(new Dimension(175, 30));
            lbArticleFin.setPreferredSize(new Dimension(175, 30));
            lbArticleFin.setName("lbArticleFin");
            pnlCritereSelectionArticle.add(lbArticleFin, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snArticleFin ----
            snArticleFin.setName("snArticleFin");
            pnlCritereSelectionArticle.add(snArticleFin, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelectionArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setEnabled(false);
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCorus ----
            tfPeriodeEnCorus.setText("@WENCX@");
            tfPeriodeEnCorus.setEditable(false);
            tfPeriodeEnCorus.setEnabled(false);
            tfPeriodeEnCorus.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCorus.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCorus.setName("tfPeriodeEnCorus");
            pnlEtablissement.add(tfPeriodeEnCorus, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionTraitement ========
          {
            pnlOptionTraitement.setTitre("Options de traitement ");
            pnlOptionTraitement.setName("pnlOptionTraitement");
            pnlOptionTraitement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionTraitement.getLayout()).columnWidths = new int[] { 55, 0, 0 };
            ((GridBagLayout) pnlOptionTraitement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionTraitement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionTraitement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin ");
            lbMagasin.setPreferredSize(new Dimension(175, 30));
            lbMagasin.setMinimumSize(new Dimension(175, 30));
            lbMagasin.setMaximumSize(new Dimension(175, 30));
            lbMagasin.setName("lbMagasin");
            pnlOptionTraitement.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin1 ----
            snMagasin1.setName("snMagasin1");
            pnlOptionTraitement.add(snMagasin1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDate ----
            lbDate.setText("Date contr\u00f4le stock");
            lbDate.setName("lbDate");
            pnlOptionTraitement.add(lbDate, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- DATTRT ----
            DATTRT.setComponentPopupMenu(null);
            DATTRT.setPreferredSize(new Dimension(115, 30));
            DATTRT.setMinimumSize(new Dimension(115, 30));
            DATTRT.setMaximumSize(new Dimension(105, 30));
            DATTRT.setFont(new Font("sansserif", Font.PLAIN, 14));
            DATTRT.setName("DATTRT");
            pnlOptionTraitement.add(DATTRT, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionTraitement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pmBTD ========
    {
      pmBTD.setName("pmBTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelectionArticle;
  private SNLabelChamp lbFamilleDebut;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFamilleFin;
  private SNPanel pnlSelectionDeBon;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbSousFamilleDebut;
  private SNSousFamille snSousFamilleDebut;
  private SNLabelChamp lbSousFamilleFin;
  private SNSousFamille snSousFamilleFin;
  private SNLabelChamp lbArticleDebut;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbArticleFin;
  private SNArticle snArticleFin;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCorus;
  private SNPanelTitre pnlOptionTraitement;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbDate;
  private XRiCalendrier DATTRT;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
