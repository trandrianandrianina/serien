/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9170;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.commun.snunite.SNUnite;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNNombreDecimal;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;

/**
 * 
 */
public class DialogueDetailStock extends JDialog {
  
  public DialogueDetailStock() {
    super();
    initComponents();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    snPanelEntete = new SNPanel();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    tabbedPane1 = new JTabbedPane();
    pnlPrincipal = new SNPanelContenu();
    pnlPrincipalGauche = new SNPanel();
    pnlQuantite = new SNPanelTitre();
    sNLabelChamp3 = new SNLabelChamp();
    sNNombreDecimal1 = new SNNombreDecimal();
    sNLabelChamp16 = new SNLabelChamp();
    sNTexte4 = new SNTexte();
    sNLabelChamp5 = new SNLabelChamp();
    sNNombreDecimal4 = new SNNombreDecimal();
    sNLabelChamp7 = new SNLabelChamp();
    sNNombreDecimal6 = new SNNombreDecimal();
    sNLabelChamp9 = new SNLabelChamp();
    sNNombreDecimal8 = new SNNombreDecimal();
    sNLabelChamp4 = new SNLabelChamp();
    sNNombreDecimal2 = new SNNombreDecimal();
    sNLabelChamp6 = new SNLabelChamp();
    sNNombreDecimal5 = new SNNombreDecimal();
    sNLabelChamp8 = new SNLabelChamp();
    sNNombreDecimal7 = new SNNombreDecimal();
    sNLabelChamp10 = new SNLabelChamp();
    sNNombreDecimal9 = new SNNombreDecimal();
    pnlMiniEstime = new SNPanelTitre();
    sNLabelChamp13 = new SNLabelChamp();
    sNTexte1 = new SNTexte();
    sNLabelChamp14 = new SNLabelChamp();
    sNTexte2 = new SNTexte();
    sNLabelChamp15 = new SNLabelChamp();
    sNTexte3 = new SNTexte();
    pnlPrincipalDroite = new SNPanel();
    pnlUnite = new SNPanelTitre();
    sNLabelChamp11 = new SNLabelChamp();
    sNUnite1 = new SNUnite();
    pnlAttenduCommande = new SNPanelContenu();
    spAttenduCommande = new JScrollPane();
    tbAttenduCommande = new JTable();
    pnlMouvementStock = new SNPanelContenu();
    scrollPane1 = new JScrollPane();
    table1 = new JTable();
    pnlStockParMagasin = new SNPanelContenu();
    spListeMagasin = new JScrollPane();
    tbListeMagasin = new JTable();
    pnbBarrreBouton = new SNPanelContenu();
    btnFermer = new SNBouton();
    
    // ======== this ========
    setTitle("D\u00e9tail du stock article");
    setMinimumSize(new Dimension(400, 365));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ======== snPanelEntete ========
        {
          snPanelEntete.setName("snPanelEntete");
          snPanelEntete.setLayout(new GridBagLayout());
          ((GridBagLayout) snPanelEntete.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) snPanelEntete.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) snPanelEntete.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) snPanelEntete.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbArticle ----
          lbArticle.setText("Article");
          lbArticle.setName("lbArticle");
          snPanelEntete.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticle ----
          snArticle.setEnabled(false);
          snArticle.setName("snArticle");
          snPanelEntete.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          snPanelEntete.add(lbEtablissement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setEnabled(false);
          snEtablissement.setName("snEtablissement");
          snPanelEntete.add(snEtablissement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          snPanelEntete.add(lbMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setEnabled(false);
          snMagasin.setName("snMagasin");
          snPanelEntete.add(snMagasin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(snPanelEntete, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== tabbedPane1 ========
        {
          tabbedPane1.setFont(new Font("sansserif", Font.PLAIN, 14));
          tabbedPane1.setName("tabbedPane1");
          
          // ======== pnlPrincipal ========
          {
            pnlPrincipal.setName("pnlPrincipal");
            pnlPrincipal.setLayout(new GridLayout(1, 2));
            
            // ======== pnlPrincipalGauche ========
            {
              pnlPrincipalGauche.setName("pnlPrincipalGauche");
              pnlPrincipalGauche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ======== pnlQuantite ========
              {
                pnlQuantite.setTitre("Stock");
                pnlQuantite.setName("pnlQuantite");
                pnlQuantite.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlQuantite.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlQuantite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlQuantite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlQuantite.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0, 1.0, 1.0, 1.0E-4 };
                
                // ---- sNLabelChamp3 ----
                sNLabelChamp3.setText("Physique");
                sNLabelChamp3.setName("sNLabelChamp3");
                pnlQuantite.add(sNLabelChamp3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNNombreDecimal1 ----
                sNNombreDecimal1.setEditable(false);
                sNNombreDecimal1.setEnabled(false);
                sNNombreDecimal1.setName("sNNombreDecimal1");
                pnlQuantite.add(sNNombreDecimal1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNLabelChamp16 ----
                sNLabelChamp16.setText("Date");
                sNLabelChamp16.setName("sNLabelChamp16");
                pnlQuantite.add(sNLabelChamp16, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNTexte4 ----
                sNTexte4.setEnabled(false);
                sNTexte4.setName("sNTexte4");
                pnlQuantite.add(sNTexte4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- sNLabelChamp5 ----
                sNLabelChamp5.setText("Command\u00e9");
                sNLabelChamp5.setName("sNLabelChamp5");
                pnlQuantite.add(sNLabelChamp5, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNNombreDecimal4 ----
                sNNombreDecimal4.setEditable(false);
                sNNombreDecimal4.setEnabled(false);
                sNNombreDecimal4.setName("sNNombreDecimal4");
                pnlQuantite.add(sNNombreDecimal4, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNLabelChamp7 ----
                sNLabelChamp7.setText("Dont r\u00e9serv\u00e9");
                sNLabelChamp7.setName("sNLabelChamp7");
                pnlQuantite.add(sNLabelChamp7, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNNombreDecimal6 ----
                sNNombreDecimal6.setEnabled(false);
                sNNombreDecimal6.setName("sNNombreDecimal6");
                pnlQuantite.add(sNNombreDecimal6, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- sNLabelChamp9 ----
                sNLabelChamp9.setText("Net");
                sNLabelChamp9.setImportanceMessage(EnumImportanceMessage.MOYEN);
                sNLabelChamp9.setName("sNLabelChamp9");
                pnlQuantite.add(sNLabelChamp9, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNNombreDecimal8 ----
                sNNombreDecimal8.setEditable(false);
                sNNombreDecimal8.setEnabled(false);
                sNNombreDecimal8.setName("sNNombreDecimal8");
                pnlQuantite.add(sNNombreDecimal8, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNLabelChamp4 ----
                sNLabelChamp4.setText("Attendu");
                sNLabelChamp4.setName("sNLabelChamp4");
                pnlQuantite.add(sNLabelChamp4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNNombreDecimal2 ----
                sNNombreDecimal2.setEnabled(false);
                sNNombreDecimal2.setName("sNNombreDecimal2");
                pnlQuantite.add(sNNombreDecimal2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNLabelChamp6 ----
                sNLabelChamp6.setText("Command\u00e9 li\u00e9 achats");
                sNLabelChamp6.setName("sNLabelChamp6");
                pnlQuantite.add(sNLabelChamp6, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNNombreDecimal5 ----
                sNNombreDecimal5.setEnabled(false);
                sNNombreDecimal5.setName("sNNombreDecimal5");
                pnlQuantite.add(sNNombreDecimal5, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- sNLabelChamp8 ----
                sNLabelChamp8.setText("Th\u00e9orique");
                sNLabelChamp8.setImportanceMessage(EnumImportanceMessage.MOYEN);
                sNLabelChamp8.setName("sNLabelChamp8");
                pnlQuantite.add(sNLabelChamp8, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- sNNombreDecimal7 ----
                sNNombreDecimal7.setEnabled(false);
                sNNombreDecimal7.setName("sNNombreDecimal7");
                pnlQuantite.add(sNNombreDecimal7, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- sNLabelChamp10 ----
                sNLabelChamp10.setText("Disponible");
                sNLabelChamp10.setImportanceMessage(EnumImportanceMessage.MOYEN);
                sNLabelChamp10.setName("sNLabelChamp10");
                pnlQuantite.add(sNLabelChamp10, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- sNNombreDecimal9 ----
                sNNombreDecimal9.setEnabled(false);
                sNNombreDecimal9.setName("sNNombreDecimal9");
                pnlQuantite.add(sNNombreDecimal9, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPrincipalGauche.add(pnlQuantite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== pnlMiniEstime ========
              {
                pnlMiniEstime.setTitre("Stock avant rupture");
                pnlMiniEstime.setName("pnlMiniEstime");
                pnlMiniEstime.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlMiniEstime.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlMiniEstime.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlMiniEstime.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlMiniEstime.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- sNLabelChamp13 ----
                sNLabelChamp13.setText("Avant rupture");
                sNLabelChamp13.setName("sNLabelChamp13");
                pnlMiniEstime.add(sNLabelChamp13, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNTexte1 ----
                sNTexte1.setEnabled(false);
                sNTexte1.setName("sNTexte1");
                pnlMiniEstime.add(sNTexte1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- sNLabelChamp14 ----
                sNLabelChamp14.setText("Date rupture possible");
                sNLabelChamp14.setName("sNLabelChamp14");
                pnlMiniEstime.add(sNLabelChamp14, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- sNTexte2 ----
                sNTexte2.setEnabled(false);
                sNTexte2.setName("sNTexte2");
                pnlMiniEstime.add(sNTexte2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- sNLabelChamp15 ----
                sNLabelChamp15.setText("Date fin de rupture");
                sNLabelChamp15.setName("sNLabelChamp15");
                pnlMiniEstime.add(sNLabelChamp15, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- sNTexte3 ----
                sNTexte3.setEnabled(false);
                sNTexte3.setName("sNTexte3");
                pnlMiniEstime.add(sNTexte3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPrincipalGauche.add(pnlMiniEstime, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlPrincipal.add(pnlPrincipalGauche);
            
            // ======== pnlPrincipalDroite ========
            {
              pnlPrincipalDroite.setName("pnlPrincipalDroite");
              pnlPrincipalDroite.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ======== pnlUnite ========
              {
                pnlUnite.setTitre("Unit\u00e9s");
                pnlUnite.setName("pnlUnite");
                pnlUnite.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlUnite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlUnite.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlUnite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlUnite.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
                
                // ---- sNLabelChamp11 ----
                sNLabelChamp11.setText("Unit\u00e9 de stock");
                sNLabelChamp11.setName("sNLabelChamp11");
                pnlUnite.add(sNLabelChamp11, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- sNUnite1 ----
                sNUnite1.setEnabled(false);
                sNUnite1.setName("sNUnite1");
                pnlUnite.add(sNUnite1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPrincipalDroite.add(pnlUnite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlPrincipal.add(pnlPrincipalDroite);
          }
          tabbedPane1.addTab("Principal", pnlPrincipal);
          
          // ======== pnlAttenduCommande ========
          {
            pnlAttenduCommande.setName("pnlAttenduCommande");
            pnlAttenduCommande.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlAttenduCommande.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlAttenduCommande.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlAttenduCommande.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlAttenduCommande.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== spAttenduCommande ========
            {
              spAttenduCommande.setName("spAttenduCommande");
              
              // ---- tbAttenduCommande ----
              tbAttenduCommande.setModel(new DefaultTableModel(
                  new Object[][] {
                      { "17/11/2021", "580812", "1030", "26/07/2021", "THE LIBERIO TENNIS ACCE (00327)", null, -1, 919, null },
                      { "25/11/2021", "022581", "140", "19/10/2021", "COMPOSITES SA (401026)", 24, null, 944, 834 }, },
                  new String[] { "<html>Date livraison<br>pr\u00e9vue</html>", "Document", "Ligne", "Date", "Tiers", "Entr\u00e9e",
                      "Sortie", "Th\u00e9orique", "Avant rupture" }) {
                Class<?>[] columnTypes = new Class<?>[] { String.class, Object.class, Object.class, String.class, Object.class,
                    Integer.class, Integer.class, Integer.class, Integer.class };
                
                @Override
                public Class<?> getColumnClass(int columnIndex) {
                  return columnTypes[columnIndex];
                }
              });
              {
                TableColumnModel cm = tbAttenduCommande.getColumnModel();
                cm.getColumn(4).setMinWidth(200);
                cm.getColumn(4).setPreferredWidth(200);
              }
              tbAttenduCommande.setName("tbAttenduCommande");
              spAttenduCommande.setViewportView(tbAttenduCommande);
            }
            pnlAttenduCommande.add(spAttenduCommande, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          tabbedPane1.addTab("Attendus et command\u00e9s", pnlAttenduCommande);
          
          // ======== pnlMouvementStock ========
          {
            pnlMouvementStock.setName("pnlMouvementStock");
            pnlMouvementStock.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlMouvementStock.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlMouvementStock.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlMouvementStock.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlMouvementStock.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== scrollPane1 ========
            {
              scrollPane1.setPreferredSize(new Dimension(456, 150));
              scrollPane1.setName("scrollPane1");
              
              // ---- table1 ----
              table1.setModel(new DefaultTableModel(
                  new Object[][] {
                      { "08/11/2021", 1002, "590730/1", "120", "LACOSTE JAPAN CO LT (230)", "Sortie", "Vente", null, -24, 876 },
                      { "10/11/2021", 1, "022581/1", "20", "COUSIN COMPOSITES (80)", "Entr\u00e9e", "Achat", 96, null, 972 }, },
                  new String[] { "Date", "Ordre", "Document", "Ligne", "Libell\u00e9", "Type", "Origine", "Entr\u00e9e", "Sortie",
                      "Physique" }) {
                Class<?>[] columnTypes = new Class<?>[] { Object.class, Integer.class, Object.class, Object.class, Object.class,
                    Object.class, Object.class, Integer.class, Integer.class, Integer.class };
                
                @Override
                public Class<?> getColumnClass(int columnIndex) {
                  return columnTypes[columnIndex];
                }
              });
              {
                TableColumnModel cm = table1.getColumnModel();
                cm.getColumn(1).setMinWidth(50);
                cm.getColumn(1).setMaxWidth(50);
                cm.getColumn(1).setPreferredWidth(50);
                cm.getColumn(4).setMinWidth(400);
                cm.getColumn(4).setMaxWidth(400);
                cm.getColumn(4).setPreferredWidth(400);
              }
              table1.setName("table1");
              scrollPane1.setViewportView(table1);
            }
            pnlMouvementStock.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          tabbedPane1.addTab("Mouvements de stock", pnlMouvementStock);
          
          // ======== pnlStockParMagasin ========
          {
            pnlStockParMagasin.setName("pnlStockParMagasin");
            pnlStockParMagasin.setLayout(new BorderLayout());
            
            // ======== spListeMagasin ========
            {
              spListeMagasin.setName("spListeMagasin");
              
              // ---- tbListeMagasin ----
              tbListeMagasin.setModel(new DefaultTableModel(
                  new Object[][] { { "LETNA (LE)", 920, 144, 328, 5, 736, 602, 592 }, { "STOCK 01 (01)", 0, 0, 0, 0, 0, 0, 0 },
                      { "ALAIN SAYD (09)", 5, 0, 0, 0, 5, 5, 5 }, { "Total", 925, 144, 328, 5, 741, 607, 597 }, },
                  new String[] { "Magasin", "Physique", "Attendu", "Command\u00e9", "R\u00e9serv\u00e9", "Th\u00e9orique", "Disponible",
                      "Net" }) {
                Class<?>[] columnTypes = new Class<?>[] { String.class, Integer.class, Integer.class, Integer.class, Integer.class,
                    Integer.class, Integer.class, Integer.class };
                
                @Override
                public Class<?> getColumnClass(int columnIndex) {
                  return columnTypes[columnIndex];
                }
              });
              {
                TableColumnModel cm = tbListeMagasin.getColumnModel();
                cm.getColumn(0).setMinWidth(300);
                cm.getColumn(0).setPreferredWidth(300);
              }
              tbListeMagasin.setName("tbListeMagasin");
              spListeMagasin.setViewportView(tbListeMagasin);
            }
            pnlStockParMagasin.add(spListeMagasin, BorderLayout.CENTER);
          }
          tabbedPane1.addTab("Stock par magasin", pnlStockParMagasin);
        }
        pnlContenu.add(tabbedPane1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.NORTH);
      
      // ======== pnbBarrreBouton ========
      {
        pnbBarrreBouton.setName("pnbBarrreBouton");
        pnbBarrreBouton.setLayout(new GridBagLayout());
        ((GridBagLayout) pnbBarrreBouton.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnbBarrreBouton.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnbBarrreBouton.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnbBarrreBouton.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- btnFermer ----
        btnFermer.setText("text");
        btnFermer.setBoutonPreconfigure(EnumBouton.FERMER);
        btnFermer.setName("btnFermer");
        pnbBarrreBouton.add(btnFermer, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnbBarrreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNPanelContenu pnlContenu;
  private SNPanel snPanelEntete;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private JTabbedPane tabbedPane1;
  private SNPanelContenu pnlPrincipal;
  private SNPanel pnlPrincipalGauche;
  private SNPanelTitre pnlQuantite;
  private SNLabelChamp sNLabelChamp3;
  private SNNombreDecimal sNNombreDecimal1;
  private SNLabelChamp sNLabelChamp16;
  private SNTexte sNTexte4;
  private SNLabelChamp sNLabelChamp5;
  private SNNombreDecimal sNNombreDecimal4;
  private SNLabelChamp sNLabelChamp7;
  private SNNombreDecimal sNNombreDecimal6;
  private SNLabelChamp sNLabelChamp9;
  private SNNombreDecimal sNNombreDecimal8;
  private SNLabelChamp sNLabelChamp4;
  private SNNombreDecimal sNNombreDecimal2;
  private SNLabelChamp sNLabelChamp6;
  private SNNombreDecimal sNNombreDecimal5;
  private SNLabelChamp sNLabelChamp8;
  private SNNombreDecimal sNNombreDecimal7;
  private SNLabelChamp sNLabelChamp10;
  private SNNombreDecimal sNNombreDecimal9;
  private SNPanelTitre pnlMiniEstime;
  private SNLabelChamp sNLabelChamp13;
  private SNTexte sNTexte1;
  private SNLabelChamp sNLabelChamp14;
  private SNTexte sNTexte2;
  private SNLabelChamp sNLabelChamp15;
  private SNTexte sNTexte3;
  private SNPanel pnlPrincipalDroite;
  private SNPanelTitre pnlUnite;
  private SNLabelChamp sNLabelChamp11;
  private SNUnite sNUnite1;
  private SNPanelContenu pnlAttenduCommande;
  private JScrollPane spAttenduCommande;
  private JTable tbAttenduCommande;
  private SNPanelContenu pnlMouvementStock;
  private JScrollPane scrollPane1;
  private JTable table1;
  private SNPanelContenu pnlStockParMagasin;
  private JScrollPane spListeMagasin;
  private JTable tbListeMagasin;
  private SNPanelContenu pnbBarrreBouton;
  private SNBouton btnFermer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
