/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9741;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snadressefournisseur.SNAdresseFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

public class VueOngletEdition extends JPanel {
  /**
   * Constructeur.
   */
  public VueOngletEdition() {
  }
  
  // -- Méthodes évènementielles --------------------------------------------
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
  }
  
  private void ckBonDePreparationActionPerformed(ActionEvent e) {
  }
  
  private void rbEditionSansPiedItemStateChanged(ItemEvent e) {
  }
  
  private void rbEditionChiffreeItemStateChanged(ItemEvent e) {
  }
  
  private void rbEditionNonChiffreItemStateChanged(ItemEvent e) {
  }
  
  private void rbEditionProformaItemStateChanged(ItemEvent e) {
  }
  
  private void ckGenerationCommandeAchatActionPerformed(ActionEvent e) {
  }
  
  private void ckImprimerActionPerformed(ActionEvent e) {
  }
  
  private void ckEnvoyerParMailActionPerformed(ActionEvent e) {
  }
  
  private void ckVisualiserActionPerformed(ActionEvent e) {
  }
  
  private void ckEnvoyerParFaxActionPerformed(ActionEvent e) {
  }
  
  private void tfEmailDestinataireFocusLost(FocusEvent e) {
  }
  
  private void rbMultiFournisseurActionPerformed(ActionEvent e) {
  }
  
  private void rbMonoFournisseurActionPerformed(ActionEvent e) {
  }
  
  private void snContactMailValueChanged(SNComposantEvent e) {
  }
  
  private void snContactFaxValueChanged(SNComposantEvent e) {
  }
  
  private void tfFaxDestinataireFocusLost(FocusEvent e) {
  }
  
  private void rbFactureProformaItemStateChanged(ItemEvent e) {
  }
  
  private void rbDevisSansTotauxItemStateChanged(ItemEvent e) {
  }
  
  private void rbDevisAvecTotauxItemStateChanged(ItemEvent e) {
  }
  
  private void calDateFacturationMinimaleActionPerformed(ActionEvent e) {
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlSuperieur = new SNPanel();
    pnlGauche = new SNPanel();
    pnlChiffrage = new SNPanelTitre();
    rbEditionAvecTotaux = new JRadioButton();
    rbEditionSansTotaux = new JRadioButton();
    rbEditionNonChiffre = new JRadioButton();
    pnlChoixEdition = new SNPanelTitre();
    ckVisualiser = new JCheckBox();
    ckImprimer = new JCheckBox();
    pnlEnvoyerParMail = new SNPanel();
    ckEnvoyerParMail = new JCheckBox();
    pnlMail = new JPanel();
    lbContactMail = new SNLabelChamp();
    snContactMail = new SNContact();
    lbMail = new SNLabelChamp();
    tfEmailDestinataire = new SNTexte();
    pnlEnvoyerParFax = new SNPanel();
    ckEnvoyerParFax = new JCheckBox();
    pnlFax = new JPanel();
    lbContactFax = new SNLabelChamp();
    snContactFax = new SNContact();
    lbFax = new SNLabelChamp();
    tfFaxDestinataire = new SNTexte();
    pnlDroite = new SNPanel();
    pnlEditionDocumentSupplementaire = new SNPanelTitre();
    ckBonDePreparation = new XRiCheckBox();
    pnlOptionsComplementairesAchat = new SNPanelTitre();
    rbMultiFournisseur = new JRadioButton();
    rbMonoFournisseur = new JRadioButton();
    pnlAdresseFournisseur = new JPanel();
    lbAdresseFournisseur = new SNLabelChamp();
    snAdresseFournisseur = new SNAdresseFournisseur();
    ckGenerationCommandeAchat = new JCheckBox();
    sNLabelTitre1 = new SNLabelTitre();
    scpListeDocument = new JScrollPane();
    tblListeDocument = new NRiTable();
    pnlOptionFacturation = new SNPanelTitre();
    lbDateFacturationMinimale = new SNLabelChamp();
    calDateFacturationMinimale = new XRiCalendrier();
    pnlEditionDevis = new SNPanelTitre();
    rbDevisAvecTotaux = new JRadioButton();
    rbDevisSansTotaux = new JRadioButton();
    rbFactureProforma = new JRadioButton();
    pnlTypeEdition = new SNPanelTitre();
    rbEditionFacture = new JRadioButton();
    rbEditionTicketCaisse = new JRadioButton();
    bgTypeEditionDevis = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1240, 750));
    setPreferredSize(new Dimension(1240, 750));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlSuperieur ========
      {
        pnlSuperieur.setName("pnlSuperieur");
        pnlSuperieur.setLayout(new GridLayout(1, 2, 5, 5));
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlChiffrage ========
          {
            pnlChiffrage.setTitre("Chiffrage");
            pnlChiffrage.setName("pnlChiffrage");
            pnlChiffrage.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlChiffrage.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlChiffrage.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlChiffrage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlChiffrage.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- rbEditionAvecTotaux ----
            rbEditionAvecTotaux.setText("Edition chiffr\u00e9e avec totaux");
            rbEditionAvecTotaux.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbEditionAvecTotaux.setSelected(true);
            rbEditionAvecTotaux.setName("label21");
            rbEditionAvecTotaux.setMinimumSize(new Dimension(150, 30));
            rbEditionAvecTotaux.setPreferredSize(new Dimension(150, 30));
            rbEditionAvecTotaux.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbEditionAvecTotaux.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                rbEditionChiffreeItemStateChanged(e);
              }
            });
            pnlChiffrage.add(rbEditionAvecTotaux, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbEditionSansTotaux ----
            rbEditionSansTotaux.setText("Edition chiffr\u00e9e sans totaux");
            rbEditionSansTotaux.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbEditionSansTotaux.setName("label21");
            rbEditionSansTotaux.setMinimumSize(new Dimension(150, 30));
            rbEditionSansTotaux.setPreferredSize(new Dimension(150, 30));
            rbEditionSansTotaux.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbEditionSansTotaux.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                rbEditionSansPiedItemStateChanged(e);
              }
            });
            pnlChiffrage.add(rbEditionSansTotaux, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbEditionNonChiffre ----
            rbEditionNonChiffre.setText("Edition non chiffr\u00e9e");
            rbEditionNonChiffre.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbEditionNonChiffre.setName("label21");
            rbEditionNonChiffre.setMinimumSize(new Dimension(150, 30));
            rbEditionNonChiffre.setPreferredSize(new Dimension(150, 30));
            rbEditionNonChiffre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbEditionNonChiffre.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                rbEditionNonChiffreItemStateChanged(e);
              }
            });
            pnlChiffrage.add(rbEditionNonChiffre, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlChiffrage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlChoixEdition ========
          {
            pnlChoixEdition.setTitre("Mode d'\u00e9dition");
            pnlChoixEdition.setName("pnlChoixEdition");
            pnlChoixEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlChoixEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlChoixEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlChoixEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlChoixEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- ckVisualiser ----
            ckVisualiser.setText("Visualiser");
            ckVisualiser.setPreferredSize(new Dimension(200, 30));
            ckVisualiser.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ckVisualiser.setFont(ckVisualiser.getFont().deriveFont(ckVisualiser.getFont().getSize() + 2f));
            ckVisualiser.setName("ckVisualiser");
            ckVisualiser.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ckVisualiserActionPerformed(e);
              }
            });
            pnlChoixEdition.add(ckVisualiser, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ckImprimer ----
            ckImprimer.setText("Imprimer");
            ckImprimer.setPreferredSize(new Dimension(200, 30));
            ckImprimer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ckImprimer.setFont(ckImprimer.getFont().deriveFont(ckImprimer.getFont().getSize() + 2f));
            ckImprimer.setName("ckImprimer");
            ckImprimer.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ckImprimerActionPerformed(e);
              }
            });
            pnlChoixEdition.add(ckImprimer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
                GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlEnvoyerParMail ========
            {
              pnlEnvoyerParMail.setName("pnlEnvoyerParMail");
              pnlEnvoyerParMail.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEnvoyerParMail.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEnvoyerParMail.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlEnvoyerParMail.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEnvoyerParMail.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- ckEnvoyerParMail ----
              ckEnvoyerParMail.setText("Envoyer par mail \u00e0 ");
              ckEnvoyerParMail.setFont(new Font("sansserif", Font.PLAIN, 14));
              ckEnvoyerParMail.setName("label21");
              ckEnvoyerParMail.setMinimumSize(new Dimension(150, 30));
              ckEnvoyerParMail.setPreferredSize(new Dimension(150, 30));
              ckEnvoyerParMail.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ckEnvoyerParMail.setMaximumSize(new Dimension(150, 30));
              ckEnvoyerParMail.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  ckEnvoyerParMailActionPerformed(e);
                }
              });
              pnlEnvoyerParMail.add(ckEnvoyerParMail, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ======== pnlMail ========
              {
                pnlMail.setOpaque(false);
                pnlMail.setName("pnlMail");
                pnlMail.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlMail.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlMail.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlMail.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlMail.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbContactMail ----
                lbContactMail.setText("Contact");
                lbContactMail.setMaximumSize(new Dimension(75, 30));
                lbContactMail.setMinimumSize(new Dimension(75, 30));
                lbContactMail.setPreferredSize(new Dimension(75, 30));
                lbContactMail.setName("lbContactMail");
                pnlMail.add(lbContactMail, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- snContactMail ----
                snContactMail.setName("snContactMail");
                snContactMail.addSNComposantListener(new InterfaceSNComposantListener() {
                  @Override
                  public void valueChanged(SNComposantEvent e) {
                    snContactMailValueChanged(e);
                  }
                });
                pnlMail.add(snContactMail, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbMail ----
                lbMail.setText("Mail");
                lbMail.setMaximumSize(new Dimension(75, 30));
                lbMail.setMinimumSize(new Dimension(75, 30));
                lbMail.setPreferredSize(new Dimension(75, 30));
                lbMail.setName("lbMail");
                pnlMail.add(lbMail, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfEmailDestinataire ----
                tfEmailDestinataire.setMinimumSize(new Dimension(200, 30));
                tfEmailDestinataire.setPreferredSize(new Dimension(200, 30));
                tfEmailDestinataire.setName("tfEmailDestinataire");
                tfEmailDestinataire.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    tfEmailDestinataireFocusLost(e);
                  }
                });
                pnlMail.add(tfEmailDestinataire, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlEnvoyerParMail.add(pnlMail, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlChoixEdition.add(pnlEnvoyerParMail, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlEnvoyerParFax ========
            {
              pnlEnvoyerParFax.setName("pnlEnvoyerParFax");
              pnlEnvoyerParFax.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEnvoyerParFax.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEnvoyerParFax.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlEnvoyerParFax.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEnvoyerParFax.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- ckEnvoyerParFax ----
              ckEnvoyerParFax.setText("Envoyer par fax au");
              ckEnvoyerParFax.setFont(new Font("sansserif", Font.PLAIN, 14));
              ckEnvoyerParFax.setName("label21");
              ckEnvoyerParFax.setMinimumSize(new Dimension(150, 30));
              ckEnvoyerParFax.setPreferredSize(new Dimension(150, 30));
              ckEnvoyerParFax.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ckEnvoyerParFax.setMaximumSize(new Dimension(150, 30));
              ckEnvoyerParFax.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  ckEnvoyerParFaxActionPerformed(e);
                }
              });
              pnlEnvoyerParFax.add(ckEnvoyerParFax, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ======== pnlFax ========
              {
                pnlFax.setOpaque(false);
                pnlFax.setName("pnlFax");
                pnlFax.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlFax.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlFax.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlFax.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlFax.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbContactFax ----
                lbContactFax.setText("Contact");
                lbContactFax.setMaximumSize(new Dimension(75, 30));
                lbContactFax.setMinimumSize(new Dimension(75, 30));
                lbContactFax.setPreferredSize(new Dimension(75, 30));
                lbContactFax.setName("lbContactFax");
                pnlFax.add(lbContactFax, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- snContactFax ----
                snContactFax.setName("snContactFax");
                snContactFax.addSNComposantListener(new InterfaceSNComposantListener() {
                  @Override
                  public void valueChanged(SNComposantEvent e) {
                    snContactFaxValueChanged(e);
                  }
                });
                pnlFax.add(snContactFax, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbFax ----
                lbFax.setText("Fax");
                lbFax.setMaximumSize(new Dimension(75, 30));
                lbFax.setMinimumSize(new Dimension(75, 30));
                lbFax.setPreferredSize(new Dimension(75, 30));
                lbFax.setName("lbFax");
                pnlFax.add(lbFax, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfFaxDestinataire ----
                tfFaxDestinataire.setMinimumSize(new Dimension(200, 30));
                tfFaxDestinataire.setPreferredSize(new Dimension(200, 30));
                tfFaxDestinataire.setName("tfFaxDestinataire");
                tfFaxDestinataire.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    tfFaxDestinataireFocusLost(e);
                  }
                });
                pnlFax.add(tfFaxDestinataire, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlEnvoyerParFax.add(pnlFax, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlChoixEdition.add(pnlEnvoyerParFax, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlChoixEdition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSuperieur.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEditionDocumentSupplementaire ========
          {
            pnlEditionDocumentSupplementaire.setTitre("Edition de documents suppl\u00e9mentaires");
            pnlEditionDocumentSupplementaire.setName("pnlEditionDocumentSupplementaire");
            pnlEditionDocumentSupplementaire.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEditionDocumentSupplementaire.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlEditionDocumentSupplementaire.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlEditionDocumentSupplementaire.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEditionDocumentSupplementaire.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- ckBonDePreparation ----
            ckBonDePreparation.setText("Avec bon de pr\u00e9paration");
            ckBonDePreparation.setFont(new Font("sansserif", Font.PLAIN, 14));
            ckBonDePreparation.setName("label21");
            ckBonDePreparation.setPreferredSize(new Dimension(200, 30));
            ckBonDePreparation.setMaximumSize(new Dimension(250, 30));
            ckBonDePreparation.setMinimumSize(new Dimension(200, 30));
            ckBonDePreparation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ckBonDePreparation.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ckBonDePreparationActionPerformed(e);
              }
            });
            pnlEditionDocumentSupplementaire.add(ckBonDePreparation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEditionDocumentSupplementaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptionsComplementairesAchat ========
          {
            pnlOptionsComplementairesAchat.setTitre("Documents achats");
            pnlOptionsComplementairesAchat.setName("pnlOptionsComplementairesAchat");
            pnlOptionsComplementairesAchat.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionsComplementairesAchat.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionsComplementairesAchat.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionsComplementairesAchat.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionsComplementairesAchat.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- rbMultiFournisseur ----
            rbMultiFournisseur.setText("Multi-fournisseurs");
            rbMultiFournisseur.setPreferredSize(new Dimension(150, 30));
            rbMultiFournisseur.setFont(rbMultiFournisseur.getFont().deriveFont(rbMultiFournisseur.getFont().getSize() + 2f));
            rbMultiFournisseur.setMinimumSize(new Dimension(150, 30));
            rbMultiFournisseur.setMaximumSize(new Dimension(150, 30));
            rbMultiFournisseur.setName("rbMultiFournisseur");
            rbMultiFournisseur.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                rbMultiFournisseurActionPerformed(e);
              }
            });
            pnlOptionsComplementairesAchat.add(rbMultiFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbMonoFournisseur ----
            rbMonoFournisseur.setText("Mono-fournisseur");
            rbMonoFournisseur.setPreferredSize(new Dimension(150, 30));
            rbMonoFournisseur.setFont(rbMonoFournisseur.getFont().deriveFont(rbMonoFournisseur.getFont().getSize() + 2f));
            rbMonoFournisseur.setMaximumSize(new Dimension(150, 30));
            rbMonoFournisseur.setMinimumSize(new Dimension(150, 30));
            rbMonoFournisseur.setName("rbMonoFournisseur");
            rbMonoFournisseur.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                rbMonoFournisseurActionPerformed(e);
              }
            });
            pnlOptionsComplementairesAchat.add(rbMonoFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlAdresseFournisseur ========
            {
              pnlAdresseFournisseur.setOpaque(false);
              pnlAdresseFournisseur.setName("pnlAdresseFournisseur");
              pnlAdresseFournisseur.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlAdresseFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlAdresseFournisseur.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlAdresseFournisseur.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlAdresseFournisseur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbAdresseFournisseur ----
              lbAdresseFournisseur.setText("Adresse fournisseur");
              lbAdresseFournisseur.setPreferredSize(new Dimension(150, 30));
              lbAdresseFournisseur.setMinimumSize(new Dimension(150, 30));
              lbAdresseFournisseur.setMaximumSize(new Dimension(150, 30));
              lbAdresseFournisseur.setName("lbAdresseFournisseur");
              pnlAdresseFournisseur.add(lbAdresseFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snAdresseFournisseur ----
              snAdresseFournisseur.setName("snAdresseFournisseur");
              pnlAdresseFournisseur.add(snAdresseFournisseur, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOptionsComplementairesAchat.add(pnlAdresseFournisseur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ckGenerationCommandeAchat ----
            ckGenerationCommandeAchat.setText("G\u00e9n\u00e9ration imm\u00e9diate de la commande d'achat");
            ckGenerationCommandeAchat.setName("chk_GenerationCommandeAchat");
            ckGenerationCommandeAchat
                .setFont(ckGenerationCommandeAchat.getFont().deriveFont(ckGenerationCommandeAchat.getFont().getSize() + 2f));
            ckGenerationCommandeAchat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ckGenerationCommandeAchat.setMaximumSize(new Dimension(400, 30));
            ckGenerationCommandeAchat.setMinimumSize(new Dimension(400, 30));
            ckGenerationCommandeAchat.setPreferredSize(new Dimension(400, 30));
            ckGenerationCommandeAchat.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ckGenerationCommandeAchatActionPerformed(e);
              }
            });
            pnlOptionsComplementairesAchat.add(ckGenerationCommandeAchat, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionsComplementairesAchat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSuperieur.add(pnlDroite);
      }
      pnlContenu.add(pnlSuperieur,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- sNLabelTitre1 ----
      sNLabelTitre1.setText("Commandes d'achats");
      sNLabelTitre1.setName("sNLabelTitre1");
      pnlContenu.add(sNLabelTitre1,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpListeDocument ========
      {
        scpListeDocument.setPreferredSize(new Dimension(1050, 235));
        scpListeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
        scpListeDocument.setMinimumSize(new Dimension(1050, 230));
        scpListeDocument.setName("scpListeDocument");
        
        // ---- tblListeDocument ----
        tblListeDocument.setShowVerticalLines(true);
        tblListeDocument.setShowHorizontalLines(true);
        tblListeDocument.setBackground(Color.white);
        tblListeDocument.setRowHeight(20);
        tblListeDocument.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblListeDocument.setAutoCreateRowSorter(true);
        tblListeDocument.setSelectionBackground(new Color(57, 105, 138));
        tblListeDocument.setGridColor(new Color(204, 204, 204));
        tblListeDocument.setName("tblListeDocument");
        scpListeDocument.setViewportView(tblListeDocument);
      }
      pnlContenu.add(scpListeDocument,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ======== pnlOptionFacturation ========
    {
      pnlOptionFacturation.setTitre("Options de facturation");
      pnlOptionFacturation.setPreferredSize(new Dimension(800, 80));
      pnlOptionFacturation.setMaximumSize(new Dimension(2147483647, 80));
      pnlOptionFacturation.setName("pnlOptionFacturation");
      pnlOptionFacturation.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlOptionFacturation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlOptionFacturation.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlOptionFacturation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlOptionFacturation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      
      // ---- lbDateFacturationMinimale ----
      lbDateFacturationMinimale.setText("Ne sera pas factur\u00e9 avant le");
      lbDateFacturationMinimale.setPreferredSize(new Dimension(230, 30));
      lbDateFacturationMinimale.setMinimumSize(new Dimension(230, 30));
      lbDateFacturationMinimale.setMaximumSize(new Dimension(230, 30));
      lbDateFacturationMinimale.setName("lbDateFacturationMinimale");
      pnlOptionFacturation.add(lbDateFacturationMinimale,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- calDateFacturationMinimale ----
      calDateFacturationMinimale.setMinimumSize(new Dimension(115, 30));
      calDateFacturationMinimale.setMaximumSize(new Dimension(115, 30));
      calDateFacturationMinimale.setPreferredSize(new Dimension(115, 30));
      calDateFacturationMinimale.setFont(new Font("sansserif", Font.PLAIN, 14));
      calDateFacturationMinimale.setName("calDateFacturationMinimale");
      calDateFacturationMinimale.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          calDateFacturationMinimaleActionPerformed(e);
        }
      });
      pnlOptionFacturation.add(calDateFacturationMinimale,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    
    // ======== pnlEditionDevis ========
    {
      pnlEditionDevis.setTitre("Type d'\u00e9dition");
      pnlEditionDevis.setName("pnlEditionDevis");
      pnlEditionDevis.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlEditionDevis.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlEditionDevis.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlEditionDevis.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlEditionDevis.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      
      // ---- rbDevisAvecTotaux ----
      rbDevisAvecTotaux.setText("Devis avec totaux");
      rbDevisAvecTotaux.setFont(new Font("sansserif", Font.PLAIN, 14));
      rbDevisAvecTotaux.setSelected(true);
      rbDevisAvecTotaux.setName("label21");
      rbDevisAvecTotaux.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      rbDevisAvecTotaux.setPreferredSize(new Dimension(250, 30));
      rbDevisAvecTotaux.setMinimumSize(new Dimension(150, 30));
      rbDevisAvecTotaux.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          rbDevisAvecTotauxItemStateChanged(e);
        }
      });
      pnlEditionDevis.add(rbDevisAvecTotaux, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- rbDevisSansTotaux ----
      rbDevisSansTotaux.setText("Devis sans totaux");
      rbDevisSansTotaux.setFont(new Font("sansserif", Font.PLAIN, 14));
      rbDevisSansTotaux.setName("label21");
      rbDevisSansTotaux.setMinimumSize(new Dimension(150, 30));
      rbDevisSansTotaux.setPreferredSize(new Dimension(250, 30));
      rbDevisSansTotaux.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      rbDevisSansTotaux.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          rbDevisSansTotauxItemStateChanged(e);
        }
      });
      pnlEditionDevis.add(rbDevisSansTotaux, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- rbFactureProforma ----
      rbFactureProforma.setText("Facture proforma");
      rbFactureProforma.setFont(new Font("sansserif", Font.PLAIN, 14));
      rbFactureProforma.setSelected(true);
      rbFactureProforma.setName("label21");
      rbFactureProforma.setMinimumSize(new Dimension(150, 30));
      rbFactureProforma.setPreferredSize(new Dimension(250, 30));
      rbFactureProforma.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      rbFactureProforma.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          rbFactureProformaItemStateChanged(e);
        }
      });
      pnlEditionDevis.add(rbFactureProforma, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    }
    
    // ======== pnlTypeEdition ========
    {
      pnlTypeEdition.setTitre("Type d'\u00e9dition");
      pnlTypeEdition.setName("pnlTypeEdition");
      pnlTypeEdition.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlTypeEdition.getLayout()).columnWidths = new int[] { 595, 0, 0 };
      ((GridBagLayout) pnlTypeEdition.getLayout()).rowHeights = new int[] { 25, 0 };
      ((GridBagLayout) pnlTypeEdition.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlTypeEdition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      
      // ---- rbEditionFacture ----
      rbEditionFacture.setText("Edition facture");
      rbEditionFacture.setFont(new Font("sansserif", Font.PLAIN, 14));
      rbEditionFacture.setMinimumSize(new Dimension(250, 30));
      rbEditionFacture.setPreferredSize(new Dimension(200, 30));
      rbEditionFacture.setMaximumSize(new Dimension(250, 30));
      rbEditionFacture.setName("rbEditionFacture");
      pnlTypeEdition.add(rbEditionFacture, new GridBagConstraints(0, 0, 2, 1, 0.5, 0.0, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- rbEditionTicketCaisse ----
      rbEditionTicketCaisse.setText("Edition ticket de caisse");
      rbEditionTicketCaisse.setFont(new Font("sansserif", Font.PLAIN, 14));
      rbEditionTicketCaisse.setMinimumSize(new Dimension(250, 30));
      rbEditionTicketCaisse.setPreferredSize(new Dimension(200, 30));
      rbEditionTicketCaisse.setMaximumSize(new Dimension(250, 30));
      rbEditionTicketCaisse.setName("rbEditionTicketCaisse");
      pnlTypeEdition.add(rbEditionTicketCaisse, new GridBagConstraints(1, 0, 1, 1, 0.5, 0.0, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
    }
    
    // ---- btgPiedChiffre ----
    ButtonGroup btgPiedChiffre = new ButtonGroup();
    btgPiedChiffre.add(rbEditionAvecTotaux);
    btgPiedChiffre.add(rbEditionSansTotaux);
    btgPiedChiffre.add(rbEditionNonChiffre);
    
    // ---- groupe ----
    ButtonGroup groupe = new ButtonGroup();
    groupe.add(rbMultiFournisseur);
    groupe.add(rbMonoFournisseur);
    
    // ---- bgTypeEditionDevis ----
    bgTypeEditionDevis.add(rbDevisAvecTotaux);
    bgTypeEditionDevis.add(rbDevisSansTotaux);
    bgTypeEditionDevis.add(rbFactureProforma);
    
    // ---- btgEditionFacture ----
    ButtonGroup btgEditionFacture = new ButtonGroup();
    btgEditionFacture.add(rbEditionFacture);
    btgEditionFacture.add(rbEditionTicketCaisse);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlSuperieur;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlChiffrage;
  private JRadioButton rbEditionAvecTotaux;
  private JRadioButton rbEditionSansTotaux;
  private JRadioButton rbEditionNonChiffre;
  private SNPanelTitre pnlChoixEdition;
  private JCheckBox ckVisualiser;
  private JCheckBox ckImprimer;
  private SNPanel pnlEnvoyerParMail;
  private JCheckBox ckEnvoyerParMail;
  private JPanel pnlMail;
  private SNLabelChamp lbContactMail;
  private SNContact snContactMail;
  private SNLabelChamp lbMail;
  private SNTexte tfEmailDestinataire;
  private SNPanel pnlEnvoyerParFax;
  private JCheckBox ckEnvoyerParFax;
  private JPanel pnlFax;
  private SNLabelChamp lbContactFax;
  private SNContact snContactFax;
  private SNLabelChamp lbFax;
  private SNTexte tfFaxDestinataire;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEditionDocumentSupplementaire;
  private XRiCheckBox ckBonDePreparation;
  private SNPanelTitre pnlOptionsComplementairesAchat;
  private JRadioButton rbMultiFournisseur;
  private JRadioButton rbMonoFournisseur;
  private JPanel pnlAdresseFournisseur;
  private SNLabelChamp lbAdresseFournisseur;
  private SNAdresseFournisseur snAdresseFournisseur;
  private JCheckBox ckGenerationCommandeAchat;
  private SNLabelTitre sNLabelTitre1;
  private JScrollPane scpListeDocument;
  private NRiTable tblListeDocument;
  private SNPanelTitre pnlOptionFacturation;
  private SNLabelChamp lbDateFacturationMinimale;
  private XRiCalendrier calDateFacturationMinimale;
  private SNPanelTitre pnlEditionDevis;
  private JRadioButton rbDevisAvecTotaux;
  private JRadioButton rbDevisSansTotaux;
  private JRadioButton rbFactureProforma;
  private SNPanelTitre pnlTypeEdition;
  private JRadioButton rbEditionFacture;
  private JRadioButton rbEditionTicketCaisse;
  private ButtonGroup bgTypeEditionDevis;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
