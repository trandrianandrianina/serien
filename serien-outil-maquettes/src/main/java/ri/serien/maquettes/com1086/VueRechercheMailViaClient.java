/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.com1086;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;

/**
 * Com 1806 écran de recherche d'un mail via une fiche client
 */
public class VueRechercheMailViaClient extends JDialog {
  
  public VueRechercheMailViaClient() {
    super();
    initComponents();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    sNBarreBouton1 = new SNBarreBouton();
    sNPanelEcranRPG1 = new SNPanelEcranRPG();
    sNPanelContenu1 = new SNPanelContenu();
    sNPanelTitre1 = new SNPanel();
    sNPanel1 = new SNPanel();
    lbRecherche = new SNLabelChamp();
    sNTextField2 = new SNTexte();
    lbContact = new SNLabelChamp();
    snContact = new SNContact();
    sNLabelChamp3 = new SNLabelChamp();
    xRiCalendrier1 = new XRiCalendrier();
    sNLabelChamp5 = new SNLabelChamp();
    xRiCalendrier2 = new XRiCalendrier();
    sNLabelChamp4 = new SNLabelChamp();
    xRiComboBox1 = new SNComboBox();
    sNPanel2 = new SNPanel();
    sNBarreRecherche1 = new SNBarreRecherche();
    sNLabelTitre1 = new SNLabelTitre();
    scrollPane1 = new JScrollPane();
    nRiTable1 = new NRiTable();
    
    // ======== this ========
    setMinimumSize(new Dimension(1050, 620));
    setTitle("Mails du client : GOODENOUGHT DAVID");
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ---- sNBarreBouton1 ----
    sNBarreBouton1.setName("sNBarreBouton1");
    contentPane.add(sNBarreBouton1, BorderLayout.SOUTH);
    
    // ======== sNPanelEcranRPG1 ========
    {
      sNPanelEcranRPG1.setName("sNPanelEcranRPG1");
      sNPanelEcranRPG1.setLayout(new BorderLayout());
      
      // ======== sNPanelContenu1 ========
      {
        sNPanelContenu1.setName("sNPanelContenu1");
        sNPanelContenu1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== sNPanelTitre1 ========
        {
          sNPanelTitre1.setName("sNPanelTitre1");
          sNPanelTitre1.setLayout(new GridLayout());
          
          // ======== sNPanel1 ========
          {
            sNPanel1.setName("sNPanel1");
            sNPanel1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbRecherche ----
            lbRecherche.setText("Recherche");
            lbRecherche.setName("lbRecherche");
            sNPanel1.add(lbRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- sNTextField2 ----
            sNTextField2.setName("sNTextField2");
            sNPanel1.add(sNTextField2, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbContact ----
            lbContact.setText("Contact");
            lbContact.setName("lbContact");
            sNPanel1.add(lbContact, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snContact ----
            snContact.setName("snContact");
            sNPanel1.add(snContact, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp3 ----
            sNLabelChamp3.setText("Date");
            sNLabelChamp3.setName("sNLabelChamp3");
            sNPanel1.add(sNLabelChamp3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- xRiCalendrier1 ----
            xRiCalendrier1.setPreferredSize(new Dimension(110, 30));
            xRiCalendrier1.setMinimumSize(new Dimension(110, 30));
            xRiCalendrier1.setMaximumSize(new Dimension(110, 30));
            xRiCalendrier1.setName("xRiCalendrier1");
            sNPanel1.add(xRiCalendrier1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- sNLabelChamp5 ----
            sNLabelChamp5.setText("au");
            sNLabelChamp5.setPreferredSize(new Dimension(16, 30));
            sNLabelChamp5.setMinimumSize(new Dimension(16, 30));
            sNLabelChamp5.setMaximumSize(new Dimension(16, 30));
            sNLabelChamp5.setName("sNLabelChamp5");
            sNPanel1.add(sNLabelChamp5, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- xRiCalendrier2 ----
            xRiCalendrier2.setPreferredSize(new Dimension(110, 30));
            xRiCalendrier2.setMinimumSize(new Dimension(110, 30));
            xRiCalendrier2.setMaximumSize(new Dimension(110, 30));
            xRiCalendrier2.setName("xRiCalendrier2");
            sNPanel1.add(xRiCalendrier2, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp4 ----
            sNLabelChamp4.setText("Type");
            sNLabelChamp4.setName("sNLabelChamp4");
            sNPanel1.add(sNLabelChamp4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- xRiComboBox1 ----
            xRiComboBox1.setFont(new Font("sansserif", Font.PLAIN, 14));
            xRiComboBox1.setPreferredSize(new Dimension(175, 30));
            xRiComboBox1.setMinimumSize(new Dimension(175, 30));
            xRiComboBox1.setMaximumSize(new Dimension(175, 30));
            xRiComboBox1.setModel(new DefaultComboBoxModel(new String[] { "," }));
            xRiComboBox1.setName("xRiComboBox1");
            sNPanel1.add(xRiComboBox1, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanelTitre1.add(sNPanel1);
          
          // ======== sNPanel2 ========
          {
            sNPanel2.setName("sNPanel2");
            sNPanel2.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            
            // ---- sNBarreRecherche1 ----
            sNBarreRecherche1.setName("sNBarreRecherche1");
            sNPanel2.add(sNBarreRecherche1, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanelTitre1.add(sNPanel2);
        }
        sNPanelContenu1.add(sNPanelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelTitre1 ----
        sNLabelTitre1.setText("Mails correspondants \u00e0 votre recherche");
        sNLabelTitre1.setName("sNLabelTitre1");
        sNPanelContenu1.add(sNLabelTitre1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");
          
          // ---- nRiTable1 ----
          nRiTable1.setModel(new DefaultTableModel(
              new Object[][] { { null, "02.12.2020", null, "Commande client disponible", "(Image)", null, "(logo couleur vert)" },
                  { null, null, null, null, null, null, null }, { null, null, null, null, null, null, null },
                  { null, null, null, null, null, null, null }, { null, null, null, null, null, null, null },
                  { null, null, null, null, null, null, null }, { null, null, null, null, null, null, null },
                  { null, null, null, null, null, null, null }, { null, null, null, null, null, null, null },
                  { null, null, null, null, null, null, null }, },
              new String[] { "Identifiant", "Date", "Objet du mail", "Type du mail", "Pi\u00e8ce jointe", "Destinataire", "Statut" }));
          {
            TableColumnModel cm = nRiTable1.getColumnModel();
            cm.getColumn(0).setMinWidth(85);
            cm.getColumn(0).setMaxWidth(85);
            cm.getColumn(0).setPreferredWidth(85);
            cm.getColumn(1).setMinWidth(75);
            cm.getColumn(1).setMaxWidth(75);
            cm.getColumn(1).setPreferredWidth(75);
            cm.getColumn(3).setMinWidth(170);
            cm.getColumn(3).setMaxWidth(170);
            cm.getColumn(3).setPreferredWidth(170);
            cm.getColumn(4).setMinWidth(85);
            cm.getColumn(4).setMaxWidth(85);
            cm.getColumn(4).setPreferredWidth(85);
            cm.getColumn(6).setMinWidth(125);
            cm.getColumn(6).setMaxWidth(125);
            cm.getColumn(6).setPreferredWidth(125);
          }
          nRiTable1.setName("nRiTable1");
          scrollPane1.setViewportView(nRiTable1);
        }
        sNPanelContenu1.add(scrollPane1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelEcranRPG1.add(sNPanelContenu1, BorderLayout.CENTER);
    }
    contentPane.add(sNPanelEcranRPG1, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton sNBarreBouton1;
  private SNPanelEcranRPG sNPanelEcranRPG1;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel sNPanelTitre1;
  private SNPanel sNPanel1;
  private SNLabelChamp lbRecherche;
  private SNTexte sNTextField2;
  private SNLabelChamp lbContact;
  private SNContact snContact;
  private SNLabelChamp sNLabelChamp3;
  private XRiCalendrier xRiCalendrier1;
  private SNLabelChamp sNLabelChamp5;
  private XRiCalendrier xRiCalendrier2;
  private SNLabelChamp sNLabelChamp4;
  private SNComboBox xRiComboBox1;
  private SNPanel sNPanel2;
  private SNBarreRecherche sNBarreRecherche1;
  private SNLabelTitre sNLabelTitre1;
  private JScrollPane scrollPane1;
  private NRiTable nRiTable1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
