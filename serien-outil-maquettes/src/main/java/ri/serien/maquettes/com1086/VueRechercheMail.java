/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.com1086;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Ecran de recherche de mail
 */
public class VueRechercheMail extends JPanel {
  
  public VueRechercheMail() {
    super();
    initComponents();
  }
  
  private void tfTexteRechercheActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteRechercheFocusGained(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void calDebutPopupMenuWillBecomeInvisible(PopupMenuEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void calFinPopupMenuWillBecomeInvisible(PopupMenuEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbTypeItemStateChanged(ItemEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBandeauTitre = new SNBandeauTitre();
    sNPanelEcranRPG1 = new SNPanelEcranRPG();
    sNPanelContenu1 = new SNPanelContenu();
    sNPanelTitre1 = new SNPanel();
    pnlRechercheGauche = new JPanel();
    lbTexteRecherche = new SNLabelChamp();
    tfTexteRecherche = new SNTexte();
    sNLabelChamp1 = new SNLabelChamp();
    sNClient1 = new SNClient();
    lbFamille = new SNLabelChamp();
    pnlDateCreation = new SNPanel();
    calDebut = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    calFin = new XRiCalendrier();
    lbType = new SNLabelChamp();
    cbType = new SNComboBox();
    pnlRechercheDroite = new JPanel();
    snBarreRecherche = new SNBarreRecherche();
    sNLabelTitre1 = new SNLabelTitre();
    scrollPane1 = new JScrollPane();
    nRiTable1 = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setPreferredSize(new Dimension(1190, 700));
    setMinimumSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBandeauTitre ----
    snBandeauTitre.setText("Mails envoy\u00e9s");
    snBandeauTitre.setName("snBandeauTitre");
    add(snBandeauTitre, BorderLayout.NORTH);
    
    // ======== sNPanelEcranRPG1 ========
    {
      sNPanelEcranRPG1.setName("sNPanelEcranRPG1");
      sNPanelEcranRPG1.setLayout(new BorderLayout());
      
      // ======== sNPanelContenu1 ========
      {
        sNPanelContenu1.setName("sNPanelContenu1");
        sNPanelContenu1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== sNPanelTitre1 ========
        {
          sNPanelTitre1.setName("sNPanelTitre1");
          sNPanelTitre1.setLayout(new GridLayout());
          
          // ======== pnlRechercheGauche ========
          {
            pnlRechercheGauche.setOpaque(false);
            pnlRechercheGauche.setBorder(null);
            pnlRechercheGauche.setName("pnlRechercheGauche");
            pnlRechercheGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWidths = new int[] { 165, 300, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbTexteRecherche ----
            lbTexteRecherche.setText("Recherche");
            lbTexteRecherche.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTexteRecherche.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTexteRecherche.setPreferredSize(new Dimension(100, 30));
            lbTexteRecherche.setName("lbTexteRecherche");
            pnlRechercheGauche.add(lbTexteRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfTexteRecherche ----
            tfTexteRecherche.setName("tfTexteRecherche");
            tfTexteRecherche.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                tfTexteRechercheActionPerformed(e);
              }
            });
            tfTexteRecherche.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                tfTexteRechercheFocusGained(e);
              }
            });
            pnlRechercheGauche.add(tfTexteRecherche, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp1 ----
            sNLabelChamp1.setText("Client");
            sNLabelChamp1.setName("sNLabelChamp1");
            pnlRechercheGauche.add(sNLabelChamp1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- sNClient1 ----
            sNClient1.setName("sNClient1");
            pnlRechercheGauche.add(sNClient1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamille ----
            lbFamille.setText("Date du");
            lbFamille.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFamille.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFamille.setPreferredSize(new Dimension(100, 30));
            lbFamille.setName("lbFamille");
            pnlRechercheGauche.add(lbFamille, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlDateCreation ========
            {
              pnlDateCreation.setName("pnlDateCreation");
              pnlDateCreation.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateCreation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateCreation.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateCreation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateCreation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- calDebut ----
              calDebut.setMinimumSize(new Dimension(110, 30));
              calDebut.setMaximumSize(new Dimension(110, 30));
              calDebut.setPreferredSize(new Dimension(110, 30));
              calDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
              calDebut.setName("calDebut");
              calDebut.addPopupMenuListener(new PopupMenuListener() {
                @Override
                public void popupMenuCanceled(PopupMenuEvent e) {
                }
                
                @Override
                public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                  calDebutPopupMenuWillBecomeInvisible(e);
                }
                
                @Override
                public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                }
              });
              pnlDateCreation.add(calDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setMinimumSize(new Dimension(16, 30));
              lbAu.setPreferredSize(new Dimension(16, 30));
              lbAu.setMaximumSize(new Dimension(16, 30));
              lbAu.setName("lbAu");
              pnlDateCreation.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- calFin ----
              calFin.setMinimumSize(new Dimension(110, 30));
              calFin.setMaximumSize(new Dimension(110, 30));
              calFin.setPreferredSize(new Dimension(110, 30));
              calFin.setFont(new Font("sansserif", Font.PLAIN, 14));
              calFin.setName("calFin");
              calFin.addPopupMenuListener(new PopupMenuListener() {
                @Override
                public void popupMenuCanceled(PopupMenuEvent e) {
                }
                
                @Override
                public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                  calFinPopupMenuWillBecomeInvisible(e);
                }
                
                @Override
                public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                }
              });
              pnlDateCreation.add(calFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlRechercheGauche.add(pnlDateCreation, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbType ----
            lbType.setText("Type");
            lbType.setHorizontalAlignment(SwingConstants.RIGHT);
            lbType.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbType.setPreferredSize(new Dimension(100, 30));
            lbType.setName("lbType");
            pnlRechercheGauche.add(lbType, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbType ----
            cbType.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbType.setPreferredSize(new Dimension(175, 30));
            cbType.setMinimumSize(new Dimension(175, 30));
            cbType.setMaximumSize(new Dimension(175, 30));
            cbType.setName("cbType");
            cbType.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeItemStateChanged(e);
              }
            });
            pnlRechercheGauche.add(cbType, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanelTitre1.add(pnlRechercheGauche);
          
          // ======== pnlRechercheDroite ========
          {
            pnlRechercheDroite.setOpaque(false);
            pnlRechercheDroite.setName("pnlRechercheDroite");
            pnlRechercheDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlRechercheDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            
            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlRechercheDroite.add(snBarreRecherche, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanelTitre1.add(pnlRechercheDroite);
        }
        sNPanelContenu1.add(sNPanelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelTitre1 ----
        sNLabelTitre1.setText("Mails correspondants \u00e0 votre recherche");
        sNLabelTitre1.setName("sNLabelTitre1");
        sNPanelContenu1.add(sNLabelTitre1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");
          
          // ---- nRiTable1 ----
          nRiTable1.setModel(new DefaultTableModel(
              new Object[][] { { "", "02.12.2020", null, "Commande client disponible", "(Image)", null, "(logo couleur vert)" },
                  { null, null, null, null, null, null, null }, { null, null, null, null, null, null, null },
                  { null, null, null, null, null, null, null }, { null, null, null, null, null, null, null },
                  { null, null, null, null, null, null, null }, { null, null, null, null, null, null, null },
                  { null, null, null, null, null, null, null }, { null, null, null, null, null, null, null },
                  { null, null, null, null, null, null, null }, },
              new String[] { "Identifiant", "Date", "Objet du mail", "Type de mail", "Pi\u00e8ces jointe", "Destinataire", "Statut" }));
          {
            TableColumnModel cm = nRiTable1.getColumnModel();
            cm.getColumn(0).setMinWidth(80);
            cm.getColumn(0).setMaxWidth(80);
            cm.getColumn(0).setPreferredWidth(80);
            cm.getColumn(1).setMinWidth(75);
            cm.getColumn(1).setMaxWidth(75);
            cm.getColumn(1).setPreferredWidth(75);
            cm.getColumn(3).setMinWidth(170);
            cm.getColumn(3).setMaxWidth(170);
            cm.getColumn(3).setPreferredWidth(170);
            cm.getColumn(4).setMinWidth(85);
            cm.getColumn(4).setMaxWidth(85);
            cm.getColumn(4).setPreferredWidth(85);
            cm.getColumn(6).setMinWidth(125);
            cm.getColumn(6).setMaxWidth(125);
            cm.getColumn(6).setPreferredWidth(125);
          }
          nRiTable1.setName("nRiTable1");
          scrollPane1.setViewportView(nRiTable1);
        }
        sNPanelContenu1.add(scrollPane1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelEcranRPG1.add(sNPanelContenu1, BorderLayout.CENTER);
    }
    add(sNPanelEcranRPG1, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre snBandeauTitre;
  private SNPanelEcranRPG sNPanelEcranRPG1;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel sNPanelTitre1;
  private JPanel pnlRechercheGauche;
  private SNLabelChamp lbTexteRecherche;
  private SNTexte tfTexteRecherche;
  private SNLabelChamp sNLabelChamp1;
  private SNClient sNClient1;
  private SNLabelChamp lbFamille;
  private SNPanel pnlDateCreation;
  private XRiCalendrier calDebut;
  private SNLabelChamp lbAu;
  private XRiCalendrier calFin;
  private SNLabelChamp lbType;
  private SNComboBox cbType;
  private JPanel pnlRechercheDroite;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre sNLabelTitre1;
  private JScrollPane scrollPane1;
  private NRiTable nRiTable1;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
