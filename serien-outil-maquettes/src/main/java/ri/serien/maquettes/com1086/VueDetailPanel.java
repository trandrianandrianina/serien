/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.com1086;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Com-1086 vue détail d'un mail
 */
public class VueDetailPanel extends JPanel implements ioFrame {
  
  public VueDetailPanel() {
    super();
    initComponents();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    riBandeau = new SNBandeauTitre();
    pnlPrincipal = new SNPanelEcranRPG();
    pnlContenu = new SNPanelContenu();
    tpOnglet = new JTabbedPane();
    pnlGeneral = new SNPanel();
    pnlInformation = new SNPanelEcranRPG();
    pnlStatut = new SNPanelContenu();
    lbStatut = new SNLabelChamp();
    xRiComboBox1 = new XRiComboBox();
    lbType = new SNLabelChamp();
    xRiComboBox2 = new XRiComboBox();
    lbIdentifiantDocument = new SNLabelChamp();
    tfIdentifiantDocument = new SNTexte();
    sNLabelChamp1 = new SNLabelChamp();
    xRiTextField1 = new XRiTextField();
    pnlEtablissement = new SNPanelContenu();
    lbEtablissement = new SNLabelChamp();
    snEtablissemet = new SNEtablissement();
    lbDateCreation = new SNLabelChamp();
    xRiCalendrier1 = new XRiCalendrier();
    lbDateEnvoi = new SNLabelChamp();
    xRiCalendrier2 = new XRiCalendrier();
    pnlMail = new SNPanelContenu();
    lbMessage = new SNMessage();
    lbObjet = new SNLabelChamp();
    tfObjet = new SNTexte();
    lbCorps = new SNLabelChamp();
    scrollPane1 = new JScrollPane();
    taCorpMail = new JTextArea();
    pnlDestinataire = new SNPanelContenu();
    scrollPane2 = new JScrollPane();
    nRiTable1 = new NRiTable();
    pnlPieceJointe = new SNPanelContenu();
    scrollPane3 = new JScrollPane();
    nRiTable2 = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setPreferredSize(new Dimension(1190, 700));
    setMinimumSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- riBandeau ----
    riBandeau.setText("D\u00e9tail du mail 11");
    riBandeau.setName("riBandeau");
    add(riBandeau, BorderLayout.NORTH);
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== tpOnglet ========
        {
          tpOnglet.setFont(new Font("sansserif", Font.PLAIN, 14));
          tpOnglet.setName("tpOnglet");
          
          // ======== pnlGeneral ========
          {
            pnlGeneral.setName("pnlGeneral");
            pnlGeneral.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGeneral.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlGeneral.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGeneral.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            
            // ======== pnlInformation ========
            {
              pnlInformation.setName("pnlInformation");
              pnlInformation.setLayout(new GridLayout());
              
              // ======== pnlStatut ========
              {
                pnlStatut.setName("pnlStatut");
                pnlStatut.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlStatut.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlStatut.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlStatut.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlStatut.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- lbStatut ----
                lbStatut.setText("Statut");
                lbStatut.setName("lbStatut");
                pnlStatut.add(lbStatut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- xRiComboBox1 ----
                xRiComboBox1.setModel(new DefaultComboBoxModel(new String[] { "En cours de pr\u00e9paration" }));
                xRiComboBox1.setFont(new Font("sansserif", Font.PLAIN, 14));
                xRiComboBox1.setMinimumSize(new Dimension(200, 30));
                xRiComboBox1.setPreferredSize(new Dimension(200, 30));
                xRiComboBox1.setMaximumSize(new Dimension(200, 30));
                xRiComboBox1.setName("xRiComboBox1");
                pnlStatut.add(xRiComboBox1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbType ----
                lbType.setText("Type");
                lbType.setName("lbType");
                pnlStatut.add(lbType, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- xRiComboBox2 ----
                xRiComboBox2.setFont(new Font("sansserif", Font.PLAIN, 14));
                xRiComboBox2.setModel(new DefaultComboBoxModel(new String[] { "Mail avec document en pi\u00e8ce" }));
                xRiComboBox2.setPreferredSize(new Dimension(218, 30));
                xRiComboBox2.setMinimumSize(new Dimension(218, 30));
                xRiComboBox2.setMaximumSize(new Dimension(218, 30));
                xRiComboBox2.setName("xRiComboBox2");
                pnlStatut.add(xRiComboBox2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbIdentifiantDocument ----
                lbIdentifiantDocument.setText("Identifiant document");
                lbIdentifiantDocument.setName("lbIdentifiantDocument");
                pnlStatut.add(lbIdentifiantDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfIdentifiantDocument ----
                tfIdentifiantDocument.setName("tfIdentifiantDocument");
                pnlStatut.add(tfIdentifiantDocument, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- sNLabelChamp1 ----
                sNLabelChamp1.setText("Destinataire principal");
                sNLabelChamp1.setName("sNLabelChamp1");
                pnlStatut.add(sNLabelChamp1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- xRiTextField1 ----
                xRiTextField1.setName("xRiTextField1");
                pnlStatut.add(xRiTextField1, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlInformation.add(pnlStatut);
              
              // ======== pnlEtablissement ========
              {
                pnlEtablissement.setName("pnlEtablissement");
                pnlEtablissement.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- lbEtablissement ----
                lbEtablissement.setText("Etablissement");
                lbEtablissement.setName("lbEtablissement");
                pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- snEtablissemet ----
                snEtablissemet.setName("snEtablissemet");
                pnlEtablissement.add(snEtablissemet, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbDateCreation ----
                lbDateCreation.setText("Date de cr\u00e9ation");
                lbDateCreation.setName("lbDateCreation");
                pnlEtablissement.add(lbDateCreation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- xRiCalendrier1 ----
                xRiCalendrier1.setPreferredSize(new Dimension(110, 30));
                xRiCalendrier1.setMinimumSize(new Dimension(110, 30));
                xRiCalendrier1.setMaximumSize(new Dimension(110, 30));
                xRiCalendrier1.setName("xRiCalendrier1");
                pnlEtablissement.add(xRiCalendrier1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbDateEnvoi ----
                lbDateEnvoi.setText("Date d'envoi");
                lbDateEnvoi.setName("lbDateEnvoi");
                pnlEtablissement.add(lbDateEnvoi, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- xRiCalendrier2 ----
                xRiCalendrier2.setPreferredSize(new Dimension(110, 30));
                xRiCalendrier2.setMinimumSize(new Dimension(110, 30));
                xRiCalendrier2.setName("xRiCalendrier2");
                pnlEtablissement.add(xRiCalendrier2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlInformation.add(pnlEtablissement);
            }
            pnlGeneral.add(pnlInformation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlMail ========
            {
              pnlMail.setName("pnlMail");
              pnlMail.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMail.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlMail.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlMail.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlMail.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              
              // ---- lbMessage ----
              lbMessage.setText("Le message n'a pas pu \u00eatre envoy\u00e9 car le serveur de messagerie est indisponible");
              lbMessage.setName("lbMessage");
              pnlMail.add(lbMessage, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbObjet ----
              lbObjet.setText("Objet");
              lbObjet.setName("lbObjet");
              pnlMail.add(lbObjet, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfObjet ----
              tfObjet.setName("tfObjet");
              pnlMail.add(tfObjet, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCorps ----
              lbCorps.setText("Corps");
              lbCorps.setName("lbCorps");
              pnlMail.add(lbCorps, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ======== scrollPane1 ========
              {
                scrollPane1.setName("scrollPane1");
                
                // ---- taCorpMail ----
                taCorpMail.setName("taCorpMail");
                scrollPane1.setViewportView(taCorpMail);
              }
              pnlMail.add(scrollPane1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGeneral.add(pnlMail, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          tpOnglet.addTab("G\u00e9n\u00e9ral", pnlGeneral);
          
          // ======== pnlDestinataire ========
          {
            pnlDestinataire.setName("pnlDestinataire");
            pnlDestinataire.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDestinataire.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlDestinataire.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlDestinataire.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDestinataire.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== scrollPane2 ========
            {
              scrollPane2.setName("scrollPane2");
              
              // ---- nRiTable1 ----
              nRiTable1.setModel(new DefaultTableModel(
                  new Object[][] { { "david.goodenought@free.fr", "CC", "M Goodenought David (0123456789)" },
                      { "michel@free.fr", "A", "M Michel robert" }, { null, null, null }, { null, null, null }, { null, null, null },
                      { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null },
                      { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, { null, null, null }, },
                  new String[] { "Mail", "Type destinataire", "Contact" }));
              {
                TableColumnModel cm = nRiTable1.getColumnModel();
                cm.getColumn(1).setMinWidth(110);
                cm.getColumn(1).setMaxWidth(110);
                cm.getColumn(1).setPreferredWidth(110);
              }
              nRiTable1.setName("nRiTable1");
              scrollPane2.setViewportView(nRiTable1);
            }
            pnlDestinataire.add(scrollPane2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          tpOnglet.addTab("Destinataires (2)", pnlDestinataire);
          
          // ======== pnlPieceJointe ========
          {
            pnlPieceJointe.setName("pnlPieceJointe");
            pnlPieceJointe.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPieceJointe.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlPieceJointe.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPieceJointe.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlPieceJointe.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== scrollPane3 ========
            {
              scrollPane3.setName("scrollPane3");
              
              // ---- nRiTable2 ----
              nRiTable2
                  .setModel(new DefaultTableModel(
                      new Object[][] { { "Nom_document_test", ".pdf", "oui", "0123456789" }, { null, null, null, null },
                          { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
                          { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
                          { null, null, null, null }, { null, null, null, null }, { null, null, null, null }, { null, null, null, null },
                          { null, null, null, null }, },
                      new String[] { "Nom", "Type", "Visualisable", "Identifiant document stock\u00e9" }));
              {
                TableColumnModel cm = nRiTable2.getColumnModel();
                cm.getColumn(1).setMinWidth(50);
                cm.getColumn(1).setMaxWidth(50);
                cm.getColumn(1).setPreferredWidth(50);
                cm.getColumn(2).setMinWidth(100);
                cm.getColumn(2).setMaxWidth(100);
                cm.getColumn(2).setPreferredWidth(100);
                cm.getColumn(3).setMinWidth(175);
                cm.getColumn(3).setMaxWidth(175);
                cm.getColumn(3).setPreferredWidth(175);
              }
              nRiTable2.setName("nRiTable2");
              scrollPane3.setViewportView(nRiTable2);
            }
            pnlPieceJointe.add(scrollPane3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          tpOnglet.addTab("Pi\u00e8ces jointes (1)", pnlPieceJointe);
        }
        pnlContenu.add(tpOnglet, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre riBandeau;
  private SNPanelEcranRPG pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private JTabbedPane tpOnglet;
  private SNPanel pnlGeneral;
  private SNPanelEcranRPG pnlInformation;
  private SNPanelContenu pnlStatut;
  private SNLabelChamp lbStatut;
  private XRiComboBox xRiComboBox1;
  private SNLabelChamp lbType;
  private XRiComboBox xRiComboBox2;
  private SNLabelChamp lbIdentifiantDocument;
  private SNTexte tfIdentifiantDocument;
  private SNLabelChamp sNLabelChamp1;
  private XRiTextField xRiTextField1;
  private SNPanelContenu pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissemet;
  private SNLabelChamp lbDateCreation;
  private XRiCalendrier xRiCalendrier1;
  private SNLabelChamp lbDateEnvoi;
  private XRiCalendrier xRiCalendrier2;
  private SNPanelContenu pnlMail;
  private SNMessage lbMessage;
  private SNLabelChamp lbObjet;
  private SNTexte tfObjet;
  private SNLabelChamp lbCorps;
  private JScrollPane scrollPane1;
  private JTextArea taCorpMail;
  private SNPanelContenu pnlDestinataire;
  private JScrollPane scrollPane2;
  private NRiTable nRiTable1;
  private SNPanelContenu pnlPieceJointe;
  private JScrollPane scrollPane3;
  private NRiTable nRiTable2;
  private SNBarreBouton snBarreBouton;
  
  // JFormDesigner - End of variables declaration //GEN-END:variables
  @Override
  public void setData() {
    // XXX Auto-generated method stub
    
  }
  
  @Override
  public void getData() {
    // XXX Auto-generated method stub
    
  }
}
