/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc10880;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;

import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Vue de l'écran liste de la consultation de documents de ventes.
 */
public class VueExportHistoriqueStock extends JPanel {
  // Constantes
  /**
   * Constructeur.
   */
  public VueExportHistoriqueStock() {
    super();
  }
  
  // -- Méthodes protégées
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
  }
  
  private void traiterClicBoutonDocument(SNBouton pSNBouton) {
  }
  
  private void traiterClicBoutonLigne(SNBouton pSNBouton) {
  }
  
  private void keyFleches(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
  }
  
  private void scpListeLigneStateChanged(ChangeEvent e) {
  }
  
  private void scpListeDocumentsStateChanged(ChangeEvent e) {
  }
  
  private void tfNumeroDocumentFocusLost(FocusEvent e) {
  }
  
  private void cbMagasinItemStateChanged(ItemEvent e) {
  }
  
  private void tfReferenceClientDocumentFocusLost(FocusEvent e) {
  }
  
  private void tblListeDocumentsMouseClicked(MouseEvent e) {
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
  }
  
  private void cbTypeDocumentItemStateChanged(ItemEvent e) {
  }
  
  private void rbRechercheDocumentItemStateChanged(ItemEvent e) {
  }
  
  private void rbRechercheLigneItemStateChanged(ItemEvent e) {
  }
  
  private void tfNumeroLigneFocusLost(FocusEvent e) {
  }
  
  private void tfReferenceClientLigneFocusLost(FocusEvent e) {
  }
  
  private void snEtablissementLigneValueChanged(SNComposantEvent e) {
  }
  
  private void cbMagasinLigneItemStateChanged(ItemEvent e) {
  }
  
  private void snPlageDateLigneValueChanged(SNComposantEvent e) {
  }
  
  private void snArticleStandardLigneValueChanged(SNComposantEvent e) {
  }
  
  private void snArticleStandardDocumentValueChanged(SNComposantEvent e) {
  }
  
  private void snFournisseurDocumentValueChanged(SNComposantEvent e) {
  }
  
  private void snFournisseurLigneValueChanged(SNComposantEvent e) {
  }
  
  private void cbTypeDocumentLigneItemStateChanged(ItemEvent e) {
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    tabbedPane1 = new JTabbedPane();
    sNPanel1 = new SNPanel();
    sNPanelTitre1 = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissementDocument = new SNEtablissement();
    lbEtablissement2 = new SNLabelChamp();
    snPlageDateDocument = new SNPlageDate();
    sNPanelTitre2 = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    cbMagasinDocument = new SNComboBox();
    lbEtablissement3 = new SNLabelChamp();
    snPlageDateDocument2 = new SNPlageDate();
    lbFamilleDebut = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFamilleFin = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    lbSousFamilleDebut = new SNLabelChamp();
    snSousFamilleDebut = new SNSousFamille();
    lbSousFamilleFin = new SNLabelChamp();
    snSousFamilleFin = new SNSousFamille();
    lbArticleDebut = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticleFin = new SNLabelChamp();
    snArticleFin = new SNArticle();
    sNPanelTitre3 = new SNPanelTitre();
    EDTDES = new XRiCheckBox();
    EDTDES2 = new XRiCheckBox();
    pnChamps = new SNPanel();
    pnlColonne = new SNPanel();
    sNPanelContenu1 = new SNPanelContenu();
    scrollPane1 = new JScrollPane();
    tblListeDocument = new NRiTable();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setPreferredSize(new Dimension(1205, 655));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.PAGE_END);
    
    // ======== tabbedPane1 ========
    {
      tabbedPane1.setFont(new Font("sansserif", Font.PLAIN, 14));
      tabbedPane1.setName("tabbedPane1");
      
      // ======== sNPanel1 ========
      {
        sNPanel1.setName("sNPanel1");
        sNPanel1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== sNPanelTitre1 ========
        {
          sNPanelTitre1.setTitre("Etablissement");
          sNPanelTitre1.setName("sNPanelTitre1");
          sNPanelTitre1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          sNPanelTitre1.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissementDocument ----
          snEtablissementDocument.setMaximumSize(new Dimension(500, 30));
          snEtablissementDocument.setMinimumSize(new Dimension(500, 30));
          snEtablissementDocument.setPreferredSize(new Dimension(500, 30));
          snEtablissementDocument.setName("snEtablissementDocument");
          snEtablissementDocument.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          sNPanelTitre1.add(snEtablissementDocument, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtablissement2 ----
          lbEtablissement2.setText("P\u00e9riode en cours");
          lbEtablissement2.setName("lbEtablissement2");
          sNPanelTitre1.add(lbEtablissement2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snPlageDateDocument ----
          snPlageDateDocument.setName("snPlageDateDocument");
          sNPanelTitre1.add(snPlageDateDocument, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanel1.add(sNPanelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanelTitre2 ========
        {
          sNPanelTitre2.setTitre("Crit\u00e8res de s\u00e9lection");
          sNPanelTitre2.setName("sNPanelTitre2");
          sNPanelTitre2.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelTitre2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanelTitre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          sNPanelTitre2.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- cbMagasinDocument ----
          cbMagasinDocument.setName("cbMagasinDocument");
          cbMagasinDocument.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              cbMagasinItemStateChanged(e);
            }
          });
          sNPanelTitre2.add(cbMagasinDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbEtablissement3 ----
          lbEtablissement3.setText("Plage de dates");
          lbEtablissement3.setName("lbEtablissement3");
          sNPanelTitre2.add(lbEtablissement3, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snPlageDateDocument2 ----
          snPlageDateDocument2.setName("snPlageDateDocument2");
          sNPanelTitre2.add(snPlageDateDocument2, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbFamilleDebut ----
          lbFamilleDebut.setText("Plage de familles");
          lbFamilleDebut.setMinimumSize(new Dimension(185, 30));
          lbFamilleDebut.setPreferredSize(new Dimension(185, 30));
          lbFamilleDebut.setMaximumSize(new Dimension(185, 30));
          lbFamilleDebut.setName("lbFamilleDebut");
          sNPanelTitre2.add(lbFamilleDebut, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snFamilleDebut ----
          snFamilleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
          snFamilleDebut.setName("snFamilleDebut");
          sNPanelTitre2.add(snFamilleDebut, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbFamilleFin ----
          lbFamilleFin.setText("\u00e0");
          lbFamilleFin.setMinimumSize(new Dimension(60, 30));
          lbFamilleFin.setPreferredSize(new Dimension(60, 30));
          lbFamilleFin.setMaximumSize(new Dimension(60, 30));
          lbFamilleFin.setName("lbFamilleFin");
          sNPanelTitre2.add(lbFamilleFin, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snFamilleFin ----
          snFamilleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
          snFamilleFin.setName("snFamilleFin");
          sNPanelTitre2.add(snFamilleFin, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbSousFamilleDebut ----
          lbSousFamilleDebut.setText("Plage de sous-familles");
          lbSousFamilleDebut.setMinimumSize(new Dimension(185, 30));
          lbSousFamilleDebut.setPreferredSize(new Dimension(185, 30));
          lbSousFamilleDebut.setMaximumSize(new Dimension(185, 30));
          lbSousFamilleDebut.setName("lbSousFamilleDebut");
          sNPanelTitre2.add(lbSousFamilleDebut, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snSousFamilleDebut ----
          snSousFamilleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
          snSousFamilleDebut.setName("snSousFamilleDebut");
          sNPanelTitre2.add(snSousFamilleDebut, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbSousFamilleFin ----
          lbSousFamilleFin.setText("\u00e0");
          lbSousFamilleFin.setMinimumSize(new Dimension(60, 30));
          lbSousFamilleFin.setPreferredSize(new Dimension(60, 30));
          lbSousFamilleFin.setMaximumSize(new Dimension(60, 30));
          lbSousFamilleFin.setName("lbSousFamilleFin");
          sNPanelTitre2.add(lbSousFamilleFin, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snSousFamilleFin ----
          snSousFamilleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
          snSousFamilleFin.setName("snSousFamilleFin");
          sNPanelTitre2.add(snSousFamilleFin, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbArticleDebut ----
          lbArticleDebut.setText("Plage d'articles");
          lbArticleDebut.setMinimumSize(new Dimension(185, 30));
          lbArticleDebut.setPreferredSize(new Dimension(185, 30));
          lbArticleDebut.setMaximumSize(new Dimension(185, 30));
          lbArticleDebut.setName("lbArticleDebut");
          sNPanelTitre2.add(lbArticleDebut, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticleDebut ----
          snArticleDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
          snArticleDebut.setEnabled(false);
          snArticleDebut.setMaximumSize(new Dimension(350, 30));
          snArticleDebut.setMinimumSize(new Dimension(350, 30));
          snArticleDebut.setPreferredSize(new Dimension(350, 30));
          snArticleDebut.setName("snArticleDebut");
          sNPanelTitre2.add(snArticleDebut, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbArticleFin ----
          lbArticleFin.setText("\u00e0");
          lbArticleFin.setMinimumSize(new Dimension(60, 30));
          lbArticleFin.setPreferredSize(new Dimension(60, 30));
          lbArticleFin.setMaximumSize(new Dimension(60, 30));
          lbArticleFin.setName("lbArticleFin");
          sNPanelTitre2.add(lbArticleFin, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snArticleFin ----
          snArticleFin.setFont(new Font("sansserif", Font.PLAIN, 14));
          snArticleFin.setEnabled(false);
          snArticleFin.setMinimumSize(new Dimension(350, 30));
          snArticleFin.setMaximumSize(new Dimension(350, 30));
          snArticleFin.setPreferredSize(new Dimension(350, 30));
          snArticleFin.setName("snArticleFin");
          sNPanelTitre2.add(snArticleFin, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
        }
        sNPanel1.add(sNPanelTitre2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanelTitre3 ========
        {
          sNPanelTitre3.setTitre("Options");
          sNPanelTitre3.setName("sNPanelTitre3");
          sNPanelTitre3.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelTitre3.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) sNPanelTitre3.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre3.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanelTitre3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- EDTDES ----
          EDTDES.setText("Exportation des articles d\u00e9sactiv\u00e9s");
          EDTDES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTDES.setFont(new Font("sansserif", Font.PLAIN, 14));
          EDTDES.setPreferredSize(new Dimension(185, 30));
          EDTDES.setMinimumSize(new Dimension(185, 30));
          EDTDES.setName("EDTDES");
          sNPanelTitre3.add(EDTDES, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- EDTDES2 ----
          EDTDES2.setText("S\u00e9lection des articles non g\u00e9r\u00e9s sur la p\u00e9riode");
          EDTDES2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTDES2.setFont(new Font("sansserif", Font.PLAIN, 14));
          EDTDES2.setPreferredSize(new Dimension(185, 30));
          EDTDES2.setMinimumSize(new Dimension(185, 30));
          EDTDES2.setName("EDTDES2");
          sNPanelTitre3.add(EDTDES2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanel1.add(sNPanelTitre3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      tabbedPane1.addTab("Crit\u00e8res de s\u00e9lection", sNPanel1);
      
      // ======== pnChamps ========
      {
        pnChamps.setName("pnChamps");
        pnChamps.setLayout(new GridBagLayout());
        ((GridBagLayout) pnChamps.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnChamps.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnChamps.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnChamps.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== pnlColonne ========
        {
          pnlColonne.setName("pnlColonne");
          pnlColonne.setLayout(new GridLayout());
          
          // ======== sNPanelContenu1 ========
          {
            sNPanelContenu1.setName("sNPanelContenu1");
            sNPanelContenu1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== scrollPane1 ========
            {
              scrollPane1.setBackground(Color.white);
              scrollPane1.setFont(new Font("sansserif", Font.PLAIN, 14));
              scrollPane1.setName("scrollPane1");
              
              // ---- tblListeDocument ----
              tblListeDocument.setShowVerticalLines(true);
              tblListeDocument.setShowHorizontalLines(true);
              tblListeDocument.setBackground(Color.white);
              tblListeDocument.setRowHeight(20);
              tblListeDocument.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
              tblListeDocument.setAutoCreateRowSorter(true);
              tblListeDocument.setSelectionBackground(new Color(57, 105, 138));
              tblListeDocument.setGridColor(new Color(204, 204, 204));
              tblListeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
              tblListeDocument.setName("tblListeDocument");
              tblListeDocument.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  tblListeDocumentsMouseClicked(e);
                }
              });
              scrollPane1.setViewportView(tblListeDocument);
            }
            sNPanelContenu1.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlColonne.add(sNPanelContenu1);
        }
        pnChamps.add(pnlColonne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      tabbedPane1.addTab("Donn\u00e9es \u00e0 exporter", pnChamps);
    }
    add(tabbedPane1, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private JTabbedPane tabbedPane1;
  private SNPanel sNPanel1;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissementDocument;
  private SNLabelChamp lbEtablissement2;
  private SNPlageDate snPlageDateDocument;
  private SNPanelTitre sNPanelTitre2;
  private SNLabelChamp lbMagasin;
  private SNComboBox cbMagasinDocument;
  private SNLabelChamp lbEtablissement3;
  private SNPlageDate snPlageDateDocument2;
  private SNLabelChamp lbFamilleDebut;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFamilleFin;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbSousFamilleDebut;
  private SNSousFamille snSousFamilleDebut;
  private SNLabelChamp lbSousFamilleFin;
  private SNSousFamille snSousFamilleFin;
  private SNLabelChamp lbArticleDebut;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbArticleFin;
  private SNArticle snArticleFin;
  private SNPanelTitre sNPanelTitre3;
  private XRiCheckBox EDTDES;
  private XRiCheckBox EDTDES2;
  private SNPanel pnChamps;
  private SNPanel pnlColonne;
  private SNPanelContenu sNPanelContenu1;
  private JScrollPane scrollPane1;
  private NRiTable tblListeDocument;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
