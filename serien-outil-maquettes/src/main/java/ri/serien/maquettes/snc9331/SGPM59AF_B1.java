/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9331;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.EnumCategorieDate;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SGPM59AF_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private Message LOCTP = null;
  private ArrayList<EnumCategorieDate> listeEnumCategorieDate = new ArrayList<EnumCategorieDate>();
  
  public SGPM59AF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Ajout
    initDiverses();
    snFamilleDebut.lierComposantFin(snFamilleFin);
    snSousFamilleDebut.lierComposantFin(snSousFamilleFin);
    WPQT.setValeursSelection("1", " ");
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Chargement de la liste des valeurs de l'EnumDatePreEnregister
    for (int i = 0; i < EnumCategorieDate.values().length; i++) {
      listeEnumCategorieDate.add(EnumCategorieDate.values()[i]);
    }
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfPeriodeEnCorus.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    // Gestion de LOCTP
    pnlMessage.setVisible(!lexique.HostFieldGetData("LOCTP").trim().isEmpty());
    
    lbLOCTP.setMessage(LOCTP);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialise l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialisation des composants
    chargerComposantMagasin();
    chargerListeComposantsMagasin();
    chargerFournisseur();
    chargerArticle();
    
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin0.renseignerChampRPG(lexique, "MA01");
    snMagasin1.renseignerChampRPG(lexique, "MA02");
    snMagasin2.renseignerChampRPG(lexique, "MA03");
    snMagasin3.renseignerChampRPG(lexique, "MA04");
    snMagasin4.renseignerChampRPG(lexique, "MA05");
    snMagasin5.renseignerChampRPG(lexique, "MA06");
    snMagasin6.renseignerChampRPG(lexique, "MA07");
    // Famille
    if (snFamilleDebut.isVisible() && snFamilleFin.isVisible()) {
      if (snFamilleDebut.getIdSelection() == null && snFamilleFin.getIdSelection() == null) {
        lexique.HostFieldPutData("WTOU1", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOU1", 0, "");
        snFamilleDebut.renseignerChampRPG(lexique, "FAMDEB");
        snFamilleFin.renseignerChampRPG(lexique, "FAMFIN");
      }
    }
    // Sous Famille
    if (snSousFamilleDebut.isVisible() && snSousFamilleFin.isVisible()) {
      if (snSousFamilleDebut.getIdSelection() == null && snSousFamilleFin.getIdSelection() == null) {
        lexique.HostFieldPutData("WTSFA", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTSFA", 0, "");
        snSousFamilleDebut.renseignerChampRPG(lexique, "SFADEB");
        snSousFamilleFin.renseignerChampRPG(lexique, "SFAFIN");
      }
    }
    // Fournisseur
    if (snFournisseurDebut.isVisible() && snFournisseurFin.isVisible()) {
      if (snFournisseurDebut.getSelection() == null & snFournisseurFin.getSelection() == null) {
        lexique.HostFieldPutData("WTOU2", 0, "**");
      }
      else {
        lexique.HostFieldPutData("WTOU2", 0, "");
        snFournisseurDebut.renseignerChampRPG(lexique, "FRSDEB");
        snFournisseurFin.renseignerChampRPG(lexique, "FRSFIN");
      }
    }
    // Article
    if (snArticleDebut.getSelection() == null && snArticleFin.getSelection() == null) {
      lexique.HostFieldPutData("WTOU2", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOU2", 0, "");
      snArticleDebut.renseignerChampRPG(lexique, "ARTDEB");
      snArticleFin.renseignerChampRPG(lexique, "ARTFIN");
    }
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge le composant magasin
   */
  private void chargerComposantMagasin() {
    snMagasin0.setSession(getSession());
    snMagasin0.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin0.setTousAutorise(true);
    snMagasin0.charger(true);
    snMagasin0.setSelectionParChampRPG(lexique, "MA01");
  }
  
  /**
   * Initialise et charge les composants magasin.
   * Permet d'ajuster le nombre de composants visibles en fonction du nombre de choix de magasins dans la comboBox.
   * Permet de ne rendre visibles les comboBox seulement si le snMagasin1 ne contient pas "Tous".
   * 
   * snMagasin1-6 : "Tous" interdit, "aucun" autorisé
   * Gestion de l'affichage des libellés en fonction de la visibilité des composants.
   */
  private void chargerListeComposantsMagasin() {
    // On charge toujours le composant snMagasin1 en visible
    snMagasin1.setSession(getSession());
    snMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin1.setTousAutorise(false);
    snMagasin1.setAucunAutorise(true);
    snMagasin1.charger(false);
    snMagasin1.setSelectionParChampRPG(lexique, "MA01");
    
    // Gestion de l'affichage et du chargement des magasins suivant le nombre de magasins présent.
    // (On enleve le 1er magasin car le composant SNMagasin1 sera toujours visible).
    
    // On liste les champs snMagasin disponibles et les champs RPG associés.
    List<SNMagasin> listeComposant = Arrays.asList(snMagasin2, snMagasin3, snMagasin4, snMagasin5, snMagasin6);
    List<String> listeChamp = Arrays.asList("MA03", "MA04", "MA05", "MA06", "MA07");
    
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable() - 1;
    
    // On vérifie que le nombre de magasins n'est pas supérieur au nombre de composant magasin présent.
    // On charge un nombre de composants égal au nombre de choix possibles ou au nombre maximal de composants disponibles.
    if (nombreMagasin > listeComposant.size()) {
      nombreMagasin = listeComposant.size();
    }
    for (int i = 0; i < listeComposant.size(); i++) {
      listeComposant.get(i).setVisible(false);
    }
    
    // On charge les composants snMagasin.
    for (int i = 0; i < nombreMagasin; i++) {
      listeComposant.get(i).setSession(getSession());
      listeComposant.get(i).setIdEtablissement(snEtablissement.getIdSelection());
      // On interdit "Tous" et on autorise "Aucun" dans tous les composants snMagasin à part snMagasin1.
      listeComposant.get(i).setTousAutorise(false);
      listeComposant.get(i).setAucunAutorise(true);
      listeComposant.get(i).charger(false);
      listeComposant.get(i).setSelectionParChampRPG(lexique, listeChamp.get(i));
      // On rend le composant snMagasin2 visibles si le composant snMagasin1 contient autre chose que "Tous".
      if (i == 0) {
        listeComposant.get(i).setVisible(!(snMagasin1.getIdSelection() == null));
      }
      // On rend les composants suivants visibles si le composant précédent contient autre chose que "Aucun" (null)
      else {
        listeComposant.get(i).setVisible(!(listeComposant.get(i - 1).getIdSelection() == null));
      }
    }
    
    // On affiche les libellés en fonction de la visibilité du composant associé.
    lbMagasin2.setVisible(snMagasin2.isVisible());
    lbMagasin3.setVisible(snMagasin3.isVisible());
    lbMagasin4.setVisible(snMagasin4.isVisible());
    lbMagasin5.setVisible(snMagasin5.isVisible());
    lbMagasin6.setVisible(snMagasin6.isVisible());
  }
  
  /**
   * Charge les famille
   */
  private void chargerFamille() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "FAMDEB");
    
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "FAMFIN");
  }
  
  /**
   * Charge les sous famille
   */
  private void chargerSousFamille() {
    snSousFamilleDebut.setSession(getSession());
    snSousFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleDebut.setTousAutorise(true);
    snSousFamilleDebut.charger(false);
    snSousFamilleDebut.setSelectionParChampRPG(lexique, "SFADEB");
    
    snSousFamilleFin.setSession(getSession());
    snSousFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleFin.setTousAutorise(true);
    snSousFamilleFin.charger(false);
    snSousFamilleFin.setSelectionParChampRPG(lexique, "SFAFIN");
  }
  
  /*
   * Charge les fournisseurs
   */
  private void chargerFournisseur() {
    snFournisseurDebut.setSession(getSession());
    snFournisseurDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseurDebut.charger(false);
    snFournisseurDebut.setSelectionParChampRPG(lexique, "FRSDEB");
    
    snFournisseurFin.setSession(getSession());
    snFournisseurFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseurFin.charger(false);
    snFournisseurFin.setSelectionParChampRPG(lexique, "FRSFIN");
  }
  
  /**
   * Charge les article
   */
  private void chargerArticle() {
    snArticleDebut.setSession(getSession());
    snArticleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleDebut.charger(false);
    snArticleDebut.setSelectionParChampRPG(lexique, "ARTDEB");
    
    snArticleFin.setSession(getSession());
    snArticleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleFin.charger(false);
    snArticleFin.setSelectionParChampRPG(lexique, "ARTFIN");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      chargerListeComposantsMagasin();
      chargerComposantMagasin();
      chargerFamille();
      chargerSousFamille();
      chargerFournisseur();
      chargerArticle();
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // On observe le changement de valeur du composant snMagasin1.
  // On affiche ou non le composants snMagasin2 si snMagasin1 différent de "Aucun" (null).
  // On affiche le composant s'il est censé pouvoir être affiché.
  private void snMagasin1ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 2 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 2) {
      snMagasin2.setVisible(!(snMagasin1.getIdSelection() == null));
      lbMagasin2.setVisible(snMagasin2.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin1.getIdSelection() == null) {
      snMagasin2.setSelection(null);
    }
  }
  
  // On effectue la même observation pour chaque composant snMagasin.
  // On affiche le composant suivant selon les même conditions.
  private void snMagasin2ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 3 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 3) {
      snMagasin3.setVisible(!(snMagasin2.getIdSelection() == null));
      lbMagasin3.setVisible(snMagasin3.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin2.getIdSelection() == null) {
      snMagasin3.setSelection(null);
    }
  }
  
  private void snMagasin3ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 4 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 4) {
      snMagasin4.setVisible(!(snMagasin3.getIdSelection() == null));
      lbMagasin4.setVisible(snMagasin4.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin3.getIdSelection() == null) {
      snMagasin4.setSelection(null);
    }
  }
  
  private void snMagasin4ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 5 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 5) {
      snMagasin5.setVisible(!(snMagasin4.getIdSelection() == null));
      lbMagasin5.setVisible(snMagasin5.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin4.getIdSelection() == null) {
      snMagasin5.setSelection(null);
    }
  }
  
  private void snMagasin5ValueChanged(SNComposantEvent e) {
    // On compte le nombre d'éléments disponibles dans les combobox snMagasin pour ajuster le nombre de composants à afficher.
    int nombreMagasin = snMagasin1.getNombreObjetMetierSelectionnnable();
    // S'il y a au moins 6 magasins disponibles et si le composant précédent ne contient pas "Aucun" on affiche le composant suivant.
    if (nombreMagasin >= 6) {
      snMagasin6.setVisible(!(snMagasin5.getIdSelection() == null));
      lbMagasin6.setVisible(snMagasin6.isVisible());
    }
    // Si le composant retourne à une valeur null, le suivant repasse également à null
    if (snMagasin5.getIdSelection() == null) {
      snMagasin6.setSelection(null);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereSelectionArticle = new SNPanelTitre();
    lbFamilleDebut = new SNLabelChamp();
    snFamilleDebut = new SNFamille();
    lbFamilleFin = new SNLabelChamp();
    pnlSelectionDeBon = new SNPanel();
    snFamilleFin = new SNFamille();
    lbSousFamilleDebut = new SNLabelChamp();
    snSousFamilleDebut = new SNSousFamille();
    lbSousFamilleFin = new SNLabelChamp();
    snSousFamilleFin = new SNSousFamille();
    lbFournisseurDebut = new SNLabelChamp();
    snFournisseurDebut = new SNFournisseur();
    lbFournisseurFin = new SNLabelChamp();
    snFournisseurFin = new SNFournisseur();
    lbArticleDebut = new SNLabelChamp();
    snArticleDebut = new SNArticle();
    lbArticleFin = new SNLabelChamp();
    snArticleFin = new SNArticle();
    lbGestionnaireProduit = new SNLabelChamp();
    pnlNumeroDeCommande = new SNPanel();
    WCGP = new XRiTextField();
    pnlOptions = new SNPanelTitre();
    WPQT = new XRiCheckBox();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissementEnCours = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCorus = new SNTexte();
    pnlMagasinApprovisionnement = new SNPanelTitre();
    lbMagasin7 = new SNLabelChamp();
    snMagasin0 = new SNMagasin();
    pnlMagasinsRegroupement = new SNPanelTitre();
    lbMagasin1 = new SNLabelChamp();
    snMagasin1 = new SNMagasin();
    lbMagasin2 = new SNLabelChamp();
    snMagasin2 = new SNMagasin();
    lbMagasin3 = new SNLabelChamp();
    snMagasin3 = new SNMagasin();
    lbMagasin4 = new SNLabelChamp();
    snMagasin4 = new SNMagasin();
    lbMagasin5 = new SNLabelChamp();
    snMagasin5 = new SNMagasin();
    lbMagasin6 = new SNLabelChamp();
    snMagasin6 = new SNMagasin();
    snBarreBouton = new SNBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(960, 640));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("LOCTP");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlCritereSelectionArticle ========
          {
            pnlCritereSelectionArticle.setTitre("Crit\u00e8res de s\u00e9lection articles");
            pnlCritereSelectionArticle.setName("pnlCritereSelectionArticle");
            pnlCritereSelectionArticle.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereSelectionArticle.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbFamilleDebut ----
            lbFamilleDebut.setText("Famille de d\u00e9but");
            lbFamilleDebut.setMaximumSize(new Dimension(175, 30));
            lbFamilleDebut.setMinimumSize(new Dimension(175, 30));
            lbFamilleDebut.setPreferredSize(new Dimension(175, 30));
            lbFamilleDebut.setName("lbFamilleDebut");
            pnlCritereSelectionArticle.add(lbFamilleDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFamilleDebut ----
            snFamilleDebut.setName("snFamilleDebut");
            pnlCritereSelectionArticle.add(snFamilleDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFamilleFin ----
            lbFamilleFin.setText("Famille de fin");
            lbFamilleFin.setMaximumSize(new Dimension(175, 30));
            lbFamilleFin.setMinimumSize(new Dimension(175, 30));
            lbFamilleFin.setPreferredSize(new Dimension(175, 30));
            lbFamilleFin.setName("lbFamilleFin");
            pnlCritereSelectionArticle.add(lbFamilleFin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlSelectionDeBon ========
            {
              pnlSelectionDeBon.setName("pnlSelectionDeBon");
              pnlSelectionDeBon.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSelectionDeBon.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- snFamilleFin ----
              snFamilleFin.setName("snFamilleFin");
              pnlSelectionDeBon.add(snFamilleFin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelectionArticle.add(pnlSelectionDeBon, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousFamilleDebut ----
            lbSousFamilleDebut.setText("Sous-famille de d\u00e9but");
            lbSousFamilleDebut.setMaximumSize(new Dimension(175, 30));
            lbSousFamilleDebut.setMinimumSize(new Dimension(175, 30));
            lbSousFamilleDebut.setPreferredSize(new Dimension(175, 30));
            lbSousFamilleDebut.setName("lbSousFamilleDebut");
            pnlCritereSelectionArticle.add(lbSousFamilleDebut, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSousFamilleDebut ----
            snSousFamilleDebut.setName("snSousFamilleDebut");
            pnlCritereSelectionArticle.add(snSousFamilleDebut, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousFamilleFin ----
            lbSousFamilleFin.setText("Sous-famille de fin");
            lbSousFamilleFin.setMaximumSize(new Dimension(175, 30));
            lbSousFamilleFin.setMinimumSize(new Dimension(175, 30));
            lbSousFamilleFin.setPreferredSize(new Dimension(175, 30));
            lbSousFamilleFin.setName("lbSousFamilleFin");
            pnlCritereSelectionArticle.add(lbSousFamilleFin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snSousFamilleFin ----
            snSousFamilleFin.setName("snSousFamilleFin");
            pnlCritereSelectionArticle.add(snSousFamilleFin, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseurDebut ----
            lbFournisseurDebut.setText("Fournisseur d\u00e9but ");
            lbFournisseurDebut.setMinimumSize(new Dimension(185, 30));
            lbFournisseurDebut.setPreferredSize(new Dimension(185, 30));
            lbFournisseurDebut.setMaximumSize(new Dimension(185, 30));
            lbFournisseurDebut.setName("lbFournisseurDebut");
            pnlCritereSelectionArticle.add(lbFournisseurDebut, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseurDebut ----
            snFournisseurDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
            snFournisseurDebut.setEnabled(false);
            snFournisseurDebut.setName("snFournisseurDebut");
            pnlCritereSelectionArticle.add(snFournisseurDebut, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseurFin ----
            lbFournisseurFin.setText("Fournisseur fin");
            lbFournisseurFin.setMinimumSize(new Dimension(185, 30));
            lbFournisseurFin.setPreferredSize(new Dimension(185, 30));
            lbFournisseurFin.setMaximumSize(new Dimension(185, 30));
            lbFournisseurFin.setName("lbFournisseurFin");
            pnlCritereSelectionArticle.add(lbFournisseurFin, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseurFin ----
            snFournisseurFin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snFournisseurFin.setEnabled(false);
            snFournisseurFin.setName("snFournisseurFin");
            pnlCritereSelectionArticle.add(snFournisseurFin, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticleDebut ----
            lbArticleDebut.setText("Article de d\u00e9but");
            lbArticleDebut.setMinimumSize(new Dimension(175, 30));
            lbArticleDebut.setMaximumSize(new Dimension(175, 30));
            lbArticleDebut.setPreferredSize(new Dimension(175, 30));
            lbArticleDebut.setName("lbArticleDebut");
            pnlCritereSelectionArticle.add(lbArticleDebut, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticleDebut ----
            snArticleDebut.setName("snArticleDebut");
            pnlCritereSelectionArticle.add(snArticleDebut, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticleFin ----
            lbArticleFin.setText("Article de fin");
            lbArticleFin.setMinimumSize(new Dimension(175, 30));
            lbArticleFin.setMaximumSize(new Dimension(175, 30));
            lbArticleFin.setPreferredSize(new Dimension(175, 30));
            lbArticleFin.setName("lbArticleFin");
            pnlCritereSelectionArticle.add(lbArticleFin, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticleFin ----
            snArticleFin.setName("snArticleFin");
            pnlCritereSelectionArticle.add(snArticleFin, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbGestionnaireProduit ----
            lbGestionnaireProduit.setText("Gestionnaire produit");
            lbGestionnaireProduit.setMinimumSize(new Dimension(185, 30));
            lbGestionnaireProduit.setPreferredSize(new Dimension(185, 30));
            lbGestionnaireProduit.setMaximumSize(new Dimension(185, 30));
            lbGestionnaireProduit.setName("lbGestionnaireProduit");
            pnlCritereSelectionArticle.add(lbGestionnaireProduit, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlNumeroDeCommande ========
            {
              pnlNumeroDeCommande.setName("pnlNumeroDeCommande");
              pnlNumeroDeCommande.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlNumeroDeCommande.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WCGP ----
              WCGP.setComponentPopupMenu(pmBTD);
              WCGP.setFont(new Font("sansserif", Font.PLAIN, 14));
              WCGP.setMaximumSize(new Dimension(24, 30));
              WCGP.setMinimumSize(new Dimension(24, 30));
              WCGP.setPreferredSize(new Dimension(24, 30));
              WCGP.setName("WCGP");
              pnlNumeroDeCommande.add(WCGP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereSelectionArticle.add(pnlNumeroDeCommande, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereSelectionArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlOptions ========
          {
            pnlOptions.setTitre("Options g\u00e9n\u00e9ration");
            pnlOptions.setName("pnlOptions");
            pnlOptions.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- WPQT ----
            WPQT.setText("Proposition quantit\u00e9");
            WPQT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WPQT.setFont(new Font("sansserif", Font.PLAIN, 14));
            WPQT.setMinimumSize(new Dimension(151, 30));
            WPQT.setPreferredSize(new Dimension(151, 30));
            WPQT.setName("WPQT");
            pnlOptions.add(WPQT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlGauche.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementEnCours ----
            lbEtablissementEnCours.setText("Etablissement en cours");
            lbEtablissementEnCours.setName("lbEtablissementEnCours");
            pnlEtablissement.add(lbEtablissementEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setEnabled(false);
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCorus ----
            tfPeriodeEnCorus.setText("@WENCX@");
            tfPeriodeEnCorus.setEditable(false);
            tfPeriodeEnCorus.setEnabled(false);
            tfPeriodeEnCorus.setMinimumSize(new Dimension(260, 30));
            tfPeriodeEnCorus.setPreferredSize(new Dimension(260, 30));
            tfPeriodeEnCorus.setName("tfPeriodeEnCorus");
            pnlEtablissement.add(tfPeriodeEnCorus, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlMagasinApprovisionnement ========
          {
            pnlMagasinApprovisionnement.setTitre("R\u00e9approvisionnement");
            pnlMagasinApprovisionnement.setName("pnlMagasinApprovisionnement");
            pnlMagasinApprovisionnement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlMagasinApprovisionnement.getLayout()).columnWidths = new int[] { 55, 0, 0 };
            ((GridBagLayout) pnlMagasinApprovisionnement.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlMagasinApprovisionnement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlMagasinApprovisionnement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbMagasin7 ----
            lbMagasin7.setText("Magasin ");
            lbMagasin7.setPreferredSize(new Dimension(175, 30));
            lbMagasin7.setMinimumSize(new Dimension(175, 30));
            lbMagasin7.setMaximumSize(new Dimension(175, 30));
            lbMagasin7.setName("lbMagasin7");
            pnlMagasinApprovisionnement.add(lbMagasin7, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin0 ----
            snMagasin0.setName("snMagasin0");
            pnlMagasinApprovisionnement.add(snMagasin0, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlMagasinApprovisionnement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlMagasinsRegroupement ========
          {
            pnlMagasinsRegroupement.setTitre("Magasins \u00e0 regrouper");
            pnlMagasinsRegroupement.setName("pnlMagasinsRegroupement");
            pnlMagasinsRegroupement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlMagasinsRegroupement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlMagasinsRegroupement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlMagasinsRegroupement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlMagasinsRegroupement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin1 ----
            lbMagasin1.setText("Magasin 1");
            lbMagasin1.setPreferredSize(new Dimension(175, 30));
            lbMagasin1.setMinimumSize(new Dimension(175, 30));
            lbMagasin1.setMaximumSize(new Dimension(175, 30));
            lbMagasin1.setName("lbMagasin1");
            pnlMagasinsRegroupement.add(lbMagasin1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin1 ----
            snMagasin1.setName("snMagasin1");
            snMagasin1.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin1ValueChanged(e);
              }
            });
            pnlMagasinsRegroupement.add(snMagasin1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin 2");
            lbMagasin2.setPreferredSize(new Dimension(175, 30));
            lbMagasin2.setMinimumSize(new Dimension(175, 30));
            lbMagasin2.setMaximumSize(new Dimension(175, 30));
            lbMagasin2.setName("lbMagasin2");
            pnlMagasinsRegroupement.add(lbMagasin2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin2 ----
            snMagasin2.setName("snMagasin2");
            snMagasin2.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin2ValueChanged(e);
              }
            });
            pnlMagasinsRegroupement.add(snMagasin2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin3 ----
            lbMagasin3.setText("Magasin 3");
            lbMagasin3.setPreferredSize(new Dimension(175, 30));
            lbMagasin3.setMinimumSize(new Dimension(175, 30));
            lbMagasin3.setMaximumSize(new Dimension(175, 30));
            lbMagasin3.setName("lbMagasin3");
            pnlMagasinsRegroupement.add(lbMagasin3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin3 ----
            snMagasin3.setName("snMagasin3");
            snMagasin3.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin3ValueChanged(e);
              }
            });
            pnlMagasinsRegroupement.add(snMagasin3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin4 ----
            lbMagasin4.setText("Magasin 4");
            lbMagasin4.setPreferredSize(new Dimension(175, 30));
            lbMagasin4.setMinimumSize(new Dimension(175, 30));
            lbMagasin4.setMaximumSize(new Dimension(175, 30));
            lbMagasin4.setName("lbMagasin4");
            pnlMagasinsRegroupement.add(lbMagasin4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin4 ----
            snMagasin4.setName("snMagasin4");
            snMagasin4.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin4ValueChanged(e);
              }
            });
            pnlMagasinsRegroupement.add(snMagasin4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin5 ----
            lbMagasin5.setText("Magasin 5");
            lbMagasin5.setPreferredSize(new Dimension(175, 30));
            lbMagasin5.setMinimumSize(new Dimension(175, 30));
            lbMagasin5.setMaximumSize(new Dimension(175, 30));
            lbMagasin5.setName("lbMagasin5");
            pnlMagasinsRegroupement.add(lbMagasin5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin5 ----
            snMagasin5.setName("snMagasin5");
            snMagasin5.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snMagasin5ValueChanged(e);
              }
            });
            pnlMagasinsRegroupement.add(snMagasin5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin6 ----
            lbMagasin6.setText("Magasin 6");
            lbMagasin6.setPreferredSize(new Dimension(175, 30));
            lbMagasin6.setMinimumSize(new Dimension(175, 30));
            lbMagasin6.setMaximumSize(new Dimension(175, 30));
            lbMagasin6.setName("lbMagasin6");
            pnlMagasinsRegroupement.add(lbMagasin6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snMagasin6 ----
            snMagasin6.setName("snMagasin6");
            pnlMagasinsRegroupement.add(snMagasin6, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlMagasinsRegroupement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pmBTD ========
    {
      pmBTD.setName("pmBTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereSelectionArticle;
  private SNLabelChamp lbFamilleDebut;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFamilleFin;
  private SNPanel pnlSelectionDeBon;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbSousFamilleDebut;
  private SNSousFamille snSousFamilleDebut;
  private SNLabelChamp lbSousFamilleFin;
  private SNSousFamille snSousFamilleFin;
  private SNLabelChamp lbFournisseurDebut;
  private SNFournisseur snFournisseurDebut;
  private SNLabelChamp lbFournisseurFin;
  private SNFournisseur snFournisseurFin;
  private SNLabelChamp lbArticleDebut;
  private SNArticle snArticleDebut;
  private SNLabelChamp lbArticleFin;
  private SNArticle snArticleFin;
  private SNLabelChamp lbGestionnaireProduit;
  private SNPanel pnlNumeroDeCommande;
  private XRiTextField WCGP;
  private SNPanelTitre pnlOptions;
  private XRiCheckBox WPQT;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissementEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCorus;
  private SNPanelTitre pnlMagasinApprovisionnement;
  private SNLabelChamp lbMagasin7;
  private SNMagasin snMagasin0;
  private SNPanelTitre pnlMagasinsRegroupement;
  private SNLabelChamp lbMagasin1;
  private SNMagasin snMagasin1;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin2;
  private SNLabelChamp lbMagasin3;
  private SNMagasin snMagasin3;
  private SNLabelChamp lbMagasin4;
  private SNMagasin snMagasin4;
  private SNLabelChamp lbMagasin5;
  private SNMagasin snMagasin5;
  private SNLabelChamp lbMagasin6;
  private SNMagasin snMagasin6;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
