/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9737;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VEXP01FX_FX extends SNPanelEcranRPG implements ioFrame {
  
  public VEXP01FX_FX(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    FXETA.setValeursSelection("1", "0");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    lbFXLIB = new JLabel();
    FXLIB = new XRiTextField();
    FXETA = new XRiCheckBox();
    lbFXINT = new JLabel();
    FXINT = new XRiTextField();
    lbFXFTP = new JLabel();
    FXFTP = new XRiTextField();
    lbFXINT2 = new JLabel();
    FXINT3 = new XRiTextField();
    label2 = new JLabel();
    FXINT4 = new XRiTextField();
    label13 = new JLabel();
    FXINT19 = new XRiTextField();
    label14 = new JLabel();
    FXINT20 = new XRiTextField();
    FXINT5 = new XRiTextField();
    label3 = new JLabel();
    FXINT6 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    xTitledPanel2 = new JXTitledPanel();
    lbFXLIB2 = new JLabel();
    FXLIB2 = new XRiTextField();
    FXETA2 = new XRiCheckBox();
    lbFXINT5 = new JLabel();
    FXINT9 = new XRiTextField();
    lbFXFTP2 = new JLabel();
    FXFTP2 = new XRiTextField();
    label6 = new JLabel();
    FXINT10 = new XRiTextField();
    lbFXINT6 = new JLabel();
    FXINT11 = new XRiTextField();
    label7 = new JLabel();
    FXINT12 = new XRiTextField();
    lbFXINT7 = new JLabel();
    FXINT13 = new XRiTextField();
    label8 = new JLabel();
    FXINT14 = new XRiTextField();
    lbFXINT8 = new JLabel();
    FXINT15 = new XRiTextField();
    label9 = new JLabel();
    FXINT16 = new XRiTextField();
    label10 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    lbFXLIB3 = new JLabel();
    FXLIB3 = new XRiTextField();
    FXETA3 = new XRiCheckBox();
    lbFXINT9 = new JLabel();
    FXINT29 = new XRiTextField();
    lbFXFTP3 = new JLabel();
    FXFTP3 = new XRiTextField();
    label22 = new JLabel();
    FXINT30 = new XRiTextField();
    lbFXINT10 = new JLabel();
    lbFXINT11 = new JLabel();
    lbFXINT12 = new JLabel();
    label23 = new JLabel();
    FXINT31 = new XRiTextField();
    label24 = new JLabel();
    FXINT32 = new XRiTextField();
    label25 = new JLabel();
    FXINT33 = new XRiTextField();
    label26 = new JLabel();
    FXINT34 = new XRiTextField();
    label27 = new JLabel();
    FXINT35 = new XRiTextField();
    label28 = new JLabel();
    FXINT36 = new XRiTextField();
    FXINT37 = new XRiTextField();
    label29 = new JLabel();
    FXINT38 = new XRiTextField();
    label30 = new JLabel();
    FXINT39 = new XRiTextField();
    label31 = new JLabel();
    FXINT40 = new XRiTextField();
    FXINT41 = new XRiTextField();
    label32 = new JLabel();
    FXINT42 = new XRiTextField();
    label33 = new JLabel();
    FXINT43 = new XRiTextField();
    label34 = new JLabel();
    FXINT44 = new XRiTextField();
    FXINT45 = new XRiTextField();
    label35 = new JLabel();
    FXINT46 = new XRiTextField();
    label36 = new JLabel();
    FXINT47 = new XRiTextField();
    label37 = new JLabel();
    FXINT48 = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de l'exploitation");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          
          // ---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setText("@INDTYP@");
          INDTYP.setOpaque(false);
          INDTYP.setName("INDTYP");
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");
          
          // ---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE).addGap(2, 2, 2)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE).addGap(16, 16, 16)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                  .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(INDIND, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setMinimumSize(new Dimension(670, 300));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(670, 300));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(670, 300));
          p_contenu.setName("p_contenu");
          
          // ======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Personnalisation de flux");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setMinimumSize(new Dimension(550, 300));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);
            
            // ---- lbFXLIB ----
            lbFXLIB.setText("Libell\u00e9");
            lbFXLIB.setName("lbFXLIB");
            xTitledPanel1ContentContainer.add(lbFXLIB);
            lbFXLIB.setBounds(35, 23, 155, 22);
            
            // ---- FXLIB ----
            FXLIB.setComponentPopupMenu(BTD);
            FXLIB.setText("STOCKS");
            FXLIB.setName("FXLIB");
            xTitledPanel1ContentContainer.add(FXLIB);
            FXLIB.setBounds(195, 20, 310, FXLIB.getPreferredSize().height);
            
            // ---- FXETA ----
            FXETA.setText("actif");
            FXETA.setSelected(true);
            FXETA.setName("FXETA");
            xTitledPanel1ContentContainer.add(FXETA);
            FXETA.setBounds(195, 60, 110, FXETA.getPreferredSize().height);
            
            // ---- lbFXINT ----
            lbFXINT.setText("Intervalle (en seconde) pour l'envoi des mouvements de stocks");
            lbFXINT.setName("lbFXINT");
            xTitledPanel1ContentContainer.add(lbFXINT);
            lbFXINT.setBounds(35, 93, 355, 22);
            
            // ---- FXINT ----
            FXINT.setComponentPopupMenu(BTD);
            FXINT.setText("1900");
            FXINT.setHorizontalAlignment(SwingConstants.RIGHT);
            FXINT.setName("FXINT");
            xTitledPanel1ContentContainer.add(FXINT);
            FXINT.setBounds(435, 90, 70, FXINT.getPreferredSize().height);
            
            // ---- lbFXFTP ----
            lbFXFTP.setText("Code FTP");
            lbFXFTP.setName("lbFXFTP");
            xTitledPanel1ContentContainer.add(lbFXFTP);
            lbFXFTP.setBounds(35, 160, 155, 22);
            
            // ---- FXFTP ----
            FXFTP.setComponentPopupMenu(BTD);
            FXFTP.setText("FX006");
            FXFTP.setName("FXFTP");
            xTitledPanel1ContentContainer.add(FXFTP);
            FXFTP.setBounds(195, 157, 60, FXFTP.getPreferredSize().height);
            
            // ---- lbFXINT2 ----
            lbFXINT2.setText("Heure de r\u00e9f\u00e9rence pour l'envoi du stock complet");
            lbFXINT2.setName("lbFXINT2");
            xTitledPanel1ContentContainer.add(lbFXINT2);
            lbFXINT2.setBounds(35, 125, 355, 22);
            
            // ---- FXINT3 ----
            FXINT3.setComponentPopupMenu(BTD);
            FXINT3.setText("22");
            FXINT3.setName("FXINT3");
            xTitledPanel1ContentContainer.add(FXINT3);
            FXINT3.setBounds(0, 265, 30, 28);
            
            // ---- label2 ----
            label2.setText("/");
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setName("label2");
            xTitledPanel1ContentContainer.add(label2);
            label2.setBounds(30, 270, 13, 20);
            
            // ---- FXINT4 ----
            FXINT4.setComponentPopupMenu(BTD);
            FXINT4.setText("09");
            FXINT4.setName("FXINT4");
            xTitledPanel1ContentContainer.add(FXINT4);
            FXINT4.setBounds(40, 265, 30, 28);
            
            // ---- label13 ----
            label13.setText("\u00e0");
            label13.setHorizontalAlignment(SwingConstants.CENTER);
            label13.setName("label13");
            xTitledPanel1ContentContainer.add(label13);
            label13.setBounds(70, 270, 13, 20);
            
            // ---- FXINT19 ----
            FXINT19.setComponentPopupMenu(BTD);
            FXINT19.setText("18");
            FXINT19.setName("FXINT19");
            xTitledPanel1ContentContainer.add(FXINT19);
            FXINT19.setBounds(85, 265, 30, 28);
            
            // ---- label14 ----
            label14.setText(":");
            label14.setHorizontalAlignment(SwingConstants.CENTER);
            label14.setName("label14");
            xTitledPanel1ContentContainer.add(label14);
            label14.setBounds(115, 270, 13, 20);
            
            // ---- FXINT20 ----
            FXINT20.setComponentPopupMenu(BTD);
            FXINT20.setText("00");
            FXINT20.setName("FXINT20");
            xTitledPanel1ContentContainer.add(FXINT20);
            FXINT20.setBounds(125, 265, 30, 28);
            
            // ---- FXINT5 ----
            FXINT5.setComponentPopupMenu(BTD);
            FXINT5.setText("20");
            FXINT5.setName("FXINT5");
            xTitledPanel1ContentContainer.add(FXINT5);
            FXINT5.setBounds(435, 122, 30, 28);
            
            // ---- label3 ----
            label3.setText(":");
            label3.setHorizontalAlignment(SwingConstants.CENTER);
            label3.setName("label3");
            xTitledPanel1ContentContainer.add(label3);
            label3.setBounds(465, 126, 13, 20);
            
            // ---- FXINT6 ----
            FXINT6.setComponentPopupMenu(BTD);
            FXINT6.setText("00");
            FXINT6.setName("FXINT6");
            xTitledPanel1ContentContainer.add(FXINT6);
            FXINT6.setBounds(475, 122, 30, 28);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
                      .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 656, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, Short.MAX_VALUE)))
              .addGap(0, 668, Short.MAX_VALUE));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup().addGap(0, 0, Short.MAX_VALUE)
                      .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 286, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, Short.MAX_VALUE)))
              .addGap(0, 298, Short.MAX_VALUE));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    
    // ======== xTitledPanel2 ========
    {
      xTitledPanel2.setTitle("Personnalisation de flux");
      xTitledPanel2.setBorder(new DropShadowBorder());
      xTitledPanel2.setTitleFont(new Font("sansserif", Font.BOLD, 12));
      xTitledPanel2.setMinimumSize(new Dimension(550, 300));
      xTitledPanel2.setName("xTitledPanel2");
      Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
      xTitledPanel2ContentContainer.setLayout(null);
      
      // ---- lbFXLIB2 ----
      lbFXLIB2.setText("Libell\u00e9");
      lbFXLIB2.setName("lbFXLIB2");
      xTitledPanel2ContentContainer.add(lbFXLIB2);
      lbFXLIB2.setBounds(35, 23, 155, 22);
      
      // ---- FXLIB2 ----
      FXLIB2.setComponentPopupMenu(BTD);
      FXLIB2.setText("CLIENT");
      FXLIB2.setName("FXLIB2");
      xTitledPanel2ContentContainer.add(FXLIB2);
      FXLIB2.setBounds(195, 20, 310, FXLIB2.getPreferredSize().height);
      
      // ---- FXETA2 ----
      FXETA2.setText("actif");
      FXETA2.setSelected(true);
      FXETA2.setName("FXETA2");
      xTitledPanel2ContentContainer.add(FXETA2);
      FXETA2.setBounds(195, 60, 110, FXETA2.getPreferredSize().height);
      
      // ---- lbFXINT5 ----
      lbFXINT5.setText("1 - Heure / minute d'envoi");
      lbFXINT5.setName("lbFXINT5");
      xTitledPanel2ContentContainer.add(lbFXINT5);
      lbFXINT5.setBounds(35, 93, 155, 22);
      
      // ---- FXINT9 ----
      FXINT9.setComponentPopupMenu(BTD);
      FXINT9.setText("18");
      FXINT9.setName("FXINT9");
      xTitledPanel2ContentContainer.add(FXINT9);
      FXINT9.setBounds(195, 90, 30, FXINT9.getPreferredSize().height);
      
      // ---- lbFXFTP2 ----
      lbFXFTP2.setText("Code FTP");
      lbFXFTP2.setName("lbFXFTP2");
      xTitledPanel2ContentContainer.add(lbFXFTP2);
      lbFXFTP2.setBounds(35, 225, 155, 22);
      
      // ---- FXFTP2 ----
      FXFTP2.setComponentPopupMenu(BTD);
      FXFTP2.setText("FX004");
      FXFTP2.setName("FXFTP2");
      xTitledPanel2ContentContainer.add(FXFTP2);
      FXFTP2.setBounds(195, 220, 60, FXFTP2.getPreferredSize().height);
      
      // ---- label6 ----
      label6.setText(":");
      label6.setHorizontalAlignment(SwingConstants.CENTER);
      label6.setName("label6");
      xTitledPanel2ContentContainer.add(label6);
      label6.setBounds(224, 94, 13, 20);
      
      // ---- FXINT10 ----
      FXINT10.setComponentPopupMenu(BTD);
      FXINT10.setText("00");
      FXINT10.setName("FXINT10");
      xTitledPanel2ContentContainer.add(FXINT10);
      FXINT10.setBounds(236, 90, 30, 28);
      
      // ---- lbFXINT6 ----
      lbFXINT6.setText("2 - Heure / minute d'envoi");
      lbFXINT6.setName("lbFXINT6");
      xTitledPanel2ContentContainer.add(lbFXINT6);
      lbFXINT6.setBounds(35, 125, 155, 22);
      
      // ---- FXINT11 ----
      FXINT11.setComponentPopupMenu(BTD);
      FXINT11.setName("FXINT11");
      xTitledPanel2ContentContainer.add(FXINT11);
      FXINT11.setBounds(195, 122, 30, 28);
      
      // ---- label7 ----
      label7.setText(":");
      label7.setHorizontalAlignment(SwingConstants.CENTER);
      label7.setName("label7");
      xTitledPanel2ContentContainer.add(label7);
      label7.setBounds(224, 126, 13, 20);
      
      // ---- FXINT12 ----
      FXINT12.setComponentPopupMenu(BTD);
      FXINT12.setName("FXINT12");
      xTitledPanel2ContentContainer.add(FXINT12);
      FXINT12.setBounds(236, 122, 30, 28);
      
      // ---- lbFXINT7 ----
      lbFXINT7.setText("3 - Heure / minute d'envoi");
      lbFXINT7.setName("lbFXINT7");
      xTitledPanel2ContentContainer.add(lbFXINT7);
      lbFXINT7.setBounds(35, 157, 155, 22);
      
      // ---- FXINT13 ----
      FXINT13.setComponentPopupMenu(BTD);
      FXINT13.setName("FXINT13");
      xTitledPanel2ContentContainer.add(FXINT13);
      FXINT13.setBounds(195, 154, 30, 28);
      
      // ---- label8 ----
      label8.setText(":");
      label8.setHorizontalAlignment(SwingConstants.CENTER);
      label8.setName("label8");
      xTitledPanel2ContentContainer.add(label8);
      label8.setBounds(224, 158, 13, 20);
      
      // ---- FXINT14 ----
      FXINT14.setComponentPopupMenu(BTD);
      FXINT14.setName("FXINT14");
      xTitledPanel2ContentContainer.add(FXINT14);
      FXINT14.setBounds(236, 154, 30, 28);
      
      // ---- lbFXINT8 ----
      lbFXINT8.setText("4 - Heure / minute d'envoi");
      lbFXINT8.setName("lbFXINT8");
      xTitledPanel2ContentContainer.add(lbFXINT8);
      lbFXINT8.setBounds(35, 189, 155, 22);
      
      // ---- FXINT15 ----
      FXINT15.setComponentPopupMenu(BTD);
      FXINT15.setName("FXINT15");
      xTitledPanel2ContentContainer.add(FXINT15);
      FXINT15.setBounds(195, 186, 30, 28);
      
      // ---- label9 ----
      label9.setText(":");
      label9.setHorizontalAlignment(SwingConstants.CENTER);
      label9.setName("label9");
      xTitledPanel2ContentContainer.add(label9);
      label9.setBounds(224, 190, 13, 20);
      
      // ---- FXINT16 ----
      FXINT16.setComponentPopupMenu(BTD);
      FXINT16.setName("FXINT16");
      xTitledPanel2ContentContainer.add(FXINT16);
      FXINT16.setBounds(236, 186, 30, 28);
      
      // ---- label10 ----
      label10.setText(
          "<html><u>Note</u><br>Le flux \u00e0 2 modes de d\u00e9clenchement :<br> - soit aucune heure n'est saisie alors le flux sera d\u00e9clench\u00e9 \u00e0 intervalle r\u00e9gulier comme cela a \u00e9t\u00e9 param\u00e9tr\u00e9 dans le fichier msflux.ini avec le mot cl\u00e9 <b>delai</b>,<br> - soit le flux sera d\u00e9clench\u00e9 uniquement aux heures qui ont \u00e9t\u00e9 saisie (de 1 \u00e0 4 possibles).</html>");
      label10.setName("label10");
      xTitledPanel2ContentContainer.add(label10);
      label10.setBounds(290, 85, 355, 135);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
          Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = xTitledPanel2ContentContainer.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
        xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
      }
    }
    
    // ======== xTitledPanel3 ========
    {
      xTitledPanel3.setTitle("Personnalisation de flux");
      xTitledPanel3.setBorder(new DropShadowBorder());
      xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
      xTitledPanel3.setMinimumSize(new Dimension(550, 300));
      xTitledPanel3.setName("xTitledPanel3");
      Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
      xTitledPanel3ContentContainer.setLayout(null);
      
      // ---- lbFXLIB3 ----
      lbFXLIB3.setText("Libell\u00e9");
      lbFXLIB3.setName("lbFXLIB3");
      xTitledPanel3ContentContainer.add(lbFXLIB3);
      lbFXLIB3.setBounds(35, 23, 155, 22);
      
      // ---- FXLIB3 ----
      FXLIB3.setComponentPopupMenu(BTD);
      FXLIB3.setText("PRIX");
      FXLIB3.setName("FXLIB3");
      xTitledPanel3ContentContainer.add(FXLIB3);
      FXLIB3.setBounds(195, 20, 310, FXLIB3.getPreferredSize().height);
      
      // ---- FXETA3 ----
      FXETA3.setText("actif");
      FXETA3.setSelected(true);
      FXETA3.setName("FXETA3");
      xTitledPanel3ContentContainer.add(FXETA3);
      FXETA3.setBounds(195, 60, 110, FXETA3.getPreferredSize().height);
      
      // ---- lbFXINT9 ----
      lbFXINT9.setText("1 - Jour / Mois / Heure / minute d'envoi");
      lbFXINT9.setName("lbFXINT9");
      xTitledPanel3ContentContainer.add(lbFXINT9);
      lbFXINT9.setBounds(35, 93, 220, 22);
      
      // ---- FXINT29 ----
      FXINT29.setComponentPopupMenu(BTD);
      FXINT29.setText("22");
      FXINT29.setName("FXINT29");
      xTitledPanel3ContentContainer.add(FXINT29);
      FXINT29.setBounds(260, 90, 30, FXINT29.getPreferredSize().height);
      
      // ---- lbFXFTP3 ----
      lbFXFTP3.setText("Code FTP");
      lbFXFTP3.setName("lbFXFTP3");
      xTitledPanel3ContentContainer.add(lbFXFTP3);
      lbFXFTP3.setBounds(35, 225, 155, 22);
      
      // ---- FXFTP3 ----
      FXFTP3.setComponentPopupMenu(BTD);
      FXFTP3.setText("FX013");
      FXFTP3.setName("FXFTP3");
      xTitledPanel3ContentContainer.add(FXFTP3);
      FXFTP3.setBounds(195, 220, 60, FXFTP3.getPreferredSize().height);
      
      // ---- label22 ----
      label22.setText("/");
      label22.setHorizontalAlignment(SwingConstants.CENTER);
      label22.setName("label22");
      xTitledPanel3ContentContainer.add(label22);
      label22.setBounds(290, 95, 13, 20);
      
      // ---- FXINT30 ----
      FXINT30.setComponentPopupMenu(BTD);
      FXINT30.setText("09");
      FXINT30.setName("FXINT30");
      xTitledPanel3ContentContainer.add(FXINT30);
      FXINT30.setBounds(300, 90, 30, 28);
      
      // ---- lbFXINT10 ----
      lbFXINT10.setText("2 - Jour / Mois / Heure / minute d'envoi");
      lbFXINT10.setName("lbFXINT10");
      xTitledPanel3ContentContainer.add(lbFXINT10);
      lbFXINT10.setBounds(35, 125, 220, 22);
      
      // ---- lbFXINT11 ----
      lbFXINT11.setText("3 - Jour / Mois / Heure / minute d'envoi");
      lbFXINT11.setName("lbFXINT11");
      xTitledPanel3ContentContainer.add(lbFXINT11);
      lbFXINT11.setBounds(35, 157, 220, 22);
      
      // ---- lbFXINT12 ----
      lbFXINT12.setText("4 - Jour / Mois / Heure / minute d'envoi");
      lbFXINT12.setName("lbFXINT12");
      xTitledPanel3ContentContainer.add(lbFXINT12);
      lbFXINT12.setBounds(35, 189, 220, 22);
      
      // ---- label23 ----
      label23.setText(
          "<html><u>Note</u><br>Le flux \u00e0 2 modes de d\u00e9clenchement :<br> - soit aucune heure n'est saisie alors le flux sera d\u00e9clench\u00e9 \u00e0 intervalle r\u00e9gulier comme cela a \u00e9t\u00e9 param\u00e9tr\u00e9 dans le fichier msflux.ini avec le mot cl\u00e9 <b>delai</b>,<br> - soit le flux sera d\u00e9clench\u00e9 uniquement aux dates et heures qui ont \u00e9t\u00e9 saisie (de 1 \u00e0 4 possibles).</html>");
      label23.setName("label23");
      xTitledPanel3ContentContainer.add(label23);
      label23.setBounds(420, 55, 225, 195);
      
      // ---- FXINT31 ----
      FXINT31.setComponentPopupMenu(BTD);
      FXINT31.setText("18");
      FXINT31.setName("FXINT31");
      xTitledPanel3ContentContainer.add(FXINT31);
      FXINT31.setBounds(345, 90, 30, 28);
      
      // ---- label24 ----
      label24.setText(":");
      label24.setHorizontalAlignment(SwingConstants.CENTER);
      label24.setName("label24");
      xTitledPanel3ContentContainer.add(label24);
      label24.setBounds(375, 95, 13, 20);
      
      // ---- FXINT32 ----
      FXINT32.setComponentPopupMenu(BTD);
      FXINT32.setText("00");
      FXINT32.setName("FXINT32");
      xTitledPanel3ContentContainer.add(FXINT32);
      FXINT32.setBounds(385, 90, 30, 28);
      
      // ---- label25 ----
      label25.setText("\u00e0");
      label25.setHorizontalAlignment(SwingConstants.CENTER);
      label25.setName("label25");
      xTitledPanel3ContentContainer.add(label25);
      label25.setBounds(330, 95, 13, 20);
      
      // ---- FXINT33 ----
      FXINT33.setComponentPopupMenu(BTD);
      FXINT33.setText("22");
      FXINT33.setName("FXINT33");
      xTitledPanel3ContentContainer.add(FXINT33);
      FXINT33.setBounds(0, 265, 30, 28);
      
      // ---- label26 ----
      label26.setText("/");
      label26.setHorizontalAlignment(SwingConstants.CENTER);
      label26.setName("label26");
      xTitledPanel3ContentContainer.add(label26);
      label26.setBounds(30, 270, 13, 20);
      
      // ---- FXINT34 ----
      FXINT34.setComponentPopupMenu(BTD);
      FXINT34.setText("09");
      FXINT34.setName("FXINT34");
      xTitledPanel3ContentContainer.add(FXINT34);
      FXINT34.setBounds(40, 265, 30, 28);
      
      // ---- label27 ----
      label27.setText("\u00e0");
      label27.setHorizontalAlignment(SwingConstants.CENTER);
      label27.setName("label27");
      xTitledPanel3ContentContainer.add(label27);
      label27.setBounds(70, 270, 13, 20);
      
      // ---- FXINT35 ----
      FXINT35.setComponentPopupMenu(BTD);
      FXINT35.setText("18");
      FXINT35.setName("FXINT35");
      xTitledPanel3ContentContainer.add(FXINT35);
      FXINT35.setBounds(85, 265, 30, 28);
      
      // ---- label28 ----
      label28.setText(":");
      label28.setHorizontalAlignment(SwingConstants.CENTER);
      label28.setName("label28");
      xTitledPanel3ContentContainer.add(label28);
      label28.setBounds(115, 270, 13, 20);
      
      // ---- FXINT36 ----
      FXINT36.setComponentPopupMenu(BTD);
      FXINT36.setText("00");
      FXINT36.setName("FXINT36");
      xTitledPanel3ContentContainer.add(FXINT36);
      FXINT36.setBounds(125, 265, 30, 28);
      
      // ---- FXINT37 ----
      FXINT37.setComponentPopupMenu(BTD);
      FXINT37.setName("FXINT37");
      xTitledPanel3ContentContainer.add(FXINT37);
      FXINT37.setBounds(260, 122, 30, 28);
      
      // ---- label29 ----
      label29.setText("/");
      label29.setHorizontalAlignment(SwingConstants.CENTER);
      label29.setName("label29");
      xTitledPanel3ContentContainer.add(label29);
      label29.setBounds(290, 126, 13, 20);
      
      // ---- FXINT38 ----
      FXINT38.setComponentPopupMenu(BTD);
      FXINT38.setName("FXINT38");
      xTitledPanel3ContentContainer.add(FXINT38);
      FXINT38.setBounds(300, 122, 30, 28);
      
      // ---- label30 ----
      label30.setText("\u00e0");
      label30.setHorizontalAlignment(SwingConstants.CENTER);
      label30.setName("label30");
      xTitledPanel3ContentContainer.add(label30);
      label30.setBounds(330, 126, 13, 20);
      
      // ---- FXINT39 ----
      FXINT39.setComponentPopupMenu(BTD);
      FXINT39.setName("FXINT39");
      xTitledPanel3ContentContainer.add(FXINT39);
      FXINT39.setBounds(345, 122, 30, 28);
      
      // ---- label31 ----
      label31.setText(":");
      label31.setHorizontalAlignment(SwingConstants.CENTER);
      label31.setName("label31");
      xTitledPanel3ContentContainer.add(label31);
      label31.setBounds(375, 126, 13, 20);
      
      // ---- FXINT40 ----
      FXINT40.setComponentPopupMenu(BTD);
      FXINT40.setName("FXINT40");
      xTitledPanel3ContentContainer.add(FXINT40);
      FXINT40.setBounds(385, 122, 30, 28);
      
      // ---- FXINT41 ----
      FXINT41.setComponentPopupMenu(BTD);
      FXINT41.setName("FXINT41");
      xTitledPanel3ContentContainer.add(FXINT41);
      FXINT41.setBounds(260, 154, 30, 28);
      
      // ---- label32 ----
      label32.setText("/");
      label32.setHorizontalAlignment(SwingConstants.CENTER);
      label32.setName("label32");
      xTitledPanel3ContentContainer.add(label32);
      label32.setBounds(290, 158, 13, 20);
      
      // ---- FXINT42 ----
      FXINT42.setComponentPopupMenu(BTD);
      FXINT42.setName("FXINT42");
      xTitledPanel3ContentContainer.add(FXINT42);
      FXINT42.setBounds(300, 154, 30, 28);
      
      // ---- label33 ----
      label33.setText("\u00e0");
      label33.setHorizontalAlignment(SwingConstants.CENTER);
      label33.setName("label33");
      xTitledPanel3ContentContainer.add(label33);
      label33.setBounds(330, 158, 13, 20);
      
      // ---- FXINT43 ----
      FXINT43.setComponentPopupMenu(BTD);
      FXINT43.setName("FXINT43");
      xTitledPanel3ContentContainer.add(FXINT43);
      FXINT43.setBounds(345, 154, 30, 28);
      
      // ---- label34 ----
      label34.setText(":");
      label34.setHorizontalAlignment(SwingConstants.CENTER);
      label34.setName("label34");
      xTitledPanel3ContentContainer.add(label34);
      label34.setBounds(375, 158, 13, 20);
      
      // ---- FXINT44 ----
      FXINT44.setComponentPopupMenu(BTD);
      FXINT44.setName("FXINT44");
      xTitledPanel3ContentContainer.add(FXINT44);
      FXINT44.setBounds(385, 154, 30, 28);
      
      // ---- FXINT45 ----
      FXINT45.setComponentPopupMenu(BTD);
      FXINT45.setName("FXINT45");
      xTitledPanel3ContentContainer.add(FXINT45);
      FXINT45.setBounds(260, 186, 30, 28);
      
      // ---- label35 ----
      label35.setText("/");
      label35.setHorizontalAlignment(SwingConstants.CENTER);
      label35.setName("label35");
      xTitledPanel3ContentContainer.add(label35);
      label35.setBounds(290, 190, 13, 20);
      
      // ---- FXINT46 ----
      FXINT46.setComponentPopupMenu(BTD);
      FXINT46.setName("FXINT46");
      xTitledPanel3ContentContainer.add(FXINT46);
      FXINT46.setBounds(300, 186, 30, 28);
      
      // ---- label36 ----
      label36.setText("\u00e0");
      label36.setHorizontalAlignment(SwingConstants.CENTER);
      label36.setName("label36");
      xTitledPanel3ContentContainer.add(label36);
      label36.setBounds(330, 190, 13, 20);
      
      // ---- FXINT47 ----
      FXINT47.setComponentPopupMenu(BTD);
      FXINT47.setName("FXINT47");
      xTitledPanel3ContentContainer.add(FXINT47);
      FXINT47.setBounds(345, 186, 30, 28);
      
      // ---- label37 ----
      label37.setText(":");
      label37.setHorizontalAlignment(SwingConstants.CENTER);
      label37.setName("label37");
      xTitledPanel3ContentContainer.add(label37);
      label37.setBounds(375, 190, 13, 20);
      
      // ---- FXINT48 ----
      FXINT48.setComponentPopupMenu(BTD);
      FXINT48.setName("FXINT48");
      xTitledPanel3ContentContainer.add(FXINT48);
      FXINT48.setBounds(385, 186, 30, 28);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
          Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = xTitledPanel3ContentContainer.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
        xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
      }
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel lbFXLIB;
  private XRiTextField FXLIB;
  private XRiCheckBox FXETA;
  private JLabel lbFXINT;
  private XRiTextField FXINT;
  private JLabel lbFXFTP;
  private XRiTextField FXFTP;
  private JLabel lbFXINT2;
  private XRiTextField FXINT3;
  private JLabel label2;
  private XRiTextField FXINT4;
  private JLabel label13;
  private XRiTextField FXINT19;
  private JLabel label14;
  private XRiTextField FXINT20;
  private XRiTextField FXINT5;
  private JLabel label3;
  private XRiTextField FXINT6;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JXTitledPanel xTitledPanel2;
  private JLabel lbFXLIB2;
  private XRiTextField FXLIB2;
  private XRiCheckBox FXETA2;
  private JLabel lbFXINT5;
  private XRiTextField FXINT9;
  private JLabel lbFXFTP2;
  private XRiTextField FXFTP2;
  private JLabel label6;
  private XRiTextField FXINT10;
  private JLabel lbFXINT6;
  private XRiTextField FXINT11;
  private JLabel label7;
  private XRiTextField FXINT12;
  private JLabel lbFXINT7;
  private XRiTextField FXINT13;
  private JLabel label8;
  private XRiTextField FXINT14;
  private JLabel lbFXINT8;
  private XRiTextField FXINT15;
  private JLabel label9;
  private XRiTextField FXINT16;
  private JLabel label10;
  private JXTitledPanel xTitledPanel3;
  private JLabel lbFXLIB3;
  private XRiTextField FXLIB3;
  private XRiCheckBox FXETA3;
  private JLabel lbFXINT9;
  private XRiTextField FXINT29;
  private JLabel lbFXFTP3;
  private XRiTextField FXFTP3;
  private JLabel label22;
  private XRiTextField FXINT30;
  private JLabel lbFXINT10;
  private JLabel lbFXINT11;
  private JLabel lbFXINT12;
  private JLabel label23;
  private XRiTextField FXINT31;
  private JLabel label24;
  private XRiTextField FXINT32;
  private JLabel label25;
  private XRiTextField FXINT33;
  private JLabel label26;
  private XRiTextField FXINT34;
  private JLabel label27;
  private XRiTextField FXINT35;
  private JLabel label28;
  private XRiTextField FXINT36;
  private XRiTextField FXINT37;
  private JLabel label29;
  private XRiTextField FXINT38;
  private JLabel label30;
  private XRiTextField FXINT39;
  private JLabel label31;
  private XRiTextField FXINT40;
  private XRiTextField FXINT41;
  private JLabel label32;
  private XRiTextField FXINT42;
  private JLabel label33;
  private XRiTextField FXINT43;
  private JLabel label34;
  private XRiTextField FXINT44;
  private XRiTextField FXINT45;
  private JLabel label35;
  private XRiTextField FXINT46;
  private JLabel label36;
  private XRiTextField FXINT47;
  private JLabel label37;
  private XRiTextField FXINT48;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
