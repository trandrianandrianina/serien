/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc10368;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.desktop.metier.gescom.comptoir.EnumComboLivraisonPartielle;
import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.IdModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.transporteur.IdTransporteur;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.sntypefacturation.SNTypeFacturation;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.ModeleChantier;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.SNChantier;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

public class VueOngletLivraison extends JPanel implements InterfaceVueOnglet {
  // Constantes
  private static String LIBELLE_NOM = "Nom";
  private static String LIBELLE_PRENOM = "Compl\u00e9ment";
  private static String LIBELLE_RAISON_SOCIALE = "Raison sociale";
  private static String LIBELLE_COMPLEMENT = "Compl\u00e9ment";
  
  private static final int NOMBRE_DECIMALE_MONTANT = 2;
  private static final int NOMBRE_DECIMALE_QUANTITE = 2;
  
  private static final String NON_PRECISE = "Non précisé";
  
  private static final int ONGLET_ADRESSE_FACTURATION = 0;
  private static final int ONGLET_ADRESSE_LIVRAISON = 1;
  
  // Variables
  private ModeleComptoir modele = null;
  private boolean executerEvenements = false;
  private String[] listeNomsContacts = null;
  
  /**
   * Constructeur.
   */
  public VueOngletLivraison(ModeleComptoir acomptoir) {
    modele = acomptoir;
  }
  
  // -- Méthodes publiques
  
  /**
   * Affiche l'écran.
   */
  @Override
  public void initialiserComposants() {
    Trace.info("--> Début initialiserComposants");
    initComponents();
    setName(getClass().getSimpleName());
    Trace.info("--> Milieu initialiserComposants");
    
    tfReferenceDocument.setLongueur(DocumentVente.LONGUEUR_REFERENCE_LONGUE);
    tfReferenceCourteDocument.setLongueur(DocumentVente.LONGUEUR_REFERENCE_COURTE);
    tfNomLivraison.setLongueur(Client.TAILLE_ZONE_NOM);
    tfComplementNomLivraison.setLongueur(Client.TAILLE_ZONE_NOM);
    tfRueLivraison.setLongueur(Client.TAILLE_ZONE_NOM);
    tfLocalisationLivraison.setLongueur(Client.TAILLE_ZONE_NOM);
    tfImmatriculation.setLongueur(DocumentVente.TAILLE_ZONE_IMMATRICULATION);
    
    // Création des évènements FocusListener pour les objets JXDatePicker
    // La déclaration est manuelle car sinon l'èvenment n'est pas pris en compte car il doit être sur l'éditor et non l'objet lui-même
    calDateTraitementDocument.getEditor().addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfDateTraitementDocumentFocusLost(e);
      }
    });
    calDateValiditeDocument.getEditor().addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfDateValiditeDocumentFocusLost(e);
      }
    });
    calDateRelanceDocument.getEditor().addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfDateRelanceDocumentFocusLost(e);
      }
    });
    calDateLivraisonSouhaitee.getEditor().addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfDateLivraisonSouhaiteeFocusLost(e);
      }
    });
    calDateLivraisonPrevue.getEditor().addFocusListener(new FocusAdapter() {
      @Override
      public void focusLost(FocusEvent e) {
        tfDateLivraisonPrevueFocusLost(e);
      }
    });
    
    // Configurer le composant "Pris par"
    cbPrisPar.getEditor().getEditorComponent().addFocusListener(new FocusListener() {
      @Override
      public void focusLost(FocusEvent pFocusEvent) {
        cbPrisParFocusLost(pFocusEvent);
      }
      
      @Override
      public void focusGained(FocusEvent arg0) {
        ((JTextField) cbPrisPar.getEditor().getEditorComponent()).selectAll();
      }
    });
    
    // Configurer le champ "Pris par"
    final JTextField tfFiltreContact = (JTextField) cbPrisPar.getEditor().getEditorComponent();
    tfFiltreContact.setDisabledTextColor(Constantes.COULEUR_TEXTE_DESACTIVE);
    tfFiltreContact.addKeyListener(new KeyAdapter() {
      @Override
      public void keyReleased(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_ENTER || ke.getKeyCode() == KeyEvent.VK_DOWN || ke.getKeyCode() == KeyEvent.VK_UP) {
          return;
        }
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            filterContact(tfFiltreContact.getText());
          }
        });
      }
    });
    
    // Formatage des zones de saisie (TODO à faire celles qui restent + mettre des constantes)
    
    // Permet d'intercepter la touche TAB pour envoyé le focus sur un autre composant
    taInformationLivraisonEnlevement.getInputMap().put(SNCharteGraphique.TOUCHE_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocus();
      }
    });
    taInformationLivraisonEnlevement.getInputMap().put(SNCharteGraphique.TOUCHE_SHIFT_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocusBackward();
      }
    });
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Raccourcis clavier
    rbModeEnlevement.setMnemonic(KeyEvent.VK_E);
    rbModeLivraison.setMnemonic(KeyEvent.VK_L);
    ckDirectUsine.setMnemonic(KeyEvent.VK_U);
    Trace.info("--> Fin   initialiserComposants");
  }
  
  @Override
  public void rafraichir() {
    Trace.info("--> Début Rafraichir VueOngletLivraison");
    executerEvenements = false;
    
    // Panneau mode de livraison
    rafraichirModeLivraison();
    rafraichirDirectUsine();
    
    // Panneau informations documents
    rafraichirReferenceLongue();
    rafraichirReferenceCourte();
    rafraichirDateValidite();
    rafraichirDateRelance();
    rafraichirDateDocument();
    rafraichirTypeFacturation();
    
    // Panneau adresse de facturation
    rafraichirNomFacturation();
    rafraichirComplementNomFacturation();
    rafraichirRueFacturation();
    rafraichirLocalisationFacturation();
    rafraichirCodePostalCommuneFacturation();
    rafraichirContactFacturation();
    rafraichirTelephoneFacturation();
    rafraichirMailFacturation();
    rafraichirFaxFacturation();
    
    // Panneau adresse de livraison
    rafraichirNomLivraison();
    rafraichirComplementNomLivraison();
    rafraichirRueLivraison();
    rafraichirLocalisationLivraison();
    rafraichirCodePostalCommuneLivraison();
    rafraichirContactlivraison();
    rafraichirTelephoneLivraison();
    rafraichirMailLivraison();
    rafraichirFaxLivraison();
    
    // Panneau mémo
    rafraichirInformationLivraisonEnlevement();
    
    // Panneau enlèvement
    rafraichirPanneauEnlevement();
    
    // Panneau transport
    rafraichirPanneauTransporteur();
    
    // Panneau liste des chantiers
    rafraichirPanneauChantier();
    
    // Adapte le titre du bloc-note livraison
    rafraichirTitreBlocNotesLivraison();
    
    // Affiche le bouton pour sélectionner une adresse de livraison dans les adresses multiples du chantier
    rafraichirBoutonSelectionAdresseLivraison();
    
    // Positionner le focus (trouver une meilleure solution)
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        Trace.info("---> Début EventQueue.invokeLater Focus");
        switch (modele.getComposantAyantLeFocus()) {
          case ModeleComptoir.FOCUS_LIVRAISON_REFERENCE_DOCUMENT:
            tfReferenceCourteDocument.requestFocus();
            break;
          case ModeleComptoir.FOCUS_LIVRAISON_CIVILITE:
            tfNomLivraison.requestFocus();
            break;
          case ModeleComptoir.FOCUS_LIVRAISON_NOM:
            tfNomLivraison.requestFocus();
            break;
        }
        Trace.info("---> Fin   EventQueue.invokeLater Focus");
      }
    });
    
    executerEvenements = true;
    Trace.info("--> Fin   Rafraichir VueOngletLivraison");
  }
  
  // -- Méthodes privées
  
  private void rafraichirModeLivraison() {
    Trace.info("---> Début rafraichirModeLivraison");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null) {
      if (documentVente.isLivraison()) {
        rbModeLivraison.setSelected(true);
        rbModeEnlevement.setSelected(false);
        tabAdresses.setSelectedIndex(ONGLET_ADRESSE_LIVRAISON);
      }
      else if (documentVente.isEnlevement()) {
        rbModeLivraison.setSelected(false);
        rbModeEnlevement.setSelected(true);
        tabAdresses.setSelectedIndex(ONGLET_ADRESSE_FACTURATION);
      }
      else {
        btgMode.clearSelection();
        tabAdresses.setSelectedIndex(ONGLET_ADRESSE_FACTURATION);
      }
      
      Trace.info("----> Document modifiable:" + documentVente.isModifiable());
      rbModeLivraison.setEnabled(documentVente.isModifiable());
      rbModeEnlevement.setEnabled(documentVente.isModifiable());
    }
    else {
      btgMode.clearSelection();
      Trace.info("----> Document modifiable:" + documentVente);
      rbModeLivraison.setEnabled(false);
      rbModeEnlevement.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirModeLivraison");
  }
  
  private void rafraichirDirectUsine() {
    Trace.info("---> Début rafraichirDirectUsine");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    // L'option direct usine n'est affichée que pour les commandes
    if (documentVente != null) {
      ckDirectUsine.setVisible(modele.isAfficherOptionDirectUsine());
      ckDirectUsine.setSelected(documentVente.isDirectUsine());
      ckDirectUsine.setEnabled(modele.isDirectUsineActif());
    }
    else {
      ckDirectUsine.setSelected(false);
    }
    Trace.info("---> Fin   rafraichirDirectUsine");
  }
  
  private void rafraichirReferenceLongue() {
    Trace.info("---> Début rafraichirReferenceLongue");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && !documentVente.isModeExpeditionNonDefini()) {
      tfReferenceDocument.setText(documentVente.getReferenceLongue());
      tfReferenceDocument.setEnabled(documentVente.isModifiable());
    }
    else {
      tfReferenceDocument.setText("");
      tfReferenceDocument.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirReferenceLongue");
  }
  
  private void rafraichirReferenceCourte() {
    Trace.info("---> Début rafraichirReferenceCourte");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && !documentVente.isModeExpeditionNonDefini()) {
      tfReferenceCourteDocument.setText(documentVente.getReferenceCourte());
      tfReferenceCourteDocument.setEnabled(documentVente.isModifiable());
    }
    else {
      tfReferenceCourteDocument.setText("");
      tfReferenceCourteDocument.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirReferenceCourte");
  }
  
  private void rafraichirDateDocument() {
    Trace.info("---> Début rafraichirDateDocument");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    // Afficher la date du document si elle existe
    if (documentVente != null && documentVente.getDateDocument() != null) {
      calDateTraitementDocument.setDate(documentVente.getDateDocument());
    }
    // Sinon, afficher la date de traitement
    else if (documentVente != null && modele.getDateTraitement() != null) {
      calDateTraitementDocument.setDate(modele.getDateTraitement());
    }
    else {
      calDateTraitementDocument.setDate(null);
    }
    
    // La date du document n'est modifiable que si le document est en cours de création
    calDateTraitementDocument.setEnabled(documentVente != null && documentVente.isEnCoursCreation());
    Trace.info("---> Fin   rafraichirDateDocument");
  }
  
  private void rafraichirDateValidite() {
    Trace.info("---> Début rafraichirDateValidite");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null) {
      switch (documentVente.getTypeDocumentVente()) {
        case DEVIS:
          lbDateValiditeDocument.setVisible(true);
          calDateValiditeDocument.setVisible(true);
          calDateValiditeDocument.setDate(modele.getDateValiditeDocument());
          break;
        default:
          lbDateValiditeDocument.setVisible(false);
          calDateValiditeDocument.setVisible(false);
          calDateValiditeDocument.setDate(null);
      }
    }
    else {
      lbDateValiditeDocument.setVisible(false);
      calDateValiditeDocument.setVisible(false);
      calDateValiditeDocument.setDate(null);
    }
    
    // La date de validité n'est modifiable que si le document est en cours de création
    calDateValiditeDocument.setEnabled(documentVente != null && documentVente.isModifiable());
    Trace.info("---> Fin   rafraichirDateValidite");
  }
  
  private void rafraichirDateRelance() {
    Trace.info("---> Début rafraichirDateRelance");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null) {
      switch (documentVente.getTypeDocumentVente()) {
        case DEVIS:
          lbDateRelanceDocument.setVisible(true);
          calDateRelanceDocument.setVisible(true);
          calDateRelanceDocument.setDate(modele.getDateRelanceDocument());
          break;
        default:
          lbDateRelanceDocument.setVisible(false);
          calDateRelanceDocument.setVisible(false);
          calDateRelanceDocument.setDate(null);
      }
    }
    else {
      lbDateRelanceDocument.setVisible(false);
      calDateRelanceDocument.setVisible(false);
      calDateRelanceDocument.setDate(null);
    }
    
    // La date de relance n'est modifiable que si le document est en cours de création
    calDateRelanceDocument.setEnabled(documentVente != null && documentVente.isModifiable());
    Trace.info("---> Fin   rafraichirDateRelance");
  }
  
  private void rafraichirTypeFacturation() {
    Trace.info("---> Début rafraichirTypeFacturation");
    IdEtablissement idEtablissement = modele.getEtablissement().getId();
    cbTypesFacturation.setSession(modele.getSession());
    cbTypesFacturation.setIdEtablissement(idEtablissement);
    cbTypesFacturation.charger(false);
    cbTypesFacturation.ajouterFacturationSansTVA(idEtablissement);
    cbTypesFacturation.ajouterFacturationUE(idEtablissement);
    
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null) {
      cbTypesFacturation.setIdSelection(documentVente.getIdTypeFacturation());
      
      if (ManagerSessionClient.getInstance().verifierDroitGescom(modele.getIdSession(),
          EnumDroitSecurite.IS_AUTORISE_MODIFICATION_TYPE_FACTURATION) && documentVente.isModifiable()) {
        cbTypesFacturation.setEnabled(true);
      }
      else {
        cbTypesFacturation.setEnabled(false);
      }
    }
    else {
      cbTypesFacturation.setSelection(null);
      cbTypesFacturation.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirTypeFacturation");
  }
  
  private void rafraichirNomFacturation() {
    Trace.info("---> Début rafraichirNomFacturation");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.getAdresseFacturation() != null) {
      tfNomFacturation.setText(documentVente.getAdresseFacturation().getNom());
      tfNomFacturation.setEnabled(documentVente.isModifiable());
    }
    else {
      tfNomFacturation.setText("");
      tfNomFacturation.setEnabled(false);
    }
    
    if (modele.getClientCourant().isParticulier()) {
      lbNomFacturation.setText(LIBELLE_NOM);
    }
    else {
      lbNomFacturation.setText(LIBELLE_RAISON_SOCIALE);
    }
    Trace.info("---> Fin   rafraichirNomFacturation");
  }
  
  private void rafraichirComplementNomFacturation() {
    Trace.info("---> Début rafraichirComplementNomFacturation");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.getAdresseFacturation() != null) {
      tfComplementNomFacturation.setText(documentVente.getAdresseFacturation().getComplementNom());
      tfComplementNomFacturation.setEnabled(documentVente.isModifiable());
    }
    else {
      tfComplementNomFacturation.setText("");
      tfComplementNomFacturation.setEnabled(false);
    }
    
    if (modele.getClientCourant() == null || modele.getClientCourant().isParticulier()) {
      lbComplementNomFacturation.setText(LIBELLE_PRENOM);
    }
    else {
      lbComplementNomFacturation.setText(LIBELLE_COMPLEMENT);
    }
    Trace.info("---> Fin   rafraichirComplementNomFacturation");
  }
  
  private void rafraichirRueFacturation() {
    Trace.info("---> Début rafraichirRueFacturation");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.getAdresseFacturation() != null) {
      tfRueFacturation.setText(documentVente.getAdresseFacturation().getRue());
      tfRueFacturation.setEnabled(documentVente.isModifiable());
    }
    else {
      tfRueFacturation.setText("");
      tfRueFacturation.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirRueFacturation");
  }
  
  private void rafraichirLocalisationFacturation() {
    Trace.info("---> Début rafraichirLocalisationFacturation");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.getAdresseFacturation() != null) {
      tfLocalisationFacturation.setText(documentVente.getAdresseFacturation().getLocalisation());
      tfLocalisationFacturation.setEnabled(documentVente.isModifiable());
    }
    else {
      tfLocalisationFacturation.setText("");
      tfLocalisationFacturation.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirLocalisationFacturation");
  }
  
  private void rafraichirCodePostalCommuneFacturation() {
    Trace.info("---> Début rafraichirCodePostalCommuneFacturation");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    snCodePostalCommuneFacturation.setSession(modele.getSession());
    snCodePostalCommuneFacturation.charger(false);
    if (documentVente != null && documentVente.getAdresseFacturation() != null) {
      if (modele.getEtablissement() == null) {
        Trace.info("---> Fin   rafraichirCodePostalCommuneFacturation");
        return;
      }
      snCodePostalCommuneFacturation.setIdEtablissement(modele.getEtablissement().getId());
      
      Adresse adresse = documentVente.getAdresseFacturation();
      CodePostalCommune codePostalCommuneClient = null;
      if (adresse != null) {
        codePostalCommuneClient = CodePostalCommune.getInstance(adresse.getCodePostalFormate(), adresse.getVille());
        snCodePostalCommuneFacturation.charger(false);
        snCodePostalCommuneFacturation.setSelection(codePostalCommuneClient);
      }
      
      snCodePostalCommuneFacturation.setEnabled(documentVente.isModifiable());
    }
    else {
      snCodePostalCommuneFacturation.setSelection(null);
      snCodePostalCommuneFacturation.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirCodePostalCommuneFacturation");
  }
  
  private void rafraichirContactFacturation() {
    Trace.info("---> Début rafraichirContactFacturation");
    lbContactFacturation.setVisible(false);
    tfContactFacturation.setVisible(false);
    Trace.info("---> Fin   rafraichirContactFacturation");
  }
  
  private void rafraichirTelephoneFacturation() {
    Trace.info("---> Début rafraichirTelephoneFacturation");
    lbTelephoneFacturation.setVisible(false);
    tfTelephoneFacturation.setVisible(false);
    Trace.info("---> Fin   rafraichirTelephoneFacturation");
  }
  
  private void rafraichirMailFacturation() {
    Trace.info("---> Début rafraichirMailFacturation");
    lbEmailFacturation.setVisible(false);
    tfEmailFacturation.setVisible(false);
    Trace.info("---> Fin   rafraichirMailFacturation");
  }
  
  private void rafraichirFaxFacturation() {
    Trace.info("---> Début rafraichirFaxFacturation");
    lbFaxFacturation.setVisible(false);
    tfFaxFacturation.setVisible(false);
    Trace.info("---> Fin   rafraichirFaxFacturation");
  }
  
  private void rafraichirNomLivraison() {
    Trace.info("---> Début rafraichirNomLivraison");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.isLivraison() && documentVente.getTransport() != null
        && documentVente.getTransport().getAdresseLivraison() != null) {
      tfNomLivraison.setText(documentVente.getTransport().getAdresseLivraison().getNom());
      tfNomLivraison.setEnabled(documentVente.isModifiable());
    }
    else {
      tfNomLivraison.setText("");
      tfNomLivraison.setEnabled(false);
    }
    
    if (modele.getClientCourant().isParticulier()) {
      lbNomLivraison.setText(LIBELLE_NOM);
    }
    else {
      lbNomLivraison.setText(LIBELLE_RAISON_SOCIALE);
    }
    Trace.info("---> Fin   rafraichirNomLivraison");
  }
  
  private void rafraichirComplementNomLivraison() {
    Trace.info("---> Début rafraichirComplementNomLivraison");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.isLivraison() && documentVente.getTransport() != null
        && documentVente.getTransport().getAdresseLivraison() != null) {
      tfComplementNomLivraison.setText(documentVente.getTransport().getAdresseLivraison().getComplementNom());
      tfComplementNomLivraison.setEnabled(documentVente.isModifiable());
    }
    else {
      tfComplementNomLivraison.setText("");
      tfComplementNomLivraison.setEnabled(false);
    }
    
    if (modele.getClientCourant() == null || modele.getClientCourant().isParticulier()) {
      lbComplementNomLivraison.setText(LIBELLE_PRENOM);
    }
    else {
      lbComplementNomLivraison.setText(LIBELLE_COMPLEMENT);
    }
    Trace.info("---> Fin   rafraichirComplementNomLivraison");
  }
  
  private void rafraichirRueLivraison() {
    Trace.info("---> Début rafraichirRueLivraison");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.isLivraison() && documentVente.getTransport() != null
        && documentVente.getTransport().getAdresseLivraison() != null) {
      tfRueLivraison.setText(documentVente.getTransport().getAdresseLivraison().getRue());
      tfRueLivraison.setEnabled(documentVente.isModifiable());
    }
    else {
      tfRueLivraison.setText("");
      tfRueLivraison.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirRueLivraison");
  }
  
  private void rafraichirLocalisationLivraison() {
    Trace.info("---> Début rafraichirLocalisationLivraison");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.isLivraison() && documentVente.getTransport() != null
        && documentVente.getTransport().getAdresseLivraison() != null) {
      tfLocalisationLivraison.setText(documentVente.getTransport().getAdresseLivraison().getLocalisation());
      tfLocalisationLivraison.setEnabled(documentVente.isModifiable());
    }
    else {
      tfLocalisationLivraison.setText("");
      tfLocalisationLivraison.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirLocalisationLivraison");
  }
  
  private void rafraichirCodePostalCommuneLivraison() {
    Trace.info("---> Début rafraichirCodePostalCommuneLivraison");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    snCodePostalCommuneLivraison.setSession(modele.getSession());
    if (documentVente != null && documentVente.isLivraison() && documentVente.getTransport() != null
        && documentVente.getTransport().getAdresseLivraison() != null) {
      if (modele.getEtablissement() == null) {
        Trace.info("---> Fin   rafraichirCodePostalCommuneLivraison");
        return;
      }
      snCodePostalCommuneLivraison.setIdEtablissement(modele.getEtablissement().getId());
      
      Adresse adresse = documentVente.getTransport().getAdresseLivraison();
      CodePostalCommune codePostalCommuneClient = null;
      if (adresse != null) {
        codePostalCommuneClient = CodePostalCommune.getInstance(adresse.getCodePostalFormate(), adresse.getVille());
        snCodePostalCommuneLivraison.charger(false);
        snCodePostalCommuneLivraison.setSelection(codePostalCommuneClient);
      }
      
      snCodePostalCommuneLivraison.setEnabled(documentVente.isModifiable());
    }
    else {
      snCodePostalCommuneLivraison.setSelection(null);
      snCodePostalCommuneLivraison.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirCodePostalCommuneLivraison");
  }
  
  private void rafraichirContactlivraison() {
    Trace.info("---> Début rafraichirContactlivraison");
    lbContactLivraison.setVisible(false);
    tfContactLivraison.setVisible(false);
    Trace.info("---> Fin   rafraichirContactlivraison");
  }
  
  private void rafraichirTelephoneLivraison() {
    Trace.info("---> Début rafraichirTelephoneLivraison");
    lbTelephoneLivraison.setVisible(false);
    tfTelephoneLivraison.setVisible(false);
    Trace.info("---> Fin   rafraichirTelephoneLivraison");
  }
  
  private void rafraichirMailLivraison() {
    Trace.info("---> Début rafraichirMailLivraison");
    lbEmailLivraison.setVisible(false);
    tfEmailLivraison.setVisible(false);
    Trace.info("---> Fin   rafraichirMailLivraison");
  }
  
  private void rafraichirFaxLivraison() {
    Trace.info("---> Début rafraichirFaxLivraison");
    lbFaxLivraison.setVisible(false);
    tfFaxLivraison.setVisible(false);
    Trace.info("---> Fin   rafraichirFaxLivraison");
  }
  
  private void rafraichirInformationLivraisonEnlevement() {
    Trace.info("---> Début rafraichirInformationLivraisonEnlevement");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null) {
      taInformationLivraisonEnlevement.setText(documentVente.getTexteLivraisonEnlevement());
      taInformationLivraisonEnlevement.setCaretPosition(0);
      taInformationLivraisonEnlevement.setEnabled(documentVente.isModifiable());
    }
    else {
      taInformationLivraisonEnlevement.setText("");
      taInformationLivraisonEnlevement.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirInformationLivraisonEnlevement");
  }
  
  private void rafraichirPanneauEnlevement() {
    Trace.info("---> Début rafraichirPanneauEnlevement");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.isEnlevement()) {
      pnlEnlevement.setVisible(true);
      rafraichirPrisPar();
      rafraichirImmatriculation();
    }
    else {
      pnlEnlevement.setVisible(false);
    }
    Trace.info("---> Fin   rafraichirPanneauEnlevement");
  }
  
  private void rafraichirPrisPar() {
    Trace.info("---> Début rafraichirPrisPar");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null) {
      // Rafraîchir la liste
      cbPrisPar.removeAllItems();
      if (modele.getListeContactClient() != null) {
        listeNomsContacts = new String[modele.getListeContactClient().size() + 1];
        listeNomsContacts[0] = documentVente.getPrisPar();
        for (int i = 0; i < modele.getListeContactClient().size(); i++) {
          listeNomsContacts[i + 1] = modele.getListeContactClient().get(i).getNomComplet();
        }
      }
      else {
        listeNomsContacts = new String[1];
        listeNomsContacts[0] = documentVente.getPrisPar();
      }
      
      cbPrisPar.setModel(new DefaultComboBoxModel(listeNomsContacts));
      // On intègre la gestion du filtre dans la combobox contact
      cbPrisPar.setEditable(documentVente.isModifiable());
      
      cbPrisPar.setEnabled(documentVente.isModifiable());
    }
    else {
      cbPrisPar.removeAllItems();
      cbPrisPar.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirPrisPar");
  }
  
  private void rafraichirImmatriculation() {
    Trace.info("---> Début rafraichirImmatriculation");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && !documentVente.isModeExpeditionNonDefini()) {
      tfImmatriculation.setText(documentVente.getImmatriculationVehiculeClient());
      tfImmatriculation.setEnabled(documentVente.isModifiable());
    }
    else {
      tfImmatriculation.setText("");
      tfImmatriculation.setEnabled(false);
    }
    Trace.info("---> Fin   rafraichirImmatriculation");
  }
  
  private void rafraichirPanneauTransporteur() {
    Trace.info("---> Début rafraichirPanneauTransporteur");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.isLivraison()) {
      pnlTransporteur.setVisible(true);
      rafraichirTransporteur();
      rafraichirLivraisonPartielle();
      rafraichirFranco();
      rafraichirLivraisonSouhaitee();
      rafraichirLivraisonPrevue();
    }
    else {
      pnlTransporteur.setVisible(false);
    }
    Trace.info("---> Fin   rafraichirPanneauTransporteur");
  }
  
  private void rafraichirTransporteur() {
    Trace.info("----> Début rafraichirTransporteur");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    IdTransporteur idTransporteur = documentVente.getTransport().getIdTransporteur();
    snTransporteur.setSession(modele.getSession());
    snTransporteur.setIdEtablissement(modele.getEtablissement().getId());
    snTransporteur.setTousAutorise(true);
    snTransporteur.charger(false);
    snTransporteur.setIdSelection(idTransporteur);
    snTransporteur.setEnabled(documentVente.isModifiable());
    Trace.info("----> Fin   rafraichirTransporteur");
  }
  
  private void rafraichirLivraisonPartielle() {
    Trace.info("----> Début rafraichirLivraisonPartielle");
    // Initialiser la liste
    if (cbLivraisonPartielle.getItemCount() == 0) {
      cbLivraisonPartielle.removeAllItems();
      cbLivraisonPartielle.setModel(new DefaultComboBoxModel(EnumComboLivraisonPartielle.values()));
    }
    
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null) {
      cbLivraisonPartielle.setSelectedItem(EnumComboLivraisonPartielle.valueOfByCode(modele.getLivraisonPartielle()));
      cbLivraisonPartielle.setEnabled(documentVente.isModifiable());
    }
    else {
      cbLivraisonPartielle.setSelectedItem(null);
      cbLivraisonPartielle.setEnabled(false);
    }
    Trace.info("----> Fin   rafraichirLivraisonPartielle");
  }
  
  private void rafraichirFranco() {
    Trace.info("----> Début rafraichirFranco");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null) {
      tfFranco.setText(Constantes.convertirIntegerEnTexte(modele.getClientCourant().getMontantFrancoPort(), 0));
      tfFranco.setEnabled(documentVente.isModifiable());
    }
    else {
      tfFranco.setText("");
      tfFranco.setEnabled(false);
    }
    Trace.info("----> Fin   rafraichirFranco");
  }
  
  private void rafraichirLivraisonSouhaitee() {
    Trace.info("----> Début rafraichirLivraisonSouhaitee");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.getTransport() != null) {
      calDateLivraisonSouhaitee.setDate(documentVente.getTransport().getDateLivraisonSouhaitee());
      calDateLivraisonSouhaitee.setEnabled(documentVente.isModifiable());
    }
    else {
      calDateLivraisonSouhaitee.setDate(null);
      calDateLivraisonSouhaitee.setEnabled(true);
    }
    Trace.info("----> Début rafraichirLivraisonSouhaitee");
  }
  
  private void rafraichirLivraisonPrevue() {
    Trace.info("----> Début rafraichirLivraisonPrevue");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.getTransport() != null) {
      calDateLivraisonPrevue.setDate(documentVente.getTransport().getDateLivraisonPrevue());
      calDateLivraisonPrevue.setEnabled(documentVente.isModifiable());
    }
    else {
      calDateLivraisonPrevue.setDate(null);
      calDateLivraisonPrevue.setEnabled(false);
    }
    Trace.info("----> Fin   rafraichirLivraisonPrevue");
  }
  
  private void rafraichirPanneauChantier() {
    Trace.info("---> Début rafraichirPanneauChantier");
    if (modele.isAfficherChantier()) {
      pnlChantier.setVisible(true);
      rafraichirChantier();
      rafraichirChantierReferenceCourte();
      rafraichirChantierDateDebut();
      rafraichirChantierDateFin();
    }
    else {
      pnlChantier.setVisible(false);
    }
    Trace.info("---> Fin   rafraichirPanneauChantier");
  }
  
  private void rafraichirChantier() {
    Trace.info("----> Début rafraichirChantier");
    if (modele.getEtablissement() != null && modele.getClientCourant() != null) {
      snChantier.setSession(modele.getSession());
      snChantier.setIdEtablissement(modele.getEtablissement().getId());
      snChantier.setIdClient(modele.getClientCourant().getId());
      snChantier.setSelection(modele.getChantier());
      snChantier.setEnabled(modele.isDonneesChargees() && modele.isChantierModifiable());
      if (modele.getDocumentVenteEnCours().isDevis()) {
        snChantier.setModeComposant(ModeleChantier.MODE_COMPOSANT_SELECTION_AVEC_BLOQUES);
      }
      else {
        snChantier.setModeComposant(ModeleChantier.MODE_COMPOSANT_SELECTION);
      }
    }
    else {
      snChantier.setSelection(null);
      snChantier.setEnabled(false);
    }
    Trace.info("----> Fin   rafraichirChantier");
  }
  
  /**
   * Met à jour la référence courte du chantier sélectionné
   */
  private void rafraichirChantierReferenceCourte() {
    Trace.info("----> Début rafraichirChantierReferenceCourte");
    Chantier chantier = modele.getChantier();
    if (chantier != null && chantier.getReferenceCourte() != null) {
      tfChantierReferenceCourte.setText(chantier.getReferenceCourte());
    }
    else {
      tfChantierReferenceCourte.setText("");
    }
    tfChantierReferenceCourte.setEnabled(false);
    Trace.info("----> Fin   rafraichirChantierReferenceCourte");
  }
  
  /**
   * Met à jour la date de début de validité du chantier sélectionné
   */
  private void rafraichirChantierDateDebut() {
    Trace.info("----> Début rafraichirChantierDateDebut");
    Chantier chantier = modele.getChantier();
    if (chantier != null && chantier.getDateDebutValidite() != null) {
      calChantierDateDebut.setDate(chantier.getDateDebutValidite());
    }
    else {
      calChantierDateDebut.setDate(null);
    }
    calChantierDateDebut.setEnabled(false);
    Trace.info("----> Fin   rafraichirChantierDateDebut");
  }
  
  /**
   * Met à jour la date de fin de validité du chantier sélectionné
   */
  private void rafraichirChantierDateFin() {
    Trace.info("----> Début rafraichirChantierDateFin");
    Chantier chantier = modele.getChantier();
    if (chantier != null && chantier.getDateFinValidite() != null) {
      calChantierDateFin.setDate(chantier.getDateFinValidite());
    }
    else {
      calChantierDateFin.setDate(null);
    }
    calChantierDateFin.setEnabled(false);
    Trace.info("----> Fin   rafraichirChantierDateFin");
  }
  
  private void rafraichirTitreBlocNotesLivraison() {
    Trace.info("---> Début rafraichirTitreBlocNotesLivraison");
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente == null) {
      lbTitreZoneSaisie.setVisible(false);
    }
    else if (documentVente.isLivraison()) {
      lbTitreZoneSaisie.setText("Informations de livraison");
    }
    else {
      lbTitreZoneSaisie.setText("Informations d'enlèvement");
    }
    Trace.info("---> Fin   rafraichirTitreBlocNotesLivraison");
  }
  
  private void rafraichirBoutonSelectionAdresseLivraison() {
    Trace.info("---> Début rafraichirBoutonSelectionAdresseLivraison");
    btnAutresAdresses.setVisible(modele.isSelectionAdresseChantier());
    Trace.info("---> Fin   rafraichirBoutonSelectionAdresseLivraison");
  }
  
  /**
   * Permet de filtrer les contacts dans la combobox.
   */
  private void filterContact(String pTexteSaisi) {
    Trace.info("---> Début filterContact");
    if (listeNomsContacts == null) {
      Trace.info("---> Fin   filterContact");
      return;
    }
    
    List<String> filterArray = new ArrayList<String>();
    
    // On filtre sur les noms de fournisseur
    for (String contact : listeNomsContacts) {
      if (contact.toLowerCase().startsWith(pTexteSaisi.toLowerCase())) {
        filterArray.add(contact);
      }
    }
    if (filterArray.size() > 0) {
      cbPrisPar.setModel(new DefaultComboBoxModel(filterArray.toArray()));
      cbPrisPar.setSelectedItem(pTexteSaisi);
      cbPrisPar.showPopup();
    }
    else {
      cbPrisPar.hidePopup();
    }
    Trace.info("---> Fin   filterContact");
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        modele.validerOngletLivraison();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.annulerDocumentVente();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbModeEnlevementItemStateChanged(ItemEvent e) {
    try {
      if (executerEvenements && e.getStateChange() == ItemEvent.SELECTED) {
        modele.modifierModeLivraison(IdModeExpedition.getInstance(IdModeExpedition.CODE_ENLEVEMENT));
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbModeLivraisonItemStateChanged(ItemEvent e) {
    try {
      if (executerEvenements && e.getStateChange() == ItemEvent.SELECTED) {
        modele.modifierModeLivraison(IdModeExpedition.getInstance(IdModeExpedition.CODE_LIVRAISON));
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfReferenceDocumentFocusLost(FocusEvent e) {
    try {
      modele.modifierReferenceDocument(tfReferenceDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfDateTraitementDocumentFocusLost(FocusEvent e) {
    try {
      // Si le champ est modifiable, ce n'est pas la date du document qui est modifiée mais la date de traitement
      if (calDateTraitementDocument.isEnabled()) {
        modele.modifierDateTraitement(calDateTraitementDocument.getDate());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichirDateDocument();
    }
  }
  
  private void tfNomLivraisonFocusLost(FocusEvent e) {
    try {
      modele.modifierNomLivraison(tfNomLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfComplementNomLivraisonFocusLost(FocusEvent e) {
    try {
      modele.modifierComplementNomLivraison(tfComplementNomLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRueLivraisonFocusLost(FocusEvent e) {
    try {
      modele.modifierRueLivraison(tfRueLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLocalisationLivraisonFocusLost(FocusEvent e) {
    try {
      modele.modifierLocalisationLivraison(tfLocalisationLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfContactLivraisonFocusLost(FocusEvent e) {
    try {
      modele.modifierContactLivraison(tfContactLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTelephoneLivraisonFocusLost(FocusEvent e) {
    try {
      modele.modifierTelephoneLivraison(tfTelephoneLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfFaxLivraisonFocusLost(FocusEvent e) {
    try {
      modele.modifierFaxLivraison(tfFaxLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfEmailLivraisonFocusLost(FocusEvent e) {
    try {
      modele.modifierMailLivraison(tfEmailLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbLivraisonPartielleItemStateChanged(ItemEvent e) {
    try {
      if (executerEvenements && e.getStateChange() == ItemEvent.SELECTED) {
        modele.modifierLivraisonPartielle(((EnumComboLivraisonPartielle) cbLivraisonPartielle.getSelectedItem()));
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfImmatriculationFocusLost(FocusEvent e) {
    try {
      modele.modifierImmatriculation(tfImmatriculation.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void taInformationLivraisonEnlevementFocusLost(FocusEvent e) {
    try {
      modele.modifierTexteLivraisonEnlevement(taInformationLivraisonEnlevement.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckDirectUsineActionPerformed(ActionEvent e) {
    try {
      modele.modifierDirectUsine(ckDirectUsine.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfDateValiditeDocumentFocusLost(FocusEvent e) {
    try {
      modele.modifierDateValiditeDocument(calDateValiditeDocument.getDate());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfDateRelanceDocumentFocusLost(FocusEvent e) {
    try {
      modele.modifierDateRelanceDocument(calDateRelanceDocument.getDate());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfDateLivraisonSouhaiteeFocusLost(FocusEvent e) {
    try {
      modele.modifierDateLivraisonSouhaitee(calDateLivraisonSouhaitee.getDate());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfDateLivraisonPrevueFocusLost(FocusEvent e) {
    try {
      modele.modifierDateLivraisonPrevue(calDateLivraisonPrevue.getDate());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfFrancoActionPerformed(ActionEvent e) {
    try {
      modele.modifierFranco(tfFranco.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfFrancoFocusLost(FocusEvent e) {
    try {
      modele.modifierFranco(tfFranco.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfReferenceCourteDocumentFocusLost(FocusEvent e) {
    try {
      modele.modifierReferenceCourteDocument(tfReferenceCourteDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbPrisParFocusLost(FocusEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierPrisPar(((JTextField) cbPrisPar.getEditor().getEditorComponent()).getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snChantierValueChanged(SNComposantEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierChantier(snChantier.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbTypesFacturationValueChanged(SNComposantEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierTypesFacturation(cbTypesFacturation.getSelection(), true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snTransporteurValueChanged(SNComposantEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierTransporteur(snTransporteur.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snCodePostalCommuneLivraisonValueChanged(SNComposantEvent e) {
    try {
      if (executerEvenements) {
        if (snCodePostalCommuneLivraison.getSelection() != null && snCodePostalCommuneLivraison.getSelection().isComplet()) {
          modele.modifierCodePostalCommuneLivraison(snCodePostalCommuneLivraison.getSelection());
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btnAutresAdressesActionPerformed(ActionEvent e) {
    try {
      modele.modifierAdresseLivraison();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNomFacturationFocusLost(FocusEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierNomFacturation(tfNomFacturation.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfComplementNomFacturationFocusLost(FocusEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierComplementNomFacturation(tfComplementNomFacturation.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfRueFacturationFocusLost(FocusEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierRueFacturation(tfRueFacturation.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfLocalisationFacturationFocusLost(FocusEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierLocalisationFacturation(tfLocalisationFacturation.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snCodePostalCommuneFacturationValueChanged(SNComposantEvent e) {
    try {
      if (executerEvenements) {
        if (snCodePostalCommuneFacturation.getSelection() != null && snCodePostalCommuneFacturation.getSelection().isComplet()) {
          modele.modifierCodePostalCommuneFacturation(snCodePostalCommuneFacturation.getSelection());
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfContactFacturationFocusLost(FocusEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierContactFacturation(tfContactFacturation.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfTelephoneFacturationFocusLost(FocusEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierTelephoneFacturation(tfTelephoneFacturation.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfEmailFacturationFocusLost(FocusEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierEmailFacturation(tfEmailFacturation.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfFaxFacturationFocusLost(FocusEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierFaxFacturation(tfFaxFacturation.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new JPanel();
    lbTitreZoneSaisie = new SNLabelTitre();
    pnlTypeDocument = new JPanel();
    rbModeEnlevement = new JRadioButton();
    rbModeLivraison = new JRadioButton();
    ckDirectUsine = new JCheckBox();
    pnlInfosDocument = new JPanel();
    pnlReferenceCourte = new SNPanel();
    lbReferenceCourteDocument = new SNLabelChamp();
    tfReferenceCourteDocument = new SNTexte();
    lbTypeFacturation = new SNLabelChamp();
    cbTypesFacturation = new SNTypeFacturation();
    pnlReferenceLongue = new SNPanel();
    lbReferenceDocument = new SNLabelChamp();
    tfReferenceDocument = new SNTexte();
    pnlDateDocument = new SNPanel();
    lbDateTraitementDocument = new SNLabelChamp();
    calDateTraitementDocument = new XRiCalendrier();
    lbDateValiditeDocument = new SNLabelChamp();
    calDateValiditeDocument = new XRiCalendrier();
    lbDateRelanceDocument = new SNLabelChamp();
    calDateRelanceDocument = new XRiCalendrier();
    sNPanel2 = new SNPanel();
    sNLabelChamp1 = new SNLabelChamp();
    sNContact1 = new SNContact();
    pnlInformationLivraisonEnlevement = new SNPanel();
    scpInformationLivraisonEnlevement = new JScrollPane();
    taInformationLivraisonEnlevement = new RiTextArea();
    pnlEnvTrans = new SNPanel();
    pnlEnlevement = new JPanel();
    lbPrisPar = new JLabel();
    cbPrisPar = new JComboBox();
    lbImmatriculation = new JLabel();
    tfImmatriculation = new SNTexte();
    pnlTransporteur = new JPanel();
    lbTransporteurs = new JLabel();
    lbTransporteurs2 = new JLabel();
    snTransporteur = new SNTransporteur();
    snTransporteur2 = new SNTransporteur();
    lbLivraisonPartielle = new JLabel();
    lbLivraisonPartielle2 = new JLabel();
    cbLivraisonPartielle = new XRiComboBox();
    cbLivraisonPartielle2 = new XRiComboBox();
    lbFranco = new JLabel();
    lbFranco2 = new JLabel();
    tfFranco = new SNTexte();
    lbDateLivraisonSouhaitee = new JLabel();
    calDateLivraisonSouhaitee = new XRiCalendrier();
    lbDateLivraisonPrevue = new JLabel();
    calDateLivraisonPrevue = new XRiCalendrier();
    tabAdresses = new JTabbedPane();
    pnlCoordonneesClientFacturation = new SNPanelContenu();
    lbNomFacturation = new SNLabelChamp();
    tfNomFacturation = new SNTexte();
    lbComplementNomFacturation = new JLabel();
    tfComplementNomFacturation = new SNTexte();
    lbLocalisationFacturation = new JLabel();
    tfRueFacturation = new SNTexte();
    lbRueFacturation = new JLabel();
    tfLocalisationFacturation = new SNTexte();
    lbCodePostalFacturation = new JLabel();
    snCodePostalCommuneFacturation = new SNCodePostalCommune();
    lbContactFacturation = new JLabel();
    pnlContact2 = new SNPanel();
    tfContactFacturation = new SNTexte();
    lbTelephoneFacturation = new JLabel();
    tfTelephoneFacturation = new SNTexte();
    lbEmailFacturation = new JLabel();
    pnlEmail2 = new SNPanel();
    tfEmailFacturation = new SNTexte();
    lbFaxFacturation = new JLabel();
    tfFaxFacturation = new SNTexte();
    pnlCoordonneesClientLivraison = new SNPanelContenu();
    lbNomLivraison = new SNLabelChamp();
    tfNomLivraison = new SNTexte();
    lbComplementNomLivraison = new JLabel();
    tfComplementNomLivraison = new SNTexte();
    lbLocalisationLivraison = new JLabel();
    tfRueLivraison = new SNTexte();
    lbRueLivraison = new JLabel();
    tfLocalisationLivraison = new SNTexte();
    lbCodePostalLivraison = new JLabel();
    snCodePostalCommuneLivraison = new SNCodePostalCommune();
    lbContactLivraison = new JLabel();
    pnlContact = new SNPanel();
    tfContactLivraison = new SNTexte();
    lbTelephoneLivraison = new JLabel();
    tfTelephoneLivraison = new SNTexte();
    lbEmailLivraison = new JLabel();
    pnlEmail = new SNPanel();
    tfEmailLivraison = new SNTexte();
    lbFaxLivraison = new JLabel();
    tfFaxLivraison = new SNTexte();
    btnAutresAdresses = new SNBoutonDetail();
    sNPanel1 = new SNPanel();
    pnlChantier = new SNPanelTitre();
    lbChantierReferenceCourte2 = new SNLabelChamp();
    snChantier = new SNChantier();
    lbChantierReferenceCourte = new SNLabelChamp();
    tfChantierReferenceCourte = new SNTexte();
    lbChantierDateDebut = new SNLabelChamp();
    pnlDateChantier = new SNPanel();
    calChantierDateDebut = new XRiCalendrier();
    lbChantierDateFin = new SNLabelChamp();
    calChantierDateFin = new XRiCalendrier();
    snBarreBouton = new SNBarreBouton();
    btgMode = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1240, 660));
    setPreferredSize(new Dimension(1240, 660));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setOpaque(false);
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setPreferredSize(new Dimension(1226, 700));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 1.0E-4 };
      
      // ---- lbTitreZoneSaisie ----
      lbTitreZoneSaisie.setText("Voir code");
      lbTitreZoneSaisie.setName("lbTitreZoneSaisie");
      pnlContenu.add(lbTitreZoneSaisie,
          new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlTypeDocument ========
      {
        pnlTypeDocument.setBorder(new TitledBorder(""));
        pnlTypeDocument.setOpaque(false);
        pnlTypeDocument.setMinimumSize(new Dimension(150, 120));
        pnlTypeDocument.setPreferredSize(new Dimension(150, 120));
        pnlTypeDocument.setMaximumSize(new Dimension(190, 2147483647));
        pnlTypeDocument.setName("pnlTypeDocument");
        pnlTypeDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlTypeDocument.getLayout()).columnWidths = new int[] { 150, 0 };
        ((GridBagLayout) pnlTypeDocument.getLayout()).rowHeights = new int[] { 36, 36, 35, 0 };
        ((GridBagLayout) pnlTypeDocument.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlTypeDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- rbModeEnlevement ----
        rbModeEnlevement.setText("Enl\u00e8vement");
        rbModeEnlevement.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbModeEnlevement.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbModeEnlevement.setDisplayedMnemonicIndex(0);
        rbModeEnlevement.setName("rbModeEnlevement");
        rbModeEnlevement.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbModeEnlevementItemStateChanged(e);
          }
        });
        pnlTypeDocument.add(rbModeEnlevement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(5, 20, 5, 0), 0, 0));
        
        // ---- rbModeLivraison ----
        rbModeLivraison.setText("Livraison");
        rbModeLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbModeLivraison.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbModeLivraison.setDisplayedMnemonicIndex(0);
        rbModeLivraison.setName("rbModeLivraison");
        rbModeLivraison.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbModeLivraisonItemStateChanged(e);
          }
        });
        pnlTypeDocument.add(rbModeLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 20, 5, 0), 0, 0));
        
        // ---- ckDirectUsine ----
        ckDirectUsine.setText("<html>Direct <u>u</u>sine</html>");
        ckDirectUsine.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ckDirectUsine.setFont(ckDirectUsine.getFont().deriveFont(ckDirectUsine.getFont().getSize() + 2f));
        ckDirectUsine.setName("ckDirectUsine");
        ckDirectUsine.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            ckDirectUsineActionPerformed(e);
          }
        });
        pnlTypeDocument.add(ckDirectUsine, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 20, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlTypeDocument,
          new GridBagConstraints(0, 0, 1, 2, 0.1, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlInfosDocument ========
      {
        pnlInfosDocument.setOpaque(false);
        pnlInfosDocument.setPreferredSize(new Dimension(600, 160));
        pnlInfosDocument.setMinimumSize(new Dimension(600, 160));
        pnlInfosDocument.setBorder(new CompoundBorder(
            new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)),
            new EmptyBorder(10, 10, 10, 10)));
        pnlInfosDocument.setMaximumSize(new Dimension(600, 160));
        pnlInfosDocument.setName("pnlInfosDocument");
        pnlInfosDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInfosDocument.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlInfosDocument.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInfosDocument.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlInfosDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlReferenceCourte ========
        {
          pnlReferenceCourte.setMaximumSize(new Dimension(590, 30));
          pnlReferenceCourte.setMinimumSize(new Dimension(590, 30));
          pnlReferenceCourte.setName("pnlReferenceCourte");
          pnlReferenceCourte.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlReferenceCourte.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlReferenceCourte.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlReferenceCourte.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlReferenceCourte.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbReferenceCourteDocument ----
          lbReferenceCourteDocument.setText("R\u00e9f\u00e9rence courte");
          lbReferenceCourteDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbReferenceCourteDocument.setHorizontalAlignment(SwingConstants.RIGHT);
          lbReferenceCourteDocument.setPreferredSize(new Dimension(115, 30));
          lbReferenceCourteDocument.setMinimumSize(new Dimension(115, 19));
          lbReferenceCourteDocument.setMaximumSize(new Dimension(115, 19));
          lbReferenceCourteDocument.setName("lbReferenceCourteDocument");
          pnlReferenceCourte.add(lbReferenceCourteDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfReferenceCourteDocument ----
          tfReferenceCourteDocument.setComponentPopupMenu(null);
          tfReferenceCourteDocument.setMinimumSize(new Dimension(180, 30));
          tfReferenceCourteDocument.setPreferredSize(new Dimension(180, 30));
          tfReferenceCourteDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfReferenceCourteDocument.setName("tfReferenceCourteDocument");
          tfReferenceCourteDocument.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfReferenceCourteDocumentFocusLost(e);
            }
          });
          pnlReferenceCourte.add(tfReferenceCourteDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbTypeFacturation ----
          lbTypeFacturation.setText("Type de facturation");
          lbTypeFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTypeFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTypeFacturation.setPreferredSize(new Dimension(130, 30));
          lbTypeFacturation.setName("lbTypeFacturation");
          pnlReferenceCourte.add(lbTypeFacturation, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- cbTypesFacturation ----
          cbTypesFacturation.setPreferredSize(new Dimension(150, 30));
          cbTypesFacturation.setMinimumSize(new Dimension(150, 30));
          cbTypesFacturation.setName("cbTypesFacturation");
          cbTypesFacturation.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              cbTypesFacturationValueChanged(e);
            }
          });
          pnlReferenceCourte.add(cbTypesFacturation, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInfosDocument.add(pnlReferenceCourte, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlReferenceLongue ========
        {
          pnlReferenceLongue.setMaximumSize(new Dimension(590, 30));
          pnlReferenceLongue.setMinimumSize(new Dimension(590, 30));
          pnlReferenceLongue.setPreferredSize(new Dimension(590, 30));
          pnlReferenceLongue.setName("pnlReferenceLongue");
          pnlReferenceLongue.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlReferenceLongue.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlReferenceLongue.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlReferenceLongue.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlReferenceLongue.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbReferenceDocument ----
          lbReferenceDocument.setText("R\u00e9f\u00e9rence longue");
          lbReferenceDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbReferenceDocument.setHorizontalAlignment(SwingConstants.RIGHT);
          lbReferenceDocument.setPreferredSize(new Dimension(115, 30));
          lbReferenceDocument.setMinimumSize(new Dimension(115, 19));
          lbReferenceDocument.setMaximumSize(new Dimension(115, 19));
          lbReferenceDocument.setName("lbReferenceDocument");
          pnlReferenceLongue.add(lbReferenceDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfReferenceDocument ----
          tfReferenceDocument.setComponentPopupMenu(null);
          tfReferenceDocument.setMinimumSize(new Dimension(350, 30));
          tfReferenceDocument.setPreferredSize(new Dimension(350, 30));
          tfReferenceDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfReferenceDocument.setName("tfReferenceDocument");
          tfReferenceDocument.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfReferenceDocumentFocusLost(e);
            }
          });
          pnlReferenceLongue.add(tfReferenceDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInfosDocument.add(pnlReferenceLongue, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlDateDocument ========
        {
          pnlDateDocument.setMaximumSize(new Dimension(590, 30));
          pnlDateDocument.setMinimumSize(new Dimension(590, 30));
          pnlDateDocument.setPreferredSize(new Dimension(590, 30));
          pnlDateDocument.setName("pnlDateDocument");
          pnlDateDocument.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDateDocument.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlDateDocument.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDateDocument.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDateDocument.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbDateTraitementDocument ----
          lbDateTraitementDocument.setText("Date document");
          lbDateTraitementDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateTraitementDocument.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDateTraitementDocument.setMaximumSize(new Dimension(115, 30));
          lbDateTraitementDocument.setMinimumSize(new Dimension(115, 30));
          lbDateTraitementDocument.setPreferredSize(new Dimension(115, 30));
          lbDateTraitementDocument.setName("lbDateTraitementDocument");
          pnlDateDocument.add(lbDateTraitementDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- calDateTraitementDocument ----
          calDateTraitementDocument.setComponentPopupMenu(null);
          calDateTraitementDocument.setPreferredSize(new Dimension(115, 30));
          calDateTraitementDocument.setMinimumSize(new Dimension(115, 30));
          calDateTraitementDocument.setMaximumSize(new Dimension(105, 30));
          calDateTraitementDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          calDateTraitementDocument.setName("calDateTraitementDocument");
          pnlDateDocument.add(calDateTraitementDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbDateValiditeDocument ----
          lbDateValiditeDocument.setText("Validit\u00e9");
          lbDateValiditeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateValiditeDocument.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDateValiditeDocument.setPreferredSize(new Dimension(50, 30));
          lbDateValiditeDocument.setMaximumSize(new Dimension(50, 30));
          lbDateValiditeDocument.setMinimumSize(new Dimension(50, 30));
          lbDateValiditeDocument.setName("lbDateValiditeDocument");
          pnlDateDocument.add(lbDateValiditeDocument, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- calDateValiditeDocument ----
          calDateValiditeDocument.setComponentPopupMenu(null);
          calDateValiditeDocument.setPreferredSize(new Dimension(115, 30));
          calDateValiditeDocument.setMinimumSize(new Dimension(115, 30));
          calDateValiditeDocument.setMaximumSize(new Dimension(105, 30));
          calDateValiditeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          calDateValiditeDocument.setName("calDateValiditeDocument");
          pnlDateDocument.add(calDateValiditeDocument, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbDateRelanceDocument ----
          lbDateRelanceDocument.setText("Relance");
          lbDateRelanceDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateRelanceDocument.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDateRelanceDocument.setPreferredSize(new Dimension(60, 30));
          lbDateRelanceDocument.setMinimumSize(new Dimension(60, 30));
          lbDateRelanceDocument.setMaximumSize(new Dimension(60, 30));
          lbDateRelanceDocument.setName("lbDateRelanceDocument");
          pnlDateDocument.add(lbDateRelanceDocument, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- calDateRelanceDocument ----
          calDateRelanceDocument.setComponentPopupMenu(null);
          calDateRelanceDocument.setPreferredSize(new Dimension(115, 30));
          calDateRelanceDocument.setMinimumSize(new Dimension(115, 30));
          calDateRelanceDocument.setMaximumSize(new Dimension(105, 30));
          calDateRelanceDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          calDateRelanceDocument.setName("calDateRelanceDocument");
          pnlDateDocument.add(calDateRelanceDocument, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInfosDocument.add(pnlDateDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanel2 ========
        {
          sNPanel2.setName("sNPanel2");
          sNPanel2.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- sNLabelChamp1 ----
          sNLabelChamp1.setText("Contact");
          sNLabelChamp1.setMaximumSize(new Dimension(115, 30));
          sNLabelChamp1.setMinimumSize(new Dimension(115, 30));
          sNLabelChamp1.setPreferredSize(new Dimension(115, 30));
          sNLabelChamp1.setName("lbContact");
          sNPanel2.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- sNContact1 ----
          sNContact1.setMaximumSize(new Dimension(400, 30));
          sNContact1.setMinimumSize(new Dimension(400, 30));
          sNContact1.setPreferredSize(new Dimension(400, 30));
          sNContact1.setName("sNContact1");
          sNPanel2.add(sNContact1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInfosDocument.add(sNPanel2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInfosDocument,
          new GridBagConstraints(1, 0, 1, 2, 0.4, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlInformationLivraisonEnlevement ========
      {
        pnlInformationLivraisonEnlevement.setName("p_Chantier");
        pnlInformationLivraisonEnlevement.setOpaque(false);
        pnlInformationLivraisonEnlevement.setLayout(new BorderLayout());
        
        // ======== scpInformationLivraisonEnlevement ========
        {
          scpInformationLivraisonEnlevement.setName("scpInformationLivraisonEnlevement");
          
          // ---- taInformationLivraisonEnlevement ----
          taInformationLivraisonEnlevement.setFont(new Font("sansserif", Font.PLAIN, 14));
          taInformationLivraisonEnlevement.setName("taInformationLivraisonEnlevement");
          taInformationLivraisonEnlevement.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              taInformationLivraisonEnlevementFocusLost(e);
            }
          });
          scpInformationLivraisonEnlevement.setViewportView(taInformationLivraisonEnlevement);
        }
        pnlInformationLivraisonEnlevement.add(scpInformationLivraisonEnlevement, BorderLayout.CENTER);
      }
      pnlContenu.add(pnlInformationLivraisonEnlevement,
          new GridBagConstraints(2, 1, 1, 1, 0.4, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlEnvTrans ========
      {
        pnlEnvTrans.setName("pnlEnvTrans");
        pnlEnvTrans.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEnvTrans.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlEnvTrans.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlEnvTrans.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlEnvTrans.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEnlevement ========
        {
          pnlEnlevement.setBorder(new TitledBorder(null, "Enl\u00e8vement", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
              new Font("sansserif", Font.BOLD, 14)));
          pnlEnlevement.setOpaque(false);
          pnlEnlevement.setMinimumSize(new Dimension(440, 110));
          pnlEnlevement.setPreferredSize(new Dimension(440, 110));
          pnlEnlevement.setName("pnlEnlevement");
          pnlEnlevement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEnlevement.getLayout()).columnWidths = new int[] { 105, 280, 0 };
          ((GridBagLayout) pnlEnlevement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEnlevement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlEnlevement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbPrisPar ----
          lbPrisPar.setText("Pris par");
          lbPrisPar.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbPrisPar.setName("lbPrisPar");
          pnlEnlevement.add(lbPrisPar, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- cbPrisPar ----
          cbPrisPar.setFont(new Font("sansserif", Font.PLAIN, 14));
          cbPrisPar.setPreferredSize(new Dimension(12, 30));
          cbPrisPar.setName("cbPrisPar");
          pnlEnlevement.add(cbPrisPar, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbImmatriculation ----
          lbImmatriculation.setText("Immatriculation");
          lbImmatriculation.setFont(lbImmatriculation.getFont().deriveFont(lbImmatriculation.getFont().getSize() + 2f));
          lbImmatriculation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbImmatriculation.setMinimumSize(new Dimension(94, 19));
          lbImmatriculation.setPreferredSize(new Dimension(94, 19));
          lbImmatriculation.setName("lbImmatriculation");
          pnlEnlevement.add(lbImmatriculation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfImmatriculation ----
          tfImmatriculation.setMaximumSize(new Dimension(120, 30));
          tfImmatriculation.setMinimumSize(new Dimension(120, 30));
          tfImmatriculation.setPreferredSize(new Dimension(120, 30));
          tfImmatriculation.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfImmatriculation.setName("tfImmatriculation");
          tfImmatriculation.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfImmatriculationFocusLost(e);
            }
          });
          pnlEnlevement.add(tfImmatriculation, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlEnvTrans.add(pnlEnlevement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTransporteur ========
        {
          pnlTransporteur.setBorder(new TitledBorder(null, "Transport", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
              new Font("sansserif", Font.BOLD, 14)));
          pnlTransporteur.setOpaque(false);
          pnlTransporteur.setMinimumSize(new Dimension(446, 200));
          pnlTransporteur.setPreferredSize(new Dimension(446, 200));
          pnlTransporteur.setName("pnlTransporteur");
          pnlTransporteur.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTransporteur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTransporteur.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTransporteur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTransporteur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbTransporteurs ----
          lbTransporteurs.setText("Transporteur");
          lbTransporteurs.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTransporteurs.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTransporteurs.setName("lbTransporteurs");
          pnlTransporteur.add(lbTransporteurs, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTransporteurs2 ----
          lbTransporteurs2.setText("Transporteur");
          lbTransporteurs2.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTransporteurs2.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTransporteurs2.setName("lbTransporteurs2");
          pnlTransporteur.add(lbTransporteurs2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snTransporteur ----
          snTransporteur.setPreferredSize(new Dimension(290, 30));
          snTransporteur.setMinimumSize(new Dimension(290, 30));
          snTransporteur.setMaximumSize(new Dimension(290, 30));
          snTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
          snTransporteur.setName("snTransporteur");
          snTransporteur.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snTransporteurValueChanged(e);
            }
          });
          pnlTransporteur.add(snTransporteur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- snTransporteur2 ----
          snTransporteur2.setPreferredSize(new Dimension(290, 30));
          snTransporteur2.setMinimumSize(new Dimension(290, 30));
          snTransporteur2.setMaximumSize(new Dimension(290, 30));
          snTransporteur2.setFont(new Font("sansserif", Font.PLAIN, 14));
          snTransporteur2.setName("snTransporteur2");
          snTransporteur2.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snTransporteurValueChanged(e);
            }
          });
          pnlTransporteur.add(snTransporteur2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLivraisonPartielle ----
          lbLivraisonPartielle.setText("Livraison partielle");
          lbLivraisonPartielle.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLivraisonPartielle.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbLivraisonPartielle.setName("lbLivraisonPartielle");
          pnlTransporteur.add(lbLivraisonPartielle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbLivraisonPartielle2 ----
          lbLivraisonPartielle2.setText("Livraison partielle");
          lbLivraisonPartielle2.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLivraisonPartielle2.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbLivraisonPartielle2.setName("lbLivraisonPartielle2");
          pnlTransporteur.add(lbLivraisonPartielle2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- cbLivraisonPartielle ----
          cbLivraisonPartielle.setBackground(new Color(171, 148, 79));
          cbLivraisonPartielle.setFont(new Font("sansserif", Font.PLAIN, 14));
          cbLivraisonPartielle.setPreferredSize(new Dimension(215, 30));
          cbLivraisonPartielle.setMinimumSize(new Dimension(215, 30));
          cbLivraisonPartielle.setName("cbLivraisonPartielle");
          cbLivraisonPartielle.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              cbLivraisonPartielleItemStateChanged(e);
            }
          });
          pnlTransporteur.add(cbLivraisonPartielle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- cbLivraisonPartielle2 ----
          cbLivraisonPartielle2.setBackground(new Color(171, 148, 79));
          cbLivraisonPartielle2.setFont(new Font("sansserif", Font.PLAIN, 14));
          cbLivraisonPartielle2.setPreferredSize(new Dimension(215, 30));
          cbLivraisonPartielle2.setMinimumSize(new Dimension(215, 30));
          cbLivraisonPartielle2.setName("cbLivraisonPartielle2");
          cbLivraisonPartielle2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              cbLivraisonPartielleItemStateChanged(e);
            }
          });
          pnlTransporteur.add(cbLivraisonPartielle2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbFranco ----
          lbFranco.setText("Franco");
          lbFranco.setHorizontalAlignment(SwingConstants.RIGHT);
          lbFranco.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbFranco.setName("lbFranco");
          pnlTransporteur.add(lbFranco, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbFranco2 ----
          lbFranco2.setText("Franco");
          lbFranco2.setHorizontalAlignment(SwingConstants.RIGHT);
          lbFranco2.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbFranco2.setName("lbFranco2");
          pnlTransporteur.add(lbFranco2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfFranco ----
          tfFranco.setComponentPopupMenu(null);
          tfFranco.setMinimumSize(new Dimension(105, 30));
          tfFranco.setPreferredSize(new Dimension(105, 30));
          tfFranco.setMaximumSize(new Dimension(2147483647, 30));
          tfFranco.setHorizontalAlignment(SwingConstants.RIGHT);
          tfFranco.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfFranco.setName("tfFranco");
          tfFranco.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tfFrancoActionPerformed(e);
            }
          });
          tfFranco.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfFrancoFocusLost(e);
            }
          });
          pnlTransporteur.add(tfFranco, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbDateLivraisonSouhaitee ----
          lbDateLivraisonSouhaitee.setText("Livraison souhait\u00e9e");
          lbDateLivraisonSouhaitee.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateLivraisonSouhaitee.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDateLivraisonSouhaitee.setName("lbDateLivraisonSouhaitee");
          pnlTransporteur.add(lbDateLivraisonSouhaitee, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- calDateLivraisonSouhaitee ----
          calDateLivraisonSouhaitee.setComponentPopupMenu(null);
          calDateLivraisonSouhaitee.setMinimumSize(new Dimension(115, 30));
          calDateLivraisonSouhaitee.setPreferredSize(new Dimension(115, 30));
          calDateLivraisonSouhaitee.setFont(new Font("sansserif", Font.PLAIN, 14));
          calDateLivraisonSouhaitee.setName("calDateLivraisonSouhaitee");
          pnlTransporteur.add(calDateLivraisonSouhaitee, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbDateLivraisonPrevue ----
          lbDateLivraisonPrevue.setText("Livraison pr\u00e9vue");
          lbDateLivraisonPrevue.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateLivraisonPrevue.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDateLivraisonPrevue.setName("lbDateLivraisonPrevue");
          pnlTransporteur.add(lbDateLivraisonPrevue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- calDateLivraisonPrevue ----
          calDateLivraisonPrevue.setComponentPopupMenu(null);
          calDateLivraisonPrevue.setMinimumSize(new Dimension(115, 30));
          calDateLivraisonPrevue.setPreferredSize(new Dimension(115, 30));
          calDateLivraisonPrevue.setFont(new Font("sansserif", Font.PLAIN, 14));
          calDateLivraisonPrevue.setName("calDateLivraisonPrevue");
          pnlTransporteur.add(calDateLivraisonPrevue, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlEnvTrans.add(pnlTransporteur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlEnvTrans,
          new GridBagConstraints(2, 2, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== tabAdresses ========
      {
        tabAdresses.setFont(new Font("sansserif", Font.PLAIN, 14));
        tabAdresses.setName("tabAdresses");
        
        // ======== pnlCoordonneesClientFacturation ========
        {
          pnlCoordonneesClientFacturation.setName("pnlCoordonneesClientFacturation");
          pnlCoordonneesClientFacturation.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCoordonneesClientFacturation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCoordonneesClientFacturation.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCoordonneesClientFacturation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCoordonneesClientFacturation.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbNomFacturation ----
          lbNomFacturation.setText("Raison sociale");
          lbNomFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbNomFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbNomFacturation.setName("lbNomFacturation");
          pnlCoordonneesClientFacturation.add(lbNomFacturation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNomFacturation ----
          tfNomFacturation.setBackground(Color.white);
          tfNomFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfNomFacturation.setEditable(false);
          tfNomFacturation.setMinimumSize(new Dimension(485, 30));
          tfNomFacturation.setPreferredSize(new Dimension(485, 30));
          tfNomFacturation.setName("tfNomFacturation");
          tfNomFacturation.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNomFacturationFocusLost(e);
            }
          });
          pnlCoordonneesClientFacturation.add(tfNomFacturation, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 1, 1));
          
          // ---- lbComplementNomFacturation ----
          lbComplementNomFacturation.setText("Compl\u00e9ment");
          lbComplementNomFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbComplementNomFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbComplementNomFacturation.setPreferredSize(new Dimension(150, 30));
          lbComplementNomFacturation.setName("lbNomLivraison");
          lbComplementNomFacturation.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientFacturation.add(lbComplementNomFacturation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfComplementNomFacturation ----
          tfComplementNomFacturation.setBackground(Color.white);
          tfComplementNomFacturation.setMinimumSize(new Dimension(485, 30));
          tfComplementNomFacturation.setPreferredSize(new Dimension(485, 30));
          tfComplementNomFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfComplementNomFacturation.setName("tfComplementNomFacturation");
          tfComplementNomFacturation.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfComplementNomFacturationFocusLost(e);
            }
          });
          pnlCoordonneesClientFacturation.add(tfComplementNomFacturation, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbLocalisationFacturation ----
          lbLocalisationFacturation.setText("Adresse 1");
          lbLocalisationFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbLocalisationFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLocalisationFacturation.setPreferredSize(new Dimension(150, 30));
          lbLocalisationFacturation.setName("lbNomLivraison");
          lbLocalisationFacturation.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientFacturation.add(lbLocalisationFacturation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfRueFacturation ----
          tfRueFacturation.setBackground(Color.white);
          tfRueFacturation.setMinimumSize(new Dimension(485, 30));
          tfRueFacturation.setPreferredSize(new Dimension(485, 30));
          tfRueFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfRueFacturation.setName("tfRueFacturation");
          tfRueFacturation.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfRueFacturationFocusLost(e);
            }
          });
          pnlCoordonneesClientFacturation.add(tfRueFacturation, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbRueFacturation ----
          lbRueFacturation.setText("Adresse 2");
          lbRueFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbRueFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbRueFacturation.setPreferredSize(new Dimension(150, 30));
          lbRueFacturation.setName("lbNomLivraison");
          lbRueFacturation.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientFacturation.add(lbRueFacturation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfLocalisationFacturation ----
          tfLocalisationFacturation.setBackground(Color.white);
          tfLocalisationFacturation.setMinimumSize(new Dimension(485, 30));
          tfLocalisationFacturation.setPreferredSize(new Dimension(485, 30));
          tfLocalisationFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfLocalisationFacturation.setName("tfLocalisationFacturation");
          tfLocalisationFacturation.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfLocalisationFacturationFocusLost(e);
            }
          });
          pnlCoordonneesClientFacturation.add(tfLocalisationFacturation, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbCodePostalFacturation ----
          lbCodePostalFacturation.setText("Commune");
          lbCodePostalFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbCodePostalFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCodePostalFacturation.setPreferredSize(new Dimension(150, 30));
          lbCodePostalFacturation.setName("lbNomLivraison");
          lbCodePostalFacturation.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientFacturation.add(lbCodePostalFacturation, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snCodePostalCommuneFacturation ----
          snCodePostalCommuneFacturation.setName("snCodePostalCommuneFacturation");
          snCodePostalCommuneFacturation.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snCodePostalCommuneFacturationValueChanged(e);
            }
          });
          pnlCoordonneesClientFacturation.add(snCodePostalCommuneFacturation, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbContactFacturation ----
          lbContactFacturation.setText("Contact");
          lbContactFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbContactFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbContactFacturation.setDisplayedMnemonicIndex(2);
          lbContactFacturation.setPreferredSize(new Dimension(150, 30));
          lbContactFacturation.setName("lbNomLivraison");
          lbContactFacturation.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientFacturation.add(lbContactFacturation, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlContact2 ========
          {
            pnlContact2.setName("pnlContact2");
            pnlContact2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlContact2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlContact2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlContact2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlContact2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfContactFacturation ----
            tfContactFacturation.setBackground(Color.white);
            tfContactFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfContactFacturation.setMinimumSize(new Dimension(215, 30));
            tfContactFacturation.setPreferredSize(new Dimension(215, 30));
            tfContactFacturation.setName("tfContactFacturation");
            tfContactFacturation.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfContactFacturationFocusLost(e);
              }
            });
            pnlContact2.add(tfContactFacturation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbTelephoneFacturation ----
            lbTelephoneFacturation.setText("T\u00e9l\u00e9phone");
            lbTelephoneFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTelephoneFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTelephoneFacturation.setName("lbTelephoneFacturation");
            pnlContact2.add(lbTelephoneFacturation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfTelephoneFacturation ----
            tfTelephoneFacturation.setBackground(Color.white);
            tfTelephoneFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfTelephoneFacturation.setMinimumSize(new Dimension(140, 30));
            tfTelephoneFacturation.setPreferredSize(new Dimension(140, 30));
            tfTelephoneFacturation.setName("tfTelephoneFacturation");
            tfTelephoneFacturation.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfTelephoneFacturationFocusLost(e);
              }
            });
            pnlContact2.add(tfTelephoneFacturation, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 10), 0, 0));
          }
          pnlCoordonneesClientFacturation.add(pnlContact2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEmailFacturation ----
          lbEmailFacturation.setText("Email");
          lbEmailFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbEmailFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbEmailFacturation.setPreferredSize(new Dimension(150, 30));
          lbEmailFacturation.setName("lbNomLivraison");
          lbEmailFacturation.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientFacturation.add(lbEmailFacturation, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlEmail2 ========
          {
            pnlEmail2.setName("pnlEmail2");
            pnlEmail2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEmail2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlEmail2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlEmail2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlEmail2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfEmailFacturation ----
            tfEmailFacturation.setBackground(Color.white);
            tfEmailFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfEmailFacturation.setMinimumSize(new Dimension(250, 30));
            tfEmailFacturation.setPreferredSize(new Dimension(250, 30));
            tfEmailFacturation.setName("tfEmailFacturation");
            tfEmailFacturation.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfEmailFacturationFocusLost(e);
              }
            });
            pnlEmail2.add(tfEmailFacturation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbFaxFacturation ----
            lbFaxFacturation.setText("Fax");
            lbFaxFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFaxFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFaxFacturation.setName("lbFaxFacturation");
            pnlEmail2.add(lbFaxFacturation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfFaxFacturation ----
            tfFaxFacturation.setBackground(Color.white);
            tfFaxFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfFaxFacturation.setPreferredSize(new Dimension(140, 30));
            tfFaxFacturation.setMinimumSize(new Dimension(140, 30));
            tfFaxFacturation.setName("tfFaxFacturation");
            tfFaxFacturation.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfFaxFacturationFocusLost(e);
              }
            });
            pnlEmail2.add(tfFaxFacturation, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 15), 0, 0));
          }
          pnlCoordonneesClientFacturation.add(pnlEmail2, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        tabAdresses.addTab("Adresse de facturation", pnlCoordonneesClientFacturation);
        
        // ======== pnlCoordonneesClientLivraison ========
        {
          pnlCoordonneesClientLivraison.setName("pnlCoordonneesClientLivraison");
          pnlCoordonneesClientLivraison.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbNomLivraison ----
          lbNomLivraison.setText("Raison sociale");
          lbNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbNomLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbNomLivraison.setName("lbNomLivraison");
          pnlCoordonneesClientLivraison.add(lbNomLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNomLivraison ----
          tfNomLivraison.setBackground(Color.white);
          tfNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfNomLivraison.setEditable(false);
          tfNomLivraison.setMinimumSize(new Dimension(485, 30));
          tfNomLivraison.setPreferredSize(new Dimension(485, 30));
          tfNomLivraison.setName("tfNomLivraison");
          tfNomLivraison.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNomLivraisonFocusLost(e);
            }
          });
          pnlCoordonneesClientLivraison.add(tfNomLivraison, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 1, 1));
          
          // ---- lbComplementNomLivraison ----
          lbComplementNomLivraison.setText("Compl\u00e9ment");
          lbComplementNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbComplementNomLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbComplementNomLivraison.setPreferredSize(new Dimension(150, 30));
          lbComplementNomLivraison.setName("lbNomLivraison");
          lbComplementNomLivraison.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientLivraison.add(lbComplementNomLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfComplementNomLivraison ----
          tfComplementNomLivraison.setBackground(Color.white);
          tfComplementNomLivraison.setMinimumSize(new Dimension(485, 30));
          tfComplementNomLivraison.setPreferredSize(new Dimension(485, 30));
          tfComplementNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfComplementNomLivraison.setName("tfComplementNomLivraison");
          tfComplementNomLivraison.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfComplementNomLivraisonFocusLost(e);
            }
          });
          pnlCoordonneesClientLivraison.add(tfComplementNomLivraison, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbLocalisationLivraison ----
          lbLocalisationLivraison.setText("Adresse 1");
          lbLocalisationLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbLocalisationLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLocalisationLivraison.setPreferredSize(new Dimension(150, 30));
          lbLocalisationLivraison.setName("lbNomLivraison");
          lbLocalisationLivraison.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientLivraison.add(lbLocalisationLivraison, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfRueLivraison ----
          tfRueLivraison.setBackground(Color.white);
          tfRueLivraison.setMinimumSize(new Dimension(485, 30));
          tfRueLivraison.setPreferredSize(new Dimension(485, 30));
          tfRueLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfRueLivraison.setName("tfRueLivraison");
          tfRueLivraison.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfRueLivraisonFocusLost(e);
            }
          });
          pnlCoordonneesClientLivraison.add(tfRueLivraison, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbRueLivraison ----
          lbRueLivraison.setText("Adresse 2");
          lbRueLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbRueLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbRueLivraison.setPreferredSize(new Dimension(150, 30));
          lbRueLivraison.setName("lbNomLivraison");
          lbRueLivraison.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientLivraison.add(lbRueLivraison, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfLocalisationLivraison ----
          tfLocalisationLivraison.setBackground(Color.white);
          tfLocalisationLivraison.setMinimumSize(new Dimension(485, 30));
          tfLocalisationLivraison.setPreferredSize(new Dimension(485, 30));
          tfLocalisationLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfLocalisationLivraison.setName("tfLocalisationLivraison");
          tfLocalisationLivraison.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfLocalisationLivraisonFocusLost(e);
            }
          });
          pnlCoordonneesClientLivraison.add(tfLocalisationLivraison, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- lbCodePostalLivraison ----
          lbCodePostalLivraison.setText("Commune");
          lbCodePostalLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbCodePostalLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCodePostalLivraison.setPreferredSize(new Dimension(150, 30));
          lbCodePostalLivraison.setName("lbNomLivraison");
          lbCodePostalLivraison.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientLivraison.add(lbCodePostalLivraison, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snCodePostalCommuneLivraison ----
          snCodePostalCommuneLivraison.setName("snCodePostalCommuneLivraison");
          snCodePostalCommuneLivraison.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snCodePostalCommuneLivraisonValueChanged(e);
            }
          });
          pnlCoordonneesClientLivraison.add(snCodePostalCommuneLivraison, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbContactLivraison ----
          lbContactLivraison.setText("Contact");
          lbContactLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbContactLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbContactLivraison.setDisplayedMnemonicIndex(2);
          lbContactLivraison.setPreferredSize(new Dimension(150, 30));
          lbContactLivraison.setName("lbNomLivraison");
          lbContactLivraison.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientLivraison.add(lbContactLivraison, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlContact ========
          {
            pnlContact.setName("pnlContact");
            pnlContact.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlContact.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlContact.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlContact.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlContact.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfContactLivraison ----
            tfContactLivraison.setBackground(Color.white);
            tfContactLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfContactLivraison.setMinimumSize(new Dimension(215, 30));
            tfContactLivraison.setPreferredSize(new Dimension(215, 30));
            tfContactLivraison.setName("tfContactLivraison");
            tfContactLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfContactLivraisonFocusLost(e);
              }
            });
            pnlContact.add(tfContactLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbTelephoneLivraison ----
            lbTelephoneLivraison.setText("T\u00e9l\u00e9phone");
            lbTelephoneLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTelephoneLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTelephoneLivraison.setName("lbTelephoneLivraison");
            pnlContact.add(lbTelephoneLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfTelephoneLivraison ----
            tfTelephoneLivraison.setBackground(Color.white);
            tfTelephoneLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfTelephoneLivraison.setMinimumSize(new Dimension(140, 30));
            tfTelephoneLivraison.setPreferredSize(new Dimension(140, 30));
            tfTelephoneLivraison.setName("tfTelephoneLivraison");
            tfTelephoneLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfTelephoneLivraisonFocusLost(e);
              }
            });
            pnlContact.add(tfTelephoneLivraison, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 10), 0, 0));
          }
          pnlCoordonneesClientLivraison.add(pnlContact, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEmailLivraison ----
          lbEmailLivraison.setText("Email");
          lbEmailLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbEmailLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
          lbEmailLivraison.setPreferredSize(new Dimension(150, 30));
          lbEmailLivraison.setName("lbNomLivraison");
          lbEmailLivraison.setMinimumSize(new Dimension(150, 30));
          pnlCoordonneesClientLivraison.add(lbEmailLivraison, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlEmail ========
          {
            pnlEmail.setName("pnlEmail");
            pnlEmail.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEmail.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlEmail.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlEmail.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlEmail.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfEmailLivraison ----
            tfEmailLivraison.setBackground(Color.white);
            tfEmailLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfEmailLivraison.setMinimumSize(new Dimension(250, 30));
            tfEmailLivraison.setPreferredSize(new Dimension(250, 30));
            tfEmailLivraison.setName("tfEmailLivraison");
            tfEmailLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfEmailLivraisonFocusLost(e);
              }
            });
            pnlEmail.add(tfEmailLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbFaxLivraison ----
            lbFaxLivraison.setText("Fax");
            lbFaxLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFaxLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFaxLivraison.setName("lbFaxLivraison");
            pnlEmail.add(lbFaxLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfFaxLivraison ----
            tfFaxLivraison.setBackground(Color.white);
            tfFaxLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfFaxLivraison.setPreferredSize(new Dimension(140, 30));
            tfFaxLivraison.setMinimumSize(new Dimension(140, 30));
            tfFaxLivraison.setName("tfFaxLivraison");
            tfFaxLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfFaxLivraisonFocusLost(e);
              }
            });
            pnlEmail.add(tfFaxLivraison, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 15), 0, 0));
          }
          pnlCoordonneesClientLivraison.add(pnlEmail, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- btnAutresAdresses ----
          btnAutresAdresses.setToolTipText("Autres adresses de livraison");
          btnAutresAdresses.setPreferredSize(new Dimension(30, 30));
          btnAutresAdresses.setMinimumSize(new Dimension(30, 30));
          btnAutresAdresses.setHorizontalAlignment(SwingConstants.TRAILING);
          btnAutresAdresses.setName("btnAutresAdresses");
          btnAutresAdresses.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              btnAutresAdressesActionPerformed(e);
            }
          });
          pnlCoordonneesClientLivraison.add(btnAutresAdresses, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        tabAdresses.addTab("Adresse de livraison", pnlCoordonneesClientLivraison);
      }
      pnlContenu.add(tabAdresses,
          new GridBagConstraints(0, 2, 2, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== sNPanel1 ========
      {
        sNPanel1.setName("sNPanel1");
        sNPanel1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      }
      pnlContenu.add(sNPanel1,
          new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlChantier ========
      {
        pnlChantier.setTitre("Chantier");
        pnlChantier.setMinimumSize(new Dimension(667, 160));
        pnlChantier.setPreferredSize(new Dimension(683, 160));
        pnlChantier.setName("pnlChantier");
        pnlChantier.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlChantier.getLayout()).columnWeights = new double[] { 0.0, 1.0 };
        
        // ---- lbChantierReferenceCourte2 ----
        lbChantierReferenceCourte2.setText("Libell\u00e9");
        lbChantierReferenceCourte2.setName("lbChantierReferenceCourte2");
        pnlChantier.add(lbChantierReferenceCourte2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snChantier ----
        snChantier.setName("snChantier");
        snChantier.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snChantierValueChanged(e);
          }
        });
        pnlChantier.add(snChantier, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbChantierReferenceCourte ----
        lbChantierReferenceCourte.setText("R\u00e9f\u00e9rence courte");
        lbChantierReferenceCourte.setName("lbChantierReferenceCourte");
        pnlChantier.add(lbChantierReferenceCourte, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfChantierReferenceCourte ----
        tfChantierReferenceCourte.setComponentPopupMenu(null);
        tfChantierReferenceCourte.setMinimumSize(new Dimension(180, 30));
        tfChantierReferenceCourte.setPreferredSize(new Dimension(180, 30));
        tfChantierReferenceCourte.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfChantierReferenceCourte.setName("tfChantierReferenceCourte");
        tfChantierReferenceCourte.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfReferenceCourteDocumentFocusLost(e);
          }
        });
        pnlChantier.add(tfChantierReferenceCourte, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbChantierDateDebut ----
        lbChantierDateDebut.setText("Date d\u00e9but");
        lbChantierDateDebut.setName("lbChantierDateDebut");
        pnlChantier.add(lbChantierDateDebut, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlDateChantier ========
        {
          pnlDateChantier.setName("pnlDateChantier");
          pnlDateChantier.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDateChantier.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDateChantier.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDateChantier.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDateChantier.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- calChantierDateDebut ----
          calChantierDateDebut.setComponentPopupMenu(null);
          calChantierDateDebut.setMinimumSize(new Dimension(115, 30));
          calChantierDateDebut.setPreferredSize(new Dimension(115, 30));
          calChantierDateDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
          calChantierDateDebut.setName("calChantierDateDebut");
          pnlDateChantier.add(calChantierDateDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbChantierDateFin ----
          lbChantierDateFin.setText("Date fin");
          lbChantierDateFin.setName("lbChantierDateFin");
          pnlDateChantier.add(lbChantierDateFin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- calChantierDateFin ----
          calChantierDateFin.setComponentPopupMenu(null);
          calChantierDateFin.setMinimumSize(new Dimension(115, 30));
          calChantierDateFin.setPreferredSize(new Dimension(115, 30));
          calChantierDateFin.setFont(new Font("sansserif", Font.PLAIN, 14));
          calChantierDateFin.setName("calChantierDateFin");
          pnlDateChantier.add(calChantierDateFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlChantier.add(pnlDateChantier, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlChantier,
          new GridBagConstraints(0, 5, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- btgMode ----
    btgMode.add(rbModeEnlevement);
    btgMode.add(rbModeLivraison);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlContenu;
  private SNLabelTitre lbTitreZoneSaisie;
  private JPanel pnlTypeDocument;
  private JRadioButton rbModeEnlevement;
  private JRadioButton rbModeLivraison;
  private JCheckBox ckDirectUsine;
  private JPanel pnlInfosDocument;
  private SNPanel pnlReferenceCourte;
  private SNLabelChamp lbReferenceCourteDocument;
  private SNTexte tfReferenceCourteDocument;
  private SNLabelChamp lbTypeFacturation;
  private SNTypeFacturation cbTypesFacturation;
  private SNPanel pnlReferenceLongue;
  private SNLabelChamp lbReferenceDocument;
  private SNTexte tfReferenceDocument;
  private SNPanel pnlDateDocument;
  private SNLabelChamp lbDateTraitementDocument;
  private XRiCalendrier calDateTraitementDocument;
  private SNLabelChamp lbDateValiditeDocument;
  private XRiCalendrier calDateValiditeDocument;
  private SNLabelChamp lbDateRelanceDocument;
  private XRiCalendrier calDateRelanceDocument;
  private SNPanel sNPanel2;
  private SNLabelChamp sNLabelChamp1;
  private SNContact sNContact1;
  private SNPanel pnlInformationLivraisonEnlevement;
  private JScrollPane scpInformationLivraisonEnlevement;
  private RiTextArea taInformationLivraisonEnlevement;
  private SNPanel pnlEnvTrans;
  private JPanel pnlEnlevement;
  private JLabel lbPrisPar;
  private JComboBox cbPrisPar;
  private JLabel lbImmatriculation;
  private SNTexte tfImmatriculation;
  private JPanel pnlTransporteur;
  private JLabel lbTransporteurs;
  private JLabel lbTransporteurs2;
  private SNTransporteur snTransporteur;
  private SNTransporteur snTransporteur2;
  private JLabel lbLivraisonPartielle;
  private JLabel lbLivraisonPartielle2;
  private XRiComboBox cbLivraisonPartielle;
  private XRiComboBox cbLivraisonPartielle2;
  private JLabel lbFranco;
  private JLabel lbFranco2;
  private SNTexte tfFranco;
  private JLabel lbDateLivraisonSouhaitee;
  private XRiCalendrier calDateLivraisonSouhaitee;
  private JLabel lbDateLivraisonPrevue;
  private XRiCalendrier calDateLivraisonPrevue;
  private JTabbedPane tabAdresses;
  private SNPanelContenu pnlCoordonneesClientFacturation;
  private SNLabelChamp lbNomFacturation;
  private SNTexte tfNomFacturation;
  private JLabel lbComplementNomFacturation;
  private SNTexte tfComplementNomFacturation;
  private JLabel lbLocalisationFacturation;
  private SNTexte tfRueFacturation;
  private JLabel lbRueFacturation;
  private SNTexte tfLocalisationFacturation;
  private JLabel lbCodePostalFacturation;
  private SNCodePostalCommune snCodePostalCommuneFacturation;
  private JLabel lbContactFacturation;
  private SNPanel pnlContact2;
  private SNTexte tfContactFacturation;
  private JLabel lbTelephoneFacturation;
  private SNTexte tfTelephoneFacturation;
  private JLabel lbEmailFacturation;
  private SNPanel pnlEmail2;
  private SNTexte tfEmailFacturation;
  private JLabel lbFaxFacturation;
  private SNTexte tfFaxFacturation;
  private SNPanelContenu pnlCoordonneesClientLivraison;
  private SNLabelChamp lbNomLivraison;
  private SNTexte tfNomLivraison;
  private JLabel lbComplementNomLivraison;
  private SNTexte tfComplementNomLivraison;
  private JLabel lbLocalisationLivraison;
  private SNTexte tfRueLivraison;
  private JLabel lbRueLivraison;
  private SNTexte tfLocalisationLivraison;
  private JLabel lbCodePostalLivraison;
  private SNCodePostalCommune snCodePostalCommuneLivraison;
  private JLabel lbContactLivraison;
  private SNPanel pnlContact;
  private SNTexte tfContactLivraison;
  private JLabel lbTelephoneLivraison;
  private SNTexte tfTelephoneLivraison;
  private JLabel lbEmailLivraison;
  private SNPanel pnlEmail;
  private SNTexte tfEmailLivraison;
  private JLabel lbFaxLivraison;
  private SNTexte tfFaxLivraison;
  private SNBoutonDetail btnAutresAdresses;
  private SNPanel sNPanel1;
  private SNPanelTitre pnlChantier;
  private SNLabelChamp lbChantierReferenceCourte2;
  private SNChantier snChantier;
  private SNLabelChamp lbChantierReferenceCourte;
  private SNTexte tfChantierReferenceCourte;
  private SNLabelChamp lbChantierDateDebut;
  private SNPanel pnlDateChantier;
  private XRiCalendrier calChantierDateDebut;
  private SNLabelChamp lbChantierDateFin;
  private XRiCalendrier calChantierDateFin;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup btgMode;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
