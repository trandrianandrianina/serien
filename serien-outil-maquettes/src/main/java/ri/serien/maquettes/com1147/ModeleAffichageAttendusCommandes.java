/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.com1147;

import java.util.List;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.document.CritereAttenduCommande;
import ri.serien.libcommun.gescom.commun.document.EnumAttenduCommande;
import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceStock;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

public class ModeleAffichageAttendusCommandes extends AbstractModelePanel {
  // Variables
  private Article article = null;
  private ListeMagasin listeMagasin = null;
  private Magasin magasin = null;
  private List<LigneAttenduCommande> listeAttenduCommande = null;
  private IdMagasin idMagasinLigne = null;
  
  /**
   * Constructeur.
   */
  public ModeleAffichageAttendusCommandes(SessionBase pSession, Article pArticle, IdMagasin pIdMagasin) {
    super(pSession);
    article = pArticle;
    idMagasinLigne = pIdMagasin;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Récupération de la liste des magasins
    listeMagasin = new ListeMagasin();
    listeMagasin = listeMagasin.charger(getIdSession(), article.getId().getIdEtablissement());
    
    // Les attendus commandé pour le magasin sur lequel on travaille ou à défaut pour le premier magasin de la liste
    if (listeMagasin != null && !listeMagasin.isEmpty()) {
      if (idMagasinLigne != null) {
        magasin = listeMagasin.get(idMagasinLigne);
      }
      else {
        magasin = listeMagasin.get(0);
      }
      chargerAttenduCommande();
    }
  }
  
  // -- Méthodes publiques
  /**
   * Change le magasin à afficher
   */
  public void modifierMagasin(Magasin pMagasin) {
    if (Constantes.equals(pMagasin, magasin)) {
      return;
    }
    magasin = pMagasin;
    chargerAttenduCommande();
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Charger les attendu/commande pour le magasin et l'article en cours.
   */
  private void chargerAttenduCommande() {
    if (magasin == null) {
      throw new MessageErreurException("Le magasin n'est valide.");
    }
    if (article == null) {
      throw new MessageErreurException("L'article n'est valide.");
    }
    CritereAttenduCommande critereAttenduCommande = new CritereAttenduCommande();
    critereAttenduCommande.setTypefiltrage(EnumAttenduCommande.ATTENDU_ET_COMMANDE);
    critereAttenduCommande.setIdEtablissement(article.getId().getIdEtablissement());
    critereAttenduCommande.setIdArticle(article.getId());
    critereAttenduCommande.setIdMagasin(magasin.getId());
    listeAttenduCommande = ManagerServiceStock.chargerListeLigneAttenduCommande(getIdSession(), critereAttenduCommande);
  }
  
  // -- Accesseurs
  
  /**
   * Article dont on charge l'attendu commandé.
   */
  public Article getArticle() {
    return article;
  }
  
  /**
   * Liste des magasins.
   */
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  /**
   * Liste des attendus commandés pour l'article.
   */
  public List<LigneAttenduCommande> getListeAttenduCommande() {
    return listeAttenduCommande;
  }
  
  public Magasin getMagasin() {
    return magasin;
  }
}
