/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.com1147;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.commun.stock.ListeStock;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'onglet stock de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueAffichageAttendusCommandes extends AbstractVuePanel<ModeleAffichageAttendusCommandes> {
  // Constantes
  private static final String[] TITRE_LISTE_ATTCMD =
      new String[] { "Num\u00e9ro", "Livraison pr\u00e9vue", "Quantit\u00e9", "Tiers", "Stock gliss\u00e9" };
  
  // Variables
  private ListeStock listeStockParMagasin = null;
  private ListeStock listeStockMagasinsEtablissement = null;
  private int indexLigneListeMouvements = -1;
  private int indexLigneListeAttenduCommande = -1;
  private int indexLigneListeEtatStocks = -1;
  
  /**
   * Constructeur.
   */
  public VueAffichageAttendusCommandes(ModeleAffichageAttendusCommandes pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    tblAttenduCommande.personnaliserAspect(TITRE_LISTE_ATTCMD, new int[] { 80, 120, 80, 250, 90 }, new int[] { 100, 140, 90, -1, 100 },
        new int[] { NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.DROITE }, 14);
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewport = scpAttenduCommande.getViewport();
    viewport.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        // scpAttenduCommandeStateChanged(e);
      }
    });
    scpAttenduCommande.setBackground(Color.WHITE);
    scpAttenduCommande.getViewport().setBackground(Color.WHITE);
  }
  
  @Override
  public void rafraichir() {
    rafraichirMagasin();
    rafraichirAttenduCommande();
  }
  
  // -- Méthodes privées
  
  private void rafraichirMagasin() {
    cbMagasinsAttenduCommande.setSession(getModele().getSession());
    cbMagasinsAttenduCommande.setIdEtablissement(getModele().getArticle().getId().getIdEtablissement());
    cbMagasinsAttenduCommande.charger(false);
    cbMagasinsAttenduCommande.setSelection(getModele().getMagasin());
  }
  
  /**
   * Affichage des données du sous-onglet attendu commandé
   */
  private void rafraichirAttenduCommande() {
    List<LigneAttenduCommande> attcmd = getModele().getListeAttenduCommande();
    Object[][] donneesBrutes = null;
    Object[][] donnees = null;
    
    if (attcmd != null) {
      donneesBrutes = new Object[attcmd.size()][TITRE_LISTE_ATTCMD.length];
      for (int ligne = 0; ligne < attcmd.size(); ligne++) {
        LigneAttenduCommande ligneAttenduCommande = attcmd.get(ligne);
        if (ligneAttenduCommande != null && ligneAttenduCommande.getIdMagasin().equals(getModele().getMagasin().getId())) {
          /*if (attcmd.get(ligne).getIdLigneVente() == null) {
            donneesBrutes[ligne][1] = "";
          }
          else {
            donneesBrutes[ligne][1] = attcmd.get(ligne).getIdDocumentVente().toString();
          }*/
          if (ligneAttenduCommande.getIdDocumentVente() != null) {
            donneesBrutes[ligne][0] = ligneAttenduCommande.getIdDocumentVente().toString();
          }
          else if (ligneAttenduCommande.getIdDocumentAchat() != null) {
            donneesBrutes[ligne][0] = ligneAttenduCommande.getIdDocumentAchat().toString();
          }
          
          donneesBrutes[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, ligneAttenduCommande.getDateLivraisonPrevue());
          // En fonction du type de la ligne vente ou achat
          if (ligneAttenduCommande.getOrigineLigne().equals(LigneAttenduCommande.ORIGINE_LIGNE_VENTE)) {
            donneesBrutes[ligne][2] = ligneAttenduCommande.getQuantiteCommandeeUS();
          }
          else if (ligneAttenduCommande.getOrigineLigne().equals(LigneAttenduCommande.ORIGINE_LIGNE_ACHAT)) {
            donneesBrutes[ligne][2] = ligneAttenduCommande.getQuantiteAttendueUS();
          }
          donneesBrutes[ligne][3] = ligneAttenduCommande.getNomTiersMouvement();
          donneesBrutes[ligne][4] = ligneAttenduCommande.getQuantiteStockGlisse(); // TODO à confirmer car disponible à l'origine
        }
      }
      
      // Nettoyage des lignes vides
      int nbrLignesReelles = attcmd.size();
      for (int i = 0; i < donneesBrutes.length; i++) {
        if (donneesBrutes[i][0] == null) {
          nbrLignesReelles--;
        }
      }
      donnees = new Object[nbrLignesReelles][TITRE_LISTE_ATTCMD.length];
      int indiceNettoye = -1;
      for (int ligne = 0; ligne < donneesBrutes.length; ligne++) {
        if (donneesBrutes[ligne][0] != null) {
          indiceNettoye++;
          donnees[indiceNettoye][0] = donneesBrutes[ligne][0];
          donnees[indiceNettoye][1] = donneesBrutes[ligne][1];
          donnees[indiceNettoye][2] = donneesBrutes[ligne][2];
          donnees[indiceNettoye][3] = donneesBrutes[ligne][3];
          donnees[indiceNettoye][4] = donneesBrutes[ligne][4];
          // donnees[indiceNettoye][5] = donneesBrutes[ligne][5];
        }
      }
    }
    tblAttenduCommande.mettreAJourDonnees(donnees);
  }
  
  // -- Accesseurs et méthodes évènementielles
  
  private void cbMagasinsAttenduCommandeValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierMagasin(cbMagasinsAttenduCommande.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tbpStocksMouseClicked(MouseEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelFond();
    tbpContenu = new JTabbedPane();
    pnlStocksTousDepots = new SNPanel();
    pnlMouvementsDeStocks = new SNPanel();
    pnlStocksAttenduCommande = new SNPanel();
    pnlPanelPrincipal = new SNPanelContenu();
    pnlChoixMagasin = new SNPanel();
    lbMagasin = new SNLabelChamp();
    cbMagasinsAttenduCommande = new SNMagasin();
    scpAttenduCommande = new JScrollPane();
    tblAttenduCommande = new NRiTable();
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
      
      // ======== tbpContenu ========
      {
        tbpContenu.setTabPlacement(SwingConstants.LEFT);
        tbpContenu.setMinimumSize(new Dimension(600, 900));
        tbpContenu.setName("tbpContenu");
        tbpContenu.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tbpStocksMouseClicked(e);
          }
        });
        
        // ======== pnlStocksTousDepots ========
        {
          pnlStocksTousDepots.setOpaque(false);
          pnlStocksTousDepots.setName("pnlStocksTousDepots");
          pnlStocksTousDepots.setLayout(new BorderLayout());
        }
        tbpContenu.addTab("<html><center>Etat des<br />Stocks</center></html>", pnlStocksTousDepots);
        
        // ======== pnlMouvementsDeStocks ========
        {
          pnlMouvementsDeStocks.setOpaque(false);
          pnlMouvementsDeStocks.setName("pnlMouvementsDeStocks");
          pnlMouvementsDeStocks.setLayout(new BorderLayout());
        }
        tbpContenu.addTab("<html><center>Mouvements<br />de stocks</center></html>", pnlMouvementsDeStocks);
        
        // ======== pnlStocksAttenduCommande ========
        {
          pnlStocksAttenduCommande.setOpaque(false);
          pnlStocksAttenduCommande.setName("pnlStocksAttenduCommande");
          pnlStocksAttenduCommande.setLayout(new BorderLayout());
          
          // ======== pnlPanelPrincipal ========
          {
            pnlPanelPrincipal.setMinimumSize(new Dimension(917, 515));
            pnlPanelPrincipal.setMaximumSize(new Dimension(917, 515));
            pnlPanelPrincipal.setName("pnlPanelPrincipal");
            pnlPanelPrincipal.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPanelPrincipal.getLayout()).columnWidths = new int[] { 235, 0 };
            ((GridBagLayout) pnlPanelPrincipal.getLayout()).rowHeights = new int[] { 48, 288, 0 };
            ((GridBagLayout) pnlPanelPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlPanelPrincipal.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            
            // ======== pnlChoixMagasin ========
            {
              pnlChoixMagasin.setOpaque(false);
              pnlChoixMagasin.setName("pnlChoixMagasin");
              pnlChoixMagasin.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlChoixMagasin.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlChoixMagasin.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlChoixMagasin.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlChoixMagasin.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbMagasin ----
              lbMagasin.setText("Magasin");
              lbMagasin.setName("lbMagasin");
              pnlChoixMagasin.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- cbMagasinsAttenduCommande ----
              cbMagasinsAttenduCommande.setBackground(new Color(171, 148, 79));
              cbMagasinsAttenduCommande.setMinimumSize(new Dimension(400, 30));
              cbMagasinsAttenduCommande.setMaximumSize(new Dimension(32767, 30));
              cbMagasinsAttenduCommande.setPreferredSize(new Dimension(400, 30));
              cbMagasinsAttenduCommande.setName("cbMagasinsAttenduCommande");
              cbMagasinsAttenduCommande.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  cbMagasinsAttenduCommandeValueChanged(e);
                }
              });
              pnlChoixMagasin.add(cbMagasinsAttenduCommande, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlPanelPrincipal.add(pnlChoixMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== scpAttenduCommande ========
            {
              scpAttenduCommande.setName("scpAttenduCommande");
              
              // ---- tblAttenduCommande ----
              tblAttenduCommande.setShowVerticalLines(true);
              tblAttenduCommande.setShowHorizontalLines(true);
              tblAttenduCommande.setBackground(Color.white);
              tblAttenduCommande.setOpaque(false);
              tblAttenduCommande.setRowHeight(20);
              tblAttenduCommande.setGridColor(new Color(204, 204, 204));
              tblAttenduCommande.setName("tblAttenduCommande");
              scpAttenduCommande.setViewportView(tblAttenduCommande);
            }
            pnlPanelPrincipal.add(scpAttenduCommande, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlStocksAttenduCommande.add(pnlPanelPrincipal, BorderLayout.CENTER);
        }
        tbpContenu.addTab("<html><center>Attendu<br />command\u00e9</center></html>", pnlStocksAttenduCommande);
      }
      pnlContenu.add(tbpContenu,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlContenu;
  private JTabbedPane tbpContenu;
  private SNPanel pnlStocksTousDepots;
  private SNPanel pnlMouvementsDeStocks;
  private SNPanel pnlStocksAttenduCommande;
  private SNPanelContenu pnlPanelPrincipal;
  private SNPanel pnlChoixMagasin;
  private SNLabelChamp lbMagasin;
  private SNMagasin cbMagasinsAttenduCommande;
  private JScrollPane scpAttenduCommande;
  private NRiTable tblAttenduCommande;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
