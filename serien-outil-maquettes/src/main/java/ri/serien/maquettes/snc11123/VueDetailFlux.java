/*
 * Created by JFormDesigner on Tue Mar 29 10:06:41 CEST 2022
 */

package ri.serien.maquettes.snc11123;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * @author User #8
 */
public class VueDetailFlux extends AbstractVueDialogue<ModeleFlux> {
  
  public VueDetailFlux(ModeleFlux pModele) {
    super(pModele);
    // XXX Auto-generated constructor stub
  }
  
  @Override
  public void initialiserComposants() {
    // XXX Auto-generated method stub
    
  }
  
  @Override
  public void rafraichir() {
    // XXX Auto-generated method stub
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    sNPanelFond1 = new SNPanelFond();
    sNBarreBouton1 = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    sNLabelChamp1 = new SNLabelChamp();
    sNEtablissement1 = new SNEtablissement();
    sNLabelChamp2 = new SNLabelChamp();
    sNLabelChamp6 = new SNLabelChamp();
    sNLabelChamp3 = new SNLabelChamp();
    sNLabelChamp4 = new SNLabelChamp();
    sNLabelChamp5 = new SNLabelChamp();
    
    // ======== this ========
    setTitle("D\u00e9tail du flux");
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new GridLayout());
    
    // ======== sNPanelFond1 ========
    {
      sNPanelFond1.setName("sNPanelFond1");
      sNPanelFond1.setLayout(new BorderLayout());
      
      // ---- sNBarreBouton1 ----
      sNBarreBouton1.setName("sNBarreBouton1");
      sNPanelFond1.add(sNBarreBouton1, BorderLayout.SOUTH);
      
      // ======== sNPanelContenu1 ========
      {
        sNPanelContenu1.setName("sNPanelContenu1");
        sNPanelContenu1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- sNLabelChamp1 ----
        sNLabelChamp1.setText("Etablissement");
        sNLabelChamp1.setName("sNLabelChamp1");
        sNPanelContenu1.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNEtablissement1 ----
        sNEtablissement1.setName("sNEtablissement1");
        sNPanelContenu1.add(sNEtablissement1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp2 ----
        sNLabelChamp2.setText("Date de cr\u00e9ation");
        sNLabelChamp2.setName("sNLabelChamp2");
        sNPanelContenu1.add(sNLabelChamp2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNLabelChamp6 ----
        sNLabelChamp6.setText("Date de traitement");
        sNLabelChamp6.setName("sNLabelChamp6");
        sNPanelContenu1.add(sNLabelChamp6, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNLabelChamp3 ----
        sNLabelChamp3.setText("Sens du flux");
        sNLabelChamp3.setName("sNLabelChamp3");
        sNPanelContenu1.add(sNLabelChamp3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNLabelChamp4 ----
        sNLabelChamp4.setText("Statut du flux");
        sNLabelChamp4.setName("sNLabelChamp4");
        sNPanelContenu1.add(sNLabelChamp4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNLabelChamp5 ----
        sNLabelChamp5.setText("Statut d'erreur");
        sNLabelChamp5.setName("sNLabelChamp5");
        sNPanelContenu1.add(sNLabelChamp5, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      }
      sNPanelFond1.add(sNPanelContenu1, BorderLayout.CENTER);
    }
    contentPane.add(sNPanelFond1);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond sNPanelFond1;
  private SNBarreBouton sNBarreBouton1;
  private SNPanelContenu sNPanelContenu1;
  private SNLabelChamp sNLabelChamp1;
  private SNEtablissement sNEtablissement1;
  private SNLabelChamp sNLabelChamp2;
  private SNLabelChamp sNLabelChamp6;
  private SNLabelChamp sNLabelChamp3;
  private SNLabelChamp sNLabelChamp4;
  private SNLabelChamp sNLabelChamp5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
