/*
 * Created by JFormDesigner on Tue Mar 29 10:05:34 CEST 2022
 */

package ri.serien.maquettes.snc11123;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;

import javax.swing.JScrollPane;

import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * @author User #8
 */
public class VueRechercheFlux extends AbstractVueDialogue<ModeleFlux> {
  
  public VueRechercheFlux(ModeleFlux pModele) {
    super(pModele);
    // XXX Auto-generated constructor stub
  }
  
  @Override
  public void initialiserComposants() {
    // XXX Auto-generated method stub
    
  }
  
  @Override
  public void rafraichir() {
    // XXX Auto-generated method stub
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    sNBandeauTitre1 = new SNBandeauTitre();
    sNBarreBouton1 = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    sNPanel1 = new SNPanel();
    sNLabelTitre1 = new SNLabelTitre();
    sNPanel2 = new SNPanel();
    sNPanel3 = new SNPanel();
    sNLabelChamp3 = new SNLabelChamp();
    sNComboBox1 = new SNComboBox();
    sNLabelChamp4 = new SNLabelChamp();
    sNComboBox2 = new SNComboBox();
    sNLabelChamp5 = new SNLabelChamp();
    sNComboBox3 = new SNComboBox();
    sNLabelChamp6 = new SNLabelChamp();
    sNComboBox4 = new SNComboBox();
    sNPanel4 = new SNPanel();
    sNLabelChamp1 = new SNLabelChamp();
    sNPlageDate1 = new SNPlageDate();
    sNLabelChamp2 = new SNLabelChamp();
    sNPlageDate2 = new SNPlageDate();
    sNBarreRecherche1 = new SNBarreRecherche();
    sNPanel5 = new SNPanel();
    sNLabelTitre2 = new SNLabelTitre();
    scrollPane1 = new JScrollPane();
    xRiTable1 = new XRiTable();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- sNBandeauTitre1 ----
    sNBandeauTitre1.setName("sNBandeauTitre1");
    add(sNBandeauTitre1, BorderLayout.NORTH);
    
    // ---- sNBarreBouton1 ----
    sNBarreBouton1.setName("sNBarreBouton1");
    add(sNBarreBouton1, BorderLayout.SOUTH);
    
    // ======== sNPanelContenu1 ========
    {
      sNPanelContenu1.setName("sNPanelContenu1");
      sNPanelContenu1.setLayout(new GridBagLayout());
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== sNPanel1 ========
      {
        sNPanel1.setName("sNPanel1");
        sNPanel1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- sNLabelTitre1 ----
        sNLabelTitre1.setText("Crit\u00e8res de recherche");
        sNLabelTitre1.setName("sNLabelTitre1");
        sNPanel1.add(sNLabelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanel2 ========
        {
          sNPanel2.setName("sNPanel2");
          sNPanel2.setLayout(new GridLayout(1, 2));
          
          // ======== sNPanel3 ========
          {
            sNPanel3.setName("sNPanel3");
            sNPanel3.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel3.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel3.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- sNLabelChamp3 ----
            sNLabelChamp3.setText("Type du flux");
            sNLabelChamp3.setName("sNLabelChamp3");
            sNPanel3.add(sNLabelChamp3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- sNComboBox1 ----
            sNComboBox1.setName("sNComboBox1");
            sNPanel3.add(sNComboBox1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp4 ----
            sNLabelChamp4.setText("Sens du flux");
            sNLabelChamp4.setName("sNLabelChamp4");
            sNPanel3.add(sNLabelChamp4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- sNComboBox2 ----
            sNComboBox2.setName("sNComboBox2");
            sNPanel3.add(sNComboBox2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp5 ----
            sNLabelChamp5.setText("Statut du flux");
            sNLabelChamp5.setName("sNLabelChamp5");
            sNPanel3.add(sNLabelChamp5, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- sNComboBox3 ----
            sNComboBox3.setName("sNComboBox3");
            sNPanel3.add(sNComboBox3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp6 ----
            sNLabelChamp6.setText("Code erreur");
            sNLabelChamp6.setName("sNLabelChamp6");
            sNPanel3.add(sNLabelChamp6, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- sNComboBox4 ----
            sNComboBox4.setName("sNComboBox4");
            sNPanel3.add(sNComboBox4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanel2.add(sNPanel3);
          
          // ======== sNPanel4 ========
          {
            sNPanel4.setName("sNPanel4");
            sNPanel4.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel4.getLayout()).columnWidths = new int[] { 197, 0, 0 };
            ((GridBagLayout) sNPanel4.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel4.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- sNLabelChamp1 ----
            sNLabelChamp1.setText("Plage de date de cr\u00e9ation");
            sNLabelChamp1.setName("sNLabelChamp1");
            sNPanel4.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- sNPlageDate1 ----
            sNPlageDate1.setName("sNPlageDate1");
            sNPanel4.add(sNPlageDate1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNLabelChamp2 ----
            sNLabelChamp2.setText("Plage de date de traitement");
            sNLabelChamp2.setName("sNLabelChamp2");
            sNPanel4.add(sNLabelChamp2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- sNPlageDate2 ----
            sNPlageDate2.setName("sNPlageDate2");
            sNPanel4.add(sNPlageDate2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- sNBarreRecherche1 ----
            sNBarreRecherche1.setName("sNBarreRecherche1");
            sNPanel4.add(sNBarreRecherche1, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanel2.add(sNPanel4);
        }
        sNPanel1.add(sNPanel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(sNPanel1,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ======== sNPanel5 ========
      {
        sNPanel5.setName("sNPanel5");
        sNPanel5.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanel5.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) sNPanel5.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanel5.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanel5.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ---- sNLabelTitre2 ----
        sNLabelTitre2.setText("Flux correspondants \u00e0 votre recherche");
        sNLabelTitre2.setName("sNLabelTitre2");
        sNPanel5.add(sNLabelTitre2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");
          
          // ---- xRiTable1 ----
          xRiTable1.setName("xRiTable1");
          scrollPane1.setViewportView(xRiTable1);
        }
        sNPanel5.add(scrollPane1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(sNPanel5,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelContenu1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre sNBandeauTitre1;
  private SNBarreBouton sNBarreBouton1;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel sNPanel1;
  private SNLabelTitre sNLabelTitre1;
  private SNPanel sNPanel2;
  private SNPanel sNPanel3;
  private SNLabelChamp sNLabelChamp3;
  private SNComboBox sNComboBox1;
  private SNLabelChamp sNLabelChamp4;
  private SNComboBox sNComboBox2;
  private SNLabelChamp sNLabelChamp5;
  private SNComboBox sNComboBox3;
  private SNLabelChamp sNLabelChamp6;
  private SNComboBox sNComboBox4;
  private SNPanel sNPanel4;
  private SNLabelChamp sNLabelChamp1;
  private SNPlageDate sNPlageDate1;
  private SNLabelChamp sNLabelChamp2;
  private SNPlageDate sNPlageDate2;
  private SNBarreRecherche sNBarreRecherche1;
  private SNPanel sNPanel5;
  private SNLabelTitre sNLabelTitre2;
  private JScrollPane scrollPane1;
  private XRiTable xRiTable1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
