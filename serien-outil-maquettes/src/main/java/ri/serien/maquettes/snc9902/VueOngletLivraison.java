/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9902;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libswing.composant.metier.referentiel.client.sntypefacturation.SNTypeFacturation;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.SNChantier;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

public class VueOngletLivraison extends JPanel {
  /**
   * Constructeur.
   */
  public VueOngletLivraison() {
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
  }
  
  private void rbModeEnlevementItemStateChanged(ItemEvent e) {
  }
  
  private void rbModeLivraisonItemStateChanged(ItemEvent e) {
  }
  
  private void tfReferenceDocumentFocusLost(FocusEvent e) {
  }
  
  private void tfDateTraitementDocumentFocusLost(FocusEvent e) {
  }
  
  private void tfNomLivraisonFocusLost(FocusEvent e) {
  }
  
  private void tfComplementNomLivraisonFocusLost(FocusEvent e) {
  }
  
  private void tfRueLivraisonFocusLost(FocusEvent e) {
  }
  
  private void tfLocalisationLivraisonFocusLost(FocusEvent e) {
  }
  
  private void tfContactLivraisonFocusLost(FocusEvent e) {
  }
  
  private void tfTelephoneLivraisonFocusLost(FocusEvent e) {
  }
  
  private void tfFaxLivraisonFocusLost(FocusEvent e) {
  }
  
  private void tfEmailLivraisonFocusLost(FocusEvent e) {
  }
  
  private void cbLivraisonPartielleItemStateChanged(ItemEvent e) {
  }
  
  private void tfImmatriculationFocusLost(FocusEvent e) {
  }
  
  private void taInformationLivraisonEnlevementFocusLost(FocusEvent e) {
  }
  
  private void ckDirectUsineActionPerformed(ActionEvent e) {
  }
  
  private void tfDateValiditeDocumentFocusLost(FocusEvent e) {
  }
  
  private void tfDateRelanceDocumentFocusLost(FocusEvent e) {
  }
  
  private void tfDateLivraisonSouhaiteeFocusLost(FocusEvent e) {
  }
  
  private void tfDateLivraisonPrevueFocusLost(FocusEvent e) {
  }
  
  private void tfFrancoActionPerformed(ActionEvent e) {
  }
  
  private void tfFrancoFocusLost(FocusEvent e) {
  }
  
  private void tfReferenceCourteDocumentFocusLost(FocusEvent e) {
  }
  
  private void cbPrisParFocusLost(FocusEvent e) {
  }
  
  private void snChantierValueChanged(SNComposantEvent e) {
  }
  
  private void cbTypesFacturationValueChanged(SNComposantEvent e) {
  }
  
  private void snTransporteurValueChanged(SNComposantEvent e) {
  }
  
  private void snCodePostalCommuneLivraisonValueChanged(SNComposantEvent e) {
  }
  
  private void btnAutresAdressesActionPerformed(ActionEvent e) {
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new JPanel();
    pnlInformationsGenerales = new SNPanel();
    pnlTypeDocument = new JPanel();
    rbModeEnlevement = new JRadioButton();
    rbModeLivraison = new JRadioButton();
    ckDirectUsine = new JCheckBox();
    pnlInfosDocument = new JPanel();
    pnlReferenceCourte = new SNPanel();
    lbReferenceCourteDocument = new SNLabelChamp();
    tfReferenceCourteDocument = new SNTexte();
    lbTypeFacturation = new SNLabelChamp();
    cbTypesFacturation = new SNTypeFacturation();
    pnlReferenceLongue = new SNPanel();
    lbReferenceDocument = new SNLabelChamp();
    tfReferenceDocument = new SNTexte();
    pnlDateDocument = new SNPanel();
    lbDateTraitementDocument = new SNLabelChamp();
    calDateTraitementDocument = new XRiCalendrier();
    lbDateValiditeDocument = new SNLabelChamp();
    calDateValiditeDocument = new XRiCalendrier();
    lbDateRelanceDocument = new SNLabelChamp();
    calDateRelanceDocument = new XRiCalendrier();
    pnlMemo = new SNPanel();
    lbTitreZoneSaisie = new SNLabelTitre();
    scpInformationLivraisonEnlevement = new JScrollPane();
    taInformationLivraisonEnlevement = new RiTextArea();
    pnlDetailLivraison = new SNPanel();
    tabbedPane1 = new JTabbedPane();
    pnlCoordonneesClientLivraison2 = new JPanel();
    lbNomLivraison2 = new SNLabelChamp();
    tfNomLivraison2 = new SNTexte();
    lbComplementNomLivraison2 = new JLabel();
    tfComplementNomLivraison2 = new SNTexte();
    lbLocalisationLivraison2 = new JLabel();
    tfRueLivraison2 = new SNTexte();
    lbRueLivraison2 = new JLabel();
    tfLocalisationLivraison2 = new SNTexte();
    lbCodePostalLivraison2 = new JLabel();
    snCodePostalCommuneLivraison2 = new SNCodePostalCommune();
    lbContactLivraison2 = new JLabel();
    tfContactLivraison2 = new SNTexte();
    lbTelephoneLivraison2 = new JLabel();
    tfTelephoneLivraison2 = new SNTexte();
    lbEmailLivraison2 = new JLabel();
    tfEmailLivraison2 = new SNTexte();
    lbFaxLivraison2 = new JLabel();
    tfFaxLivraison2 = new SNTexte();
    pnlCoordonneesClientLivraison = new JPanel();
    btnAutresAdresses = new SNBoutonDetail();
    lbNomLivraison = new SNLabelChamp();
    tfNomLivraison = new SNTexte();
    lbComplementNomLivraison = new JLabel();
    tfComplementNomLivraison = new SNTexte();
    lbLocalisationLivraison = new JLabel();
    tfRueLivraison = new SNTexte();
    lbRueLivraison = new JLabel();
    tfLocalisationLivraison = new SNTexte();
    lbCodePostalLivraison = new JLabel();
    snCodePostalCommuneLivraison = new SNCodePostalCommune();
    lbContactLivraison = new JLabel();
    tfContactLivraison = new SNTexte();
    lbTelephoneLivraison = new JLabel();
    tfTelephoneLivraison = new SNTexte();
    lbEmailLivraison = new JLabel();
    tfEmailLivraison = new SNTexte();
    lbFaxLivraison = new JLabel();
    tfFaxLivraison = new SNTexte();
    pnlEnvTrans = new SNPanel();
    pnlEnlevement = new JPanel();
    lbPrisPar = new JLabel();
    cbPrisPar = new JComboBox();
    lbImmatriculation = new JLabel();
    tfImmatriculation = new SNTexte();
    pnlTransporteur = new JPanel();
    lbTransporteurs = new JLabel();
    lbTransporteurs2 = new JLabel();
    snTransporteur = new SNTransporteur();
    snTransporteur2 = new SNTransporteur();
    lbLivraisonPartielle = new JLabel();
    lbLivraisonPartielle2 = new JLabel();
    cbLivraisonPartielle = new XRiComboBox();
    cbLivraisonPartielle2 = new XRiComboBox();
    lbFranco = new JLabel();
    lbFranco2 = new JLabel();
    tfFranco = new SNTexte();
    lbDateLivraisonSouhaitee = new JLabel();
    calDateLivraisonSouhaitee = new XRiCalendrier();
    lbDateLivraisonPrevue = new JLabel();
    calDateLivraisonPrevue = new XRiCalendrier();
    pnlChantier = new SNPanelTitre();
    lbChantierReferenceCourte2 = new SNLabelChamp();
    snChantier = new SNChantier();
    lbChantierReferenceCourte = new SNLabelChamp();
    tfChantierReferenceCourte = new SNTexte();
    lbChantierDateDebut = new SNLabelChamp();
    pnlDateChantier = new SNPanel();
    calChantierDateDebut = new XRiCalendrier();
    lbChantierDateFin = new SNLabelChamp();
    calChantierDateFin = new XRiCalendrier();
    snBarreBouton = new SNBarreBouton();
    btgMode = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1240, 660));
    setPreferredSize(new Dimension(1240, 660));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setOpaque(false);
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setPreferredSize(new Dimension(1226, 700));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
      
      // ======== pnlInformationsGenerales ========
      {
        pnlInformationsGenerales.setName("pnlInformationsGenerales");
        pnlInformationsGenerales.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInformationsGenerales.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlInformationsGenerales.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlInformationsGenerales.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlInformationsGenerales.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlTypeDocument ========
        {
          pnlTypeDocument.setBorder(new TitledBorder(""));
          pnlTypeDocument.setOpaque(false);
          pnlTypeDocument.setMinimumSize(new Dimension(150, 120));
          pnlTypeDocument.setPreferredSize(new Dimension(150, 120));
          pnlTypeDocument.setMaximumSize(new Dimension(190, 2147483647));
          pnlTypeDocument.setName("pnlTypeDocument");
          pnlTypeDocument.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTypeDocument.getLayout()).columnWidths = new int[] { 150, 0 };
          ((GridBagLayout) pnlTypeDocument.getLayout()).rowHeights = new int[] { 36, 36, 35, 0 };
          ((GridBagLayout) pnlTypeDocument.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTypeDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- rbModeEnlevement ----
          rbModeEnlevement.setText("Enl\u00e8vement");
          rbModeEnlevement.setFont(new Font("sansserif", Font.PLAIN, 14));
          rbModeEnlevement.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbModeEnlevement.setDisplayedMnemonicIndex(0);
          rbModeEnlevement.setName("rbModeEnlevement");
          rbModeEnlevement.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              rbModeEnlevementItemStateChanged(e);
            }
          });
          pnlTypeDocument.add(rbModeEnlevement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(5, 20, 5, 0), 0, 0));
          
          // ---- rbModeLivraison ----
          rbModeLivraison.setText("Livraison");
          rbModeLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
          rbModeLivraison.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbModeLivraison.setDisplayedMnemonicIndex(0);
          rbModeLivraison.setName("rbModeLivraison");
          rbModeLivraison.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              rbModeLivraisonItemStateChanged(e);
            }
          });
          pnlTypeDocument.add(rbModeLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 20, 5, 0), 0, 0));
          
          // ---- ckDirectUsine ----
          ckDirectUsine.setText("<html>Direct <u>u</u>sine</html>");
          ckDirectUsine.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ckDirectUsine.setFont(ckDirectUsine.getFont().deriveFont(ckDirectUsine.getFont().getSize() + 2f));
          ckDirectUsine.setName("ckDirectUsine");
          ckDirectUsine.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ckDirectUsineActionPerformed(e);
            }
          });
          pnlTypeDocument.add(ckDirectUsine, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 20, 0, 0), 0, 0));
        }
        pnlInformationsGenerales.add(pnlTypeDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlInfosDocument ========
        {
          pnlInfosDocument.setOpaque(false);
          pnlInfosDocument.setPreferredSize(new Dimension(600, 120));
          pnlInfosDocument.setMinimumSize(new Dimension(600, 120));
          pnlInfosDocument.setBorder(new CompoundBorder(
              new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)),
              new EmptyBorder(10, 10, 10, 10)));
          pnlInfosDocument.setMaximumSize(new Dimension(600, 120));
          pnlInfosDocument.setName("pnlInfosDocument");
          pnlInfosDocument.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlInfosDocument.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlInfosDocument.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlInfosDocument.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlInfosDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlReferenceCourte ========
          {
            pnlReferenceCourte.setMaximumSize(new Dimension(590, 30));
            pnlReferenceCourte.setMinimumSize(new Dimension(590, 30));
            pnlReferenceCourte.setName("pnlReferenceCourte");
            pnlReferenceCourte.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlReferenceCourte.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlReferenceCourte.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlReferenceCourte.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlReferenceCourte.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbReferenceCourteDocument ----
            lbReferenceCourteDocument.setText("R\u00e9f\u00e9rence courte");
            lbReferenceCourteDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbReferenceCourteDocument.setHorizontalAlignment(SwingConstants.RIGHT);
            lbReferenceCourteDocument.setPreferredSize(new Dimension(115, 30));
            lbReferenceCourteDocument.setMinimumSize(new Dimension(115, 19));
            lbReferenceCourteDocument.setMaximumSize(new Dimension(115, 19));
            lbReferenceCourteDocument.setName("lbReferenceCourteDocument");
            pnlReferenceCourte.add(lbReferenceCourteDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfReferenceCourteDocument ----
            tfReferenceCourteDocument.setComponentPopupMenu(null);
            tfReferenceCourteDocument.setMinimumSize(new Dimension(180, 30));
            tfReferenceCourteDocument.setPreferredSize(new Dimension(180, 30));
            tfReferenceCourteDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfReferenceCourteDocument.setName("tfReferenceCourteDocument");
            tfReferenceCourteDocument.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfReferenceCourteDocumentFocusLost(e);
              }
            });
            pnlReferenceCourte.add(tfReferenceCourteDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbTypeFacturation ----
            lbTypeFacturation.setText("Type de facturation");
            lbTypeFacturation.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTypeFacturation.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTypeFacturation.setPreferredSize(new Dimension(130, 30));
            lbTypeFacturation.setName("lbTypeFacturation");
            pnlReferenceCourte.add(lbTypeFacturation, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbTypesFacturation ----
            cbTypesFacturation.setPreferredSize(new Dimension(150, 30));
            cbTypesFacturation.setMinimumSize(new Dimension(150, 30));
            cbTypesFacturation.setName("cbTypesFacturation");
            cbTypesFacturation.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                cbTypesFacturationValueChanged(e);
              }
            });
            pnlReferenceCourte.add(cbTypesFacturation, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInfosDocument.add(pnlReferenceCourte, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlReferenceLongue ========
          {
            pnlReferenceLongue.setMaximumSize(new Dimension(590, 30));
            pnlReferenceLongue.setMinimumSize(new Dimension(590, 30));
            pnlReferenceLongue.setPreferredSize(new Dimension(590, 30));
            pnlReferenceLongue.setName("pnlReferenceLongue");
            pnlReferenceLongue.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlReferenceLongue.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlReferenceLongue.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlReferenceLongue.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlReferenceLongue.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbReferenceDocument ----
            lbReferenceDocument.setText("R\u00e9f\u00e9rence longue");
            lbReferenceDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbReferenceDocument.setHorizontalAlignment(SwingConstants.RIGHT);
            lbReferenceDocument.setPreferredSize(new Dimension(115, 30));
            lbReferenceDocument.setMinimumSize(new Dimension(115, 19));
            lbReferenceDocument.setMaximumSize(new Dimension(115, 19));
            lbReferenceDocument.setName("lbReferenceDocument");
            pnlReferenceLongue.add(lbReferenceDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfReferenceDocument ----
            tfReferenceDocument.setComponentPopupMenu(null);
            tfReferenceDocument.setMinimumSize(new Dimension(350, 30));
            tfReferenceDocument.setPreferredSize(new Dimension(350, 30));
            tfReferenceDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfReferenceDocument.setName("tfReferenceDocument");
            tfReferenceDocument.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfReferenceDocumentFocusLost(e);
              }
            });
            pnlReferenceLongue.add(tfReferenceDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInfosDocument.add(pnlReferenceLongue, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlDateDocument ========
          {
            pnlDateDocument.setMaximumSize(new Dimension(590, 30));
            pnlDateDocument.setMinimumSize(new Dimension(590, 30));
            pnlDateDocument.setPreferredSize(new Dimension(590, 30));
            pnlDateDocument.setName("pnlDateDocument");
            pnlDateDocument.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDateDocument.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlDateDocument.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlDateDocument.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlDateDocument.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbDateTraitementDocument ----
            lbDateTraitementDocument.setText("Date document");
            lbDateTraitementDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDateTraitementDocument.setHorizontalAlignment(SwingConstants.RIGHT);
            lbDateTraitementDocument.setMaximumSize(new Dimension(115, 30));
            lbDateTraitementDocument.setMinimumSize(new Dimension(115, 30));
            lbDateTraitementDocument.setPreferredSize(new Dimension(115, 30));
            lbDateTraitementDocument.setName("lbDateTraitementDocument");
            pnlDateDocument.add(lbDateTraitementDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- calDateTraitementDocument ----
            calDateTraitementDocument.setComponentPopupMenu(null);
            calDateTraitementDocument.setPreferredSize(new Dimension(115, 30));
            calDateTraitementDocument.setMinimumSize(new Dimension(115, 30));
            calDateTraitementDocument.setMaximumSize(new Dimension(105, 30));
            calDateTraitementDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            calDateTraitementDocument.setName("calDateTraitementDocument");
            pnlDateDocument.add(calDateTraitementDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbDateValiditeDocument ----
            lbDateValiditeDocument.setText("Validit\u00e9");
            lbDateValiditeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDateValiditeDocument.setHorizontalAlignment(SwingConstants.RIGHT);
            lbDateValiditeDocument.setPreferredSize(new Dimension(50, 30));
            lbDateValiditeDocument.setMaximumSize(new Dimension(50, 30));
            lbDateValiditeDocument.setMinimumSize(new Dimension(50, 30));
            lbDateValiditeDocument.setName("lbDateValiditeDocument");
            pnlDateDocument.add(lbDateValiditeDocument, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- calDateValiditeDocument ----
            calDateValiditeDocument.setComponentPopupMenu(null);
            calDateValiditeDocument.setPreferredSize(new Dimension(115, 30));
            calDateValiditeDocument.setMinimumSize(new Dimension(115, 30));
            calDateValiditeDocument.setMaximumSize(new Dimension(105, 30));
            calDateValiditeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            calDateValiditeDocument.setName("calDateValiditeDocument");
            pnlDateDocument.add(calDateValiditeDocument, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbDateRelanceDocument ----
            lbDateRelanceDocument.setText("Relance");
            lbDateRelanceDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDateRelanceDocument.setHorizontalAlignment(SwingConstants.RIGHT);
            lbDateRelanceDocument.setPreferredSize(new Dimension(60, 30));
            lbDateRelanceDocument.setMinimumSize(new Dimension(60, 30));
            lbDateRelanceDocument.setMaximumSize(new Dimension(60, 30));
            lbDateRelanceDocument.setName("lbDateRelanceDocument");
            pnlDateDocument.add(lbDateRelanceDocument, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- calDateRelanceDocument ----
            calDateRelanceDocument.setComponentPopupMenu(null);
            calDateRelanceDocument.setPreferredSize(new Dimension(115, 30));
            calDateRelanceDocument.setMinimumSize(new Dimension(115, 30));
            calDateRelanceDocument.setMaximumSize(new Dimension(105, 30));
            calDateRelanceDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            calDateRelanceDocument.setName("calDateRelanceDocument");
            pnlDateDocument.add(calDateRelanceDocument, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInfosDocument.add(pnlDateDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformationsGenerales.add(pnlInfosDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlMemo ========
        {
          pnlMemo.setName("pnlMemo");
          pnlMemo.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMemo.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlMemo.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMemo.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlMemo.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          
          // ---- lbTitreZoneSaisie ----
          lbTitreZoneSaisie.setText("Voir code");
          lbTitreZoneSaisie.setName("lbTitreZoneSaisie");
          pnlMemo.add(lbTitreZoneSaisie, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== scpInformationLivraisonEnlevement ========
          {
            scpInformationLivraisonEnlevement.setName("scpInformationLivraisonEnlevement");
            
            // ---- taInformationLivraisonEnlevement ----
            taInformationLivraisonEnlevement.setFont(new Font("sansserif", Font.PLAIN, 14));
            taInformationLivraisonEnlevement.setName("taInformationLivraisonEnlevement");
            taInformationLivraisonEnlevement.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                taInformationLivraisonEnlevementFocusLost(e);
              }
            });
            scpInformationLivraisonEnlevement.setViewportView(taInformationLivraisonEnlevement);
          }
          pnlMemo.add(scpInformationLivraisonEnlevement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformationsGenerales.add(pnlMemo, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInformationsGenerales,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlDetailLivraison ========
      {
        pnlDetailLivraison.setName("pnlDetailLivraison");
        pnlDetailLivraison.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDetailLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDetailLivraison.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlDetailLivraison.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlDetailLivraison.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== tabbedPane1 ========
        {
          tabbedPane1.setFont(new Font("sansserif", Font.BOLD, 14));
          tabbedPane1.setName("tabbedPane1");
          
          // ======== pnlCoordonneesClientLivraison2 ========
          {
            pnlCoordonneesClientLivraison2.setBorder(new CompoundBorder(
                new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)),
                new EmptyBorder(10, 10, 10, 10)));
            pnlCoordonneesClientLivraison2.setOpaque(false);
            pnlCoordonneesClientLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            pnlCoordonneesClientLivraison2.setMinimumSize(new Dimension(600, 250));
            pnlCoordonneesClientLivraison2.setPreferredSize(new Dimension(600, 250));
            pnlCoordonneesClientLivraison2.setMaximumSize(new Dimension(2000, 310));
            pnlCoordonneesClientLivraison2.setName("pnlCoordonneesClientLivraison2");
            pnlCoordonneesClientLivraison2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCoordonneesClientLivraison2.getLayout()).columnWidths = new int[] { 135, 0, 0, 0, 0, 0, 130, 0 };
            ((GridBagLayout) pnlCoordonneesClientLivraison2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 35, 0 };
            ((GridBagLayout) pnlCoordonneesClientLivraison2.getLayout()).columnWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCoordonneesClientLivraison2.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNomLivraison2 ----
            lbNomLivraison2.setText("Raison sociale");
            lbNomLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbNomLivraison2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbNomLivraison2.setName("lbNomLivraison2");
            pnlCoordonneesClientLivraison2.add(lbNomLivraison2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfNomLivraison2 ----
            tfNomLivraison2.setBackground(Color.white);
            tfNomLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfNomLivraison2.setEditable(false);
            tfNomLivraison2.setMinimumSize(new Dimension(485, 30));
            tfNomLivraison2.setPreferredSize(new Dimension(485, 30));
            tfNomLivraison2.setName("tfNomLivraison2");
            tfNomLivraison2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfNomLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison2.add(tfNomLivraison2, new GridBagConstraints(1, 0, 6, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 1, 1));
            
            // ---- lbComplementNomLivraison2 ----
            lbComplementNomLivraison2.setText("Compl\u00e9ment");
            lbComplementNomLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbComplementNomLivraison2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbComplementNomLivraison2.setName("lbComplementNomLivraison2");
            pnlCoordonneesClientLivraison2.add(lbComplementNomLivraison2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfComplementNomLivraison2 ----
            tfComplementNomLivraison2.setBackground(Color.white);
            tfComplementNomLivraison2.setMinimumSize(new Dimension(485, 30));
            tfComplementNomLivraison2.setPreferredSize(new Dimension(485, 30));
            tfComplementNomLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfComplementNomLivraison2.setName("tfComplementNomLivraison2");
            tfComplementNomLivraison2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfComplementNomLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison2.add(tfComplementNomLivraison2, new GridBagConstraints(1, 1, 6, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
            
            // ---- lbLocalisationLivraison2 ----
            lbLocalisationLivraison2.setText("Adresse 1");
            lbLocalisationLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbLocalisationLivraison2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLocalisationLivraison2.setName("lbLocalisationLivraison2");
            pnlCoordonneesClientLivraison2.add(lbLocalisationLivraison2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfRueLivraison2 ----
            tfRueLivraison2.setBackground(Color.white);
            tfRueLivraison2.setMinimumSize(new Dimension(485, 30));
            tfRueLivraison2.setPreferredSize(new Dimension(485, 30));
            tfRueLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfRueLivraison2.setName("tfRueLivraison2");
            tfRueLivraison2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRueLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison2.add(tfRueLivraison2, new GridBagConstraints(1, 2, 6, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
            
            // ---- lbRueLivraison2 ----
            lbRueLivraison2.setText("Adresse 2");
            lbRueLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbRueLivraison2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbRueLivraison2.setName("lbRueLivraison2");
            pnlCoordonneesClientLivraison2.add(lbRueLivraison2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfLocalisationLivraison2 ----
            tfLocalisationLivraison2.setBackground(Color.white);
            tfLocalisationLivraison2.setMinimumSize(new Dimension(485, 30));
            tfLocalisationLivraison2.setPreferredSize(new Dimension(485, 30));
            tfLocalisationLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfLocalisationLivraison2.setName("tfLocalisationLivraison2");
            tfLocalisationLivraison2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfLocalisationLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison2.add(tfLocalisationLivraison2, new GridBagConstraints(1, 3, 6, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
            
            // ---- lbCodePostalLivraison2 ----
            lbCodePostalLivraison2.setText("Commune");
            lbCodePostalLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbCodePostalLivraison2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbCodePostalLivraison2.setName("lbCodePostalLivraison2");
            pnlCoordonneesClientLivraison2.add(lbCodePostalLivraison2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCodePostalCommuneLivraison2 ----
            snCodePostalCommuneLivraison2.setName("snCodePostalCommuneLivraison2");
            snCodePostalCommuneLivraison2.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snCodePostalCommuneLivraisonValueChanged(e);
              }
            });
            pnlCoordonneesClientLivraison2.add(snCodePostalCommuneLivraison2, new GridBagConstraints(1, 4, 6, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbContactLivraison2 ----
            lbContactLivraison2.setText("Contact");
            lbContactLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbContactLivraison2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbContactLivraison2.setDisplayedMnemonicIndex(2);
            lbContactLivraison2.setName("lbContactLivraison2");
            pnlCoordonneesClientLivraison2.add(lbContactLivraison2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfContactLivraison2 ----
            tfContactLivraison2.setBackground(Color.white);
            tfContactLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfContactLivraison2.setMinimumSize(new Dimension(215, 30));
            tfContactLivraison2.setPreferredSize(new Dimension(215, 30));
            tfContactLivraison2.setName("tfContactLivraison2");
            tfContactLivraison2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfContactLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison2.add(tfContactLivraison2, new GridBagConstraints(1, 5, 3, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbTelephoneLivraison2 ----
            lbTelephoneLivraison2.setText("T\u00e9l\u00e9phone");
            lbTelephoneLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTelephoneLivraison2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTelephoneLivraison2.setName("lbTelephoneLivraison2");
            pnlCoordonneesClientLivraison2.add(lbTelephoneLivraison2, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfTelephoneLivraison2 ----
            tfTelephoneLivraison2.setBackground(Color.white);
            tfTelephoneLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfTelephoneLivraison2.setMinimumSize(new Dimension(140, 30));
            tfTelephoneLivraison2.setPreferredSize(new Dimension(140, 30));
            tfTelephoneLivraison2.setName("tfTelephoneLivraison2");
            tfTelephoneLivraison2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfTelephoneLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison2.add(tfTelephoneLivraison2, new GridBagConstraints(6, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
            
            // ---- lbEmailLivraison2 ----
            lbEmailLivraison2.setText("Email");
            lbEmailLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbEmailLivraison2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbEmailLivraison2.setName("lbEmailLivraison2");
            pnlCoordonneesClientLivraison2.add(lbEmailLivraison2, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfEmailLivraison2 ----
            tfEmailLivraison2.setBackground(Color.white);
            tfEmailLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfEmailLivraison2.setMinimumSize(new Dimension(250, 30));
            tfEmailLivraison2.setPreferredSize(new Dimension(250, 30));
            tfEmailLivraison2.setName("tfEmailLivraison2");
            tfEmailLivraison2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfEmailLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison2.add(tfEmailLivraison2, new GridBagConstraints(1, 6, 4, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbFaxLivraison2 ----
            lbFaxLivraison2.setText("Fax");
            lbFaxLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFaxLivraison2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFaxLivraison2.setName("lbFaxLivraison2");
            pnlCoordonneesClientLivraison2.add(lbFaxLivraison2, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfFaxLivraison2 ----
            tfFaxLivraison2.setBackground(Color.white);
            tfFaxLivraison2.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfFaxLivraison2.setPreferredSize(new Dimension(140, 30));
            tfFaxLivraison2.setMinimumSize(new Dimension(140, 30));
            tfFaxLivraison2.setName("tfFaxLivraison2");
            tfFaxLivraison2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfFaxLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison2.add(tfFaxLivraison2, new GridBagConstraints(6, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 10), 0, 0));
          }
          tabbedPane1.addTab("Adresse de facturation", pnlCoordonneesClientLivraison2);
          
          // ======== pnlCoordonneesClientLivraison ========
          {
            pnlCoordonneesClientLivraison.setBorder(new CompoundBorder(
                new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)),
                new EmptyBorder(10, 10, 10, 10)));
            pnlCoordonneesClientLivraison.setOpaque(false);
            pnlCoordonneesClientLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            pnlCoordonneesClientLivraison.setMinimumSize(new Dimension(600, 250));
            pnlCoordonneesClientLivraison.setPreferredSize(new Dimension(600, 250));
            pnlCoordonneesClientLivraison.setMaximumSize(new Dimension(2000, 310));
            pnlCoordonneesClientLivraison.setName("pnlCoordonneesClientLivraison");
            pnlCoordonneesClientLivraison.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).columnWidths = new int[] { 135, 0, 0, 0, 0, 0, 130, 0 };
            ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 35, 0 };
            ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).columnWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCoordonneesClientLivraison.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- btnAutresAdresses ----
            btnAutresAdresses.setToolTipText("Autres adresses de livraison");
            btnAutresAdresses.setHorizontalAlignment(SwingConstants.TRAILING);
            btnAutresAdresses.setName("btnAutresAdresses");
            btnAutresAdresses.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnAutresAdressesActionPerformed(e);
              }
            });
            pnlCoordonneesClientLivraison.add(btnAutresAdresses, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNomLivraison ----
            lbNomLivraison.setText("Raison sociale");
            lbNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbNomLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbNomLivraison.setName("lbNomLivraison");
            pnlCoordonneesClientLivraison.add(lbNomLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfNomLivraison ----
            tfNomLivraison.setBackground(Color.white);
            tfNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfNomLivraison.setEditable(false);
            tfNomLivraison.setMinimumSize(new Dimension(485, 30));
            tfNomLivraison.setPreferredSize(new Dimension(485, 30));
            tfNomLivraison.setName("tfNomLivraison");
            tfNomLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfNomLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison.add(tfNomLivraison, new GridBagConstraints(1, 1, 6, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 1, 1));
            
            // ---- lbComplementNomLivraison ----
            lbComplementNomLivraison.setText("Compl\u00e9ment");
            lbComplementNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbComplementNomLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbComplementNomLivraison.setName("lbComplementNomLivraison");
            pnlCoordonneesClientLivraison.add(lbComplementNomLivraison, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfComplementNomLivraison ----
            tfComplementNomLivraison.setBackground(Color.white);
            tfComplementNomLivraison.setMinimumSize(new Dimension(485, 30));
            tfComplementNomLivraison.setPreferredSize(new Dimension(485, 30));
            tfComplementNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfComplementNomLivraison.setName("tfComplementNomLivraison");
            tfComplementNomLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfComplementNomLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison.add(tfComplementNomLivraison, new GridBagConstraints(1, 2, 6, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
            
            // ---- lbLocalisationLivraison ----
            lbLocalisationLivraison.setText("Adresse 1");
            lbLocalisationLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbLocalisationLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLocalisationLivraison.setName("lbLocalisationLivraison");
            pnlCoordonneesClientLivraison.add(lbLocalisationLivraison, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfRueLivraison ----
            tfRueLivraison.setBackground(Color.white);
            tfRueLivraison.setMinimumSize(new Dimension(485, 30));
            tfRueLivraison.setPreferredSize(new Dimension(485, 30));
            tfRueLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfRueLivraison.setName("tfRueLivraison");
            tfRueLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRueLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison.add(tfRueLivraison, new GridBagConstraints(1, 3, 6, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
            
            // ---- lbRueLivraison ----
            lbRueLivraison.setText("Adresse 2");
            lbRueLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbRueLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbRueLivraison.setName("lbRueLivraison");
            pnlCoordonneesClientLivraison.add(lbRueLivraison, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfLocalisationLivraison ----
            tfLocalisationLivraison.setBackground(Color.white);
            tfLocalisationLivraison.setMinimumSize(new Dimension(485, 30));
            tfLocalisationLivraison.setPreferredSize(new Dimension(485, 30));
            tfLocalisationLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfLocalisationLivraison.setName("tfLocalisationLivraison");
            tfLocalisationLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfLocalisationLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison.add(tfLocalisationLivraison, new GridBagConstraints(1, 4, 6, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
            
            // ---- lbCodePostalLivraison ----
            lbCodePostalLivraison.setText("Commune");
            lbCodePostalLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbCodePostalLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbCodePostalLivraison.setName("lbCodePostalLivraison");
            pnlCoordonneesClientLivraison.add(lbCodePostalLivraison, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCodePostalCommuneLivraison ----
            snCodePostalCommuneLivraison.setName("snCodePostalCommuneLivraison");
            snCodePostalCommuneLivraison.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snCodePostalCommuneLivraisonValueChanged(e);
              }
            });
            pnlCoordonneesClientLivraison.add(snCodePostalCommuneLivraison, new GridBagConstraints(1, 5, 6, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbContactLivraison ----
            lbContactLivraison.setText("Contact");
            lbContactLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbContactLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbContactLivraison.setDisplayedMnemonicIndex(2);
            lbContactLivraison.setName("lbContactLivraison");
            pnlCoordonneesClientLivraison.add(lbContactLivraison, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfContactLivraison ----
            tfContactLivraison.setBackground(Color.white);
            tfContactLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfContactLivraison.setMinimumSize(new Dimension(215, 30));
            tfContactLivraison.setPreferredSize(new Dimension(215, 30));
            tfContactLivraison.setName("tfContactLivraison");
            tfContactLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfContactLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison.add(tfContactLivraison, new GridBagConstraints(1, 6, 3, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbTelephoneLivraison ----
            lbTelephoneLivraison.setText("T\u00e9l\u00e9phone");
            lbTelephoneLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTelephoneLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTelephoneLivraison.setName("lbTelephoneLivraison");
            pnlCoordonneesClientLivraison.add(lbTelephoneLivraison, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfTelephoneLivraison ----
            tfTelephoneLivraison.setBackground(Color.white);
            tfTelephoneLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfTelephoneLivraison.setMinimumSize(new Dimension(140, 30));
            tfTelephoneLivraison.setPreferredSize(new Dimension(140, 30));
            tfTelephoneLivraison.setName("tfTelephoneLivraison");
            tfTelephoneLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfTelephoneLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison.add(tfTelephoneLivraison, new GridBagConstraints(6, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
            
            // ---- lbEmailLivraison ----
            lbEmailLivraison.setText("Email");
            lbEmailLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbEmailLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbEmailLivraison.setName("lbEmailLivraison");
            pnlCoordonneesClientLivraison.add(lbEmailLivraison, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfEmailLivraison ----
            tfEmailLivraison.setBackground(Color.white);
            tfEmailLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfEmailLivraison.setMinimumSize(new Dimension(250, 30));
            tfEmailLivraison.setPreferredSize(new Dimension(250, 30));
            tfEmailLivraison.setName("tfEmailLivraison");
            tfEmailLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfEmailLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison.add(tfEmailLivraison, new GridBagConstraints(1, 7, 4, 1, 1.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbFaxLivraison ----
            lbFaxLivraison.setText("Fax");
            lbFaxLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFaxLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFaxLivraison.setName("lbFaxLivraison");
            pnlCoordonneesClientLivraison.add(lbFaxLivraison, new GridBagConstraints(5, 7, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfFaxLivraison ----
            tfFaxLivraison.setBackground(Color.white);
            tfFaxLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfFaxLivraison.setPreferredSize(new Dimension(140, 30));
            tfFaxLivraison.setMinimumSize(new Dimension(140, 30));
            tfFaxLivraison.setName("tfFaxLivraison");
            tfFaxLivraison.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfFaxLivraisonFocusLost(e);
              }
            });
            pnlCoordonneesClientLivraison.add(tfFaxLivraison, new GridBagConstraints(6, 7, 1, 1, 0.0, 0.0,
                GridBagConstraints.BELOW_BASELINE, GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 10), 0, 0));
          }
          tabbedPane1.addTab("Adresse de livraison", pnlCoordonneesClientLivraison);
        }
        pnlDetailLivraison.add(tabbedPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlEnvTrans ========
        {
          pnlEnvTrans.setName("pnlEnvTrans");
          pnlEnvTrans.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEnvTrans.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlEnvTrans.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEnvTrans.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlEnvTrans.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEnlevement ========
          {
            pnlEnlevement.setBorder(new TitledBorder(null, "Enl\u00e8vement", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
                new Font("sansserif", Font.BOLD, 14)));
            pnlEnlevement.setOpaque(false);
            pnlEnlevement.setMinimumSize(new Dimension(440, 110));
            pnlEnlevement.setPreferredSize(new Dimension(440, 110));
            pnlEnlevement.setName("pnlEnlevement");
            pnlEnlevement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEnlevement.getLayout()).columnWidths = new int[] { 105, 280, 0 };
            ((GridBagLayout) pnlEnlevement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEnlevement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlEnlevement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPrisPar ----
            lbPrisPar.setText("Pris par");
            lbPrisPar.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbPrisPar.setName("lbPrisPar");
            pnlEnlevement.add(lbPrisPar, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbPrisPar ----
            cbPrisPar.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbPrisPar.setPreferredSize(new Dimension(12, 30));
            cbPrisPar.setName("cbPrisPar");
            pnlEnlevement.add(cbPrisPar, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbImmatriculation ----
            lbImmatriculation.setText("Immatriculation");
            lbImmatriculation.setFont(lbImmatriculation.getFont().deriveFont(lbImmatriculation.getFont().getSize() + 2f));
            lbImmatriculation.setHorizontalAlignment(SwingConstants.RIGHT);
            lbImmatriculation.setMinimumSize(new Dimension(94, 19));
            lbImmatriculation.setPreferredSize(new Dimension(94, 19));
            lbImmatriculation.setName("lbImmatriculation");
            pnlEnlevement.add(lbImmatriculation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfImmatriculation ----
            tfImmatriculation.setMaximumSize(new Dimension(120, 30));
            tfImmatriculation.setMinimumSize(new Dimension(120, 30));
            tfImmatriculation.setPreferredSize(new Dimension(120, 30));
            tfImmatriculation.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfImmatriculation.setName("tfImmatriculation");
            tfImmatriculation.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfImmatriculationFocusLost(e);
              }
            });
            pnlEnlevement.add(tfImmatriculation, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEnvTrans.add(pnlEnlevement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTransporteur ========
          {
            pnlTransporteur.setBorder(new TitledBorder(null, "Transport", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
                new Font("sansserif", Font.BOLD, 14)));
            pnlTransporteur.setOpaque(false);
            pnlTransporteur.setMinimumSize(new Dimension(446, 200));
            pnlTransporteur.setPreferredSize(new Dimension(446, 200));
            pnlTransporteur.setName("pnlTransporteur");
            pnlTransporteur.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTransporteur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTransporteur.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlTransporteur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTransporteur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbTransporteurs ----
            lbTransporteurs.setText("Transporteur");
            lbTransporteurs.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTransporteurs.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTransporteurs.setName("lbTransporteurs");
            pnlTransporteur.add(lbTransporteurs, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbTransporteurs2 ----
            lbTransporteurs2.setText("Transporteur");
            lbTransporteurs2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTransporteurs2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTransporteurs2.setName("lbTransporteurs2");
            pnlTransporteur.add(lbTransporteurs2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snTransporteur ----
            snTransporteur.setPreferredSize(new Dimension(290, 30));
            snTransporteur.setMinimumSize(new Dimension(290, 30));
            snTransporteur.setMaximumSize(new Dimension(290, 30));
            snTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
            snTransporteur.setName("snTransporteur");
            snTransporteur.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snTransporteurValueChanged(e);
              }
            });
            pnlTransporteur.add(snTransporteur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snTransporteur2 ----
            snTransporteur2.setPreferredSize(new Dimension(290, 30));
            snTransporteur2.setMinimumSize(new Dimension(290, 30));
            snTransporteur2.setMaximumSize(new Dimension(290, 30));
            snTransporteur2.setFont(new Font("sansserif", Font.PLAIN, 14));
            snTransporteur2.setName("snTransporteur2");
            snTransporteur2.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snTransporteurValueChanged(e);
              }
            });
            pnlTransporteur.add(snTransporteur2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLivraisonPartielle ----
            lbLivraisonPartielle.setText("Livraison partielle");
            lbLivraisonPartielle.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLivraisonPartielle.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbLivraisonPartielle.setName("lbLivraisonPartielle");
            pnlTransporteur.add(lbLivraisonPartielle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbLivraisonPartielle2 ----
            lbLivraisonPartielle2.setText("Livraison partielle");
            lbLivraisonPartielle2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLivraisonPartielle2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbLivraisonPartielle2.setName("lbLivraisonPartielle2");
            pnlTransporteur.add(lbLivraisonPartielle2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbLivraisonPartielle ----
            cbLivraisonPartielle.setBackground(new Color(171, 148, 79));
            cbLivraisonPartielle.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbLivraisonPartielle.setPreferredSize(new Dimension(215, 30));
            cbLivraisonPartielle.setMinimumSize(new Dimension(215, 30));
            cbLivraisonPartielle.setName("cbLivraisonPartielle");
            cbLivraisonPartielle.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbLivraisonPartielleItemStateChanged(e);
              }
            });
            pnlTransporteur.add(cbLivraisonPartielle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- cbLivraisonPartielle2 ----
            cbLivraisonPartielle2.setBackground(new Color(171, 148, 79));
            cbLivraisonPartielle2.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbLivraisonPartielle2.setPreferredSize(new Dimension(215, 30));
            cbLivraisonPartielle2.setMinimumSize(new Dimension(215, 30));
            cbLivraisonPartielle2.setName("cbLivraisonPartielle2");
            cbLivraisonPartielle2.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbLivraisonPartielleItemStateChanged(e);
              }
            });
            pnlTransporteur.add(cbLivraisonPartielle2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFranco ----
            lbFranco.setText("Franco");
            lbFranco.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFranco.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFranco.setName("lbFranco");
            pnlTransporteur.add(lbFranco, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbFranco2 ----
            lbFranco2.setText("Franco");
            lbFranco2.setHorizontalAlignment(SwingConstants.RIGHT);
            lbFranco2.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbFranco2.setName("lbFranco2");
            pnlTransporteur.add(lbFranco2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfFranco ----
            tfFranco.setComponentPopupMenu(null);
            tfFranco.setMinimumSize(new Dimension(105, 30));
            tfFranco.setPreferredSize(new Dimension(105, 30));
            tfFranco.setMaximumSize(new Dimension(2147483647, 30));
            tfFranco.setHorizontalAlignment(SwingConstants.RIGHT);
            tfFranco.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfFranco.setName("tfFranco");
            tfFranco.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                tfFrancoActionPerformed(e);
              }
            });
            tfFranco.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfFrancoFocusLost(e);
              }
            });
            pnlTransporteur.add(tfFranco, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDateLivraisonSouhaitee ----
            lbDateLivraisonSouhaitee.setText("Livraison souhait\u00e9e");
            lbDateLivraisonSouhaitee.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDateLivraisonSouhaitee.setHorizontalAlignment(SwingConstants.RIGHT);
            lbDateLivraisonSouhaitee.setName("lbDateLivraisonSouhaitee");
            pnlTransporteur.add(lbDateLivraisonSouhaitee, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- calDateLivraisonSouhaitee ----
            calDateLivraisonSouhaitee.setComponentPopupMenu(null);
            calDateLivraisonSouhaitee.setMinimumSize(new Dimension(115, 30));
            calDateLivraisonSouhaitee.setPreferredSize(new Dimension(115, 30));
            calDateLivraisonSouhaitee.setFont(new Font("sansserif", Font.PLAIN, 14));
            calDateLivraisonSouhaitee.setName("calDateLivraisonSouhaitee");
            pnlTransporteur.add(calDateLivraisonSouhaitee, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDateLivraisonPrevue ----
            lbDateLivraisonPrevue.setText("Livraison pr\u00e9vue");
            lbDateLivraisonPrevue.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDateLivraisonPrevue.setHorizontalAlignment(SwingConstants.RIGHT);
            lbDateLivraisonPrevue.setName("lbDateLivraisonPrevue");
            pnlTransporteur.add(lbDateLivraisonPrevue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- calDateLivraisonPrevue ----
            calDateLivraisonPrevue.setComponentPopupMenu(null);
            calDateLivraisonPrevue.setMinimumSize(new Dimension(115, 30));
            calDateLivraisonPrevue.setPreferredSize(new Dimension(115, 30));
            calDateLivraisonPrevue.setFont(new Font("sansserif", Font.PLAIN, 14));
            calDateLivraisonPrevue.setName("calDateLivraisonPrevue");
            pnlTransporteur.add(calDateLivraisonPrevue, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEnvTrans.add(pnlTransporteur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDetailLivraison.add(pnlEnvTrans, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDetailLivraison,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlChantier ========
      {
        pnlChantier.setTitre("Chantier");
        pnlChantier.setMinimumSize(new Dimension(667, 160));
        pnlChantier.setPreferredSize(new Dimension(683, 160));
        pnlChantier.setName("pnlChantier");
        pnlChantier.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlChantier.getLayout()).columnWeights = new double[] { 0.0, 1.0 };
        
        // ---- lbChantierReferenceCourte2 ----
        lbChantierReferenceCourte2.setText("Libell\u00e9");
        lbChantierReferenceCourte2.setName("lbChantierReferenceCourte2");
        pnlChantier.add(lbChantierReferenceCourte2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snChantier ----
        snChantier.setName("snChantier");
        snChantier.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snChantierValueChanged(e);
          }
        });
        pnlChantier.add(snChantier, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbChantierReferenceCourte ----
        lbChantierReferenceCourte.setText("R\u00e9f\u00e9rence courte");
        lbChantierReferenceCourte.setName("lbChantierReferenceCourte");
        pnlChantier.add(lbChantierReferenceCourte, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfChantierReferenceCourte ----
        tfChantierReferenceCourte.setComponentPopupMenu(null);
        tfChantierReferenceCourte.setMinimumSize(new Dimension(180, 30));
        tfChantierReferenceCourte.setPreferredSize(new Dimension(180, 30));
        tfChantierReferenceCourte.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfChantierReferenceCourte.setName("tfChantierReferenceCourte");
        tfChantierReferenceCourte.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfReferenceCourteDocumentFocusLost(e);
          }
        });
        pnlChantier.add(tfChantierReferenceCourte, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbChantierDateDebut ----
        lbChantierDateDebut.setText("Date d\u00e9but");
        lbChantierDateDebut.setName("lbChantierDateDebut");
        pnlChantier.add(lbChantierDateDebut, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlDateChantier ========
        {
          pnlDateChantier.setName("pnlDateChantier");
          pnlDateChantier.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDateChantier.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDateChantier.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDateChantier.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDateChantier.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- calChantierDateDebut ----
          calChantierDateDebut.setComponentPopupMenu(null);
          calChantierDateDebut.setMinimumSize(new Dimension(115, 30));
          calChantierDateDebut.setPreferredSize(new Dimension(115, 30));
          calChantierDateDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
          calChantierDateDebut.setName("calChantierDateDebut");
          pnlDateChantier.add(calChantierDateDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbChantierDateFin ----
          lbChantierDateFin.setText("Date fin");
          lbChantierDateFin.setName("lbChantierDateFin");
          pnlDateChantier.add(lbChantierDateFin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- calChantierDateFin ----
          calChantierDateFin.setComponentPopupMenu(null);
          calChantierDateFin.setMinimumSize(new Dimension(115, 30));
          calChantierDateFin.setPreferredSize(new Dimension(115, 30));
          calChantierDateFin.setFont(new Font("sansserif", Font.PLAIN, 14));
          calChantierDateFin.setName("calChantierDateFin");
          pnlDateChantier.add(calChantierDateFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlChantier.add(pnlDateChantier, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlChantier,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- btgMode ----
    btgMode.add(rbModeEnlevement);
    btgMode.add(rbModeLivraison);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlContenu;
  private SNPanel pnlInformationsGenerales;
  private JPanel pnlTypeDocument;
  private JRadioButton rbModeEnlevement;
  private JRadioButton rbModeLivraison;
  private JCheckBox ckDirectUsine;
  private JPanel pnlInfosDocument;
  private SNPanel pnlReferenceCourte;
  private SNLabelChamp lbReferenceCourteDocument;
  private SNTexte tfReferenceCourteDocument;
  private SNLabelChamp lbTypeFacturation;
  private SNTypeFacturation cbTypesFacturation;
  private SNPanel pnlReferenceLongue;
  private SNLabelChamp lbReferenceDocument;
  private SNTexte tfReferenceDocument;
  private SNPanel pnlDateDocument;
  private SNLabelChamp lbDateTraitementDocument;
  private XRiCalendrier calDateTraitementDocument;
  private SNLabelChamp lbDateValiditeDocument;
  private XRiCalendrier calDateValiditeDocument;
  private SNLabelChamp lbDateRelanceDocument;
  private XRiCalendrier calDateRelanceDocument;
  private SNPanel pnlMemo;
  private SNLabelTitre lbTitreZoneSaisie;
  private JScrollPane scpInformationLivraisonEnlevement;
  private RiTextArea taInformationLivraisonEnlevement;
  private SNPanel pnlDetailLivraison;
  private JTabbedPane tabbedPane1;
  private JPanel pnlCoordonneesClientLivraison2;
  private SNLabelChamp lbNomLivraison2;
  private SNTexte tfNomLivraison2;
  private JLabel lbComplementNomLivraison2;
  private SNTexte tfComplementNomLivraison2;
  private JLabel lbLocalisationLivraison2;
  private SNTexte tfRueLivraison2;
  private JLabel lbRueLivraison2;
  private SNTexte tfLocalisationLivraison2;
  private JLabel lbCodePostalLivraison2;
  private SNCodePostalCommune snCodePostalCommuneLivraison2;
  private JLabel lbContactLivraison2;
  private SNTexte tfContactLivraison2;
  private JLabel lbTelephoneLivraison2;
  private SNTexte tfTelephoneLivraison2;
  private JLabel lbEmailLivraison2;
  private SNTexte tfEmailLivraison2;
  private JLabel lbFaxLivraison2;
  private SNTexte tfFaxLivraison2;
  private JPanel pnlCoordonneesClientLivraison;
  private SNBoutonDetail btnAutresAdresses;
  private SNLabelChamp lbNomLivraison;
  private SNTexte tfNomLivraison;
  private JLabel lbComplementNomLivraison;
  private SNTexte tfComplementNomLivraison;
  private JLabel lbLocalisationLivraison;
  private SNTexte tfRueLivraison;
  private JLabel lbRueLivraison;
  private SNTexte tfLocalisationLivraison;
  private JLabel lbCodePostalLivraison;
  private SNCodePostalCommune snCodePostalCommuneLivraison;
  private JLabel lbContactLivraison;
  private SNTexte tfContactLivraison;
  private JLabel lbTelephoneLivraison;
  private SNTexte tfTelephoneLivraison;
  private JLabel lbEmailLivraison;
  private SNTexte tfEmailLivraison;
  private JLabel lbFaxLivraison;
  private SNTexte tfFaxLivraison;
  private SNPanel pnlEnvTrans;
  private JPanel pnlEnlevement;
  private JLabel lbPrisPar;
  private JComboBox cbPrisPar;
  private JLabel lbImmatriculation;
  private SNTexte tfImmatriculation;
  private JPanel pnlTransporteur;
  private JLabel lbTransporteurs;
  private JLabel lbTransporteurs2;
  private SNTransporteur snTransporteur;
  private SNTransporteur snTransporteur2;
  private JLabel lbLivraisonPartielle;
  private JLabel lbLivraisonPartielle2;
  private XRiComboBox cbLivraisonPartielle;
  private XRiComboBox cbLivraisonPartielle2;
  private JLabel lbFranco;
  private JLabel lbFranco2;
  private SNTexte tfFranco;
  private JLabel lbDateLivraisonSouhaitee;
  private XRiCalendrier calDateLivraisonSouhaitee;
  private JLabel lbDateLivraisonPrevue;
  private XRiCalendrier calDateLivraisonPrevue;
  private SNPanelTitre pnlChantier;
  private SNLabelChamp lbChantierReferenceCourte2;
  private SNChantier snChantier;
  private SNLabelChamp lbChantierReferenceCourte;
  private SNTexte tfChantierReferenceCourte;
  private SNLabelChamp lbChantierDateDebut;
  private SNPanel pnlDateChantier;
  private XRiCalendrier calChantierDateDebut;
  private SNLabelChamp lbChantierDateFin;
  private XRiCalendrier calChantierDateFin;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup btgMode;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
