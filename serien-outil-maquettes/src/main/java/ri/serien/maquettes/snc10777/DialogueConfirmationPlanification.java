/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc10777;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;
import ri.serien.maquettes.snc9413.etude.ihm.ModeleDialogueCalculPrixVente;

/**
 * Vue de la boîte de dialogue "Message d'information".
 * Elle permet d'afficher des messages simples à l'utilisateur. Celui-ci peut uniquement cliquer sur un bouton "Fermer" pour fermer
 * la boîte de dialogue.
 */
public class DialogueConfirmationPlanification extends AbstractVueDialogue<ModeleDialogueCalculPrixVente> {
  
  // Variables
  private static String BOUTON_OUI = "Oui";
  private static String BOUTON_NON = "Non";
  
  /**
   * Contructeur.
   */
  public DialogueConfirmationPlanification(ModeleDialogueCalculPrixVente pModele) {
    super(pModele);
  }
  
  /**
   * Afficher la boîte de dialogue avec un message.
   * A utiliser uniquement si la variante avec Component n'est pas utilisable.
   */
  public static void afficher(String pMessage) {
    
  }
  
  /**
   * Afficher la boîte de dialogue avec un message sans subir le formatage Html par défaut.
   * A utiliser uniquement si la variante avec Component n'est pas utilisable.
   */
  public static void afficherTexteNonCentre(String pMessage) {
    
  }
  
  /**
   * Initialiser les composants graphiques de la boîte de dialogue.
   */
  @Override
  public void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_OUI, 'o', true);
    snBarreBouton.ajouterBouton(BOUTON_NON, 'n', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Rafraichissement des données à l'écran.
   */
  @Override
  public void rafraichir() {
  }
  
  // Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    sNPanel1 = new SNPanel();
    xRiCheckBox1 = new XRiCheckBox();
    xRiCheckBox5 = new XRiCheckBox();
    xRiCheckBox2 = new XRiCheckBox();
    xRiCheckBox6 = new XRiCheckBox();
    xRiCheckBox3 = new XRiCheckBox();
    xRiCheckBox7 = new XRiCheckBox();
    xRiCheckBox4 = new XRiCheckBox();
    sNPanel2 = new SNPanel();
    sNLabelChamp1 = new SNLabelChamp();
    sNTexte1 = new SNTexte();
    sNLabelTitre4 = new SNLabelTitre();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Mise au planning");
    setBackground(new Color(238, 238, 210));
    setResizable(false);
    setModal(true);
    setMinimumSize(new Dimension(350, 750));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBorder(null);
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setOpaque(true);
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setOpaque(false);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- xRiCheckBox1 ----
          xRiCheckBox1.setText("Lundi");
          xRiCheckBox1.setFont(new Font("sansserif", Font.PLAIN, 14));
          xRiCheckBox1.setMinimumSize(new Dimension(100, 30));
          xRiCheckBox1.setPreferredSize(new Dimension(150, 30));
          xRiCheckBox1.setName("xRiCheckBox1");
          sNPanel1.add(xRiCheckBox1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- xRiCheckBox5 ----
          xRiCheckBox5.setText("Vendredi");
          xRiCheckBox5.setFont(new Font("sansserif", Font.PLAIN, 14));
          xRiCheckBox5.setMinimumSize(new Dimension(100, 30));
          xRiCheckBox5.setPreferredSize(new Dimension(150, 30));
          xRiCheckBox5.setName("xRiCheckBox5");
          sNPanel1.add(xRiCheckBox5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- xRiCheckBox2 ----
          xRiCheckBox2.setText("Mardi");
          xRiCheckBox2.setFont(new Font("sansserif", Font.PLAIN, 14));
          xRiCheckBox2.setMinimumSize(new Dimension(100, 30));
          xRiCheckBox2.setPreferredSize(new Dimension(150, 30));
          xRiCheckBox2.setName("xRiCheckBox2");
          sNPanel1.add(xRiCheckBox2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- xRiCheckBox6 ----
          xRiCheckBox6.setText("Samedi");
          xRiCheckBox6.setFont(new Font("sansserif", Font.PLAIN, 14));
          xRiCheckBox6.setMinimumSize(new Dimension(100, 30));
          xRiCheckBox6.setPreferredSize(new Dimension(150, 30));
          xRiCheckBox6.setName("xRiCheckBox6");
          sNPanel1.add(xRiCheckBox6, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- xRiCheckBox3 ----
          xRiCheckBox3.setText("Mercredi");
          xRiCheckBox3.setFont(new Font("sansserif", Font.PLAIN, 14));
          xRiCheckBox3.setMinimumSize(new Dimension(100, 30));
          xRiCheckBox3.setPreferredSize(new Dimension(150, 30));
          xRiCheckBox3.setName("xRiCheckBox3");
          sNPanel1.add(xRiCheckBox3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- xRiCheckBox7 ----
          xRiCheckBox7.setText("Dimanche");
          xRiCheckBox7.setFont(new Font("sansserif", Font.PLAIN, 14));
          xRiCheckBox7.setMinimumSize(new Dimension(100, 30));
          xRiCheckBox7.setPreferredSize(new Dimension(150, 30));
          xRiCheckBox7.setName("xRiCheckBox7");
          sNPanel1.add(xRiCheckBox7, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- xRiCheckBox4 ----
          xRiCheckBox4.setText("Jeudi");
          xRiCheckBox4.setFont(new Font("sansserif", Font.PLAIN, 14));
          xRiCheckBox4.setMinimumSize(new Dimension(100, 30));
          xRiCheckBox4.setPreferredSize(new Dimension(150, 30));
          xRiCheckBox4.setName("xRiCheckBox4");
          sNPanel1.add(xRiCheckBox4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(sNPanel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanel2 ========
        {
          sNPanel2.setName("sNPanel2");
          sNPanel2.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- sNLabelChamp1 ----
          sNLabelChamp1.setText("Heure de d\u00e9but");
          sNLabelChamp1.setName("sNLabelChamp1");
          sNPanel2.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- sNTexte1 ----
          sNTexte1.setName("sNTexte1");
          sNPanel2.add(sNTexte1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(sNPanel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelTitre4 ----
        sNLabelTitre4.setText("Dossier du fichier d'extraction : /DOCUMENTS/FMDEV/DEM/ART/extractionArticle.CSV");
        sNLabelTitre4.setFont(new Font("sansserif", Font.PLAIN, 14));
        sNLabelTitre4.setName("sNLabelTitre4");
        pnlContenu.add(sNLabelTitre4, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNPanel sNPanel1;
  private XRiCheckBox xRiCheckBox1;
  private XRiCheckBox xRiCheckBox5;
  private XRiCheckBox xRiCheckBox2;
  private XRiCheckBox xRiCheckBox6;
  private XRiCheckBox xRiCheckBox3;
  private XRiCheckBox xRiCheckBox7;
  private XRiCheckBox xRiCheckBox4;
  private SNPanel sNPanel2;
  private SNLabelChamp sNLabelChamp1;
  private SNTexte sNTexte1;
  private SNLabelTitre sNLabelTitre4;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
