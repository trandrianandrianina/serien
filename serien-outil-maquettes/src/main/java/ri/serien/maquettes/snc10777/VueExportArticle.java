/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc10777;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.ChangeEvent;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Vue de l'écran liste de la consultation de documents de ventes.
 */
public class VueExportArticle extends JPanel {
  // Constantes
  /**
   * Constructeur.
   */
  public VueExportArticle() {
    super();
  }
  
  // -- Méthodes protégées
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
  }
  
  private void traiterClicBoutonDocument(SNBouton pSNBouton) {
  }
  
  private void traiterClicBoutonLigne(SNBouton pSNBouton) {
  }
  
  private void keyFleches(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
  }
  
  private void scpListeLigneStateChanged(ChangeEvent e) {
  }
  
  private void scpListeDocumentsStateChanged(ChangeEvent e) {
  }
  
  private void tfNumeroDocumentFocusLost(FocusEvent e) {
  }
  
  private void cbMagasinItemStateChanged(ItemEvent e) {
  }
  
  private void tfReferenceClientDocumentFocusLost(FocusEvent e) {
  }
  
  private void tblListeDocumentsMouseClicked(MouseEvent e) {
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
  }
  
  private void cbTypeDocumentItemStateChanged(ItemEvent e) {
  }
  
  private void rbRechercheDocumentItemStateChanged(ItemEvent e) {
  }
  
  private void rbRechercheLigneItemStateChanged(ItemEvent e) {
  }
  
  private void tfNumeroLigneFocusLost(FocusEvent e) {
  }
  
  private void tfReferenceClientLigneFocusLost(FocusEvent e) {
  }
  
  private void snEtablissementLigneValueChanged(SNComposantEvent e) {
  }
  
  private void cbMagasinLigneItemStateChanged(ItemEvent e) {
  }
  
  private void snPlageDateLigneValueChanged(SNComposantEvent e) {
  }
  
  private void snArticleStandardLigneValueChanged(SNComposantEvent e) {
  }
  
  private void snArticleStandardDocumentValueChanged(SNComposantEvent e) {
  }
  
  private void snFournisseurDocumentValueChanged(SNComposantEvent e) {
  }
  
  private void snFournisseurLigneValueChanged(SNComposantEvent e) {
  }
  
  private void cbTypeDocumentLigneItemStateChanged(ItemEvent e) {
  }
  
  private void ARG10AItemStateChanged(ItemEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void btnSCAN32ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void btnSCAN23ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void cbClasseItemStateChanged(ItemEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void btnSCAN30ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void btnSCAN29ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void snCategorieZPValueChanged(SNComposantEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void tfZPLongue1CaretUpdate(CaretEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void tfZPLongue2CaretUpdate(CaretEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void tfZPLongue3CaretUpdate(CaretEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void cbTriListeItemStateChanged(ItemEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    pnlBpresentation = new SNBandeauTitre();
    sNBarreBouton1 = new SNBarreBouton();
    tabbedPane1 = new JTabbedPane();
    sNPanel2 = new SNPanel();
    pnlOngletPrincipal = new JPanel();
    pnlPrincipalGauche = new JPanel();
    sNLabelTitre1 = new SNLabelTitre();
    lbRechercheGenerique = new JLabel();
    ARG76A = new XRiTextField();
    lbCodeArticle = new JLabel();
    ARG2A = new XRiTextField();
    lbMotClassement1 = new JLabel();
    ARG3A = new XRiTextField();
    lbMotClassement2 = new JLabel();
    ARG4A = new XRiTextField();
    lbFamille = new JLabel();
    snFamille = new SNFamille();
    lbSousFamille = new JLabel();
    snSousFamille = new SNSousFamille();
    lbEtat = new JLabel();
    ARG10A = new XRiComboBox();
    lbModeGestion = new JLabel();
    ARG32A = new XRiComboBox();
    lbTypeSaisie = new JLabel();
    ARG23A = new XRiComboBox();
    lbClasse = new JLabel();
    cbClasse = new XRiComboBox();
    lbSpecial = new JLabel();
    ARG24A = new XRiComboBox();
    ARG67A = new XRiCheckBox();
    ARG25A = new XRiCheckBox();
    ARG13A = new XRiCheckBox();
    lbDateCreation = new JLabel();
    pnlDateCreation = new JPanel();
    ARG20D = new XRiCalendrier();
    PLA20D = new XRiCalendrier();
    lbArticleSubstitution = new JLabel();
    ARG11A = new XRiTextField();
    lbGencode = new JLabel();
    ARG9N = new XRiTextField();
    WTGCD = new XRiCheckBox();
    pnlPrincipalDroite = new JPanel();
    lbEtablissement = new JLabel();
    snEtablissement = new SNEtablissement();
    sNLabelTitre2 = new SNLabelTitre();
    lbFournisseur = new JLabel();
    snFournisseur = new SNFournisseur();
    OPT8 = new XRiCheckBox();
    lbClassementFournisseur = new JLabel();
    WFRCLA = new XRiTextField();
    OBJ_243_OBJ_243 = new JLabel();
    ARG38N = new XRiTextField();
    OBJ_237_OBJ_237 = new JLabel();
    ARG36A = new XRiTextField();
    OBJ_240_OBJ_240 = new JLabel();
    ARG37A = new XRiTextField();
    sNLabelTitre3 = new SNLabelTitre();
    lbDisponibilite = new JLabel();
    DISPO = new XRiComboBox();
    lbTypeReapprovisionnement = new JLabel();
    ARG29A = new XRiComboBox();
    lbGestionLot = new JLabel();
    ARG31A = new XRiComboBox();
    lbTypeArticle2 = new JLabel();
    SCAN73 = new XRiComboBox();
    lbDateDernierAchat = new JLabel();
    pnlDateDernierAchat = new JPanel();
    ARG12D = new XRiCalendrier();
    PLA12D = new XRiCalendrier();
    lbUniteStock = new JLabel();
    ARG26A = new XRiTextField();
    lbFicheStockMagasin = new JLabel();
    ARG71A = new XRiTextField();
    sNLabelTitre4 = new SNLabelTitre();
    sNPanelContenu2 = new SNPanelContenu();
    scrollPane1 = new JScrollPane();
    tblListeDocument = new NRiTable();
    
    // ======== this ========
    setMinimumSize(new Dimension(1024, 780));
    setPreferredSize(new Dimension(1024, 780));
    setOpaque(false);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ---- pnlBpresentation ----
      pnlBpresentation.setText("Export articles");
      pnlBpresentation.setName("pnlBpresentation");
      pnlPrincipal.add(pnlBpresentation, BorderLayout.NORTH);
      
      // ---- sNBarreBouton1 ----
      sNBarreBouton1.setName("sNBarreBouton1");
      pnlPrincipal.add(sNBarreBouton1, BorderLayout.SOUTH);
      
      // ======== tabbedPane1 ========
      {
        tabbedPane1.setFont(new Font("sansserif", Font.PLAIN, 14));
        tabbedPane1.setName("tabbedPane1");
        
        // ======== sNPanel2 ========
        {
          sNPanel2.setName("sNPanel2");
          sNPanel2.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== pnlOngletPrincipal ========
          {
            pnlOngletPrincipal.setMinimumSize(new Dimension(840, 250));
            pnlOngletPrincipal.setMaximumSize(new Dimension(840, 5000));
            pnlOngletPrincipal.setBackground(new Color(239, 239, 222));
            pnlOngletPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
            pnlOngletPrincipal.setPreferredSize(new Dimension(915, 210));
            pnlOngletPrincipal.setName("pnlOngletPrincipal");
            pnlOngletPrincipal.setLayout(new GridLayout(1, 2, 5, 5));
            
            // ======== pnlPrincipalGauche ========
            {
              pnlPrincipalGauche.setMinimumSize(new Dimension(450, 200));
              pnlPrincipalGauche.setBackground(new Color(239, 239, 222));
              pnlPrincipalGauche.setPreferredSize(new Dimension(450, 200));
              pnlPrincipalGauche.setBorder(null);
              pnlPrincipalGauche.setName("pnlPrincipalGauche");
              pnlPrincipalGauche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).columnWidths = new int[] { 185, 0, 0 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).rowHeights =
                  new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- sNLabelTitre1 ----
              sNLabelTitre1.setText("Crit\u00e8res article");
              sNLabelTitre1.setName("sNLabelTitre1");
              pnlPrincipalGauche.add(sNLabelTitre1, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbRechercheGenerique ----
              lbRechercheGenerique.setText("Recherche g\u00e9n\u00e9rique");
              lbRechercheGenerique.setToolTipText("Recherche sur le code article, ses libell\u00e9s et r\u00e9f\u00e9rences");
              lbRechercheGenerique.setPreferredSize(new Dimension(200, 30));
              lbRechercheGenerique.setMinimumSize(new Dimension(200, 30));
              lbRechercheGenerique.setFont(lbRechercheGenerique.getFont().deriveFont(lbRechercheGenerique.getFont().getSize() + 2f));
              lbRechercheGenerique.setHorizontalAlignment(SwingConstants.TRAILING);
              lbRechercheGenerique.setMaximumSize(new Dimension(200, 30));
              lbRechercheGenerique.setName("lbRechercheGenerique");
              pnlPrincipalGauche.add(lbRechercheGenerique, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG76A ----
              ARG76A.setComponentPopupMenu(null);
              ARG76A.setMinimumSize(new Dimension(250, 30));
              ARG76A.setPreferredSize(new Dimension(250, 30));
              ARG76A.setToolTipText(
                  "<html>Recherche sur :<br>\n- libell\u00e9 article (milieu)<br>\n- code article (d\u00e9but)<br>\n- mot de classement 1 (d\u00e9but)<br>\n- mot de classement 2 (d\u00e9but)<br>\n- gencode (d\u00e9but)<br>\n- gencode compl\u00e9mentaire (d\u00e9but)<br>\n- raison sociale fournisseur (d\u00e9but)<br>\n- r\u00e9f\u00e9rence fournisseur de la condition d'achats (d\u00e9but)<br>\n- r\u00e9f\u00e9rence constructeur de la condition d'achats (d\u00e9but).<br>\n</html>");
              ARG76A.setFont(ARG76A.getFont().deriveFont(ARG76A.getFont().getSize() + 2f));
              ARG76A.setName("ARG76A");
              pnlPrincipalGauche.add(ARG76A, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCodeArticle ----
              lbCodeArticle.setText("Code article");
              lbCodeArticle.setComponentPopupMenu(null);
              lbCodeArticle.setMaximumSize(new Dimension(150, 30));
              lbCodeArticle.setMinimumSize(new Dimension(150, 30));
              lbCodeArticle.setPreferredSize(new Dimension(150, 30));
              lbCodeArticle.setFont(lbCodeArticle.getFont().deriveFont(lbCodeArticle.getFont().getSize() + 2f));
              lbCodeArticle.setHorizontalAlignment(SwingConstants.TRAILING);
              lbCodeArticle.setName("lbCodeArticle");
              pnlPrincipalGauche.add(lbCodeArticle, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG2A ----
              ARG2A.setComponentPopupMenu(null);
              ARG2A.setMinimumSize(new Dimension(200, 30));
              ARG2A.setPreferredSize(new Dimension(200, 30));
              ARG2A.setFont(ARG2A.getFont().deriveFont(ARG2A.getFont().getSize() + 2f));
              ARG2A.setName("ARG2A");
              pnlPrincipalGauche.add(ARG2A, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMotClassement1 ----
              lbMotClassement1.setText("Mot de classement 1");
              lbMotClassement1.setComponentPopupMenu(null);
              lbMotClassement1.setMaximumSize(new Dimension(150, 30));
              lbMotClassement1.setMinimumSize(new Dimension(150, 30));
              lbMotClassement1.setPreferredSize(new Dimension(150, 30));
              lbMotClassement1.setFont(lbMotClassement1.getFont().deriveFont(lbMotClassement1.getFont().getSize() + 2f));
              lbMotClassement1.setHorizontalAlignment(SwingConstants.TRAILING);
              lbMotClassement1.setName("lbMotClassement1");
              pnlPrincipalGauche.add(lbMotClassement1, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG3A ----
              ARG3A.setComponentPopupMenu(null);
              ARG3A.setMinimumSize(new Dimension(200, 30));
              ARG3A.setPreferredSize(new Dimension(200, 30));
              ARG3A.setFont(ARG3A.getFont().deriveFont(ARG3A.getFont().getSize() + 2f));
              ARG3A.setName("ARG3A");
              pnlPrincipalGauche.add(ARG3A, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMotClassement2 ----
              lbMotClassement2.setText("Mot de classement 2");
              lbMotClassement2.setComponentPopupMenu(null);
              lbMotClassement2.setMaximumSize(new Dimension(150, 30));
              lbMotClassement2.setMinimumSize(new Dimension(150, 30));
              lbMotClassement2.setPreferredSize(new Dimension(150, 30));
              lbMotClassement2.setFont(lbMotClassement2.getFont().deriveFont(lbMotClassement2.getFont().getSize() + 2f));
              lbMotClassement2.setHorizontalAlignment(SwingConstants.TRAILING);
              lbMotClassement2.setName("lbMotClassement2");
              pnlPrincipalGauche.add(lbMotClassement2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG4A ----
              ARG4A.setComponentPopupMenu(null);
              ARG4A.setMinimumSize(new Dimension(200, 30));
              ARG4A.setPreferredSize(new Dimension(200, 30));
              ARG4A.setFont(ARG4A.getFont().deriveFont(ARG4A.getFont().getSize() + 2f));
              ARG4A.setName("ARG4A");
              pnlPrincipalGauche.add(ARG4A, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbFamille ----
              lbFamille.setText("Famille");
              lbFamille.setComponentPopupMenu(null);
              lbFamille.setMaximumSize(new Dimension(150, 30));
              lbFamille.setMinimumSize(new Dimension(150, 30));
              lbFamille.setPreferredSize(new Dimension(150, 30));
              lbFamille.setFont(lbFamille.getFont().deriveFont(lbFamille.getFont().getSize() + 2f));
              lbFamille.setHorizontalAlignment(SwingConstants.TRAILING);
              lbFamille.setName("lbFamille");
              pnlPrincipalGauche.add(lbFamille, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snFamille ----
              snFamille.setName("snFamille");
              pnlPrincipalGauche.add(snFamille, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbSousFamille ----
              lbSousFamille.setText("Sous-famille");
              lbSousFamille.setComponentPopupMenu(null);
              lbSousFamille.setMaximumSize(new Dimension(150, 30));
              lbSousFamille.setMinimumSize(new Dimension(150, 30));
              lbSousFamille.setPreferredSize(new Dimension(150, 30));
              lbSousFamille.setFont(lbSousFamille.getFont().deriveFont(lbSousFamille.getFont().getSize() + 2f));
              lbSousFamille.setHorizontalAlignment(SwingConstants.TRAILING);
              lbSousFamille.setName("lbSousFamille");
              pnlPrincipalGauche.add(lbSousFamille, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snSousFamille ----
              snSousFamille.setName("snSousFamille");
              pnlPrincipalGauche.add(snSousFamille, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbEtat ----
              lbEtat.setText("Etat");
              lbEtat.setPreferredSize(new Dimension(150, 30));
              lbEtat.setMinimumSize(new Dimension(150, 30));
              lbEtat.setMaximumSize(new Dimension(150, 30));
              lbEtat.setFont(lbEtat.getFont().deriveFont(lbEtat.getFont().getSize() + 2f));
              lbEtat.setHorizontalAlignment(SwingConstants.TRAILING);
              lbEtat.setInheritsPopupMenu(false);
              lbEtat.setName("lbEtat");
              pnlPrincipalGauche.add(lbEtat, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG10A ----
              ARG10A.setModel(new DefaultComboBoxModel(
                  new String[] { "Tous", "D\u00e9sactiv\u00e9", "Epuis\u00e9", "Pr\u00e9-fin de s\u00e9rie", "Fin de s\u00e9rie" }));
              ARG10A.setComponentPopupMenu(null);
              ARG10A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG10A.setFont(ARG10A.getFont().deriveFont(ARG10A.getFont().getSize() + 2f));
              ARG10A.setMinimumSize(new Dimension(200, 30));
              ARG10A.setPreferredSize(new Dimension(200, 30));
              ARG10A.setMaximumSize(new Dimension(200, 30));
              ARG10A.setName("ARG10A");
              ARG10A.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                  ARG10AItemStateChanged(e);
                }
              });
              pnlPrincipalGauche.add(ARG10A, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbModeGestion ----
              lbModeGestion.setText("Mode de gestion");
              lbModeGestion.setMinimumSize(new Dimension(150, 30));
              lbModeGestion.setPreferredSize(new Dimension(150, 30));
              lbModeGestion.setFont(lbModeGestion.getFont().deriveFont(lbModeGestion.getFont().getSize() + 2f));
              lbModeGestion.setHorizontalAlignment(SwingConstants.TRAILING);
              lbModeGestion.setMaximumSize(new Dimension(150, 30));
              lbModeGestion.setName("lbModeGestion");
              pnlPrincipalGauche.add(lbModeGestion, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG32A ----
              ARG32A.setModel(new DefaultComboBoxModel(
                  new String[] { "N\u00e9goce", "Achet\u00e9, non vendu", "Vendu, non achet\u00e9", "Fabriqu\u00e9" }));
              ARG32A.setComponentPopupMenu(null);
              ARG32A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG32A.setFont(ARG32A.getFont().deriveFont(ARG32A.getFont().getSize() + 2f));
              ARG32A.setPreferredSize(new Dimension(200, 30));
              ARG32A.setMinimumSize(new Dimension(200, 30));
              ARG32A.setName("ARG32A");
              pnlPrincipalGauche.add(ARG32A, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTypeSaisie ----
              lbTypeSaisie.setText("Type de saisie");
              lbTypeSaisie.setMinimumSize(new Dimension(150, 30));
              lbTypeSaisie.setPreferredSize(new Dimension(150, 30));
              lbTypeSaisie.setFont(lbTypeSaisie.getFont().deriveFont(lbTypeSaisie.getFont().getSize() + 2f));
              lbTypeSaisie.setHorizontalAlignment(SwingConstants.TRAILING);
              lbTypeSaisie.setMaximumSize(new Dimension(150, 30));
              lbTypeSaisie.setName("lbTypeSaisie");
              pnlPrincipalGauche.add(lbTypeSaisie, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG23A ----
              ARG23A.setModel(new DefaultComboBoxModel(new String[] { "Standard", "Article transport",
                  "Article sur-conditionnement (palette)", "Article kit commentaire", "Par num\u00e9ro de s\u00e9rie", "En valeur",
                  "En double quantit\u00e9", "En longueur", "En surface", "En surface et nombre", "En volume" }));
              ARG23A.setComponentPopupMenu(null);
              ARG23A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG23A.setFont(ARG23A.getFont().deriveFont(ARG23A.getFont().getSize() + 2f));
              ARG23A.setPreferredSize(new Dimension(260, 30));
              ARG23A.setMinimumSize(new Dimension(260, 30));
              ARG23A.setMaximumRowCount(15);
              ARG23A.setName("ARG23A");
              pnlPrincipalGauche.add(ARG23A, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbClasse ----
              lbClasse.setText("Classe");
              lbClasse.setComponentPopupMenu(null);
              lbClasse.setMaximumSize(new Dimension(150, 30));
              lbClasse.setMinimumSize(new Dimension(150, 30));
              lbClasse.setPreferredSize(new Dimension(150, 30));
              lbClasse.setFont(lbClasse.getFont().deriveFont(lbClasse.getFont().getSize() + 2f));
              lbClasse.setHorizontalAlignment(SwingConstants.TRAILING);
              lbClasse.setName("lbClasse");
              pnlPrincipalGauche.add(lbClasse, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- cbClasse ----
              cbClasse.setModel(new DefaultComboBoxModel(new String[] { "Gamme", "Hors gamme", "Personnalis\u00e9e" }));
              cbClasse.setComponentPopupMenu(null);
              cbClasse.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              cbClasse.setFont(cbClasse.getFont().deriveFont(cbClasse.getFont().getSize() + 2f));
              cbClasse.setMinimumSize(new Dimension(200, 30));
              cbClasse.setPreferredSize(new Dimension(200, 30));
              cbClasse.setMaximumSize(new Dimension(200, 30));
              cbClasse.setName("cbClasse");
              cbClasse.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                  ARG10AItemStateChanged(e);
                  cbClasseItemStateChanged(e);
                }
              });
              pnlPrincipalGauche.add(cbClasse, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbSpecial ----
              lbSpecial.setText("Sp\u00e9cial");
              lbSpecial.setComponentPopupMenu(null);
              lbSpecial.setMaximumSize(new Dimension(150, 30));
              lbSpecial.setMinimumSize(new Dimension(150, 30));
              lbSpecial.setPreferredSize(new Dimension(150, 30));
              lbSpecial.setFont(lbSpecial.getFont().deriveFont(lbSpecial.getFont().getSize() + 2f));
              lbSpecial.setHorizontalAlignment(SwingConstants.TRAILING);
              lbSpecial.setName("lbSpecial");
              pnlPrincipalGauche.add(lbSpecial, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG24A ----
              ARG24A.setModel(
                  new DefaultComboBoxModel(new String[] { "Tous", "Ne pas afficher les sp\u00e9ciaux", "Uniquement les sp\u00e9ciaux" }));
              ARG24A.setComponentPopupMenu(null);
              ARG24A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG24A.setFont(ARG24A.getFont().deriveFont(ARG24A.getFont().getSize() + 2f));
              ARG24A.setPreferredSize(new Dimension(260, 30));
              ARG24A.setMinimumSize(new Dimension(260, 30));
              ARG24A.setMaximumRowCount(15);
              ARG24A.setToolTipText(
                  "<html>L'attribut \"Sp\u00e9cial\" se configure dans les informations diverses, onglet \"Divers stock/unit\u00e9\" d'un article.</html>\n");
              ARG24A.setName("ARG24A");
              pnlPrincipalGauche.add(ARG24A, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- ARG67A ----
              ARG67A.setText("Direct usine");
              ARG67A.setMinimumSize(new Dimension(200, 30));
              ARG67A.setPreferredSize(new Dimension(200, 30));
              ARG67A.setFont(ARG67A.getFont().deriveFont(ARG67A.getFont().getSize() + 2f));
              ARG67A.setComponentPopupMenu(null);
              ARG67A.setName("ARG67A");
              pnlPrincipalGauche.add(ARG67A, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG25A ----
              ARG25A.setText("Non stock\u00e9");
              ARG25A.setComponentPopupMenu(null);
              ARG25A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG25A.setMinimumSize(new Dimension(150, 30));
              ARG25A.setPreferredSize(new Dimension(150, 30));
              ARG25A.setFont(ARG25A.getFont().deriveFont(ARG25A.getFont().getSize() + 2f));
              ARG25A.setMaximumSize(new Dimension(150, 30));
              ARG25A.setName("ARG25A");
              pnlPrincipalGauche.add(ARG25A, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- ARG13A ----
              ARG13A.setText("Nomenclature");
              ARG13A.setComponentPopupMenu(null);
              ARG13A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG13A.setMinimumSize(new Dimension(150, 30));
              ARG13A.setPreferredSize(new Dimension(150, 30));
              ARG13A.setFont(ARG13A.getFont().deriveFont(ARG13A.getFont().getSize() + 2f));
              ARG13A.setMaximumSize(new Dimension(150, 30));
              ARG13A.setName("ARG13A");
              pnlPrincipalGauche.add(ARG13A, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbDateCreation ----
              lbDateCreation.setText("Date de cr\u00e9ation");
              lbDateCreation.setComponentPopupMenu(null);
              lbDateCreation.setMaximumSize(new Dimension(200, 30));
              lbDateCreation.setMinimumSize(new Dimension(200, 30));
              lbDateCreation.setPreferredSize(new Dimension(200, 30));
              lbDateCreation.setFont(lbDateCreation.getFont().deriveFont(lbDateCreation.getFont().getSize() + 2f));
              lbDateCreation.setHorizontalAlignment(SwingConstants.TRAILING);
              lbDateCreation.setName("lbDateCreation");
              pnlPrincipalGauche.add(lbDateCreation, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlDateCreation ========
              {
                pnlDateCreation.setOpaque(false);
                pnlDateCreation.setName("pnlDateCreation");
                pnlDateCreation.setLayout(new GridLayout(1, 2, 5, 5));
                
                // ---- ARG20D ----
                ARG20D.setComponentPopupMenu(null);
                ARG20D.setMinimumSize(new Dimension(110, 30));
                ARG20D.setPreferredSize(new Dimension(110, 30));
                ARG20D.setFont(ARG20D.getFont().deriveFont(ARG20D.getFont().getSize() + 2f));
                ARG20D.setName("ARG20D");
                pnlDateCreation.add(ARG20D);
                
                // ---- PLA20D ----
                PLA20D.setComponentPopupMenu(null);
                PLA20D.setMinimumSize(new Dimension(110, 30));
                PLA20D.setPreferredSize(new Dimension(110, 30));
                PLA20D.setFont(PLA20D.getFont().deriveFont(PLA20D.getFont().getSize() + 2f));
                PLA20D.setName("PLA20D");
                pnlDateCreation.add(PLA20D);
              }
              pnlPrincipalGauche.add(pnlDateCreation, new GridBagConstraints(1, 14, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbArticleSubstitution ----
              lbArticleSubstitution.setText("Code article de substitution");
              lbArticleSubstitution.setComponentPopupMenu(null);
              lbArticleSubstitution.setMaximumSize(new Dimension(200, 30));
              lbArticleSubstitution.setMinimumSize(new Dimension(200, 30));
              lbArticleSubstitution.setPreferredSize(new Dimension(200, 30));
              lbArticleSubstitution.setFont(lbArticleSubstitution.getFont().deriveFont(lbArticleSubstitution.getFont().getSize() + 2f));
              lbArticleSubstitution.setHorizontalAlignment(SwingConstants.TRAILING);
              lbArticleSubstitution.setName("lbArticleSubstitution");
              pnlPrincipalGauche.add(lbArticleSubstitution, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG11A ----
              ARG11A.setComponentPopupMenu(null);
              ARG11A.setMinimumSize(new Dimension(200, 30));
              ARG11A.setPreferredSize(new Dimension(200, 30));
              ARG11A.setFont(ARG11A.getFont().deriveFont(ARG11A.getFont().getSize() + 2f));
              ARG11A.setName("ARG11A");
              pnlPrincipalGauche.add(ARG11A, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbGencode ----
              lbGencode.setText("Gencode");
              lbGencode.setComponentPopupMenu(null);
              lbGencode.setMaximumSize(new Dimension(200, 30));
              lbGencode.setMinimumSize(new Dimension(200, 30));
              lbGencode.setPreferredSize(new Dimension(200, 30));
              lbGencode.setFont(lbGencode.getFont().deriveFont(lbGencode.getFont().getSize() + 2f));
              lbGencode.setHorizontalAlignment(SwingConstants.TRAILING);
              lbGencode.setName("lbGencode");
              pnlPrincipalGauche.add(lbGencode, new GridBagConstraints(0, 16, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG9N ----
              ARG9N.setComponentPopupMenu(null);
              ARG9N.setMinimumSize(new Dimension(150, 30));
              ARG9N.setPreferredSize(new Dimension(200, 30));
              ARG9N.setFont(ARG9N.getFont().deriveFont(ARG9N.getFont().getSize() + 2f));
              ARG9N.setName("ARG9N");
              pnlPrincipalGauche.add(ARG9N, new GridBagConstraints(1, 16, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- WTGCD ----
              WTGCD.setText("Gencode secondaire");
              WTGCD.setComponentPopupMenu(null);
              WTGCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WTGCD.setMinimumSize(new Dimension(100, 30));
              WTGCD.setPreferredSize(new Dimension(100, 30));
              WTGCD.setFont(WTGCD.getFont().deriveFont(WTGCD.getFont().getSize() + 2f));
              WTGCD.setToolTipText(
                  "<html>Non coch\u00e9 = Recherche sur le gencode principal de la fiche article. <br>\nCoch\u00e9 = Recherche dans les gencodes secondaires de l'article (juqu'\u00e0 5 gencodes secondaires suppl\u00e9mentaires).<br>\n</html>");
              WTGCD.setName("WTGCD");
              pnlPrincipalGauche.add(WTGCD, new GridBagConstraints(1, 17, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletPrincipal.add(pnlPrincipalGauche);
            
            // ======== pnlPrincipalDroite ========
            {
              pnlPrincipalDroite.setPreferredSize(new Dimension(450, 200));
              pnlPrincipalDroite.setMinimumSize(new Dimension(450, 200));
              pnlPrincipalDroite.setOpaque(false);
              pnlPrincipalDroite.setBorder(null);
              pnlPrincipalDroite.setName("pnlPrincipalDroite");
              pnlPrincipalDroite.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).columnWidths = new int[] { 185, 313, 0 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).rowHeights =
                  new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbEtablissement ----
              lbEtablissement.setText("Etablissement");
              lbEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbEtablissement.setHorizontalAlignment(SwingConstants.RIGHT);
              lbEtablissement.setMinimumSize(new Dimension(150, 30));
              lbEtablissement.setPreferredSize(new Dimension(150, 30));
              lbEtablissement.setMaximumSize(new Dimension(150, 30));
              lbEtablissement.setName("lbEtablissement");
              pnlPrincipalDroite.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snEtablissement ----
              snEtablissement.setMinimumSize(new Dimension(320, 30));
              snEtablissement.setPreferredSize(new Dimension(320, 30));
              snEtablissement.setName("snEtablissement");
              pnlPrincipalDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelTitre2 ----
              sNLabelTitre2.setText("Crit\u00e8res fournisseur");
              sNLabelTitre2.setName("sNLabelTitre2");
              pnlPrincipalDroite.add(sNLabelTitre2, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbFournisseur ----
              lbFournisseur.setText("Fournisseur");
              lbFournisseur.setFont(lbFournisseur.getFont().deriveFont(lbFournisseur.getFont().getSize() + 2f));
              lbFournisseur.setMinimumSize(new Dimension(150, 30));
              lbFournisseur.setPreferredSize(new Dimension(150, 30));
              lbFournisseur.setHorizontalAlignment(SwingConstants.TRAILING);
              lbFournisseur.setMaximumSize(new Dimension(150, 30));
              lbFournisseur.setName("lbFournisseur");
              pnlPrincipalDroite.add(lbFournisseur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snFournisseur ----
              snFournisseur.setComponentPopupMenu(null);
              snFournisseur.setName("snFournisseur");
              pnlPrincipalDroite.add(snFournisseur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- OPT8 ----
              OPT8.setText("Uniquement fournisseur principal");
              OPT8.setComponentPopupMenu(null);
              OPT8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OPT8.setMinimumSize(new Dimension(100, 30));
              OPT8.setPreferredSize(new Dimension(100, 30));
              OPT8.setFont(OPT8.getFont().deriveFont(OPT8.getFont().getSize() + 2f));
              OPT8.setToolTipText(
                  "<html>Non coch\u00e9 = Recherche le num\u00e9ro fournisseur dans les conditions d'achats, donc parmi le fournisseur principal et les fournisseurs secondaires.<br>\nCoch\u00e9 = Recherche dans le num\u00e9ro fournisseur associ\u00e9 \u00e0 la fiche article, donc uniquement le fournisseur principal. <br>\n</html>");
              OPT8.setName("OPT8");
              pnlPrincipalDroite.add(OPT8, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbClassementFournisseur ----
              lbClassementFournisseur.setText("Classement fournisseur");
              lbClassementFournisseur
                  .setFont(lbClassementFournisseur.getFont().deriveFont(lbClassementFournisseur.getFont().getSize() + 2f));
              lbClassementFournisseur.setMinimumSize(new Dimension(150, 30));
              lbClassementFournisseur.setPreferredSize(new Dimension(150, 30));
              lbClassementFournisseur.setHorizontalAlignment(SwingConstants.TRAILING);
              lbClassementFournisseur.setMaximumSize(new Dimension(150, 30));
              lbClassementFournisseur.setName("lbClassementFournisseur");
              pnlPrincipalDroite.add(lbClassementFournisseur, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- WFRCLA ----
              WFRCLA.setComponentPopupMenu(null);
              WFRCLA.setFont(WFRCLA.getFont().deriveFont(WFRCLA.getFont().getSize() + 2f));
              WFRCLA.setMinimumSize(new Dimension(200, 30));
              WFRCLA.setPreferredSize(new Dimension(200, 30));
              WFRCLA.setName("WFRCLA");
              pnlPrincipalDroite.add(WFRCLA, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- OBJ_243_OBJ_243 ----
              OBJ_243_OBJ_243.setText("Gencode article fournisseur");
              OBJ_243_OBJ_243.setFont(OBJ_243_OBJ_243.getFont().deriveFont(OBJ_243_OBJ_243.getFont().getSize() + 2f));
              OBJ_243_OBJ_243.setMinimumSize(new Dimension(200, 30));
              OBJ_243_OBJ_243.setPreferredSize(new Dimension(200, 30));
              OBJ_243_OBJ_243.setHorizontalAlignment(SwingConstants.TRAILING);
              OBJ_243_OBJ_243.setName("OBJ_243_OBJ_243");
              pnlPrincipalDroite.add(OBJ_243_OBJ_243, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG38N ----
              ARG38N.setComponentPopupMenu(null);
              ARG38N.setFont(ARG38N.getFont().deriveFont(ARG38N.getFont().getSize() + 2f));
              ARG38N.setMinimumSize(new Dimension(200, 30));
              ARG38N.setPreferredSize(new Dimension(200, 30));
              ARG38N.setToolTipText("Gencode de l'article chez le fournisseur, d\u00e9fini dans la condition d'achats.");
              ARG38N.setName("ARG38N");
              pnlPrincipalDroite.add(ARG38N, new GridBagConstraints(1, 5, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- OBJ_237_OBJ_237 ----
              OBJ_237_OBJ_237.setText("R\u00e9f\u00e9rence article fournisseur");
              OBJ_237_OBJ_237.setFont(OBJ_237_OBJ_237.getFont().deriveFont(OBJ_237_OBJ_237.getFont().getSize() + 2f));
              OBJ_237_OBJ_237.setMinimumSize(new Dimension(200, 30));
              OBJ_237_OBJ_237.setPreferredSize(new Dimension(200, 30));
              OBJ_237_OBJ_237.setHorizontalAlignment(SwingConstants.TRAILING);
              OBJ_237_OBJ_237.setName("OBJ_237_OBJ_237");
              pnlPrincipalDroite.add(OBJ_237_OBJ_237, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG36A ----
              ARG36A.setComponentPopupMenu(null);
              ARG36A.setFont(ARG36A.getFont().deriveFont(ARG36A.getFont().getSize() + 2f));
              ARG36A.setMinimumSize(new Dimension(200, 30));
              ARG36A.setPreferredSize(new Dimension(200, 30));
              ARG36A.setName("ARG36A");
              pnlPrincipalDroite.add(ARG36A, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- OBJ_240_OBJ_240 ----
              OBJ_240_OBJ_240.setText("R\u00e9f\u00e9rence article fabricant");
              OBJ_240_OBJ_240.setFont(OBJ_240_OBJ_240.getFont().deriveFont(OBJ_240_OBJ_240.getFont().getSize() + 2f));
              OBJ_240_OBJ_240.setMinimumSize(new Dimension(200, 30));
              OBJ_240_OBJ_240.setPreferredSize(new Dimension(200, 30));
              OBJ_240_OBJ_240.setHorizontalAlignment(SwingConstants.TRAILING);
              OBJ_240_OBJ_240.setName("OBJ_240_OBJ_240");
              pnlPrincipalDroite.add(OBJ_240_OBJ_240, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG37A ----
              ARG37A.setComponentPopupMenu(null);
              ARG37A.setFont(ARG37A.getFont().deriveFont(ARG37A.getFont().getSize() + 2f));
              ARG37A.setMinimumSize(new Dimension(200, 30));
              ARG37A.setPreferredSize(new Dimension(200, 30));
              ARG37A.setName("ARG37A");
              pnlPrincipalDroite.add(ARG37A, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelTitre3 ----
              sNLabelTitre3.setText("Crit\u00e8res achats/stock");
              sNLabelTitre3.setName("sNLabelTitre3");
              pnlPrincipalDroite.add(sNLabelTitre3, new GridBagConstraints(0, 8, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbDisponibilite ----
              lbDisponibilite.setText("Disponibilit\u00e9");
              lbDisponibilite.setMinimumSize(new Dimension(200, 30));
              lbDisponibilite.setPreferredSize(new Dimension(200, 30));
              lbDisponibilite.setFont(lbDisponibilite.getFont().deriveFont(lbDisponibilite.getFont().getSize() + 2f));
              lbDisponibilite.setHorizontalAlignment(SwingConstants.TRAILING);
              lbDisponibilite.setName("lbDisponibilite");
              pnlPrincipalDroite.add(lbDisponibilite, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- DISPO ----
              DISPO.setModel(
                  new DefaultComboBoxModel(new String[] { "Tous", "Disponible", "Non disponible", "En stock", "En rupture de stock" }));
              DISPO.setComponentPopupMenu(null);
              DISPO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              DISPO.setFont(DISPO.getFont().deriveFont(DISPO.getFont().getSize() + 2f));
              DISPO.setMinimumSize(new Dimension(300, 30));
              DISPO.setPreferredSize(new Dimension(300, 30));
              DISPO.setToolTipText(
                  "<html>Disponible = Le stock disponible \u00e0 la vente est positif.<br>\nNon disponible = Le stock disponible \u00e0 la vente est n\u00e9gatif ou nul.<br>\nEn stock = Le stock physique est positif.<br>\nEn rupture de stock =  Le stock physique est n\u00e9gatif ou nul.\n</html>");
              DISPO.setName("DISPO");
              pnlPrincipalDroite.add(DISPO, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTypeReapprovisionnement ----
              lbTypeReapprovisionnement.setText("Type de r\u00e9approvisionnement");
              lbTypeReapprovisionnement.setMinimumSize(new Dimension(200, 30));
              lbTypeReapprovisionnement.setPreferredSize(new Dimension(200, 30));
              lbTypeReapprovisionnement
                  .setFont(lbTypeReapprovisionnement.getFont().deriveFont(lbTypeReapprovisionnement.getFont().getSize() + 2f));
              lbTypeReapprovisionnement.setHorizontalAlignment(SwingConstants.TRAILING);
              lbTypeReapprovisionnement.setMaximumSize(new Dimension(200, 30));
              lbTypeReapprovisionnement.setInheritsPopupMenu(false);
              lbTypeReapprovisionnement.setName("lbTypeReapprovisionnement");
              pnlPrincipalDroite.add(lbTypeReapprovisionnement, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG29A ----
              ARG29A.setModel(new DefaultComboBoxModel(new String[] { "Rupture sur stock minimum", "Sur consommation moyenne", "Manuel",
                  "Sur consommation pr\u00e9vue", "Sur consommation moyenne plafonn\u00e9e", "Plafond converte globale",
                  "Compl\u00e9ment stock maximum", "Plafond de converture par magasin", "Non g\u00e9r\u00e9" }));
              ARG29A.setComponentPopupMenu(null);
              ARG29A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG29A.setFont(ARG29A.getFont().deriveFont(ARG29A.getFont().getSize() + 2f));
              ARG29A.setMinimumSize(new Dimension(300, 30));
              ARG29A.setPreferredSize(new Dimension(300, 30));
              ARG29A.setMaximumRowCount(15);
              ARG29A.setName("ARG29A");
              pnlPrincipalDroite.add(ARG29A, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbGestionLot ----
              lbGestionLot.setText("Gestion des lots");
              lbGestionLot.setMaximumSize(new Dimension(200, 30));
              lbGestionLot.setMinimumSize(new Dimension(200, 30));
              lbGestionLot.setPreferredSize(new Dimension(200, 30));
              lbGestionLot.setFont(lbGestionLot.getFont().deriveFont(lbGestionLot.getFont().getSize() + 2f));
              lbGestionLot.setHorizontalAlignment(SwingConstants.TRAILING);
              lbGestionLot.setName("lbGestionLot");
              pnlPrincipalDroite.add(lbGestionLot, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG31A ----
              ARG31A
                  .setModel(new DefaultComboBoxModel(new String[] { "Tous", "G\u00e9r\u00e9 par lots", "G\u00e9r\u00e9 par sous lots" }));
              ARG31A.setComponentPopupMenu(null);
              ARG31A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG31A.setFont(ARG31A.getFont().deriveFont(ARG31A.getFont().getSize() + 2f));
              ARG31A.setMinimumSize(new Dimension(200, 30));
              ARG31A.setPreferredSize(new Dimension(200, 30));
              ARG31A.setMaximumSize(new Dimension(200, 30));
              ARG31A.setName("ARG31A");
              pnlPrincipalDroite.add(ARG31A, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTypeArticle2 ----
              lbTypeArticle2.setText("D\u00e9pr\u00e9ciation");
              lbTypeArticle2.setMinimumSize(new Dimension(200, 30));
              lbTypeArticle2.setPreferredSize(new Dimension(200, 30));
              lbTypeArticle2.setFont(lbTypeArticle2.getFont().deriveFont(lbTypeArticle2.getFont().getSize() + 2f));
              lbTypeArticle2.setHorizontalAlignment(SwingConstants.TRAILING);
              lbTypeArticle2.setMaximumSize(new Dimension(200, 30));
              lbTypeArticle2.setName("lbTypeArticle2");
              pnlPrincipalDroite.add(lbTypeArticle2, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- SCAN73 ----
              SCAN73.setModel(new DefaultComboBoxModel(
                  new String[] { "Tous", "Article non d\u00e9pr\u00e9ci\u00e9", "Article d\u00e9pr\u00e9ci\u00e9" }));
              SCAN73.setComponentPopupMenu(null);
              SCAN73.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN73.setFont(SCAN73.getFont().deriveFont(SCAN73.getFont().getSize() + 2f));
              SCAN73.setPreferredSize(new Dimension(200, 30));
              SCAN73.setMinimumSize(new Dimension(200, 30));
              SCAN73.setMaximumRowCount(15);
              SCAN73.setToolTipText(
                  "<html>D\u00e9pri\u00e9c\u00e9 = S\u00e9lectionner les articles d\u00e9pr\u00e9ci\u00e9s (pourcentage de d\u00e9pr\u00e9ciation > 0).<br>\nNon d\u00e9pri\u00e9c\u00e9 = S\u00e9lectionner les articles non d\u00e9pr\u00e9ci\u00e9s (pourcentage de d\u00e9pr\u00e9ciation = 0).<br>\n</html>\n");
              SCAN73.setName("SCAN73");
              pnlPrincipalDroite.add(SCAN73, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbDateDernierAchat ----
              lbDateDernierAchat.setText("Date de dernier achat");
              lbDateDernierAchat.setComponentPopupMenu(null);
              lbDateDernierAchat.setMaximumSize(new Dimension(200, 30));
              lbDateDernierAchat.setMinimumSize(new Dimension(200, 30));
              lbDateDernierAchat.setPreferredSize(new Dimension(200, 30));
              lbDateDernierAchat.setFont(lbDateDernierAchat.getFont().deriveFont(lbDateDernierAchat.getFont().getSize() + 2f));
              lbDateDernierAchat.setHorizontalAlignment(SwingConstants.TRAILING);
              lbDateDernierAchat.setName("lbDateDernierAchat");
              pnlPrincipalDroite.add(lbDateDernierAchat, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlDateDernierAchat ========
              {
                pnlDateDernierAchat.setOpaque(false);
                pnlDateDernierAchat.setName("pnlDateDernierAchat");
                pnlDateDernierAchat.setLayout(new GridLayout(1, 2, 5, 5));
                
                // ---- ARG12D ----
                ARG12D.setComponentPopupMenu(null);
                ARG12D.setMinimumSize(new Dimension(110, 30));
                ARG12D.setPreferredSize(new Dimension(110, 30));
                ARG12D.setFont(ARG12D.getFont().deriveFont(ARG12D.getFont().getSize() + 2f));
                ARG12D.setName("ARG12D");
                pnlDateDernierAchat.add(ARG12D);
                
                // ---- PLA12D ----
                PLA12D.setComponentPopupMenu(null);
                PLA12D.setMinimumSize(new Dimension(110, 30));
                PLA12D.setPreferredSize(new Dimension(110, 30));
                PLA12D.setFont(PLA12D.getFont().deriveFont(PLA12D.getFont().getSize() + 2f));
                PLA12D.setName("PLA12D");
                pnlDateDernierAchat.add(PLA12D);
              }
              pnlPrincipalDroite.add(pnlDateDernierAchat, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbUniteStock ----
              lbUniteStock.setText("Unit\u00e9 de stock (US)");
              lbUniteStock.setComponentPopupMenu(null);
              lbUniteStock.setMinimumSize(new Dimension(200, 30));
              lbUniteStock.setPreferredSize(new Dimension(200, 30));
              lbUniteStock.setFont(lbUniteStock.getFont().deriveFont(lbUniteStock.getFont().getSize() + 2f));
              lbUniteStock.setHorizontalAlignment(SwingConstants.TRAILING);
              lbUniteStock.setMaximumSize(new Dimension(200, 30));
              lbUniteStock.setName("lbUniteStock");
              pnlPrincipalDroite.add(lbUniteStock, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG26A ----
              ARG26A.setComponentPopupMenu(null);
              ARG26A.setMinimumSize(new Dimension(50, 30));
              ARG26A.setPreferredSize(new Dimension(50, 30));
              ARG26A.setFont(ARG26A.getFont().deriveFont(ARG26A.getFont().getSize() + 2f));
              ARG26A.setName("ARG26A");
              pnlPrincipalDroite.add(ARG26A, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbFicheStockMagasin ----
              lbFicheStockMagasin.setText("Magasin (fiche stock pr\u00e9sente)");
              lbFicheStockMagasin.setFont(lbFicheStockMagasin.getFont().deriveFont(lbFicheStockMagasin.getFont().getSize() + 2f));
              lbFicheStockMagasin.setHorizontalAlignment(SwingConstants.TRAILING);
              lbFicheStockMagasin.setMinimumSize(new Dimension(200, 30));
              lbFicheStockMagasin.setMaximumSize(new Dimension(200, 30));
              lbFicheStockMagasin.setPreferredSize(new Dimension(200, 30));
              lbFicheStockMagasin.setName("lbFicheStockMagasin");
              pnlPrincipalDroite.add(lbFicheStockMagasin, new GridBagConstraints(0, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG71A ----
              ARG71A.setComponentPopupMenu(null);
              ARG71A.setMinimumSize(new Dimension(50, 30));
              ARG71A.setPreferredSize(new Dimension(50, 30));
              ARG71A.setFont(ARG71A.getFont().deriveFont(ARG71A.getFont().getSize() + 2f));
              ARG71A.setToolTipText(
                  "L'article est s\u00e9lectionn\u00e9 s'il poss\u00e8de une fiche stock pour le magasin saisi, y compris si le stock est \u00e9gal \u00e0 z\u00e9ro dans cette fiche.");
              ARG71A.setName("ARG71A");
              pnlPrincipalDroite.add(ARG71A, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelTitre4 ----
              sNLabelTitre4.setText("Crit\u00e8res ventes");
              sNLabelTitre4.setName("sNLabelTitre4");
              pnlPrincipalDroite.add(sNLabelTitre4, new GridBagConstraints(0, 16, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletPrincipal.add(pnlPrincipalDroite);
          }
          sNPanel2.add(pnlOngletPrincipal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        tabbedPane1.addTab("S\u00e9lection articles", sNPanel2);
        
        // ======== sNPanelContenu2 ========
        {
          sNPanelContenu2.setName("sNPanelContenu2");
          sNPanelContenu2.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelContenu2.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) sNPanelContenu2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanelContenu2.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanelContenu2.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== scrollPane1 ========
          {
            scrollPane1.setName("scrollPane1");
            
            // ---- tblListeDocument ----
            tblListeDocument.setShowVerticalLines(true);
            tblListeDocument.setShowHorizontalLines(true);
            tblListeDocument.setBackground(Color.white);
            tblListeDocument.setRowHeight(20);
            tblListeDocument.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblListeDocument.setAutoCreateRowSorter(true);
            tblListeDocument.setSelectionBackground(new Color(57, 105, 138));
            tblListeDocument.setGridColor(new Color(204, 204, 204));
            tblListeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            tblListeDocument.setName("tblListeDocument");
            tblListeDocument.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblListeDocumentsMouseClicked(e);
              }
            });
            scrollPane1.setViewportView(tblListeDocument);
          }
          sNPanelContenu2.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        tabbedPane1.addTab("Champs \u00e0 exporter", sNPanelContenu2);
      }
      pnlPrincipal.add(tabbedPane1, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNBandeauTitre pnlBpresentation;
  private SNBarreBouton sNBarreBouton1;
  private JTabbedPane tabbedPane1;
  private SNPanel sNPanel2;
  private JPanel pnlOngletPrincipal;
  private JPanel pnlPrincipalGauche;
  private SNLabelTitre sNLabelTitre1;
  private JLabel lbRechercheGenerique;
  private XRiTextField ARG76A;
  private JLabel lbCodeArticle;
  private XRiTextField ARG2A;
  private JLabel lbMotClassement1;
  private XRiTextField ARG3A;
  private JLabel lbMotClassement2;
  private XRiTextField ARG4A;
  private JLabel lbFamille;
  private SNFamille snFamille;
  private JLabel lbSousFamille;
  private SNSousFamille snSousFamille;
  private JLabel lbEtat;
  private XRiComboBox ARG10A;
  private JLabel lbModeGestion;
  private XRiComboBox ARG32A;
  private JLabel lbTypeSaisie;
  private XRiComboBox ARG23A;
  private JLabel lbClasse;
  private XRiComboBox cbClasse;
  private JLabel lbSpecial;
  private XRiComboBox ARG24A;
  private XRiCheckBox ARG67A;
  private XRiCheckBox ARG25A;
  private XRiCheckBox ARG13A;
  private JLabel lbDateCreation;
  private JPanel pnlDateCreation;
  private XRiCalendrier ARG20D;
  private XRiCalendrier PLA20D;
  private JLabel lbArticleSubstitution;
  private XRiTextField ARG11A;
  private JLabel lbGencode;
  private XRiTextField ARG9N;
  private XRiCheckBox WTGCD;
  private JPanel pnlPrincipalDroite;
  private JLabel lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelTitre sNLabelTitre2;
  private JLabel lbFournisseur;
  private SNFournisseur snFournisseur;
  private XRiCheckBox OPT8;
  private JLabel lbClassementFournisseur;
  private XRiTextField WFRCLA;
  private JLabel OBJ_243_OBJ_243;
  private XRiTextField ARG38N;
  private JLabel OBJ_237_OBJ_237;
  private XRiTextField ARG36A;
  private JLabel OBJ_240_OBJ_240;
  private XRiTextField ARG37A;
  private SNLabelTitre sNLabelTitre3;
  private JLabel lbDisponibilite;
  private XRiComboBox DISPO;
  private JLabel lbTypeReapprovisionnement;
  private XRiComboBox ARG29A;
  private JLabel lbGestionLot;
  private XRiComboBox ARG31A;
  private JLabel lbTypeArticle2;
  private XRiComboBox SCAN73;
  private JLabel lbDateDernierAchat;
  private JPanel pnlDateDernierAchat;
  private XRiCalendrier ARG12D;
  private XRiCalendrier PLA12D;
  private JLabel lbUniteStock;
  private XRiTextField ARG26A;
  private JLabel lbFicheStockMagasin;
  private XRiTextField ARG71A;
  private SNLabelTitre sNLabelTitre4;
  private SNPanelContenu sNPanelContenu2;
  private JScrollPane scrollPane1;
  private NRiTable tblListeDocument;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
