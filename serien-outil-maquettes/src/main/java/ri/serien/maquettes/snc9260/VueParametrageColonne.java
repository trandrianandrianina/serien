/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9260;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JDialog;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author User #10
 */
public class VueParametrageColonne extends JDialog {
  
  public VueParametrageColonne() {
    super();
    initComponents();
    setSize(400, 365);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelEcranRPG();
    pnlContenu = new SNPanelContenu();
    scrollPane1 = new JScrollPane();
    table1 = new JTable();
    snBarreBouton = new SNBarreBouton();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();
    menuItem2 = new JMenuItem();
    menuItem3 = new JMenuItem();
    menuItem4 = new JMenuItem();
    menuItem5 = new JMenuItem();
    
    // ======== this ========
    setTitle("S\u00e9lection des colonnes \u00e0 afficher");
    setMinimumSize(new Dimension(400, 365));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPrincipal.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlPrincipal.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlPrincipal.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new BorderLayout());
        
        // ======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");
          
          // ---- table1 ----
          table1.setFont(table1.getFont().deriveFont(table1.getFont().getSize() + 2f));
          table1.setModel(new DefaultTableModel(
              new Object[][] { { "Num\u00e9ro client", true }, { "Civilit\u00e9", null }, { "Nom", true },
                  { "Compl\u00e9ment de nom", null }, { "Rue", null }, { "Localisation", null }, { "Code postal", true },
                  { "Ville", true }, { "Pays", null }, { "Code pays", null }, { "Num\u00e9ro de t\u00e9l\u00e9phone", true },
                  { "Cat\u00e9gorie du client", true }, { "Type de client", null }, { "Type de compte", true }, },
              new String[] { "Colonne", " " }) {
            Class<?>[] columnTypes = new Class<?>[] { String.class, Boolean.class };
            boolean[] columnEditable = new boolean[] { false, false };
            
            @Override
            public Class<?> getColumnClass(int columnIndex) {
              return columnTypes[columnIndex];
            }
            
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
              return columnEditable[columnIndex];
            }
          });
          {
            TableColumnModel cm = table1.getColumnModel();
            cm.getColumn(0).setPreferredWidth(360);
            cm.getColumn(1).setResizable(false);
          }
          table1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          table1.setName("table1");
          scrollPane1.setViewportView(table1);
        }
        pnlContenu.add(scrollPane1, BorderLayout.CENTER);
      }
      pnlPrincipal.add(pnlContenu,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    contentPane.add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");
      
      // ---- menuItem1 ----
      menuItem1.setText("S\u00e9lectionner");
      menuItem1.setFont(menuItem1.getFont().deriveFont(menuItem1.getFont().getStyle() | Font.BOLD));
      menuItem1.setName("menuItem1");
      popupMenu1.add(menuItem1);
      
      // ---- menuItem2 ----
      menuItem2.setText("D\u00e9placer vers le haut");
      menuItem2.setName("menuItem2");
      popupMenu1.add(menuItem2);
      
      // ---- menuItem3 ----
      menuItem3.setText("D\u00e9placer vers le bas");
      menuItem3.setName("menuItem3");
      popupMenu1.add(menuItem3);
      popupMenu1.addSeparator();
      
      // ---- menuItem4 ----
      menuItem4.setText("D\u00e9s\u00e9lectionner toutes les colonnes");
      menuItem4.setName("menuItem4");
      popupMenu1.add(menuItem4);
      
      // ---- menuItem5 ----
      menuItem5.setText("Remettre les colonnes par d\u00e9faut");
      menuItem5.setName("menuItem5");
      popupMenu1.add(menuItem5);
    }
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelEcranRPG pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private JScrollPane scrollPane1;
  private JTable table1;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  private JMenuItem menuItem2;
  private JMenuItem menuItem3;
  private JMenuItem menuItem4;
  private JMenuItem menuItem5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
