/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc11458;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ChangeEvent;

import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Vue de l'écran liste de la consultation de documents de ventes.
 */
public class VueSaisieLots extends JPanel {
  // Constantes
  /**
   * Constructeur.
   */
  public VueSaisieLots() {
    super();
  }
  
  // -- Méthodes protégées
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
  }
  
  private void traiterClicBoutonDocument(SNBouton pSNBouton) {
  }
  
  private void traiterClicBoutonLigne(SNBouton pSNBouton) {
  }
  
  private void keyFleches(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
  }
  
  private void scpListeLigneStateChanged(ChangeEvent e) {
  }
  
  private void scpListeDocumentsStateChanged(ChangeEvent e) {
  }
  
  private void tfNumeroDocumentFocusLost(FocusEvent e) {
  }
  
  private void cbMagasinItemStateChanged(ItemEvent e) {
  }
  
  private void tfReferenceClientDocumentFocusLost(FocusEvent e) {
  }
  
  private void tblListeDocumentsMouseClicked(MouseEvent e) {
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
  }
  
  private void cbTypeDocumentItemStateChanged(ItemEvent e) {
  }
  
  private void rbRechercheDocumentItemStateChanged(ItemEvent e) {
  }
  
  private void rbRechercheLigneItemStateChanged(ItemEvent e) {
  }
  
  private void tfNumeroLigneFocusLost(FocusEvent e) {
  }
  
  private void tfReferenceClientLigneFocusLost(FocusEvent e) {
  }
  
  private void snEtablissementLigneValueChanged(SNComposantEvent e) {
  }
  
  private void cbMagasinLigneItemStateChanged(ItemEvent e) {
  }
  
  private void snPlageDateLigneValueChanged(SNComposantEvent e) {
  }
  
  private void snArticleStandardLigneValueChanged(SNComposantEvent e) {
  }
  
  private void snArticleStandardDocumentValueChanged(SNComposantEvent e) {
  }
  
  private void snFournisseurDocumentValueChanged(SNComposantEvent e) {
  }
  
  private void snFournisseurLigneValueChanged(SNComposantEvent e) {
  }
  
  private void cbTypeDocumentLigneItemStateChanged(ItemEvent e) {
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    this2 = new JDialog();
    sNPanelFond1 = new SNPanelFond();
    sNBarreBouton1 = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    sNLabelTitre1 = new SNLabelTitre();
    scrollPane1 = new JScrollPane();
    tblListeDocument = new NRiTable();
    
    // ======== this2 ========
    {
      this2.setTitle("Saisie de lots pour retour de l'article Colle universelle (COLU78569X)");
      this2.setBackground(new Color(238, 238, 210));
      this2.setResizable(false);
      this2.setModal(true);
      this2.setName("this2");
      Container this2ContentPane = this2.getContentPane();
      this2ContentPane.setLayout(null);
      
      // ======== sNPanelFond1 ========
      {
        sNPanelFond1.setName("sNPanelFond1");
        sNPanelFond1.setLayout(new BorderLayout());
        
        // ---- sNBarreBouton1 ----
        sNBarreBouton1.setName("sNBarreBouton1");
        sNPanelFond1.add(sNBarreBouton1, BorderLayout.SOUTH);
        
        // ======== sNPanelContenu1 ========
        {
          sNPanelContenu1.setName("sNPanelContenu1");
          sNPanelContenu1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
          
          // ---- sNLabelTitre1 ----
          sNLabelTitre1.setText("Lots ");
          sNLabelTitre1.setName("sNLabelTitre1");
          sNPanelContenu1.add(sNLabelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== scrollPane1 ========
          {
            scrollPane1.setName("scrollPane1");
            
            // ---- tblListeDocument ----
            tblListeDocument.setShowVerticalLines(true);
            tblListeDocument.setShowHorizontalLines(true);
            tblListeDocument.setBackground(Color.white);
            tblListeDocument.setRowHeight(20);
            tblListeDocument.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblListeDocument.setAutoCreateRowSorter(true);
            tblListeDocument.setSelectionBackground(new Color(57, 105, 138));
            tblListeDocument.setGridColor(new Color(204, 204, 204));
            tblListeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
            tblListeDocument.setName("tblListeDocument");
            tblListeDocument.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblListeDocumentsMouseClicked(e);
              }
            });
            scrollPane1.setViewportView(tblListeDocument);
          }
          sNPanelContenu1.add(scrollPane1, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        }
        sNPanelFond1.add(sNPanelContenu1, BorderLayout.CENTER);
      }
      this2ContentPane.add(sNPanelFond1);
      sNPanelFond1.setBounds(0, 0, 1135, 490);
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < this2ContentPane.getComponentCount(); i++) {
          Rectangle bounds = this2ContentPane.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = this2ContentPane.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        this2ContentPane.setMinimumSize(preferredSize);
        this2ContentPane.setPreferredSize(preferredSize);
      }
    }
    
    this2.pack();
    this2.setLocationRelativeTo(this2.getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JDialog this2;
  private SNPanelFond sNPanelFond1;
  private SNBarreBouton sNBarreBouton1;
  private SNPanelContenu sNPanelContenu1;
  private SNLabelTitre sNLabelTitre1;
  private JScrollPane scrollPane1;
  private NRiTable tblListeDocument;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
