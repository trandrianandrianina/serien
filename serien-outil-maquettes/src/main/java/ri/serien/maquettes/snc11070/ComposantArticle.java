/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc11070;

public class ComposantArticle {
  private String nom;
  private int quantiteBesoin;
  private int stock;
  private ListComposantArticle listNoeud = new ListComposantArticle();
  
  public String getNom() {
    return nom;
  }
  
  public void setNom(String pNom) {
    this.nom = pNom;
  }
  
  public int getQuantiteBesoin() {
    return quantiteBesoin;
  }
  
  public void setQuantiteBesoin(int quantite) {
    this.quantiteBesoin = quantite;
  }
  
  public ListComposantArticle getListNoeud() {
    return listNoeud;
  }
  
  public void setListNoeud(ListComposantArticle listNoeud) {
    this.listNoeud = listNoeud;
  }
  
  public void ajouterComposant(ComposantArticle pNoeud) {
    this.listNoeud.add(pNoeud);
  }
  
  public boolean hasEnfant() {
    return !this.listNoeud.isEmpty();
  }
  
  public int getStock() {
    return stock;
  }
  
  public void setStock(int stock) {
    this.stock = stock;
  }
  
  public String getLibelle() {
    return this.nom + " [" + this.quantiteBesoin + "]";
  }
}
