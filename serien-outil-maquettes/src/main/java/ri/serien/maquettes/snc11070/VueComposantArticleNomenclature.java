/*
 * Created by JFormDesigner on Mon Oct 24 14:43:00 MSK 2022
 */

package ri.serien.maquettes.snc11070;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;

import ri.serien.desktop.metier.exploitation.menus.FeuilleMutableTreeNode;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * @author User #9
 */
public class VueComposantArticleNomenclature extends AbstractVueDialogue<ModeleComposantArticle> {
  private static final long serialVersionUID = 1L;
  private ResourceBundle oldbundle = null;
  private boolean loadFileI18n = true, executeI18n = false;
  private static final String[] TITRE_COLONNE_TABLEAU = new String[] { "Article", "Stock" };
  
  public VueComposantArticleNomenclature(ModeleComposantArticle pModeleNoeud) {
    super(pModeleNoeud);
    initComponents();
    setTitle("Arborescence nomenclature");
    
    // Ajout
    
    // Bouton par défaut
  }
  
  public void setData() {
    executeI18n = true;
  }
  
  /**
   * Convertie le noeud en composant de l'arborescence.
   * 
   * @param pNoeud Noeud à convertir
   * @return
   */
  private FeuilleMutableTreeNode traiterNoeud(ComposantArticle pNoeud) {
    FeuilleMutableTreeNode noeudParent = null;
    if (pNoeud != null) {
      noeudParent = new FeuilleMutableTreeNode(pNoeud.getLibelle());
      if (pNoeud.hasEnfant()) {
        for (ComposantArticle noeud : pNoeud.getListNoeud()) {
          FeuilleMutableTreeNode feuille = new FeuilleMutableTreeNode(noeud.getLibelle());
          if (noeud.hasEnfant()) {
            feuille = traiterNoeud(noeud);
          }
          noeudParent.add(feuille);
        }
      }
    }
    
    return noeudParent;
  }
  
  /**
   * Rafraichie la liste dans le tableau
   *
   */
  private void rafraichirTableau() {
    Object[][] donnees = null;
    
    LinkedHashMap<String, Integer> mapNoeud = new LinkedHashMap<String, Integer>();
    // Convertir Objet Noeud en liste
    ComposantArticle noeud = getModele().retournerNoeud();
    transformerNoeudEnMap(noeud, mapNoeud);
    
    donnees = new Object[mapNoeud.size() - 1][TITRE_COLONNE_TABLEAU.length];
    
    int index = 0;
    for (Entry<String, Integer> entry : mapNoeud.entrySet()) {
      // Ne pas inclure le composant racine
      if (index == 0) {
        index++;
        continue;
      }
      // Colonne article
      donnees[index - 1][0] = entry.getKey();
      // Colonne stock
      donnees[index - 1][1] = entry.getValue();
      index++;
    }
    
    tblArticle.mettreAJourDonnees(donnees);
  }
  
  /**
   * Transforme le noeud en Map.
   * 
   * @param pNoeud Noeud à transformer
   * @param pMap Map à alimenter
   */
  private void transformerNoeudEnMap(ComposantArticle pNoeud, LinkedHashMap<String, Integer> pMap) {
    if (pNoeud != null) {
      if (!pMap.containsKey(pNoeud.getNom())) {
        pMap.put(pNoeud.getNom(), pNoeud.getStock());
      }
      if (pNoeud.hasEnfant()) {
        for (ComposantArticle noeudEnfant : pNoeud.getListNoeud()) {
          transformerNoeudEnMap(noeudEnfant, pMap);
        }
      }
    }
  }
  
  @Override
  public void initialiserComposants() {
    ComposantArticle noeud = getModele().retournerNoeud();
    FeuilleMutableTreeNode racine = new FeuilleMutableTreeNode();
    if (noeud != null) {
      racine = traiterNoeud(noeud);
    }
    
    treeArticleNomenclature = new JTree(racine);
    scrollPane1.setViewportView(treeArticleNomenclature);
  }
  
  @Override
  public void rafraichir() {
    rafraichirTableau();
  }
  
  public void getData() {
    // TODO Récup spécifiques
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    scrollPane1 = new JScrollPane();
    treeArticleNomenclature = new JTree();
    scrollPane2 = new JScrollPane();
    tblArticle = new NRiTable();
    
    // ======== this ========
    setMinimumSize(new Dimension(400, 300));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new GridLayout());
    
    // ======== scrollPane1 ========
    {
      scrollPane1.setName("scrollPane1");
      
      // ---- treeArticleNomenclature ----
      treeArticleNomenclature.setName("treeArticleNomenclature");
      scrollPane1.setViewportView(treeArticleNomenclature);
    }
    contentPane.add(scrollPane1);
    
    // ======== scrollPane2 ========
    {
      scrollPane2.setName("scrollPane2");
      
      // ---- tblArticle ----
      tblArticle.setModel(new DefaultTableModel(new Object[][] { { null, null }, }, new String[] { "Article", "Stock" }));
      tblArticle.setName("tblArticle");
      scrollPane2.setViewportView(tblArticle);
    }
    contentPane.add(scrollPane2);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane scrollPane1;
  private JTree treeArticleNomenclature;
  private JScrollPane scrollPane2;
  private NRiTable tblArticle;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
