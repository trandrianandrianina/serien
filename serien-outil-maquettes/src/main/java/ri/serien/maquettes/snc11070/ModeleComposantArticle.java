/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc11070;

import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

public class ModeleComposantArticle extends AbstractModelePanel {
  private ComposantArticle noeud = new ComposantArticle();
  
  public ModeleComposantArticle(SessionBase pSession) {
    super(pSession);
    // XXX Auto-generated constructor stub
  }
  
  @Override
  public void initialiserDonnees() {
    // Noeud Racine
    noeud.setNom("Article nomenclature A");
    noeud.setQuantiteBesoin(1);
    
    // Noeud composant C
    ComposantArticle noeudComposantC = new ComposantArticle();
    noeudComposantC.setNom("Composant C");
    noeudComposantC.setQuantiteBesoin(2);
    noeudComposantC.setStock(100);
    noeud.ajouterComposant(noeudComposantC);
    
    // Noeud composant D
    ComposantArticle noeudComposantD = new ComposantArticle();
    noeudComposantD.setNom("Composant D");
    noeudComposantD.setQuantiteBesoin(3);
    noeudComposantD.setStock(100);
    noeud.ajouterComposant(noeudComposantD);
    
    // Noeud composant E
    ComposantArticle noeudComposantE = new ComposantArticle();
    noeudComposantE.setNom("Composant E");
    noeudComposantE.setQuantiteBesoin(4);
    noeudComposantE.setStock(500);
    noeud.ajouterComposant(noeudComposantE);
    
    // Noeud composant B1
    ComposantArticle noeudComposantB1 = new ComposantArticle();
    noeudComposantB1.setNom("Composant B1");
    noeudComposantB1.setQuantiteBesoin(5);
    noeudComposantB1.setStock(42);
    noeud.ajouterComposant(noeudComposantB1);
    
    // Noeud composant B2
    ComposantArticle noeudComposantB2 = new ComposantArticle();
    noeudComposantB2.setNom("Composant B2");
    noeudComposantB2.setQuantiteBesoin(6);
    noeudComposantB2.setStock(15);
    noeud.ajouterComposant(noeudComposantB2);
    
    // Noeud composant B3
    ComposantArticle noeudComposantB3 = new ComposantArticle();
    noeudComposantB3.setNom("Composant B3");
    noeudComposantB3.setQuantiteBesoin(4);
    noeudComposantB3.setStock(100);
    noeudComposantB1.ajouterComposant(noeudComposantB3);
    
    // Noeud composant F
    ComposantArticle noeudComposantF = new ComposantArticle();
    noeudComposantF.setNom("Composant F");
    noeudComposantF.setQuantiteBesoin(2);
    noeudComposantF.setStock(100);
    noeudComposantB1.ajouterComposant(noeudComposantF);
    
    // Noeud composant EB1
    ComposantArticle noeudComposantEB1 = new ComposantArticle();
    noeudComposantEB1.setNom("Composant E");
    noeudComposantEB1.setQuantiteBesoin(3);
    noeudComposantEB1.setStock(500);
    noeudComposantB1.ajouterComposant(noeudComposantEB1);
    
    // Noeud composant EB2
    ComposantArticle noeudComposantEB2 = new ComposantArticle();
    noeudComposantEB2.setNom("Composant E");
    noeudComposantEB2.setQuantiteBesoin(3);
    noeudComposantEB2.setStock(500);
    noeudComposantB2.ajouterComposant(noeudComposantEB2);
    
    // Noeud composant EB3
    ComposantArticle noeudComposantEB3 = new ComposantArticle();
    noeudComposantEB3.setNom("Composant E");
    noeudComposantEB3.setQuantiteBesoin(2);
    noeudComposantEB3.setStock(500);
    noeudComposantB3.ajouterComposant(noeudComposantEB3);
    
  }
  
  @Override
  public void chargerDonnees() {
    // XXX Auto-generated method stub
    
  }
  
  public ComposantArticle retournerNoeud() {
    return noeud;
  }
  
  // public
  
}
