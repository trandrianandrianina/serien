/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.snc9149;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;

import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snadressefournisseur.SNAdresseFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNIdentifiant;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * Vue de l'écran liste de la consultation de documents de ventes.
 */
public class VueListeConsultationDocumentAchat extends JPanel {
  // Constantes
  /**
   * Constructeur.
   */
  public VueListeConsultationDocumentAchat() {
    super();
  }
  
  // -- Méthodes protégées
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
  }
  
  private void traiterClicBoutonDocument(SNBouton pSNBouton) {
  }
  
  private void traiterClicBoutonLigne(SNBouton pSNBouton) {
  }
  
  private void keyFleches(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
  }
  
  private void scpListeLigneStateChanged(ChangeEvent e) {
  }
  
  private void scpListeDocumentsStateChanged(ChangeEvent e) {
  }
  
  private void tfNumeroDocumentFocusLost(FocusEvent e) {
  }
  
  private void cbMagasinItemStateChanged(ItemEvent e) {
  }
  
  private void tfReferenceClientDocumentFocusLost(FocusEvent e) {
  }
  
  private void tblListeDocumentsMouseClicked(MouseEvent e) {
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
  }
  
  private void cbTypeDocumentItemStateChanged(ItemEvent e) {
  }
  
  private void rbRechercheDocumentItemStateChanged(ItemEvent e) {
  }
  
  private void rbRechercheLigneItemStateChanged(ItemEvent e) {
  }
  
  private void tfNumeroLigneFocusLost(FocusEvent e) {
  }
  
  private void tfReferenceClientLigneFocusLost(FocusEvent e) {
  }
  
  private void snEtablissementLigneValueChanged(SNComposantEvent e) {
  }
  
  private void cbMagasinLigneItemStateChanged(ItemEvent e) {
  }
  
  private void snPlageDateLigneValueChanged(SNComposantEvent e) {
  }
  
  private void snArticleStandardLigneValueChanged(SNComposantEvent e) {
  }
  
  private void snArticleStandardDocumentValueChanged(SNComposantEvent e) {
  }
  
  private void snFournisseurDocumentValueChanged(SNComposantEvent e) {
  }
  
  private void snFournisseurLigneValueChanged(SNComposantEvent e) {
  }
  
  private void cbTypeDocumentLigneItemStateChanged(ItemEvent e) {
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlChoixTypeRecherche = new SNPanelTitre();
    rbRechercheDocument = new SNRadioButton();
    rbRechercheLigne = new SNRadioButton();
    pnlContenu = new JPanel();
    pnlDocument = new JPanel();
    pnlFiltre = new JPanel();
    pnlFiltre1 = new JPanel();
    lbNumeroDocument = new SNLabelChamp();
    tfNumeroDocument = new SNIdentifiant();
    lbReferenceClientDocument = new SNLabelChamp();
    tfReferenceClientDocument = new SNTexte();
    lbTypeDocument = new SNLabelChamp();
    cbTypeDocument = new SNComboBox();
    lbFournisseur = new SNLabelChamp();
    snAdresseFournisseurDocument = new SNAdresseFournisseur();
    lbVille = new SNLabelChamp();
    tfCodePostalDocument = new SNTexte();
    tfVilleDocument = new SNTexte();
    lbArticle = new SNLabelChamp();
    snArticleStandardDocument = new SNArticle();
    pnlFiltre2 = new JPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissementDocument = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    cbMagasinDocument = new SNComboBox();
    lbMagasin3 = new SNLabelChamp();
    cbAcheteur = new XRiComboBox();
    ldDateCreation = new SNLabelChamp();
    snPlageDateDocument = new SNPlageDate();
    snBarreRechercheDocument = new SNBarreRecherche();
    lblTitreResultatDocument = new SNLabelTitre();
    scpListeDocument = new JScrollPane();
    tblListeDocument = new NRiTable();
    pnlLigne = new JPanel();
    pnlFiltre3 = new JPanel();
    pnlFiltre4 = new JPanel();
    lbNumeroDocument2 = new SNLabelChamp();
    tfNumeroLigne = new SNIdentifiant();
    lbReferenceClientDocument2 = new SNLabelChamp();
    tfReferenceClientLigne = new SNTexte();
    lbTypeDocument2 = new SNLabelChamp();
    cbTypeDocumentLigne = new SNComboBox();
    lbFournisseur2 = new SNLabelChamp();
    snAdresseFournisseurLigne = new SNAdresseFournisseur();
    lbVille2 = new SNLabelChamp();
    tfCodePostalLigne = new SNTexte();
    tfVilleLigne = new SNTexte();
    lbArticle2 = new SNLabelChamp();
    snArticleStandardLigne = new SNArticle();
    pnlFiltre5 = new JPanel();
    lbEtablissement2 = new SNLabelChamp();
    snEtablissementLigne = new SNEtablissement();
    lbMagasin2 = new SNLabelChamp();
    cbMagasinLigne = new SNComboBox();
    ldDateCreation2 = new SNLabelChamp();
    snPlageDateLigne = new SNPlageDate();
    snBarreRechercheLigne = new SNBarreRecherche();
    lblTitreResultatLigne = new SNLabelTitre();
    scpListeLigne = new JScrollPane();
    tblListeLigne = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setPreferredSize(new Dimension(1205, 655));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlChoixTypeRecherche ========
    {
      pnlChoixTypeRecherche.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 0, 10),
          new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14))));
      pnlChoixTypeRecherche.setName("pnlChoixTypeRecherche");
      pnlChoixTypeRecherche.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      
      // ---- rbRechercheDocument ----
      rbRechercheDocument.setText("Recherche de documents d'achat");
      rbRechercheDocument.setPreferredSize(new Dimension(350, 30));
      rbRechercheDocument.setMinimumSize(new Dimension(350, 30));
      rbRechercheDocument.setName("rbRechercheDocument");
      rbRechercheDocument.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          rbRechercheDocumentItemStateChanged(e);
        }
      });
      pnlChoixTypeRecherche.add(rbRechercheDocument,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- rbRechercheLigne ----
      rbRechercheLigne.setText("Recherche de lignes d'achat");
      rbRechercheLigne.setPreferredSize(new Dimension(350, 30));
      rbRechercheLigne.setMinimumSize(new Dimension(350, 30));
      rbRechercheLigne.setName("rbRechercheLigne");
      rbRechercheLigne.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          rbRechercheLigneItemStateChanged(e);
        }
      });
      pnlChoixTypeRecherche.add(rbRechercheLigne,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlChoixTypeRecherche, BorderLayout.PAGE_START);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(238, 238, 210));
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setOpaque(false);
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new CardLayout());
      
      // ======== pnlDocument ========
      {
        pnlDocument.setOpaque(false);
        pnlDocument.setName("pnlDocument");
        pnlDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDocument.getLayout()).columnWidths = new int[] { 1205, 0 };
        ((GridBagLayout) pnlDocument.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDocument.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlFiltre ========
        {
          pnlFiltre.setOpaque(false);
          pnlFiltre.setBorder(new TitledBorder(""));
          pnlFiltre.setName("pnlFiltre");
          pnlFiltre.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlFiltre1 ========
          {
            pnlFiltre1.setOpaque(false);
            pnlFiltre1.setName("pnlFiltre1");
            pnlFiltre1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWidths = new int[] { 0, 0, 188, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDocument ----
            lbNumeroDocument.setText("Num\u00e9ro document");
            lbNumeroDocument.setName("lbNumeroDocument");
            pnlFiltre1.add(lbNumeroDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfNumeroDocument ----
            tfNumeroDocument.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
            tfNumeroDocument.setName("tfNumeroDocument");
            tfNumeroDocument.addFocusListener(new FocusAdapter() {
              
              @Override
              public void focusLost(FocusEvent e) {
                tfNumeroDocumentFocusLost(e);
              }
            });
            pnlFiltre1.add(tfNumeroDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbReferenceClientDocument ----
            lbReferenceClientDocument.setText("R\u00e9f\u00e9rence document");
            lbReferenceClientDocument.setName("lbReferenceClientDocument");
            pnlFiltre1.add(lbReferenceClientDocument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfReferenceClientDocument ----
            tfReferenceClientDocument.setName("tfReferenceClientDocument");
            tfReferenceClientDocument.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfReferenceClientDocumentFocusLost(e);
              }
            });
            pnlFiltre1.add(tfReferenceClientDocument, new GridBagConstraints(1, 1, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbTypeDocument ----
            lbTypeDocument.setText("Type document");
            lbTypeDocument.setName("lbTypeDocument");
            pnlFiltre1.add(lbTypeDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbTypeDocument ----
            cbTypeDocument.setMinimumSize(new Dimension(150, 30));
            cbTypeDocument.setPreferredSize(new Dimension(150, 30));
            cbTypeDocument.setName("cbTypeDocument");
            cbTypeDocument.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeDocumentItemStateChanged(e);
              }
            });
            pnlFiltre1.add(cbTypeDocument, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbFournisseur ----
            lbFournisseur.setText("Fournisseur");
            lbFournisseur.setName("lbFournisseur");
            pnlFiltre1.add(lbFournisseur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snAdresseFournisseurDocument ----
            snAdresseFournisseurDocument.setName("snAdresseFournisseurDocument");
            pnlFiltre1.add(snAdresseFournisseurDocument, new GridBagConstraints(1, 3, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVille ----
            lbVille.setText("Ville");
            lbVille.setName("lbVille");
            pnlFiltre1.add(lbVille, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfCodePostalDocument ----
            tfCodePostalDocument.setEnabled(false);
            tfCodePostalDocument.setName("tfCodePostalDocument");
            pnlFiltre1.add(tfCodePostalDocument, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfVilleDocument ----
            tfVilleDocument.setEnabled(false);
            tfVilleDocument.setName("tfVilleDocument");
            pnlFiltre1.add(tfVilleDocument, new GridBagConstraints(2, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbArticle ----
            lbArticle.setText("Article");
            lbArticle.setName("lbArticle");
            pnlFiltre1.add(lbArticle, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snArticleStandardDocument ----
            snArticleStandardDocument.setName("snArticleStandardDocument");
            snArticleStandardDocument.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snArticleStandardDocumentValueChanged(e);
              }
            });
            pnlFiltre1.add(snArticleStandardDocument, new GridBagConstraints(1, 5, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre.add(pnlFiltre1);
          
          // ======== pnlFiltre2 ========
          {
            pnlFiltre2.setOpaque(false);
            pnlFiltre2.setName("pnlFiltre2");
            pnlFiltre2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement");
            lbEtablissement.setName("lbEtablissement");
            pnlFiltre2.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissementDocument ----
            snEtablissementDocument.setName("snEtablissementDocument");
            snEtablissementDocument.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlFiltre2.add(snEtablissementDocument, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlFiltre2.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbMagasinDocument ----
            cbMagasinDocument.setName("cbMagasinDocument");
            cbMagasinDocument.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbMagasinItemStateChanged(e);
              }
            });
            pnlFiltre2.add(cbMagasinDocument, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin3 ----
            lbMagasin3.setText("Acheteur");
            lbMagasin3.setName("lbMagasin3");
            pnlFiltre2.add(lbMagasin3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbAcheteur ----
            cbAcheteur.setBackground(new Color(171, 148, 79));
            cbAcheteur.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbAcheteur.setMinimumSize(new Dimension(315, 30));
            cbAcheteur.setPreferredSize(new Dimension(315, 30));
            cbAcheteur.setModel(new DefaultComboBoxModel(new String[] { "Acheteur 1" }));
            cbAcheteur.setName("cbAcheteur");
            pnlFiltre2.add(cbAcheteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ldDateCreation ----
            ldDateCreation.setText("Date de cr\u00e9ation");
            ldDateCreation.setName("ldDateCreation");
            pnlFiltre2.add(ldDateCreation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPlageDateDocument ----
            snPlageDateDocument.setName("snPlageDateDocument");
            pnlFiltre2.add(snPlageDateDocument, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snBarreRechercheDocument ----
            snBarreRechercheDocument.setName("snBarreRechercheDocument");
            pnlFiltre2.add(snBarreRechercheDocument, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre.add(pnlFiltre2);
        }
        pnlDocument.add(pnlFiltre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lblTitreResultatDocument ----
        lblTitreResultatDocument.setText("Documents correspondant \u00e0 votre recherche");
        lblTitreResultatDocument.setName("lblTitreResultatDocument");
        pnlDocument.add(lblTitreResultatDocument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeDocument ========
        {
          scpListeDocument.setPreferredSize(new Dimension(1050, 424));
          scpListeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          scpListeDocument.setName("scpListeDocument");
          
          // ---- tblListeDocument ----
          tblListeDocument.setShowVerticalLines(true);
          tblListeDocument.setShowHorizontalLines(true);
          tblListeDocument.setBackground(Color.white);
          tblListeDocument.setRowHeight(20);
          tblListeDocument.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeDocument.setAutoCreateRowSorter(true);
          tblListeDocument.setSelectionBackground(new Color(57, 105, 138));
          tblListeDocument.setGridColor(new Color(204, 204, 204));
          tblListeDocument.setName("tblListeDocument");
          tblListeDocument.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeDocumentsMouseClicked(e);
            }
          });
          scpListeDocument.setViewportView(tblListeDocument);
        }
        pnlDocument.add(scpListeDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDocument, "card1");
      
      // ======== pnlLigne ========
      {
        pnlLigne.setOpaque(false);
        pnlLigne.setName("pnlLigne");
        pnlLigne.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlLigne.getLayout()).columnWidths = new int[] { 1205, 0 };
        ((GridBagLayout) pnlLigne.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlLigne.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlLigne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlFiltre3 ========
        {
          pnlFiltre3.setOpaque(false);
          pnlFiltre3.setBorder(new TitledBorder(""));
          pnlFiltre3.setName("pnlFiltre3");
          pnlFiltre3.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlFiltre4 ========
          {
            pnlFiltre4.setOpaque(false);
            pnlFiltre4.setName("pnlFiltre4");
            pnlFiltre4.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre4.getLayout()).columnWidths = new int[] { 0, 0, 188, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre4.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDocument2 ----
            lbNumeroDocument2.setText("Num\u00e9ro document");
            lbNumeroDocument2.setName("lbNumeroDocument2");
            pnlFiltre4.add(lbNumeroDocument2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfNumeroLigne ----
            tfNumeroLigne.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
            tfNumeroLigne.setName("tfNumeroLigne");
            tfNumeroLigne.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfNumeroLigneFocusLost(e);
              }
            });
            pnlFiltre4.add(tfNumeroLigne, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbReferenceClientDocument2 ----
            lbReferenceClientDocument2.setText("R\u00e9f\u00e9rence document");
            lbReferenceClientDocument2.setName("lbReferenceClientDocument2");
            pnlFiltre4.add(lbReferenceClientDocument2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfReferenceClientLigne ----
            tfReferenceClientLigne.setName("tfReferenceClientLigne");
            tfReferenceClientLigne.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfReferenceClientLigneFocusLost(e);
              }
            });
            pnlFiltre4.add(tfReferenceClientLigne, new GridBagConstraints(1, 1, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbTypeDocument2 ----
            lbTypeDocument2.setText("Type document");
            lbTypeDocument2.setName("lbTypeDocument2");
            pnlFiltre4.add(lbTypeDocument2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbTypeDocumentLigne ----
            cbTypeDocumentLigne.setMinimumSize(new Dimension(150, 30));
            cbTypeDocumentLigne.setPreferredSize(new Dimension(150, 30));
            cbTypeDocumentLigne.setName("cbTypeDocumentLigne");
            cbTypeDocumentLigne.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeDocumentLigneItemStateChanged(e);
              }
            });
            pnlFiltre4.add(cbTypeDocumentLigne, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbFournisseur2 ----
            lbFournisseur2.setText("Fournisseur");
            lbFournisseur2.setName("lbFournisseur2");
            pnlFiltre4.add(lbFournisseur2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snAdresseFournisseurLigne ----
            snAdresseFournisseurLigne.setName("snAdresseFournisseurLigne");
            pnlFiltre4.add(snAdresseFournisseurLigne, new GridBagConstraints(1, 3, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVille2 ----
            lbVille2.setText("Ville");
            lbVille2.setName("lbVille2");
            pnlFiltre4.add(lbVille2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfCodePostalLigne ----
            tfCodePostalLigne.setEnabled(false);
            tfCodePostalLigne.setName("tfCodePostalLigne");
            pnlFiltre4.add(tfCodePostalLigne, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfVilleLigne ----
            tfVilleLigne.setEnabled(false);
            tfVilleLigne.setName("tfVilleLigne");
            pnlFiltre4.add(tfVilleLigne, new GridBagConstraints(2, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbArticle2 ----
            lbArticle2.setText("Article");
            lbArticle2.setName("lbArticle2");
            pnlFiltre4.add(lbArticle2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snArticleStandardLigne ----
            snArticleStandardLigne.setName("snArticleStandardLigne");
            snArticleStandardLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snArticleStandardLigneValueChanged(e);
              }
            });
            pnlFiltre4.add(snArticleStandardLigne, new GridBagConstraints(1, 5, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre3.add(pnlFiltre4);
          
          // ======== pnlFiltre5 ========
          {
            pnlFiltre5.setOpaque(false);
            pnlFiltre5.setName("pnlFiltre5");
            pnlFiltre5.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement2 ----
            lbEtablissement2.setText("Etablissement");
            lbEtablissement2.setName("lbEtablissement2");
            pnlFiltre5.add(lbEtablissement2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissementLigne ----
            snEtablissementLigne.setName("snEtablissementLigne");
            snEtablissementLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementLigneValueChanged(e);
              }
            });
            pnlFiltre5.add(snEtablissementLigne, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin");
            lbMagasin2.setName("lbMagasin2");
            pnlFiltre5.add(lbMagasin2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbMagasinLigne ----
            cbMagasinLigne.setName("cbMagasinLigne");
            cbMagasinLigne.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbMagasinLigneItemStateChanged(e);
              }
            });
            pnlFiltre5.add(cbMagasinLigne, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ldDateCreation2 ----
            ldDateCreation2.setText("Date de cr\u00e9ation");
            ldDateCreation2.setName("ldDateCreation2");
            pnlFiltre5.add(ldDateCreation2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPlageDateLigne ----
            snPlageDateLigne.setName("snPlageDateLigne");
            snPlageDateLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPlageDateLigneValueChanged(e);
              }
            });
            pnlFiltre5.add(snPlageDateLigne, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snBarreRechercheLigne ----
            snBarreRechercheLigne.setName("snBarreRechercheLigne");
            pnlFiltre5.add(snBarreRechercheLigne, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre3.add(pnlFiltre5);
        }
        pnlLigne.add(pnlFiltre3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lblTitreResultatLigne ----
        lblTitreResultatLigne.setText("Documents correspondant \u00e0 votre recherche");
        lblTitreResultatLigne.setName("lblTitreResultatLigne");
        pnlLigne.add(lblTitreResultatLigne, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeLigne ========
        {
          scpListeLigne.setPreferredSize(new Dimension(1050, 424));
          scpListeLigne.setFont(new Font("sansserif", Font.PLAIN, 14));
          scpListeLigne.setName("scpListeLigne");
          
          // ---- tblListeLigne ----
          tblListeLigne.setShowVerticalLines(true);
          tblListeLigne.setShowHorizontalLines(true);
          tblListeLigne.setBackground(Color.white);
          tblListeLigne.setRowHeight(20);
          tblListeLigne.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeLigne.setAutoCreateRowSorter(true);
          tblListeLigne.setSelectionBackground(new Color(57, 105, 138));
          tblListeLigne.setGridColor(new Color(204, 204, 204));
          tblListeLigne.setName("tblListeLigne");
          tblListeLigne.addMouseListener(new MouseAdapter() {
            
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeDocumentsMouseClicked(e);
            }
          });
          scpListeLigne.setViewportView(tblListeLigne);
        }
        pnlLigne.add(scpListeLigne, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlLigne, "card2");
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.PAGE_END);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbRechercheDocument);
    buttonGroup1.add(rbRechercheLigne);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelTitre pnlChoixTypeRecherche;
  private SNRadioButton rbRechercheDocument;
  private SNRadioButton rbRechercheLigne;
  private JPanel pnlContenu;
  private JPanel pnlDocument;
  private JPanel pnlFiltre;
  private JPanel pnlFiltre1;
  private SNLabelChamp lbNumeroDocument;
  private SNIdentifiant tfNumeroDocument;
  private SNLabelChamp lbReferenceClientDocument;
  private SNTexte tfReferenceClientDocument;
  private SNLabelChamp lbTypeDocument;
  private SNComboBox cbTypeDocument;
  private SNLabelChamp lbFournisseur;
  private SNAdresseFournisseur snAdresseFournisseurDocument;
  private SNLabelChamp lbVille;
  private SNTexte tfCodePostalDocument;
  private SNTexte tfVilleDocument;
  private SNLabelChamp lbArticle;
  private SNArticle snArticleStandardDocument;
  private JPanel pnlFiltre2;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissementDocument;
  private SNLabelChamp lbMagasin;
  private SNComboBox cbMagasinDocument;
  private SNLabelChamp lbMagasin3;
  private XRiComboBox cbAcheteur;
  private SNLabelChamp ldDateCreation;
  private SNPlageDate snPlageDateDocument;
  private SNBarreRecherche snBarreRechercheDocument;
  private SNLabelTitre lblTitreResultatDocument;
  private JScrollPane scpListeDocument;
  private NRiTable tblListeDocument;
  private JPanel pnlLigne;
  private JPanel pnlFiltre3;
  private JPanel pnlFiltre4;
  private SNLabelChamp lbNumeroDocument2;
  private SNIdentifiant tfNumeroLigne;
  private SNLabelChamp lbReferenceClientDocument2;
  private SNTexte tfReferenceClientLigne;
  private SNLabelChamp lbTypeDocument2;
  private SNComboBox cbTypeDocumentLigne;
  private SNLabelChamp lbFournisseur2;
  private SNAdresseFournisseur snAdresseFournisseurLigne;
  private SNLabelChamp lbVille2;
  private SNTexte tfCodePostalLigne;
  private SNTexte tfVilleLigne;
  private SNLabelChamp lbArticle2;
  private SNArticle snArticleStandardLigne;
  private JPanel pnlFiltre5;
  private SNLabelChamp lbEtablissement2;
  private SNEtablissement snEtablissementLigne;
  private SNLabelChamp lbMagasin2;
  private SNComboBox cbMagasinLigne;
  private SNLabelChamp ldDateCreation2;
  private SNPlageDate snPlageDateLigne;
  private SNBarreRecherche snBarreRechercheLigne;
  private SNLabelTitre lblTitreResultatLigne;
  private JScrollPane scpListeLigne;
  private NRiTable tblListeLigne;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
