/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.maquettes.trello;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * COM-1054
 */
public class SaisieCommande extends JPanel {
  public SaisieCommande() {
    super();
    initComponents();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpBandeau = new SNBandeauTitre();
    pnlPrincipal = new SNPanelEcranRPG();
    sNPanelContenu1 = new SNPanelContenu();
    sNPanel1 = new SNPanel();
    sNPanel2 = new SNPanel();
    scrollPane1 = new JScrollPane();
    tree1 = new JTree();
    sNPanel3 = new SNPanel();
    sNLabelTitre2 = new SNLabelTitre();
    sNPanel5 = new SNPanel();
    sNLabelTitre5 = new SNLabelTitre();
    sNComboBox5 = new SNComboBox();
    sNLabelChamp13 = new SNLabelChamp();
    sNComboBox6 = new SNComboBox();
    sNLabelChamp14 = new SNLabelChamp();
    sNComboBox7 = new SNComboBox();
    sNLabelTitre3 = new SNLabelTitre();
    sNLabelChamp3 = new SNLabelChamp();
    sNComboBox1 = new SNComboBox();
    sNLabelChamp4 = new SNLabelChamp();
    sNComboBox2 = new SNComboBox();
    sNLabelChamp5 = new SNLabelChamp();
    sNComboBox3 = new SNComboBox();
    sNLabelChamp6 = new SNLabelChamp();
    sNComboBox4 = new SNComboBox();
    sNLabelChamp7 = new SNLabelChamp();
    sNPanel6 = new SNPanel();
    sNRadioButton2 = new SNRadioButton();
    sNRadioButton1 = new SNRadioButton();
    sNLabelChamp8 = new SNLabelChamp();
    sNPanel7 = new SNPanel();
    sNRadioButton3 = new SNRadioButton();
    sNRadioButton4 = new SNRadioButton();
    sNLabelChamp9 = new SNLabelChamp();
    sNPanel8 = new SNPanel();
    sNRadioButton5 = new SNRadioButton();
    sNRadioButton6 = new SNRadioButton();
    sNLabelTitre4 = new SNLabelTitre();
    sNLabelChamp10 = new SNLabelChamp();
    sNArticle1 = new SNArticle();
    sNLabelChamp11 = new SNLabelChamp();
    sNArticle2 = new SNArticle();
    sNLabelChamp12 = new SNLabelChamp();
    sNArticle3 = new SNArticle();
    snBarreBouton = new SNBarreBouton();
    sNLabelChamp15 = new SNLabelChamp();
    
    // ======== this ========
    setPreferredSize(new Dimension(1190, 700));
    setMinimumSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpBandeau ----
    bpBandeau.setText("Param\u00e9trage");
    bpBandeau.setName("bpBandeau");
    add(bpBandeau, BorderLayout.NORTH);
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== sNPanelContenu1 ========
      {
        sNPanelContenu1.setName("sNPanelContenu1");
        sNPanelContenu1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== sNPanel2 ========
          {
            sNPanel2.setAutoscrolls(true);
            sNPanel2.setPreferredSize(new Dimension(100, 350));
            sNPanel2.setMinimumSize(new Dimension(100, 350));
            sNPanel2.setName("sNPanel2");
            sNPanel2.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== scrollPane1 ========
            {
              scrollPane1.setName("scrollPane1");
              
              // ---- tree1 ----
              tree1.setModel(new DefaultTreeModel(new DefaultMutableTreeNode("Param\u00e9trage") {
                {
                  add(new DefaultMutableTreeNode("Param\u00e9trage g\u00e9n\u00e9ral"));
                  DefaultMutableTreeNode node1 = new DefaultMutableTreeNode("Ventes");
                  node1.add(new DefaultMutableTreeNode("G\u00e9n\u00e9ral"));
                  DefaultMutableTreeNode node2 = new DefaultMutableTreeNode("Documents de ventes");
                  node2.add(new DefaultMutableTreeNode("G\u00e9n\u00e9ral"));
                  node2.add(new DefaultMutableTreeNode("Prix"));
                  node2.add(new DefaultMutableTreeNode("Client"));
                  node2.add(new DefaultMutableTreeNode("Livraison"));
                  node1.add(node2);
                  node1.add(new DefaultMutableTreeNode("Stocks"));
                  node1.add(new DefaultMutableTreeNode("Statistiques"));
                  add(node1);
                  add(new DefaultMutableTreeNode("Achats"));
                  add(new DefaultMutableTreeNode("Comptabilit\u00e9"));
                }
              }));
              tree1.setBackground(new Color(239, 239, 222));
              tree1.setName("tree1");
              scrollPane1.setViewportView(tree1);
            }
            sNPanel2.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanel1.add(sNPanel2, new GridBagConstraints(0, 0, 1, 1, 25.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== sNPanel3 ========
          {
            sNPanel3.setName("sNPanel3");
            sNPanel3.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel3.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel3.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel3.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel3.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            
            // ---- sNLabelTitre2 ----
            sNLabelTitre2.setText("Ventes > Documents de ventes > Prix");
            sNLabelTitre2.setName("sNLabelTitre2");
            sNPanel3.add(sNLabelTitre2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== sNPanel5 ========
            {
              sNPanel5.setName("sNPanel5");
              sNPanel5.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel5.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) sNPanel5.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- sNLabelTitre5 ----
              sNLabelTitre5.setText("Autorisations utilisateur");
              sNLabelTitre5.setName("sNLabelTitre5");
              sNPanel5.add(sNLabelTitre5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNComboBox5 ----
              sNComboBox5.setModel(new DefaultComboBoxModel(new String[] { "Herv\u00e9 (HBS)" }));
              sNComboBox5.setName("sNComboBox5");
              sNPanel5.add(sNComboBox5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp13 ----
              sNLabelChamp13.setText("Gestion des tarifs en cours");
              sNLabelChamp13.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp13.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp13.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp13.setName("sNLabelChamp13");
              sNPanel5.add(sNLabelChamp13, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNComboBox6 ----
              sNComboBox6.setModel(new DefaultComboBoxModel(new String[] { "Tous les droits" }));
              sNComboBox6.setName("sNComboBox6");
              sNPanel5.add(sNComboBox6, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp14 ----
              sNLabelChamp14.setText("Gestion des tarifs en pr\u00e9paration");
              sNLabelChamp14.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp14.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp14.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp14.setName("sNLabelChamp14");
              sNPanel5.add(sNLabelChamp14, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNComboBox7 ----
              sNComboBox7.setModel(new DefaultComboBoxModel(new String[] { "Tous les droits" }));
              sNComboBox7.setName("sNComboBox7");
              sNPanel5.add(sNComboBox7, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelTitre3 ----
              sNLabelTitre3.setText("Options");
              sNLabelTitre3.setName("sNLabelTitre3");
              sNPanel5.add(sNLabelTitre3, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp3 ----
              sNLabelChamp3.setText("Vente impossible si le PUMP est \u00e0 z\u00e9ro");
              sNLabelChamp3.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp3.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp3.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp3.setName("sNLabelChamp3");
              sNPanel5.add(sNLabelChamp3, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNComboBox1 ----
              sNComboBox1.setModel(new DefaultComboBoxModel(new String[] { "Pas d'interdiction" }));
              sNComboBox1.setName("sNComboBox1");
              sNPanel5.add(sNComboBox1, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp4 ----
              sNLabelChamp4.setText("Contr\u00f4le du prix de vente");
              sNLabelChamp4.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp4.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp4.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp4.setName("sNLabelChamp4");
              sNPanel5.add(sNLabelChamp4, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNComboBox2 ----
              sNComboBox2.setModel(new DefaultComboBoxModel(new String[] { "Erreur si le prix de vente est inf\u00e9rieur au PUMP" }));
              sNComboBox2.setName("sNComboBox2");
              sNPanel5.add(sNComboBox2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp5 ----
              sNLabelChamp5.setText("Saisie des prix de revient \u00e0 la ligne");
              sNLabelChamp5.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp5.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp5.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp5.setName("sNLabelChamp5");
              sNPanel5.add(sNLabelChamp5, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNComboBox3 ----
              sNComboBox3.setModel(new DefaultComboBoxModel(
                  new String[] { "Pas de proposition de saisie de prix si le prix de revient et le PUMP \u00e9galent z\u00e9ro" }));
              sNComboBox3.setName("sNComboBox3");
              sNPanel5.add(sNComboBox3, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp6 ----
              sNLabelChamp6.setText("Marge minimal oblligatoire");
              sNLabelChamp6.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp6.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp6.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp6.setName("sNLabelChamp6");
              sNPanel5.add(sNLabelChamp6, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNComboBox4 ----
              sNComboBox4.setModel(new DefaultComboBoxModel(new String[] { "Erreur si le minimum de marge n'est pas atteint" }));
              sNComboBox4.setName("sNComboBox4");
              sNPanel5.add(sNComboBox4, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp7 ----
              sNLabelChamp7.setText("Calcul des prix tarifs en cascade");
              sNLabelChamp7.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp7.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp7.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp7.setName("sNLabelChamp7");
              sNPanel5.add(sNLabelChamp7, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== sNPanel6 ========
              {
                sNPanel6.setName("sNPanel6");
                sNPanel6.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel6.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) sNPanel6.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) sNPanel6.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) sNPanel6.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- sNRadioButton2 ----
                sNRadioButton2.setText("Oui");
                sNRadioButton2.setPreferredSize(new Dimension(80, 30));
                sNRadioButton2.setMinimumSize(new Dimension(80, 30));
                sNRadioButton2.setName("sNRadioButton2");
                sNPanel6.add(sNRadioButton2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- sNRadioButton1 ----
                sNRadioButton1.setText("Non");
                sNRadioButton1.setPreferredSize(new Dimension(80, 30));
                sNRadioButton1.setMinimumSize(new Dimension(80, 30));
                sNRadioButton1.setName("sNRadioButton1");
                sNPanel6.add(sNRadioButton1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              }
              sNPanel5.add(sNPanel6, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp8 ----
              sNLabelChamp8.setText("Colonne de tarif pr\u00e9c\u00e9dente si le tarif client n'est pas d\u00e9fini");
              sNLabelChamp8.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp8.setPreferredSize(new Dimension(370, 30));
              sNLabelChamp8.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp8.setName("sNLabelChamp8");
              sNPanel5.add(sNLabelChamp8, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== sNPanel7 ========
              {
                sNPanel7.setName("sNPanel7");
                sNPanel7.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel7.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) sNPanel7.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) sNPanel7.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) sNPanel7.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- sNRadioButton3 ----
                sNRadioButton3.setText("Oui");
                sNRadioButton3.setPreferredSize(new Dimension(80, 30));
                sNRadioButton3.setMinimumSize(new Dimension(80, 30));
                sNRadioButton3.setName("sNRadioButton3");
                sNPanel7.add(sNRadioButton3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- sNRadioButton4 ----
                sNRadioButton4.setText("Non");
                sNRadioButton4.setPreferredSize(new Dimension(80, 30));
                sNRadioButton4.setMinimumSize(new Dimension(80, 30));
                sNRadioButton4.setName("sNRadioButton4");
                sNPanel7.add(sNRadioButton4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              }
              sNPanel5.add(sNPanel7, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp9 ----
              sNLabelChamp9.setText("Calcul du prix net \u00e0 partir de la colonne 1");
              sNLabelChamp9.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp9.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp9.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp9.setName("sNLabelChamp9");
              sNPanel5.add(sNLabelChamp9, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== sNPanel8 ========
              {
                sNPanel8.setName("sNPanel8");
                sNPanel8.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel8.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) sNPanel8.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) sNPanel8.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) sNPanel8.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- sNRadioButton5 ----
                sNRadioButton5.setText("Oui");
                sNRadioButton5.setPreferredSize(new Dimension(80, 30));
                sNRadioButton5.setMinimumSize(new Dimension(80, 30));
                sNRadioButton5.setName("sNRadioButton5");
                sNPanel8.add(sNRadioButton5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- sNRadioButton6 ----
                sNRadioButton6.setText("Non");
                sNRadioButton6.setPreferredSize(new Dimension(80, 30));
                sNRadioButton6.setMinimumSize(new Dimension(80, 30));
                sNRadioButton6.setName("sNRadioButton6");
                sNPanel8.add(sNRadioButton6, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              }
              sNPanel5.add(sNPanel8, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelTitre4 ----
              sNLabelTitre4.setText("Donn\u00e9es");
              sNLabelTitre4.setName("sNLabelTitre4");
              sNPanel5.add(sNLabelTitre4, new GridBagConstraints(0, 11, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp10 ----
              sNLabelChamp10.setText("Conditions de ventes");
              sNLabelChamp10.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp10.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp10.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp10.setName("sNLabelChamp10");
              sNPanel5.add(sNLabelChamp10, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNArticle1 ----
              sNArticle1.setMinimumSize(new Dimension(280, 30));
              sNArticle1.setPreferredSize(new Dimension(280, 30));
              sNArticle1.setName("sNArticle1");
              sNPanel5.add(sNArticle1, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp11 ----
              sNLabelChamp11.setText("Devises");
              sNLabelChamp11.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp11.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp11.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp11.setName("sNLabelChamp11");
              sNPanel5.add(sNLabelChamp11, new GridBagConstraints(0, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNArticle2 ----
              sNArticle2.setMinimumSize(new Dimension(280, 30));
              sNArticle2.setPreferredSize(new Dimension(280, 30));
              sNArticle2.setName("sNArticle2");
              sNPanel5.add(sNArticle2, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- sNLabelChamp12 ----
              sNLabelChamp12.setText("Types de gratuits");
              sNLabelChamp12.setMinimumSize(new Dimension(350, 30));
              sNLabelChamp12.setPreferredSize(new Dimension(350, 30));
              sNLabelChamp12.setMaximumSize(new Dimension(350, 30));
              sNLabelChamp12.setName("sNLabelChamp12");
              sNPanel5.add(sNLabelChamp12, new GridBagConstraints(0, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- sNArticle3 ----
              sNArticle3.setMinimumSize(new Dimension(280, 30));
              sNArticle3.setPreferredSize(new Dimension(280, 30));
              sNArticle3.setName("sNArticle3");
              sNPanel5.add(sNArticle3, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanel3.add(sNPanel5, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanel1.add(sNPanel3, new GridBagConstraints(1, 0, 1, 1, 75.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelContenu1.add(sNPanel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(sNPanelContenu1, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- sNLabelChamp15 ----
    sNLabelChamp15.setText("Gestion des tarifs en pr\u00e9paration");
    sNLabelChamp15.setMinimumSize(new Dimension(350, 30));
    sNLabelChamp15.setPreferredSize(new Dimension(350, 30));
    sNLabelChamp15.setMaximumSize(new Dimension(350, 30));
    sNLabelChamp15.setName("sNLabelChamp15");
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(sNRadioButton2);
    buttonGroup1.add(sNRadioButton1);
    buttonGroup1.add(sNRadioButton3);
    buttonGroup1.add(sNRadioButton4);
    buttonGroup1.add(sNRadioButton5);
    buttonGroup1.add(sNRadioButton6);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpBandeau;
  private SNPanelEcranRPG pnlPrincipal;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel sNPanel1;
  private SNPanel sNPanel2;
  private JScrollPane scrollPane1;
  private JTree tree1;
  private SNPanel sNPanel3;
  private SNLabelTitre sNLabelTitre2;
  private SNPanel sNPanel5;
  private SNLabelTitre sNLabelTitre5;
  private SNComboBox sNComboBox5;
  private SNLabelChamp sNLabelChamp13;
  private SNComboBox sNComboBox6;
  private SNLabelChamp sNLabelChamp14;
  private SNComboBox sNComboBox7;
  private SNLabelTitre sNLabelTitre3;
  private SNLabelChamp sNLabelChamp3;
  private SNComboBox sNComboBox1;
  private SNLabelChamp sNLabelChamp4;
  private SNComboBox sNComboBox2;
  private SNLabelChamp sNLabelChamp5;
  private SNComboBox sNComboBox3;
  private SNLabelChamp sNLabelChamp6;
  private SNComboBox sNComboBox4;
  private SNLabelChamp sNLabelChamp7;
  private SNPanel sNPanel6;
  private SNRadioButton sNRadioButton2;
  private SNRadioButton sNRadioButton1;
  private SNLabelChamp sNLabelChamp8;
  private SNPanel sNPanel7;
  private SNRadioButton sNRadioButton3;
  private SNRadioButton sNRadioButton4;
  private SNLabelChamp sNLabelChamp9;
  private SNPanel sNPanel8;
  private SNRadioButton sNRadioButton5;
  private SNRadioButton sNRadioButton6;
  private SNLabelTitre sNLabelTitre4;
  private SNLabelChamp sNLabelChamp10;
  private SNArticle sNArticle1;
  private SNLabelChamp sNLabelChamp11;
  private SNArticle sNArticle2;
  private SNLabelChamp sNLabelChamp12;
  private SNArticle sNArticle3;
  private SNBarreBouton snBarreBouton;
  private SNLabelChamp sNLabelChamp15;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
