/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.installweb.installation;

import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.serien.libas400.system.SystemeManager;

/**
 * Récupère les informations sur l'AS400.
 */
public class AS400Informations {
  // Constantes
  private static final int LONG_DTAARA = 64;
  private static final String DTA_NAME = "VGMDTA";
  private static final String LIB_NAME = "QGPL";
  
  // Variables
  private String adresseIP = null;
  
  private SystemeManager system = null;
  private char letter = 'X';
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur.
   */
  public AS400Informations(SystemeManager asystem, char aletter) {
    system = asystem;
    if (aletter != letter) {
      letter = aletter;
    }
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Récupère les informations sur la configuration de l'AS400.
   */
  public boolean readInformations() {
    QSYSObjectPathName path = new QSYSObjectPathName(LIB_NAME, DTA_NAME, "DTAARA");
    CharacterDataArea dataArea = new CharacterDataArea(system.getSystem(), path.getPath());
    String data = readData(dataArea);
    if (data == null) {
      return false;
    }
    
    return analyzeData(data);
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Analyse les données de la dataarea.
   */
  private boolean analyzeData(String data) {
    if (data.length() < LONG_DTAARA) {
      msgErreur += "\n[AS400Informations] (analyzeData) Longueur de dataarea inférieure à " + LONG_DTAARA + " octets.";
      return false;
    }
    
    adresseIP = data.substring(0, 15).trim();
    
    return true;
  }
  
  /**
   * Lit et retourne le contenu de la DTAARA.
   */
  private String readData(CharacterDataArea dataarea) {
    String data = null;
    
    try {
      data = dataarea.read();
    }
    catch (Exception e) {
      msgErreur += "\n[AS400Informations] (readData) Erreur : " + e;
    }
    
    return data;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le adresse.
   */
  public String getAdresseIP() {
    return adresseIP;
  }
  
  /**
   * @param adresse le adresse à définir.
   */
  public void setAdresseIP(String adresse) {
    this.adresseIP = adresse;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
