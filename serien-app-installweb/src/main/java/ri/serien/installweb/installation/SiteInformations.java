/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.installweb.installation;

import java.io.File;

import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.serien.libas400.system.SystemeManager;

/**
 * Récupère les informations pour la connexion au site de mise à jour.
 */
public class SiteInformations {
  // Constantes
  private static final int LONG_DTAARA = 500;
  private static final String DTA_NAME = "DWN_SERIEM";
  
  // Variables
  private String login = null;
  private String mdp = null;
  private String adresse = null;
  private String repertoireFTP = null;
  
  private File cheminRacine = null;
  
  private SystemeManager system = null;
  private char letter = 'X';
  private String libExpas = letter + "EXPAS";
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur.
   */
  public SiteInformations(SystemeManager asystem, char aletter, String pProfil, String pMdp, String pAdresse, String pRepertoire) {
    system = asystem;
    if (aletter != letter) {
      letter = aletter;
      libExpas = letter + libExpas.substring(1);
    }
    login = pProfil;
    mdp = pMdp;
    adresse = pAdresse;
    repertoireFTP = pRepertoire;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Récupère les informations afin de pouvoir se connecter au site.
   */
  public boolean readInformations() {
    QSYSObjectPathName path = new QSYSObjectPathName(libExpas, DTA_NAME, "DTAARA");
    CharacterDataArea dataArea = new CharacterDataArea(system.getSystem(), path.getPath());
    String data = readData(dataArea);
    if (data == null) {
      return false;
    }
    
    return analyzeData(data);
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Analyse les données de la dataarea.
   */
  private boolean analyzeData(String data) {
    if (data.length() < LONG_DTAARA) {
      msgErreur += "\n[SiteInformations] (analyzeData) Longueur de dataarea inférieure à " + LONG_DTAARA + " octets.";
      return false;
    }
    
    login = data.substring(0, 20).trim();
    mdp = data.substring(20, 40).trim();
    adresse = data.substring(40, 79).trim();
    cheminRacine = new File(data.substring(300, 399).trim()).getParentFile();
    
    return true;
  }
  
  /**
   * Lit et retourne le contenu de la DTAARA.
   */
  private String readData(CharacterDataArea dataarea) {
    String data = null;
    
    try {
      data = dataarea.read();
    }
    catch (Exception e) {
      msgErreur += "\n[SiteInformations] (readData) Erreur : " + e;
    }
    
    return data;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le login.
   */
  public String getLogin() {
    return login;
  }
  
  /**
   * @param login le login à définir.
   */
  public void setLogin(String login) {
    this.login = login;
  }
  
  /**
   * @return le mdp.
   */
  public String getMdp() {
    return mdp;
  }
  
  /**
   * @param mdp le mdp à définir.
   */
  public void setMdp(String mdp) {
    this.mdp = mdp;
  }
  
  /**
   * @return le adresse.
   */
  public String getAdresse() {
    return adresse;
  }
  
  /**
   * @param adresse le adresse à définir.
   */
  public void setAdresse(String adresse) {
    this.adresse = adresse;
  }
  
  /**
   * @return le cheminRacine.
   */
  public File getCheminRacine() {
    return cheminRacine;
  }
  
  /**
   * @param cheminRacine le cheminRacine à définir.
   */
  public void setCheminRacine(File cheminRacine) {
    this.cheminRacine = cheminRacine;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  public String getRepertoireFTP() {
    return repertoireFTP;
  }
  
}
