/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.installweb.installation;

import java.util.Date;

public class Application {
  private String name = null;
  private String lastModified = "";
  private String lastModifiedOnFTP = "";
  private String file = null;
  private int offsetName = 0;
  private int offsetLastModified = 0;
  private boolean maj = false;
  
  public Application(String aname, String lastmod) {
    setName(aname);
    setLastModified(lastmod);
  }
  
  /**
   * Formate la date correctement.
   */
  private String formateDate(String datefile) {
    if ((datefile == null) || (datefile.trim().length() == 0)) {
      return "";
    }
    
    Date date = new Date();
    boolean withours = (datefile.lastIndexOf(':') != -1);
    String[] tab = datefile.split("\\s+");
    int jour = Integer.parseInt(tab[1]);
    if (tab.length == 3) {
      if (withours) {
        datefile = String.format("%s %02d %s %s", tab[0], jour, String.format("%tY", date), tab[2]);
      }
      else {
        datefile = String.format("%s %02d %s 00:00", tab[0], jour, tab[2]);
      }
    }
    else if (tab.length == 4) {
      datefile = String.format("%s %02d %s %s", tab[0], jour, tab[2], tab[3]);
    }
    return datefile;
  }
  
  /**
   * @return le name.
   */
  public String getName() {
    return name;
  }
  
  /**
   * @param name le name à définir.
   */
  public void setName(String name) {
    if (name != null) {
      this.name = name.trim();
      if (!this.name.endsWith(".war")) {
        this.name += ".war";
      }
    }
    else {
      this.name = null;
    }
  }
  
  /**
   * @return le lastModified.
   */
  public String getLastModified() {
    return lastModified;
  }
  
  /**
   * @param lastModified le lastModified à définir.
   */
  public void setLastModified(String lastModified) {
    this.lastModified = formateDate(lastModified);
    if (this.lastModified == null) {
      this.lastModified = "";
    }
  }
  
  /**
   * @return le lastModifiedOnFTP.
   */
  public String getLastModifiedOnFTP() {
    return lastModifiedOnFTP;
  }
  
  /**
   * @param lastModifiedOnFTP le lastModifiedOnFTP à définir.
   */
  public void setLastModifiedOnFTP(String lastModifiedOnFTP) {
    this.lastModifiedOnFTP = formateDate(lastModifiedOnFTP);
    if (this.lastModifiedOnFTP == null) {
      this.lastModifiedOnFTP = "";
    }
  }
  
  /**
   * @return le file.
   */
  public String getFile() {
    return file;
  }
  
  /**
   * @param file le file à définir.
   */
  public void setFile(String file) {
    this.file = file;
  }
  
  /**
   * @return le offsetName.
   */
  public int getOffsetName() {
    return offsetName;
  }
  
  /**
   * @param offsetName le offsetName à définir.
   */
  public void setOffsetName(int offsetName) {
    this.offsetName = offsetName;
  }
  
  /**
   * @return le offsetLastModified.
   */
  public int getOffsetLastModified() {
    return offsetLastModified;
  }
  
  /**
   * @param offsetLastModified le offsetLastModified à définir.
   */
  public void setOffsetLastModified(int offsetLastModified) {
    this.offsetLastModified = offsetLastModified;
  }
  
  /**
   * @return le maj.
   */
  public boolean isMaj() {
    return maj;
  }
  
  /**
   * @param maj le maj à définir.
   */
  public void setMaj(boolean maj) {
    this.maj = maj;
  }
  
}
