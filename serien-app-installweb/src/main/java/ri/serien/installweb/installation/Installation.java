/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.installweb.installation;

import java.io.File;
import java.util.ArrayList;

import com.ibm.as400.access.ISeriesNetServer;
import com.ibm.as400.access.ISeriesNetServerFileShare;

import ri.serien.libas400.system.AS400FTPManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.libcommun.outils.fichier.GestionFichierZip;

/**
 * Gestion de l'installation pour les applications WEB.
 */
public class Installation {
  // Constantes
  private static final String VERSION = "1.02";
  private static final String RADICAL_WEB = "web";
  private static final String RADICAL_SERIEN = "serien";
  private static final String TEMP_IFS_FOLDER = "/tmp";
  private static final String RADICAL_TOMCAT = "apache-tomcat";
  private static final String TOMCAT = "tomcat";
  private static final String NAME_WEBSHOP = "WebShop";
  private static final ArrayList<Character> MONTH = new ArrayList<Character>() {
    
    {
      add('J');
    }
    
    {
      add('F');
    }
    
    {
      add('M');
    }
    
    {
      add('A');
    }
    
    {
      add('S');
    }
    
    {
      add('O');
    }
    
    {
      add('N');
    }
    
    {
      add('D');
    }
  };
  
  // Variables
  private SystemeManager system = new SystemeManager(false);
  private char letter = 'X'; // La lettre de l'environnement
  private String installfolder = null; // Le dossier racine d'installation
  private String library = "xweb"; // Le nom de la bibliothèque
  private String profilFTP = null;
  private String mdpFTP = null;
  private String adresseFTP = null;
  private String repertoireFTP = null;
  private char digit = '7'; // Correspond au chiffre des dizaines 8080 devient 8070 par exemple
  private String adresseIPlocale = "localhost";
  private AS400FTPManager ftp = null;
  private String fileDlTomcat = null;
  private String folderwebapps = null;
  private WebInformations webinfos = null;
  private SiteInformations infosFTP = null;
  private String profilManager = null;
  private String mdpManager = null;
  
  /**
   * Constructeur
   */
  public Installation(char pLetter, String pRacine, String pLib, String pProfil, String pMdp, String pAdress, String pRepertoire,
      String pProfilManager, String pMdpManager) {
    letter = pLetter;
    if (pLib != null) {
      library = pLib.trim();
    }
    if (pRacine != null) {
      installfolder = pRacine.toLowerCase();
    }
    
    if (pProfil != null) {
      profilFTP = pProfil.trim();
    }
    
    if (pMdp != null) {
      mdpFTP = pMdp.trim();
    }
    
    if (pAdress != null) {
      adresseFTP = pAdress.trim();
    }
    
    if (pRepertoire != null) {
      repertoireFTP = pRepertoire.trim();
      if (!repertoireFTP.endsWith("/")) {
        repertoireFTP = repertoireFTP + "/";
      }
    }
    
    if (pProfilManager != null) {
      profilManager = pProfilManager.trim();
    }
    
    if (pMdpManager != null) {
      mdpManager = pMdpManager.trim();
    }
    
    // On controle la validité du nom du dossier racine
    if (installfolder == null || installfolder.isEmpty()) {
      System.exit(-1);
    }
    
    // Les traces sont effacées avant
    File dossierRacine = new File(installfolder);
    Trace.demarrerLogiciel(dossierRacine.getParent(), "installweb", "Série N Installation Web");
    
    // Récupération des données sur les applis Web
    webinfos = new WebInformations(system, library, letter);
    if (!webinfos.readInformations()) {
      Trace.erreur(webinfos.getMsgError());
    }
    else {
      digit = webinfos.getDigit();
    }
    
  }
  
  /**
   * Traitement pour l'installation.
   */
  public boolean treatment() {
    if (webinfos.isInstallation()) {
      return treatmentInstallation();
    }
    else {
      return treatmentUpdate();
    }
  }
  
  /**
   * Traitement pour l'installation.
   */
  private boolean treatmentInstallation() {
    boolean tomcatIsInstalled = false;
    
    Trace.info("Installation pour les applications mobiles (" + VERSION + ") ==");
    /* Plus besoin de partager le dossier puisqu'il se trouve dans le dossier centralisateur de Serie N
    Log.info("\t-- Partage du dossier de l'IFS");
    shareIFSFolder();
    */
    
    Trace.info("Contrôle de l'installation");
    if (!executeInstallation()) {
      Trace.info("Tomcat est déjà installé.");
      tomcatIsInstalled = true;
    }
    
    Trace.info("Connexion au serveur FTP");
    if (!connexion()) {
      return false;
    }
    
    if (!tomcatIsInstalled) {
      Trace.info("Téléchargement de Tomcat");
      if (!downloadTomcat()) {
        return false;
      }
      
      Trace.info("Décompression de Tomcat");
      if (!unzipTomcat()) {
        return false;
      }
      
      Trace.info("Configuration de Tomcat");
      if (!configureTomcat()) {
        return false;
      }
    }
    
    Trace.info("Téléchargement des applications web");
    if (!downloadApplis()) {
      return false;
    }
    
    String cmd =
        "QSH CMD('" + installfolder + File.separatorChar + TOMCAT + File.separatorChar + "bin" + File.separatorChar + "startup.sh')";
    String url = "http://ADRESSE_IP_AS400:80" + digit + "0/" + NAME_WEBSHOP;
    Trace.info("Installation terminée.");
    Trace.info("Il ne vous reste plus qu'à démarrer le serveur Tomcat avec la commande:");
    Trace.info("" + cmd);
    Trace.info("Puis à vous connecter localement avec l'URL suivante :");
    Trace.info("" + url);
    Trace.info("Note: Attention à la casse du chemin pour démarrer le serveur Tomcat (Erreur: ...is malformed and will be ignored...).");
    return true;
  }
  
  /**
   * Traitement pour la mise à jour.
   */
  private boolean treatmentUpdate() {
    Trace.info("Mise à jour des applications mobiles (" + VERSION + ") ==");
    
    if (!webinfos.isMajTomcat() && !webinfos.isMajApplis()) {
      Trace.info("Aucune mise à jour demandée");
      return true;
    }
    
    Trace.info("Connexion au serveur FTP");
    if (!connexion()) {
      return false;
    }
    
    if (!updateTomcat()) {
      return false;
    }
    
    Trace.info("Téléchargement des applications web");
    if (!downloadApplis()) {
      return false;
    }
    
    String cmd =
        "QSH CMD('" + installfolder + File.separatorChar + TOMCAT + File.separatorChar + "bin" + File.separatorChar + "startup.sh')";
    String url = "http://ADRESSE_IP_AS400:80" + digit + "0/" + NAME_WEBSHOP;
    if (webinfos.isMajTomcat()) {
      Trace.info("Installation terminée.");
      Trace.info("Il ne vous reste plus qu'à démarrer le serveur Tomcat avec la commande:");
      Trace.info("" + cmd);
      Trace.info("Puis à vous connecter localement avec l'URL suivante :");
      Trace.info("" + url);
    }
    else {
      Trace.info("Installation terminée.");
      Trace.info("Il ne vous reste plus qu'à vous connecter localement avec l'URL suivante :");
      Trace.info("" + url);
    }
    Trace.info("Note: Attention à la casse du chemin pour démarrer le serveur Tomcat (Erreur: ...is malformed and will be ignored...).");
    return true;
  }
  
  /**
   * Mise à jour de Tomcat.
   */
  private boolean updateTomcat() {
    // On vérifie si l'on doit mettre à jour Tomcat (demande depuis InstWebCL)
    if (!webinfos.isMajTomcat()) {
      return true;
    }
    
    Trace.info("Téléchargement de Tomcat");
    if (!downloadTomcat()) {
      return false;
    }
    
    // On vérifie si l'on doit mettre à jour Tomcat (download a vérifié si c'était vraiment nécessaire)
    if (!webinfos.isMajTomcat()) {
      return true;
    }
    
    Trace.info("Sauvegarde des fichiers de configuration de Tomcat");
    if (!saveConfTomcat()) {
      return false;
    }
    
    Trace.info("Suppression du dossier de Tomcat");
    File tomcatfolder = new File(installfolder + File.separatorChar + TOMCAT);
    FileNG.remove(tomcatfolder);
    if (tomcatfolder.exists()) {
      Trace.erreur("\nLe dossier de Tomcat n'a pas été supprimé correctement");
      return false;
    }
    
    Trace.info("Décompression de Tomcat");
    if (!unzipTomcat()) {
      return false;
    }
    
    Trace.info("Configuration de Tomcat");
    if (!configureTomcat()) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Sauvegarde des fichiers de configuration de Tomcat.
   */
  private boolean saveConfTomcat() {
    File savefolder = new File(TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT);
    if (savefolder.exists()) {
      FileNG.removeContents(savefolder);
    }
    else {
      savefolder.mkdirs();
    }
    
    FileNG context =
        new FileNG(installfolder + File.separatorChar + TOMCAT + File.separatorChar + "conf" + File.separatorChar + "context.xml");
    boolean ret1 = context.copyTo(savefolder.getAbsolutePath() + File.separatorChar + "context.xml");
    if (!ret1) {
      Trace.erreur("Echec de la sauvegarde du fichier " + context.getAbsolutePath());
    }
    
    FileNG server =
        new FileNG(installfolder + File.separatorChar + TOMCAT + File.separatorChar + "conf" + File.separatorChar + "server.xml");
    boolean ret2 = server.copyTo(savefolder.getAbsolutePath() + File.separatorChar + "server.xml");
    if (!ret2) {
      Trace.erreur("Echec de la sauvegarde du fichier " + server.getAbsolutePath());
    }
    
    FileNG setenv =
        new FileNG(installfolder + File.separatorChar + TOMCAT + File.separatorChar + "bin" + File.separatorChar + "setenv.sh");
    boolean ret3 = setenv.copyTo(savefolder.getAbsolutePath() + File.separatorChar + "setenv.sh");
    if (!ret3) {
      Trace.erreur("Echec de la sauvegarde du fichier " + setenv.getAbsolutePath());
    }
    
    FileNG tomcat_users =
        new FileNG(installfolder + File.separatorChar + TOMCAT + File.separatorChar + "conf" + File.separatorChar + "tomcat-users.xml");
    boolean ret4 = tomcat_users.copyTo(savefolder.getAbsolutePath() + File.separatorChar + "tomcat-users.xml");
    if (!ret4) {
      Trace.erreur("Echec de la sauvegarde du fichier " + tomcat_users.getAbsolutePath());
    }
    
    return ret1 & ret2 & ret3 & ret4;
  }
  
  /**
   * Restaure les fichiers de configuration de Tomcat.
   */
  private boolean restoreConfTomcat() {
    File finstalltomcatfolder = new File(installfolder + File.separatorChar + TOMCAT);
    
    // Sauvegarde les nouveaux fichiers
    boolean ret1 = false;
    FileNG context =
        new FileNG(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "context.xml");
    ret1 = context
        .renameTo(new File(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "context.new"));
    FileNG server = new FileNG(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "server.xml");
    ret1 = server
        .renameTo(new File(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "server.new"));
    FileNG tomcat_users =
        new FileNG(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "tomcat-users.xml");
    ret1 = tomcat_users.renameTo(
        new File(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "tomcat-users.new"));
    
    // Restaure les anciens
    context = new FileNG(TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT + File.separatorChar + "context.xml");
    File rstfolderconf = new File(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf");
    ret1 = context.copyTo(rstfolderconf.getAbsolutePath() + File.separatorChar + "context.xml");
    if (!ret1) {
      Trace.erreur("Echec de la restauration du fichier " + context.getAbsolutePath());
    }
    
    server = new FileNG(TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT + File.separatorChar + "server.xml");
    boolean ret2 = server.copyTo(rstfolderconf.getAbsolutePath() + File.separatorChar + "server.xml");
    if (!ret2) {
      Trace.erreur("Echec de la restauration du fichier " + server.getAbsolutePath());
    }
    
    File rstfolderbin = new File(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "bin");
    FileNG setenv = new FileNG(TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT + File.separatorChar + "setenv.sh");
    boolean ret3 = setenv.copyTo(rstfolderbin.getAbsolutePath() + File.separatorChar + "setenv.sh");
    if (!ret3) {
      Trace.erreur("Echec de la restauration du fichier " + setenv.getAbsolutePath());
    }
    
    tomcat_users = new FileNG(TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT + File.separatorChar + "tomcat-users.xml");
    boolean ret4 = tomcat_users.copyTo(rstfolderconf.getAbsolutePath() + File.separatorChar + "tomcat-users.xml");
    if (!ret4) {
      Trace.erreur("Echec de la restauration du fichier " + tomcat_users.getAbsolutePath());
    }
    
    return ret1 & ret2 & ret3 & ret4;
  }
  
  /**
   * Controle l'installation de Tomcat afin de déterminer la suite de l'installation.
   */
  private boolean executeInstallation() {
    File tomcatfolder = new File(installfolder + File.separatorChar + TOMCAT);
    if (!tomcatfolder.exists()) {
      return true;
    }
    
    File tomcatstartup = new File(tomcatfolder.getAbsolutePath() + File.separatorChar + "bin" + File.separatorChar + "startup.sh");
    if (!tomcatstartup.exists()) {
      return true;
    }
    
    return false;
  }
  
  /**
   * Connexion au serveur FTP
   * @return
   */
  private boolean connexion() {
    // Récupération des données pour la connexion
    infosFTP = new SiteInformations(system, letter, profilFTP, mdpFTP, adresseFTP, repertoireFTP);
    
    // Connexion au serveur FTP
    ftp = new AS400FTPManager();
    boolean ret = ftp.connexion(infosFTP.getAdresse(), infosFTP.getLogin(), infosFTP.getMdp());
    if (!ret) {
      Trace.erreur("Erreur lors de la connexion au serveur FTP : " + ftp.getMsgError());
    }
    else {
      Trace.info("Ok connexion FTP");
    }
    
    return ret;
  }
  
  /**
   * Téléchargement du fichier zip de Tomcat.
   */
  private boolean downloadTomcat() {
    // Récupération du dossier racine sur le serveur FTP
    String dossierTomcat = infosFTP.getRepertoireFTP();
    int pos = dossierTomcat.indexOf("/", 1);
    if (pos != -1) {
      dossierTomcat = dossierTomcat.substring(0, pos);
    }
    if (!dossierTomcat.endsWith("/")) {
      dossierTomcat = dossierTomcat + "/tomcat/";
    }
    else {
      dossierTomcat = dossierTomcat + "tomcat/";
    }
    
    // Listage des fichiers sur le serveur FTP
    String[] list = ftp.listFolder(dossierTomcat);
    if (list == null) {
      Trace.erreur("Erreur lors du listage du dossier " + dossierTomcat + "tomcat/");
      return false;
    }
    // On s'assure qu'il s'y trouve
    String fileServerTomcat = null;
    for (String file : list) {
      if ((file.indexOf(RADICAL_TOMCAT) != -1) && file.endsWith(".zip")) {
        // On vérifie que le dossier de Tomcat soit bien présent
        File tomcatfolder = new File(installfolder + File.separatorChar + TOMCAT);
        if (!tomcatfolder.exists()) {
          fileServerTomcat = file;
          webinfos.setMajTomcat(true);
        }
        // On vérifie la version (si c'est la même on ne fait rien)
        else if ((webinfos.getVersionTomcat().length() == 0) || (file.indexOf(webinfos.getVersionTomcat()) == -1)) {
          fileServerTomcat = file;
          webinfos.setMajTomcat(true);
        }
        else {
          Trace.info("Tomcat est à jour (" + webinfos.getVersionTomcat() + ")");
          webinfos.setMajTomcat(false);
          return true;
        }
        break;
      }
    }
    if (fileServerTomcat == null) {
      Trace.erreur("Erreur le fichier zip de tomcat n'a pas été trouvé.");
      return false;
    }
    
    // Téléchargement du fichier
    pos = fileServerTomcat.indexOf(RADICAL_TOMCAT);
    if (pos != -1) {
      fileDlTomcat = TEMP_IFS_FOLDER + File.separatorChar + fileServerTomcat.substring(pos);
    }
    boolean ret = ftp.downloadFile(fileServerTomcat, fileDlTomcat, true);
    if (!ret) {
      Trace.erreur(ftp.getMsgError());
    }
    return ret;
  }
  
  /**
   * Dézippe le fichier zip de Tomcat dans le dossier voulu.
   */
  private boolean unzipTomcat() {
    if (fileDlTomcat == null) {
      return false;
    }
    File zip = new File(fileDlTomcat);
    if (!GestionFichierZip.unzip(zip, new File(installfolder))) {
      Trace.erreur(
          "[Installation] (unzipTomcat) Erreur lors du dézippage du fichier " + fileDlTomcat + " dans le dossier " + installfolder);
      return false;
    }
    else { // On supprime le fichier zip
      zip.delete();
      fileDlTomcat = zip.getName().substring(0, zip.getName().length() - 4);
    }
    
    return true;
  }
  
  /**
   * Configure Tomcat.
   */
  private boolean configureTomcat() {
    // On renomme le dossier Tomcat
    File finstallfolder = new File(installfolder);
    File finstalltomcatfolder = new File(installfolder + File.separatorChar + TOMCAT);
    File[] listFolder = finstallfolder.listFiles();
    for (File folder : listFolder) {
      if (folder.getName().startsWith(RADICAL_TOMCAT)) {
        folder.renameTo(finstalltomcatfolder);
        webinfos.setVersionTomcat(folder.getName());
        webinfos.writeVersionTomcat();
        break;
      }
    }
    
    // Création du fichier de version dans le dossier Tomcat
    GestionFichierTexte gft = new GestionFichierTexte();
    gft.setNomFichier(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "tomcat.version");
    gft.setContenuFichier(fileDlTomcat);
    gft.ecritureFichier();
    gft.dispose();
    
    // Configuration des fichiers
    if (webinfos.isInstallation()) {
      // Lors de l'installation
      configureServer_XML(finstalltomcatfolder);
      configureContext_XML(finstalltomcatfolder);
      configureTomcatUsers_XML(finstalltomcatfolder);
      configureLogging_properties(finstalltomcatfolder);
      createSetEnv();
      
    }
    else {
      // Lors de la mise à jour
      if (!restoreConfTomcat()) {
        Trace.erreur("Echec de la restauration des fichiers de configuration de Tomcat");
        return false;
      }
      else {
        FileNG.remove(new File(TEMP_IFS_FOLDER + File.separatorChar + "save_" + TOMCAT));
      }
    }
    
    return true;
  }
  
  /**
   * Modifie le fichier server.xml.
   */
  private boolean configureServer_XML(File finstalltomcatfolder) {
    GestionFichierTexte server =
        new GestionFichierTexte(finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "server.xml");
    if (server.lectureFichier() == Constantes.ERREUR) {
      Trace.erreur(server.getMsgErreur());
      return false;
    }
    
    boolean isComment = false;
    String[] source = server.getContenuFichierTab();
    for (int i = 0; i < source.length; i++) {
      // Gestion des commentaires
      if (source[i].trim().startsWith("<!--")) {
        isComment = true;
      }
      if (isComment) {
        if (source[i].trim().endsWith("-->")) {
          isComment = false;
        }
        if (isComment) {
          continue;
        }
      }
      // Gestion des lines à modifier
      String line = modifiePortLine(source[i], "port=\"", "SHUTDOWN");
      if (line != null) {
        source[i] = line;
      }
      else {
        line = modifiePortLine(source[i], "port=\"", "HTTP/1.1");
        if (line != null) {
          source[i] = line;
        }
        else {
          line = modifiePortLine(source[i], "port=\"", "AJP/1.3");
          if (line != null) {
            source[i] = line;
          }
        }
      }
    }
    
    // On réécrit le fichier modifié
    server.setContenuFichier(source);
    if (server.ecritureFichier() == Constantes.ERREUR) {
      Trace.erreur(server.getMsgErreur());
      return false;
    }
    return true;
  }
  
  /**
   * Modifie le fichier context.xml.
   */
  private boolean configureContext_XML(File finstalltomcatfolder) {
    // Chargement du fichier context.xml
    GestionFichierTexte context = new GestionFichierTexte(
        finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "context.xml");
    if (context.lectureFichier() == Constantes.ERREUR) {
      Trace.erreur(context.getMsgErreur());
      return false;
    }
    
    ArrayList<String> source = context.getContenuFichier();
    // On insère les lignes à la fin du fichier
    int avtderniere = source.size() - 2;
    source.add(avtderniere++, "\t<!-- MOBILITE & WEBSHOP -->");
    source.add(avtderniere++, String.format("\t<Parameter name=\"LIB_RACINE\" value=\"%s\" override=\"false\"/>", library));
    source.add(avtderniere++, "\t<Parameter name=\"IS_ECLIPSE\" value=\"false\" override=\"false\"/>");
    source.add(avtderniere++,
        "\t<!-- " + String.format("<Parameter name=\"LIBELLE_SERVEUR\" value=\"%s\" override=\"false\"/>", "#A COMPLETER#") + " -->");
    source.add(avtderniere++, String.format("\t<Parameter name=\"DOSSIER_RACINE_TOMCAT\" value=\"%s\" override=\"false\"/>",
        finstalltomcatfolder.getAbsolutePath() + File.separatorChar));
    source.add(avtderniere++, String.format("\t<Parameter name=\"DOSSIER_RACINE_PUBLIC\" value=\"%s\" override=\"false\"/>",
        finstalltomcatfolder.getParent() + File.separatorChar + "public" + File.separatorChar));
    source.add(avtderniere++, String.format("\t<Parameter name=\"DOSSIER_RACINE_PRIVATE\" value=\"%s\" override=\"false\"/>",
        finstalltomcatfolder.getParent() + File.separatorChar + "private" + File.separatorChar));
    source.add(avtderniere++, "");
    source.add(avtderniere++, "\t<!-- WEBSHOP -->");
    source.add(avtderniere++, String.format("\t<Parameter name=\"ADRESSE_AS400\" value=\"%s\" override=\"false\"/>", adresseIPlocale));
    source.add(avtderniere++,
        String.format("\t<Parameter name=\"DOSSIER_RACINE_XWEBSHOP\" value=\"%s\" override=\"false\"/>",
            finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "webapps" + File.separatorChar + NAME_WEBSHOP
                + File.separatorChar));
    
    // On réécrit le fichier modifié
    context.setContenuFichier(source);
    if (context.ecritureFichier() == Constantes.ERREUR) {
      Trace.erreur(context.getMsgErreur());
      return false;
    }
    return true;
  }
  
  /**
   * Modifie le fichier tomcat-users.xml.
   */
  private boolean configureTomcatUsers_XML(File finstalltomcatfolder) {
    if (profilManager == null || profilManager.isEmpty() || mdpManager == null || mdpManager.isEmpty()) {
      return true;
    }
    GestionFichierTexte tomcat_users = new GestionFichierTexte(
        finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "tomcat-users.xml");
    if (tomcat_users.lectureFichier() == Constantes.ERREUR) {
      Trace.erreur(tomcat_users.getMsgErreur());
      return false;
    }
    
    int indiceInsertion = -1;
    ArrayList<String> source = tomcat_users.getContenuFichier();
    // On parcoourt le source de la fin ver sle début afin de trouver le marqueur </tomcat-users>
    for (int ligne = source.size(); --ligne >= 0;) {
      if (source.get(ligne).trim().equals("</tomcat-users>")) {
        indiceInsertion = ligne--;
        break;
      }
    }
    // Si on a trouvé le marqueur alors on insère les lignes qui nous intéresse
    if (indiceInsertion > -1) {
      // Construction de la liste des information à insérer
      ArrayList<String> donneesManager = new ArrayList<String>();
      donneesManager.add(" <role rolename=\"manager\"/>");
      donneesManager.add(" <user username=\"" + profilManager + "\" password=\"" + mdpManager + "\" roles=\"manager\"/>");
      source.addAll(indiceInsertion, donneesManager);
      
      // On réécrit le fichier modifié
      tomcat_users.setContenuFichier(source);
      if (tomcat_users.ecritureFichier() == Constantes.ERREUR) {
        Trace.erreur(tomcat_users.getMsgErreur());
        return false;
      }
    }
    return true;
  }
  
  /**
   * Modifie le fichier logging.properties.
   */
  private boolean configureLogging_properties(File finstalltomcatfolder) {
    GestionFichierTexte logging = new GestionFichierTexte(
        finstalltomcatfolder.getAbsolutePath() + File.separatorChar + "conf" + File.separatorChar + "logging.properties");
    if (logging.lectureFichier() == Constantes.ERREUR) {
      Trace.erreur(logging.getMsgErreur());
      return false;
    }
    
    String[] contenu = logging.getContenuFichierTab();
    for (int i = 0; i < contenu.length; i++) {
      if (contenu[i].startsWith("handlers")) {
        contenu[i] = contenu[i].replace("handlers", "# handlers");
      }
      else if (contenu[i].startsWith(".handlers")) {
        contenu[i] = contenu[i].replace(".handlers", "handlers");
      }
      else if (contenu[i].startsWith("1catalina")) {
        contenu[i] = contenu[i].replace("1catalina", "#1catalina");
      }
      else if (contenu[i].startsWith("2localhost")) {
        contenu[i] = contenu[i].replace("2localhost", "#2localhost");
      }
      else if (contenu[i].startsWith("3manager")) {
        contenu[i] = contenu[i].replace("3manager", "#3manager");
      }
      else if (contenu[i].startsWith("4host")) {
        contenu[i] = contenu[i].replace("4host", "#4host");
      }
      else if (contenu[i].startsWith("org.")) {
        contenu[i] = contenu[i].replace("org.", "#org.");
      }
    }
    // On réécrit le fichier modifié
    logging.setContenuFichier(contenu);
    if (logging.ecritureFichier() == Constantes.ERREUR) {
      Trace.erreur(logging.getMsgErreur());
      return false;
    }
    
    return true;
  }
  
  /**
   * Modifie une ligne si nécessaire.
   */
  private String modifiePortLine(String line, String token, String confirm) {
    int pos = line.indexOf(token);
    if (pos == -1) {
      return null;
    }
    if (line.indexOf(confirm) == -1) {
      return null;
    }
    
    pos += token.length() + 2; // 3 pour la position des dizaines
    line = line.substring(0, pos) + digit + line.substring(pos + 1);
    
    return line;
  }
  
  /**
   * Créer le fichier setenv.sh dans le dossier bin du serveur Tomcat.
   */
  private boolean createSetEnv() {
    GestionFichierTexte gft = new GestionFichierTexte(
        installfolder + File.separatorChar + TOMCAT + File.separatorChar + "bin" + File.separatorChar + "setenv.sh");
    if (gft.isPresent()) {
      return true;
    }
    
    String[] data = new String[3];
    data[0] = "#!/bin/sh";
    data[1] = "JAVA_HOME=/QOpensys/QIBM/ProdData/JavaVM/jdk60/32bit";
    data[2] = "JRE_HOME=/QOpensys/QIBM/ProdData/JavaVM/jdk60/32bit/jre";
    gft.setContenuFichier(data);
    if (gft.ecritureFichier() == Constantes.ERREUR) {
      Trace.erreur(gft.getMsgErreur());
      return false;
    }
    return true;
  }
  
  /**
   * Partage du dossier dans l'IFS.
   */
  private boolean shareIFSFolder() {
    ISeriesNetServer ns = new ISeriesNetServer(system.getSystem());
    
    // On recherche si le dossier est déjà partagé
    String nameshare = library.toLowerCase();
    String namepath = null;
    boolean foundshare = false;
    boolean foundpath = false;
    try {
      ISeriesNetServerFileShare[] liste = ns.listFileShares();
      for (ISeriesNetServerFileShare elt : liste) {
        if (elt.getPath().equalsIgnoreCase(installfolder)) {
          foundpath = true;
        }
        if (elt.getName().equalsIgnoreCase(nameshare)) {
          foundshare = true;
          namepath = elt.getPath();
        }
      }
      if (!foundpath && !foundshare) {
        ns.createFileShare(nameshare, installfolder);
        Trace.info("Le dossier '" + installfolder + "' est partagé sous le nom '" + nameshare + "'");
      }
      else if (foundshare && foundpath) {
        Trace.info("Le dossier '" + installfolder + "' est déjà partagé sous le nom '" + nameshare + "'");
      }
      else if (foundshare) {
        Trace.info("Le dossier '" + namepath + "' utilise déjà le nom '" + nameshare + "'");
      }
      return true;
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du partage du dossier dans l'IFS");
      return false;
    }
  }
  
  /**
   * Téléchargement des fichiers war nécessaires.
   */
  private boolean downloadApplis() {
    ArrayList<Application> listapp = webinfos.getListapp();
    if (listapp.isEmpty()) {
      return true;
    }
    
    // Listage des fichiers war sur le serveur FTP
    String[] list = ftp.listFolderWithAttribute(infosFTP.getRepertoireFTP());
    if (list == null) {
      Trace.erreur("Erreur lors du listage du dossier " + infosFTP.getRepertoireFTP());
      return false;
    }
    // On découpe les attributs du listage (du serveur FTP)
    String[] date = retrieveInfos(list);
    
    // On compare la liste du serveur avec celle des applis à télécharger et on alimente les attributs
    int i = 0;
    while (i < listapp.size()) {
      int index = isFound(list, listapp.get(i).getName());
      if (index > -1) {
        listapp.get(i).setFile(list[index]);
        listapp.get(i).setLastModifiedOnFTP(date[index]);
        i++;
      }
      else {
        Trace.info("" + listapp.get(i).getName() + " non trouvé sur le serveur FTP");
        listapp.remove(i);
      }
    }
    if (listapp.isEmpty()) {
      Trace.erreur("Aucune application à télécharger");
      return true;
    }
    
    // Téléchargement des fichiers war
    folderwebapps = installfolder + File.separatorChar + TOMCAT + File.separatorChar + "webapps";
    boolean ret = true;
    File destination = null;
    for (Application war : listapp) {
      destination = new File(folderwebapps + File.separator + war.getFile());
      if (!war.getLastModified().equalsIgnoreCase(war.getLastModifiedOnFTP()) || !destination.exists()) {
        Trace.info("" + destination + " à télécharger");
        // TODO à améliorer comparaison juste
        ret = ftp.downloadFile(infosFTP.getRepertoireFTP() + war.getFile(), destination.getAbsolutePath(), true);
        if (!ret) {
          Trace.erreur(ftp.getMsgError());
        }
        else { // Mise à jour des infos dans la Dtaara
          webinfos.writeDate(war.getLastModifiedOnFTP(), 20, war.getOffsetLastModified());
        }
      }
      else {
        Trace.info("" + war.getName() + " est à jour");
      }
    }
    
    return ret;
  }
  
  /**
   * Recherche un fichier dans une liste.
   */
  private int isFound(String[] listremote, String filetofind) {
    filetofind = filetofind.toUpperCase();
    int i = 0;
    for (String war : listremote) {
      if (war.toUpperCase().equals(filetofind)) {
        return i;
      }
      i++;
    }
    return -1;
  }
  
  /**
   * Retrouve le nom du fichier et la date dans les attributs.
   */
  private String[] retrieveInfos(String[] list) {
    String[] attributs = new String[list.length];
    
    for (int i = 0; i < list.length; i++) {
      // Recherche du nom
      int pos = list[i].lastIndexOf(' ');
      int posnom = list[i].indexOf(' ', pos); // TODO a corrigé
      String chaine = list[i].substring(posnom + 1);
      // Recherche de la date
      for (int j = pos - 1; j != 0; j--) {
        if (MONTH.contains(list[i].charAt(j))) {
          pos = j;
          break;
        }
      }
      attributs[i] = list[i].substring(pos, posnom);
      list[i] = chaine;
    }
    return attributs;
  }
  
  /**
   * Déconnexion de tout.
   */
  public void dispose() {
    webinfos.resetToDo();
    system.deconnecter();
    if (ftp != null) {
      ftp.disconnect();
    }
  }
  
  /**
   * @param args.
   */
  public static void main(String[] args) {
    if (args.length != 9) {
      System.exit(-1);
    }
    
    Installation inst = new Installation(args[0].charAt(0), args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8]);
    inst.treatment();
    inst.dispose();
  }
  
}
